// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions




/*template<typename T> void swap(T& x, T& y)
{
        T tmp = x;
        x = y;
        y = tmp;
}*/



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#pragma warning(disable : 4996)

#include <dshow.h>
#include <strsafe.h>
//#include <qedit.h>
#pragma warning( disable: 4049 )  /* more than 64k source lines */

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0338 */
/* Compiler settings for qedit.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __qedit_h__
#define __qedit_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IPropertySetter_FWD_DEFINED__
#define __IPropertySetter_FWD_DEFINED__
typedef interface IPropertySetter IPropertySetter;
#endif 	/* __IPropertySetter_FWD_DEFINED__ */


#ifndef __IDxtCompositor_FWD_DEFINED__
#define __IDxtCompositor_FWD_DEFINED__
typedef interface IDxtCompositor IDxtCompositor;
#endif 	/* __IDxtCompositor_FWD_DEFINED__ */


#ifndef __IDxtAlphaSetter_FWD_DEFINED__
#define __IDxtAlphaSetter_FWD_DEFINED__
typedef interface IDxtAlphaSetter IDxtAlphaSetter;
#endif 	/* __IDxtAlphaSetter_FWD_DEFINED__ */


#ifndef __IDxtJpeg_FWD_DEFINED__
#define __IDxtJpeg_FWD_DEFINED__
typedef interface IDxtJpeg IDxtJpeg;
#endif 	/* __IDxtJpeg_FWD_DEFINED__ */


#ifndef __IDxtKey_FWD_DEFINED__
#define __IDxtKey_FWD_DEFINED__
typedef interface IDxtKey IDxtKey;
#endif 	/* __IDxtKey_FWD_DEFINED__ */


#ifndef __IMediaLocator_FWD_DEFINED__
#define __IMediaLocator_FWD_DEFINED__
typedef interface IMediaLocator IMediaLocator;
#endif 	/* __IMediaLocator_FWD_DEFINED__ */


#ifndef __IMediaDet_FWD_DEFINED__
#define __IMediaDet_FWD_DEFINED__
typedef interface IMediaDet IMediaDet;
#endif 	/* __IMediaDet_FWD_DEFINED__ */


#ifndef __IGrfCache_FWD_DEFINED__
#define __IGrfCache_FWD_DEFINED__
typedef interface IGrfCache IGrfCache;
#endif 	/* __IGrfCache_FWD_DEFINED__ */


#ifndef __IRenderEngine_FWD_DEFINED__
#define __IRenderEngine_FWD_DEFINED__
typedef interface IRenderEngine IRenderEngine;
#endif 	/* __IRenderEngine_FWD_DEFINED__ */


#ifndef __IFindCompressorCB_FWD_DEFINED__
#define __IFindCompressorCB_FWD_DEFINED__
typedef interface IFindCompressorCB IFindCompressorCB;
#endif 	/* __IFindCompressorCB_FWD_DEFINED__ */


#ifndef __ISmartRenderEngine_FWD_DEFINED__
#define __ISmartRenderEngine_FWD_DEFINED__
typedef interface ISmartRenderEngine ISmartRenderEngine;
#endif 	/* __ISmartRenderEngine_FWD_DEFINED__ */


#ifndef __IAMTimelineObj_FWD_DEFINED__
#define __IAMTimelineObj_FWD_DEFINED__
typedef interface IAMTimelineObj IAMTimelineObj;
#endif 	/* __IAMTimelineObj_FWD_DEFINED__ */


#ifndef __IAMTimelineEffectable_FWD_DEFINED__
#define __IAMTimelineEffectable_FWD_DEFINED__
typedef interface IAMTimelineEffectable IAMTimelineEffectable;
#endif 	/* __IAMTimelineEffectable_FWD_DEFINED__ */


#ifndef __IAMTimelineEffect_FWD_DEFINED__
#define __IAMTimelineEffect_FWD_DEFINED__
typedef interface IAMTimelineEffect IAMTimelineEffect;
#endif 	/* __IAMTimelineEffect_FWD_DEFINED__ */


#ifndef __IAMTimelineTransable_FWD_DEFINED__
#define __IAMTimelineTransable_FWD_DEFINED__
typedef interface IAMTimelineTransable IAMTimelineTransable;
#endif 	/* __IAMTimelineTransable_FWD_DEFINED__ */


#ifndef __IAMTimelineSplittable_FWD_DEFINED__
#define __IAMTimelineSplittable_FWD_DEFINED__
typedef interface IAMTimelineSplittable IAMTimelineSplittable;
#endif 	/* __IAMTimelineSplittable_FWD_DEFINED__ */


#ifndef __IAMTimelineTrans_FWD_DEFINED__
#define __IAMTimelineTrans_FWD_DEFINED__
typedef interface IAMTimelineTrans IAMTimelineTrans;
#endif 	/* __IAMTimelineTrans_FWD_DEFINED__ */


#ifndef __IAMTimelineSrc_FWD_DEFINED__
#define __IAMTimelineSrc_FWD_DEFINED__
typedef interface IAMTimelineSrc IAMTimelineSrc;
#endif 	/* __IAMTimelineSrc_FWD_DEFINED__ */


#ifndef __IAMTimelineTrack_FWD_DEFINED__
#define __IAMTimelineTrack_FWD_DEFINED__
typedef interface IAMTimelineTrack IAMTimelineTrack;
#endif 	/* __IAMTimelineTrack_FWD_DEFINED__ */


#ifndef __IAMTimelineVirtualTrack_FWD_DEFINED__
#define __IAMTimelineVirtualTrack_FWD_DEFINED__
typedef interface IAMTimelineVirtualTrack IAMTimelineVirtualTrack;
#endif 	/* __IAMTimelineVirtualTrack_FWD_DEFINED__ */


#ifndef __IAMTimelineComp_FWD_DEFINED__
#define __IAMTimelineComp_FWD_DEFINED__
typedef interface IAMTimelineComp IAMTimelineComp;
#endif 	/* __IAMTimelineComp_FWD_DEFINED__ */


#ifndef __IAMTimelineGroup_FWD_DEFINED__
#define __IAMTimelineGroup_FWD_DEFINED__
typedef interface IAMTimelineGroup IAMTimelineGroup;
#endif 	/* __IAMTimelineGroup_FWD_DEFINED__ */


#ifndef __IAMTimeline_FWD_DEFINED__
#define __IAMTimeline_FWD_DEFINED__
typedef interface IAMTimeline IAMTimeline;
#endif 	/* __IAMTimeline_FWD_DEFINED__ */


#ifndef __IXml2Dex_FWD_DEFINED__
#define __IXml2Dex_FWD_DEFINED__
typedef interface IXml2Dex IXml2Dex;
#endif 	/* __IXml2Dex_FWD_DEFINED__ */


#ifndef __IAMErrorLog_FWD_DEFINED__
#define __IAMErrorLog_FWD_DEFINED__
typedef interface IAMErrorLog IAMErrorLog;
#endif 	/* __IAMErrorLog_FWD_DEFINED__ */


#ifndef __IAMSetErrorLog_FWD_DEFINED__
#define __IAMSetErrorLog_FWD_DEFINED__
typedef interface IAMSetErrorLog IAMSetErrorLog;
#endif 	/* __IAMSetErrorLog_FWD_DEFINED__ */


#ifndef __ISampleGrabberCB_FWD_DEFINED__
#define __ISampleGrabberCB_FWD_DEFINED__
typedef interface ISampleGrabberCB ISampleGrabberCB;
#endif 	/* __ISampleGrabberCB_FWD_DEFINED__ */


#ifndef __ISampleGrabber_FWD_DEFINED__
#define __ISampleGrabber_FWD_DEFINED__
typedef interface ISampleGrabber ISampleGrabber;
#endif 	/* __ISampleGrabber_FWD_DEFINED__ */


#ifndef __AMTimeline_FWD_DEFINED__
#define __AMTimeline_FWD_DEFINED__

#ifdef __cplusplus
typedef class AMTimeline AMTimeline;
#else
typedef struct AMTimeline AMTimeline;
#endif /* __cplusplus */

#endif 	/* __AMTimeline_FWD_DEFINED__ */


#ifndef __AMTimelineObj_FWD_DEFINED__
#define __AMTimelineObj_FWD_DEFINED__

#ifdef __cplusplus
typedef class AMTimelineObj AMTimelineObj;
#else
typedef struct AMTimelineObj AMTimelineObj;
#endif /* __cplusplus */

#endif 	/* __AMTimelineObj_FWD_DEFINED__ */


#ifndef __AMTimelineSrc_FWD_DEFINED__
#define __AMTimelineSrc_FWD_DEFINED__

#ifdef __cplusplus
typedef class AMTimelineSrc AMTimelineSrc;
#else
typedef struct AMTimelineSrc AMTimelineSrc;
#endif /* __cplusplus */

#endif 	/* __AMTimelineSrc_FWD_DEFINED__ */


#ifndef __AMTimelineTrack_FWD_DEFINED__
#define __AMTimelineTrack_FWD_DEFINED__

#ifdef __cplusplus
typedef class AMTimelineTrack AMTimelineTrack;
#else
typedef struct AMTimelineTrack AMTimelineTrack;
#endif /* __cplusplus */

#endif 	/* __AMTimelineTrack_FWD_DEFINED__ */


#ifndef __AMTimelineComp_FWD_DEFINED__
#define __AMTimelineComp_FWD_DEFINED__

#ifdef __cplusplus
typedef class AMTimelineComp AMTimelineComp;
#else
typedef struct AMTimelineComp AMTimelineComp;
#endif /* __cplusplus */

#endif 	/* __AMTimelineComp_FWD_DEFINED__ */


#ifndef __AMTimelineGroup_FWD_DEFINED__
#define __AMTimelineGroup_FWD_DEFINED__

#ifdef __cplusplus
typedef class AMTimelineGroup AMTimelineGroup;
#else
typedef struct AMTimelineGroup AMTimelineGroup;
#endif /* __cplusplus */

#endif 	/* __AMTimelineGroup_FWD_DEFINED__ */


#ifndef __AMTimelineTrans_FWD_DEFINED__
#define __AMTimelineTrans_FWD_DEFINED__

#ifdef __cplusplus
typedef class AMTimelineTrans AMTimelineTrans;
#else
typedef struct AMTimelineTrans AMTimelineTrans;
#endif /* __cplusplus */

#endif 	/* __AMTimelineTrans_FWD_DEFINED__ */


#ifndef __AMTimelineEffect_FWD_DEFINED__
#define __AMTimelineEffect_FWD_DEFINED__

#ifdef __cplusplus
typedef class AMTimelineEffect AMTimelineEffect;
#else
typedef struct AMTimelineEffect AMTimelineEffect;
#endif /* __cplusplus */

#endif 	/* __AMTimelineEffect_FWD_DEFINED__ */


#ifndef __RenderEngine_FWD_DEFINED__
#define __RenderEngine_FWD_DEFINED__

#ifdef __cplusplus
typedef class RenderEngine RenderEngine;
#else
typedef struct RenderEngine RenderEngine;
#endif /* __cplusplus */

#endif 	/* __RenderEngine_FWD_DEFINED__ */


#ifndef __SmartRenderEngine_FWD_DEFINED__
#define __SmartRenderEngine_FWD_DEFINED__

#ifdef __cplusplus
typedef class SmartRenderEngine SmartRenderEngine;
#else
typedef struct SmartRenderEngine SmartRenderEngine;
#endif /* __cplusplus */

#endif 	/* __SmartRenderEngine_FWD_DEFINED__ */


#ifndef __AudMixer_FWD_DEFINED__
#define __AudMixer_FWD_DEFINED__

#ifdef __cplusplus
typedef class AudMixer AudMixer;
#else
typedef struct AudMixer AudMixer;
#endif /* __cplusplus */

#endif 	/* __AudMixer_FWD_DEFINED__ */


#ifndef __Xml2Dex_FWD_DEFINED__
#define __Xml2Dex_FWD_DEFINED__

#ifdef __cplusplus
typedef class Xml2Dex Xml2Dex;
#else
typedef struct Xml2Dex Xml2Dex;
#endif /* __cplusplus */

#endif 	/* __Xml2Dex_FWD_DEFINED__ */


#ifndef __MediaLocator_FWD_DEFINED__
#define __MediaLocator_FWD_DEFINED__

#ifdef __cplusplus
typedef class MediaLocator MediaLocator;
#else
typedef struct MediaLocator MediaLocator;
#endif /* __cplusplus */

#endif 	/* __MediaLocator_FWD_DEFINED__ */


#ifndef __PropertySetter_FWD_DEFINED__
#define __PropertySetter_FWD_DEFINED__

#ifdef __cplusplus
typedef class PropertySetter PropertySetter;
#else
typedef struct PropertySetter PropertySetter;
#endif /* __cplusplus */

#endif 	/* __PropertySetter_FWD_DEFINED__ */


#ifndef __MediaDet_FWD_DEFINED__
#define __MediaDet_FWD_DEFINED__

#ifdef __cplusplus
typedef class MediaDet MediaDet;
#else
typedef struct MediaDet MediaDet;
#endif /* __cplusplus */

#endif 	/* __MediaDet_FWD_DEFINED__ */


#ifndef __SampleGrabber_FWD_DEFINED__
#define __SampleGrabber_FWD_DEFINED__

#ifdef __cplusplus
typedef class SampleGrabber SampleGrabber;
#else
typedef struct SampleGrabber SampleGrabber;
#endif /* __cplusplus */

#endif 	/* __SampleGrabber_FWD_DEFINED__ */


#ifndef __NullRenderer_FWD_DEFINED__
#define __NullRenderer_FWD_DEFINED__

#ifdef __cplusplus
typedef class NullRenderer NullRenderer;
#else
typedef struct NullRenderer NullRenderer;
#endif /* __cplusplus */

#endif 	/* __NullRenderer_FWD_DEFINED__ */


#ifndef __DxtCompositor_FWD_DEFINED__
#define __DxtCompositor_FWD_DEFINED__

#ifdef __cplusplus
typedef class DxtCompositor DxtCompositor;
#else
typedef struct DxtCompositor DxtCompositor;
#endif /* __cplusplus */

#endif 	/* __DxtCompositor_FWD_DEFINED__ */


#ifndef __DxtAlphaSetter_FWD_DEFINED__
#define __DxtAlphaSetter_FWD_DEFINED__

#ifdef __cplusplus
typedef class DxtAlphaSetter DxtAlphaSetter;
#else
typedef struct DxtAlphaSetter DxtAlphaSetter;
#endif /* __cplusplus */

#endif 	/* __DxtAlphaSetter_FWD_DEFINED__ */


#ifndef __DxtJpeg_FWD_DEFINED__
#define __DxtJpeg_FWD_DEFINED__

#ifdef __cplusplus
typedef class DxtJpeg DxtJpeg;
#else
typedef struct DxtJpeg DxtJpeg;
#endif /* __cplusplus */

#endif 	/* __DxtJpeg_FWD_DEFINED__ */


#ifndef __ColorSource_FWD_DEFINED__
#define __ColorSource_FWD_DEFINED__

#ifdef __cplusplus
typedef class ColorSource ColorSource;
#else
typedef struct ColorSource ColorSource;
#endif /* __cplusplus */

#endif 	/* __ColorSource_FWD_DEFINED__ */


#ifndef __DxtKey_FWD_DEFINED__
#define __DxtKey_FWD_DEFINED__

#ifdef __cplusplus
typedef class DxtKey DxtKey;
#else
typedef struct DxtKey DxtKey;
#endif /* __cplusplus */

#endif 	/* __DxtKey_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
//#include "dxtrans.h"
#pragma warning( disable: 4049 )  /* more than 64k source lines */

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0334 */
/* Compiler settings for dxtrans.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __dxtrans_h__
#define __dxtrans_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IDXBaseObject_FWD_DEFINED__
#define __IDXBaseObject_FWD_DEFINED__
typedef interface IDXBaseObject IDXBaseObject;
#endif 	/* __IDXBaseObject_FWD_DEFINED__ */


#ifndef __IDXTransformFactory_FWD_DEFINED__
#define __IDXTransformFactory_FWD_DEFINED__
typedef interface IDXTransformFactory IDXTransformFactory;
#endif 	/* __IDXTransformFactory_FWD_DEFINED__ */


#ifndef __IDXTransform_FWD_DEFINED__
#define __IDXTransform_FWD_DEFINED__
typedef interface IDXTransform IDXTransform;
#endif 	/* __IDXTransform_FWD_DEFINED__ */


#ifndef __IDXSurfacePick_FWD_DEFINED__
#define __IDXSurfacePick_FWD_DEFINED__
typedef interface IDXSurfacePick IDXSurfacePick;
#endif 	/* __IDXSurfacePick_FWD_DEFINED__ */


#ifndef __IDXTBindHost_FWD_DEFINED__
#define __IDXTBindHost_FWD_DEFINED__
typedef interface IDXTBindHost IDXTBindHost;
#endif 	/* __IDXTBindHost_FWD_DEFINED__ */


#ifndef __IDXTaskManager_FWD_DEFINED__
#define __IDXTaskManager_FWD_DEFINED__
typedef interface IDXTaskManager IDXTaskManager;
#endif 	/* __IDXTaskManager_FWD_DEFINED__ */


#ifndef __IDXSurfaceFactory_FWD_DEFINED__
#define __IDXSurfaceFactory_FWD_DEFINED__
typedef interface IDXSurfaceFactory IDXSurfaceFactory;
#endif 	/* __IDXSurfaceFactory_FWD_DEFINED__ */


#ifndef __IDXSurfaceModifier_FWD_DEFINED__
#define __IDXSurfaceModifier_FWD_DEFINED__
typedef interface IDXSurfaceModifier IDXSurfaceModifier;
#endif 	/* __IDXSurfaceModifier_FWD_DEFINED__ */


#ifndef __IDXSurface_FWD_DEFINED__
#define __IDXSurface_FWD_DEFINED__
typedef interface IDXSurface IDXSurface;
#endif 	/* __IDXSurface_FWD_DEFINED__ */


#ifndef __IDXSurfaceInit_FWD_DEFINED__
#define __IDXSurfaceInit_FWD_DEFINED__
typedef interface IDXSurfaceInit IDXSurfaceInit;
#endif 	/* __IDXSurfaceInit_FWD_DEFINED__ */


#ifndef __IDXARGBSurfaceInit_FWD_DEFINED__
#define __IDXARGBSurfaceInit_FWD_DEFINED__
typedef interface IDXARGBSurfaceInit IDXARGBSurfaceInit;
#endif 	/* __IDXARGBSurfaceInit_FWD_DEFINED__ */


#ifndef __IDXARGBReadPtr_FWD_DEFINED__
#define __IDXARGBReadPtr_FWD_DEFINED__
typedef interface IDXARGBReadPtr IDXARGBReadPtr;
#endif 	/* __IDXARGBReadPtr_FWD_DEFINED__ */


#ifndef __IDXARGBReadWritePtr_FWD_DEFINED__
#define __IDXARGBReadWritePtr_FWD_DEFINED__
typedef interface IDXARGBReadWritePtr IDXARGBReadWritePtr;
#endif 	/* __IDXARGBReadWritePtr_FWD_DEFINED__ */


#ifndef __IDXDCLock_FWD_DEFINED__
#define __IDXDCLock_FWD_DEFINED__
typedef interface IDXDCLock IDXDCLock;
#endif 	/* __IDXDCLock_FWD_DEFINED__ */


#ifndef __IDXTScaleOutput_FWD_DEFINED__
#define __IDXTScaleOutput_FWD_DEFINED__
typedef interface IDXTScaleOutput IDXTScaleOutput;
#endif 	/* __IDXTScaleOutput_FWD_DEFINED__ */


#ifndef __IDXGradient_FWD_DEFINED__
#define __IDXGradient_FWD_DEFINED__
typedef interface IDXGradient IDXGradient;
#endif 	/* __IDXGradient_FWD_DEFINED__ */


#ifndef __IDXTScale_FWD_DEFINED__
#define __IDXTScale_FWD_DEFINED__
typedef interface IDXTScale IDXTScale;
#endif 	/* __IDXTScale_FWD_DEFINED__ */


#ifndef __IDXEffect_FWD_DEFINED__
#define __IDXEffect_FWD_DEFINED__
typedef interface IDXEffect IDXEffect;
#endif 	/* __IDXEffect_FWD_DEFINED__ */


#ifndef __IDXLookupTable_FWD_DEFINED__
#define __IDXLookupTable_FWD_DEFINED__
typedef interface IDXLookupTable IDXLookupTable;
#endif 	/* __IDXLookupTable_FWD_DEFINED__ */


#ifndef __IDXRawSurface_FWD_DEFINED__
#define __IDXRawSurface_FWD_DEFINED__
typedef interface IDXRawSurface IDXRawSurface;
#endif 	/* __IDXRawSurface_FWD_DEFINED__ */


#ifndef __IHTMLDXTransform_FWD_DEFINED__
#define __IHTMLDXTransform_FWD_DEFINED__
typedef interface IHTMLDXTransform IHTMLDXTransform;
#endif 	/* __IHTMLDXTransform_FWD_DEFINED__ */


#ifndef __DXTransformFactory_FWD_DEFINED__
#define __DXTransformFactory_FWD_DEFINED__

#ifdef __cplusplus
typedef class DXTransformFactory DXTransformFactory;
#else
typedef struct DXTransformFactory DXTransformFactory;
#endif /* __cplusplus */

#endif 	/* __DXTransformFactory_FWD_DEFINED__ */


#ifndef __DXTaskManager_FWD_DEFINED__
#define __DXTaskManager_FWD_DEFINED__

#ifdef __cplusplus
typedef class DXTaskManager DXTaskManager;
#else
typedef struct DXTaskManager DXTaskManager;
#endif /* __cplusplus */

#endif 	/* __DXTaskManager_FWD_DEFINED__ */


#ifndef __DXTScale_FWD_DEFINED__
#define __DXTScale_FWD_DEFINED__

#ifdef __cplusplus
typedef class DXTScale DXTScale;
#else
typedef struct DXTScale DXTScale;
#endif /* __cplusplus */

#endif 	/* __DXTScale_FWD_DEFINED__ */


#ifndef __DXSurface_FWD_DEFINED__
#define __DXSurface_FWD_DEFINED__

#ifdef __cplusplus
typedef class DXSurface DXSurface;
#else
typedef struct DXSurface DXSurface;
#endif /* __cplusplus */

#endif 	/* __DXSurface_FWD_DEFINED__ */


#ifndef __DXSurfaceModifier_FWD_DEFINED__
#define __DXSurfaceModifier_FWD_DEFINED__

#ifdef __cplusplus
typedef class DXSurfaceModifier DXSurfaceModifier;
#else
typedef struct DXSurfaceModifier DXSurfaceModifier;
#endif /* __cplusplus */

#endif 	/* __DXSurfaceModifier_FWD_DEFINED__ */


#ifndef __DXGradient_FWD_DEFINED__
#define __DXGradient_FWD_DEFINED__

#ifdef __cplusplus
typedef class DXGradient DXGradient;
#else
typedef struct DXGradient DXGradient;
#endif /* __cplusplus */

#endif 	/* __DXGradient_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "comcat.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

/* interface __MIDL_itf_dxtrans_0000 */
/* [local] */ 

#include <servprov.h>
#include <ddraw.h>
//#include <d3d.h>
#pragma warning( disable: 4049 )  /* more than 64k source lines */

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0334 */
/* Compiler settings for dxtrans.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __dxtrans_h__
#define __dxtrans_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IDXBaseObject_FWD_DEFINED__
#define __IDXBaseObject_FWD_DEFINED__
typedef interface IDXBaseObject IDXBaseObject;
#endif 	/* __IDXBaseObject_FWD_DEFINED__ */


#ifndef __IDXTransformFactory_FWD_DEFINED__
#define __IDXTransformFactory_FWD_DEFINED__
typedef interface IDXTransformFactory IDXTransformFactory;
#endif 	/* __IDXTransformFactory_FWD_DEFINED__ */


#ifndef __IDXTransform_FWD_DEFINED__
#define __IDXTransform_FWD_DEFINED__
typedef interface IDXTransform IDXTransform;
#endif 	/* __IDXTransform_FWD_DEFINED__ */


#ifndef __IDXSurfacePick_FWD_DEFINED__
#define __IDXSurfacePick_FWD_DEFINED__
typedef interface IDXSurfacePick IDXSurfacePick;
#endif 	/* __IDXSurfacePick_FWD_DEFINED__ */


#ifndef __IDXTBindHost_FWD_DEFINED__
#define __IDXTBindHost_FWD_DEFINED__
typedef interface IDXTBindHost IDXTBindHost;
#endif 	/* __IDXTBindHost_FWD_DEFINED__ */


#ifndef __IDXTaskManager_FWD_DEFINED__
#define __IDXTaskManager_FWD_DEFINED__
typedef interface IDXTaskManager IDXTaskManager;
#endif 	/* __IDXTaskManager_FWD_DEFINED__ */


#ifndef __IDXSurfaceFactory_FWD_DEFINED__
#define __IDXSurfaceFactory_FWD_DEFINED__
typedef interface IDXSurfaceFactory IDXSurfaceFactory;
#endif 	/* __IDXSurfaceFactory_FWD_DEFINED__ */


#ifndef __IDXSurfaceModifier_FWD_DEFINED__
#define __IDXSurfaceModifier_FWD_DEFINED__
typedef interface IDXSurfaceModifier IDXSurfaceModifier;
#endif 	/* __IDXSurfaceModifier_FWD_DEFINED__ */


#ifndef __IDXSurface_FWD_DEFINED__
#define __IDXSurface_FWD_DEFINED__
typedef interface IDXSurface IDXSurface;
#endif 	/* __IDXSurface_FWD_DEFINED__ */


#ifndef __IDXSurfaceInit_FWD_DEFINED__
#define __IDXSurfaceInit_FWD_DEFINED__
typedef interface IDXSurfaceInit IDXSurfaceInit;
#endif 	/* __IDXSurfaceInit_FWD_DEFINED__ */


#ifndef __IDXARGBSurfaceInit_FWD_DEFINED__
#define __IDXARGBSurfaceInit_FWD_DEFINED__
typedef interface IDXARGBSurfaceInit IDXARGBSurfaceInit;
#endif 	/* __IDXARGBSurfaceInit_FWD_DEFINED__ */


#ifndef __IDXARGBReadPtr_FWD_DEFINED__
#define __IDXARGBReadPtr_FWD_DEFINED__
typedef interface IDXARGBReadPtr IDXARGBReadPtr;
#endif 	/* __IDXARGBReadPtr_FWD_DEFINED__ */


#ifndef __IDXARGBReadWritePtr_FWD_DEFINED__
#define __IDXARGBReadWritePtr_FWD_DEFINED__
typedef interface IDXARGBReadWritePtr IDXARGBReadWritePtr;
#endif 	/* __IDXARGBReadWritePtr_FWD_DEFINED__ */


#ifndef __IDXDCLock_FWD_DEFINED__
#define __IDXDCLock_FWD_DEFINED__
typedef interface IDXDCLock IDXDCLock;
#endif 	/* __IDXDCLock_FWD_DEFINED__ */


#ifndef __IDXTScaleOutput_FWD_DEFINED__
#define __IDXTScaleOutput_FWD_DEFINED__
typedef interface IDXTScaleOutput IDXTScaleOutput;
#endif 	/* __IDXTScaleOutput_FWD_DEFINED__ */


#ifndef __IDXGradient_FWD_DEFINED__
#define __IDXGradient_FWD_DEFINED__
typedef interface IDXGradient IDXGradient;
#endif 	/* __IDXGradient_FWD_DEFINED__ */


#ifndef __IDXTScale_FWD_DEFINED__
#define __IDXTScale_FWD_DEFINED__
typedef interface IDXTScale IDXTScale;
#endif 	/* __IDXTScale_FWD_DEFINED__ */


#ifndef __IDXEffect_FWD_DEFINED__
#define __IDXEffect_FWD_DEFINED__
typedef interface IDXEffect IDXEffect;
#endif 	/* __IDXEffect_FWD_DEFINED__ */


#ifndef __IDXLookupTable_FWD_DEFINED__
#define __IDXLookupTable_FWD_DEFINED__
typedef interface IDXLookupTable IDXLookupTable;
#endif 	/* __IDXLookupTable_FWD_DEFINED__ */


#ifndef __IDXRawSurface_FWD_DEFINED__
#define __IDXRawSurface_FWD_DEFINED__
typedef interface IDXRawSurface IDXRawSurface;
#endif 	/* __IDXRawSurface_FWD_DEFINED__ */


#ifndef __IHTMLDXTransform_FWD_DEFINED__
#define __IHTMLDXTransform_FWD_DEFINED__
typedef interface IHTMLDXTransform IHTMLDXTransform;
#endif 	/* __IHTMLDXTransform_FWD_DEFINED__ */


#ifndef __DXTransformFactory_FWD_DEFINED__
#define __DXTransformFactory_FWD_DEFINED__

#ifdef __cplusplus
typedef class DXTransformFactory DXTransformFactory;
#else
typedef struct DXTransformFactory DXTransformFactory;
#endif /* __cplusplus */

#endif 	/* __DXTransformFactory_FWD_DEFINED__ */


#ifndef __DXTaskManager_FWD_DEFINED__
#define __DXTaskManager_FWD_DEFINED__

#ifdef __cplusplus
typedef class DXTaskManager DXTaskManager;
#else
typedef struct DXTaskManager DXTaskManager;
#endif /* __cplusplus */

#endif 	/* __DXTaskManager_FWD_DEFINED__ */


#ifndef __DXTScale_FWD_DEFINED__
#define __DXTScale_FWD_DEFINED__

#ifdef __cplusplus
typedef class DXTScale DXTScale;
#else
typedef struct DXTScale DXTScale;
#endif /* __cplusplus */

#endif 	/* __DXTScale_FWD_DEFINED__ */


#ifndef __DXSurface_FWD_DEFINED__
#define __DXSurface_FWD_DEFINED__

#ifdef __cplusplus
typedef class DXSurface DXSurface;
#else
typedef struct DXSurface DXSurface;
#endif /* __cplusplus */

#endif 	/* __DXSurface_FWD_DEFINED__ */


#ifndef __DXSurfaceModifier_FWD_DEFINED__
#define __DXSurfaceModifier_FWD_DEFINED__

#ifdef __cplusplus
typedef class DXSurfaceModifier DXSurfaceModifier;
#else
typedef struct DXSurfaceModifier DXSurfaceModifier;
#endif /* __cplusplus */

#endif 	/* __DXSurfaceModifier_FWD_DEFINED__ */


#ifndef __DXGradient_FWD_DEFINED__
#define __DXGradient_FWD_DEFINED__

#ifdef __cplusplus
typedef class DXGradient DXGradient;
#else
typedef struct DXGradient DXGradient;
#endif /* __cplusplus */

#endif 	/* __DXGradient_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "comcat.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

/* interface __MIDL_itf_dxtrans_0000 */
/* [local] */ 

#include <servprov.h>
#include <ddraw.h>
#include <d3d.h>
#include <d3drm.h>
#include <urlmon.h>
#if 0
// Bogus definition used to make MIDL compiler happy
typedef void DDSURFACEDESC;

typedef void D3DRMBOX;

typedef void D3DVECTOR;

typedef void D3DRMMATRIX4D;

typedef void *LPSECURITY_ATTRIBUTES;

#endif
#ifdef _DXTRANSIMPL
    #define _DXTRANS_IMPL_EXT _declspec(dllexport)
#else
    #define _DXTRANS_IMPL_EXT _declspec(dllimport)
#endif
















//
//   All GUIDs for DXTransform are declared in DXTGUID.C in the SDK include directory
//
EXTERN_C const GUID DDPF_RGB1;
EXTERN_C const GUID DDPF_RGB2;
EXTERN_C const GUID DDPF_RGB4;
EXTERN_C const GUID DDPF_RGB8;
EXTERN_C const GUID DDPF_RGB332;
EXTERN_C const GUID DDPF_ARGB4444;
EXTERN_C const GUID DDPF_RGB565;
EXTERN_C const GUID DDPF_BGR565;
EXTERN_C const GUID DDPF_RGB555;
EXTERN_C const GUID DDPF_ARGB1555;
EXTERN_C const GUID DDPF_RGB24;
EXTERN_C const GUID DDPF_BGR24;
EXTERN_C const GUID DDPF_RGB32;
EXTERN_C const GUID DDPF_BGR32;
EXTERN_C const GUID DDPF_ABGR32;
EXTERN_C const GUID DDPF_ARGB32;
EXTERN_C const GUID DDPF_PMARGB32;
EXTERN_C const GUID DDPF_A1;
EXTERN_C const GUID DDPF_A2;
EXTERN_C const GUID DDPF_A4;
EXTERN_C const GUID DDPF_A8;
EXTERN_C const GUID DDPF_Z8;
EXTERN_C const GUID DDPF_Z16;
EXTERN_C const GUID DDPF_Z24;
EXTERN_C const GUID DDPF_Z32;
//
//   Component categories
//
EXTERN_C const GUID CATID_DXImageTransform;
EXTERN_C const GUID CATID_DX3DTransform;
EXTERN_C const GUID CATID_DXAuthoringTransform;
EXTERN_C const GUID CATID_DXSurface;
//
//   Service IDs
//
EXTERN_C const GUID SID_SDirectDraw;
EXTERN_C const GUID SID_SDirect3DRM;
#define SID_SDXTaskManager CLSID_DXTaskManager
#define SID_SDXSurfaceFactory IID_IDXSurfaceFactory
#define SID_SDXTransformFactory IID_IDXTransformFactory


extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0000_v0_0_s_ifspec;

#ifndef __IDXBaseObject_INTERFACE_DEFINED__
#define __IDXBaseObject_INTERFACE_DEFINED__

/* interface IDXBaseObject */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXBaseObject;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("17B59B2B-9CC8-11d1-9053-00C04FD9189D")
    IDXBaseObject : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetGenerationId( 
            /* [out] */ ULONG *pID) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IncrementGenerationId( 
            /* [in] */ BOOL bRefresh) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetObjectSize( 
            /* [out] */ ULONG *pcbSize) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXBaseObjectVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXBaseObject * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXBaseObject * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXBaseObject * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetGenerationId )( 
            IDXBaseObject * This,
            /* [out] */ ULONG *pID);
        
        HRESULT ( STDMETHODCALLTYPE *IncrementGenerationId )( 
            IDXBaseObject * This,
            /* [in] */ BOOL bRefresh);
        
        HRESULT ( STDMETHODCALLTYPE *GetObjectSize )( 
            IDXBaseObject * This,
            /* [out] */ ULONG *pcbSize);
        
        END_INTERFACE
    } IDXBaseObjectVtbl;

    interface IDXBaseObject
    {
        CONST_VTBL struct IDXBaseObjectVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXBaseObject_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXBaseObject_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXBaseObject_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXBaseObject_GetGenerationId(This,pID)	\
    (This)->lpVtbl -> GetGenerationId(This,pID)

#define IDXBaseObject_IncrementGenerationId(This,bRefresh)	\
    (This)->lpVtbl -> IncrementGenerationId(This,bRefresh)

#define IDXBaseObject_GetObjectSize(This,pcbSize)	\
    (This)->lpVtbl -> GetObjectSize(This,pcbSize)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXBaseObject_GetGenerationId_Proxy( 
    IDXBaseObject * This,
    /* [out] */ ULONG *pID);


void __RPC_STUB IDXBaseObject_GetGenerationId_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXBaseObject_IncrementGenerationId_Proxy( 
    IDXBaseObject * This,
    /* [in] */ BOOL bRefresh);


void __RPC_STUB IDXBaseObject_IncrementGenerationId_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXBaseObject_GetObjectSize_Proxy( 
    IDXBaseObject * This,
    /* [out] */ ULONG *pcbSize);


void __RPC_STUB IDXBaseObject_GetObjectSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXBaseObject_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0253 */
/* [local] */ 

typedef 
enum DXBNDID
    {	DXB_X	= 0,
	DXB_Y	= 1,
	DXB_Z	= 2,
	DXB_T	= 3
    } 	DXBNDID;

typedef 
enum DXBNDTYPE
    {	DXBT_DISCRETE	= 0,
	DXBT_DISCRETE64	= DXBT_DISCRETE + 1,
	DXBT_CONTINUOUS	= DXBT_DISCRETE64 + 1,
	DXBT_CONTINUOUS64	= DXBT_CONTINUOUS + 1
    } 	DXBNDTYPE;

typedef struct DXDBND
    {
    long Min;
    long Max;
    } 	DXDBND;

typedef DXDBND DXDBNDS[ 4 ];

typedef struct DXDBND64
    {
    LONGLONG Min;
    LONGLONG Max;
    } 	DXDBND64;

typedef DXDBND64 DXDBNDS64[ 4 ];

typedef struct DXCBND
    {
    float Min;
    float Max;
    } 	DXCBND;

typedef DXCBND DXCBNDS[ 4 ];

typedef struct DXCBND64
    {
    double Min;
    double Max;
    } 	DXCBND64;

typedef DXCBND64 DXCBNDS64[ 4 ];

typedef struct DXBNDS
    {
    DXBNDTYPE eType;
    /* [switch_is] */ /* [switch_type] */ union __MIDL___MIDL_itf_dxtrans_0253_0001
        {
        /* [case()] */ DXDBND D[ 4 ];
        /* [case()] */ DXDBND64 LD[ 4 ];
        /* [case()] */ DXCBND C[ 4 ];
        /* [case()] */ DXCBND64 LC[ 4 ];
        } 	u;
    } 	DXBNDS;

typedef long DXDVEC[ 4 ];

typedef LONGLONG DXDVEC64[ 4 ];

typedef float DXCVEC[ 4 ];

typedef double DXCVEC64[ 4 ];

typedef struct DXVEC
    {
    DXBNDTYPE eType;
    /* [switch_is] */ /* [switch_type] */ union __MIDL___MIDL_itf_dxtrans_0253_0002
        {
        /* [case()] */ long D[ 4 ];
        /* [case()] */ LONGLONG LD[ 4 ];
        /* [case()] */ float C[ 4 ];
        /* [case()] */ double LC[ 4 ];
        } 	u;
    } 	DXVEC;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0253_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0253_v0_0_s_ifspec;

#ifndef __IDXTransformFactory_INTERFACE_DEFINED__
#define __IDXTransformFactory_INTERFACE_DEFINED__

/* interface IDXTransformFactory */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXTransformFactory;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6A950B2B-A971-11d1-81C8-0000F87557DB")
    IDXTransformFactory : public IServiceProvider
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetService( 
            /* [in] */ REFGUID guidService,
            /* [in] */ IUnknown *pUnkService,
            /* [in] */ BOOL bWeakReference) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE CreateTransform( 
            /* [size_is][in] */ IUnknown **punkInputs,
            /* [in] */ ULONG ulNumInputs,
            /* [size_is][in] */ IUnknown **punkOutputs,
            /* [in] */ ULONG ulNumOutputs,
            /* [in] */ IPropertyBag *pInitProps,
            /* [in] */ IErrorLog *pErrLog,
            /* [in] */ REFCLSID TransCLSID,
            /* [in] */ REFIID TransIID,
            /* [iid_is][out] */ void **ppTransform) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE InitializeTransform( 
            /* [in] */ IDXTransform *pTransform,
            /* [size_is][in] */ IUnknown **punkInputs,
            /* [in] */ ULONG ulNumInputs,
            /* [size_is][in] */ IUnknown **punkOutputs,
            /* [in] */ ULONG ulNumOutputs,
            /* [in] */ IPropertyBag *pInitProps,
            /* [in] */ IErrorLog *pErrLog) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXTransformFactoryVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXTransformFactory * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXTransformFactory * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXTransformFactory * This);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *QueryService )( 
            IDXTransformFactory * This,
            /* [in] */ REFGUID guidService,
            /* [in] */ REFIID riid,
            /* [out] */ void **ppvObject);
        
        HRESULT ( STDMETHODCALLTYPE *SetService )( 
            IDXTransformFactory * This,
            /* [in] */ REFGUID guidService,
            /* [in] */ IUnknown *pUnkService,
            /* [in] */ BOOL bWeakReference);
        
        HRESULT ( STDMETHODCALLTYPE *CreateTransform )( 
            IDXTransformFactory * This,
            /* [size_is][in] */ IUnknown **punkInputs,
            /* [in] */ ULONG ulNumInputs,
            /* [size_is][in] */ IUnknown **punkOutputs,
            /* [in] */ ULONG ulNumOutputs,
            /* [in] */ IPropertyBag *pInitProps,
            /* [in] */ IErrorLog *pErrLog,
            /* [in] */ REFCLSID TransCLSID,
            /* [in] */ REFIID TransIID,
            /* [iid_is][out] */ void **ppTransform);
        
        HRESULT ( STDMETHODCALLTYPE *InitializeTransform )( 
            IDXTransformFactory * This,
            /* [in] */ IDXTransform *pTransform,
            /* [size_is][in] */ IUnknown **punkInputs,
            /* [in] */ ULONG ulNumInputs,
            /* [size_is][in] */ IUnknown **punkOutputs,
            /* [in] */ ULONG ulNumOutputs,
            /* [in] */ IPropertyBag *pInitProps,
            /* [in] */ IErrorLog *pErrLog);
        
        END_INTERFACE
    } IDXTransformFactoryVtbl;

    interface IDXTransformFactory
    {
        CONST_VTBL struct IDXTransformFactoryVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXTransformFactory_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXTransformFactory_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXTransformFactory_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXTransformFactory_QueryService(This,guidService,riid,ppvObject)	\
    (This)->lpVtbl -> QueryService(This,guidService,riid,ppvObject)


#define IDXTransformFactory_SetService(This,guidService,pUnkService,bWeakReference)	\
    (This)->lpVtbl -> SetService(This,guidService,pUnkService,bWeakReference)

#define IDXTransformFactory_CreateTransform(This,punkInputs,ulNumInputs,punkOutputs,ulNumOutputs,pInitProps,pErrLog,TransCLSID,TransIID,ppTransform)	\
    (This)->lpVtbl -> CreateTransform(This,punkInputs,ulNumInputs,punkOutputs,ulNumOutputs,pInitProps,pErrLog,TransCLSID,TransIID,ppTransform)

#define IDXTransformFactory_InitializeTransform(This,pTransform,punkInputs,ulNumInputs,punkOutputs,ulNumOutputs,pInitProps,pErrLog)	\
    (This)->lpVtbl -> InitializeTransform(This,pTransform,punkInputs,ulNumInputs,punkOutputs,ulNumOutputs,pInitProps,pErrLog)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXTransformFactory_SetService_Proxy( 
    IDXTransformFactory * This,
    /* [in] */ REFGUID guidService,
    /* [in] */ IUnknown *pUnkService,
    /* [in] */ BOOL bWeakReference);


void __RPC_STUB IDXTransformFactory_SetService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransformFactory_CreateTransform_Proxy( 
    IDXTransformFactory * This,
    /* [size_is][in] */ IUnknown **punkInputs,
    /* [in] */ ULONG ulNumInputs,
    /* [size_is][in] */ IUnknown **punkOutputs,
    /* [in] */ ULONG ulNumOutputs,
    /* [in] */ IPropertyBag *pInitProps,
    /* [in] */ IErrorLog *pErrLog,
    /* [in] */ REFCLSID TransCLSID,
    /* [in] */ REFIID TransIID,
    /* [iid_is][out] */ void **ppTransform);


void __RPC_STUB IDXTransformFactory_CreateTransform_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransformFactory_InitializeTransform_Proxy( 
    IDXTransformFactory * This,
    /* [in] */ IDXTransform *pTransform,
    /* [size_is][in] */ IUnknown **punkInputs,
    /* [in] */ ULONG ulNumInputs,
    /* [size_is][in] */ IUnknown **punkOutputs,
    /* [in] */ ULONG ulNumOutputs,
    /* [in] */ IPropertyBag *pInitProps,
    /* [in] */ IErrorLog *pErrLog);


void __RPC_STUB IDXTransformFactory_InitializeTransform_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXTransformFactory_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0254 */
/* [local] */ 

typedef 
enum DXTMISCFLAGS
    {	DXTMF_BLEND_WITH_OUTPUT	= 1L << 0,
	DXTMF_DITHER_OUTPUT	= 1L << 1,
	DXTMF_OPTION_MASK	= 0xffff,
	DXTMF_VALID_OPTIONS	= DXTMF_BLEND_WITH_OUTPUT | DXTMF_DITHER_OUTPUT,
	DXTMF_BLEND_SUPPORTED	= 1L << 16,
	DXTMF_DITHER_SUPPORTED	= 1L << 17,
	DXTMF_INPLACE_OPERATION	= 1L << 24,
	DXTMF_BOUNDS_SUPPORTED	= 1L << 25,
	DXTMF_PLACEMENT_SUPPORTED	= 1L << 26,
	DXTMF_QUALITY_SUPPORTED	= 1L << 27,
	DXTMF_OPAQUE_RESULT	= 1L << 28
    } 	DXTMISCFLAGS;

typedef 
enum DXINOUTINFOFLAGS
    {	DXINOUTF_OPTIONAL	= 1L << 0
    } 	DXINOUTINFOFLAGS;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0254_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0254_v0_0_s_ifspec;

#ifndef __IDXTransform_INTERFACE_DEFINED__
#define __IDXTransform_INTERFACE_DEFINED__

/* interface IDXTransform */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXTransform;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("30A5FB78-E11F-11d1-9064-00C04FD9189D")
    IDXTransform : public IDXBaseObject
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Setup( 
            /* [size_is][in] */ IUnknown *const *punkInputs,
            /* [in] */ ULONG ulNumInputs,
            /* [size_is][in] */ IUnknown *const *punkOutputs,
            /* [in] */ ULONG ulNumOutputs,
            /* [in] */ DWORD dwFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Execute( 
            /* [in] */ const GUID *pRequestID,
            /* [in] */ const DXBNDS *pClipBnds,
            /* [in] */ const DXVEC *pPlacement) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE MapBoundsIn2Out( 
            /* [in] */ const DXBNDS *pInBounds,
            /* [in] */ ULONG ulNumInBnds,
            /* [in] */ ULONG ulOutIndex,
            /* [out] */ DXBNDS *pOutBounds) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE MapBoundsOut2In( 
            /* [in] */ ULONG ulOutIndex,
            /* [in] */ const DXBNDS *pOutBounds,
            /* [in] */ ULONG ulInIndex,
            /* [out] */ DXBNDS *pInBounds) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetMiscFlags( 
            /* [in] */ DWORD dwMiscFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetMiscFlags( 
            /* [out] */ DWORD *pdwMiscFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetInOutInfo( 
            /* [in] */ BOOL bIsOutput,
            /* [in] */ ULONG ulIndex,
            /* [out] */ DWORD *pdwFlags,
            /* [size_is][out] */ GUID *pIDs,
            /* [out][in] */ ULONG *pcIDs,
            /* [out] */ IUnknown **ppUnkCurrentObject) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetQuality( 
            /* [in] */ float fQuality) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetQuality( 
            /* [out] */ float *fQuality) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXTransformVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXTransform * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXTransform * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXTransform * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetGenerationId )( 
            IDXTransform * This,
            /* [out] */ ULONG *pID);
        
        HRESULT ( STDMETHODCALLTYPE *IncrementGenerationId )( 
            IDXTransform * This,
            /* [in] */ BOOL bRefresh);
        
        HRESULT ( STDMETHODCALLTYPE *GetObjectSize )( 
            IDXTransform * This,
            /* [out] */ ULONG *pcbSize);
        
        HRESULT ( STDMETHODCALLTYPE *Setup )( 
            IDXTransform * This,
            /* [size_is][in] */ IUnknown *const *punkInputs,
            /* [in] */ ULONG ulNumInputs,
            /* [size_is][in] */ IUnknown *const *punkOutputs,
            /* [in] */ ULONG ulNumOutputs,
            /* [in] */ DWORD dwFlags);
        
        HRESULT ( STDMETHODCALLTYPE *Execute )( 
            IDXTransform * This,
            /* [in] */ const GUID *pRequestID,
            /* [in] */ const DXBNDS *pClipBnds,
            /* [in] */ const DXVEC *pPlacement);
        
        HRESULT ( STDMETHODCALLTYPE *MapBoundsIn2Out )( 
            IDXTransform * This,
            /* [in] */ const DXBNDS *pInBounds,
            /* [in] */ ULONG ulNumInBnds,
            /* [in] */ ULONG ulOutIndex,
            /* [out] */ DXBNDS *pOutBounds);
        
        HRESULT ( STDMETHODCALLTYPE *MapBoundsOut2In )( 
            IDXTransform * This,
            /* [in] */ ULONG ulOutIndex,
            /* [in] */ const DXBNDS *pOutBounds,
            /* [in] */ ULONG ulInIndex,
            /* [out] */ DXBNDS *pInBounds);
        
        HRESULT ( STDMETHODCALLTYPE *SetMiscFlags )( 
            IDXTransform * This,
            /* [in] */ DWORD dwMiscFlags);
        
        HRESULT ( STDMETHODCALLTYPE *GetMiscFlags )( 
            IDXTransform * This,
            /* [out] */ DWORD *pdwMiscFlags);
        
        HRESULT ( STDMETHODCALLTYPE *GetInOutInfo )( 
            IDXTransform * This,
            /* [in] */ BOOL bIsOutput,
            /* [in] */ ULONG ulIndex,
            /* [out] */ DWORD *pdwFlags,
            /* [size_is][out] */ GUID *pIDs,
            /* [out][in] */ ULONG *pcIDs,
            /* [out] */ IUnknown **ppUnkCurrentObject);
        
        HRESULT ( STDMETHODCALLTYPE *SetQuality )( 
            IDXTransform * This,
            /* [in] */ float fQuality);
        
        HRESULT ( STDMETHODCALLTYPE *GetQuality )( 
            IDXTransform * This,
            /* [out] */ float *fQuality);
        
        END_INTERFACE
    } IDXTransformVtbl;

    interface IDXTransform
    {
        CONST_VTBL struct IDXTransformVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXTransform_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXTransform_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXTransform_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXTransform_GetGenerationId(This,pID)	\
    (This)->lpVtbl -> GetGenerationId(This,pID)

#define IDXTransform_IncrementGenerationId(This,bRefresh)	\
    (This)->lpVtbl -> IncrementGenerationId(This,bRefresh)

#define IDXTransform_GetObjectSize(This,pcbSize)	\
    (This)->lpVtbl -> GetObjectSize(This,pcbSize)


#define IDXTransform_Setup(This,punkInputs,ulNumInputs,punkOutputs,ulNumOutputs,dwFlags)	\
    (This)->lpVtbl -> Setup(This,punkInputs,ulNumInputs,punkOutputs,ulNumOutputs,dwFlags)

#define IDXTransform_Execute(This,pRequestID,pClipBnds,pPlacement)	\
    (This)->lpVtbl -> Execute(This,pRequestID,pClipBnds,pPlacement)

#define IDXTransform_MapBoundsIn2Out(This,pInBounds,ulNumInBnds,ulOutIndex,pOutBounds)	\
    (This)->lpVtbl -> MapBoundsIn2Out(This,pInBounds,ulNumInBnds,ulOutIndex,pOutBounds)

#define IDXTransform_MapBoundsOut2In(This,ulOutIndex,pOutBounds,ulInIndex,pInBounds)	\
    (This)->lpVtbl -> MapBoundsOut2In(This,ulOutIndex,pOutBounds,ulInIndex,pInBounds)

#define IDXTransform_SetMiscFlags(This,dwMiscFlags)	\
    (This)->lpVtbl -> SetMiscFlags(This,dwMiscFlags)

#define IDXTransform_GetMiscFlags(This,pdwMiscFlags)	\
    (This)->lpVtbl -> GetMiscFlags(This,pdwMiscFlags)

#define IDXTransform_GetInOutInfo(This,bIsOutput,ulIndex,pdwFlags,pIDs,pcIDs,ppUnkCurrentObject)	\
    (This)->lpVtbl -> GetInOutInfo(This,bIsOutput,ulIndex,pdwFlags,pIDs,pcIDs,ppUnkCurrentObject)

#define IDXTransform_SetQuality(This,fQuality)	\
    (This)->lpVtbl -> SetQuality(This,fQuality)

#define IDXTransform_GetQuality(This,fQuality)	\
    (This)->lpVtbl -> GetQuality(This,fQuality)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXTransform_Setup_Proxy( 
    IDXTransform * This,
    /* [size_is][in] */ IUnknown *const *punkInputs,
    /* [in] */ ULONG ulNumInputs,
    /* [size_is][in] */ IUnknown *const *punkOutputs,
    /* [in] */ ULONG ulNumOutputs,
    /* [in] */ DWORD dwFlags);


void __RPC_STUB IDXTransform_Setup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_Execute_Proxy( 
    IDXTransform * This,
    /* [in] */ const GUID *pRequestID,
    /* [in] */ const DXBNDS *pClipBnds,
    /* [in] */ const DXVEC *pPlacement);


void __RPC_STUB IDXTransform_Execute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_MapBoundsIn2Out_Proxy( 
    IDXTransform * This,
    /* [in] */ const DXBNDS *pInBounds,
    /* [in] */ ULONG ulNumInBnds,
    /* [in] */ ULONG ulOutIndex,
    /* [out] */ DXBNDS *pOutBounds);


void __RPC_STUB IDXTransform_MapBoundsIn2Out_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_MapBoundsOut2In_Proxy( 
    IDXTransform * This,
    /* [in] */ ULONG ulOutIndex,
    /* [in] */ const DXBNDS *pOutBounds,
    /* [in] */ ULONG ulInIndex,
    /* [out] */ DXBNDS *pInBounds);


void __RPC_STUB IDXTransform_MapBoundsOut2In_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_SetMiscFlags_Proxy( 
    IDXTransform * This,
    /* [in] */ DWORD dwMiscFlags);


void __RPC_STUB IDXTransform_SetMiscFlags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_GetMiscFlags_Proxy( 
    IDXTransform * This,
    /* [out] */ DWORD *pdwMiscFlags);


void __RPC_STUB IDXTransform_GetMiscFlags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_GetInOutInfo_Proxy( 
    IDXTransform * This,
    /* [in] */ BOOL bIsOutput,
    /* [in] */ ULONG ulIndex,
    /* [out] */ DWORD *pdwFlags,
    /* [size_is][out] */ GUID *pIDs,
    /* [out][in] */ ULONG *pcIDs,
    /* [out] */ IUnknown **ppUnkCurrentObject);


void __RPC_STUB IDXTransform_GetInOutInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_SetQuality_Proxy( 
    IDXTransform * This,
    /* [in] */ float fQuality);


void __RPC_STUB IDXTransform_SetQuality_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_GetQuality_Proxy( 
    IDXTransform * This,
    /* [out] */ float *fQuality);


void __RPC_STUB IDXTransform_GetQuality_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXTransform_INTERFACE_DEFINED__ */


#ifndef __IDXSurfacePick_INTERFACE_DEFINED__
#define __IDXSurfacePick_INTERFACE_DEFINED__

/* interface IDXSurfacePick */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXSurfacePick;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("30A5FB79-E11F-11d1-9064-00C04FD9189D")
    IDXSurfacePick : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE PointPick( 
            /* [in] */ const DXVEC *pPoint,
            /* [out] */ ULONG *pulInputSurfaceIndex,
            /* [out] */ DXVEC *pInputPoint) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXSurfacePickVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXSurfacePick * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXSurfacePick * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXSurfacePick * This);
        
        HRESULT ( STDMETHODCALLTYPE *PointPick )( 
            IDXSurfacePick * This,
            /* [in] */ const DXVEC *pPoint,
            /* [out] */ ULONG *pulInputSurfaceIndex,
            /* [out] */ DXVEC *pInputPoint);
        
        END_INTERFACE
    } IDXSurfacePickVtbl;

    interface IDXSurfacePick
    {
        CONST_VTBL struct IDXSurfacePickVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXSurfacePick_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXSurfacePick_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXSurfacePick_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXSurfacePick_PointPick(This,pPoint,pulInputSurfaceIndex,pInputPoint)	\
    (This)->lpVtbl -> PointPick(This,pPoint,pulInputSurfaceIndex,pInputPoint)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXSurfacePick_PointPick_Proxy( 
    IDXSurfacePick * This,
    /* [in] */ const DXVEC *pPoint,
    /* [out] */ ULONG *pulInputSurfaceIndex,
    /* [out] */ DXVEC *pInputPoint);


void __RPC_STUB IDXSurfacePick_PointPick_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXSurfacePick_INTERFACE_DEFINED__ */


#ifndef __IDXTBindHost_INTERFACE_DEFINED__
#define __IDXTBindHost_INTERFACE_DEFINED__

/* interface IDXTBindHost */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXTBindHost;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D26BCE55-E9DC-11d1-9066-00C04FD9189D")
    IDXTBindHost : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetBindHost( 
            /* [in] */ IBindHost *pBindHost) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXTBindHostVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXTBindHost * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXTBindHost * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXTBindHost * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetBindHost )( 
            IDXTBindHost * This,
            /* [in] */ IBindHost *pBindHost);
        
        END_INTERFACE
    } IDXTBindHostVtbl;

    interface IDXTBindHost
    {
        CONST_VTBL struct IDXTBindHostVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXTBindHost_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXTBindHost_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXTBindHost_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXTBindHost_SetBindHost(This,pBindHost)	\
    (This)->lpVtbl -> SetBindHost(This,pBindHost)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXTBindHost_SetBindHost_Proxy( 
    IDXTBindHost * This,
    /* [in] */ IBindHost *pBindHost);


void __RPC_STUB IDXTBindHost_SetBindHost_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXTBindHost_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0257 */
/* [local] */ 

typedef void __stdcall __stdcall DXTASKPROC( 
    void *pTaskData,
    BOOL *pbContinueProcessing);

typedef DXTASKPROC *PFNDXTASKPROC;

typedef void __stdcall __stdcall DXAPCPROC( 
    DWORD dwData);

typedef DXAPCPROC *PFNDXAPCPROC;

#ifdef __cplusplus
typedef struct DXTMTASKINFO
{
    PFNDXTASKPROC pfnTaskProc;       // Pointer to function to execute
    PVOID         pTaskData;         // Pointer to argument data
    PFNDXAPCPROC  pfnCompletionAPC;  // Pointer to completion APC proc
    DWORD         dwCompletionData;  // Pointer to APC proc data
    const GUID*   pRequestID;        // Used to identify groups of tasks
} DXTMTASKINFO;
#else
typedef struct DXTMTASKINFO
    {
    PVOID pfnTaskProc;
    PVOID pTaskData;
    PVOID pfnCompletionAPC;
    DWORD dwCompletionData;
    const GUID *pRequestID;
    } 	DXTMTASKINFO;

#endif


extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0257_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0257_v0_0_s_ifspec;

#ifndef __IDXTaskManager_INTERFACE_DEFINED__
#define __IDXTaskManager_INTERFACE_DEFINED__

/* interface IDXTaskManager */
/* [object][unique][helpstring][uuid][local] */ 


EXTERN_C const IID IID_IDXTaskManager;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("254DBBC1-F922-11d0-883A-3C8B00C10000")
    IDXTaskManager : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE QueryNumProcessors( 
            /* [out] */ ULONG *pulNumProc) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetThreadPoolSize( 
            /* [in] */ ULONG ulNumThreads) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetThreadPoolSize( 
            /* [out] */ ULONG *pulNumThreads) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetConcurrencyLimit( 
            /* [in] */ ULONG ulNumThreads) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetConcurrencyLimit( 
            /* [out] */ ULONG *pulNumThreads) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ScheduleTasks( 
            /* [in] */ DXTMTASKINFO TaskInfo[  ],
            /* [in] */ HANDLE Events[  ],
            /* [out] */ DWORD TaskIDs[  ],
            /* [in] */ ULONG ulNumTasks,
            /* [in] */ ULONG ulWaitPeriod) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE TerminateTasks( 
            /* [in] */ DWORD TaskIDs[  ],
            /* [in] */ ULONG ulCount,
            /* [in] */ ULONG ulTimeOut) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE TerminateRequest( 
            /* [in] */ REFIID RequestID,
            /* [in] */ ULONG ulTimeOut) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXTaskManagerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXTaskManager * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXTaskManager * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXTaskManager * This);
        
        HRESULT ( STDMETHODCALLTYPE *QueryNumProcessors )( 
            IDXTaskManager * This,
            /* [out] */ ULONG *pulNumProc);
        
        HRESULT ( STDMETHODCALLTYPE *SetThreadPoolSize )( 
            IDXTaskManager * This,
            /* [in] */ ULONG ulNumThreads);
        
        HRESULT ( STDMETHODCALLTYPE *GetThreadPoolSize )( 
            IDXTaskManager * This,
            /* [out] */ ULONG *pulNumThreads);
        
        HRESULT ( STDMETHODCALLTYPE *SetConcurrencyLimit )( 
            IDXTaskManager * This,
            /* [in] */ ULONG ulNumThreads);
        
        HRESULT ( STDMETHODCALLTYPE *GetConcurrencyLimit )( 
            IDXTaskManager * This,
            /* [out] */ ULONG *pulNumThreads);
        
        HRESULT ( STDMETHODCALLTYPE *ScheduleTasks )( 
            IDXTaskManager * This,
            /* [in] */ DXTMTASKINFO TaskInfo[  ],
            /* [in] */ HANDLE Events[  ],
            /* [out] */ DWORD TaskIDs[  ],
            /* [in] */ ULONG ulNumTasks,
            /* [in] */ ULONG ulWaitPeriod);
        
        HRESULT ( STDMETHODCALLTYPE *TerminateTasks )( 
            IDXTaskManager * This,
            /* [in] */ DWORD TaskIDs[  ],
            /* [in] */ ULONG ulCount,
            /* [in] */ ULONG ulTimeOut);
        
        HRESULT ( STDMETHODCALLTYPE *TerminateRequest )( 
            IDXTaskManager * This,
            /* [in] */ REFIID RequestID,
            /* [in] */ ULONG ulTimeOut);
        
        END_INTERFACE
    } IDXTaskManagerVtbl;

    interface IDXTaskManager
    {
        CONST_VTBL struct IDXTaskManagerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXTaskManager_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXTaskManager_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXTaskManager_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXTaskManager_QueryNumProcessors(This,pulNumProc)	\
    (This)->lpVtbl -> QueryNumProcessors(This,pulNumProc)

#define IDXTaskManager_SetThreadPoolSize(This,ulNumThreads)	\
    (This)->lpVtbl -> SetThreadPoolSize(This,ulNumThreads)

#define IDXTaskManager_GetThreadPoolSize(This,pulNumThreads)	\
    (This)->lpVtbl -> GetThreadPoolSize(This,pulNumThreads)

#define IDXTaskManager_SetConcurrencyLimit(This,ulNumThreads)	\
    (This)->lpVtbl -> SetConcurrencyLimit(This,ulNumThreads)

#define IDXTaskManager_GetConcurrencyLimit(This,pulNumThreads)	\
    (This)->lpVtbl -> GetConcurrencyLimit(This,pulNumThreads)

#define IDXTaskManager_ScheduleTasks(This,TaskInfo,Events,TaskIDs,ulNumTasks,ulWaitPeriod)	\
    (This)->lpVtbl -> ScheduleTasks(This,TaskInfo,Events,TaskIDs,ulNumTasks,ulWaitPeriod)

#define IDXTaskManager_TerminateTasks(This,TaskIDs,ulCount,ulTimeOut)	\
    (This)->lpVtbl -> TerminateTasks(This,TaskIDs,ulCount,ulTimeOut)

#define IDXTaskManager_TerminateRequest(This,RequestID,ulTimeOut)	\
    (This)->lpVtbl -> TerminateRequest(This,RequestID,ulTimeOut)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXTaskManager_QueryNumProcessors_Proxy( 
    IDXTaskManager * This,
    /* [out] */ ULONG *pulNumProc);


void __RPC_STUB IDXTaskManager_QueryNumProcessors_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_SetThreadPoolSize_Proxy( 
    IDXTaskManager * This,
    /* [in] */ ULONG ulNumThreads);


void __RPC_STUB IDXTaskManager_SetThreadPoolSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_GetThreadPoolSize_Proxy( 
    IDXTaskManager * This,
    /* [out] */ ULONG *pulNumThreads);


void __RPC_STUB IDXTaskManager_GetThreadPoolSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_SetConcurrencyLimit_Proxy( 
    IDXTaskManager * This,
    /* [in] */ ULONG ulNumThreads);


void __RPC_STUB IDXTaskManager_SetConcurrencyLimit_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_GetConcurrencyLimit_Proxy( 
    IDXTaskManager * This,
    /* [out] */ ULONG *pulNumThreads);


void __RPC_STUB IDXTaskManager_GetConcurrencyLimit_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_ScheduleTasks_Proxy( 
    IDXTaskManager * This,
    /* [in] */ DXTMTASKINFO TaskInfo[  ],
    /* [in] */ HANDLE Events[  ],
    /* [out] */ DWORD TaskIDs[  ],
    /* [in] */ ULONG ulNumTasks,
    /* [in] */ ULONG ulWaitPeriod);


void __RPC_STUB IDXTaskManager_ScheduleTasks_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_TerminateTasks_Proxy( 
    IDXTaskManager * This,
    /* [in] */ DWORD TaskIDs[  ],
    /* [in] */ ULONG ulCount,
    /* [in] */ ULONG ulTimeOut);


void __RPC_STUB IDXTaskManager_TerminateTasks_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_TerminateRequest_Proxy( 
    IDXTaskManager * This,
    /* [in] */ REFIID RequestID,
    /* [in] */ ULONG ulTimeOut);


void __RPC_STUB IDXTaskManager_TerminateRequest_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXTaskManager_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0258 */
/* [local] */ 

#ifdef __cplusplus
/////////////////////////////////////////////////////

class DXBASESAMPLE;
class DXSAMPLE;
class DXPMSAMPLE;

/////////////////////////////////////////////////////

class DXBASESAMPLE
{
public:
    BYTE Blue;
    BYTE Green;
    BYTE Red;
    BYTE Alpha;
    DXBASESAMPLE() {}
    DXBASESAMPLE(const BYTE alpha, const BYTE red, const BYTE green, const BYTE blue) :
        Alpha(alpha),
        Red(red),
        Green(green),
        Blue(blue) {}
    DXBASESAMPLE(const DWORD val) { *this = (*(DXBASESAMPLE *)&val); }
    operator DWORD () const {return *((DWORD *)this); }
    DWORD operator=(const DWORD val) { return *this = *((DXBASESAMPLE *)&val); }
}; // DXBASESAMPLE

/////////////////////////////////////////////////////

class DXSAMPLE : public DXBASESAMPLE
{
public:
    DXSAMPLE() {}
    DXSAMPLE(const BYTE alpha, const BYTE red, const BYTE green, const BYTE blue) :
         DXBASESAMPLE(alpha, red, green, blue) {}
    DXSAMPLE(const DWORD val) { *this = (*(DXSAMPLE *)&val); }
    operator DWORD () const {return *((DWORD *)this); }
    DWORD operator=(const DWORD val) { return *this = *((DXSAMPLE *)&val); }
    operator DXPMSAMPLE() const;
}; // DXSAMPLE

/////////////////////////////////////////////////////

class DXPMSAMPLE : public DXBASESAMPLE
{
public:
    DXPMSAMPLE() {}
    DXPMSAMPLE(const BYTE alpha, const BYTE red, const BYTE green, const BYTE blue) :
         DXBASESAMPLE(alpha, red, green, blue) {}
    DXPMSAMPLE(const DWORD val) { *this = (*(DXPMSAMPLE *)&val); }
    operator DWORD () const {return *((DWORD *)this); }
    DWORD operator=(const DWORD val) { return *this = *((DXPMSAMPLE *)&val); }
    operator DXSAMPLE() const;
}; // DXPMSAMPLE

//
// The following cast operators are to prevent a direct assignment of a DXSAMPLE to a DXPMSAMPLE
//
inline DXSAMPLE::operator DXPMSAMPLE() const { return *((DXPMSAMPLE *)this); }
inline DXPMSAMPLE::operator DXSAMPLE() const { return *((DXSAMPLE *)this); }
#else // !__cplusplus
typedef struct DXBASESAMPLE
    {
    BYTE Blue;
    BYTE Green;
    BYTE Red;
    BYTE Alpha;
    } 	DXBASESAMPLE;

typedef struct DXSAMPLE
    {
    BYTE Blue;
    BYTE Green;
    BYTE Red;
    BYTE Alpha;
    } 	DXSAMPLE;

typedef struct DXPMSAMPLE
    {
    BYTE Blue;
    BYTE Green;
    BYTE Red;
    BYTE Alpha;
    } 	DXPMSAMPLE;

#endif // !__cplusplus
typedef 
enum DXRUNTYPE
    {	DXRUNTYPE_CLEAR	= 0,
	DXRUNTYPE_OPAQUE	= 1,
	DXRUNTYPE_TRANS	= 2,
	DXRUNTYPE_UNKNOWN	= 3
    } 	DXRUNTYPE;

#define	DX_MAX_RUN_INFO_COUNT	( 128 )

// Ignore the definition used by MIDL for TLB generation
#if 0
typedef struct DXRUNINFO
    {
    ULONG Bitfields;
    } 	DXRUNINFO;

#endif // 0
typedef struct DXRUNINFO
{
    ULONG   Type  : 2;   // Type
    ULONG   Count : 30;  // Number of samples in run
} DXRUNINFO;
typedef 
enum DXSFCREATE
    {	DXSF_FORMAT_IS_CLSID	= 1L << 0,
	DXSF_NO_LAZY_DDRAW_LOCK	= 1L << 1
    } 	DXSFCREATE;

typedef 
enum DXBLTOPTIONS
    {	DXBOF_DO_OVER	= 1L << 0,
	DXBOF_DITHER	= 1L << 1
    } 	DXBLTOPTIONS;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0258_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0258_v0_0_s_ifspec;

#ifndef __IDXSurfaceFactory_INTERFACE_DEFINED__
#define __IDXSurfaceFactory_INTERFACE_DEFINED__

/* interface IDXSurfaceFactory */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXSurfaceFactory;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("144946F5-C4D4-11d1-81D1-0000F87557DB")
    IDXSurfaceFactory : public IUnknown
    {
    public:
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE CreateSurface( 
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ DWORD dwFlags,
            /* [in] */ IUnknown *punkOuter,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE CreateFromDDSurface( 
            /* [in] */ IUnknown *pDDrawSurface,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ DWORD dwFlags,
            /* [in] */ IUnknown *punkOuter,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface) = 0;
        
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE LoadImage( 
            /* [in] */ const LPWSTR pszFileName,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface) = 0;
        
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE LoadImageFromStream( 
            /* [in] */ IStream *pStream,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface) = 0;
        
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE CopySurfaceToNewFormat( 
            /* [in] */ IDXSurface *pSrc,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pDestFormatID,
            /* [out] */ IDXSurface **ppNewSurface) = 0;
        
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE CreateD3DRMTexture( 
            /* [in] */ IDXSurface *pSrc,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ IUnknown *pD3DRM3,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppTexture3) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE BitBlt( 
            /* [in] */ IDXSurface *pDest,
            /* [in] */ const DXVEC *pPlacement,
            /* [in] */ IDXSurface *pSrc,
            /* [in] */ const DXBNDS *pClipBounds,
            /* [in] */ DWORD dwFlags) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXSurfaceFactoryVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXSurfaceFactory * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXSurfaceFactory * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXSurfaceFactory * This);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *CreateSurface )( 
            IDXSurfaceFactory * This,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ DWORD dwFlags,
            /* [in] */ IUnknown *punkOuter,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface);
        
        HRESULT ( STDMETHODCALLTYPE *CreateFromDDSurface )( 
            IDXSurfaceFactory * This,
            /* [in] */ IUnknown *pDDrawSurface,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ DWORD dwFlags,
            /* [in] */ IUnknown *punkOuter,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *LoadImage )( 
            IDXSurfaceFactory * This,
            /* [in] */ const LPWSTR pszFileName,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *LoadImageFromStream )( 
            IDXSurfaceFactory * This,
            /* [in] */ IStream *pStream,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *CopySurfaceToNewFormat )( 
            IDXSurfaceFactory * This,
            /* [in] */ IDXSurface *pSrc,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pDestFormatID,
            /* [out] */ IDXSurface **ppNewSurface);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *CreateD3DRMTexture )( 
            IDXSurfaceFactory * This,
            /* [in] */ IDXSurface *pSrc,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ IUnknown *pD3DRM3,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppTexture3);
        
        HRESULT ( STDMETHODCALLTYPE *BitBlt )( 
            IDXSurfaceFactory * This,
            /* [in] */ IDXSurface *pDest,
            /* [in] */ const DXVEC *pPlacement,
            /* [in] */ IDXSurface *pSrc,
            /* [in] */ const DXBNDS *pClipBounds,
            /* [in] */ DWORD dwFlags);
        
        END_INTERFACE
    } IDXSurfaceFactoryVtbl;

    interface IDXSurfaceFactory
    {
        CONST_VTBL struct IDXSurfaceFactoryVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXSurfaceFactory_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXSurfaceFactory_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXSurfaceFactory_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXSurfaceFactory_CreateSurface(This,pDirectDraw,pDDSurfaceDesc,pFormatID,pBounds,dwFlags,punkOuter,riid,ppDXSurface)	\
    (This)->lpVtbl -> CreateSurface(This,pDirectDraw,pDDSurfaceDesc,pFormatID,pBounds,dwFlags,punkOuter,riid,ppDXSurface)

#define IDXSurfaceFactory_CreateFromDDSurface(This,pDDrawSurface,pFormatID,dwFlags,punkOuter,riid,ppDXSurface)	\
    (This)->lpVtbl -> CreateFromDDSurface(This,pDDrawSurface,pFormatID,dwFlags,punkOuter,riid,ppDXSurface)

#define IDXSurfaceFactory_LoadImage(This,pszFileName,pDirectDraw,pDDSurfaceDesc,pFormatID,riid,ppDXSurface)	\
    (This)->lpVtbl -> LoadImage(This,pszFileName,pDirectDraw,pDDSurfaceDesc,pFormatID,riid,ppDXSurface)

#define IDXSurfaceFactory_LoadImageFromStream(This,pStream,pDirectDraw,pDDSurfaceDesc,pFormatID,riid,ppDXSurface)	\
    (This)->lpVtbl -> LoadImageFromStream(This,pStream,pDirectDraw,pDDSurfaceDesc,pFormatID,riid,ppDXSurface)

#define IDXSurfaceFactory_CopySurfaceToNewFormat(This,pSrc,pDirectDraw,pDDSurfaceDesc,pDestFormatID,ppNewSurface)	\
    (This)->lpVtbl -> CopySurfaceToNewFormat(This,pSrc,pDirectDraw,pDDSurfaceDesc,pDestFormatID,ppNewSurface)

#define IDXSurfaceFactory_CreateD3DRMTexture(This,pSrc,pDirectDraw,pD3DRM3,riid,ppTexture3)	\
    (This)->lpVtbl -> CreateD3DRMTexture(This,pSrc,pDirectDraw,pD3DRM3,riid,ppTexture3)

#define IDXSurfaceFactory_BitBlt(This,pDest,pPlacement,pSrc,pClipBounds,dwFlags)	\
    (This)->lpVtbl -> BitBlt(This,pDest,pPlacement,pSrc,pClipBounds,dwFlags)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [local] */ HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_CreateSurface_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ IUnknown *pDirectDraw,
    /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
    /* [in] */ const GUID *pFormatID,
    /* [in] */ const DXBNDS *pBounds,
    /* [in] */ DWORD dwFlags,
    /* [in] */ IUnknown *punkOuter,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppDXSurface);


void __RPC_STUB IDXSurfaceFactory_CreateSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_CreateFromDDSurface_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ IUnknown *pDDrawSurface,
    /* [in] */ const GUID *pFormatID,
    /* [in] */ DWORD dwFlags,
    /* [in] */ IUnknown *punkOuter,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppDXSurface);


void __RPC_STUB IDXSurfaceFactory_CreateFromDDSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [local] */ HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_LoadImage_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ const LPWSTR pszFileName,
    /* [in] */ IUnknown *pDirectDraw,
    /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
    /* [in] */ const GUID *pFormatID,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppDXSurface);


void __RPC_STUB IDXSurfaceFactory_LoadImage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [local] */ HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_LoadImageFromStream_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ IStream *pStream,
    /* [in] */ IUnknown *pDirectDraw,
    /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
    /* [in] */ const GUID *pFormatID,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppDXSurface);


void __RPC_STUB IDXSurfaceFactory_LoadImageFromStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [local] */ HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_CopySurfaceToNewFormat_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ IDXSurface *pSrc,
    /* [in] */ IUnknown *pDirectDraw,
    /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
    /* [in] */ const GUID *pDestFormatID,
    /* [out] */ IDXSurface **ppNewSurface);


void __RPC_STUB IDXSurfaceFactory_CopySurfaceToNewFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [local] */ HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_CreateD3DRMTexture_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ IDXSurface *pSrc,
    /* [in] */ IUnknown *pDirectDraw,
    /* [in] */ IUnknown *pD3DRM3,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppTexture3);


void __RPC_STUB IDXSurfaceFactory_CreateD3DRMTexture_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_BitBlt_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ IDXSurface *pDest,
    /* [in] */ const DXVEC *pPlacement,
    /* [in] */ IDXSurface *pSrc,
    /* [in] */ const DXBNDS *pClipBounds,
    /* [in] */ DWORD dwFlags);


void __RPC_STUB IDXSurfaceFactory_BitBlt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXSurfaceFactory_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0259 */
/* [local] */ 

typedef 
enum DXSURFMODCOMPOP
    {	DXSURFMOD_COMP_OVER	= 0,
	DXSURFMOD_COMP_ALPHA_MASK	= 1,
	DXSURFMOD_COMP_MAX_VALID	= 1
    } 	DXSURFMODCOMPOP;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0259_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0259_v0_0_s_ifspec;

#ifndef __IDXSurfaceModifier_INTERFACE_DEFINED__
#define __IDXSurfaceModifier_INTERFACE_DEFINED__

/* interface IDXSurfaceModifier */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXSurfaceModifier;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9EA3B637-C37D-11d1-905E-00C04FD9189D")
    IDXSurfaceModifier : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetFillColor( 
            /* [in] */ DXSAMPLE Color) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetFillColor( 
            /* [out] */ DXSAMPLE *pColor) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetBounds( 
            /* [in] */ const DXBNDS *pBounds) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetBackground( 
            /* [in] */ IDXSurface *pSurface) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetBackground( 
            /* [out] */ IDXSurface **ppSurface) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetCompositeOperation( 
            /* [in] */ DXSURFMODCOMPOP CompOp) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetCompositeOperation( 
            /* [out] */ DXSURFMODCOMPOP *pCompOp) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetForeground( 
            /* [in] */ IDXSurface *pSurface,
            /* [in] */ BOOL bTile,
            /* [in] */ const POINT *pOrigin) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetForeground( 
            /* [out] */ IDXSurface **ppSurface,
            /* [out] */ BOOL *pbTile,
            /* [out] */ POINT *pOrigin) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetOpacity( 
            /* [in] */ float Opacity) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetOpacity( 
            /* [out] */ float *pOpacity) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetLookup( 
            /* [in] */ IDXLookupTable *pLookupTable) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetLookup( 
            /* [out] */ IDXLookupTable **ppLookupTable) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXSurfaceModifierVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXSurfaceModifier * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXSurfaceModifier * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXSurfaceModifier * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetFillColor )( 
            IDXSurfaceModifier * This,
            /* [in] */ DXSAMPLE Color);
        
        HRESULT ( STDMETHODCALLTYPE *GetFillColor )( 
            IDXSurfaceModifier * This,
            /* [out] */ DXSAMPLE *pColor);
        
        HRESULT ( STDMETHODCALLTYPE *SetBounds )( 
            IDXSurfaceModifier * This,
            /* [in] */ const DXBNDS *pBounds);
        
        HRESULT ( STDMETHODCALLTYPE *SetBackground )( 
            IDXSurfaceModifier * This,
            /* [in] */ IDXSurface *pSurface);
        
        HRESULT ( STDMETHODCALLTYPE *GetBackground )( 
            IDXSurfaceModifier * This,
            /* [out] */ IDXSurface **ppSurface);
        
        HRESULT ( STDMETHODCALLTYPE *SetCompositeOperation )( 
            IDXSurfaceModifier * This,
            /* [in] */ DXSURFMODCOMPOP CompOp);
        
        HRESULT ( STDMETHODCALLTYPE *GetCompositeOperation )( 
            IDXSurfaceModifier * This,
            /* [out] */ DXSURFMODCOMPOP *pCompOp);
        
        HRESULT ( STDMETHODCALLTYPE *SetForeground )( 
            IDXSurfaceModifier * This,
            /* [in] */ IDXSurface *pSurface,
            /* [in] */ BOOL bTile,
            /* [in] */ const POINT *pOrigin);
        
        HRESULT ( STDMETHODCALLTYPE *GetForeground )( 
            IDXSurfaceModifier * This,
            /* [out] */ IDXSurface **ppSurface,
            /* [out] */ BOOL *pbTile,
            /* [out] */ POINT *pOrigin);
        
        HRESULT ( STDMETHODCALLTYPE *SetOpacity )( 
            IDXSurfaceModifier * This,
            /* [in] */ float Opacity);
        
        HRESULT ( STDMETHODCALLTYPE *GetOpacity )( 
            IDXSurfaceModifier * This,
            /* [out] */ float *pOpacity);
        
        HRESULT ( STDMETHODCALLTYPE *SetLookup )( 
            IDXSurfaceModifier * This,
            /* [in] */ IDXLookupTable *pLookupTable);
        
        HRESULT ( STDMETHODCALLTYPE *GetLookup )( 
            IDXSurfaceModifier * This,
            /* [out] */ IDXLookupTable **ppLookupTable);
        
        END_INTERFACE
    } IDXSurfaceModifierVtbl;

    interface IDXSurfaceModifier
    {
        CONST_VTBL struct IDXSurfaceModifierVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXSurfaceModifier_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXSurfaceModifier_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXSurfaceModifier_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXSurfaceModifier_SetFillColor(This,Color)	\
    (This)->lpVtbl -> SetFillColor(This,Color)

#define IDXSurfaceModifier_GetFillColor(This,pColor)	\
    (This)->lpVtbl -> GetFillColor(This,pColor)

#define IDXSurfaceModifier_SetBounds(This,pBounds)	\
    (This)->lpVtbl -> SetBounds(This,pBounds)

#define IDXSurfaceModifier_SetBackground(This,pSurface)	\
    (This)->lpVtbl -> SetBackground(This,pSurface)

#define IDXSurfaceModifier_GetBackground(This,ppSurface)	\
    (This)->lpVtbl -> GetBackground(This,ppSurface)

#define IDXSurfaceModifier_SetCompositeOperation(This,CompOp)	\
    (This)->lpVtbl -> SetCompositeOperation(This,CompOp)

#define IDXSurfaceModifier_GetCompositeOperation(This,pCompOp)	\
    (This)->lpVtbl -> GetCompositeOperation(This,pCompOp)

#define IDXSurfaceModifier_SetForeground(This,pSurface,bTile,pOrigin)	\
    (This)->lpVtbl -> SetForeground(This,pSurface,bTile,pOrigin)

#define IDXSurfaceModifier_GetForeground(This,ppSurface,pbTile,pOrigin)	\
    (This)->lpVtbl -> GetForeground(This,ppSurface,pbTile,pOrigin)

#define IDXSurfaceModifier_SetOpacity(This,Opacity)	\
    (This)->lpVtbl -> SetOpacity(This,Opacity)

#define IDXSurfaceModifier_GetOpacity(This,pOpacity)	\
    (This)->lpVtbl -> GetOpacity(This,pOpacity)

#define IDXSurfaceModifier_SetLookup(This,pLookupTable)	\
    (This)->lpVtbl -> SetLookup(This,pLookupTable)

#define IDXSurfaceModifier_GetLookup(This,ppLookupTable)	\
    (This)->lpVtbl -> GetLookup(This,ppLookupTable)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetFillColor_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ DXSAMPLE Color);


void __RPC_STUB IDXSurfaceModifier_SetFillColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_GetFillColor_Proxy( 
    IDXSurfaceModifier * This,
    /* [out] */ DXSAMPLE *pColor);


void __RPC_STUB IDXSurfaceModifier_GetFillColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetBounds_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ const DXBNDS *pBounds);


void __RPC_STUB IDXSurfaceModifier_SetBounds_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetBackground_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ IDXSurface *pSurface);


void __RPC_STUB IDXSurfaceModifier_SetBackground_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_GetBackground_Proxy( 
    IDXSurfaceModifier * This,
    /* [out] */ IDXSurface **ppSurface);


void __RPC_STUB IDXSurfaceModifier_GetBackground_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetCompositeOperation_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ DXSURFMODCOMPOP CompOp);


void __RPC_STUB IDXSurfaceModifier_SetCompositeOperation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_GetCompositeOperation_Proxy( 
    IDXSurfaceModifier * This,
    /* [out] */ DXSURFMODCOMPOP *pCompOp);


void __RPC_STUB IDXSurfaceModifier_GetCompositeOperation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetForeground_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ IDXSurface *pSurface,
    /* [in] */ BOOL bTile,
    /* [in] */ const POINT *pOrigin);


void __RPC_STUB IDXSurfaceModifier_SetForeground_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_GetForeground_Proxy( 
    IDXSurfaceModifier * This,
    /* [out] */ IDXSurface **ppSurface,
    /* [out] */ BOOL *pbTile,
    /* [out] */ POINT *pOrigin);


void __RPC_STUB IDXSurfaceModifier_GetForeground_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetOpacity_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ float Opacity);


void __RPC_STUB IDXSurfaceModifier_SetOpacity_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_GetOpacity_Proxy( 
    IDXSurfaceModifier * This,
    /* [out] */ float *pOpacity);


void __RPC_STUB IDXSurfaceModifier_GetOpacity_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetLookup_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ IDXLookupTable *pLookupTable);


void __RPC_STUB IDXSurfaceModifier_SetLookup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_GetLookup_Proxy( 
    IDXSurfaceModifier * This,
    /* [out] */ IDXLookupTable **ppLookupTable);


void __RPC_STUB IDXSurfaceModifier_GetLookup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXSurfaceModifier_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0260 */
/* [local] */ 

typedef 
enum DXSAMPLEFORMATENUM
    {	DXPF_FLAGSMASK	= 0xffff0000,
	DXPF_NONPREMULT	= 0x10000,
	DXPF_TRANSPARENCY	= 0x20000,
	DXPF_TRANSLUCENCY	= 0x40000,
	DXPF_2BITERROR	= 0x200000,
	DXPF_3BITERROR	= 0x300000,
	DXPF_4BITERROR	= 0x400000,
	DXPF_5BITERROR	= 0x500000,
	DXPF_ERRORMASK	= 0x700000,
	DXPF_NONSTANDARD	= 0,
	DXPF_PMARGB32	= 1 | DXPF_TRANSPARENCY | DXPF_TRANSLUCENCY,
	DXPF_ARGB32	= 2 | DXPF_NONPREMULT | DXPF_TRANSPARENCY | DXPF_TRANSLUCENCY,
	DXPF_ARGB4444	= 3 | DXPF_NONPREMULT | DXPF_TRANSPARENCY | DXPF_TRANSLUCENCY | DXPF_4BITERROR,
	DXPF_A8	= 4 | DXPF_TRANSPARENCY | DXPF_TRANSLUCENCY,
	DXPF_RGB32	= 5,
	DXPF_RGB24	= 6,
	DXPF_RGB565	= 7 | DXPF_3BITERROR,
	DXPF_RGB555	= 8 | DXPF_3BITERROR,
	DXPF_RGB8	= 9 | DXPF_5BITERROR,
	DXPF_ARGB1555	= 10 | DXPF_TRANSPARENCY | DXPF_3BITERROR,
	DXPF_RGB32_CK	= DXPF_RGB32 | DXPF_TRANSPARENCY,
	DXPF_RGB24_CK	= DXPF_RGB24 | DXPF_TRANSPARENCY,
	DXPF_RGB555_CK	= DXPF_RGB555 | DXPF_TRANSPARENCY,
	DXPF_RGB565_CK	= DXPF_RGB565 | DXPF_TRANSPARENCY,
	DXPF_RGB8_CK	= DXPF_RGB8 | DXPF_TRANSPARENCY
    } 	DXSAMPLEFORMATENUM;

typedef 
enum DXLOCKSURF
    {	DXLOCKF_READ	= 0,
	DXLOCKF_READWRITE	= 1 << 0,
	DXLOCKF_EXISTINGINFOONLY	= 1 << 1,
	DXLOCKF_WANTRUNINFO	= 1 << 2,
	DXLOCKF_NONPREMULT	= 1 << 16,
	DXLOCKF_VALIDFLAGS	= DXLOCKF_READWRITE | DXLOCKF_EXISTINGINFOONLY | DXLOCKF_WANTRUNINFO | DXLOCKF_NONPREMULT
    } 	DXLOCKSURF;

typedef 
enum DXSURFSTATUS
    {	DXSURF_TRANSIENT	= 1 << 0,
	DXSURF_READONLY	= 1 << 1,
	DXSURF_VALIDFLAGS	= DXSURF_TRANSIENT | DXSURF_READONLY
    } 	DXSURFSTATUS;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0260_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0260_v0_0_s_ifspec;

#ifndef __IDXSurface_INTERFACE_DEFINED__
#define __IDXSurface_INTERFACE_DEFINED__

/* interface IDXSurface */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXSurface;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B39FD73F-E139-11d1-9065-00C04FD9189D")
    IDXSurface : public IDXBaseObject
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetPixelFormat( 
            /* [out] */ GUID *pFormatID,
            /* [out] */ DXSAMPLEFORMATENUM *pSampleFormatEnum) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetBounds( 
            /* [out] */ DXBNDS *pBounds) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetStatusFlags( 
            /* [out] */ DWORD *pdwStatusFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetStatusFlags( 
            /* [in] */ DWORD dwStatusFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE LockSurface( 
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ ULONG ulTimeOut,
            /* [in] */ DWORD dwFlags,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppPointer,
            /* [out] */ ULONG *pulGenerationId) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetDirectDrawSurface( 
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppSurface) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetColorKey( 
            DXSAMPLE *pColorKey) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetColorKey( 
            DXSAMPLE ColorKey) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE LockSurfaceDC( 
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ ULONG ulTimeOut,
            /* [in] */ DWORD dwFlags,
            /* [out] */ IDXDCLock **ppDCLock) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetAppData( 
            DWORD_PTR dwAppData) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetAppData( 
            DWORD_PTR *pdwAppData) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXSurfaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXSurface * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXSurface * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXSurface * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetGenerationId )( 
            IDXSurface * This,
            /* [out] */ ULONG *pID);
        
        HRESULT ( STDMETHODCALLTYPE *IncrementGenerationId )( 
            IDXSurface * This,
            /* [in] */ BOOL bRefresh);
        
        HRESULT ( STDMETHODCALLTYPE *GetObjectSize )( 
            IDXSurface * This,
            /* [out] */ ULONG *pcbSize);
        
        HRESULT ( STDMETHODCALLTYPE *GetPixelFormat )( 
            IDXSurface * This,
            /* [out] */ GUID *pFormatID,
            /* [out] */ DXSAMPLEFORMATENUM *pSampleFormatEnum);
        
        HRESULT ( STDMETHODCALLTYPE *GetBounds )( 
            IDXSurface * This,
            /* [out] */ DXBNDS *pBounds);
        
        HRESULT ( STDMETHODCALLTYPE *GetStatusFlags )( 
            IDXSurface * This,
            /* [out] */ DWORD *pdwStatusFlags);
        
        HRESULT ( STDMETHODCALLTYPE *SetStatusFlags )( 
            IDXSurface * This,
            /* [in] */ DWORD dwStatusFlags);
        
        HRESULT ( STDMETHODCALLTYPE *LockSurface )( 
            IDXSurface * This,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ ULONG ulTimeOut,
            /* [in] */ DWORD dwFlags,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppPointer,
            /* [out] */ ULONG *pulGenerationId);
        
        HRESULT ( STDMETHODCALLTYPE *GetDirectDrawSurface )( 
            IDXSurface * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppSurface);
        
        HRESULT ( STDMETHODCALLTYPE *GetColorKey )( 
            IDXSurface * This,
            DXSAMPLE *pColorKey);
        
        HRESULT ( STDMETHODCALLTYPE *SetColorKey )( 
            IDXSurface * This,
            DXSAMPLE ColorKey);
        
        HRESULT ( STDMETHODCALLTYPE *LockSurfaceDC )( 
            IDXSurface * This,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ ULONG ulTimeOut,
            /* [in] */ DWORD dwFlags,
            /* [out] */ IDXDCLock **ppDCLock);
        
        HRESULT ( STDMETHODCALLTYPE *SetAppData )( 
            IDXSurface * This,
            DWORD_PTR dwAppData);
        
        HRESULT ( STDMETHODCALLTYPE *GetAppData )( 
            IDXSurface * This,
            DWORD_PTR *pdwAppData);
        
        END_INTERFACE
    } IDXSurfaceVtbl;

    interface IDXSurface
    {
        CONST_VTBL struct IDXSurfaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXSurface_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXSurface_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXSurface_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXSurface_GetGenerationId(This,pID)	\
    (This)->lpVtbl -> GetGenerationId(This,pID)

#define IDXSurface_IncrementGenerationId(This,bRefresh)	\
    (This)->lpVtbl -> IncrementGenerationId(This,bRefresh)

#define IDXSurface_GetObjectSize(This,pcbSize)	\
    (This)->lpVtbl -> GetObjectSize(This,pcbSize)


#define IDXSurface_GetPixelFormat(This,pFormatID,pSampleFormatEnum)	\
    (This)->lpVtbl -> GetPixelFormat(This,pFormatID,pSampleFormatEnum)

#define IDXSurface_GetBounds(This,pBounds)	\
    (This)->lpVtbl -> GetBounds(This,pBounds)

#define IDXSurface_GetStatusFlags(This,pdwStatusFlags)	\
    (This)->lpVtbl -> GetStatusFlags(This,pdwStatusFlags)

#define IDXSurface_SetStatusFlags(This,dwStatusFlags)	\
    (This)->lpVtbl -> SetStatusFlags(This,dwStatusFlags)

#define IDXSurface_LockSurface(This,pBounds,ulTimeOut,dwFlags,riid,ppPointer,pulGenerationId)	\
    (This)->lpVtbl -> LockSurface(This,pBounds,ulTimeOut,dwFlags,riid,ppPointer,pulGenerationId)

#define IDXSurface_GetDirectDrawSurface(This,riid,ppSurface)	\
    (This)->lpVtbl -> GetDirectDrawSurface(This,riid,ppSurface)

#define IDXSurface_GetColorKey(This,pColorKey)	\
    (This)->lpVtbl -> GetColorKey(This,pColorKey)

#define IDXSurface_SetColorKey(This,ColorKey)	\
    (This)->lpVtbl -> SetColorKey(This,ColorKey)

#define IDXSurface_LockSurfaceDC(This,pBounds,ulTimeOut,dwFlags,ppDCLock)	\
    (This)->lpVtbl -> LockSurfaceDC(This,pBounds,ulTimeOut,dwFlags,ppDCLock)

#define IDXSurface_SetAppData(This,dwAppData)	\
    (This)->lpVtbl -> SetAppData(This,dwAppData)

#define IDXSurface_GetAppData(This,pdwAppData)	\
    (This)->lpVtbl -> GetAppData(This,pdwAppData)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXSurface_GetPixelFormat_Proxy( 
    IDXSurface * This,
    /* [out] */ GUID *pFormatID,
    /* [out] */ DXSAMPLEFORMATENUM *pSampleFormatEnum);


void __RPC_STUB IDXSurface_GetPixelFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_GetBounds_Proxy( 
    IDXSurface * This,
    /* [out] */ DXBNDS *pBounds);


void __RPC_STUB IDXSurface_GetBounds_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_GetStatusFlags_Proxy( 
    IDXSurface * This,
    /* [out] */ DWORD *pdwStatusFlags);


void __RPC_STUB IDXSurface_GetStatusFlags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_SetStatusFlags_Proxy( 
    IDXSurface * This,
    /* [in] */ DWORD dwStatusFlags);


void __RPC_STUB IDXSurface_SetStatusFlags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_LockSurface_Proxy( 
    IDXSurface * This,
    /* [in] */ const DXBNDS *pBounds,
    /* [in] */ ULONG ulTimeOut,
    /* [in] */ DWORD dwFlags,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppPointer,
    /* [out] */ ULONG *pulGenerationId);


void __RPC_STUB IDXSurface_LockSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_GetDirectDrawSurface_Proxy( 
    IDXSurface * This,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppSurface);


void __RPC_STUB IDXSurface_GetDirectDrawSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_GetColorKey_Proxy( 
    IDXSurface * This,
    DXSAMPLE *pColorKey);


void __RPC_STUB IDXSurface_GetColorKey_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_SetColorKey_Proxy( 
    IDXSurface * This,
    DXSAMPLE ColorKey);


void __RPC_STUB IDXSurface_SetColorKey_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_LockSurfaceDC_Proxy( 
    IDXSurface * This,
    /* [in] */ const DXBNDS *pBounds,
    /* [in] */ ULONG ulTimeOut,
    /* [in] */ DWORD dwFlags,
    /* [out] */ IDXDCLock **ppDCLock);


void __RPC_STUB IDXSurface_LockSurfaceDC_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_SetAppData_Proxy( 
    IDXSurface * This,
    DWORD_PTR dwAppData);


void __RPC_STUB IDXSurface_SetAppData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_GetAppData_Proxy( 
    IDXSurface * This,
    DWORD_PTR *pdwAppData);


void __RPC_STUB IDXSurface_GetAppData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXSurface_INTERFACE_DEFINED__ */


#ifndef __IDXSurfaceInit_INTERFACE_DEFINED__
#define __IDXSurfaceInit_INTERFACE_DEFINED__

/* interface IDXSurfaceInit */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXSurfaceInit;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9EA3B639-C37D-11d1-905E-00C04FD9189D")
    IDXSurfaceInit : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE InitSurface( 
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ DWORD dwFlags) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXSurfaceInitVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXSurfaceInit * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXSurfaceInit * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXSurfaceInit * This);
        
        HRESULT ( STDMETHODCALLTYPE *InitSurface )( 
            IDXSurfaceInit * This,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ DWORD dwFlags);
        
        END_INTERFACE
    } IDXSurfaceInitVtbl;

    interface IDXSurfaceInit
    {
        CONST_VTBL struct IDXSurfaceInitVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXSurfaceInit_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXSurfaceInit_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXSurfaceInit_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXSurfaceInit_InitSurface(This,pDirectDraw,pDDSurfaceDesc,pFormatID,pBounds,dwFlags)	\
    (This)->lpVtbl -> InitSurface(This,pDirectDraw,pDDSurfaceDesc,pFormatID,pBounds,dwFlags)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXSurfaceInit_InitSurface_Proxy( 
    IDXSurfaceInit * This,
    /* [in] */ IUnknown *pDirectDraw,
    /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
    /* [in] */ const GUID *pFormatID,
    /* [in] */ const DXBNDS *pBounds,
    /* [in] */ DWORD dwFlags);


void __RPC_STUB IDXSurfaceInit_InitSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXSurfaceInit_INTERFACE_DEFINED__ */


#ifndef __IDXARGBSurfaceInit_INTERFACE_DEFINED__
#define __IDXARGBSurfaceInit_INTERFACE_DEFINED__

/* interface IDXARGBSurfaceInit */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXARGBSurfaceInit;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9EA3B63A-C37D-11d1-905E-00C04FD9189D")
    IDXARGBSurfaceInit : public IDXSurfaceInit
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE InitFromDDSurface( 
            /* [in] */ IUnknown *pDDrawSurface,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ DWORD dwFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE InitFromRawSurface( 
            /* [in] */ IDXRawSurface *pRawSurface) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXARGBSurfaceInitVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXARGBSurfaceInit * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXARGBSurfaceInit * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXARGBSurfaceInit * This);
        
        HRESULT ( STDMETHODCALLTYPE *InitSurface )( 
            IDXARGBSurfaceInit * This,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ DWORD dwFlags);
        
        HRESULT ( STDMETHODCALLTYPE *InitFromDDSurface )( 
            IDXARGBSurfaceInit * This,
            /* [in] */ IUnknown *pDDrawSurface,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ DWORD dwFlags);
        
        HRESULT ( STDMETHODCALLTYPE *InitFromRawSurface )( 
            IDXARGBSurfaceInit * This,
            /* [in] */ IDXRawSurface *pRawSurface);
        
        END_INTERFACE
    } IDXARGBSurfaceInitVtbl;

    interface IDXARGBSurfaceInit
    {
        CONST_VTBL struct IDXARGBSurfaceInitVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXARGBSurfaceInit_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXARGBSurfaceInit_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXARGBSurfaceInit_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXARGBSurfaceInit_InitSurface(This,pDirectDraw,pDDSurfaceDesc,pFormatID,pBounds,dwFlags)	\
    (This)->lpVtbl -> InitSurface(This,pDirectDraw,pDDSurfaceDesc,pFormatID,pBounds,dwFlags)


#define IDXARGBSurfaceInit_InitFromDDSurface(This,pDDrawSurface,pFormatID,dwFlags)	\
    (This)->lpVtbl -> InitFromDDSurface(This,pDDrawSurface,pFormatID,dwFlags)

#define IDXARGBSurfaceInit_InitFromRawSurface(This,pRawSurface)	\
    (This)->lpVtbl -> InitFromRawSurface(This,pRawSurface)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXARGBSurfaceInit_InitFromDDSurface_Proxy( 
    IDXARGBSurfaceInit * This,
    /* [in] */ IUnknown *pDDrawSurface,
    /* [in] */ const GUID *pFormatID,
    /* [in] */ DWORD dwFlags);


void __RPC_STUB IDXARGBSurfaceInit_InitFromDDSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXARGBSurfaceInit_InitFromRawSurface_Proxy( 
    IDXARGBSurfaceInit * This,
    /* [in] */ IDXRawSurface *pRawSurface);


void __RPC_STUB IDXARGBSurfaceInit_InitFromRawSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXARGBSurfaceInit_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0263 */
/* [local] */ 

typedef struct tagDXNATIVETYPEINFO
    {
    BYTE *pCurrentData;
    BYTE *pFirstByte;
    long lPitch;
    DWORD dwColorKey;
    } 	DXNATIVETYPEINFO;

typedef struct tagDXPACKEDRECTDESC
    {
    DXBASESAMPLE *pSamples;
    BOOL bPremult;
    RECT rect;
    long lRowPadding;
    } 	DXPACKEDRECTDESC;

typedef struct tagDXOVERSAMPLEDESC
    {
    POINT p;
    DXPMSAMPLE Color;
    } 	DXOVERSAMPLEDESC;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0263_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0263_v0_0_s_ifspec;

#ifndef __IDXARGBReadPtr_INTERFACE_DEFINED__
#define __IDXARGBReadPtr_INTERFACE_DEFINED__

/* interface IDXARGBReadPtr */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXARGBReadPtr;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EAAAC2D6-C290-11d1-905D-00C04FD9189D")
    IDXARGBReadPtr : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetSurface( 
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppSurface) = 0;
        
        virtual DXSAMPLEFORMATENUM STDMETHODCALLTYPE GetNativeType( 
            /* [out] */ DXNATIVETYPEINFO *pInfo) = 0;
        
        virtual void STDMETHODCALLTYPE Move( 
            /* [in] */ long cSamples) = 0;
        
        virtual void STDMETHODCALLTYPE MoveToRow( 
            /* [in] */ ULONG y) = 0;
        
        virtual void STDMETHODCALLTYPE MoveToXY( 
            /* [in] */ ULONG x,
            /* [in] */ ULONG y) = 0;
        
        virtual ULONG STDMETHODCALLTYPE MoveAndGetRunInfo( 
            /* [in] */ ULONG Row,
            /* [out] */ const DXRUNINFO **ppInfo) = 0;
        
        virtual DXSAMPLE *STDMETHODCALLTYPE Unpack( 
            /* [in] */ DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bMove) = 0;
        
        virtual DXPMSAMPLE *STDMETHODCALLTYPE UnpackPremult( 
            /* [in] */ DXPMSAMPLE *pSamples,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bMove) = 0;
        
        virtual void STDMETHODCALLTYPE UnpackRect( 
            /* [in] */ const DXPACKEDRECTDESC *pRectDesc) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXARGBReadPtrVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXARGBReadPtr * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXARGBReadPtr * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXARGBReadPtr * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetSurface )( 
            IDXARGBReadPtr * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppSurface);
        
        DXSAMPLEFORMATENUM ( STDMETHODCALLTYPE *GetNativeType )( 
            IDXARGBReadPtr * This,
            /* [out] */ DXNATIVETYPEINFO *pInfo);
        
        void ( STDMETHODCALLTYPE *Move )( 
            IDXARGBReadPtr * This,
            /* [in] */ long cSamples);
        
        void ( STDMETHODCALLTYPE *MoveToRow )( 
            IDXARGBReadPtr * This,
            /* [in] */ ULONG y);
        
        void ( STDMETHODCALLTYPE *MoveToXY )( 
            IDXARGBReadPtr * This,
            /* [in] */ ULONG x,
            /* [in] */ ULONG y);
        
        ULONG ( STDMETHODCALLTYPE *MoveAndGetRunInfo )( 
            IDXARGBReadPtr * This,
            /* [in] */ ULONG Row,
            /* [out] */ const DXRUNINFO **ppInfo);
        
        DXSAMPLE *( STDMETHODCALLTYPE *Unpack )( 
            IDXARGBReadPtr * This,
            /* [in] */ DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bMove);
        
        DXPMSAMPLE *( STDMETHODCALLTYPE *UnpackPremult )( 
            IDXARGBReadPtr * This,
            /* [in] */ DXPMSAMPLE *pSamples,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bMove);
        
        void ( STDMETHODCALLTYPE *UnpackRect )( 
            IDXARGBReadPtr * This,
            /* [in] */ const DXPACKEDRECTDESC *pRectDesc);
        
        END_INTERFACE
    } IDXARGBReadPtrVtbl;

    interface IDXARGBReadPtr
    {
        CONST_VTBL struct IDXARGBReadPtrVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXARGBReadPtr_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXARGBReadPtr_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXARGBReadPtr_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXARGBReadPtr_GetSurface(This,riid,ppSurface)	\
    (This)->lpVtbl -> GetSurface(This,riid,ppSurface)

#define IDXARGBReadPtr_GetNativeType(This,pInfo)	\
    (This)->lpVtbl -> GetNativeType(This,pInfo)

#define IDXARGBReadPtr_Move(This,cSamples)	\
    (This)->lpVtbl -> Move(This,cSamples)

#define IDXARGBReadPtr_MoveToRow(This,y)	\
    (This)->lpVtbl -> MoveToRow(This,y)

#define IDXARGBReadPtr_MoveToXY(This,x,y)	\
    (This)->lpVtbl -> MoveToXY(This,x,y)

#define IDXARGBReadPtr_MoveAndGetRunInfo(This,Row,ppInfo)	\
    (This)->lpVtbl -> MoveAndGetRunInfo(This,Row,ppInfo)

#define IDXARGBReadPtr_Unpack(This,pSamples,cSamples,bMove)	\
    (This)->lpVtbl -> Unpack(This,pSamples,cSamples,bMove)

#define IDXARGBReadPtr_UnpackPremult(This,pSamples,cSamples,bMove)	\
    (This)->lpVtbl -> UnpackPremult(This,pSamples,cSamples,bMove)

#define IDXARGBReadPtr_UnpackRect(This,pRectDesc)	\
    (This)->lpVtbl -> UnpackRect(This,pRectDesc)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXARGBReadPtr_GetSurface_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppSurface);


void __RPC_STUB IDXARGBReadPtr_GetSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


DXSAMPLEFORMATENUM STDMETHODCALLTYPE IDXARGBReadPtr_GetNativeType_Proxy( 
    IDXARGBReadPtr * This,
    /* [out] */ DXNATIVETYPEINFO *pInfo);


void __RPC_STUB IDXARGBReadPtr_GetNativeType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadPtr_Move_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ long cSamples);


void __RPC_STUB IDXARGBReadPtr_Move_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadPtr_MoveToRow_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ ULONG y);


void __RPC_STUB IDXARGBReadPtr_MoveToRow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadPtr_MoveToXY_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ ULONG x,
    /* [in] */ ULONG y);


void __RPC_STUB IDXARGBReadPtr_MoveToXY_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


ULONG STDMETHODCALLTYPE IDXARGBReadPtr_MoveAndGetRunInfo_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ ULONG Row,
    /* [out] */ const DXRUNINFO **ppInfo);


void __RPC_STUB IDXARGBReadPtr_MoveAndGetRunInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


DXSAMPLE *STDMETHODCALLTYPE IDXARGBReadPtr_Unpack_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ DXSAMPLE *pSamples,
    /* [in] */ ULONG cSamples,
    /* [in] */ BOOL bMove);


void __RPC_STUB IDXARGBReadPtr_Unpack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


DXPMSAMPLE *STDMETHODCALLTYPE IDXARGBReadPtr_UnpackPremult_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ DXPMSAMPLE *pSamples,
    /* [in] */ ULONG cSamples,
    /* [in] */ BOOL bMove);


void __RPC_STUB IDXARGBReadPtr_UnpackPremult_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadPtr_UnpackRect_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ const DXPACKEDRECTDESC *pRectDesc);


void __RPC_STUB IDXARGBReadPtr_UnpackRect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXARGBReadPtr_INTERFACE_DEFINED__ */


#ifndef __IDXARGBReadWritePtr_INTERFACE_DEFINED__
#define __IDXARGBReadWritePtr_INTERFACE_DEFINED__

/* interface IDXARGBReadWritePtr */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXARGBReadWritePtr;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EAAAC2D7-C290-11d1-905D-00C04FD9189D")
    IDXARGBReadWritePtr : public IDXARGBReadPtr
    {
    public:
        virtual void STDMETHODCALLTYPE PackAndMove( 
            /* [in] */ const DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples) = 0;
        
        virtual void STDMETHODCALLTYPE PackPremultAndMove( 
            /* [in] */ const DXPMSAMPLE *pSamples,
            /* [in] */ ULONG cSamples) = 0;
        
        virtual void STDMETHODCALLTYPE PackRect( 
            /* [in] */ const DXPACKEDRECTDESC *pRectDesc) = 0;
        
        virtual void STDMETHODCALLTYPE CopyAndMoveBoth( 
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ IDXARGBReadPtr *pSrc,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bIsOpaque) = 0;
        
        virtual void STDMETHODCALLTYPE CopyRect( 
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ const RECT *pDestRect,
            /* [in] */ IDXARGBReadPtr *pSrc,
            /* [in] */ const POINT *pSrcOrigin,
            /* [in] */ BOOL bIsOpaque) = 0;
        
        virtual void STDMETHODCALLTYPE FillAndMove( 
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ DXPMSAMPLE SampVal,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bDoOver) = 0;
        
        virtual void STDMETHODCALLTYPE FillRect( 
            /* [in] */ const RECT *pRect,
            /* [in] */ DXPMSAMPLE SampVal,
            /* [in] */ BOOL bDoOver) = 0;
        
        virtual void STDMETHODCALLTYPE OverSample( 
            /* [in] */ const DXOVERSAMPLEDESC *pOverDesc) = 0;
        
        virtual void STDMETHODCALLTYPE OverArrayAndMove( 
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ const DXPMSAMPLE *pSrc,
            /* [in] */ ULONG cSamples) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXARGBReadWritePtrVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXARGBReadWritePtr * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXARGBReadWritePtr * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetSurface )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppSurface);
        
        DXSAMPLEFORMATENUM ( STDMETHODCALLTYPE *GetNativeType )( 
            IDXARGBReadWritePtr * This,
            /* [out] */ DXNATIVETYPEINFO *pInfo);
        
        void ( STDMETHODCALLTYPE *Move )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ long cSamples);
        
        void ( STDMETHODCALLTYPE *MoveToRow )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ ULONG y);
        
        void ( STDMETHODCALLTYPE *MoveToXY )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ ULONG x,
            /* [in] */ ULONG y);
        
        ULONG ( STDMETHODCALLTYPE *MoveAndGetRunInfo )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ ULONG Row,
            /* [out] */ const DXRUNINFO **ppInfo);
        
        DXSAMPLE *( STDMETHODCALLTYPE *Unpack )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bMove);
        
        DXPMSAMPLE *( STDMETHODCALLTYPE *UnpackPremult )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ DXPMSAMPLE *pSamples,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bMove);
        
        void ( STDMETHODCALLTYPE *UnpackRect )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ const DXPACKEDRECTDESC *pRectDesc);
        
        void ( STDMETHODCALLTYPE *PackAndMove )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ const DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples);
        
        void ( STDMETHODCALLTYPE *PackPremultAndMove )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ const DXPMSAMPLE *pSamples,
            /* [in] */ ULONG cSamples);
        
        void ( STDMETHODCALLTYPE *PackRect )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ const DXPACKEDRECTDESC *pRectDesc);
        
        void ( STDMETHODCALLTYPE *CopyAndMoveBoth )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ IDXARGBReadPtr *pSrc,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bIsOpaque);
        
        void ( STDMETHODCALLTYPE *CopyRect )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ const RECT *pDestRect,
            /* [in] */ IDXARGBReadPtr *pSrc,
            /* [in] */ const POINT *pSrcOrigin,
            /* [in] */ BOOL bIsOpaque);
        
        void ( STDMETHODCALLTYPE *FillAndMove )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ DXPMSAMPLE SampVal,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bDoOver);
        
        void ( STDMETHODCALLTYPE *FillRect )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ const RECT *pRect,
            /* [in] */ DXPMSAMPLE SampVal,
            /* [in] */ BOOL bDoOver);
        
        void ( STDMETHODCALLTYPE *OverSample )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ const DXOVERSAMPLEDESC *pOverDesc);
        
        void ( STDMETHODCALLTYPE *OverArrayAndMove )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ const DXPMSAMPLE *pSrc,
            /* [in] */ ULONG cSamples);
        
        END_INTERFACE
    } IDXARGBReadWritePtrVtbl;

    interface IDXARGBReadWritePtr
    {
        CONST_VTBL struct IDXARGBReadWritePtrVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXARGBReadWritePtr_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXARGBReadWritePtr_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXARGBReadWritePtr_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXARGBReadWritePtr_GetSurface(This,riid,ppSurface)	\
    (This)->lpVtbl -> GetSurface(This,riid,ppSurface)

#define IDXARGBReadWritePtr_GetNativeType(This,pInfo)	\
    (This)->lpVtbl -> GetNativeType(This,pInfo)

#define IDXARGBReadWritePtr_Move(This,cSamples)	\
    (This)->lpVtbl -> Move(This,cSamples)

#define IDXARGBReadWritePtr_MoveToRow(This,y)	\
    (This)->lpVtbl -> MoveToRow(This,y)

#define IDXARGBReadWritePtr_MoveToXY(This,x,y)	\
    (This)->lpVtbl -> MoveToXY(This,x,y)

#define IDXARGBReadWritePtr_MoveAndGetRunInfo(This,Row,ppInfo)	\
    (This)->lpVtbl -> MoveAndGetRunInfo(This,Row,ppInfo)

#define IDXARGBReadWritePtr_Unpack(This,pSamples,cSamples,bMove)	\
    (This)->lpVtbl -> Unpack(This,pSamples,cSamples,bMove)

#define IDXARGBReadWritePtr_UnpackPremult(This,pSamples,cSamples,bMove)	\
    (This)->lpVtbl -> UnpackPremult(This,pSamples,cSamples,bMove)

#define IDXARGBReadWritePtr_UnpackRect(This,pRectDesc)	\
    (This)->lpVtbl -> UnpackRect(This,pRectDesc)


#define IDXARGBReadWritePtr_PackAndMove(This,pSamples,cSamples)	\
    (This)->lpVtbl -> PackAndMove(This,pSamples,cSamples)

#define IDXARGBReadWritePtr_PackPremultAndMove(This,pSamples,cSamples)	\
    (This)->lpVtbl -> PackPremultAndMove(This,pSamples,cSamples)

#define IDXARGBReadWritePtr_PackRect(This,pRectDesc)	\
    (This)->lpVtbl -> PackRect(This,pRectDesc)

#define IDXARGBReadWritePtr_CopyAndMoveBoth(This,pScratchBuffer,pSrc,cSamples,bIsOpaque)	\
    (This)->lpVtbl -> CopyAndMoveBoth(This,pScratchBuffer,pSrc,cSamples,bIsOpaque)

#define IDXARGBReadWritePtr_CopyRect(This,pScratchBuffer,pDestRect,pSrc,pSrcOrigin,bIsOpaque)	\
    (This)->lpVtbl -> CopyRect(This,pScratchBuffer,pDestRect,pSrc,pSrcOrigin,bIsOpaque)

#define IDXARGBReadWritePtr_FillAndMove(This,pScratchBuffer,SampVal,cSamples,bDoOver)	\
    (This)->lpVtbl -> FillAndMove(This,pScratchBuffer,SampVal,cSamples,bDoOver)

#define IDXARGBReadWritePtr_FillRect(This,pRect,SampVal,bDoOver)	\
    (This)->lpVtbl -> FillRect(This,pRect,SampVal,bDoOver)

#define IDXARGBReadWritePtr_OverSample(This,pOverDesc)	\
    (This)->lpVtbl -> OverSample(This,pOverDesc)

#define IDXARGBReadWritePtr_OverArrayAndMove(This,pScratchBuffer,pSrc,cSamples)	\
    (This)->lpVtbl -> OverArrayAndMove(This,pScratchBuffer,pSrc,cSamples)

#endif /* COBJMACROS */


#endif 	/* C style interface */



void STDMETHODCALLTYPE IDXARGBReadWritePtr_PackAndMove_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ const DXSAMPLE *pSamples,
    /* [in] */ ULONG cSamples);


void __RPC_STUB IDXARGBReadWritePtr_PackAndMove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_PackPremultAndMove_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ const DXPMSAMPLE *pSamples,
    /* [in] */ ULONG cSamples);


void __RPC_STUB IDXARGBReadWritePtr_PackPremultAndMove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_PackRect_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ const DXPACKEDRECTDESC *pRectDesc);


void __RPC_STUB IDXARGBReadWritePtr_PackRect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_CopyAndMoveBoth_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ DXBASESAMPLE *pScratchBuffer,
    /* [in] */ IDXARGBReadPtr *pSrc,
    /* [in] */ ULONG cSamples,
    /* [in] */ BOOL bIsOpaque);


void __RPC_STUB IDXARGBReadWritePtr_CopyAndMoveBoth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_CopyRect_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ DXBASESAMPLE *pScratchBuffer,
    /* [in] */ const RECT *pDestRect,
    /* [in] */ IDXARGBReadPtr *pSrc,
    /* [in] */ const POINT *pSrcOrigin,
    /* [in] */ BOOL bIsOpaque);


void __RPC_STUB IDXARGBReadWritePtr_CopyRect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_FillAndMove_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ DXBASESAMPLE *pScratchBuffer,
    /* [in] */ DXPMSAMPLE SampVal,
    /* [in] */ ULONG cSamples,
    /* [in] */ BOOL bDoOver);


void __RPC_STUB IDXARGBReadWritePtr_FillAndMove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_FillRect_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ const RECT *pRect,
    /* [in] */ DXPMSAMPLE SampVal,
    /* [in] */ BOOL bDoOver);


void __RPC_STUB IDXARGBReadWritePtr_FillRect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_OverSample_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ const DXOVERSAMPLEDESC *pOverDesc);


void __RPC_STUB IDXARGBReadWritePtr_OverSample_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_OverArrayAndMove_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ DXBASESAMPLE *pScratchBuffer,
    /* [in] */ const DXPMSAMPLE *pSrc,
    /* [in] */ ULONG cSamples);


void __RPC_STUB IDXARGBReadWritePtr_OverArrayAndMove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXARGBReadWritePtr_INTERFACE_DEFINED__ */


#ifndef __IDXDCLock_INTERFACE_DEFINED__
#define __IDXDCLock_INTERFACE_DEFINED__

/* interface IDXDCLock */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXDCLock;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0F619456-CF39-11d1-905E-00C04FD9189D")
    IDXDCLock : public IUnknown
    {
    public:
        virtual HDC STDMETHODCALLTYPE GetDC( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXDCLockVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXDCLock * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXDCLock * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXDCLock * This);
        
        HDC ( STDMETHODCALLTYPE *GetDC )( 
            IDXDCLock * This);
        
        END_INTERFACE
    } IDXDCLockVtbl;

    interface IDXDCLock
    {
        CONST_VTBL struct IDXDCLockVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXDCLock_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXDCLock_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXDCLock_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXDCLock_GetDC(This)	\
    (This)->lpVtbl -> GetDC(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HDC STDMETHODCALLTYPE IDXDCLock_GetDC_Proxy( 
    IDXDCLock * This);


void __RPC_STUB IDXDCLock_GetDC_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXDCLock_INTERFACE_DEFINED__ */


#ifndef __IDXTScaleOutput_INTERFACE_DEFINED__
#define __IDXTScaleOutput_INTERFACE_DEFINED__

/* interface IDXTScaleOutput */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXTScaleOutput;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B2024B50-EE77-11d1-9066-00C04FD9189D")
    IDXTScaleOutput : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetOutputSize( 
            /* [in] */ const SIZE OutSize,
            /* [in] */ BOOL bMaintainAspect) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXTScaleOutputVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXTScaleOutput * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXTScaleOutput * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXTScaleOutput * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetOutputSize )( 
            IDXTScaleOutput * This,
            /* [in] */ const SIZE OutSize,
            /* [in] */ BOOL bMaintainAspect);
        
        END_INTERFACE
    } IDXTScaleOutputVtbl;

    interface IDXTScaleOutput
    {
        CONST_VTBL struct IDXTScaleOutputVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXTScaleOutput_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXTScaleOutput_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXTScaleOutput_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXTScaleOutput_SetOutputSize(This,OutSize,bMaintainAspect)	\
    (This)->lpVtbl -> SetOutputSize(This,OutSize,bMaintainAspect)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXTScaleOutput_SetOutputSize_Proxy( 
    IDXTScaleOutput * This,
    /* [in] */ const SIZE OutSize,
    /* [in] */ BOOL bMaintainAspect);


void __RPC_STUB IDXTScaleOutput_SetOutputSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXTScaleOutput_INTERFACE_DEFINED__ */


#ifndef __IDXGradient_INTERFACE_DEFINED__
#define __IDXGradient_INTERFACE_DEFINED__

/* interface IDXGradient */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXGradient;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B2024B51-EE77-11d1-9066-00C04FD9189D")
    IDXGradient : public IDXTScaleOutput
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetGradient( 
            DXSAMPLE StartColor,
            DXSAMPLE EndColor,
            BOOL bHorizontal) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetOutputSize( 
            /* [out] */ SIZE *pOutSize) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXGradientVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXGradient * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXGradient * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXGradient * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetOutputSize )( 
            IDXGradient * This,
            /* [in] */ const SIZE OutSize,
            /* [in] */ BOOL bMaintainAspect);
        
        HRESULT ( STDMETHODCALLTYPE *SetGradient )( 
            IDXGradient * This,
            DXSAMPLE StartColor,
            DXSAMPLE EndColor,
            BOOL bHorizontal);
        
        HRESULT ( STDMETHODCALLTYPE *GetOutputSize )( 
            IDXGradient * This,
            /* [out] */ SIZE *pOutSize);
        
        END_INTERFACE
    } IDXGradientVtbl;

    interface IDXGradient
    {
        CONST_VTBL struct IDXGradientVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXGradient_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXGradient_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXGradient_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXGradient_SetOutputSize(This,OutSize,bMaintainAspect)	\
    (This)->lpVtbl -> SetOutputSize(This,OutSize,bMaintainAspect)


#define IDXGradient_SetGradient(This,StartColor,EndColor,bHorizontal)	\
    (This)->lpVtbl -> SetGradient(This,StartColor,EndColor,bHorizontal)

#define IDXGradient_GetOutputSize(This,pOutSize)	\
    (This)->lpVtbl -> GetOutputSize(This,pOutSize)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXGradient_SetGradient_Proxy( 
    IDXGradient * This,
    DXSAMPLE StartColor,
    DXSAMPLE EndColor,
    BOOL bHorizontal);


void __RPC_STUB IDXGradient_SetGradient_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXGradient_GetOutputSize_Proxy( 
    IDXGradient * This,
    /* [out] */ SIZE *pOutSize);


void __RPC_STUB IDXGradient_GetOutputSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXGradient_INTERFACE_DEFINED__ */


#ifndef __IDXTScale_INTERFACE_DEFINED__
#define __IDXTScale_INTERFACE_DEFINED__

/* interface IDXTScale */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXTScale;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B39FD742-E139-11d1-9065-00C04FD9189D")
    IDXTScale : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetScales( 
            /* [in] */ float Scales[ 2 ]) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetScales( 
            /* [out] */ float Scales[ 2 ]) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ScaleFitToSize( 
            /* [out][in] */ DXBNDS *pClipBounds,
            /* [in] */ SIZE FitToSize,
            /* [in] */ BOOL bMaintainAspect) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXTScaleVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXTScale * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXTScale * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXTScale * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetScales )( 
            IDXTScale * This,
            /* [in] */ float Scales[ 2 ]);
        
        HRESULT ( STDMETHODCALLTYPE *GetScales )( 
            IDXTScale * This,
            /* [out] */ float Scales[ 2 ]);
        
        HRESULT ( STDMETHODCALLTYPE *ScaleFitToSize )( 
            IDXTScale * This,
            /* [out][in] */ DXBNDS *pClipBounds,
            /* [in] */ SIZE FitToSize,
            /* [in] */ BOOL bMaintainAspect);
        
        END_INTERFACE
    } IDXTScaleVtbl;

    interface IDXTScale
    {
        CONST_VTBL struct IDXTScaleVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXTScale_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXTScale_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXTScale_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXTScale_SetScales(This,Scales)	\
    (This)->lpVtbl -> SetScales(This,Scales)

#define IDXTScale_GetScales(This,Scales)	\
    (This)->lpVtbl -> GetScales(This,Scales)

#define IDXTScale_ScaleFitToSize(This,pClipBounds,FitToSize,bMaintainAspect)	\
    (This)->lpVtbl -> ScaleFitToSize(This,pClipBounds,FitToSize,bMaintainAspect)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXTScale_SetScales_Proxy( 
    IDXTScale * This,
    /* [in] */ float Scales[ 2 ]);


void __RPC_STUB IDXTScale_SetScales_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTScale_GetScales_Proxy( 
    IDXTScale * This,
    /* [out] */ float Scales[ 2 ]);


void __RPC_STUB IDXTScale_GetScales_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTScale_ScaleFitToSize_Proxy( 
    IDXTScale * This,
    /* [out][in] */ DXBNDS *pClipBounds,
    /* [in] */ SIZE FitToSize,
    /* [in] */ BOOL bMaintainAspect);


void __RPC_STUB IDXTScale_ScaleFitToSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXTScale_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0269 */
/* [local] */ 

typedef 
enum DISPIDDXEFFECT
    {	DISPID_DXECAPABILITIES	= 10000,
	DISPID_DXEPROGRESS	= DISPID_DXECAPABILITIES + 1,
	DISPID_DXESTEP	= DISPID_DXEPROGRESS + 1,
	DISPID_DXEDURATION	= DISPID_DXESTEP + 1,
	DISPID_DXE_NEXT_ID	= DISPID_DXEDURATION + 1
    } 	DISPIDDXBOUNDEDEFFECT;

typedef 
enum DXEFFECTTYPE
    {	DXTET_PERIODIC	= 1 << 0,
	DXTET_MORPH	= 1 << 1
    } 	DXEFFECTTYPE;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0269_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0269_v0_0_s_ifspec;

#ifndef __IDXEffect_INTERFACE_DEFINED__
#define __IDXEffect_INTERFACE_DEFINED__

/* interface IDXEffect */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IDXEffect;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E31FB81B-1335-11d1-8189-0000F87557DB")
    IDXEffect : public IDispatch
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Capabilities( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Progress( 
            /* [retval][out] */ float *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Progress( 
            /* [in] */ float newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_StepResolution( 
            /* [retval][out] */ float *pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Duration( 
            /* [retval][out] */ float *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Duration( 
            /* [in] */ float newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXEffectVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXEffect * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXEffect * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXEffect * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDXEffect * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDXEffect * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDXEffect * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDXEffect * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Capabilities )( 
            IDXEffect * This,
            /* [retval][out] */ long *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Progress )( 
            IDXEffect * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Progress )( 
            IDXEffect * This,
            /* [in] */ float newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StepResolution )( 
            IDXEffect * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            IDXEffect * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Duration )( 
            IDXEffect * This,
            /* [in] */ float newVal);
        
        END_INTERFACE
    } IDXEffectVtbl;

    interface IDXEffect
    {
        CONST_VTBL struct IDXEffectVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXEffect_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXEffect_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXEffect_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXEffect_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IDXEffect_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IDXEffect_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IDXEffect_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IDXEffect_get_Capabilities(This,pVal)	\
    (This)->lpVtbl -> get_Capabilities(This,pVal)

#define IDXEffect_get_Progress(This,pVal)	\
    (This)->lpVtbl -> get_Progress(This,pVal)

#define IDXEffect_put_Progress(This,newVal)	\
    (This)->lpVtbl -> put_Progress(This,newVal)

#define IDXEffect_get_StepResolution(This,pVal)	\
    (This)->lpVtbl -> get_StepResolution(This,pVal)

#define IDXEffect_get_Duration(This,pVal)	\
    (This)->lpVtbl -> get_Duration(This,pVal)

#define IDXEffect_put_Duration(This,newVal)	\
    (This)->lpVtbl -> put_Duration(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDXEffect_get_Capabilities_Proxy( 
    IDXEffect * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDXEffect_get_Capabilities_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDXEffect_get_Progress_Proxy( 
    IDXEffect * This,
    /* [retval][out] */ float *pVal);


void __RPC_STUB IDXEffect_get_Progress_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IDXEffect_put_Progress_Proxy( 
    IDXEffect * This,
    /* [in] */ float newVal);


void __RPC_STUB IDXEffect_put_Progress_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDXEffect_get_StepResolution_Proxy( 
    IDXEffect * This,
    /* [retval][out] */ float *pVal);


void __RPC_STUB IDXEffect_get_StepResolution_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDXEffect_get_Duration_Proxy( 
    IDXEffect * This,
    /* [retval][out] */ float *pVal);


void __RPC_STUB IDXEffect_get_Duration_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IDXEffect_put_Duration_Proxy( 
    IDXEffect * This,
    /* [in] */ float newVal);


void __RPC_STUB IDXEffect_put_Duration_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXEffect_INTERFACE_DEFINED__ */


#ifndef __IDXLookupTable_INTERFACE_DEFINED__
#define __IDXLookupTable_INTERFACE_DEFINED__

/* interface IDXLookupTable */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXLookupTable;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("01BAFC7F-9E63-11d1-9053-00C04FD9189D")
    IDXLookupTable : public IDXBaseObject
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetTables( 
            /* [out] */ BYTE RedLUT[ 256 ],
            /* [out] */ BYTE GreenLUT[ 256 ],
            /* [out] */ BYTE BlueLUT[ 256 ],
            /* [out] */ BYTE AlphaLUT[ 256 ]) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IsChannelIdentity( 
            /* [out] */ DXBASESAMPLE *pSampleBools) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetIndexValues( 
            /* [in] */ ULONG Index,
            /* [out] */ DXBASESAMPLE *pSample) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ApplyTables( 
            /* [out][in] */ DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXLookupTableVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXLookupTable * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXLookupTable * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXLookupTable * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetGenerationId )( 
            IDXLookupTable * This,
            /* [out] */ ULONG *pID);
        
        HRESULT ( STDMETHODCALLTYPE *IncrementGenerationId )( 
            IDXLookupTable * This,
            /* [in] */ BOOL bRefresh);
        
        HRESULT ( STDMETHODCALLTYPE *GetObjectSize )( 
            IDXLookupTable * This,
            /* [out] */ ULONG *pcbSize);
        
        HRESULT ( STDMETHODCALLTYPE *GetTables )( 
            IDXLookupTable * This,
            /* [out] */ BYTE RedLUT[ 256 ],
            /* [out] */ BYTE GreenLUT[ 256 ],
            /* [out] */ BYTE BlueLUT[ 256 ],
            /* [out] */ BYTE AlphaLUT[ 256 ]);
        
        HRESULT ( STDMETHODCALLTYPE *IsChannelIdentity )( 
            IDXLookupTable * This,
            /* [out] */ DXBASESAMPLE *pSampleBools);
        
        HRESULT ( STDMETHODCALLTYPE *GetIndexValues )( 
            IDXLookupTable * This,
            /* [in] */ ULONG Index,
            /* [out] */ DXBASESAMPLE *pSample);
        
        HRESULT ( STDMETHODCALLTYPE *ApplyTables )( 
            IDXLookupTable * This,
            /* [out][in] */ DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples);
        
        END_INTERFACE
    } IDXLookupTableVtbl;

    interface IDXLookupTable
    {
        CONST_VTBL struct IDXLookupTableVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXLookupTable_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXLookupTable_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXLookupTable_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXLookupTable_GetGenerationId(This,pID)	\
    (This)->lpVtbl -> GetGenerationId(This,pID)

#define IDXLookupTable_IncrementGenerationId(This,bRefresh)	\
    (This)->lpVtbl -> IncrementGenerationId(This,bRefresh)

#define IDXLookupTable_GetObjectSize(This,pcbSize)	\
    (This)->lpVtbl -> GetObjectSize(This,pcbSize)


#define IDXLookupTable_GetTables(This,RedLUT,GreenLUT,BlueLUT,AlphaLUT)	\
    (This)->lpVtbl -> GetTables(This,RedLUT,GreenLUT,BlueLUT,AlphaLUT)

#define IDXLookupTable_IsChannelIdentity(This,pSampleBools)	\
    (This)->lpVtbl -> IsChannelIdentity(This,pSampleBools)

#define IDXLookupTable_GetIndexValues(This,Index,pSample)	\
    (This)->lpVtbl -> GetIndexValues(This,Index,pSample)

#define IDXLookupTable_ApplyTables(This,pSamples,cSamples)	\
    (This)->lpVtbl -> ApplyTables(This,pSamples,cSamples)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXLookupTable_GetTables_Proxy( 
    IDXLookupTable * This,
    /* [out] */ BYTE RedLUT[ 256 ],
    /* [out] */ BYTE GreenLUT[ 256 ],
    /* [out] */ BYTE BlueLUT[ 256 ],
    /* [out] */ BYTE AlphaLUT[ 256 ]);


void __RPC_STUB IDXLookupTable_GetTables_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXLookupTable_IsChannelIdentity_Proxy( 
    IDXLookupTable * This,
    /* [out] */ DXBASESAMPLE *pSampleBools);


void __RPC_STUB IDXLookupTable_IsChannelIdentity_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXLookupTable_GetIndexValues_Proxy( 
    IDXLookupTable * This,
    /* [in] */ ULONG Index,
    /* [out] */ DXBASESAMPLE *pSample);


void __RPC_STUB IDXLookupTable_GetIndexValues_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXLookupTable_ApplyTables_Proxy( 
    IDXLookupTable * This,
    /* [out][in] */ DXSAMPLE *pSamples,
    /* [in] */ ULONG cSamples);


void __RPC_STUB IDXLookupTable_ApplyTables_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXLookupTable_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0271 */
/* [local] */ 

typedef struct DXRAWSURFACEINFO
    {
    BYTE *pFirstByte;
    long lPitch;
    ULONG Width;
    ULONG Height;
    const GUID *pPixelFormat;
    HDC hdc;
    DWORD dwColorKey;
    DXBASESAMPLE *pPalette;
    } 	DXRAWSURFACEINFO;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0271_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0271_v0_0_s_ifspec;

#ifndef __IDXRawSurface_INTERFACE_DEFINED__
#define __IDXRawSurface_INTERFACE_DEFINED__

/* interface IDXRawSurface */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXRawSurface;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("09756C8A-D96A-11d1-9062-00C04FD9189D")
    IDXRawSurface : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetSurfaceInfo( 
            DXRAWSURFACEINFO *pSurfaceInfo) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXRawSurfaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXRawSurface * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXRawSurface * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXRawSurface * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetSurfaceInfo )( 
            IDXRawSurface * This,
            DXRAWSURFACEINFO *pSurfaceInfo);
        
        END_INTERFACE
    } IDXRawSurfaceVtbl;

    interface IDXRawSurface
    {
        CONST_VTBL struct IDXRawSurfaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXRawSurface_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXRawSurface_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXRawSurface_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXRawSurface_GetSurfaceInfo(This,pSurfaceInfo)	\
    (This)->lpVtbl -> GetSurfaceInfo(This,pSurfaceInfo)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXRawSurface_GetSurfaceInfo_Proxy( 
    IDXRawSurface * This,
    DXRAWSURFACEINFO *pSurfaceInfo);


void __RPC_STUB IDXRawSurface_GetSurfaceInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXRawSurface_INTERFACE_DEFINED__ */


#ifndef __IHTMLDXTransform_INTERFACE_DEFINED__
#define __IHTMLDXTransform_INTERFACE_DEFINED__

/* interface IHTMLDXTransform */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IHTMLDXTransform;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("30E2AB7D-4FDD-4159-B7EA-DC722BF4ADE5")
    IHTMLDXTransform : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetHostUrl( 
            BSTR bstrHostUrl) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHTMLDXTransformVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IHTMLDXTransform * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IHTMLDXTransform * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IHTMLDXTransform * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetHostUrl )( 
            IHTMLDXTransform * This,
            BSTR bstrHostUrl);
        
        END_INTERFACE
    } IHTMLDXTransformVtbl;

    interface IHTMLDXTransform
    {
        CONST_VTBL struct IHTMLDXTransformVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHTMLDXTransform_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IHTMLDXTransform_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IHTMLDXTransform_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IHTMLDXTransform_SetHostUrl(This,bstrHostUrl)	\
    (This)->lpVtbl -> SetHostUrl(This,bstrHostUrl)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IHTMLDXTransform_SetHostUrl_Proxy( 
    IHTMLDXTransform * This,
    BSTR bstrHostUrl);


void __RPC_STUB IHTMLDXTransform_SetHostUrl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IHTMLDXTransform_INTERFACE_DEFINED__ */



#ifndef __DXTRANSLib_LIBRARY_DEFINED__
#define __DXTRANSLib_LIBRARY_DEFINED__

/* library DXTRANSLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_DXTRANSLib;

EXTERN_C const CLSID CLSID_DXTransformFactory;

#ifdef __cplusplus

class DECLSPEC_UUID("D1FE6762-FC48-11D0-883A-3C8B00C10000")
DXTransformFactory;
#endif

EXTERN_C const CLSID CLSID_DXTaskManager;

#ifdef __cplusplus

class DECLSPEC_UUID("4CB26C03-FF93-11d0-817E-0000F87557DB")
DXTaskManager;
#endif

EXTERN_C const CLSID CLSID_DXTScale;

#ifdef __cplusplus

class DECLSPEC_UUID("555278E2-05DB-11D1-883A-3C8B00C10000")
DXTScale;
#endif

EXTERN_C const CLSID CLSID_DXSurface;

#ifdef __cplusplus

class DECLSPEC_UUID("0E890F83-5F79-11D1-9043-00C04FD9189D")
DXSurface;
#endif

EXTERN_C const CLSID CLSID_DXSurfaceModifier;

#ifdef __cplusplus

class DECLSPEC_UUID("3E669F1D-9C23-11d1-9053-00C04FD9189D")
DXSurfaceModifier;
#endif

EXTERN_C const CLSID CLSID_DXGradient;

#ifdef __cplusplus

class DECLSPEC_UUID("C6365470-F667-11d1-9067-00C04FD9189D")
DXGradient;
#endif
#endif /* __DXTRANSLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif

//#include <d3drm.h>
/*==========================================================================;
 *
 *  Copyright (C) 1995-1997 Microsoft Corporation.  All Rights Reserved.
 *
 *  File:	d3drm.h
 *  Content:	Direct3DRM include file
 *
 ***************************************************************************/

#ifndef __D3DRM_H__
#define __D3DRM_H__

#include "ddraw.h"

#ifdef __cplusplus
struct IDirect3DRM;
#endif

typedef struct IDirect3DRM *LPDIRECT3DRM;

//#include "d3drmobj.h"
/*==========================================================================;
 *
 *  Copyright (C) 1995-1997 Microsoft Corporation.  All Rights Reserved.
 *
 *  File:	d3drm.h
 *  Content:	Direct3DRM include file
 *
 ***************************************************************************/

#ifndef _D3DRMOBJ_H_
#define _D3DRMOBJ_H_

#include <objbase.h> /* Use Windows header files */
#define VIRTUAL
//#include "d3drmdef.h"
/*==========================================================================;
 *
 *  Copyright (C) 1995-1997 Microsoft Corporation.  All Rights Reserved.
 *
 *  File:	d3drm.h
 *  Content:	Direct3DRM include file
 *
 ***************************************************************************/

#ifndef __D3DRMDEFS_H__
#define __D3DRMDEFS_H__

#include <stddef.h>
//#include "d3dtypes.h"
/*==========================================================================;
 *
 *  Copyright (C) 1995-1998 Microsoft Corporation.  All Rights Reserved.
 *
 *  File:   d3dtypes.h
 *  Content:    Direct3D types include file
 *
 ***************************************************************************/

#ifndef _D3DTYPES_H_
#define _D3DTYPES_H_

#ifndef DIRECT3D_VERSION
#define DIRECT3D_VERSION         0x0700
#endif

#if (DIRECT3D_VERSION >= 0x0800)
#pragma message("should not include d3dtypes.h when compiling for DX8 or newer interfaces")
#endif

#include <windows.h>

#include <float.h>
#include "ddraw.h"

#pragma warning(disable:4201) // anonymous unions warning
#pragma pack(4)


/* D3DVALUE is the fundamental Direct3D fractional data type */

#define D3DVALP(val, prec) ((float)(val))
#define D3DVAL(val) ((float)(val))

#ifndef DX_SHARED_DEFINES

/*
 * This definition is shared with other DirectX components whose header files
 * might already have defined it. Therefore, we don't define this type if
 * someone else already has (as indicated by the definition of
 * DX_SHARED_DEFINES). We don't set DX_SHARED_DEFINES here as there are
 * other types in this header that are also shared. The last of these
 * shared defines in this file will set DX_SHARED_DEFINES.
 */
typedef float D3DVALUE, *LPD3DVALUE;

#endif /* DX_SHARED_DEFINES */

#define D3DDivide(a, b)    (float)((double) (a) / (double) (b))
#define D3DMultiply(a, b)    ((a) * (b))

typedef LONG D3DFIXED;

#ifndef RGB_MAKE
/*
 * Format of CI colors is
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *  |    alpha      |         color index           |   fraction    |
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
#define CI_GETALPHA(ci)    ((ci) >> 24)
#define CI_GETINDEX(ci)    (((ci) >> 8) & 0xffff)
#define CI_GETFRACTION(ci) ((ci) & 0xff)
#define CI_ROUNDINDEX(ci)  CI_GETINDEX((ci) + 0x80)
#define CI_MASKALPHA(ci)   ((ci) & 0xffffff)
#define CI_MAKE(a, i, f)    (((a) << 24) | ((i) << 8) | (f))

/*
 * Format of RGBA colors is
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *  |    alpha      |      red      |     green     |     blue      |
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
#define RGBA_GETALPHA(rgb)      ((rgb) >> 24)
#define RGBA_GETRED(rgb)        (((rgb) >> 16) & 0xff)
#define RGBA_GETGREEN(rgb)      (((rgb) >> 8) & 0xff)
#define RGBA_GETBLUE(rgb)       ((rgb) & 0xff)
#define RGBA_MAKE(r, g, b, a)   ((D3DCOLOR) (((a) << 24) | ((r) << 16) | ((g) << 8) | (b)))

/* D3DRGB and D3DRGBA may be used as initialisers for D3DCOLORs
 * The float values must be in the range 0..1
 */
#define D3DRGB(r, g, b) \
    (0xff000000L | ( ((long)((r) * 255)) << 16) | (((long)((g) * 255)) << 8) | (long)((b) * 255))
#define D3DRGBA(r, g, b, a) \
    (   (((long)((a) * 255)) << 24) | (((long)((r) * 255)) << 16) \
    |   (((long)((g) * 255)) << 8) | (long)((b) * 255) \
    )

/*
 * Format of RGB colors is
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *  |    ignored    |      red      |     green     |     blue      |
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
#define RGB_GETRED(rgb)         (((rgb) >> 16) & 0xff)
#define RGB_GETGREEN(rgb)       (((rgb) >> 8) & 0xff)
#define RGB_GETBLUE(rgb)        ((rgb) & 0xff)
#define RGBA_SETALPHA(rgba, x) (((x) << 24) | ((rgba) & 0x00ffffff))
#define RGB_MAKE(r, g, b)       ((D3DCOLOR) (((r) << 16) | ((g) << 8) | (b)))
#define RGBA_TORGB(rgba)       ((D3DCOLOR) ((rgba) & 0xffffff))
#define RGB_TORGBA(rgb)        ((D3DCOLOR) ((rgb) | 0xff000000))

#endif

/*
 * Flags for Enumerate functions
 */

/*
 * Stop the enumeration
 */
#define D3DENUMRET_CANCEL                        DDENUMRET_CANCEL

/*
 * Continue the enumeration
 */
#define D3DENUMRET_OK                            DDENUMRET_OK

typedef HRESULT (CALLBACK* LPD3DVALIDATECALLBACK)(LPVOID lpUserArg, DWORD dwOffset);
typedef HRESULT (CALLBACK* LPD3DENUMTEXTUREFORMATSCALLBACK)(LPDDSURFACEDESC lpDdsd, LPVOID lpContext);
typedef HRESULT (CALLBACK* LPD3DENUMPIXELFORMATSCALLBACK)(LPDDPIXELFORMAT lpDDPixFmt, LPVOID lpContext);

#ifndef DX_SHARED_DEFINES

/*
 * This definition is shared with other DirectX components whose header files
 * might already have defined it. Therefore, we don't define this type if
 * someone else already has (as indicated by the definition of
 * DX_SHARED_DEFINES). We don't set DX_SHARED_DEFINES here as there are
 * other types in this header that are also shared. The last of these
 * shared defines in this file will set DX_SHARED_DEFINES.
 */
#ifndef D3DCOLOR_DEFINED
typedef DWORD D3DCOLOR;
#define D3DCOLOR_DEFINED
#endif
typedef DWORD *LPD3DCOLOR;

#endif /* DX_SHARED_DEFINES */

typedef DWORD D3DMATERIALHANDLE, *LPD3DMATERIALHANDLE;
typedef DWORD D3DTEXTUREHANDLE, *LPD3DTEXTUREHANDLE;
typedef DWORD D3DMATRIXHANDLE, *LPD3DMATRIXHANDLE;

#ifndef D3DCOLORVALUE_DEFINED
typedef struct _D3DCOLORVALUE {
    union {
    D3DVALUE r;
    D3DVALUE dvR;
    };
    union {
    D3DVALUE g;
    D3DVALUE dvG;
    };
    union {
    D3DVALUE b;
    D3DVALUE dvB;
    };
    union {
    D3DVALUE a;
    D3DVALUE dvA;
    };
} D3DCOLORVALUE;
#define D3DCOLORVALUE_DEFINED
#endif
typedef struct _D3DCOLORVALUE *LPD3DCOLORVALUE;

#ifndef D3DRECT_DEFINED
typedef struct _D3DRECT {
    union {
    LONG x1;
    LONG lX1;
    };
    union {
    LONG y1;
    LONG lY1;
    };
    union {
    LONG x2;
    LONG lX2;
    };
    union {
    LONG y2;
    LONG lY2;
    };
} D3DRECT;
#define D3DRECT_DEFINED
#endif
typedef struct _D3DRECT *LPD3DRECT;

#ifndef DX_SHARED_DEFINES

/*
 * This definition is shared with other DirectX components whose header files
 * might already have defined it. Therefore, we don't define this type if
 * someone else already has (as indicated by the definition of
 * DX_SHARED_DEFINES).
 */

#ifndef D3DVECTOR_DEFINED
typedef struct _D3DVECTOR {
    union {
    D3DVALUE x;
    D3DVALUE dvX;
    };
    union {
    D3DVALUE y;
    D3DVALUE dvY;
    };
    union {
    D3DVALUE z;
    D3DVALUE dvZ;
    };
#if(DIRECT3D_VERSION >= 0x0500)
#if (defined __cplusplus) && (defined D3D_OVERLOADS)

public:

    // =====================================
    // Constructors
    // =====================================

    _D3DVECTOR() { }
    _D3DVECTOR(D3DVALUE f);
    _D3DVECTOR(D3DVALUE _x, D3DVALUE _y, D3DVALUE _z);
    _D3DVECTOR(const D3DVALUE f[3]);

    // =====================================
    // Access grants
    // =====================================

    const D3DVALUE&operator[](int i) const;
    D3DVALUE&operator[](int i);

    // =====================================
    // Assignment operators
    // =====================================

    _D3DVECTOR& operator += (const _D3DVECTOR& v);
    _D3DVECTOR& operator -= (const _D3DVECTOR& v);
    _D3DVECTOR& operator *= (const _D3DVECTOR& v);
    _D3DVECTOR& operator /= (const _D3DVECTOR& v);
    _D3DVECTOR& operator *= (D3DVALUE s);
    _D3DVECTOR& operator /= (D3DVALUE s);

    // =====================================
    // Unary operators
    // =====================================

    friend _D3DVECTOR operator + (const _D3DVECTOR& v);
    friend _D3DVECTOR operator - (const _D3DVECTOR& v);


    // =====================================
    // Binary operators
    // =====================================

    // Addition and subtraction
        friend _D3DVECTOR operator + (const _D3DVECTOR& v1, const _D3DVECTOR& v2);
        friend _D3DVECTOR operator - (const _D3DVECTOR& v1, const _D3DVECTOR& v2);
    // Scalar multiplication and division
        friend _D3DVECTOR operator * (const _D3DVECTOR& v, D3DVALUE s);
        friend _D3DVECTOR operator * (D3DVALUE s, const _D3DVECTOR& v);
        friend _D3DVECTOR operator / (const _D3DVECTOR& v, D3DVALUE s);
    // Memberwise multiplication and division
        friend _D3DVECTOR operator * (const _D3DVECTOR& v1, const _D3DVECTOR& v2);
        friend _D3DVECTOR operator / (const _D3DVECTOR& v1, const _D3DVECTOR& v2);

    // Vector dominance
        friend int operator < (const _D3DVECTOR& v1, const _D3DVECTOR& v2);
        friend int operator <= (const _D3DVECTOR& v1, const _D3DVECTOR& v2);

    // Bitwise equality
        friend int operator == (const _D3DVECTOR& v1, const _D3DVECTOR& v2);

    // Length-related functions
        friend D3DVALUE SquareMagnitude (const _D3DVECTOR& v);
        friend D3DVALUE Magnitude (const _D3DVECTOR& v);

    // Returns vector with same direction and unit length
        friend _D3DVECTOR Normalize (const _D3DVECTOR& v);

    // Return min/max component of the input vector
        friend D3DVALUE Min (const _D3DVECTOR& v);
        friend D3DVALUE Max (const _D3DVECTOR& v);

    // Return memberwise min/max of input vectors
        friend _D3DVECTOR Minimize (const _D3DVECTOR& v1, const _D3DVECTOR& v2);
        friend _D3DVECTOR Maximize (const _D3DVECTOR& v1, const _D3DVECTOR& v2);

    // Dot and cross product
        friend D3DVALUE DotProduct (const _D3DVECTOR& v1, const _D3DVECTOR& v2);
        friend _D3DVECTOR CrossProduct (const _D3DVECTOR& v1, const _D3DVECTOR& v2);

#endif
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DVECTOR;
#define D3DVECTOR_DEFINED
#endif
typedef struct _D3DVECTOR *LPD3DVECTOR;

/*
 * As this is the last of the shared defines to be defined we now set
 * D3D_SHARED_DEFINES to flag that fact that this header has defined these
 * types.
 */
#define DX_SHARED_DEFINES

#endif /* DX_SHARED_DEFINES */

/*
 * Vertex data types supported in an ExecuteBuffer.
 */

/*
 * Homogeneous vertices
 */

typedef struct _D3DHVERTEX {
    DWORD           dwFlags;        /* Homogeneous clipping flags */
    union {
    D3DVALUE    hx;
    D3DVALUE    dvHX;
    };
    union {
    D3DVALUE    hy;
    D3DVALUE    dvHY;
    };
    union {
    D3DVALUE    hz;
    D3DVALUE    dvHZ;
    };
} D3DHVERTEX, *LPD3DHVERTEX;

/*
 * Transformed/lit vertices
 */
typedef struct _D3DTLVERTEX {
    union {
    D3DVALUE    sx;             /* Screen coordinates */
    D3DVALUE    dvSX;
    };
    union {
    D3DVALUE    sy;
    D3DVALUE    dvSY;
    };
    union {
    D3DVALUE    sz;
    D3DVALUE    dvSZ;
    };
    union {
    D3DVALUE    rhw;        /* Reciprocal of homogeneous w */
    D3DVALUE    dvRHW;
    };
    union {
    D3DCOLOR    color;          /* Vertex color */
    D3DCOLOR    dcColor;
    };
    union {
    D3DCOLOR    specular;       /* Specular component of vertex */
    D3DCOLOR    dcSpecular;
    };
    union {
    D3DVALUE    tu;             /* Texture coordinates */
    D3DVALUE    dvTU;
    };
    union {
    D3DVALUE    tv;
    D3DVALUE    dvTV;
    };
#if(DIRECT3D_VERSION >= 0x0500)
#if (defined __cplusplus) && (defined D3D_OVERLOADS)
    _D3DTLVERTEX() { }
    _D3DTLVERTEX(const D3DVECTOR& v, float _rhw,
                 D3DCOLOR _color, D3DCOLOR _specular,
                 float _tu, float _tv)
        { sx = v.x; sy = v.y; sz = v.z; rhw = _rhw;
          color = _color; specular = _specular;
          tu = _tu; tv = _tv;
        }
#endif
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DTLVERTEX, *LPD3DTLVERTEX;

/*
 * Untransformed/lit vertices
 */
typedef struct _D3DLVERTEX {
    union {
    D3DVALUE     x;             /* Homogeneous coordinates */
    D3DVALUE     dvX;
    };
    union {
    D3DVALUE     y;
    D3DVALUE     dvY;
    };
    union {
    D3DVALUE     z;
    D3DVALUE     dvZ;
    };
    DWORD            dwReserved;
    union {
    D3DCOLOR     color;         /* Vertex color */
    D3DCOLOR     dcColor;
    };
    union {
    D3DCOLOR     specular;      /* Specular component of vertex */
    D3DCOLOR     dcSpecular;
    };
    union {
    D3DVALUE     tu;            /* Texture coordinates */
    D3DVALUE     dvTU;
    };
    union {
    D3DVALUE     tv;
    D3DVALUE     dvTV;
    };
#if(DIRECT3D_VERSION >= 0x0500)
#if (defined __cplusplus) && (defined D3D_OVERLOADS)
    _D3DLVERTEX() { }
    _D3DLVERTEX(const D3DVECTOR& v,
                D3DCOLOR _color, D3DCOLOR _specular,
                float _tu, float _tv)
        { x = v.x; y = v.y; z = v.z; dwReserved = 0;
          color = _color; specular = _specular;
          tu = _tu; tv = _tv;
        }
#endif
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DLVERTEX, *LPD3DLVERTEX;

/*
 * Untransformed/unlit vertices
 */

typedef struct _D3DVERTEX {
    union {
    D3DVALUE     x;             /* Homogeneous coordinates */
    D3DVALUE     dvX;
    };
    union {
    D3DVALUE     y;
    D3DVALUE     dvY;
    };
    union {
    D3DVALUE     z;
    D3DVALUE     dvZ;
    };
    union {
    D3DVALUE     nx;            /* Normal */
    D3DVALUE     dvNX;
    };
    union {
    D3DVALUE     ny;
    D3DVALUE     dvNY;
    };
    union {
    D3DVALUE     nz;
    D3DVALUE     dvNZ;
    };
    union {
    D3DVALUE     tu;            /* Texture coordinates */
    D3DVALUE     dvTU;
    };
    union {
    D3DVALUE     tv;
    D3DVALUE     dvTV;
    };
#if(DIRECT3D_VERSION >= 0x0500)
#if (defined __cplusplus) && (defined D3D_OVERLOADS)
    _D3DVERTEX() { }
    _D3DVERTEX(const D3DVECTOR& v, const D3DVECTOR& n, float _tu, float _tv)
        { x = v.x; y = v.y; z = v.z;
          nx = n.x; ny = n.y; nz = n.z;
          tu = _tu; tv = _tv;
        }
#endif
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DVERTEX, *LPD3DVERTEX;


/*
 * Matrix, viewport, and tranformation structures and definitions.
 */

#ifndef D3DMATRIX_DEFINED
typedef struct _D3DMATRIX {
#if(DIRECT3D_VERSION >= 0x0500)
#if (defined __cplusplus) && (defined D3D_OVERLOADS)
    union {
        struct {
#endif

#endif /* DIRECT3D_VERSION >= 0x0500 */
            D3DVALUE        _11, _12, _13, _14;
            D3DVALUE        _21, _22, _23, _24;
            D3DVALUE        _31, _32, _33, _34;
            D3DVALUE        _41, _42, _43, _44;

#if(DIRECT3D_VERSION >= 0x0500)
#if (defined __cplusplus) && (defined D3D_OVERLOADS)
        };
        D3DVALUE m[4][4];
    };
    _D3DMATRIX() { }
    _D3DMATRIX( D3DVALUE _m00, D3DVALUE _m01, D3DVALUE _m02, D3DVALUE _m03,
                D3DVALUE _m10, D3DVALUE _m11, D3DVALUE _m12, D3DVALUE _m13,
                D3DVALUE _m20, D3DVALUE _m21, D3DVALUE _m22, D3DVALUE _m23,
                D3DVALUE _m30, D3DVALUE _m31, D3DVALUE _m32, D3DVALUE _m33
        )
        {
                m[0][0] = _m00; m[0][1] = _m01; m[0][2] = _m02; m[0][3] = _m03;
                m[1][0] = _m10; m[1][1] = _m11; m[1][2] = _m12; m[1][3] = _m13;
                m[2][0] = _m20; m[2][1] = _m21; m[2][2] = _m22; m[2][3] = _m23;
                m[3][0] = _m30; m[3][1] = _m31; m[3][2] = _m32; m[3][3] = _m33;
        }

    D3DVALUE& operator()(int iRow, int iColumn) { return m[iRow][iColumn]; }
    const D3DVALUE& operator()(int iRow, int iColumn) const { return m[iRow][iColumn]; }
#if(DIRECT3D_VERSION >= 0x0600)
    friend _D3DMATRIX operator* (const _D3DMATRIX&, const _D3DMATRIX&);
#endif /* DIRECT3D_VERSION >= 0x0600 */
#endif
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DMATRIX;
#define D3DMATRIX_DEFINED
#endif
typedef struct _D3DMATRIX *LPD3DMATRIX;

#if (defined __cplusplus) && (defined D3D_OVERLOADS)
#include "d3dvec.inl"
#endif

typedef struct _D3DVIEWPORT {
    DWORD       dwSize;
    DWORD       dwX;
    DWORD       dwY;        /* Top left */
    DWORD       dwWidth;
    DWORD       dwHeight;   /* Dimensions */
    D3DVALUE    dvScaleX;   /* Scale homogeneous to screen */
    D3DVALUE    dvScaleY;   /* Scale homogeneous to screen */
    D3DVALUE    dvMaxX;     /* Min/max homogeneous x coord */
    D3DVALUE    dvMaxY;     /* Min/max homogeneous y coord */
    D3DVALUE    dvMinZ;
    D3DVALUE    dvMaxZ;     /* Min/max homogeneous z coord */
} D3DVIEWPORT, *LPD3DVIEWPORT;

#if(DIRECT3D_VERSION >= 0x0500)
typedef struct _D3DVIEWPORT2 {
    DWORD       dwSize;
    DWORD       dwX;
    DWORD       dwY;        /* Viewport Top left */
    DWORD       dwWidth;
    DWORD       dwHeight;   /* Viewport Dimensions */
    D3DVALUE    dvClipX;        /* Top left of clip volume */
    D3DVALUE    dvClipY;
    D3DVALUE    dvClipWidth;    /* Clip Volume Dimensions */
    D3DVALUE    dvClipHeight;
    D3DVALUE    dvMinZ;         /* Min/max of clip Volume */
    D3DVALUE    dvMaxZ;
} D3DVIEWPORT2, *LPD3DVIEWPORT2;
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0700)
typedef struct _D3DVIEWPORT7 {
    DWORD       dwX;
    DWORD       dwY;            /* Viewport Top left */
    DWORD       dwWidth;
    DWORD       dwHeight;       /* Viewport Dimensions */
    D3DVALUE    dvMinZ;         /* Min/max of clip Volume */
    D3DVALUE    dvMaxZ;
} D3DVIEWPORT7, *LPD3DVIEWPORT7;
#endif /* DIRECT3D_VERSION >= 0x0700 */

/*
 * Values for clip fields.
 */

#if(DIRECT3D_VERSION >= 0x0700)

// Max number of user clipping planes, supported in D3D.
#define D3DMAXUSERCLIPPLANES 32

// These bits could be ORed together to use with D3DRENDERSTATE_CLIPPLANEENABLE
//
#define D3DCLIPPLANE0 (1 << 0)
#define D3DCLIPPLANE1 (1 << 1)
#define D3DCLIPPLANE2 (1 << 2)
#define D3DCLIPPLANE3 (1 << 3)
#define D3DCLIPPLANE4 (1 << 4)
#define D3DCLIPPLANE5 (1 << 5)

#endif /* DIRECT3D_VERSION >= 0x0700 */

#define D3DCLIP_LEFT                0x00000001L
#define D3DCLIP_RIGHT               0x00000002L
#define D3DCLIP_TOP             0x00000004L
#define D3DCLIP_BOTTOM              0x00000008L
#define D3DCLIP_FRONT               0x00000010L
#define D3DCLIP_BACK                0x00000020L
#define D3DCLIP_GEN0                0x00000040L
#define D3DCLIP_GEN1                0x00000080L
#define D3DCLIP_GEN2                0x00000100L
#define D3DCLIP_GEN3                0x00000200L
#define D3DCLIP_GEN4                0x00000400L
#define D3DCLIP_GEN5                0x00000800L

/*
 * Values for d3d status.
 */
#define D3DSTATUS_CLIPUNIONLEFT         D3DCLIP_LEFT
#define D3DSTATUS_CLIPUNIONRIGHT        D3DCLIP_RIGHT
#define D3DSTATUS_CLIPUNIONTOP          D3DCLIP_TOP
#define D3DSTATUS_CLIPUNIONBOTTOM       D3DCLIP_BOTTOM
#define D3DSTATUS_CLIPUNIONFRONT        D3DCLIP_FRONT
#define D3DSTATUS_CLIPUNIONBACK         D3DCLIP_BACK
#define D3DSTATUS_CLIPUNIONGEN0         D3DCLIP_GEN0
#define D3DSTATUS_CLIPUNIONGEN1         D3DCLIP_GEN1
#define D3DSTATUS_CLIPUNIONGEN2         D3DCLIP_GEN2
#define D3DSTATUS_CLIPUNIONGEN3         D3DCLIP_GEN3
#define D3DSTATUS_CLIPUNIONGEN4         D3DCLIP_GEN4
#define D3DSTATUS_CLIPUNIONGEN5         D3DCLIP_GEN5

#define D3DSTATUS_CLIPINTERSECTIONLEFT      0x00001000L
#define D3DSTATUS_CLIPINTERSECTIONRIGHT     0x00002000L
#define D3DSTATUS_CLIPINTERSECTIONTOP       0x00004000L
#define D3DSTATUS_CLIPINTERSECTIONBOTTOM    0x00008000L
#define D3DSTATUS_CLIPINTERSECTIONFRONT     0x00010000L
#define D3DSTATUS_CLIPINTERSECTIONBACK      0x00020000L
#define D3DSTATUS_CLIPINTERSECTIONGEN0      0x00040000L
#define D3DSTATUS_CLIPINTERSECTIONGEN1      0x00080000L
#define D3DSTATUS_CLIPINTERSECTIONGEN2      0x00100000L
#define D3DSTATUS_CLIPINTERSECTIONGEN3      0x00200000L
#define D3DSTATUS_CLIPINTERSECTIONGEN4      0x00400000L
#define D3DSTATUS_CLIPINTERSECTIONGEN5      0x00800000L
#define D3DSTATUS_ZNOTVISIBLE               0x01000000L
/* Do not use 0x80000000 for any status flags in future as it is reserved */

#define D3DSTATUS_CLIPUNIONALL  (       \
        D3DSTATUS_CLIPUNIONLEFT |   \
        D3DSTATUS_CLIPUNIONRIGHT    |   \
        D3DSTATUS_CLIPUNIONTOP  |   \
        D3DSTATUS_CLIPUNIONBOTTOM   |   \
        D3DSTATUS_CLIPUNIONFRONT    |   \
        D3DSTATUS_CLIPUNIONBACK |   \
        D3DSTATUS_CLIPUNIONGEN0 |   \
        D3DSTATUS_CLIPUNIONGEN1 |   \
        D3DSTATUS_CLIPUNIONGEN2 |   \
        D3DSTATUS_CLIPUNIONGEN3 |   \
        D3DSTATUS_CLIPUNIONGEN4 |   \
        D3DSTATUS_CLIPUNIONGEN5     \
        )

#define D3DSTATUS_CLIPINTERSECTIONALL   (       \
        D3DSTATUS_CLIPINTERSECTIONLEFT  |   \
        D3DSTATUS_CLIPINTERSECTIONRIGHT |   \
        D3DSTATUS_CLIPINTERSECTIONTOP   |   \
        D3DSTATUS_CLIPINTERSECTIONBOTTOM    |   \
        D3DSTATUS_CLIPINTERSECTIONFRONT |   \
        D3DSTATUS_CLIPINTERSECTIONBACK  |   \
        D3DSTATUS_CLIPINTERSECTIONGEN0  |   \
        D3DSTATUS_CLIPINTERSECTIONGEN1  |   \
        D3DSTATUS_CLIPINTERSECTIONGEN2  |   \
        D3DSTATUS_CLIPINTERSECTIONGEN3  |   \
        D3DSTATUS_CLIPINTERSECTIONGEN4  |   \
        D3DSTATUS_CLIPINTERSECTIONGEN5      \
        )

#define D3DSTATUS_DEFAULT   (           \
        D3DSTATUS_CLIPINTERSECTIONALL   |   \
        D3DSTATUS_ZNOTVISIBLE)


/*
 * Options for direct transform calls
 */
#define D3DTRANSFORM_CLIPPED       0x00000001l
#define D3DTRANSFORM_UNCLIPPED     0x00000002l

typedef struct _D3DTRANSFORMDATA {
    DWORD           dwSize;
    LPVOID      lpIn;           /* Input vertices */
    DWORD           dwInSize;       /* Stride of input vertices */
    LPVOID      lpOut;          /* Output vertices */
    DWORD           dwOutSize;      /* Stride of output vertices */
    LPD3DHVERTEX    lpHOut;         /* Output homogeneous vertices */
    DWORD           dwClip;         /* Clipping hint */
    DWORD           dwClipIntersection;
    DWORD           dwClipUnion;    /* Union of all clip flags */
    D3DRECT         drExtent;       /* Extent of transformed vertices */
} D3DTRANSFORMDATA, *LPD3DTRANSFORMDATA;

/*
 * Structure defining position and direction properties for lighting.
 */
typedef struct _D3DLIGHTINGELEMENT {
    D3DVECTOR dvPosition;           /* Lightable point in model space */
    D3DVECTOR dvNormal;             /* Normalised unit vector */
} D3DLIGHTINGELEMENT, *LPD3DLIGHTINGELEMENT;

/*
 * Structure defining material properties for lighting.
 */
typedef struct _D3DMATERIAL {
    DWORD           dwSize;
    union {
    D3DCOLORVALUE   diffuse;        /* Diffuse color RGBA */
    D3DCOLORVALUE   dcvDiffuse;
    };
    union {
    D3DCOLORVALUE   ambient;        /* Ambient color RGB */
    D3DCOLORVALUE   dcvAmbient;
    };
    union {
    D3DCOLORVALUE   specular;       /* Specular 'shininess' */
    D3DCOLORVALUE   dcvSpecular;
    };
    union {
    D3DCOLORVALUE   emissive;       /* Emissive color RGB */
    D3DCOLORVALUE   dcvEmissive;
    };
    union {
    D3DVALUE        power;          /* Sharpness if specular highlight */
    D3DVALUE        dvPower;
    };
    D3DTEXTUREHANDLE    hTexture;       /* Handle to texture map */
    DWORD           dwRampSize;
} D3DMATERIAL, *LPD3DMATERIAL;

#if(DIRECT3D_VERSION >= 0x0700)

typedef struct _D3DMATERIAL7 {
    union {
    D3DCOLORVALUE   diffuse;        /* Diffuse color RGBA */
    D3DCOLORVALUE   dcvDiffuse;
    };
    union {
    D3DCOLORVALUE   ambient;        /* Ambient color RGB */
    D3DCOLORVALUE   dcvAmbient;
    };
    union {
    D3DCOLORVALUE   specular;       /* Specular 'shininess' */
    D3DCOLORVALUE   dcvSpecular;
    };
    union {
    D3DCOLORVALUE   emissive;       /* Emissive color RGB */
    D3DCOLORVALUE   dcvEmissive;
    };
    union {
    D3DVALUE        power;          /* Sharpness if specular highlight */
    D3DVALUE        dvPower;
    };
} D3DMATERIAL7, *LPD3DMATERIAL7;

#endif /* DIRECT3D_VERSION >= 0x0700 */

#if(DIRECT3D_VERSION < 0x0800)

typedef enum _D3DLIGHTTYPE {
    D3DLIGHT_POINT          = 1,
    D3DLIGHT_SPOT           = 2,
    D3DLIGHT_DIRECTIONAL    = 3,
// Note: The following light type (D3DLIGHT_PARALLELPOINT)
// is no longer supported from D3D for DX7 onwards.
    D3DLIGHT_PARALLELPOINT  = 4,
#if(DIRECT3D_VERSION < 0x0500) // For backward compatible headers
    D3DLIGHT_GLSPOT         = 5,
#endif
    D3DLIGHT_FORCE_DWORD    = 0x7fffffff, /* force 32-bit size enum */
} D3DLIGHTTYPE;

#else
typedef enum _D3DLIGHTTYPE D3DLIGHTTYPE;
#define D3DLIGHT_PARALLELPOINT  (D3DLIGHTTYPE)4
#define D3DLIGHT_GLSPOT         (D3DLIGHTTYPE)5

#endif //(DIRECT3D_VERSION < 0x0800)

/*
 * Structure defining a light source and its properties.
 */
typedef struct _D3DLIGHT {
    DWORD           dwSize;
    D3DLIGHTTYPE    dltType;            /* Type of light source */
    D3DCOLORVALUE   dcvColor;           /* Color of light */
    D3DVECTOR       dvPosition;         /* Position in world space */
    D3DVECTOR       dvDirection;        /* Direction in world space */
    D3DVALUE        dvRange;            /* Cutoff range */
    D3DVALUE        dvFalloff;          /* Falloff */
    D3DVALUE        dvAttenuation0;     /* Constant attenuation */
    D3DVALUE        dvAttenuation1;     /* Linear attenuation */
    D3DVALUE        dvAttenuation2;     /* Quadratic attenuation */
    D3DVALUE        dvTheta;            /* Inner angle of spotlight cone */
    D3DVALUE        dvPhi;              /* Outer angle of spotlight cone */
} D3DLIGHT, *LPD3DLIGHT;

#if(DIRECT3D_VERSION >= 0x0700)

typedef struct _D3DLIGHT7 {
    D3DLIGHTTYPE    dltType;            /* Type of light source */
    D3DCOLORVALUE   dcvDiffuse;         /* Diffuse color of light */
    D3DCOLORVALUE   dcvSpecular;        /* Specular color of light */
    D3DCOLORVALUE   dcvAmbient;         /* Ambient color of light */
    D3DVECTOR       dvPosition;         /* Position in world space */
    D3DVECTOR       dvDirection;        /* Direction in world space */
    D3DVALUE        dvRange;            /* Cutoff range */
    D3DVALUE        dvFalloff;          /* Falloff */
    D3DVALUE        dvAttenuation0;     /* Constant attenuation */
    D3DVALUE        dvAttenuation1;     /* Linear attenuation */
    D3DVALUE        dvAttenuation2;     /* Quadratic attenuation */
    D3DVALUE        dvTheta;            /* Inner angle of spotlight cone */
    D3DVALUE        dvPhi;              /* Outer angle of spotlight cone */
} D3DLIGHT7, *LPD3DLIGHT7;

#endif /* DIRECT3D_VERSION >= 0x0700 */

#if(DIRECT3D_VERSION >= 0x0500)
/*
 * Structure defining a light source and its properties.
 */

/* flags bits */
#define D3DLIGHT_ACTIVE         0x00000001
#define D3DLIGHT_NO_SPECULAR    0x00000002
#define D3DLIGHT_ALL (D3DLIGHT_ACTIVE | D3DLIGHT_NO_SPECULAR)

/* maximum valid light range */
#define D3DLIGHT_RANGE_MAX      ((float)sqrt(FLT_MAX))

typedef struct _D3DLIGHT2 {
    DWORD           dwSize;
    D3DLIGHTTYPE    dltType;        /* Type of light source */
    D3DCOLORVALUE   dcvColor;       /* Color of light */
    D3DVECTOR       dvPosition;     /* Position in world space */
    D3DVECTOR       dvDirection;    /* Direction in world space */
    D3DVALUE        dvRange;        /* Cutoff range */
    D3DVALUE        dvFalloff;      /* Falloff */
    D3DVALUE        dvAttenuation0; /* Constant attenuation */
    D3DVALUE        dvAttenuation1; /* Linear attenuation */
    D3DVALUE        dvAttenuation2; /* Quadratic attenuation */
    D3DVALUE        dvTheta;        /* Inner angle of spotlight cone */
    D3DVALUE        dvPhi;          /* Outer angle of spotlight cone */
    DWORD           dwFlags;
} D3DLIGHT2, *LPD3DLIGHT2;

#endif /* DIRECT3D_VERSION >= 0x0500 */
typedef struct _D3DLIGHTDATA {
    DWORD                dwSize;
    LPD3DLIGHTINGELEMENT lpIn;      /* Input positions and normals */
    DWORD                dwInSize;  /* Stride of input elements */
    LPD3DTLVERTEX        lpOut;     /* Output colors */
    DWORD                dwOutSize; /* Stride of output colors */
} D3DLIGHTDATA, *LPD3DLIGHTDATA;

#if(DIRECT3D_VERSION >= 0x0500)
/*
 * Before DX5, these values were in an enum called
 * D3DCOLORMODEL. This was not correct, since they are
 * bit flags. A driver can surface either or both flags
 * in the dcmColorModel member of D3DDEVICEDESC.
 */
#define D3DCOLOR_MONO   1
#define D3DCOLOR_RGB    2

typedef DWORD D3DCOLORMODEL;
#endif /* DIRECT3D_VERSION >= 0x0500 */

/*
 * Options for clearing
 */
#define D3DCLEAR_TARGET            0x00000001l  /* Clear target surface */
#define D3DCLEAR_ZBUFFER           0x00000002l  /* Clear target z buffer */
#if(DIRECT3D_VERSION >= 0x0600)
#define D3DCLEAR_STENCIL           0x00000004l  /* Clear stencil planes */
#endif /* DIRECT3D_VERSION >= 0x0600 */

/*
 * Execute buffers are allocated via Direct3D.  These buffers may then
 * be filled by the application with instructions to execute along with
 * vertex data.
 */

/*
 * Supported op codes for execute instructions.
 */
typedef enum _D3DOPCODE {
    D3DOP_POINT                 = 1,
    D3DOP_LINE                  = 2,
    D3DOP_TRIANGLE      = 3,
    D3DOP_MATRIXLOAD        = 4,
    D3DOP_MATRIXMULTIPLY    = 5,
    D3DOP_STATETRANSFORM        = 6,
    D3DOP_STATELIGHT        = 7,
    D3DOP_STATERENDER       = 8,
    D3DOP_PROCESSVERTICES       = 9,
    D3DOP_TEXTURELOAD       = 10,
    D3DOP_EXIT                  = 11,
    D3DOP_BRANCHFORWARD     = 12,
    D3DOP_SPAN          = 13,
    D3DOP_SETSTATUS     = 14,
#if(DIRECT3D_VERSION >= 0x0500)
    D3DOP_FORCE_DWORD           = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DOPCODE;

typedef struct _D3DINSTRUCTION {
    BYTE bOpcode;   /* Instruction opcode */
    BYTE bSize;     /* Size of each instruction data unit */
    WORD wCount;    /* Count of instruction data units to follow */
} D3DINSTRUCTION, *LPD3DINSTRUCTION;

/*
 * Structure for texture loads
 */
typedef struct _D3DTEXTURELOAD {
    D3DTEXTUREHANDLE hDestTexture;
    D3DTEXTUREHANDLE hSrcTexture;
} D3DTEXTURELOAD, *LPD3DTEXTURELOAD;

/*
 * Structure for picking
 */
typedef struct _D3DPICKRECORD {
    BYTE     bOpcode;
    BYTE     bPad;
    DWORD    dwOffset;
    D3DVALUE dvZ;
} D3DPICKRECORD, *LPD3DPICKRECORD;

/*
 * The following defines the rendering states which can be set in the
 * execute buffer.
 */

#if(DIRECT3D_VERSION < 0x0800)

typedef enum _D3DSHADEMODE {
    D3DSHADE_FLAT              = 1,
    D3DSHADE_GOURAUD           = 2,
    D3DSHADE_PHONG             = 3,
#if(DIRECT3D_VERSION >= 0x0500)
    D3DSHADE_FORCE_DWORD       = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DSHADEMODE;

typedef enum _D3DFILLMODE {
    D3DFILL_POINT          = 1,
    D3DFILL_WIREFRAME          = 2,
    D3DFILL_SOLID          = 3,
#if(DIRECT3D_VERSION >= 0x0500)
    D3DFILL_FORCE_DWORD        = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DFILLMODE;

typedef struct _D3DLINEPATTERN {
    WORD    wRepeatFactor;
    WORD    wLinePattern;
} D3DLINEPATTERN;

#endif //(DIRECT3D_VERSION < 0x0800)

typedef enum _D3DTEXTUREFILTER {
    D3DFILTER_NEAREST          = 1,
    D3DFILTER_LINEAR           = 2,
    D3DFILTER_MIPNEAREST       = 3,
    D3DFILTER_MIPLINEAR        = 4,
    D3DFILTER_LINEARMIPNEAREST = 5,
    D3DFILTER_LINEARMIPLINEAR  = 6,
#if(DIRECT3D_VERSION >= 0x0500)
    D3DFILTER_FORCE_DWORD      = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DTEXTUREFILTER;

#if(DIRECT3D_VERSION < 0x0800)

typedef enum _D3DBLEND {
    D3DBLEND_ZERO              = 1,
    D3DBLEND_ONE               = 2,
    D3DBLEND_SRCCOLOR          = 3,
    D3DBLEND_INVSRCCOLOR       = 4,
    D3DBLEND_SRCALPHA          = 5,
    D3DBLEND_INVSRCALPHA       = 6,
    D3DBLEND_DESTALPHA         = 7,
    D3DBLEND_INVDESTALPHA      = 8,
    D3DBLEND_DESTCOLOR         = 9,
    D3DBLEND_INVDESTCOLOR      = 10,
    D3DBLEND_SRCALPHASAT       = 11,
    D3DBLEND_BOTHSRCALPHA      = 12,
    D3DBLEND_BOTHINVSRCALPHA   = 13,
#if(DIRECT3D_VERSION >= 0x0500)
    D3DBLEND_FORCE_DWORD       = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DBLEND;

#endif //(DIRECT3D_VERSION < 0x0800)

typedef enum _D3DTEXTUREBLEND {
    D3DTBLEND_DECAL            = 1,
    D3DTBLEND_MODULATE         = 2,
    D3DTBLEND_DECALALPHA       = 3,
    D3DTBLEND_MODULATEALPHA    = 4,
    D3DTBLEND_DECALMASK        = 5,
    D3DTBLEND_MODULATEMASK     = 6,
    D3DTBLEND_COPY             = 7,
#if(DIRECT3D_VERSION >= 0x0500)
    D3DTBLEND_ADD              = 8,
    D3DTBLEND_FORCE_DWORD      = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DTEXTUREBLEND;

#if(DIRECT3D_VERSION < 0x0800)

typedef enum _D3DTEXTUREADDRESS {
    D3DTADDRESS_WRAP           = 1,
    D3DTADDRESS_MIRROR         = 2,
    D3DTADDRESS_CLAMP          = 3,
#if(DIRECT3D_VERSION >= 0x0500)
    D3DTADDRESS_BORDER         = 4,
    D3DTADDRESS_FORCE_DWORD    = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DTEXTUREADDRESS;

typedef enum _D3DCULL {
    D3DCULL_NONE               = 1,
    D3DCULL_CW                 = 2,
    D3DCULL_CCW                = 3,
#if(DIRECT3D_VERSION >= 0x0500)
    D3DCULL_FORCE_DWORD        = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DCULL;

typedef enum _D3DCMPFUNC {
    D3DCMP_NEVER               = 1,
    D3DCMP_LESS                = 2,
    D3DCMP_EQUAL               = 3,
    D3DCMP_LESSEQUAL           = 4,
    D3DCMP_GREATER             = 5,
    D3DCMP_NOTEQUAL            = 6,
    D3DCMP_GREATEREQUAL        = 7,
    D3DCMP_ALWAYS              = 8,
#if(DIRECT3D_VERSION >= 0x0500)
    D3DCMP_FORCE_DWORD         = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DCMPFUNC;

#if(DIRECT3D_VERSION >= 0x0600)
typedef enum _D3DSTENCILOP {
    D3DSTENCILOP_KEEP           = 1,
    D3DSTENCILOP_ZERO           = 2,
    D3DSTENCILOP_REPLACE        = 3,
    D3DSTENCILOP_INCRSAT        = 4,
    D3DSTENCILOP_DECRSAT        = 5,
    D3DSTENCILOP_INVERT         = 6,
    D3DSTENCILOP_INCR           = 7,
    D3DSTENCILOP_DECR           = 8,
    D3DSTENCILOP_FORCE_DWORD    = 0x7fffffff, /* force 32-bit size enum */
} D3DSTENCILOP;
#endif /* DIRECT3D_VERSION >= 0x0600 */

typedef enum _D3DFOGMODE {
    D3DFOG_NONE                = 0,
    D3DFOG_EXP                 = 1,
    D3DFOG_EXP2                = 2,
#if(DIRECT3D_VERSION >= 0x0500)
    D3DFOG_LINEAR              = 3,
    D3DFOG_FORCE_DWORD         = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DFOGMODE;

#if(DIRECT3D_VERSION >= 0x0600)
typedef enum _D3DZBUFFERTYPE {
    D3DZB_FALSE                 = 0,
    D3DZB_TRUE                  = 1, // Z buffering
    D3DZB_USEW                  = 2, // W buffering
    D3DZB_FORCE_DWORD           = 0x7fffffff, /* force 32-bit size enum */
} D3DZBUFFERTYPE;
#endif /* DIRECT3D_VERSION >= 0x0600 */

#endif //(DIRECT3D_VERSION < 0x0800)

#if(DIRECT3D_VERSION >= 0x0500)
typedef enum _D3DANTIALIASMODE {
    D3DANTIALIAS_NONE          = 0,
    D3DANTIALIAS_SORTDEPENDENT = 1,
    D3DANTIALIAS_SORTINDEPENDENT = 2,
    D3DANTIALIAS_FORCE_DWORD   = 0x7fffffff, /* force 32-bit size enum */
} D3DANTIALIASMODE;

// Vertex types supported by Direct3D
typedef enum _D3DVERTEXTYPE {
    D3DVT_VERTEX        = 1,
    D3DVT_LVERTEX       = 2,
    D3DVT_TLVERTEX      = 3,
    D3DVT_FORCE_DWORD   = 0x7fffffff, /* force 32-bit size enum */
} D3DVERTEXTYPE;

#if(DIRECT3D_VERSION < 0x0800)

// Primitives supported by draw-primitive API
typedef enum _D3DPRIMITIVETYPE {
    D3DPT_POINTLIST     = 1,
    D3DPT_LINELIST      = 2,
    D3DPT_LINESTRIP     = 3,
    D3DPT_TRIANGLELIST  = 4,
    D3DPT_TRIANGLESTRIP = 5,
    D3DPT_TRIANGLEFAN   = 6,
    D3DPT_FORCE_DWORD   = 0x7fffffff, /* force 32-bit size enum */
} D3DPRIMITIVETYPE;

#endif //(DIRECT3D_VERSION < 0x0800)

#endif /* DIRECT3D_VERSION >= 0x0500 */
/*
 * Amount to add to a state to generate the override for that state.
 */
#define D3DSTATE_OVERRIDE_BIAS      256

/*
 * A state which sets the override flag for the specified state type.
 */
#define D3DSTATE_OVERRIDE(type) (D3DRENDERSTATETYPE)(((DWORD) (type) + D3DSTATE_OVERRIDE_BIAS))

#if(DIRECT3D_VERSION < 0x0800)

typedef enum _D3DTRANSFORMSTATETYPE {
    D3DTRANSFORMSTATE_WORLD         = 1,
    D3DTRANSFORMSTATE_VIEW          = 2,
    D3DTRANSFORMSTATE_PROJECTION    = 3,
#if(DIRECT3D_VERSION >= 0x0700)
    D3DTRANSFORMSTATE_WORLD1        = 4,  // 2nd matrix to blend
    D3DTRANSFORMSTATE_WORLD2        = 5,  // 3rd matrix to blend
    D3DTRANSFORMSTATE_WORLD3        = 6,  // 4th matrix to blend
    D3DTRANSFORMSTATE_TEXTURE0      = 16,
    D3DTRANSFORMSTATE_TEXTURE1      = 17,
    D3DTRANSFORMSTATE_TEXTURE2      = 18,
    D3DTRANSFORMSTATE_TEXTURE3      = 19,
    D3DTRANSFORMSTATE_TEXTURE4      = 20,
    D3DTRANSFORMSTATE_TEXTURE5      = 21,
    D3DTRANSFORMSTATE_TEXTURE6      = 22,
    D3DTRANSFORMSTATE_TEXTURE7      = 23,
#endif /* DIRECT3D_VERSION >= 0x0700 */
#if(DIRECT3D_VERSION >= 0x0500)
    D3DTRANSFORMSTATE_FORCE_DWORD     = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DTRANSFORMSTATETYPE;

#else

//
// legacy transform state names
//
typedef enum _D3DTRANSFORMSTATETYPE D3DTRANSFORMSTATETYPE;
#define D3DTRANSFORMSTATE_WORLD         (D3DTRANSFORMSTATETYPE)1
#define D3DTRANSFORMSTATE_VIEW          (D3DTRANSFORMSTATETYPE)2
#define D3DTRANSFORMSTATE_PROJECTION    (D3DTRANSFORMSTATETYPE)3
#define D3DTRANSFORMSTATE_WORLD1        (D3DTRANSFORMSTATETYPE)4
#define D3DTRANSFORMSTATE_WORLD2        (D3DTRANSFORMSTATETYPE)5
#define D3DTRANSFORMSTATE_WORLD3        (D3DTRANSFORMSTATETYPE)6
#define D3DTRANSFORMSTATE_TEXTURE0      (D3DTRANSFORMSTATETYPE)16
#define D3DTRANSFORMSTATE_TEXTURE1      (D3DTRANSFORMSTATETYPE)17
#define D3DTRANSFORMSTATE_TEXTURE2      (D3DTRANSFORMSTATETYPE)18
#define D3DTRANSFORMSTATE_TEXTURE3      (D3DTRANSFORMSTATETYPE)19
#define D3DTRANSFORMSTATE_TEXTURE4      (D3DTRANSFORMSTATETYPE)20
#define D3DTRANSFORMSTATE_TEXTURE5      (D3DTRANSFORMSTATETYPE)21
#define D3DTRANSFORMSTATE_TEXTURE6      (D3DTRANSFORMSTATETYPE)22
#define D3DTRANSFORMSTATE_TEXTURE7      (D3DTRANSFORMSTATETYPE)23

#endif //(DIRECT3D_VERSION < 0x0800)

typedef enum _D3DLIGHTSTATETYPE {
    D3DLIGHTSTATE_MATERIAL          = 1,
    D3DLIGHTSTATE_AMBIENT           = 2,
    D3DLIGHTSTATE_COLORMODEL        = 3,
    D3DLIGHTSTATE_FOGMODE           = 4,
    D3DLIGHTSTATE_FOGSTART          = 5,
    D3DLIGHTSTATE_FOGEND            = 6,
    D3DLIGHTSTATE_FOGDENSITY        = 7,
#if(DIRECT3D_VERSION >= 0x0600)
    D3DLIGHTSTATE_COLORVERTEX       = 8,
#endif /* DIRECT3D_VERSION >= 0x0600 */
#if(DIRECT3D_VERSION >= 0x0500)
    D3DLIGHTSTATE_FORCE_DWORD         = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DLIGHTSTATETYPE;

#if(DIRECT3D_VERSION < 0x0800)

typedef enum _D3DRENDERSTATETYPE {
    D3DRENDERSTATE_ANTIALIAS          = 2,    /* D3DANTIALIASMODE */
    D3DRENDERSTATE_TEXTUREPERSPECTIVE = 4,    /* TRUE for perspective correction */
    D3DRENDERSTATE_ZENABLE            = 7,    /* D3DZBUFFERTYPE (or TRUE/FALSE for legacy) */
    D3DRENDERSTATE_FILLMODE           = 8,    /* D3DFILL_MODE        */
    D3DRENDERSTATE_SHADEMODE          = 9,    /* D3DSHADEMODE */
    D3DRENDERSTATE_LINEPATTERN        = 10,   /* D3DLINEPATTERN */
    D3DRENDERSTATE_ZWRITEENABLE       = 14,   /* TRUE to enable z writes */
    D3DRENDERSTATE_ALPHATESTENABLE    = 15,   /* TRUE to enable alpha tests */
    D3DRENDERSTATE_LASTPIXEL          = 16,   /* TRUE for last-pixel on lines */
    D3DRENDERSTATE_SRCBLEND           = 19,   /* D3DBLEND */
    D3DRENDERSTATE_DESTBLEND          = 20,   /* D3DBLEND */
    D3DRENDERSTATE_CULLMODE           = 22,   /* D3DCULL */
    D3DRENDERSTATE_ZFUNC              = 23,   /* D3DCMPFUNC */
    D3DRENDERSTATE_ALPHAREF           = 24,   /* D3DFIXED */
    D3DRENDERSTATE_ALPHAFUNC          = 25,   /* D3DCMPFUNC */
    D3DRENDERSTATE_DITHERENABLE       = 26,   /* TRUE to enable dithering */
#if(DIRECT3D_VERSION >= 0x0500)
    D3DRENDERSTATE_ALPHABLENDENABLE   = 27,   /* TRUE to enable alpha blending */
#endif /* DIRECT3D_VERSION >= 0x0500 */
    D3DRENDERSTATE_FOGENABLE          = 28,   /* TRUE to enable fog blending */
    D3DRENDERSTATE_SPECULARENABLE     = 29,   /* TRUE to enable specular */
    D3DRENDERSTATE_ZVISIBLE           = 30,   /* TRUE to enable z checking */
    D3DRENDERSTATE_STIPPLEDALPHA      = 33,   /* TRUE to enable stippled alpha (RGB device only) */
    D3DRENDERSTATE_FOGCOLOR           = 34,   /* D3DCOLOR */
    D3DRENDERSTATE_FOGTABLEMODE       = 35,   /* D3DFOGMODE */
#if(DIRECT3D_VERSION >= 0x0700)
    D3DRENDERSTATE_FOGSTART           = 36,   /* Fog start (for both vertex and pixel fog) */
    D3DRENDERSTATE_FOGEND             = 37,   /* Fog end      */
    D3DRENDERSTATE_FOGDENSITY         = 38,   /* Fog density  */
#endif /* DIRECT3D_VERSION >= 0x0700 */
#if(DIRECT3D_VERSION >= 0x0500)
    D3DRENDERSTATE_EDGEANTIALIAS      = 40,   /* TRUE to enable edge antialiasing */
    D3DRENDERSTATE_COLORKEYENABLE     = 41,   /* TRUE to enable source colorkeyed textures */
    D3DRENDERSTATE_ZBIAS              = 47,   /* LONG Z bias */
    D3DRENDERSTATE_RANGEFOGENABLE     = 48,   /* Enables range-based fog */
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)
    D3DRENDERSTATE_STENCILENABLE      = 52,   /* BOOL enable/disable stenciling */
    D3DRENDERSTATE_STENCILFAIL        = 53,   /* D3DSTENCILOP to do if stencil test fails */
    D3DRENDERSTATE_STENCILZFAIL       = 54,   /* D3DSTENCILOP to do if stencil test passes and Z test fails */
    D3DRENDERSTATE_STENCILPASS        = 55,   /* D3DSTENCILOP to do if both stencil and Z tests pass */
    D3DRENDERSTATE_STENCILFUNC        = 56,   /* D3DCMPFUNC fn.  Stencil Test passes if ((ref & mask) stencilfn (stencil & mask)) is true */
    D3DRENDERSTATE_STENCILREF         = 57,   /* Reference value used in stencil test */
    D3DRENDERSTATE_STENCILMASK        = 58,   /* Mask value used in stencil test */
    D3DRENDERSTATE_STENCILWRITEMASK   = 59,   /* Write mask applied to values written to stencil buffer */
    D3DRENDERSTATE_TEXTUREFACTOR      = 60,   /* D3DCOLOR used for multi-texture blend */
#endif /* DIRECT3D_VERSION >= 0x0600 */

#if(DIRECT3D_VERSION >= 0x0600)

    /*
     * 128 values [128, 255] are reserved for texture coordinate wrap flags.
     * These are constructed with the D3DWRAP_U and D3DWRAP_V macros. Using
     * a flags word preserves forward compatibility with texture coordinates
     * that are >2D.
     */
    D3DRENDERSTATE_WRAP0              = 128,  /* wrap for 1st texture coord. set */
    D3DRENDERSTATE_WRAP1              = 129,  /* wrap for 2nd texture coord. set */
    D3DRENDERSTATE_WRAP2              = 130,  /* wrap for 3rd texture coord. set */
    D3DRENDERSTATE_WRAP3              = 131,  /* wrap for 4th texture coord. set */
    D3DRENDERSTATE_WRAP4              = 132,  /* wrap for 5th texture coord. set */
    D3DRENDERSTATE_WRAP5              = 133,  /* wrap for 6th texture coord. set */
    D3DRENDERSTATE_WRAP6              = 134,  /* wrap for 7th texture coord. set */
    D3DRENDERSTATE_WRAP7              = 135,  /* wrap for 8th texture coord. set */
#endif /* DIRECT3D_VERSION >= 0x0600 */
#if(DIRECT3D_VERSION >= 0x0700)
    D3DRENDERSTATE_CLIPPING            = 136,
    D3DRENDERSTATE_LIGHTING            = 137,
    D3DRENDERSTATE_EXTENTS             = 138,
    D3DRENDERSTATE_AMBIENT             = 139,
    D3DRENDERSTATE_FOGVERTEXMODE       = 140,
    D3DRENDERSTATE_COLORVERTEX         = 141,
    D3DRENDERSTATE_LOCALVIEWER         = 142,
    D3DRENDERSTATE_NORMALIZENORMALS    = 143,
    D3DRENDERSTATE_COLORKEYBLENDENABLE = 144,
    D3DRENDERSTATE_DIFFUSEMATERIALSOURCE    = 145,
    D3DRENDERSTATE_SPECULARMATERIALSOURCE   = 146,
    D3DRENDERSTATE_AMBIENTMATERIALSOURCE    = 147,
    D3DRENDERSTATE_EMISSIVEMATERIALSOURCE   = 148,
    D3DRENDERSTATE_VERTEXBLEND              = 151,
    D3DRENDERSTATE_CLIPPLANEENABLE          = 152,

#endif /* DIRECT3D_VERSION >= 0x0700 */

//
// retired renderstates - not supported for DX7 interfaces
//
    D3DRENDERSTATE_TEXTUREHANDLE      = 1,    /* Texture handle for legacy interfaces (Texture,Texture2) */
    D3DRENDERSTATE_TEXTUREADDRESS     = 3,    /* D3DTEXTUREADDRESS  */
    D3DRENDERSTATE_WRAPU              = 5,    /* TRUE for wrapping in u */
    D3DRENDERSTATE_WRAPV              = 6,    /* TRUE for wrapping in v */
    D3DRENDERSTATE_MONOENABLE         = 11,   /* TRUE to enable mono rasterization */
    D3DRENDERSTATE_ROP2               = 12,   /* ROP2 */
    D3DRENDERSTATE_PLANEMASK          = 13,   /* DWORD physical plane mask */
    D3DRENDERSTATE_TEXTUREMAG         = 17,   /* D3DTEXTUREFILTER */
    D3DRENDERSTATE_TEXTUREMIN         = 18,   /* D3DTEXTUREFILTER */
    D3DRENDERSTATE_TEXTUREMAPBLEND    = 21,   /* D3DTEXTUREBLEND */
    D3DRENDERSTATE_SUBPIXEL           = 31,   /* TRUE to enable subpixel correction */
    D3DRENDERSTATE_SUBPIXELX          = 32,   /* TRUE to enable correction in X only */
    D3DRENDERSTATE_STIPPLEENABLE      = 39,   /* TRUE to enable stippling */
#if(DIRECT3D_VERSION >= 0x0500)
    D3DRENDERSTATE_BORDERCOLOR        = 43,   /* Border color for texturing w/border */
    D3DRENDERSTATE_TEXTUREADDRESSU    = 44,   /* Texture addressing mode for U coordinate */
    D3DRENDERSTATE_TEXTUREADDRESSV    = 45,   /* Texture addressing mode for V coordinate */
    D3DRENDERSTATE_MIPMAPLODBIAS      = 46,   /* D3DVALUE Mipmap LOD bias */
    D3DRENDERSTATE_ANISOTROPY         = 49,   /* Max. anisotropy. 1 = no anisotropy */
#endif /* DIRECT3D_VERSION >= 0x0500 */
    D3DRENDERSTATE_FLUSHBATCH         = 50,   /* Explicit flush for DP batching (DX5 Only) */
#if(DIRECT3D_VERSION >= 0x0600)
    D3DRENDERSTATE_TRANSLUCENTSORTINDEPENDENT=51, /* BOOL enable sort-independent transparency */
#endif /* DIRECT3D_VERSION >= 0x0600 */
    D3DRENDERSTATE_STIPPLEPATTERN00   = 64,   /* Stipple pattern 01...  */
    D3DRENDERSTATE_STIPPLEPATTERN01   = 65,
    D3DRENDERSTATE_STIPPLEPATTERN02   = 66,
    D3DRENDERSTATE_STIPPLEPATTERN03   = 67,
    D3DRENDERSTATE_STIPPLEPATTERN04   = 68,
    D3DRENDERSTATE_STIPPLEPATTERN05   = 69,
    D3DRENDERSTATE_STIPPLEPATTERN06   = 70,
    D3DRENDERSTATE_STIPPLEPATTERN07   = 71,
    D3DRENDERSTATE_STIPPLEPATTERN08   = 72,
    D3DRENDERSTATE_STIPPLEPATTERN09   = 73,
    D3DRENDERSTATE_STIPPLEPATTERN10   = 74,
    D3DRENDERSTATE_STIPPLEPATTERN11   = 75,
    D3DRENDERSTATE_STIPPLEPATTERN12   = 76,
    D3DRENDERSTATE_STIPPLEPATTERN13   = 77,
    D3DRENDERSTATE_STIPPLEPATTERN14   = 78,
    D3DRENDERSTATE_STIPPLEPATTERN15   = 79,
    D3DRENDERSTATE_STIPPLEPATTERN16   = 80,
    D3DRENDERSTATE_STIPPLEPATTERN17   = 81,
    D3DRENDERSTATE_STIPPLEPATTERN18   = 82,
    D3DRENDERSTATE_STIPPLEPATTERN19   = 83,
    D3DRENDERSTATE_STIPPLEPATTERN20   = 84,
    D3DRENDERSTATE_STIPPLEPATTERN21   = 85,
    D3DRENDERSTATE_STIPPLEPATTERN22   = 86,
    D3DRENDERSTATE_STIPPLEPATTERN23   = 87,
    D3DRENDERSTATE_STIPPLEPATTERN24   = 88,
    D3DRENDERSTATE_STIPPLEPATTERN25   = 89,
    D3DRENDERSTATE_STIPPLEPATTERN26   = 90,
    D3DRENDERSTATE_STIPPLEPATTERN27   = 91,
    D3DRENDERSTATE_STIPPLEPATTERN28   = 92,
    D3DRENDERSTATE_STIPPLEPATTERN29   = 93,
    D3DRENDERSTATE_STIPPLEPATTERN30   = 94,
    D3DRENDERSTATE_STIPPLEPATTERN31   = 95,

//
// retired renderstate names - the values are still used under new naming conventions
//
    D3DRENDERSTATE_FOGTABLESTART      = 36,   /* Fog table start    */
    D3DRENDERSTATE_FOGTABLEEND        = 37,   /* Fog table end      */
    D3DRENDERSTATE_FOGTABLEDENSITY    = 38,   /* Fog table density  */

#if(DIRECT3D_VERSION >= 0x0500)
    D3DRENDERSTATE_FORCE_DWORD        = 0x7fffffff, /* force 32-bit size enum */
#endif /* DIRECT3D_VERSION >= 0x0500 */
} D3DRENDERSTATETYPE;

#else

typedef enum _D3DRENDERSTATETYPE D3DRENDERSTATETYPE;

//
// legacy renderstate names
//
#define D3DRENDERSTATE_TEXTUREPERSPECTIVE       (D3DRENDERSTATETYPE)4
#define D3DRENDERSTATE_ZENABLE                  (D3DRENDERSTATETYPE)7
#define D3DRENDERSTATE_FILLMODE                 (D3DRENDERSTATETYPE)8
#define D3DRENDERSTATE_SHADEMODE                (D3DRENDERSTATETYPE)9
#define D3DRENDERSTATE_LINEPATTERN              (D3DRENDERSTATETYPE)10
#define D3DRENDERSTATE_ZWRITEENABLE             (D3DRENDERSTATETYPE)14
#define D3DRENDERSTATE_ALPHATESTENABLE          (D3DRENDERSTATETYPE)15
#define D3DRENDERSTATE_LASTPIXEL                (D3DRENDERSTATETYPE)16
#define D3DRENDERSTATE_SRCBLEND                 (D3DRENDERSTATETYPE)19
#define D3DRENDERSTATE_DESTBLEND                (D3DRENDERSTATETYPE)20
#define D3DRENDERSTATE_CULLMODE                 (D3DRENDERSTATETYPE)22
#define D3DRENDERSTATE_ZFUNC                    (D3DRENDERSTATETYPE)23
#define D3DRENDERSTATE_ALPHAREF                 (D3DRENDERSTATETYPE)24
#define D3DRENDERSTATE_ALPHAFUNC                (D3DRENDERSTATETYPE)25
#define D3DRENDERSTATE_DITHERENABLE             (D3DRENDERSTATETYPE)26
#define D3DRENDERSTATE_ALPHABLENDENABLE         (D3DRENDERSTATETYPE)27
#define D3DRENDERSTATE_FOGENABLE                (D3DRENDERSTATETYPE)28
#define D3DRENDERSTATE_SPECULARENABLE           (D3DRENDERSTATETYPE)29
#define D3DRENDERSTATE_ZVISIBLE                 (D3DRENDERSTATETYPE)30
#define D3DRENDERSTATE_STIPPLEDALPHA            (D3DRENDERSTATETYPE)33
#define D3DRENDERSTATE_FOGCOLOR                 (D3DRENDERSTATETYPE)34
#define D3DRENDERSTATE_FOGTABLEMODE             (D3DRENDERSTATETYPE)35
#define D3DRENDERSTATE_FOGSTART                 (D3DRENDERSTATETYPE)36
#define D3DRENDERSTATE_FOGEND                   (D3DRENDERSTATETYPE)37
#define D3DRENDERSTATE_FOGDENSITY               (D3DRENDERSTATETYPE)38
#define D3DRENDERSTATE_EDGEANTIALIAS            (D3DRENDERSTATETYPE)40
#define D3DRENDERSTATE_ZBIAS                    (D3DRENDERSTATETYPE)47
#define D3DRENDERSTATE_RANGEFOGENABLE           (D3DRENDERSTATETYPE)48
#define D3DRENDERSTATE_STENCILENABLE            (D3DRENDERSTATETYPE)52
#define D3DRENDERSTATE_STENCILFAIL              (D3DRENDERSTATETYPE)53
#define D3DRENDERSTATE_STENCILZFAIL             (D3DRENDERSTATETYPE)54
#define D3DRENDERSTATE_STENCILPASS              (D3DRENDERSTATETYPE)55
#define D3DRENDERSTATE_STENCILFUNC              (D3DRENDERSTATETYPE)56
#define D3DRENDERSTATE_STENCILREF               (D3DRENDERSTATETYPE)57
#define D3DRENDERSTATE_STENCILMASK              (D3DRENDERSTATETYPE)58
#define D3DRENDERSTATE_STENCILWRITEMASK         (D3DRENDERSTATETYPE)59
#define D3DRENDERSTATE_TEXTUREFACTOR            (D3DRENDERSTATETYPE)60
#define D3DRENDERSTATE_WRAP0                    (D3DRENDERSTATETYPE)128
#define D3DRENDERSTATE_WRAP1                    (D3DRENDERSTATETYPE)129
#define D3DRENDERSTATE_WRAP2                    (D3DRENDERSTATETYPE)130
#define D3DRENDERSTATE_WRAP3                    (D3DRENDERSTATETYPE)131
#define D3DRENDERSTATE_WRAP4                    (D3DRENDERSTATETYPE)132
#define D3DRENDERSTATE_WRAP5                    (D3DRENDERSTATETYPE)133
#define D3DRENDERSTATE_WRAP6                    (D3DRENDERSTATETYPE)134
#define D3DRENDERSTATE_WRAP7                    (D3DRENDERSTATETYPE)135

#define D3DRENDERSTATE_CLIPPING                 (D3DRENDERSTATETYPE)136
#define D3DRENDERSTATE_LIGHTING                 (D3DRENDERSTATETYPE)137
#define D3DRENDERSTATE_EXTENTS                  (D3DRENDERSTATETYPE)138
#define D3DRENDERSTATE_AMBIENT                  (D3DRENDERSTATETYPE)139
#define D3DRENDERSTATE_FOGVERTEXMODE            (D3DRENDERSTATETYPE)140
#define D3DRENDERSTATE_COLORVERTEX              (D3DRENDERSTATETYPE)141
#define D3DRENDERSTATE_LOCALVIEWER              (D3DRENDERSTATETYPE)142
#define D3DRENDERSTATE_NORMALIZENORMALS         (D3DRENDERSTATETYPE)143
#define D3DRENDERSTATE_COLORKEYBLENDENABLE      (D3DRENDERSTATETYPE)144
#define D3DRENDERSTATE_DIFFUSEMATERIALSOURCE    (D3DRENDERSTATETYPE)145
#define D3DRENDERSTATE_SPECULARMATERIALSOURCE   (D3DRENDERSTATETYPE)146
#define D3DRENDERSTATE_AMBIENTMATERIALSOURCE    (D3DRENDERSTATETYPE)147
#define D3DRENDERSTATE_EMISSIVEMATERIALSOURCE   (D3DRENDERSTATETYPE)148
#define D3DRENDERSTATE_VERTEXBLEND              (D3DRENDERSTATETYPE)151
#define D3DRENDERSTATE_CLIPPLANEENABLE          (D3DRENDERSTATETYPE)152

//
// retired renderstates - not supported for DX7 interfaces
//
#define D3DRENDERSTATE_TEXTUREHANDLE     (D3DRENDERSTATETYPE)1
#define D3DRENDERSTATE_ANTIALIAS         (D3DRENDERSTATETYPE)2
#define D3DRENDERSTATE_TEXTUREADDRESS    (D3DRENDERSTATETYPE)3
#define D3DRENDERSTATE_WRAPU             (D3DRENDERSTATETYPE)5
#define D3DRENDERSTATE_WRAPV             (D3DRENDERSTATETYPE)6
#define D3DRENDERSTATE_MONOENABLE        (D3DRENDERSTATETYPE)11
#define D3DRENDERSTATE_ROP2              (D3DRENDERSTATETYPE)12
#define D3DRENDERSTATE_PLANEMASK         (D3DRENDERSTATETYPE)13
#define D3DRENDERSTATE_TEXTUREMAG        (D3DRENDERSTATETYPE)17
#define D3DRENDERSTATE_TEXTUREMIN        (D3DRENDERSTATETYPE)18
#define D3DRENDERSTATE_TEXTUREMAPBLEND   (D3DRENDERSTATETYPE)21
#define D3DRENDERSTATE_SUBPIXEL          (D3DRENDERSTATETYPE)31
#define D3DRENDERSTATE_SUBPIXELX         (D3DRENDERSTATETYPE)32
#define D3DRENDERSTATE_STIPPLEENABLE     (D3DRENDERSTATETYPE)39
#define D3DRENDERSTATE_OLDALPHABLENDENABLE  (D3DRENDERSTATETYPE)42
#define D3DRENDERSTATE_BORDERCOLOR       (D3DRENDERSTATETYPE)43
#define D3DRENDERSTATE_TEXTUREADDRESSU   (D3DRENDERSTATETYPE)44
#define D3DRENDERSTATE_TEXTUREADDRESSV   (D3DRENDERSTATETYPE)45
#define D3DRENDERSTATE_MIPMAPLODBIAS     (D3DRENDERSTATETYPE)46
#define D3DRENDERSTATE_ANISOTROPY        (D3DRENDERSTATETYPE)49
#define D3DRENDERSTATE_FLUSHBATCH        (D3DRENDERSTATETYPE)50
#define D3DRENDERSTATE_TRANSLUCENTSORTINDEPENDENT (D3DRENDERSTATETYPE)51
#define D3DRENDERSTATE_STIPPLEPATTERN00  (D3DRENDERSTATETYPE)64
#define D3DRENDERSTATE_STIPPLEPATTERN01  (D3DRENDERSTATETYPE)65
#define D3DRENDERSTATE_STIPPLEPATTERN02  (D3DRENDERSTATETYPE)66
#define D3DRENDERSTATE_STIPPLEPATTERN03  (D3DRENDERSTATETYPE)67
#define D3DRENDERSTATE_STIPPLEPATTERN04  (D3DRENDERSTATETYPE)68
#define D3DRENDERSTATE_STIPPLEPATTERN05  (D3DRENDERSTATETYPE)69
#define D3DRENDERSTATE_STIPPLEPATTERN06  (D3DRENDERSTATETYPE)70
#define D3DRENDERSTATE_STIPPLEPATTERN07  (D3DRENDERSTATETYPE)71
#define D3DRENDERSTATE_STIPPLEPATTERN08  (D3DRENDERSTATETYPE)72
#define D3DRENDERSTATE_STIPPLEPATTERN09  (D3DRENDERSTATETYPE)73
#define D3DRENDERSTATE_STIPPLEPATTERN10  (D3DRENDERSTATETYPE)74
#define D3DRENDERSTATE_STIPPLEPATTERN11  (D3DRENDERSTATETYPE)75
#define D3DRENDERSTATE_STIPPLEPATTERN12  (D3DRENDERSTATETYPE)76
#define D3DRENDERSTATE_STIPPLEPATTERN13  (D3DRENDERSTATETYPE)77
#define D3DRENDERSTATE_STIPPLEPATTERN14  (D3DRENDERSTATETYPE)78
#define D3DRENDERSTATE_STIPPLEPATTERN15  (D3DRENDERSTATETYPE)79
#define D3DRENDERSTATE_STIPPLEPATTERN16  (D3DRENDERSTATETYPE)80
#define D3DRENDERSTATE_STIPPLEPATTERN17  (D3DRENDERSTATETYPE)81
#define D3DRENDERSTATE_STIPPLEPATTERN18  (D3DRENDERSTATETYPE)82
#define D3DRENDERSTATE_STIPPLEPATTERN19  (D3DRENDERSTATETYPE)83
#define D3DRENDERSTATE_STIPPLEPATTERN20  (D3DRENDERSTATETYPE)84
#define D3DRENDERSTATE_STIPPLEPATTERN21  (D3DRENDERSTATETYPE)85
#define D3DRENDERSTATE_STIPPLEPATTERN22  (D3DRENDERSTATETYPE)86
#define D3DRENDERSTATE_STIPPLEPATTERN23  (D3DRENDERSTATETYPE)87
#define D3DRENDERSTATE_STIPPLEPATTERN24  (D3DRENDERSTATETYPE)88
#define D3DRENDERSTATE_STIPPLEPATTERN25  (D3DRENDERSTATETYPE)89
#define D3DRENDERSTATE_STIPPLEPATTERN26  (D3DRENDERSTATETYPE)90
#define D3DRENDERSTATE_STIPPLEPATTERN27  (D3DRENDERSTATETYPE)91
#define D3DRENDERSTATE_STIPPLEPATTERN28  (D3DRENDERSTATETYPE)92
#define D3DRENDERSTATE_STIPPLEPATTERN29  (D3DRENDERSTATETYPE)93
#define D3DRENDERSTATE_STIPPLEPATTERN30  (D3DRENDERSTATETYPE)94
#define D3DRENDERSTATE_STIPPLEPATTERN31  (D3DRENDERSTATETYPE)95

//
// retired renderstates - not supported for DX8 interfaces
//
#define D3DRENDERSTATE_COLORKEYENABLE        (D3DRENDERSTATETYPE)41
#define D3DRENDERSTATE_COLORKEYBLENDENABLE   (D3DRENDERSTATETYPE)144

//
// retired renderstate names - the values are still used under new naming conventions
//
#define D3DRENDERSTATE_BLENDENABLE       (D3DRENDERSTATETYPE)27
#define D3DRENDERSTATE_FOGTABLESTART     (D3DRENDERSTATETYPE)36
#define D3DRENDERSTATE_FOGTABLEEND       (D3DRENDERSTATETYPE)37
#define D3DRENDERSTATE_FOGTABLEDENSITY   (D3DRENDERSTATETYPE)38

#endif //(DIRECT3D_VERSION < 0x0800)


#if(DIRECT3D_VERSION < 0x0800)

// Values for material source
typedef enum _D3DMATERIALCOLORSOURCE
{
    D3DMCS_MATERIAL = 0,                // Color from material is used
    D3DMCS_COLOR1   = 1,                // Diffuse vertex color is used
    D3DMCS_COLOR2   = 2,                // Specular vertex color is used
    D3DMCS_FORCE_DWORD = 0x7fffffff,    // force 32-bit size enum
} D3DMATERIALCOLORSOURCE;


#if(DIRECT3D_VERSION >= 0x0500)
// For back-compatibility with legacy compilations
#define D3DRENDERSTATE_BLENDENABLE      D3DRENDERSTATE_ALPHABLENDENABLE
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)

// Bias to apply to the texture coordinate set to apply a wrap to.
#define D3DRENDERSTATE_WRAPBIAS                 128UL

/* Flags to construct the WRAP render states */
#define D3DWRAP_U   0x00000001L
#define D3DWRAP_V   0x00000002L

#endif /* DIRECT3D_VERSION >= 0x0600 */

#if(DIRECT3D_VERSION >= 0x0700)

/* Flags to construct the WRAP render states for 1D thru 4D texture coordinates */
#define D3DWRAPCOORD_0   0x00000001L    // same as D3DWRAP_U
#define D3DWRAPCOORD_1   0x00000002L    // same as D3DWRAP_V
#define D3DWRAPCOORD_2   0x00000004L
#define D3DWRAPCOORD_3   0x00000008L

#endif /* DIRECT3D_VERSION >= 0x0700 */

#endif //(DIRECT3D_VERSION < 0x0800)

#define D3DRENDERSTATE_STIPPLEPATTERN(y) (D3DRENDERSTATE_STIPPLEPATTERN00 + (y))

typedef struct _D3DSTATE {
    union {
#if(DIRECT3D_VERSION < 0x0800)
    D3DTRANSFORMSTATETYPE   dtstTransformStateType;
#endif //(DIRECT3D_VERSION < 0x0800)
    D3DLIGHTSTATETYPE   dlstLightStateType;
    D3DRENDERSTATETYPE  drstRenderStateType;
    };
    union {
    DWORD           dwArg[1];
    D3DVALUE        dvArg[1];
    };
} D3DSTATE, *LPD3DSTATE;


/*
 * Operation used to load matrices
 * hDstMat = hSrcMat
 */
typedef struct _D3DMATRIXLOAD {
    D3DMATRIXHANDLE hDestMatrix;   /* Destination matrix */
    D3DMATRIXHANDLE hSrcMatrix;   /* Source matrix */
} D3DMATRIXLOAD, *LPD3DMATRIXLOAD;

/*
 * Operation used to multiply matrices
 * hDstMat = hSrcMat1 * hSrcMat2
 */
typedef struct _D3DMATRIXMULTIPLY {
    D3DMATRIXHANDLE hDestMatrix;   /* Destination matrix */
    D3DMATRIXHANDLE hSrcMatrix1;  /* First source matrix */
    D3DMATRIXHANDLE hSrcMatrix2;  /* Second source matrix */
} D3DMATRIXMULTIPLY, *LPD3DMATRIXMULTIPLY;

/*
 * Operation used to transform and light vertices.
 */
typedef struct _D3DPROCESSVERTICES {
    DWORD        dwFlags;    /* Do we transform or light or just copy? */
    WORD         wStart;     /* Index to first vertex in source */
    WORD         wDest;      /* Index to first vertex in local buffer */
    DWORD        dwCount;    /* Number of vertices to be processed */
    DWORD    dwReserved; /* Must be zero */
} D3DPROCESSVERTICES, *LPD3DPROCESSVERTICES;

#define D3DPROCESSVERTICES_TRANSFORMLIGHT   0x00000000L
#define D3DPROCESSVERTICES_TRANSFORM        0x00000001L
#define D3DPROCESSVERTICES_COPY         0x00000002L
#define D3DPROCESSVERTICES_OPMASK       0x00000007L

#define D3DPROCESSVERTICES_UPDATEEXTENTS    0x00000008L
#define D3DPROCESSVERTICES_NOCOLOR      0x00000010L


#if(DIRECT3D_VERSION >= 0x0600)


#if(DIRECT3D_VERSION < 0x0800)

/*
 * State enumerants for per-stage texture processing.
 */
typedef enum _D3DTEXTURESTAGESTATETYPE
{
    D3DTSS_COLOROP        =  1, /* D3DTEXTUREOP - per-stage blending controls for color channels */
    D3DTSS_COLORARG1      =  2, /* D3DTA_* (texture arg) */
    D3DTSS_COLORARG2      =  3, /* D3DTA_* (texture arg) */
    D3DTSS_ALPHAOP        =  4, /* D3DTEXTUREOP - per-stage blending controls for alpha channel */
    D3DTSS_ALPHAARG1      =  5, /* D3DTA_* (texture arg) */
    D3DTSS_ALPHAARG2      =  6, /* D3DTA_* (texture arg) */
    D3DTSS_BUMPENVMAT00   =  7, /* D3DVALUE (bump mapping matrix) */
    D3DTSS_BUMPENVMAT01   =  8, /* D3DVALUE (bump mapping matrix) */
    D3DTSS_BUMPENVMAT10   =  9, /* D3DVALUE (bump mapping matrix) */
    D3DTSS_BUMPENVMAT11   = 10, /* D3DVALUE (bump mapping matrix) */
    D3DTSS_TEXCOORDINDEX  = 11, /* identifies which set of texture coordinates index this texture */
    D3DTSS_ADDRESS        = 12, /* D3DTEXTUREADDRESS for both coordinates */
    D3DTSS_ADDRESSU       = 13, /* D3DTEXTUREADDRESS for U coordinate */
    D3DTSS_ADDRESSV       = 14, /* D3DTEXTUREADDRESS for V coordinate */
    D3DTSS_BORDERCOLOR    = 15, /* D3DCOLOR */
    D3DTSS_MAGFILTER      = 16, /* D3DTEXTUREMAGFILTER filter to use for magnification */
    D3DTSS_MINFILTER      = 17, /* D3DTEXTUREMINFILTER filter to use for minification */
    D3DTSS_MIPFILTER      = 18, /* D3DTEXTUREMIPFILTER filter to use between mipmaps during minification */
    D3DTSS_MIPMAPLODBIAS  = 19, /* D3DVALUE Mipmap LOD bias */
    D3DTSS_MAXMIPLEVEL    = 20, /* DWORD 0..(n-1) LOD index of largest map to use (0 == largest) */
    D3DTSS_MAXANISOTROPY  = 21, /* DWORD maximum anisotropy */
    D3DTSS_BUMPENVLSCALE  = 22, /* D3DVALUE scale for bump map luminance */
    D3DTSS_BUMPENVLOFFSET = 23, /* D3DVALUE offset for bump map luminance */
#if(DIRECT3D_VERSION >= 0x0700)
    D3DTSS_TEXTURETRANSFORMFLAGS = 24, /* D3DTEXTURETRANSFORMFLAGS controls texture transform */
#endif /* DIRECT3D_VERSION >= 0x0700 */
    D3DTSS_FORCE_DWORD   = 0x7fffffff, /* force 32-bit size enum */
} D3DTEXTURESTAGESTATETYPE;

#if(DIRECT3D_VERSION >= 0x0700)
// Values, used with D3DTSS_TEXCOORDINDEX, to specify that the vertex data(position
// and normal in the camera space) should be taken as texture coordinates
// Low 16 bits are used to specify texture coordinate index, to take the WRAP mode from
//
#define D3DTSS_TCI_PASSTHRU                             0x00000000
#define D3DTSS_TCI_CAMERASPACENORMAL                    0x00010000
#define D3DTSS_TCI_CAMERASPACEPOSITION                  0x00020000
#define D3DTSS_TCI_CAMERASPACEREFLECTIONVECTOR          0x00030000
#endif /* DIRECT3D_VERSION >= 0x0700 */

/*
 * Enumerations for COLOROP and ALPHAOP texture blending operations set in
 * texture processing stage controls in D3DRENDERSTATE.
 */
typedef enum _D3DTEXTUREOP
{
// Control
    D3DTOP_DISABLE    = 1,      // disables stage
    D3DTOP_SELECTARG1 = 2,      // the default
    D3DTOP_SELECTARG2 = 3,

// Modulate
    D3DTOP_MODULATE   = 4,      // multiply args together
    D3DTOP_MODULATE2X = 5,      // multiply and  1 bit
    D3DTOP_MODULATE4X = 6,      // multiply and  2 bits

// Add
    D3DTOP_ADD          =  7,   // add arguments together
    D3DTOP_ADDSIGNED    =  8,   // add with -0.5 bias
    D3DTOP_ADDSIGNED2X  =  9,   // as above but left  1 bit
    D3DTOP_SUBTRACT     = 10,   // Arg1 - Arg2, with no saturation
    D3DTOP_ADDSMOOTH    = 11,   // add 2 args, subtract product
                                // Arg1 + Arg2 - Arg1*Arg2
                                // = Arg1 + (1-Arg1)*Arg2

// Linear alpha blend: Arg1*(Alpha) + Arg2*(1-Alpha)
    D3DTOP_BLENDDIFFUSEALPHA    = 12, // iterated alpha
    D3DTOP_BLENDTEXTUREALPHA    = 13, // texture alpha
    D3DTOP_BLENDFACTORALPHA     = 14, // alpha from D3DRENDERSTATE_TEXTUREFACTOR
    // Linear alpha blend with pre-multiplied arg1 input: Arg1 + Arg2*(1-Alpha)
    D3DTOP_BLENDTEXTUREALPHAPM  = 15, // texture alpha
    D3DTOP_BLENDCURRENTALPHA    = 16, // by alpha of current color

// Specular mapping
    D3DTOP_PREMODULATE            = 17,     // modulate with next texture before use
    D3DTOP_MODULATEALPHA_ADDCOLOR = 18,     // Arg1.RGB + Arg1.A*Arg2.RGB
                                            // COLOROP only
    D3DTOP_MODULATECOLOR_ADDALPHA = 19,     // Arg1.RGB*Arg2.RGB + Arg1.A
                                            // COLOROP only
    D3DTOP_MODULATEINVALPHA_ADDCOLOR = 20,  // (1-Arg1.A)*Arg2.RGB + Arg1.RGB
                                            // COLOROP only
    D3DTOP_MODULATEINVCOLOR_ADDALPHA = 21,  // (1-Arg1.RGB)*Arg2.RGB + Arg1.A
                                            // COLOROP only

// Bump mapping
    D3DTOP_BUMPENVMAP           = 22, // per pixel env map perturbation
    D3DTOP_BUMPENVMAPLUMINANCE  = 23, // with luminance channel
    // This can do either diffuse or specular bump mapping with correct input.
    // Performs the function (Arg1.R*Arg2.R + Arg1.G*Arg2.G + Arg1.B*Arg2.B)
    // where each component has been scaled and offset to make it signed.
    // The result is replicated into all four (including alpha) channels.
    // This is a valid COLOROP only.
    D3DTOP_DOTPRODUCT3          = 24,

    D3DTOP_FORCE_DWORD = 0x7fffffff,
} D3DTEXTUREOP;

/*
 * Values for COLORARG1,2 and ALPHAARG1,2 texture blending operations
 * set in texture processing stage controls in D3DRENDERSTATE.
 */
#define D3DTA_SELECTMASK        0x0000000f  // mask for arg selector
#define D3DTA_DIFFUSE           0x00000000  // select diffuse color
#define D3DTA_CURRENT           0x00000001  // select result of previous stage
#define D3DTA_TEXTURE           0x00000002  // select texture color
#define D3DTA_TFACTOR           0x00000003  // select RENDERSTATE_TEXTUREFACTOR
#if(DIRECT3D_VERSION >= 0x0700)
#define D3DTA_SPECULAR          0x00000004  // select specular color
#endif /* DIRECT3D_VERSION >= 0x0700 */
#define D3DTA_COMPLEMENT        0x00000010  // take 1.0 - x
#define D3DTA_ALPHAREPLICATE    0x00000020  // replicate alpha to color components

#endif //(DIRECT3D_VERSION < 0x0800)

/*
 *  IDirect3DTexture2 State Filter Types
 */
typedef enum _D3DTEXTUREMAGFILTER
{
    D3DTFG_POINT        = 1,    // nearest
    D3DTFG_LINEAR       = 2,    // linear interpolation
    D3DTFG_FLATCUBIC    = 3,    // cubic
    D3DTFG_GAUSSIANCUBIC = 4,   // different cubic kernel
    D3DTFG_ANISOTROPIC  = 5,    //
#if(DIRECT3D_VERSION >= 0x0700)
#endif /* DIRECT3D_VERSION >= 0x0700 */
    D3DTFG_FORCE_DWORD  = 0x7fffffff,   // force 32-bit size enum
} D3DTEXTUREMAGFILTER;

typedef enum _D3DTEXTUREMINFILTER
{
    D3DTFN_POINT        = 1,    // nearest
    D3DTFN_LINEAR       = 2,    // linear interpolation
    D3DTFN_ANISOTROPIC  = 3,    //
    D3DTFN_FORCE_DWORD  = 0x7fffffff,   // force 32-bit size enum
} D3DTEXTUREMINFILTER;

typedef enum _D3DTEXTUREMIPFILTER
{
    D3DTFP_NONE         = 1,    // mipmapping disabled (use MAG filter)
    D3DTFP_POINT        = 2,    // nearest
    D3DTFP_LINEAR       = 3,    // linear interpolation
    D3DTFP_FORCE_DWORD  = 0x7fffffff,   // force 32-bit size enum
} D3DTEXTUREMIPFILTER;

#endif /* DIRECT3D_VERSION >= 0x0600 */

/*
 * Triangle flags
 */

/*
 * Tri strip and fan flags.
 * START loads all three vertices
 * EVEN and ODD load just v3 with even or odd culling
 * START_FLAT contains a count from 0 to 29 that allows the
 * whole strip or fan to be culled in one hit.
 * e.g. for a quad len = 1
 */
#define D3DTRIFLAG_START            0x00000000L
#define D3DTRIFLAG_STARTFLAT(len) (len)     /* 0 < len < 30 */
#define D3DTRIFLAG_ODD              0x0000001eL
#define D3DTRIFLAG_EVEN             0x0000001fL

/*
 * Triangle edge flags
 * enable edges for wireframe or antialiasing
 */
#define D3DTRIFLAG_EDGEENABLE1          0x00000100L /* v0-v1 edge */
#define D3DTRIFLAG_EDGEENABLE2          0x00000200L /* v1-v2 edge */
#define D3DTRIFLAG_EDGEENABLE3          0x00000400L /* v2-v0 edge */
#define D3DTRIFLAG_EDGEENABLETRIANGLE \
        (D3DTRIFLAG_EDGEENABLE1 | D3DTRIFLAG_EDGEENABLE2 | D3DTRIFLAG_EDGEENABLE3)

/*
 * Primitive structures and related defines.  Vertex offsets are to types
 * D3DVERTEX, D3DLVERTEX, or D3DTLVERTEX.
 */

/*
 * Triangle list primitive structure
 */
typedef struct _D3DTRIANGLE {
    union {
    WORD    v1;            /* Vertex indices */
    WORD    wV1;
    };
    union {
    WORD    v2;
    WORD    wV2;
    };
    union {
    WORD    v3;
    WORD    wV3;
    };
    WORD        wFlags;       /* Edge (and other) flags */
} D3DTRIANGLE, *LPD3DTRIANGLE;

/*
 * Line list structure.
 * The instruction count defines the number of line segments.
 */
typedef struct _D3DLINE {
    union {
    WORD    v1;            /* Vertex indices */
    WORD    wV1;
    };
    union {
    WORD    v2;
    WORD    wV2;
    };
} D3DLINE, *LPD3DLINE;

/*
 * Span structure
 * Spans join a list of points with the same y value.
 * If the y value changes, a new span is started.
 */
typedef struct _D3DSPAN {
    WORD    wCount; /* Number of spans */
    WORD    wFirst; /* Index to first vertex */
} D3DSPAN, *LPD3DSPAN;

/*
 * Point structure
 */
typedef struct _D3DPOINT {
    WORD    wCount;     /* number of points     */
    WORD    wFirst;     /* index to first vertex    */
} D3DPOINT, *LPD3DPOINT;


/*
 * Forward branch structure.
 * Mask is logically anded with the driver status mask
 * if the result equals 'value', the branch is taken.
 */
typedef struct _D3DBRANCH {
    DWORD   dwMask;     /* Bitmask against D3D status */
    DWORD   dwValue;
    BOOL    bNegate;        /* TRUE to negate comparison */
    DWORD   dwOffset;   /* How far to branch forward (0 for exit)*/
} D3DBRANCH, *LPD3DBRANCH;

/*
 * Status used for set status instruction.
 * The D3D status is initialised on device creation
 * and is modified by all execute calls.
 */
typedef struct _D3DSTATUS {
    DWORD       dwFlags;    /* Do we set extents or status */
    DWORD   dwStatus;   /* D3D status */
    D3DRECT drExtent;
} D3DSTATUS, *LPD3DSTATUS;

#define D3DSETSTATUS_STATUS     0x00000001L
#define D3DSETSTATUS_EXTENTS        0x00000002L
#define D3DSETSTATUS_ALL    (D3DSETSTATUS_STATUS | D3DSETSTATUS_EXTENTS)

#if(DIRECT3D_VERSION >= 0x0500)
typedef struct _D3DCLIPSTATUS {
    DWORD dwFlags; /* Do we set 2d extents, 3D extents or status */
    DWORD dwStatus; /* Clip status */
    float minx, maxx; /* X extents */
    float miny, maxy; /* Y extents */
    float minz, maxz; /* Z extents */
} D3DCLIPSTATUS, *LPD3DCLIPSTATUS;

#define D3DCLIPSTATUS_STATUS        0x00000001L
#define D3DCLIPSTATUS_EXTENTS2      0x00000002L
#define D3DCLIPSTATUS_EXTENTS3      0x00000004L

#endif /* DIRECT3D_VERSION >= 0x0500 */
/*
 * Statistics structure
 */
typedef struct _D3DSTATS {
    DWORD        dwSize;
    DWORD        dwTrianglesDrawn;
    DWORD        dwLinesDrawn;
    DWORD        dwPointsDrawn;
    DWORD        dwSpansDrawn;
    DWORD        dwVerticesProcessed;
} D3DSTATS, *LPD3DSTATS;

/*
 * Execute options.
 * When calling using D3DEXECUTE_UNCLIPPED all the primitives
 * inside the buffer must be contained within the viewport.
 */
#define D3DEXECUTE_CLIPPED       0x00000001l
#define D3DEXECUTE_UNCLIPPED     0x00000002l

typedef struct _D3DEXECUTEDATA {
    DWORD       dwSize;
    DWORD       dwVertexOffset;
    DWORD       dwVertexCount;
    DWORD       dwInstructionOffset;
    DWORD       dwInstructionLength;
    DWORD       dwHVertexOffset;
    D3DSTATUS   dsStatus;   /* Status after execute */
} D3DEXECUTEDATA, *LPD3DEXECUTEDATA;

/*
 * Palette flags.
 * This are or'ed with the peFlags in the PALETTEENTRYs passed to DirectDraw.
 */
#define D3DPAL_FREE 0x00    /* Renderer may use this entry freely */
#define D3DPAL_READONLY 0x40    /* Renderer may not set this entry */
#define D3DPAL_RESERVED 0x80    /* Renderer may not use this entry */


#if(DIRECT3D_VERSION >= 0x0600)

typedef struct _D3DVERTEXBUFFERDESC {
    DWORD dwSize;
    DWORD dwCaps;
    DWORD dwFVF;
    DWORD dwNumVertices;
} D3DVERTEXBUFFERDESC, *LPD3DVERTEXBUFFERDESC;

#define D3DVBCAPS_SYSTEMMEMORY      0x00000800l
#define D3DVBCAPS_WRITEONLY         0x00010000l
#define D3DVBCAPS_OPTIMIZED         0x80000000l
#define D3DVBCAPS_DONOTCLIP         0x00000001l

/* Vertex Operations for ProcessVertices */
#define D3DVOP_LIGHT       (1 << 10)
#define D3DVOP_TRANSFORM   (1 << 0)
#define D3DVOP_CLIP        (1 << 2)
#define D3DVOP_EXTENTS     (1 << 3)


#if(DIRECT3D_VERSION < 0x0800)

/* The maximum number of vertices user can pass to any d3d
   drawing function or to create vertex buffer with
*/
#define D3DMAXNUMVERTICES    ((1<<16) - 1)
/* The maximum number of primitives user can pass to any d3d
   drawing function.
*/
#define D3DMAXNUMPRIMITIVES  ((1<<16) - 1)

#if(DIRECT3D_VERSION >= 0x0700)

/* Bits for dwFlags in ProcessVertices call */
#define D3DPV_DONOTCOPYDATA (1 << 0)

#endif /* DIRECT3D_VERSION >= 0x0700 */

#endif //(DIRECT3D_VERSION < 0x0800)

//-------------------------------------------------------------------

#if(DIRECT3D_VERSION < 0x0800)

// Flexible vertex format bits
//
#define D3DFVF_RESERVED0        0x001
#define D3DFVF_POSITION_MASK    0x00E
#define D3DFVF_XYZ              0x002
#define D3DFVF_XYZRHW           0x004
#if(DIRECT3D_VERSION >= 0x0700)
#define D3DFVF_XYZB1            0x006
#define D3DFVF_XYZB2            0x008
#define D3DFVF_XYZB3            0x00a
#define D3DFVF_XYZB4            0x00c
#define D3DFVF_XYZB5            0x00e

#endif /* DIRECT3D_VERSION >= 0x0700 */
#define D3DFVF_NORMAL           0x010
#define D3DFVF_RESERVED1        0x020
#define D3DFVF_DIFFUSE          0x040
#define D3DFVF_SPECULAR         0x080

#define D3DFVF_TEXCOUNT_MASK    0xf00
#define D3DFVF_TEXCOUNT_SHIFT   8
#define D3DFVF_TEX0             0x000
#define D3DFVF_TEX1             0x100
#define D3DFVF_TEX2             0x200
#define D3DFVF_TEX3             0x300
#define D3DFVF_TEX4             0x400
#define D3DFVF_TEX5             0x500
#define D3DFVF_TEX6             0x600
#define D3DFVF_TEX7             0x700
#define D3DFVF_TEX8             0x800

#define D3DFVF_RESERVED2        0xf000  // 4 reserved bits

#else
#define D3DFVF_RESERVED1        0x020
#endif //(DIRECT3D_VERSION < 0x0800)

#define D3DFVF_VERTEX ( D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1 )
#define D3DFVF_LVERTEX ( D3DFVF_XYZ | D3DFVF_RESERVED1 | D3DFVF_DIFFUSE | \
                         D3DFVF_SPECULAR | D3DFVF_TEX1 )
#define D3DFVF_TLVERTEX ( D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_SPECULAR | \
                          D3DFVF_TEX1 )


typedef struct _D3DDP_PTRSTRIDE
{
    LPVOID lpvData;
    DWORD  dwStride;
} D3DDP_PTRSTRIDE;

#define D3DDP_MAXTEXCOORD 8

typedef struct _D3DDRAWPRIMITIVESTRIDEDDATA
{
    D3DDP_PTRSTRIDE position;
    D3DDP_PTRSTRIDE normal;
    D3DDP_PTRSTRIDE diffuse;
    D3DDP_PTRSTRIDE specular;
    D3DDP_PTRSTRIDE textureCoords[D3DDP_MAXTEXCOORD];
} D3DDRAWPRIMITIVESTRIDEDDATA, *LPD3DDRAWPRIMITIVESTRIDEDDATA;
//---------------------------------------------------------------------
// ComputeSphereVisibility return values
//
#define D3DVIS_INSIDE_FRUSTUM       0
#define D3DVIS_INTERSECT_FRUSTUM    1
#define D3DVIS_OUTSIDE_FRUSTUM      2
#define D3DVIS_INSIDE_LEFT          0
#define D3DVIS_INTERSECT_LEFT       (1 << 2)
#define D3DVIS_OUTSIDE_LEFT         (2 << 2)
#define D3DVIS_INSIDE_RIGHT         0
#define D3DVIS_INTERSECT_RIGHT      (1 << 4)
#define D3DVIS_OUTSIDE_RIGHT        (2 << 4)
#define D3DVIS_INSIDE_TOP           0
#define D3DVIS_INTERSECT_TOP        (1 << 6)
#define D3DVIS_OUTSIDE_TOP          (2 << 6)
#define D3DVIS_INSIDE_BOTTOM        0
#define D3DVIS_INTERSECT_BOTTOM     (1 << 8)
#define D3DVIS_OUTSIDE_BOTTOM       (2 << 8)
#define D3DVIS_INSIDE_NEAR          0
#define D3DVIS_INTERSECT_NEAR       (1 << 10)
#define D3DVIS_OUTSIDE_NEAR         (2 << 10)
#define D3DVIS_INSIDE_FAR           0
#define D3DVIS_INTERSECT_FAR        (1 << 12)
#define D3DVIS_OUTSIDE_FAR          (2 << 12)

#define D3DVIS_MASK_FRUSTUM         (3 << 0)
#define D3DVIS_MASK_LEFT            (3 << 2)
#define D3DVIS_MASK_RIGHT           (3 << 4)
#define D3DVIS_MASK_TOP             (3 << 6)
#define D3DVIS_MASK_BOTTOM          (3 << 8)
#define D3DVIS_MASK_NEAR            (3 << 10)
#define D3DVIS_MASK_FAR             (3 << 12)

#endif /* DIRECT3D_VERSION >= 0x0600 */

#if(DIRECT3D_VERSION < 0x0800)

#if(DIRECT3D_VERSION >= 0x0700)

// To be used with GetInfo()
#define D3DDEVINFOID_TEXTUREMANAGER    1
#define D3DDEVINFOID_D3DTEXTUREMANAGER 2
#define D3DDEVINFOID_TEXTURING         3

typedef enum _D3DSTATEBLOCKTYPE
{
    D3DSBT_ALL           = 1, // capture all state
    D3DSBT_PIXELSTATE    = 2, // capture pixel state
    D3DSBT_VERTEXSTATE   = 3, // capture vertex state
    D3DSBT_FORCE_DWORD   = 0xffffffff
} D3DSTATEBLOCKTYPE;

// The D3DVERTEXBLENDFLAGS type is used with D3DRENDERSTATE_VERTEXBLEND state.
//
typedef enum _D3DVERTEXBLENDFLAGS
{
    D3DVBLEND_DISABLE  = 0, // Disable vertex blending
    D3DVBLEND_1WEIGHT  = 1, // blend between 2 matrices
    D3DVBLEND_2WEIGHTS = 2, // blend between 3 matrices
    D3DVBLEND_3WEIGHTS = 3, // blend between 4 matrices
} D3DVERTEXBLENDFLAGS;

typedef enum _D3DTEXTURETRANSFORMFLAGS {
    D3DTTFF_DISABLE         = 0,    // texture coordinates are passed directly
    D3DTTFF_COUNT1          = 1,    // rasterizer should expect 1-D texture coords
    D3DTTFF_COUNT2          = 2,    // rasterizer should expect 2-D texture coords
    D3DTTFF_COUNT3          = 3,    // rasterizer should expect 3-D texture coords
    D3DTTFF_COUNT4          = 4,    // rasterizer should expect 4-D texture coords
    D3DTTFF_PROJECTED       = 256,  // texcoords to be divided by COUNTth element
    D3DTTFF_FORCE_DWORD     = 0x7fffffff,
} D3DTEXTURETRANSFORMFLAGS;

// Macros to set texture coordinate format bits in the FVF id

#define D3DFVF_TEXTUREFORMAT2 0         // Two floating point values
#define D3DFVF_TEXTUREFORMAT1 3         // One floating point value
#define D3DFVF_TEXTUREFORMAT3 1         // Three floating point values
#define D3DFVF_TEXTUREFORMAT4 2         // Four floating point values

#define D3DFVF_TEXCOORDSIZE3(CoordIndex) (D3DFVF_TEXTUREFORMAT3 << (CoordIndex*2 + 16))
#define D3DFVF_TEXCOORDSIZE2(CoordIndex) (D3DFVF_TEXTUREFORMAT2)
#define D3DFVF_TEXCOORDSIZE4(CoordIndex) (D3DFVF_TEXTUREFORMAT4 << (CoordIndex*2 + 16))
#define D3DFVF_TEXCOORDSIZE1(CoordIndex) (D3DFVF_TEXTUREFORMAT1 << (CoordIndex*2 + 16))


#endif /* DIRECT3D_VERSION >= 0x0700 */

#else
//
// legacy vertex blend names
//
typedef enum _D3DVERTEXBLENDFLAGS D3DVERTEXBLENDFLAGS;
#define D3DVBLEND_DISABLE  (D3DVERTEXBLENDFLAGS)0
#define D3DVBLEND_1WEIGHT  (D3DVERTEXBLENDFLAGS)1
#define D3DVBLEND_2WEIGHTS (D3DVERTEXBLENDFLAGS)2
#define D3DVBLEND_3WEIGHTS (D3DVERTEXBLENDFLAGS)3

#endif //(DIRECT3D_VERSION < 0x0800)

#pragma pack()
#pragma warning(default:4201)

#endif /* _D3DTYPES_H_ */

#ifdef WIN32
#define D3DRMAPI  __stdcall
#else
#define D3DRMAPI
#endif

#if defined(__cplusplus)
extern "C" {
#endif

#ifndef TRUE
#define FALSE 0
#define TRUE 1
#endif

typedef struct _D3DRMVECTOR4D
{   D3DVALUE x, y, z, w;
} D3DRMVECTOR4D, *LPD3DRMVECTOR4D;

typedef D3DVALUE D3DRMMATRIX4D[4][4];

typedef struct _D3DRMQUATERNION
{   D3DVALUE s;
    D3DVECTOR v;
} D3DRMQUATERNION, *LPD3DRMQUATERNION;

typedef struct _D3DRMRAY
{   D3DVECTOR dvDir;
    D3DVECTOR dvPos;
} D3DRMRAY, *LPD3DRMRAY;

typedef struct _D3DRMBOX
{   D3DVECTOR min, max;
} D3DRMBOX, *LPD3DRMBOX;

typedef void (*D3DRMWRAPCALLBACK)
    (LPD3DVECTOR, int* u, int* v, LPD3DVECTOR a, LPD3DVECTOR b, LPVOID);

typedef enum _D3DRMLIGHTTYPE
{   D3DRMLIGHT_AMBIENT,
    D3DRMLIGHT_POINT,
    D3DRMLIGHT_SPOT,
    D3DRMLIGHT_DIRECTIONAL,
    D3DRMLIGHT_PARALLELPOINT
} D3DRMLIGHTTYPE, *LPD3DRMLIGHTTYPE;

typedef enum _D3DRMSHADEMODE {
    D3DRMSHADE_FLAT	= 0,
    D3DRMSHADE_GOURAUD	= 1,
    D3DRMSHADE_PHONG	= 2,

    D3DRMSHADE_MASK	= 7,
    D3DRMSHADE_MAX	= 8
} D3DRMSHADEMODE, *LPD3DRMSHADEMODE;

typedef enum _D3DRMLIGHTMODE {
    D3DRMLIGHT_OFF	= 0 * D3DRMSHADE_MAX,
    D3DRMLIGHT_ON	= 1 * D3DRMSHADE_MAX,

    D3DRMLIGHT_MASK	= 7 * D3DRMSHADE_MAX,
    D3DRMLIGHT_MAX	= 8 * D3DRMSHADE_MAX
} D3DRMLIGHTMODE, *LPD3DRMLIGHTMODE;

typedef enum _D3DRMFILLMODE {
    D3DRMFILL_POINTS	= 0 * D3DRMLIGHT_MAX,
    D3DRMFILL_WIREFRAME	= 1 * D3DRMLIGHT_MAX,
    D3DRMFILL_SOLID	= 2 * D3DRMLIGHT_MAX,

    D3DRMFILL_MASK	= 7 * D3DRMLIGHT_MAX,
    D3DRMFILL_MAX	= 8 * D3DRMLIGHT_MAX
} D3DRMFILLMODE, *LPD3DRMFILLMODE;

typedef DWORD D3DRMRENDERQUALITY, *LPD3DRMRENDERQUALITY;

#define D3DRMRENDER_WIREFRAME	(D3DRMSHADE_FLAT+D3DRMLIGHT_OFF+D3DRMFILL_WIREFRAME)
#define D3DRMRENDER_UNLITFLAT	(D3DRMSHADE_FLAT+D3DRMLIGHT_OFF+D3DRMFILL_SOLID)
#define D3DRMRENDER_FLAT	(D3DRMSHADE_FLAT+D3DRMLIGHT_ON+D3DRMFILL_SOLID)
#define D3DRMRENDER_GOURAUD	(D3DRMSHADE_GOURAUD+D3DRMLIGHT_ON+D3DRMFILL_SOLID)
#define D3DRMRENDER_PHONG	(D3DRMSHADE_PHONG+D3DRMLIGHT_ON+D3DRMFILL_SOLID)

#define D3DRMRENDERMODE_BLENDEDTRANSPARENCY	1
#define D3DRMRENDERMODE_SORTEDTRANSPARENCY	2
#define D3DRMRENDERMODE_LIGHTINMODELSPACE	8
#define D3DRMRENDERMODE_VIEWDEPENDENTSPECULAR	16
#define D3DRMRENDERMODE_DISABLESORTEDALPHAZWRITE 32

typedef enum _D3DRMTEXTUREQUALITY
{   D3DRMTEXTURE_NEAREST,		/* choose nearest texel */
    D3DRMTEXTURE_LINEAR,		/* interpolate 4 texels */
    D3DRMTEXTURE_MIPNEAREST,		/* nearest texel in nearest mipmap  */
    D3DRMTEXTURE_MIPLINEAR,		/* interpolate 2 texels from 2 mipmaps */
    D3DRMTEXTURE_LINEARMIPNEAREST,	/* interpolate 4 texels in nearest mipmap */
    D3DRMTEXTURE_LINEARMIPLINEAR	/* interpolate 8 texels from 2 mipmaps */
} D3DRMTEXTUREQUALITY, *LPD3DRMTEXTUREQUALITY;

/*
 * Texture flags
 */
#define D3DRMTEXTURE_FORCERESIDENT	    0x00000001 /* texture should be kept in video memory */
#define D3DRMTEXTURE_STATIC		    0x00000002 /* texture will not change */
#define D3DRMTEXTURE_DOWNSAMPLEPOINT	    0x00000004 /* point filtering should be used when downsampling */
#define D3DRMTEXTURE_DOWNSAMPLEBILINEAR	    0x00000008 /* bilinear filtering should be used when downsampling */
#define D3DRMTEXTURE_DOWNSAMPLEREDUCEDEPTH  0x00000010 /* reduce bit depth when downsampling */
#define D3DRMTEXTURE_DOWNSAMPLENONE	    0x00000020 /* texture should never be downsampled */
#define D3DRMTEXTURE_CHANGEDPIXELS	    0x00000040 /* pixels have changed */
#define D3DRMTEXTURE_CHANGEDPALETTE	    0x00000080 /* palette has changed */
#define D3DRMTEXTURE_INVALIDATEONLY	    0x00000100 /* dirty regions are invalid */

/*
 * Shadow flags
 */
#define D3DRMSHADOW_TRUEALPHA		    0x00000001 /* shadow should render without artifacts when true alpha is on */

typedef enum _D3DRMCOMBINETYPE
{   D3DRMCOMBINE_REPLACE,
    D3DRMCOMBINE_BEFORE,
    D3DRMCOMBINE_AFTER
} D3DRMCOMBINETYPE, *LPD3DRMCOMBINETYPE;

typedef D3DCOLORMODEL D3DRMCOLORMODEL, *LPD3DRMCOLORMODEL;

typedef enum _D3DRMPALETTEFLAGS
{   D3DRMPALETTE_FREE,			/* renderer may use this entry freely */
    D3DRMPALETTE_READONLY,		/* fixed but may be used by renderer */
    D3DRMPALETTE_RESERVED		/* may not be used by renderer */
} D3DRMPALETTEFLAGS, *LPD3DRMPALETTEFLAGS;

typedef struct _D3DRMPALETTEENTRY
{   unsigned char red;		/* 0 .. 255 */
    unsigned char green;	/* 0 .. 255 */
    unsigned char blue;		/* 0 .. 255 */
    unsigned char flags;	/* one of D3DRMPALETTEFLAGS */
} D3DRMPALETTEENTRY, *LPD3DRMPALETTEENTRY;

typedef struct _D3DRMIMAGE
{   int width, height;		/* width and height in pixels */
    int aspectx, aspecty;	/* aspect ratio for non-square pixels */
    int depth;			/* bits per pixel */
    int rgb;			/* if false, pixels are indices into a
				   palette otherwise, pixels encode
				   RGB values. */
    int bytes_per_line;		/* number of bytes of memory for a
				   scanline. This must be a multiple
				   of 4. */
    void* buffer1;		/* memory to render into (first buffer). */
    void* buffer2;		/* second rendering buffer for double
				   buffering, set to NULL for single
				   buffering. */
    unsigned long red_mask;
    unsigned long green_mask;
    unsigned long blue_mask;
    unsigned long alpha_mask;	/* if rgb is true, these are masks for
				   the red, green and blue parts of a
				   pixel.  Otherwise, these are masks
				   for the significant bits of the
				   red, green and blue elements in the
				   palette.  For instance, most SVGA
				   displays use 64 intensities of red,
				   green and blue, so the masks should
				   all be set to 0xfc. */
    int palette_size;           /* number of entries in palette */
    D3DRMPALETTEENTRY* palette;	/* description of the palette (only if
				   rgb is false).  Must be (1<<depth)
				   elements. */
} D3DRMIMAGE, *LPD3DRMIMAGE;

typedef enum _D3DRMWRAPTYPE
{   D3DRMWRAP_FLAT,
    D3DRMWRAP_CYLINDER,
    D3DRMWRAP_SPHERE,
    D3DRMWRAP_CHROME,
    D3DRMWRAP_SHEET,
    D3DRMWRAP_BOX
} D3DRMWRAPTYPE, *LPD3DRMWRAPTYPE;

#define D3DRMWIREFRAME_CULL		1 /* cull backfaces */
#define D3DRMWIREFRAME_HIDDENLINE	2 /* lines are obscured by closer objects */

/*
 * Do not use righthanded perspective in Viewport2::SetProjection().
 * Set up righthanded mode by using IDirect3DRM3::SetOptions().
 */
typedef enum _D3DRMPROJECTIONTYPE
{   D3DRMPROJECT_PERSPECTIVE,
    D3DRMPROJECT_ORTHOGRAPHIC,
    D3DRMPROJECT_RIGHTHANDPERSPECTIVE, /* Only valid pre-DX6 */
    D3DRMPROJECT_RIGHTHANDORTHOGRAPHIC /* Only valid pre-DX6 */
} D3DRMPROJECTIONTYPE, *LPD3DRMPROJECTIONTYPE;

#define D3DRMOPTIONS_LEFTHANDED  0x00000001L /* Default */
#define D3DRMOPTIONS_RIGHTHANDED 0x00000002L

typedef enum _D3DRMXOFFORMAT
{   D3DRMXOF_BINARY,
    D3DRMXOF_COMPRESSED,
    D3DRMXOF_TEXT
} D3DRMXOFFORMAT, *LPD3DRMXOFFORMAT;

typedef DWORD D3DRMSAVEOPTIONS;
#define D3DRMXOFSAVE_NORMALS 1
#define D3DRMXOFSAVE_TEXTURECOORDINATES 2
#define D3DRMXOFSAVE_MATERIALS 4
#define D3DRMXOFSAVE_TEXTURENAMES 8
#define D3DRMXOFSAVE_ALL 15
#define D3DRMXOFSAVE_TEMPLATES 16
#define D3DRMXOFSAVE_TEXTURETOPOLOGY 32

typedef enum _D3DRMCOLORSOURCE
{   D3DRMCOLOR_FROMFACE,
    D3DRMCOLOR_FROMVERTEX
} D3DRMCOLORSOURCE, *LPD3DRMCOLORSOURCE;

typedef enum _D3DRMFRAMECONSTRAINT
{   D3DRMCONSTRAIN_Z,		/* use only X and Y rotations */
    D3DRMCONSTRAIN_Y,		/* use only X and Z rotations */
    D3DRMCONSTRAIN_X		/* use only Y and Z rotations */
} D3DRMFRAMECONSTRAINT, *LPD3DRMFRAMECONSTRAINT;

typedef enum _D3DRMMATERIALMODE
{   D3DRMMATERIAL_FROMMESH,
    D3DRMMATERIAL_FROMPARENT,
    D3DRMMATERIAL_FROMFRAME
} D3DRMMATERIALMODE, *LPD3DRMMATERIALMODE;

typedef enum _D3DRMFOGMODE
{   D3DRMFOG_LINEAR,		/* linear between start and end */
    D3DRMFOG_EXPONENTIAL,	/* density * exp(-distance) */
    D3DRMFOG_EXPONENTIALSQUARED	/* density * exp(-distance*distance) */
} D3DRMFOGMODE, *LPD3DRMFOGMODE;

typedef enum _D3DRMZBUFFERMODE {
    D3DRMZBUFFER_FROMPARENT,	/* default */
    D3DRMZBUFFER_ENABLE,	/* enable zbuffering */
    D3DRMZBUFFER_DISABLE	/* disable zbuffering */
} D3DRMZBUFFERMODE, *LPD3DRMZBUFFERMODE;

typedef enum _D3DRMSORTMODE {
    D3DRMSORT_FROMPARENT,	/* default */
    D3DRMSORT_NONE,		/* don't sort child frames */
    D3DRMSORT_FRONTTOBACK,	/* sort child frames front-to-back */
    D3DRMSORT_BACKTOFRONT	/* sort child frames back-to-front */
} D3DRMSORTMODE, *LPD3DRMSORTMODE;

typedef struct _D3DRMMATERIALOVERRIDE
{
    DWORD         dwSize;	/* Size of this structure */
    DWORD         dwFlags;	/* Indicate which fields are valid */
    D3DCOLORVALUE dcDiffuse;	/* RGBA */
    D3DCOLORVALUE dcAmbient;	/* RGB */
    D3DCOLORVALUE dcEmissive;	/* RGB */
    D3DCOLORVALUE dcSpecular;	/* RGB */
    D3DVALUE      dvPower;
    LPUNKNOWN     lpD3DRMTex;
} D3DRMMATERIALOVERRIDE, *LPD3DRMMATERIALOVERRIDE;

#define D3DRMMATERIALOVERRIDE_DIFFUSE_ALPHAONLY     0x00000001L
#define D3DRMMATERIALOVERRIDE_DIFFUSE_RGBONLY       0x00000002L
#define D3DRMMATERIALOVERRIDE_DIFFUSE               0x00000003L
#define D3DRMMATERIALOVERRIDE_AMBIENT               0x00000004L
#define D3DRMMATERIALOVERRIDE_EMISSIVE              0x00000008L
#define D3DRMMATERIALOVERRIDE_SPECULAR              0x00000010L
#define D3DRMMATERIALOVERRIDE_POWER                 0x00000020L
#define D3DRMMATERIALOVERRIDE_TEXTURE               0x00000040L
#define D3DRMMATERIALOVERRIDE_DIFFUSE_ALPHAMULTIPLY 0x00000080L
#define D3DRMMATERIALOVERRIDE_ALL                   0x000000FFL

#define D3DRMFPTF_ALPHA                           0x00000001L
#define D3DRMFPTF_NOALPHA                         0x00000002L
#define D3DRMFPTF_PALETTIZED                      0x00000004L
#define D3DRMFPTF_NOTPALETTIZED                   0x00000008L

#define D3DRMSTATECHANGE_UPDATEONLY		  0x000000001L
#define D3DRMSTATECHANGE_VOLATILE		  0x000000002L
#define D3DRMSTATECHANGE_NONVOLATILE		  0x000000004L
#define D3DRMSTATECHANGE_RENDER			  0x000000020L
#define D3DRMSTATECHANGE_LIGHT			  0x000000040L

/*
 * Values for flags in RM3::CreateDeviceFromSurface
 */
#define D3DRMDEVICE_NOZBUFFER           0x00000001L

/*
 * Values for flags in Object2::SetClientData
 */
#define D3DRMCLIENTDATA_NONE            0x00000001L
#define D3DRMCLIENTDATA_LOCALFREE       0x00000002L
#define D3DRMCLIENTDATA_IUNKNOWN        0x00000004L

/*
 * Values for flags in Frame2::AddMoveCallback.
 */
#define D3DRMCALLBACK_PREORDER		0
#define D3DRMCALLBACK_POSTORDER		1

/*
 * Values for flags in MeshBuilder2::RayPick.
 */
#define D3DRMRAYPICK_ONLYBOUNDINGBOXES		1
#define D3DRMRAYPICK_IGNOREFURTHERPRIMITIVES	2
#define D3DRMRAYPICK_INTERPOLATEUV		4
#define D3DRMRAYPICK_INTERPOLATECOLOR		8
#define D3DRMRAYPICK_INTERPOLATENORMAL		0x10	

/*
 * Values for flags in MeshBuilder3::AddFacesIndexed.
 */
#define D3DRMADDFACES_VERTICESONLY		1

/*
 * Values for flags in MeshBuilder2::GenerateNormals.
 */
#define D3DRMGENERATENORMALS_PRECOMPACT		1
#define D3DRMGENERATENORMALS_USECREASEANGLE	2

/*
 * Values for MeshBuilder3::GetParentMesh
 */
#define D3DRMMESHBUILDER_DIRECTPARENT		1
#define D3DRMMESHBUILDER_ROOTMESH		2

/*
 * Flags for MeshBuilder3::Enable
 */
#define D3DRMMESHBUILDER_RENDERENABLE	0x00000001L
#define D3DRMMESHBUILDER_PICKENABLE     0x00000002L

/*
 * Flags for MeshBuilder3::AddMeshBuilder
 */
#define D3DRMADDMESHBUILDER_DONTCOPYAPPDATA	1
#define D3DRMADDMESHBUILDER_FLATTENSUBMESHES	2
#define D3DRMADDMESHBUILDER_NOSUBMESHES		4

/*
 * Flags for Object2::GetAge when used with MeshBuilders
 */
#define D3DRMMESHBUILDERAGE_GEOMETRY    0x00000001L
#define D3DRMMESHBUILDERAGE_MATERIALS   0x00000002L
#define D3DRMMESHBUILDERAGE_TEXTURES    0x00000004L

/*
 * Format flags for MeshBuilder3::AddTriangles.
 */
#define D3DRMFVF_TYPE                   0x00000001L
#define D3DRMFVF_NORMAL                 0x00000002L
#define D3DRMFVF_COLOR                  0x00000004L
#define D3DRMFVF_TEXTURECOORDS          0x00000008L

#define D3DRMVERTEX_STRIP               0x00000001L
#define D3DRMVERTEX_FAN                 0x00000002L
#define D3DRMVERTEX_LIST                0x00000004L

/*
 * Values for flags in Viewport2::Clear2
 */
#define D3DRMCLEAR_TARGET               0x00000001L
#define D3DRMCLEAR_ZBUFFER              0x00000002L
#define D3DRMCLEAR_DIRTYRECTS           0x00000004L
#define D3DRMCLEAR_ALL                  (D3DRMCLEAR_TARGET | \
					 D3DRMCLEAR_ZBUFFER | \
					 D3DRMCLEAR_DIRTYRECTS)

/*
 * Values for flags in Frame3::SetSceneFogMethod
 */
#define D3DRMFOGMETHOD_VERTEX          0x00000001L
#define D3DRMFOGMETHOD_TABLE           0x00000002L
#define D3DRMFOGMETHOD_ANY             0x00000004L

/*
 * Values for flags in Frame3::SetTraversalOptions
 */
#define D3DRMFRAME_RENDERENABLE        0x00000001L
#define D3DRMFRAME_PICKENABLE          0x00000002L

typedef DWORD D3DRMANIMATIONOPTIONS;
#define D3DRMANIMATION_OPEN 0x01L
#define D3DRMANIMATION_CLOSED 0x02L
#define D3DRMANIMATION_LINEARPOSITION 0x04L
#define D3DRMANIMATION_SPLINEPOSITION 0x08L
#define D3DRMANIMATION_SCALEANDROTATION 0x00000010L
#define D3DRMANIMATION_POSITION 0x00000020L

typedef DWORD D3DRMINTERPOLATIONOPTIONS;
#define D3DRMINTERPOLATION_OPEN 0x01L
#define D3DRMINTERPOLATION_CLOSED 0x02L
#define D3DRMINTERPOLATION_NEAREST 0x0100L
#define D3DRMINTERPOLATION_LINEAR 0x04L
#define D3DRMINTERPOLATION_SPLINE 0x08L
#define D3DRMINTERPOLATION_VERTEXCOLOR 0x40L
#define D3DRMINTERPOLATION_SLERPNORMALS 0x80L

typedef DWORD D3DRMLOADOPTIONS;

#define D3DRMLOAD_FROMFILE  0x00L
#define D3DRMLOAD_FROMRESOURCE 0x01L
#define D3DRMLOAD_FROMMEMORY 0x02L
#define D3DRMLOAD_FROMSTREAM 0x04L
#define D3DRMLOAD_FROMURL 0x08L

#define D3DRMLOAD_BYNAME 0x10L
#define D3DRMLOAD_BYPOSITION 0x20L
#define D3DRMLOAD_BYGUID 0x40L
#define D3DRMLOAD_FIRST 0x80L

#define D3DRMLOAD_INSTANCEBYREFERENCE 0x100L
#define D3DRMLOAD_INSTANCEBYCOPYING 0x200L

#define D3DRMLOAD_ASYNCHRONOUS 0x400L

typedef struct _D3DRMLOADRESOURCE {
  HMODULE hModule;
  LPCTSTR lpName;
  LPCTSTR lpType;
} D3DRMLOADRESOURCE, *LPD3DRMLOADRESOURCE;

typedef struct _D3DRMLOADMEMORY {
  LPVOID lpMemory;
  DWORD dSize;
} D3DRMLOADMEMORY, *LPD3DRMLOADMEMORY;

#define D3DRMPMESHSTATUS_VALID 0x01L
#define D3DRMPMESHSTATUS_INTERRUPTED 0x02L
#define D3DRMPMESHSTATUS_BASEMESHCOMPLETE 0x04L
#define D3DRMPMESHSTATUS_COMPLETE 0x08L
#define D3DRMPMESHSTATUS_RENDERABLE 0x10L

#define D3DRMPMESHEVENT_BASEMESH 0x01L
#define D3DRMPMESHEVENT_COMPLETE 0x02L

typedef struct _D3DRMPMESHLOADSTATUS {
  DWORD dwSize;            // Size of this structure
  DWORD dwPMeshSize;       // Total Size (bytes)
  DWORD dwBaseMeshSize;    // Total Size of the Base Mesh
  DWORD dwBytesLoaded;     // Total bytes loaded
  DWORD dwVerticesLoaded;  // Number of vertices loaded
  DWORD dwFacesLoaded;     // Number of faces loaded
  HRESULT dwLoadResult;    // Result of the load operation
  DWORD dwFlags;
} D3DRMPMESHLOADSTATUS, *LPD3DRMPMESHLOADSTATUS;

typedef enum _D3DRMUSERVISUALREASON {
    D3DRMUSERVISUAL_CANSEE,
    D3DRMUSERVISUAL_RENDER
} D3DRMUSERVISUALREASON, *LPD3DRMUSERVISUALREASON;


typedef struct _D3DRMANIMATIONKEY 
{
    DWORD dwSize;
    DWORD dwKeyType;
    D3DVALUE dvTime;
    DWORD dwID;
#if (!defined __cplusplus) || (!defined D3D_OVERLOADS)
    union 
    {
	D3DRMQUATERNION dqRotateKey;
	D3DVECTOR dvScaleKey;
	D3DVECTOR dvPositionKey;
    };
#else
    /*
     * We do this as D3D_OVERLOADS defines constructors for D3DVECTOR,
     * this can then not be used in a union.  Use the inlines provided
     * to extract and set the required component.
     */
    D3DVALUE dvK[4];
#endif
} D3DRMANIMATIONKEY;
typedef D3DRMANIMATIONKEY *LPD3DRMANIMATIONKEY;

#if (defined __cplusplus) && (defined D3D_OVERLOADS)
inline VOID
D3DRMAnimationGetRotateKey(const D3DRMANIMATIONKEY& rmKey,
			   D3DRMQUATERNION& rmQuat)
{
    rmQuat.s = rmKey.dvK[0];
    rmQuat.v = D3DVECTOR(rmKey.dvK[1], rmKey.dvK[2], rmKey.dvK[3]);
}

inline VOID
D3DRMAnimationGetScaleKey(const D3DRMANIMATIONKEY& rmKey,
			  D3DVECTOR& dvVec)
{
    dvVec = D3DVECTOR(rmKey.dvK[0], rmKey.dvK[1], rmKey.dvK[2]);
}

inline VOID
D3DRMAnimationGetPositionKey(const D3DRMANIMATIONKEY& rmKey,
			     D3DVECTOR& dvVec)
{
    dvVec = D3DVECTOR(rmKey.dvK[0], rmKey.dvK[1], rmKey.dvK[2]);
}
inline VOID
D3DRMAnimationSetRotateKey(D3DRMANIMATIONKEY& rmKey,
			   const D3DRMQUATERNION& rmQuat)
{
    rmKey.dvK[0] = rmQuat.s;
    rmKey.dvK[1] = rmQuat.v.x;
    rmKey.dvK[2] = rmQuat.v.y;
    rmKey.dvK[3] = rmQuat.v.z;
}

inline VOID
D3DRMAnimationSetScaleKey(D3DRMANIMATIONKEY& rmKey,
			  const D3DVECTOR& dvVec)
{
    rmKey.dvK[0] = dvVec.x;
    rmKey.dvK[1] = dvVec.y;
    rmKey.dvK[2] = dvVec.z;
}

inline VOID
D3DRMAnimationSetPositionKey(D3DRMANIMATIONKEY& rmKey,
			     const D3DVECTOR& dvVec)
{
    rmKey.dvK[0] = dvVec.x;
    rmKey.dvK[1] = dvVec.y;
    rmKey.dvK[2] = dvVec.z;
}
#endif

#define D3DRMANIMATION_ROTATEKEY 0x01
#define D3DRMANIMATION_SCALEKEY 0x02
#define D3DRMANIMATION_POSITIONKEY 0x03


typedef DWORD D3DRMMAPPING, D3DRMMAPPINGFLAG, *LPD3DRMMAPPING;
static const D3DRMMAPPINGFLAG D3DRMMAP_WRAPU = 1;
static const D3DRMMAPPINGFLAG D3DRMMAP_WRAPV = 2;
static const D3DRMMAPPINGFLAG D3DRMMAP_PERSPCORRECT = 4;

typedef struct _D3DRMVERTEX
{   D3DVECTOR	    position;
    D3DVECTOR	    normal;
    D3DVALUE	    tu, tv;
    D3DCOLOR	    color;
} D3DRMVERTEX, *LPD3DRMVERTEX;

typedef LONG D3DRMGROUPINDEX; /* group indexes begin a 0 */
static const D3DRMGROUPINDEX D3DRMGROUP_ALLGROUPS = -1;

/*
 * Create a color from three components in the range 0-1 inclusive.
 */
extern D3DCOLOR D3DRMAPI	D3DRMCreateColorRGB(D3DVALUE red,
					  D3DVALUE green,
					  D3DVALUE blue);

/*
 * Create a color from four components in the range 0-1 inclusive.
 */
extern D3DCOLOR D3DRMAPI	D3DRMCreateColorRGBA(D3DVALUE red,
						 D3DVALUE green,
						 D3DVALUE blue,
						 D3DVALUE alpha);

/*
 * Get the red component of a color.
 */
extern D3DVALUE 		D3DRMAPI D3DRMColorGetRed(D3DCOLOR);

/*
 * Get the green component of a color.
 */
extern D3DVALUE 		D3DRMAPI D3DRMColorGetGreen(D3DCOLOR);

/*
 * Get the blue component of a color.
 */
extern D3DVALUE 		D3DRMAPI D3DRMColorGetBlue(D3DCOLOR);

/*
 * Get the alpha component of a color.
 */
extern D3DVALUE 		D3DRMAPI D3DRMColorGetAlpha(D3DCOLOR);

/*
 * Add two vectors.  Returns its first argument.
 */
extern LPD3DVECTOR 	D3DRMAPI D3DRMVectorAdd(LPD3DVECTOR d,
					  LPD3DVECTOR s1,
					  LPD3DVECTOR s2);

/*
 * Subtract two vectors.  Returns its first argument.
 */
extern LPD3DVECTOR 	D3DRMAPI D3DRMVectorSubtract(LPD3DVECTOR d,
					       LPD3DVECTOR s1,
					       LPD3DVECTOR s2);
/*
 * Reflect a ray about a given normal.  Returns its first argument.
 */
extern LPD3DVECTOR 	D3DRMAPI D3DRMVectorReflect(LPD3DVECTOR d,
					      LPD3DVECTOR ray,
					      LPD3DVECTOR norm);

/*
 * Calculate the vector cross product.  Returns its first argument.
 */
extern LPD3DVECTOR 	D3DRMAPI D3DRMVectorCrossProduct(LPD3DVECTOR d,
						   LPD3DVECTOR s1,
						   LPD3DVECTOR s2);
/*
 * Return the vector dot product.
 */
extern D3DVALUE 		D3DRMAPI D3DRMVectorDotProduct(LPD3DVECTOR s1,
						 LPD3DVECTOR s2);

/*
 * Scale a vector so that its modulus is 1.  Returns its argument or
 * NULL if there was an error (e.g. a zero vector was passed).
 */
extern LPD3DVECTOR 	D3DRMAPI D3DRMVectorNormalize(LPD3DVECTOR);
#define D3DRMVectorNormalise D3DRMVectorNormalize

/*
 * Return the length of a vector (e.g. sqrt(x*x + y*y + z*z)).
 */
extern D3DVALUE 		D3DRMAPI D3DRMVectorModulus(LPD3DVECTOR v);

/*
 * Set the rotation part of a matrix to be a rotation of theta radians
 * around the given axis.
 */

extern LPD3DVECTOR 	D3DRMAPI D3DRMVectorRotate(LPD3DVECTOR r, LPD3DVECTOR v, LPD3DVECTOR axis, D3DVALUE theta);

/*
 * Scale a vector uniformly in all three axes
 */
extern LPD3DVECTOR	D3DRMAPI D3DRMVectorScale(LPD3DVECTOR d, LPD3DVECTOR s, D3DVALUE factor);

/*
 * Return a random unit vector
 */
extern LPD3DVECTOR	D3DRMAPI D3DRMVectorRandom(LPD3DVECTOR d);

/*
 * Returns a unit quaternion that represents a rotation of theta radians
 * around the given axis.
 */

extern LPD3DRMQUATERNION D3DRMAPI D3DRMQuaternionFromRotation(LPD3DRMQUATERNION quat,
							      LPD3DVECTOR v,
							      D3DVALUE theta);

/*
 * Calculate the product of two quaternions
 */
extern LPD3DRMQUATERNION D3DRMAPI D3DRMQuaternionMultiply(LPD3DRMQUATERNION q,
						    	  LPD3DRMQUATERNION a,
						   	  LPD3DRMQUATERNION b);

/*
 * Interpolate between two quaternions
 */
extern LPD3DRMQUATERNION D3DRMAPI D3DRMQuaternionSlerp(LPD3DRMQUATERNION q,
						       LPD3DRMQUATERNION a,
						       LPD3DRMQUATERNION b,
						       D3DVALUE alpha);

/*
 * Calculate the matrix for the rotation that a unit quaternion represents
 */
extern void 		D3DRMAPI D3DRMMatrixFromQuaternion(D3DRMMATRIX4D dmMat, LPD3DRMQUATERNION lpDqQuat);

/*
 * Calculate the quaternion that corresponds to a rotation matrix
 */
extern LPD3DRMQUATERNION D3DRMAPI D3DRMQuaternionFromMatrix(LPD3DRMQUATERNION, D3DRMMATRIX4D);


#if defined(__cplusplus)
};
#endif

#endif

//#include "d3d.h"
#ifndef _D3D_H_
#define _D3D_H_

#ifndef DIRECT3D_VERSION
#define DIRECT3D_VERSION         0x0700
#endif

// include this file content only if compiling for <=DX7 interfaces
#if(DIRECT3D_VERSION < 0x0800)


#include <stdlib.h>

#define COM_NO_WINDOWS_H
#include <objbase.h>

#define D3DAPI WINAPI

/*
 * Interface IID's
 */
#if defined( _WIN32 ) && !defined( _NO_COM)
DEFINE_GUID( IID_IDirect3D,             0x3BBA0080,0x2421,0x11CF,0xA3,0x1A,0x00,0xAA,0x00,0xB9,0x33,0x56 );
#if(DIRECT3D_VERSION >= 0x0500)
DEFINE_GUID( IID_IDirect3D2,            0x6aae1ec1,0x662a,0x11d0,0x88,0x9d,0x00,0xaa,0x00,0xbb,0xb7,0x6a);
#endif /* DIRECT3D_VERSION >= 0x0500 */
#if(DIRECT3D_VERSION >= 0x0600)
DEFINE_GUID( IID_IDirect3D3,            0xbb223240,0xe72b,0x11d0,0xa9,0xb4,0x00,0xaa,0x00,0xc0,0x99,0x3e);
#endif /* DIRECT3D_VERSION >= 0x0600 */
#if(DIRECT3D_VERSION >= 0x0700)
DEFINE_GUID( IID_IDirect3D7,            0xf5049e77,0x4861,0x11d2,0xa4,0x7,0x0,0xa0,0xc9,0x6,0x29,0xa8);
#endif /* DIRECT3D_VERSION >= 0x0700 */

#if(DIRECT3D_VERSION >= 0x0500)
DEFINE_GUID( IID_IDirect3DRampDevice,   0xF2086B20,0x259F,0x11CF,0xA3,0x1A,0x00,0xAA,0x00,0xB9,0x33,0x56 );
DEFINE_GUID( IID_IDirect3DRGBDevice,    0xA4665C60,0x2673,0x11CF,0xA3,0x1A,0x00,0xAA,0x00,0xB9,0x33,0x56 );
DEFINE_GUID( IID_IDirect3DHALDevice,    0x84E63dE0,0x46AA,0x11CF,0x81,0x6F,0x00,0x00,0xC0,0x20,0x15,0x6E );
DEFINE_GUID( IID_IDirect3DMMXDevice,    0x881949a1,0xd6f3,0x11d0,0x89,0xab,0x00,0xa0,0xc9,0x05,0x41,0x29 );
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)
DEFINE_GUID( IID_IDirect3DRefDevice,    0x50936643, 0x13e9, 0x11d1, 0x89, 0xaa, 0x0, 0xa0, 0xc9, 0x5, 0x41, 0x29);
DEFINE_GUID( IID_IDirect3DNullDevice, 0x8767df22, 0xbacc, 0x11d1, 0x89, 0x69, 0x0, 0xa0, 0xc9, 0x6, 0x29, 0xa8);
#endif /* DIRECT3D_VERSION >= 0x0600 */
#if(DIRECT3D_VERSION >= 0x0700)
DEFINE_GUID( IID_IDirect3DTnLHalDevice, 0xf5049e78, 0x4861, 0x11d2, 0xa4, 0x7, 0x0, 0xa0, 0xc9, 0x6, 0x29, 0xa8);
#endif /* DIRECT3D_VERSION >= 0x0700 */

/*
 * Internal Guid to distinguish requested MMX from MMX being used as an RGB rasterizer
 */

DEFINE_GUID( IID_IDirect3DDevice,       0x64108800,0x957d,0X11d0,0x89,0xab,0x00,0xa0,0xc9,0x05,0x41,0x29 );
#if(DIRECT3D_VERSION >= 0x0500)
DEFINE_GUID( IID_IDirect3DDevice2,  0x93281501, 0x8cf8, 0x11d0, 0x89, 0xab, 0x0, 0xa0, 0xc9, 0x5, 0x41, 0x29);
#endif /* DIRECT3D_VERSION >= 0x0500 */
#if(DIRECT3D_VERSION >= 0x0600)
DEFINE_GUID( IID_IDirect3DDevice3,  0xb0ab3b60, 0x33d7, 0x11d1, 0xa9, 0x81, 0x0, 0xc0, 0x4f, 0xd7, 0xb1, 0x74);
#endif /* DIRECT3D_VERSION >= 0x0600 */
#if(DIRECT3D_VERSION >= 0x0700)
DEFINE_GUID( IID_IDirect3DDevice7,  0xf5049e79, 0x4861, 0x11d2, 0xa4, 0x7, 0x0, 0xa0, 0xc9, 0x6, 0x29, 0xa8);
#endif /* DIRECT3D_VERSION >= 0x0700 */

DEFINE_GUID( IID_IDirect3DTexture,      0x2CDCD9E0,0x25A0,0x11CF,0xA3,0x1A,0x00,0xAA,0x00,0xB9,0x33,0x56 );
#if(DIRECT3D_VERSION >= 0x0500)
DEFINE_GUID( IID_IDirect3DTexture2, 0x93281502, 0x8cf8, 0x11d0, 0x89, 0xab, 0x0, 0xa0, 0xc9, 0x5, 0x41, 0x29);
#endif /* DIRECT3D_VERSION >= 0x0500 */

DEFINE_GUID( IID_IDirect3DLight,        0x4417C142,0x33AD,0x11CF,0x81,0x6F,0x00,0x00,0xC0,0x20,0x15,0x6E );

DEFINE_GUID( IID_IDirect3DMaterial,     0x4417C144,0x33AD,0x11CF,0x81,0x6F,0x00,0x00,0xC0,0x20,0x15,0x6E );
#if(DIRECT3D_VERSION >= 0x0500)
DEFINE_GUID( IID_IDirect3DMaterial2,    0x93281503, 0x8cf8, 0x11d0, 0x89, 0xab, 0x0, 0xa0, 0xc9, 0x5, 0x41, 0x29);
#endif /* DIRECT3D_VERSION >= 0x0500 */
#if(DIRECT3D_VERSION >= 0x0600)
DEFINE_GUID( IID_IDirect3DMaterial3,    0xca9c46f4, 0xd3c5, 0x11d1, 0xb7, 0x5a, 0x0, 0x60, 0x8, 0x52, 0xb3, 0x12);
#endif /* DIRECT3D_VERSION >= 0x0600 */

DEFINE_GUID( IID_IDirect3DExecuteBuffer,0x4417C145,0x33AD,0x11CF,0x81,0x6F,0x00,0x00,0xC0,0x20,0x15,0x6E );
DEFINE_GUID( IID_IDirect3DViewport,     0x4417C146,0x33AD,0x11CF,0x81,0x6F,0x00,0x00,0xC0,0x20,0x15,0x6E );
#if(DIRECT3D_VERSION >= 0x0500)
DEFINE_GUID( IID_IDirect3DViewport2,    0x93281500, 0x8cf8, 0x11d0, 0x89, 0xab, 0x0, 0xa0, 0xc9, 0x5, 0x41, 0x29);
#endif /* DIRECT3D_VERSION >= 0x0500 */
#if(DIRECT3D_VERSION >= 0x0600)
DEFINE_GUID( IID_IDirect3DViewport3,    0xb0ab3b61, 0x33d7, 0x11d1, 0xa9, 0x81, 0x0, 0xc0, 0x4f, 0xd7, 0xb1, 0x74);
#endif /* DIRECT3D_VERSION >= 0x0600 */
#if(DIRECT3D_VERSION >= 0x0600)
DEFINE_GUID( IID_IDirect3DVertexBuffer, 0x7a503555, 0x4a83, 0x11d1, 0xa5, 0xdb, 0x0, 0xa0, 0xc9, 0x3, 0x67, 0xf8);
#endif /* DIRECT3D_VERSION >= 0x0600 */
#if(DIRECT3D_VERSION >= 0x0700)
DEFINE_GUID( IID_IDirect3DVertexBuffer7, 0xf5049e7d, 0x4861, 0x11d2, 0xa4, 0x7, 0x0, 0xa0, 0xc9, 0x6, 0x29, 0xa8);
#endif /* DIRECT3D_VERSION >= 0x0700 */
#endif

#ifdef __cplusplus
struct IDirect3D;
struct IDirect3DDevice;
struct IDirect3DLight;
struct IDirect3DMaterial;
struct IDirect3DExecuteBuffer;
struct IDirect3DTexture;
struct IDirect3DViewport;
typedef struct IDirect3D            *LPDIRECT3D;
typedef struct IDirect3DDevice      *LPDIRECT3DDEVICE;
typedef struct IDirect3DExecuteBuffer   *LPDIRECT3DEXECUTEBUFFER;
typedef struct IDirect3DLight       *LPDIRECT3DLIGHT;
typedef struct IDirect3DMaterial    *LPDIRECT3DMATERIAL;
typedef struct IDirect3DTexture     *LPDIRECT3DTEXTURE;
typedef struct IDirect3DViewport    *LPDIRECT3DVIEWPORT;

#if(DIRECT3D_VERSION >= 0x0500)
struct IDirect3D2;
struct IDirect3DDevice2;
struct IDirect3DMaterial2;
struct IDirect3DTexture2;
struct IDirect3DViewport2;
typedef struct IDirect3D2           *LPDIRECT3D2;
typedef struct IDirect3DDevice2     *LPDIRECT3DDEVICE2;
typedef struct IDirect3DMaterial2   *LPDIRECT3DMATERIAL2;
typedef struct IDirect3DTexture2    *LPDIRECT3DTEXTURE2;
typedef struct IDirect3DViewport2   *LPDIRECT3DVIEWPORT2;
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)
struct IDirect3D3;
struct IDirect3DDevice3;
struct IDirect3DMaterial3;
struct IDirect3DViewport3;
struct IDirect3DVertexBuffer;
typedef struct IDirect3D3            *LPDIRECT3D3;
typedef struct IDirect3DDevice3      *LPDIRECT3DDEVICE3;
typedef struct IDirect3DMaterial3    *LPDIRECT3DMATERIAL3;
typedef struct IDirect3DViewport3    *LPDIRECT3DVIEWPORT3;
typedef struct IDirect3DVertexBuffer *LPDIRECT3DVERTEXBUFFER;
#endif /* DIRECT3D_VERSION >= 0x0600 */

#if(DIRECT3D_VERSION >= 0x0700)
struct IDirect3D7;
struct IDirect3DDevice7;
struct IDirect3DVertexBuffer7;
typedef struct IDirect3D7             *LPDIRECT3D7;
typedef struct IDirect3DDevice7       *LPDIRECT3DDEVICE7;
typedef struct IDirect3DVertexBuffer7 *LPDIRECT3DVERTEXBUFFER7;
#endif /* DIRECT3D_VERSION >= 0x0700 */

#else

typedef struct IDirect3D        *LPDIRECT3D;
typedef struct IDirect3DDevice      *LPDIRECT3DDEVICE;
typedef struct IDirect3DExecuteBuffer   *LPDIRECT3DEXECUTEBUFFER;
typedef struct IDirect3DLight       *LPDIRECT3DLIGHT;
typedef struct IDirect3DMaterial    *LPDIRECT3DMATERIAL;
typedef struct IDirect3DTexture     *LPDIRECT3DTEXTURE;
typedef struct IDirect3DViewport    *LPDIRECT3DVIEWPORT;

#if(DIRECT3D_VERSION >= 0x0500)
typedef struct IDirect3D2           *LPDIRECT3D2;
typedef struct IDirect3DDevice2     *LPDIRECT3DDEVICE2;
typedef struct IDirect3DMaterial2   *LPDIRECT3DMATERIAL2;
typedef struct IDirect3DTexture2    *LPDIRECT3DTEXTURE2;
typedef struct IDirect3DViewport2   *LPDIRECT3DVIEWPORT2;
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)
typedef struct IDirect3D3            *LPDIRECT3D3;
typedef struct IDirect3DDevice3      *LPDIRECT3DDEVICE3;
typedef struct IDirect3DMaterial3    *LPDIRECT3DMATERIAL3;
typedef struct IDirect3DViewport3    *LPDIRECT3DVIEWPORT3;
typedef struct IDirect3DVertexBuffer *LPDIRECT3DVERTEXBUFFER;
#endif /* DIRECT3D_VERSION >= 0x0600 */

#if(DIRECT3D_VERSION >= 0x0700)
typedef struct IDirect3D7             *LPDIRECT3D7;
typedef struct IDirect3DDevice7       *LPDIRECT3DDEVICE7;
typedef struct IDirect3DVertexBuffer7 *LPDIRECT3DVERTEXBUFFER7;
#endif /* DIRECT3D_VERSION >= 0x0700 */

#endif

//#include "d3dtypes.h"

//#include "d3dcaps.h"
/*==========================================================================;
 *
 *
 *  File:       d3dcaps.h
 *  Content:    Direct3D capabilities include file
 *
 ***************************************************************************/

#ifndef _D3DCAPS_H
#define _D3DCAPS_H

/*
 *  Pull in DirectDraw include file automatically:
 */
#include "ddraw.h"

#ifndef DIRECT3D_VERSION
#define DIRECT3D_VERSION         0x0700
#endif

#pragma pack(4)

/* Description of capabilities of transform */

typedef struct _D3DTRANSFORMCAPS {
    DWORD dwSize;
    DWORD dwCaps;
} D3DTRANSFORMCAPS, *LPD3DTRANSFORMCAPS;

#define D3DTRANSFORMCAPS_CLIP           0x00000001L /* Will clip whilst transforming */

/* Description of capabilities of lighting */

typedef struct _D3DLIGHTINGCAPS {
    DWORD dwSize;
    DWORD dwCaps;                   /* Lighting caps */
    DWORD dwLightingModel;          /* Lighting model - RGB or mono */
    DWORD dwNumLights;              /* Number of lights that can be handled */
} D3DLIGHTINGCAPS, *LPD3DLIGHTINGCAPS;

#define D3DLIGHTINGMODEL_RGB            0x00000001L
#define D3DLIGHTINGMODEL_MONO           0x00000002L

#define D3DLIGHTCAPS_POINT              0x00000001L /* Point lights supported */
#define D3DLIGHTCAPS_SPOT               0x00000002L /* Spot lights supported */
#define D3DLIGHTCAPS_DIRECTIONAL        0x00000004L /* Directional lights supported */
#if(DIRECT3D_VERSION < 0x700)
#define D3DLIGHTCAPS_PARALLELPOINT      0x00000008L /* Parallel point lights supported */
#endif
#if(DIRECT3D_VERSION < 0x500)
#define D3DLIGHTCAPS_GLSPOT             0x00000010L /* GL syle spot lights supported */
#endif

/* Description of capabilities for each primitive type */

typedef struct _D3DPrimCaps {
    DWORD dwSize;
    DWORD dwMiscCaps;                 /* Capability flags */
    DWORD dwRasterCaps;
    DWORD dwZCmpCaps;
    DWORD dwSrcBlendCaps;
    DWORD dwDestBlendCaps;
    DWORD dwAlphaCmpCaps;
    DWORD dwShadeCaps;
    DWORD dwTextureCaps;
    DWORD dwTextureFilterCaps;
    DWORD dwTextureBlendCaps;
    DWORD dwTextureAddressCaps;
    DWORD dwStippleWidth;             /* maximum width and height of */
    DWORD dwStippleHeight;            /* of supported stipple (up to 32x32) */
} D3DPRIMCAPS, *LPD3DPRIMCAPS;

/* D3DPRIMCAPS dwMiscCaps */

#define D3DPMISCCAPS_MASKPLANES         0x00000001L
#define D3DPMISCCAPS_MASKZ              0x00000002L
#define D3DPMISCCAPS_LINEPATTERNREP     0x00000004L
#define D3DPMISCCAPS_CONFORMANT         0x00000008L
#define D3DPMISCCAPS_CULLNONE           0x00000010L
#define D3DPMISCCAPS_CULLCW             0x00000020L
#define D3DPMISCCAPS_CULLCCW            0x00000040L

/* D3DPRIMCAPS dwRasterCaps */

#define D3DPRASTERCAPS_DITHER                   0x00000001L
#define D3DPRASTERCAPS_ROP2                     0x00000002L
#define D3DPRASTERCAPS_XOR                      0x00000004L
#define D3DPRASTERCAPS_PAT                      0x00000008L
#define D3DPRASTERCAPS_ZTEST                    0x00000010L
#define D3DPRASTERCAPS_SUBPIXEL                 0x00000020L
#define D3DPRASTERCAPS_SUBPIXELX                0x00000040L
#define D3DPRASTERCAPS_FOGVERTEX                0x00000080L
#define D3DPRASTERCAPS_FOGTABLE                 0x00000100L
#define D3DPRASTERCAPS_STIPPLE                  0x00000200L
#if(DIRECT3D_VERSION >= 0x0500)
#define D3DPRASTERCAPS_ANTIALIASSORTDEPENDENT   0x00000400L
#define D3DPRASTERCAPS_ANTIALIASSORTINDEPENDENT 0x00000800L
#define D3DPRASTERCAPS_ANTIALIASEDGES           0x00001000L
#define D3DPRASTERCAPS_MIPMAPLODBIAS            0x00002000L
#define D3DPRASTERCAPS_ZBIAS                    0x00004000L
#define D3DPRASTERCAPS_ZBUFFERLESSHSR           0x00008000L
#define D3DPRASTERCAPS_FOGRANGE                 0x00010000L
#define D3DPRASTERCAPS_ANISOTROPY               0x00020000L
#endif /* DIRECT3D_VERSION >= 0x0500 */
#if(DIRECT3D_VERSION >= 0x0600)
#define D3DPRASTERCAPS_WBUFFER                      0x00040000L
#define D3DPRASTERCAPS_TRANSLUCENTSORTINDEPENDENT   0x00080000L
#define D3DPRASTERCAPS_WFOG                         0x00100000L
#define D3DPRASTERCAPS_ZFOG                         0x00200000L
#endif /* DIRECT3D_VERSION >= 0x0600 */

/* D3DPRIMCAPS dwZCmpCaps, dwAlphaCmpCaps */

#define D3DPCMPCAPS_NEVER               0x00000001L
#define D3DPCMPCAPS_LESS                0x00000002L
#define D3DPCMPCAPS_EQUAL               0x00000004L
#define D3DPCMPCAPS_LESSEQUAL           0x00000008L
#define D3DPCMPCAPS_GREATER             0x00000010L
#define D3DPCMPCAPS_NOTEQUAL            0x00000020L
#define D3DPCMPCAPS_GREATEREQUAL        0x00000040L
#define D3DPCMPCAPS_ALWAYS              0x00000080L

/* D3DPRIMCAPS dwSourceBlendCaps, dwDestBlendCaps */

#define D3DPBLENDCAPS_ZERO              0x00000001L
#define D3DPBLENDCAPS_ONE               0x00000002L
#define D3DPBLENDCAPS_SRCCOLOR          0x00000004L
#define D3DPBLENDCAPS_INVSRCCOLOR       0x00000008L
#define D3DPBLENDCAPS_SRCALPHA          0x00000010L
#define D3DPBLENDCAPS_INVSRCALPHA       0x00000020L
#define D3DPBLENDCAPS_DESTALPHA         0x00000040L
#define D3DPBLENDCAPS_INVDESTALPHA      0x00000080L
#define D3DPBLENDCAPS_DESTCOLOR         0x00000100L
#define D3DPBLENDCAPS_INVDESTCOLOR      0x00000200L
#define D3DPBLENDCAPS_SRCALPHASAT       0x00000400L
#define D3DPBLENDCAPS_BOTHSRCALPHA      0x00000800L
#define D3DPBLENDCAPS_BOTHINVSRCALPHA   0x00001000L

/* D3DPRIMCAPS dwShadeCaps */

#define D3DPSHADECAPS_COLORFLATMONO             0x00000001L
#define D3DPSHADECAPS_COLORFLATRGB              0x00000002L
#define D3DPSHADECAPS_COLORGOURAUDMONO          0x00000004L
#define D3DPSHADECAPS_COLORGOURAUDRGB           0x00000008L
#define D3DPSHADECAPS_COLORPHONGMONO            0x00000010L
#define D3DPSHADECAPS_COLORPHONGRGB             0x00000020L

#define D3DPSHADECAPS_SPECULARFLATMONO          0x00000040L
#define D3DPSHADECAPS_SPECULARFLATRGB           0x00000080L
#define D3DPSHADECAPS_SPECULARGOURAUDMONO       0x00000100L
#define D3DPSHADECAPS_SPECULARGOURAUDRGB        0x00000200L
#define D3DPSHADECAPS_SPECULARPHONGMONO         0x00000400L
#define D3DPSHADECAPS_SPECULARPHONGRGB          0x00000800L

#define D3DPSHADECAPS_ALPHAFLATBLEND            0x00001000L
#define D3DPSHADECAPS_ALPHAFLATSTIPPLED         0x00002000L
#define D3DPSHADECAPS_ALPHAGOURAUDBLEND         0x00004000L
#define D3DPSHADECAPS_ALPHAGOURAUDSTIPPLED      0x00008000L
#define D3DPSHADECAPS_ALPHAPHONGBLEND           0x00010000L
#define D3DPSHADECAPS_ALPHAPHONGSTIPPLED        0x00020000L

#define D3DPSHADECAPS_FOGFLAT                   0x00040000L
#define D3DPSHADECAPS_FOGGOURAUD                0x00080000L
#define D3DPSHADECAPS_FOGPHONG                  0x00100000L

/* D3DPRIMCAPS dwTextureCaps */

/*
 * Perspective-correct texturing is supported
 */
#define D3DPTEXTURECAPS_PERSPECTIVE     0x00000001L

/*
 * Power-of-2 texture dimensions are required
 */
#define D3DPTEXTURECAPS_POW2            0x00000002L

/*
 * Alpha in texture pixels is supported
 */
#define D3DPTEXTURECAPS_ALPHA           0x00000004L

/*
 * Color-keyed textures are supported
 */
#define D3DPTEXTURECAPS_TRANSPARENCY    0x00000008L

/*
 * obsolete, see D3DPTADDRESSCAPS_BORDER
 */
#define D3DPTEXTURECAPS_BORDER          0x00000010L

/*
 * Only square textures are supported
 */
#define D3DPTEXTURECAPS_SQUAREONLY      0x00000020L

#if(DIRECT3D_VERSION >= 0x0600)
/*
 * Texture indices are not scaled by the texture size prior
 * to interpolation.
 */
#define D3DPTEXTURECAPS_TEXREPEATNOTSCALEDBYSIZE 0x00000040L

/*
 * Device can draw alpha from texture palettes
 */
#define D3DPTEXTURECAPS_ALPHAPALETTE    0x00000080L

/*
 * Device can use non-POW2 textures if:
 *  1) D3DTEXTURE_ADDRESS is set to CLAMP for this texture's stage
 *  2) D3DRS_WRAP(N) is zero for this texture's coordinates
 *  3) mip mapping is not enabled (use magnification filter only)
 */
#define D3DPTEXTURECAPS_NONPOW2CONDITIONAL  0x00000100L

#endif /* DIRECT3D_VERSION >= 0x0600 */
#if(DIRECT3D_VERSION >= 0x0700)

// 0x00000200L unused

/*
 * Device can divide transformed texture coordinates by the
 * COUNTth texture coordinate (can do D3DTTFF_PROJECTED)
 */
#define D3DPTEXTURECAPS_PROJECTED  0x00000400L

/*
 * Device can do cubemap textures
 */
#define D3DPTEXTURECAPS_CUBEMAP           0x00000800L

#define D3DPTEXTURECAPS_COLORKEYBLEND     0x00001000L
#endif /* DIRECT3D_VERSION >= 0x0700 */

/* D3DPRIMCAPS dwTextureFilterCaps */

#define D3DPTFILTERCAPS_NEAREST         0x00000001L
#define D3DPTFILTERCAPS_LINEAR          0x00000002L
#define D3DPTFILTERCAPS_MIPNEAREST      0x00000004L
#define D3DPTFILTERCAPS_MIPLINEAR       0x00000008L
#define D3DPTFILTERCAPS_LINEARMIPNEAREST 0x00000010L
#define D3DPTFILTERCAPS_LINEARMIPLINEAR 0x00000020L

#if(DIRECT3D_VERSION >= 0x0600)
/* Device3 Min Filter */
#define D3DPTFILTERCAPS_MINFPOINT       0x00000100L
#define D3DPTFILTERCAPS_MINFLINEAR      0x00000200L
#define D3DPTFILTERCAPS_MINFANISOTROPIC 0x00000400L

/* Device3 Mip Filter */
#define D3DPTFILTERCAPS_MIPFPOINT       0x00010000L
#define D3DPTFILTERCAPS_MIPFLINEAR      0x00020000L

/* Device3 Mag Filter */
#define D3DPTFILTERCAPS_MAGFPOINT         0x01000000L
#define D3DPTFILTERCAPS_MAGFLINEAR        0x02000000L
#define D3DPTFILTERCAPS_MAGFANISOTROPIC   0x04000000L
#define D3DPTFILTERCAPS_MAGFAFLATCUBIC    0x08000000L
#define D3DPTFILTERCAPS_MAGFGAUSSIANCUBIC 0x10000000L
#endif /* DIRECT3D_VERSION >= 0x0600 */

/* D3DPRIMCAPS dwTextureBlendCaps */

#define D3DPTBLENDCAPS_DECAL            0x00000001L
#define D3DPTBLENDCAPS_MODULATE         0x00000002L
#define D3DPTBLENDCAPS_DECALALPHA       0x00000004L
#define D3DPTBLENDCAPS_MODULATEALPHA    0x00000008L
#define D3DPTBLENDCAPS_DECALMASK        0x00000010L
#define D3DPTBLENDCAPS_MODULATEMASK     0x00000020L
#define D3DPTBLENDCAPS_COPY             0x00000040L
#if(DIRECT3D_VERSION >= 0x0500)
#define D3DPTBLENDCAPS_ADD              0x00000080L
#endif /* DIRECT3D_VERSION >= 0x0500 */

/* D3DPRIMCAPS dwTextureAddressCaps */
#define D3DPTADDRESSCAPS_WRAP           0x00000001L
#define D3DPTADDRESSCAPS_MIRROR         0x00000002L
#define D3DPTADDRESSCAPS_CLAMP          0x00000004L
#if(DIRECT3D_VERSION >= 0x0500)
#define D3DPTADDRESSCAPS_BORDER         0x00000008L
#define D3DPTADDRESSCAPS_INDEPENDENTUV  0x00000010L
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)

/* D3DDEVICEDESC dwStencilCaps */

#define D3DSTENCILCAPS_KEEP     0x00000001L
#define D3DSTENCILCAPS_ZERO     0x00000002L
#define D3DSTENCILCAPS_REPLACE  0x00000004L
#define D3DSTENCILCAPS_INCRSAT  0x00000008L
#define D3DSTENCILCAPS_DECRSAT  0x00000010L
#define D3DSTENCILCAPS_INVERT   0x00000020L
#define D3DSTENCILCAPS_INCR     0x00000040L
#define D3DSTENCILCAPS_DECR     0x00000080L

/* D3DDEVICEDESC dwTextureOpCaps */

#define D3DTEXOPCAPS_DISABLE                    0x00000001L
#define D3DTEXOPCAPS_SELECTARG1                 0x00000002L
#define D3DTEXOPCAPS_SELECTARG2                 0x00000004L
#define D3DTEXOPCAPS_MODULATE                   0x00000008L
#define D3DTEXOPCAPS_MODULATE2X                 0x00000010L
#define D3DTEXOPCAPS_MODULATE4X                 0x00000020L
#define D3DTEXOPCAPS_ADD                        0x00000040L
#define D3DTEXOPCAPS_ADDSIGNED                  0x00000080L
#define D3DTEXOPCAPS_ADDSIGNED2X                0x00000100L
#define D3DTEXOPCAPS_SUBTRACT                   0x00000200L
#define D3DTEXOPCAPS_ADDSMOOTH                  0x00000400L
#define D3DTEXOPCAPS_BLENDDIFFUSEALPHA          0x00000800L
#define D3DTEXOPCAPS_BLENDTEXTUREALPHA          0x00001000L
#define D3DTEXOPCAPS_BLENDFACTORALPHA           0x00002000L
#define D3DTEXOPCAPS_BLENDTEXTUREALPHAPM        0x00004000L
#define D3DTEXOPCAPS_BLENDCURRENTALPHA          0x00008000L
#define D3DTEXOPCAPS_PREMODULATE                0x00010000L
#define D3DTEXOPCAPS_MODULATEALPHA_ADDCOLOR     0x00020000L
#define D3DTEXOPCAPS_MODULATECOLOR_ADDALPHA     0x00040000L
#define D3DTEXOPCAPS_MODULATEINVALPHA_ADDCOLOR  0x00080000L
#define D3DTEXOPCAPS_MODULATEINVCOLOR_ADDALPHA  0x00100000L
#define D3DTEXOPCAPS_BUMPENVMAP                 0x00200000L
#define D3DTEXOPCAPS_BUMPENVMAPLUMINANCE        0x00400000L
#define D3DTEXOPCAPS_DOTPRODUCT3                0x00800000L

/* D3DDEVICEDESC dwFVFCaps flags */

#define D3DFVFCAPS_TEXCOORDCOUNTMASK    0x0000ffffL /* mask for texture coordinate count field */
#define D3DFVFCAPS_DONOTSTRIPELEMENTS   0x00080000L /* Device prefers that vertex elements not be stripped */

#endif /* DIRECT3D_VERSION >= 0x0600 */

/*
 * Description for a device.
 * This is used to describe a device that is to be created or to query
 * the current device.
 */
typedef struct _D3DDeviceDesc {
    DWORD            dwSize;                 /* Size of D3DDEVICEDESC structure */
    DWORD            dwFlags;                /* Indicates which fields have valid data */
    D3DCOLORMODEL    dcmColorModel;          /* Color model of device */
    DWORD            dwDevCaps;              /* Capabilities of device */
    D3DTRANSFORMCAPS dtcTransformCaps;       /* Capabilities of transform */
    BOOL             bClipping;              /* Device can do 3D clipping */
    D3DLIGHTINGCAPS  dlcLightingCaps;        /* Capabilities of lighting */
    D3DPRIMCAPS      dpcLineCaps;
    D3DPRIMCAPS      dpcTriCaps;
    DWORD            dwDeviceRenderBitDepth; /* One of DDBB_8, 16, etc.. */
    DWORD            dwDeviceZBufferBitDepth;/* One of DDBD_16, 32, etc.. */
    DWORD            dwMaxBufferSize;        /* Maximum execute buffer size */
    DWORD            dwMaxVertexCount;       /* Maximum vertex count */
#if(DIRECT3D_VERSION >= 0x0500)
    // *** New fields for DX5 *** //

    // Width and height caps are 0 for legacy HALs.
    DWORD        dwMinTextureWidth, dwMinTextureHeight;
    DWORD        dwMaxTextureWidth, dwMaxTextureHeight;
    DWORD        dwMinStippleWidth, dwMaxStippleWidth;
    DWORD        dwMinStippleHeight, dwMaxStippleHeight;
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)
    // New fields for DX6
    DWORD       dwMaxTextureRepeat;
    DWORD       dwMaxTextureAspectRatio;
    DWORD       dwMaxAnisotropy;

    // Guard band that the rasterizer can accommodate
    // Screen-space vertices inside this space but outside the viewport
    // will get clipped properly.
    D3DVALUE    dvGuardBandLeft;
    D3DVALUE    dvGuardBandTop;
    D3DVALUE    dvGuardBandRight;
    D3DVALUE    dvGuardBandBottom;

    D3DVALUE    dvExtentsAdjust;
    DWORD       dwStencilCaps;

    DWORD       dwFVFCaps;
    DWORD       dwTextureOpCaps;
    WORD        wMaxTextureBlendStages;
    WORD        wMaxSimultaneousTextures;
#endif /* DIRECT3D_VERSION >= 0x0600 */
} D3DDEVICEDESC, *LPD3DDEVICEDESC;

#if(DIRECT3D_VERSION >= 0x0700)
typedef struct _D3DDeviceDesc7 {
    DWORD            dwDevCaps;              /* Capabilities of device */
    D3DPRIMCAPS      dpcLineCaps;
    D3DPRIMCAPS      dpcTriCaps;
    DWORD            dwDeviceRenderBitDepth; /* One of DDBB_8, 16, etc.. */
    DWORD            dwDeviceZBufferBitDepth;/* One of DDBD_16, 32, etc.. */

    DWORD       dwMinTextureWidth, dwMinTextureHeight;
    DWORD       dwMaxTextureWidth, dwMaxTextureHeight;

    DWORD       dwMaxTextureRepeat;
    DWORD       dwMaxTextureAspectRatio;
    DWORD       dwMaxAnisotropy;

    D3DVALUE    dvGuardBandLeft;
    D3DVALUE    dvGuardBandTop;
    D3DVALUE    dvGuardBandRight;
    D3DVALUE    dvGuardBandBottom;

    D3DVALUE    dvExtentsAdjust;
    DWORD       dwStencilCaps;

    DWORD       dwFVFCaps;
    DWORD       dwTextureOpCaps;
    WORD        wMaxTextureBlendStages;
    WORD        wMaxSimultaneousTextures;

    DWORD       dwMaxActiveLights;
    D3DVALUE    dvMaxVertexW;
    GUID        deviceGUID;

    WORD        wMaxUserClipPlanes;
    WORD        wMaxVertexBlendMatrices;

    DWORD       dwVertexProcessingCaps;

    DWORD       dwReserved1;
    DWORD       dwReserved2;
    DWORD       dwReserved3;
    DWORD       dwReserved4;
} D3DDEVICEDESC7, *LPD3DDEVICEDESC7;
#endif /* DIRECT3D_VERSION >= 0x0700 */

#define D3DDEVICEDESCSIZE (sizeof(D3DDEVICEDESC))
#define D3DDEVICEDESC7SIZE (sizeof(D3DDEVICEDESC7))

typedef HRESULT (CALLBACK * LPD3DENUMDEVICESCALLBACK)(GUID FAR *lpGuid, LPSTR lpDeviceDescription, LPSTR lpDeviceName, LPD3DDEVICEDESC, LPD3DDEVICEDESC, LPVOID);

#if(DIRECT3D_VERSION >= 0x0700)
typedef HRESULT (CALLBACK * LPD3DENUMDEVICESCALLBACK7)(LPSTR lpDeviceDescription, LPSTR lpDeviceName, LPD3DDEVICEDESC7, LPVOID);
#endif /* DIRECT3D_VERSION >= 0x0700 */

/* D3DDEVICEDESC dwFlags indicating valid fields */

#define D3DDD_COLORMODEL            0x00000001L /* dcmColorModel is valid */
#define D3DDD_DEVCAPS               0x00000002L /* dwDevCaps is valid */
#define D3DDD_TRANSFORMCAPS         0x00000004L /* dtcTransformCaps is valid */
#define D3DDD_LIGHTINGCAPS          0x00000008L /* dlcLightingCaps is valid */
#define D3DDD_BCLIPPING             0x00000010L /* bClipping is valid */
#define D3DDD_LINECAPS              0x00000020L /* dpcLineCaps is valid */
#define D3DDD_TRICAPS               0x00000040L /* dpcTriCaps is valid */
#define D3DDD_DEVICERENDERBITDEPTH  0x00000080L /* dwDeviceRenderBitDepth is valid */
#define D3DDD_DEVICEZBUFFERBITDEPTH 0x00000100L /* dwDeviceZBufferBitDepth is valid */
#define D3DDD_MAXBUFFERSIZE         0x00000200L /* dwMaxBufferSize is valid */
#define D3DDD_MAXVERTEXCOUNT        0x00000400L /* dwMaxVertexCount is valid */

/* D3DDEVICEDESC dwDevCaps flags */

#define D3DDEVCAPS_FLOATTLVERTEX        0x00000001L /* Device accepts floating point */
                                                    /* for post-transform vertex data */
#define D3DDEVCAPS_SORTINCREASINGZ      0x00000002L /* Device needs data sorted for increasing Z */
#define D3DDEVCAPS_SORTDECREASINGZ      0X00000004L /* Device needs data sorted for decreasing Z */
#define D3DDEVCAPS_SORTEXACT            0x00000008L /* Device needs data sorted exactly */

#define D3DDEVCAPS_EXECUTESYSTEMMEMORY  0x00000010L /* Device can use execute buffers from system memory */
#define D3DDEVCAPS_EXECUTEVIDEOMEMORY   0x00000020L /* Device can use execute buffers from video memory */
#define D3DDEVCAPS_TLVERTEXSYSTEMMEMORY 0x00000040L /* Device can use TL buffers from system memory */
#define D3DDEVCAPS_TLVERTEXVIDEOMEMORY  0x00000080L /* Device can use TL buffers from video memory */
#define D3DDEVCAPS_TEXTURESYSTEMMEMORY  0x00000100L /* Device can texture from system memory */
#define D3DDEVCAPS_TEXTUREVIDEOMEMORY   0x00000200L /* Device can texture from device memory */
#if(DIRECT3D_VERSION >= 0x0500)
#define D3DDEVCAPS_DRAWPRIMTLVERTEX     0x00000400L /* Device can draw TLVERTEX primitives */
#define D3DDEVCAPS_CANRENDERAFTERFLIP   0x00000800L /* Device can render without waiting for flip to complete */
#define D3DDEVCAPS_TEXTURENONLOCALVIDMEM 0x00001000L /* Device can texture from nonlocal video memory */
#endif /* DIRECT3D_VERSION >= 0x0500 */
#if(DIRECT3D_VERSION >= 0x0600)
#define D3DDEVCAPS_DRAWPRIMITIVES2         0x00002000L /* Device can support DrawPrimitives2 */
#define D3DDEVCAPS_SEPARATETEXTUREMEMORIES 0x00004000L /* Device is texturing from separate memory pools */
#define D3DDEVCAPS_DRAWPRIMITIVES2EX       0x00008000L /* Device can support Extended DrawPrimitives2 i.e. DX7 compliant driver*/
#endif /* DIRECT3D_VERSION >= 0x0600 */
#if(DIRECT3D_VERSION >= 0x0700)
#define D3DDEVCAPS_HWTRANSFORMANDLIGHT     0x00010000L /* Device can support transformation and lighting in hardware and DRAWPRIMITIVES2EX must be also */
#define D3DDEVCAPS_CANBLTSYSTONONLOCAL     0x00020000L /* Device supports a Tex Blt from system memory to non-local vidmem */
#define D3DDEVCAPS_HWRASTERIZATION         0x00080000L /* Device has HW acceleration for rasterization */

/*
 * These are the flags in the D3DDEVICEDESC7.dwVertexProcessingCaps field
 */

/* device can do texgen */
#define D3DVTXPCAPS_TEXGEN              0x00000001L
/* device can do IDirect3DDevice7 colormaterialsource ops */
#define D3DVTXPCAPS_MATERIALSOURCE7     0x00000002L
/* device can do vertex fog */
#define D3DVTXPCAPS_VERTEXFOG           0x00000004L
/* device can do directional lights */
#define D3DVTXPCAPS_DIRECTIONALLIGHTS   0x00000008L
/* device can do positional lights (includes point and spot) */
#define D3DVTXPCAPS_POSITIONALLIGHTS    0x00000010L
/* device can do local viewer */
#define D3DVTXPCAPS_LOCALVIEWER         0x00000020L

#endif /* DIRECT3D_VERSION >= 0x0700 */

#define D3DFDS_COLORMODEL        0x00000001L /* Match color model */
#define D3DFDS_GUID              0x00000002L /* Match guid */
#define D3DFDS_HARDWARE          0x00000004L /* Match hardware/software */
#define D3DFDS_TRIANGLES         0x00000008L /* Match in triCaps */
#define D3DFDS_LINES             0x00000010L /* Match in lineCaps  */
#define D3DFDS_MISCCAPS          0x00000020L /* Match primCaps.dwMiscCaps */
#define D3DFDS_RASTERCAPS        0x00000040L /* Match primCaps.dwRasterCaps */
#define D3DFDS_ZCMPCAPS          0x00000080L /* Match primCaps.dwZCmpCaps */
#define D3DFDS_ALPHACMPCAPS      0x00000100L /* Match primCaps.dwAlphaCmpCaps */
#define D3DFDS_SRCBLENDCAPS      0x00000200L /* Match primCaps.dwSourceBlendCaps */
#define D3DFDS_DSTBLENDCAPS      0x00000400L /* Match primCaps.dwDestBlendCaps */
#define D3DFDS_SHADECAPS         0x00000800L /* Match primCaps.dwShadeCaps */
#define D3DFDS_TEXTURECAPS       0x00001000L /* Match primCaps.dwTextureCaps */
#define D3DFDS_TEXTUREFILTERCAPS 0x00002000L /* Match primCaps.dwTextureFilterCaps */
#define D3DFDS_TEXTUREBLENDCAPS  0x00004000L /* Match primCaps.dwTextureBlendCaps */
#define D3DFDS_TEXTUREADDRESSCAPS  0x00008000L /* Match primCaps.dwTextureBlendCaps */

/*
 * FindDevice arguments
 */
typedef struct _D3DFINDDEVICESEARCH {
    DWORD               dwSize;
    DWORD               dwFlags;
    BOOL                bHardware;
    D3DCOLORMODEL       dcmColorModel;
    GUID                guid;
    DWORD               dwCaps;
    D3DPRIMCAPS         dpcPrimCaps;
} D3DFINDDEVICESEARCH, *LPD3DFINDDEVICESEARCH;

typedef struct _D3DFINDDEVICERESULT {
    DWORD               dwSize;
    GUID                guid;           /* guid which matched */
    D3DDEVICEDESC       ddHwDesc;       /* hardware D3DDEVICEDESC */
    D3DDEVICEDESC       ddSwDesc;       /* software D3DDEVICEDESC */
} D3DFINDDEVICERESULT, *LPD3DFINDDEVICERESULT;

/*
 * Description of execute buffer.
 */
typedef struct _D3DExecuteBufferDesc {
    DWORD               dwSize;         /* size of this structure */
    DWORD               dwFlags;        /* flags indicating which fields are valid */
    DWORD               dwCaps;         /* capabilities of execute buffer */
    DWORD               dwBufferSize;   /* size of execute buffer data */
    LPVOID              lpData;         /* pointer to actual data */
} D3DEXECUTEBUFFERDESC, *LPD3DEXECUTEBUFFERDESC;

/* D3DEXECUTEBUFFER dwFlags indicating valid fields */

#define D3DDEB_BUFSIZE          0x00000001l     /* buffer size valid */
#define D3DDEB_CAPS             0x00000002l     /* caps valid */
#define D3DDEB_LPDATA           0x00000004l     /* lpData valid */

/* D3DEXECUTEBUFFER dwCaps */

#define D3DDEBCAPS_SYSTEMMEMORY 0x00000001l     /* buffer in system memory */
#define D3DDEBCAPS_VIDEOMEMORY  0x00000002l     /* buffer in device memory */
#define D3DDEBCAPS_MEM (D3DDEBCAPS_SYSTEMMEMORY|D3DDEBCAPS_VIDEOMEMORY)

#if(DIRECT3D_VERSION < 0x0800)

#if(DIRECT3D_VERSION >= 0x0700)
typedef struct _D3DDEVINFO_TEXTUREMANAGER {
    BOOL    bThrashing;                 /* indicates if thrashing */
    DWORD   dwApproxBytesDownloaded;    /* Approximate number of bytes downloaded by texture manager */
    DWORD   dwNumEvicts;                /* number of textures evicted */
    DWORD   dwNumVidCreates;            /* number of textures created in video memory */
    DWORD   dwNumTexturesUsed;          /* number of textures used */
    DWORD   dwNumUsedTexInVid;          /* number of used textures present in video memory */
    DWORD   dwWorkingSet;               /* number of textures in video memory */
    DWORD   dwWorkingSetBytes;          /* number of bytes in video memory */
    DWORD   dwTotalManaged;             /* total number of managed textures */
    DWORD   dwTotalBytes;               /* total number of bytes of managed textures */
    DWORD   dwLastPri;                  /* priority of last texture evicted */
} D3DDEVINFO_TEXTUREMANAGER, *LPD3DDEVINFO_TEXTUREMANAGER;

typedef struct _D3DDEVINFO_TEXTURING {
    DWORD   dwNumLoads;                 /* counts Load() API calls */
    DWORD   dwApproxBytesLoaded;        /* Approximate number bytes loaded via Load() */
    DWORD   dwNumPreLoads;              /* counts PreLoad() API calls */
    DWORD   dwNumSet;                   /* counts SetTexture() API calls */
    DWORD   dwNumCreates;               /* counts texture creates */
    DWORD   dwNumDestroys;              /* counts texture destroys */
    DWORD   dwNumSetPriorities;         /* counts SetPriority() API calls */
    DWORD   dwNumSetLODs;               /* counts SetLOD() API calls */
    DWORD   dwNumLocks;                 /* counts number of texture locks */
    DWORD   dwNumGetDCs;                /* counts number of GetDCs to textures */
} D3DDEVINFO_TEXTURING, *LPD3DDEVINFO_TEXTURING;
#endif /* DIRECT3D_VERSION >= 0x0700 */

#endif //(DIRECT3D_VERSION < 0x0800)

#pragma pack()


#endif /* _D3DCAPS_H_ */




#ifdef __cplusplus
extern "C" {
#endif

/*
 * Direct3D interfaces
 */
#undef INTERFACE
#define INTERFACE IDirect3D

DECLARE_INTERFACE_(IDirect3D, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3D methods ***/
    STDMETHOD(Initialize)(THIS_ REFCLSID) PURE;
    STDMETHOD(EnumDevices)(THIS_ LPD3DENUMDEVICESCALLBACK,LPVOID) PURE;
    STDMETHOD(CreateLight)(THIS_ LPDIRECT3DLIGHT*,IUnknown*) PURE;
    STDMETHOD(CreateMaterial)(THIS_ LPDIRECT3DMATERIAL*,IUnknown*) PURE;
    STDMETHOD(CreateViewport)(THIS_ LPDIRECT3DVIEWPORT*,IUnknown*) PURE;
    STDMETHOD(FindDevice)(THIS_ LPD3DFINDDEVICESEARCH,LPD3DFINDDEVICERESULT) PURE;
};

typedef struct IDirect3D *LPDIRECT3D;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3D_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3D_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3D_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3D_Initialize(p,a) (p)->lpVtbl->Initialize(p,a)
#define IDirect3D_EnumDevices(p,a,b) (p)->lpVtbl->EnumDevices(p,a,b)
#define IDirect3D_CreateLight(p,a,b) (p)->lpVtbl->CreateLight(p,a,b)
#define IDirect3D_CreateMaterial(p,a,b) (p)->lpVtbl->CreateMaterial(p,a,b)
#define IDirect3D_CreateViewport(p,a,b) (p)->lpVtbl->CreateViewport(p,a,b)
#define IDirect3D_FindDevice(p,a,b) (p)->lpVtbl->FindDevice(p,a,b)
#else
#define IDirect3D_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3D_AddRef(p) (p)->AddRef()
#define IDirect3D_Release(p) (p)->Release()
#define IDirect3D_Initialize(p,a) (p)->Initialize(a)
#define IDirect3D_EnumDevices(p,a,b) (p)->EnumDevices(a,b)
#define IDirect3D_CreateLight(p,a,b) (p)->CreateLight(a,b)
#define IDirect3D_CreateMaterial(p,a,b) (p)->CreateMaterial(a,b)
#define IDirect3D_CreateViewport(p,a,b) (p)->CreateViewport(a,b)
#define IDirect3D_FindDevice(p,a,b) (p)->FindDevice(a,b)
#endif

#if(DIRECT3D_VERSION >= 0x0500)
#undef INTERFACE
#define INTERFACE IDirect3D2

DECLARE_INTERFACE_(IDirect3D2, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3D2 methods ***/
    STDMETHOD(EnumDevices)(THIS_ LPD3DENUMDEVICESCALLBACK,LPVOID) PURE;
    STDMETHOD(CreateLight)(THIS_ LPDIRECT3DLIGHT*,IUnknown*) PURE;
    STDMETHOD(CreateMaterial)(THIS_ LPDIRECT3DMATERIAL2*,IUnknown*) PURE;
    STDMETHOD(CreateViewport)(THIS_ LPDIRECT3DVIEWPORT2*,IUnknown*) PURE;
    STDMETHOD(FindDevice)(THIS_ LPD3DFINDDEVICESEARCH,LPD3DFINDDEVICERESULT) PURE;
    STDMETHOD(CreateDevice)(THIS_ REFCLSID,LPDIRECTDRAWSURFACE,LPDIRECT3DDEVICE2*) PURE;
};

typedef struct IDirect3D2 *LPDIRECT3D2;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3D2_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3D2_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3D2_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3D2_EnumDevices(p,a,b) (p)->lpVtbl->EnumDevices(p,a,b)
#define IDirect3D2_CreateLight(p,a,b) (p)->lpVtbl->CreateLight(p,a,b)
#define IDirect3D2_CreateMaterial(p,a,b) (p)->lpVtbl->CreateMaterial(p,a,b)
#define IDirect3D2_CreateViewport(p,a,b) (p)->lpVtbl->CreateViewport(p,a,b)
#define IDirect3D2_FindDevice(p,a,b) (p)->lpVtbl->FindDevice(p,a,b)
#define IDirect3D2_CreateDevice(p,a,b,c) (p)->lpVtbl->CreateDevice(p,a,b,c)
#else
#define IDirect3D2_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3D2_AddRef(p) (p)->AddRef()
#define IDirect3D2_Release(p) (p)->Release()
#define IDirect3D2_EnumDevices(p,a,b) (p)->EnumDevices(a,b)
#define IDirect3D2_CreateLight(p,a,b) (p)->CreateLight(a,b)
#define IDirect3D2_CreateMaterial(p,a,b) (p)->CreateMaterial(a,b)
#define IDirect3D2_CreateViewport(p,a,b) (p)->CreateViewport(a,b)
#define IDirect3D2_FindDevice(p,a,b) (p)->FindDevice(a,b)
#define IDirect3D2_CreateDevice(p,a,b,c) (p)->CreateDevice(a,b,c)
#endif
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)
#undef INTERFACE
#define INTERFACE IDirect3D3

DECLARE_INTERFACE_(IDirect3D3, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3D3 methods ***/
    STDMETHOD(EnumDevices)(THIS_ LPD3DENUMDEVICESCALLBACK,LPVOID) PURE;
    STDMETHOD(CreateLight)(THIS_ LPDIRECT3DLIGHT*,LPUNKNOWN) PURE;
    STDMETHOD(CreateMaterial)(THIS_ LPDIRECT3DMATERIAL3*,LPUNKNOWN) PURE;
    STDMETHOD(CreateViewport)(THIS_ LPDIRECT3DVIEWPORT3*,LPUNKNOWN) PURE;
    STDMETHOD(FindDevice)(THIS_ LPD3DFINDDEVICESEARCH,LPD3DFINDDEVICERESULT) PURE;
    STDMETHOD(CreateDevice)(THIS_ REFCLSID,LPDIRECTDRAWSURFACE4,LPDIRECT3DDEVICE3*,LPUNKNOWN) PURE;
    STDMETHOD(CreateVertexBuffer)(THIS_ LPD3DVERTEXBUFFERDESC,LPDIRECT3DVERTEXBUFFER*,DWORD,LPUNKNOWN) PURE;
    STDMETHOD(EnumZBufferFormats)(THIS_ REFCLSID,LPD3DENUMPIXELFORMATSCALLBACK,LPVOID) PURE;
    STDMETHOD(EvictManagedTextures)(THIS) PURE;
};

typedef struct IDirect3D3 *LPDIRECT3D3;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3D3_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3D3_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3D3_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3D3_EnumDevices(p,a,b) (p)->lpVtbl->EnumDevices(p,a,b)
#define IDirect3D3_CreateLight(p,a,b) (p)->lpVtbl->CreateLight(p,a,b)
#define IDirect3D3_CreateMaterial(p,a,b) (p)->lpVtbl->CreateMaterial(p,a,b)
#define IDirect3D3_CreateViewport(p,a,b) (p)->lpVtbl->CreateViewport(p,a,b)
#define IDirect3D3_FindDevice(p,a,b) (p)->lpVtbl->FindDevice(p,a,b)
#define IDirect3D3_CreateDevice(p,a,b,c,d) (p)->lpVtbl->CreateDevice(p,a,b,c,d)
#define IDirect3D3_CreateVertexBuffer(p,a,b,c,d) (p)->lpVtbl->CreateVertexBuffer(p,a,b,c,d)
#define IDirect3D3_EnumZBufferFormats(p,a,b,c) (p)->lpVtbl->EnumZBufferFormats(p,a,b,c)
#define IDirect3D3_EvictManagedTextures(p) (p)->lpVtbl->EvictManagedTextures(p)
#else
#define IDirect3D3_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3D3_AddRef(p) (p)->AddRef()
#define IDirect3D3_Release(p) (p)->Release()
#define IDirect3D3_EnumDevices(p,a,b) (p)->EnumDevices(a,b)
#define IDirect3D3_CreateLight(p,a,b) (p)->CreateLight(a,b)
#define IDirect3D3_CreateMaterial(p,a,b) (p)->CreateMaterial(a,b)
#define IDirect3D3_CreateViewport(p,a,b) (p)->CreateViewport(a,b)
#define IDirect3D3_FindDevice(p,a,b) (p)->FindDevice(a,b)
#define IDirect3D3_CreateDevice(p,a,b,c,d) (p)->CreateDevice(a,b,c,d)
#define IDirect3D3_CreateVertexBuffer(p,a,b,c,d) (p)->CreateVertexBuffer(a,b,c,d)
#define IDirect3D3_EnumZBufferFormats(p,a,b,c) (p)->EnumZBufferFormats(a,b,c)
#define IDirect3D3_EvictManagedTextures(p) (p)->EvictManagedTextures()
#endif
#endif /* DIRECT3D_VERSION >= 0x0600 */

#if(DIRECT3D_VERSION >= 0x0700)
#undef INTERFACE
#define INTERFACE IDirect3D7

DECLARE_INTERFACE_(IDirect3D7, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3D7 methods ***/
    STDMETHOD(EnumDevices)(THIS_ LPD3DENUMDEVICESCALLBACK7,LPVOID) PURE;
    STDMETHOD(CreateDevice)(THIS_ REFCLSID,LPDIRECTDRAWSURFACE7,LPDIRECT3DDEVICE7*) PURE;
    STDMETHOD(CreateVertexBuffer)(THIS_ LPD3DVERTEXBUFFERDESC,LPDIRECT3DVERTEXBUFFER7*,DWORD) PURE;
    STDMETHOD(EnumZBufferFormats)(THIS_ REFCLSID,LPD3DENUMPIXELFORMATSCALLBACK,LPVOID) PURE;
    STDMETHOD(EvictManagedTextures)(THIS) PURE;
};

typedef struct IDirect3D7 *LPDIRECT3D7;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3D7_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3D7_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3D7_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3D7_EnumDevices(p,a,b) (p)->lpVtbl->EnumDevices(p,a,b)
#define IDirect3D7_CreateDevice(p,a,b,c) (p)->lpVtbl->CreateDevice(p,a,b,c)
#define IDirect3D7_CreateVertexBuffer(p,a,b,c) (p)->lpVtbl->CreateVertexBuffer(p,a,b,c)
#define IDirect3D7_EnumZBufferFormats(p,a,b,c) (p)->lpVtbl->EnumZBufferFormats(p,a,b,c)
#define IDirect3D7_EvictManagedTextures(p) (p)->lpVtbl->EvictManagedTextures(p)
#else
#define IDirect3D7_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3D7_AddRef(p) (p)->AddRef()
#define IDirect3D7_Release(p) (p)->Release()
#define IDirect3D7_EnumDevices(p,a,b) (p)->EnumDevices(a,b)
#define IDirect3D7_CreateDevice(p,a,b,c) (p)->CreateDevice(a,b,c)
#define IDirect3D7_CreateVertexBuffer(p,a,b,c) (p)->CreateVertexBuffer(a,b,c)
#define IDirect3D7_EnumZBufferFormats(p,a,b,c) (p)->EnumZBufferFormats(a,b,c)
#define IDirect3D7_EvictManagedTextures(p) (p)->EvictManagedTextures()
#endif
#endif /* DIRECT3D_VERSION >= 0x0700 */
/*
 * Direct3D Device interfaces
 */
#undef INTERFACE
#define INTERFACE IDirect3DDevice

DECLARE_INTERFACE_(IDirect3DDevice, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DDevice methods ***/
    STDMETHOD(Initialize)(THIS_ LPDIRECT3D,LPGUID,LPD3DDEVICEDESC) PURE;
    STDMETHOD(GetCaps)(THIS_ LPD3DDEVICEDESC,LPD3DDEVICEDESC) PURE;
    STDMETHOD(SwapTextureHandles)(THIS_ LPDIRECT3DTEXTURE,LPDIRECT3DTEXTURE) PURE;
    STDMETHOD(CreateExecuteBuffer)(THIS_ LPD3DEXECUTEBUFFERDESC,LPDIRECT3DEXECUTEBUFFER*,IUnknown*) PURE;
    STDMETHOD(GetStats)(THIS_ LPD3DSTATS) PURE;
    STDMETHOD(Execute)(THIS_ LPDIRECT3DEXECUTEBUFFER,LPDIRECT3DVIEWPORT,DWORD) PURE;
    STDMETHOD(AddViewport)(THIS_ LPDIRECT3DVIEWPORT) PURE;
    STDMETHOD(DeleteViewport)(THIS_ LPDIRECT3DVIEWPORT) PURE;
    STDMETHOD(NextViewport)(THIS_ LPDIRECT3DVIEWPORT,LPDIRECT3DVIEWPORT*,DWORD) PURE;
    STDMETHOD(Pick)(THIS_ LPDIRECT3DEXECUTEBUFFER,LPDIRECT3DVIEWPORT,DWORD,LPD3DRECT) PURE;
    STDMETHOD(GetPickRecords)(THIS_ LPDWORD,LPD3DPICKRECORD) PURE;
    STDMETHOD(EnumTextureFormats)(THIS_ LPD3DENUMTEXTUREFORMATSCALLBACK,LPVOID) PURE;
    STDMETHOD(CreateMatrix)(THIS_ LPD3DMATRIXHANDLE) PURE;
    STDMETHOD(SetMatrix)(THIS_ D3DMATRIXHANDLE,const LPD3DMATRIX) PURE;
    STDMETHOD(GetMatrix)(THIS_ D3DMATRIXHANDLE,LPD3DMATRIX) PURE;
    STDMETHOD(DeleteMatrix)(THIS_ D3DMATRIXHANDLE) PURE;
    STDMETHOD(BeginScene)(THIS) PURE;
    STDMETHOD(EndScene)(THIS) PURE;
    STDMETHOD(GetDirect3D)(THIS_ LPDIRECT3D*) PURE;
};

typedef struct IDirect3DDevice *LPDIRECT3DDEVICE;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DDevice_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DDevice_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DDevice_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DDevice_Initialize(p,a,b,c) (p)->lpVtbl->Initialize(p,a,b,c)
#define IDirect3DDevice_GetCaps(p,a,b) (p)->lpVtbl->GetCaps(p,a,b)
#define IDirect3DDevice_SwapTextureHandles(p,a,b) (p)->lpVtbl->SwapTextureHandles(p,a,b)
#define IDirect3DDevice_CreateExecuteBuffer(p,a,b,c) (p)->lpVtbl->CreateExecuteBuffer(p,a,b,c)
#define IDirect3DDevice_GetStats(p,a) (p)->lpVtbl->GetStats(p,a)
#define IDirect3DDevice_Execute(p,a,b,c) (p)->lpVtbl->Execute(p,a,b,c)
#define IDirect3DDevice_AddViewport(p,a) (p)->lpVtbl->AddViewport(p,a)
#define IDirect3DDevice_DeleteViewport(p,a) (p)->lpVtbl->DeleteViewport(p,a)
#define IDirect3DDevice_NextViewport(p,a,b,c) (p)->lpVtbl->NextViewport(p,a,b,c)
#define IDirect3DDevice_Pick(p,a,b,c,d) (p)->lpVtbl->Pick(p,a,b,c,d)
#define IDirect3DDevice_GetPickRecords(p,a,b) (p)->lpVtbl->GetPickRecords(p,a,b)
#define IDirect3DDevice_EnumTextureFormats(p,a,b) (p)->lpVtbl->EnumTextureFormats(p,a,b)
#define IDirect3DDevice_CreateMatrix(p,a) (p)->lpVtbl->CreateMatrix(p,a)
#define IDirect3DDevice_SetMatrix(p,a,b) (p)->lpVtbl->SetMatrix(p,a,b)
#define IDirect3DDevice_GetMatrix(p,a,b) (p)->lpVtbl->GetMatrix(p,a,b)
#define IDirect3DDevice_DeleteMatrix(p,a) (p)->lpVtbl->DeleteMatrix(p,a)
#define IDirect3DDevice_BeginScene(p) (p)->lpVtbl->BeginScene(p)
#define IDirect3DDevice_EndScene(p) (p)->lpVtbl->EndScene(p)
#define IDirect3DDevice_GetDirect3D(p,a) (p)->lpVtbl->GetDirect3D(p,a)
#else
#define IDirect3DDevice_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DDevice_AddRef(p) (p)->AddRef()
#define IDirect3DDevice_Release(p) (p)->Release()
#define IDirect3DDevice_Initialize(p,a,b,c) (p)->Initialize(a,b,c)
#define IDirect3DDevice_GetCaps(p,a,b) (p)->GetCaps(a,b)
#define IDirect3DDevice_SwapTextureHandles(p,a,b) (p)->SwapTextureHandles(a,b)
#define IDirect3DDevice_CreateExecuteBuffer(p,a,b,c) (p)->CreateExecuteBuffer(a,b,c)
#define IDirect3DDevice_GetStats(p,a) (p)->GetStats(a)
#define IDirect3DDevice_Execute(p,a,b,c) (p)->Execute(a,b,c)
#define IDirect3DDevice_AddViewport(p,a) (p)->AddViewport(a)
#define IDirect3DDevice_DeleteViewport(p,a) (p)->DeleteViewport(a)
#define IDirect3DDevice_NextViewport(p,a,b,c) (p)->NextViewport(a,b,c)
#define IDirect3DDevice_Pick(p,a,b,c,d) (p)->Pick(a,b,c,d)
#define IDirect3DDevice_GetPickRecords(p,a,b) (p)->GetPickRecords(a,b)
#define IDirect3DDevice_EnumTextureFormats(p,a,b) (p)->EnumTextureFormats(a,b)
#define IDirect3DDevice_CreateMatrix(p,a) (p)->CreateMatrix(a)
#define IDirect3DDevice_SetMatrix(p,a,b) (p)->SetMatrix(a,b)
#define IDirect3DDevice_GetMatrix(p,a,b) (p)->GetMatrix(a,b)
#define IDirect3DDevice_DeleteMatrix(p,a) (p)->DeleteMatrix(a)
#define IDirect3DDevice_BeginScene(p) (p)->BeginScene()
#define IDirect3DDevice_EndScene(p) (p)->EndScene()
#define IDirect3DDevice_GetDirect3D(p,a) (p)->GetDirect3D(a)
#endif

#if(DIRECT3D_VERSION >= 0x0500)
#undef INTERFACE
#define INTERFACE IDirect3DDevice2

DECLARE_INTERFACE_(IDirect3DDevice2, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DDevice2 methods ***/
    STDMETHOD(GetCaps)(THIS_ LPD3DDEVICEDESC,LPD3DDEVICEDESC) PURE;
    STDMETHOD(SwapTextureHandles)(THIS_ LPDIRECT3DTEXTURE2,LPDIRECT3DTEXTURE2) PURE;
    STDMETHOD(GetStats)(THIS_ LPD3DSTATS) PURE;
    STDMETHOD(AddViewport)(THIS_ LPDIRECT3DVIEWPORT2) PURE;
    STDMETHOD(DeleteViewport)(THIS_ LPDIRECT3DVIEWPORT2) PURE;
    STDMETHOD(NextViewport)(THIS_ LPDIRECT3DVIEWPORT2,LPDIRECT3DVIEWPORT2*,DWORD) PURE;
    STDMETHOD(EnumTextureFormats)(THIS_ LPD3DENUMTEXTUREFORMATSCALLBACK,LPVOID) PURE;
    STDMETHOD(BeginScene)(THIS) PURE;
    STDMETHOD(EndScene)(THIS) PURE;
    STDMETHOD(GetDirect3D)(THIS_ LPDIRECT3D2*) PURE;
    STDMETHOD(SetCurrentViewport)(THIS_ LPDIRECT3DVIEWPORT2) PURE;
    STDMETHOD(GetCurrentViewport)(THIS_ LPDIRECT3DVIEWPORT2 *) PURE;
    STDMETHOD(SetRenderTarget)(THIS_ LPDIRECTDRAWSURFACE,DWORD) PURE;
    STDMETHOD(GetRenderTarget)(THIS_ LPDIRECTDRAWSURFACE *) PURE;
    STDMETHOD(Begin)(THIS_ D3DPRIMITIVETYPE,D3DVERTEXTYPE,DWORD) PURE;
    STDMETHOD(BeginIndexed)(THIS_ D3DPRIMITIVETYPE,D3DVERTEXTYPE,LPVOID,DWORD,DWORD) PURE;
    STDMETHOD(Vertex)(THIS_ LPVOID) PURE;
    STDMETHOD(Index)(THIS_ WORD) PURE;
    STDMETHOD(End)(THIS_ DWORD) PURE;
    STDMETHOD(GetRenderState)(THIS_ D3DRENDERSTATETYPE,LPDWORD) PURE;
    STDMETHOD(SetRenderState)(THIS_ D3DRENDERSTATETYPE,DWORD) PURE;
    STDMETHOD(GetLightState)(THIS_ D3DLIGHTSTATETYPE,LPDWORD) PURE;
    STDMETHOD(SetLightState)(THIS_ D3DLIGHTSTATETYPE,DWORD) PURE;
    STDMETHOD(SetTransform)(THIS_ D3DTRANSFORMSTATETYPE,LPD3DMATRIX) PURE;
    STDMETHOD(GetTransform)(THIS_ D3DTRANSFORMSTATETYPE,LPD3DMATRIX) PURE;
    STDMETHOD(MultiplyTransform)(THIS_ D3DTRANSFORMSTATETYPE,LPD3DMATRIX) PURE;
    STDMETHOD(DrawPrimitive)(THIS_ D3DPRIMITIVETYPE,D3DVERTEXTYPE,LPVOID,DWORD,DWORD) PURE;
    STDMETHOD(DrawIndexedPrimitive)(THIS_ D3DPRIMITIVETYPE,D3DVERTEXTYPE,LPVOID,DWORD,LPWORD,DWORD,DWORD) PURE;
    STDMETHOD(SetClipStatus)(THIS_ LPD3DCLIPSTATUS) PURE;
    STDMETHOD(GetClipStatus)(THIS_ LPD3DCLIPSTATUS) PURE;
};

typedef struct IDirect3DDevice2 *LPDIRECT3DDEVICE2;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DDevice2_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DDevice2_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DDevice2_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DDevice2_GetCaps(p,a,b) (p)->lpVtbl->GetCaps(p,a,b)
#define IDirect3DDevice2_SwapTextureHandles(p,a,b) (p)->lpVtbl->SwapTextureHandles(p,a,b)
#define IDirect3DDevice2_GetStats(p,a) (p)->lpVtbl->GetStats(p,a)
#define IDirect3DDevice2_AddViewport(p,a) (p)->lpVtbl->AddViewport(p,a)
#define IDirect3DDevice2_DeleteViewport(p,a) (p)->lpVtbl->DeleteViewport(p,a)
#define IDirect3DDevice2_NextViewport(p,a,b,c) (p)->lpVtbl->NextViewport(p,a,b,c)
#define IDirect3DDevice2_EnumTextureFormats(p,a,b) (p)->lpVtbl->EnumTextureFormats(p,a,b)
#define IDirect3DDevice2_BeginScene(p) (p)->lpVtbl->BeginScene(p)
#define IDirect3DDevice2_EndScene(p) (p)->lpVtbl->EndScene(p)
#define IDirect3DDevice2_GetDirect3D(p,a) (p)->lpVtbl->GetDirect3D(p,a)
#define IDirect3DDevice2_SetCurrentViewport(p,a) (p)->lpVtbl->SetCurrentViewport(p,a)
#define IDirect3DDevice2_GetCurrentViewport(p,a) (p)->lpVtbl->GetCurrentViewport(p,a)
#define IDirect3DDevice2_SetRenderTarget(p,a,b) (p)->lpVtbl->SetRenderTarget(p,a,b)
#define IDirect3DDevice2_GetRenderTarget(p,a) (p)->lpVtbl->GetRenderTarget(p,a)
#define IDirect3DDevice2_Begin(p,a,b,c) (p)->lpVtbl->Begin(p,a,b,c)
#define IDirect3DDevice2_BeginIndexed(p,a,b,c,d,e) (p)->lpVtbl->BeginIndexed(p,a,b,c,d,e)
#define IDirect3DDevice2_Vertex(p,a) (p)->lpVtbl->Vertex(p,a)
#define IDirect3DDevice2_Index(p,a) (p)->lpVtbl->Index(p,a)
#define IDirect3DDevice2_End(p,a) (p)->lpVtbl->End(p,a)
#define IDirect3DDevice2_GetRenderState(p,a,b) (p)->lpVtbl->GetRenderState(p,a,b)
#define IDirect3DDevice2_SetRenderState(p,a,b) (p)->lpVtbl->SetRenderState(p,a,b)
#define IDirect3DDevice2_GetLightState(p,a,b) (p)->lpVtbl->GetLightState(p,a,b)
#define IDirect3DDevice2_SetLightState(p,a,b) (p)->lpVtbl->SetLightState(p,a,b)
#define IDirect3DDevice2_SetTransform(p,a,b) (p)->lpVtbl->SetTransform(p,a,b)
#define IDirect3DDevice2_GetTransform(p,a,b) (p)->lpVtbl->GetTransform(p,a,b)
#define IDirect3DDevice2_MultiplyTransform(p,a,b) (p)->lpVtbl->MultiplyTransform(p,a,b)
#define IDirect3DDevice2_DrawPrimitive(p,a,b,c,d,e) (p)->lpVtbl->DrawPrimitive(p,a,b,c,d,e)
#define IDirect3DDevice2_DrawIndexedPrimitive(p,a,b,c,d,e,f,g) (p)->lpVtbl->DrawIndexedPrimitive(p,a,b,c,d,e,f,g)
#define IDirect3DDevice2_SetClipStatus(p,a) (p)->lpVtbl->SetClipStatus(p,a)
#define IDirect3DDevice2_GetClipStatus(p,a) (p)->lpVtbl->GetClipStatus(p,a)
#else
#define IDirect3DDevice2_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DDevice2_AddRef(p) (p)->AddRef()
#define IDirect3DDevice2_Release(p) (p)->Release()
#define IDirect3DDevice2_GetCaps(p,a,b) (p)->GetCaps(a,b)
#define IDirect3DDevice2_SwapTextureHandles(p,a,b) (p)->SwapTextureHandles(a,b)
#define IDirect3DDevice2_GetStats(p,a) (p)->GetStats(a)
#define IDirect3DDevice2_AddViewport(p,a) (p)->AddViewport(a)
#define IDirect3DDevice2_DeleteViewport(p,a) (p)->DeleteViewport(a)
#define IDirect3DDevice2_NextViewport(p,a,b,c) (p)->NextViewport(a,b,c)
#define IDirect3DDevice2_EnumTextureFormats(p,a,b) (p)->EnumTextureFormats(a,b)
#define IDirect3DDevice2_BeginScene(p) (p)->BeginScene()
#define IDirect3DDevice2_EndScene(p) (p)->EndScene()
#define IDirect3DDevice2_GetDirect3D(p,a) (p)->GetDirect3D(a)
#define IDirect3DDevice2_SetCurrentViewport(p,a) (p)->SetCurrentViewport(a)
#define IDirect3DDevice2_GetCurrentViewport(p,a) (p)->GetCurrentViewport(a)
#define IDirect3DDevice2_SetRenderTarget(p,a,b) (p)->SetRenderTarget(a,b)
#define IDirect3DDevice2_GetRenderTarget(p,a) (p)->GetRenderTarget(a)
#define IDirect3DDevice2_Begin(p,a,b,c) (p)->Begin(a,b,c)
#define IDirect3DDevice2_BeginIndexed(p,a,b,c,d,e) (p)->BeginIndexed(a,b,c,d,e)
#define IDirect3DDevice2_Vertex(p,a) (p)->Vertex(a)
#define IDirect3DDevice2_Index(p,a) (p)->Index(a)
#define IDirect3DDevice2_End(p,a) (p)->End(a)
#define IDirect3DDevice2_GetRenderState(p,a,b) (p)->GetRenderState(a,b)
#define IDirect3DDevice2_SetRenderState(p,a,b) (p)->SetRenderState(a,b)
#define IDirect3DDevice2_GetLightState(p,a,b) (p)->GetLightState(a,b)
#define IDirect3DDevice2_SetLightState(p,a,b) (p)->SetLightState(a,b)
#define IDirect3DDevice2_SetTransform(p,a,b) (p)->SetTransform(a,b)
#define IDirect3DDevice2_GetTransform(p,a,b) (p)->GetTransform(a,b)
#define IDirect3DDevice2_MultiplyTransform(p,a,b) (p)->MultiplyTransform(a,b)
#define IDirect3DDevice2_DrawPrimitive(p,a,b,c,d,e) (p)->DrawPrimitive(a,b,c,d,e)
#define IDirect3DDevice2_DrawIndexedPrimitive(p,a,b,c,d,e,f,g) (p)->DrawIndexedPrimitive(a,b,c,d,e,f,g)
#define IDirect3DDevice2_SetClipStatus(p,a) (p)->SetClipStatus(a)
#define IDirect3DDevice2_GetClipStatus(p,a) (p)->GetClipStatus(a)
#endif
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)
#undef INTERFACE
#define INTERFACE IDirect3DDevice3

DECLARE_INTERFACE_(IDirect3DDevice3, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DDevice3 methods ***/
    STDMETHOD(GetCaps)(THIS_ LPD3DDEVICEDESC,LPD3DDEVICEDESC) PURE;
    STDMETHOD(GetStats)(THIS_ LPD3DSTATS) PURE;
    STDMETHOD(AddViewport)(THIS_ LPDIRECT3DVIEWPORT3) PURE;
    STDMETHOD(DeleteViewport)(THIS_ LPDIRECT3DVIEWPORT3) PURE;
    STDMETHOD(NextViewport)(THIS_ LPDIRECT3DVIEWPORT3,LPDIRECT3DVIEWPORT3*,DWORD) PURE;
    STDMETHOD(EnumTextureFormats)(THIS_ LPD3DENUMPIXELFORMATSCALLBACK,LPVOID) PURE;
    STDMETHOD(BeginScene)(THIS) PURE;
    STDMETHOD(EndScene)(THIS) PURE;
    STDMETHOD(GetDirect3D)(THIS_ LPDIRECT3D3*) PURE;
    STDMETHOD(SetCurrentViewport)(THIS_ LPDIRECT3DVIEWPORT3) PURE;
    STDMETHOD(GetCurrentViewport)(THIS_ LPDIRECT3DVIEWPORT3 *) PURE;
    STDMETHOD(SetRenderTarget)(THIS_ LPDIRECTDRAWSURFACE4,DWORD) PURE;
    STDMETHOD(GetRenderTarget)(THIS_ LPDIRECTDRAWSURFACE4 *) PURE;
    STDMETHOD(Begin)(THIS_ D3DPRIMITIVETYPE,DWORD,DWORD) PURE;
    STDMETHOD(BeginIndexed)(THIS_ D3DPRIMITIVETYPE,DWORD,LPVOID,DWORD,DWORD) PURE;
    STDMETHOD(Vertex)(THIS_ LPVOID) PURE;
    STDMETHOD(Index)(THIS_ WORD) PURE;
    STDMETHOD(End)(THIS_ DWORD) PURE;
    STDMETHOD(GetRenderState)(THIS_ D3DRENDERSTATETYPE,LPDWORD) PURE;
    STDMETHOD(SetRenderState)(THIS_ D3DRENDERSTATETYPE,DWORD) PURE;
    STDMETHOD(GetLightState)(THIS_ D3DLIGHTSTATETYPE,LPDWORD) PURE;
    STDMETHOD(SetLightState)(THIS_ D3DLIGHTSTATETYPE,DWORD) PURE;
    STDMETHOD(SetTransform)(THIS_ D3DTRANSFORMSTATETYPE,LPD3DMATRIX) PURE;
    STDMETHOD(GetTransform)(THIS_ D3DTRANSFORMSTATETYPE,LPD3DMATRIX) PURE;
    STDMETHOD(MultiplyTransform)(THIS_ D3DTRANSFORMSTATETYPE,LPD3DMATRIX) PURE;
    STDMETHOD(DrawPrimitive)(THIS_ D3DPRIMITIVETYPE,DWORD,LPVOID,DWORD,DWORD) PURE;
    STDMETHOD(DrawIndexedPrimitive)(THIS_ D3DPRIMITIVETYPE,DWORD,LPVOID,DWORD,LPWORD,DWORD,DWORD) PURE;
    STDMETHOD(SetClipStatus)(THIS_ LPD3DCLIPSTATUS) PURE;
    STDMETHOD(GetClipStatus)(THIS_ LPD3DCLIPSTATUS) PURE;
    STDMETHOD(DrawPrimitiveStrided)(THIS_ D3DPRIMITIVETYPE,DWORD,LPD3DDRAWPRIMITIVESTRIDEDDATA,DWORD,DWORD) PURE;
    STDMETHOD(DrawIndexedPrimitiveStrided)(THIS_ D3DPRIMITIVETYPE,DWORD,LPD3DDRAWPRIMITIVESTRIDEDDATA,DWORD,LPWORD,DWORD,DWORD) PURE;
    STDMETHOD(DrawPrimitiveVB)(THIS_ D3DPRIMITIVETYPE,LPDIRECT3DVERTEXBUFFER,DWORD,DWORD,DWORD) PURE;
    STDMETHOD(DrawIndexedPrimitiveVB)(THIS_ D3DPRIMITIVETYPE,LPDIRECT3DVERTEXBUFFER,LPWORD,DWORD,DWORD) PURE;
    STDMETHOD(ComputeSphereVisibility)(THIS_ LPD3DVECTOR,LPD3DVALUE,DWORD,DWORD,LPDWORD) PURE;
    STDMETHOD(GetTexture)(THIS_ DWORD,LPDIRECT3DTEXTURE2 *) PURE;
    STDMETHOD(SetTexture)(THIS_ DWORD,LPDIRECT3DTEXTURE2) PURE;
    STDMETHOD(GetTextureStageState)(THIS_ DWORD,D3DTEXTURESTAGESTATETYPE,LPDWORD) PURE;
    STDMETHOD(SetTextureStageState)(THIS_ DWORD,D3DTEXTURESTAGESTATETYPE,DWORD) PURE;
    STDMETHOD(ValidateDevice)(THIS_ LPDWORD) PURE;
};

typedef struct IDirect3DDevice3 *LPDIRECT3DDEVICE3;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DDevice3_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DDevice3_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DDevice3_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DDevice3_GetCaps(p,a,b) (p)->lpVtbl->GetCaps(p,a,b)
#define IDirect3DDevice3_GetStats(p,a) (p)->lpVtbl->GetStats(p,a)
#define IDirect3DDevice3_AddViewport(p,a) (p)->lpVtbl->AddViewport(p,a)
#define IDirect3DDevice3_DeleteViewport(p,a) (p)->lpVtbl->DeleteViewport(p,a)
#define IDirect3DDevice3_NextViewport(p,a,b,c) (p)->lpVtbl->NextViewport(p,a,b,c)
#define IDirect3DDevice3_EnumTextureFormats(p,a,b) (p)->lpVtbl->EnumTextureFormats(p,a,b)
#define IDirect3DDevice3_BeginScene(p) (p)->lpVtbl->BeginScene(p)
#define IDirect3DDevice3_EndScene(p) (p)->lpVtbl->EndScene(p)
#define IDirect3DDevice3_GetDirect3D(p,a) (p)->lpVtbl->GetDirect3D(p,a)
#define IDirect3DDevice3_SetCurrentViewport(p,a) (p)->lpVtbl->SetCurrentViewport(p,a)
#define IDirect3DDevice3_GetCurrentViewport(p,a) (p)->lpVtbl->GetCurrentViewport(p,a)
#define IDirect3DDevice3_SetRenderTarget(p,a,b) (p)->lpVtbl->SetRenderTarget(p,a,b)
#define IDirect3DDevice3_GetRenderTarget(p,a) (p)->lpVtbl->GetRenderTarget(p,a)
#define IDirect3DDevice3_Begin(p,a,b,c) (p)->lpVtbl->Begin(p,a,b,c)
#define IDirect3DDevice3_BeginIndexed(p,a,b,c,d,e) (p)->lpVtbl->BeginIndexed(p,a,b,c,d,e)
#define IDirect3DDevice3_Vertex(p,a) (p)->lpVtbl->Vertex(p,a)
#define IDirect3DDevice3_Index(p,a) (p)->lpVtbl->Index(p,a)
#define IDirect3DDevice3_End(p,a) (p)->lpVtbl->End(p,a)
#define IDirect3DDevice3_GetRenderState(p,a,b) (p)->lpVtbl->GetRenderState(p,a,b)
#define IDirect3DDevice3_SetRenderState(p,a,b) (p)->lpVtbl->SetRenderState(p,a,b)
#define IDirect3DDevice3_GetLightState(p,a,b) (p)->lpVtbl->GetLightState(p,a,b)
#define IDirect3DDevice3_SetLightState(p,a,b) (p)->lpVtbl->SetLightState(p,a,b)
#define IDirect3DDevice3_SetTransform(p,a,b) (p)->lpVtbl->SetTransform(p,a,b)
#define IDirect3DDevice3_GetTransform(p,a,b) (p)->lpVtbl->GetTransform(p,a,b)
#define IDirect3DDevice3_MultiplyTransform(p,a,b) (p)->lpVtbl->MultiplyTransform(p,a,b)
#define IDirect3DDevice3_DrawPrimitive(p,a,b,c,d,e) (p)->lpVtbl->DrawPrimitive(p,a,b,c,d,e)
#define IDirect3DDevice3_DrawIndexedPrimitive(p,a,b,c,d,e,f,g) (p)->lpVtbl->DrawIndexedPrimitive(p,a,b,c,d,e,f,g)
#define IDirect3DDevice3_SetClipStatus(p,a) (p)->lpVtbl->SetClipStatus(p,a)
#define IDirect3DDevice3_GetClipStatus(p,a) (p)->lpVtbl->GetClipStatus(p,a)
#define IDirect3DDevice3_DrawPrimitiveStrided(p,a,b,c,d,e) (p)->lpVtbl->DrawPrimitiveStrided(p,a,b,c,d,e)
#define IDirect3DDevice3_DrawIndexedPrimitiveStrided(p,a,b,c,d,e,f,g) (p)->lpVtbl->DrawIndexedPrimitiveStrided(p,a,b,c,d,e,f,g)
#define IDirect3DDevice3_DrawPrimitiveVB(p,a,b,c,d,e) (p)->lpVtbl->DrawPrimitiveVB(p,a,b,c,d,e)
#define IDirect3DDevice3_DrawIndexedPrimitiveVB(p,a,b,c,d,e) (p)->lpVtbl->DrawIndexedPrimitiveVB(p,a,b,c,d,e)
#define IDirect3DDevice3_ComputeSphereVisibility(p,a,b,c,d,e) (p)->lpVtbl->ComputeSphereVisibility(p,a,b,c,d,e)
#define IDirect3DDevice3_GetTexture(p,a,b) (p)->lpVtbl->GetTexture(p,a,b)
#define IDirect3DDevice3_SetTexture(p,a,b) (p)->lpVtbl->SetTexture(p,a,b)
#define IDirect3DDevice3_GetTextureStageState(p,a,b,c) (p)->lpVtbl->GetTextureStageState(p,a,b,c)
#define IDirect3DDevice3_SetTextureStageState(p,a,b,c) (p)->lpVtbl->SetTextureStageState(p,a,b,c)
#define IDirect3DDevice3_ValidateDevice(p,a) (p)->lpVtbl->ValidateDevice(p,a)
#else
#define IDirect3DDevice3_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DDevice3_AddRef(p) (p)->AddRef()
#define IDirect3DDevice3_Release(p) (p)->Release()
#define IDirect3DDevice3_GetCaps(p,a,b) (p)->GetCaps(a,b)
#define IDirect3DDevice3_GetStats(p,a) (p)->GetStats(a)
#define IDirect3DDevice3_AddViewport(p,a) (p)->AddViewport(a)
#define IDirect3DDevice3_DeleteViewport(p,a) (p)->DeleteViewport(a)
#define IDirect3DDevice3_NextViewport(p,a,b,c) (p)->NextViewport(a,b,c)
#define IDirect3DDevice3_EnumTextureFormats(p,a,b) (p)->EnumTextureFormats(a,b)
#define IDirect3DDevice3_BeginScene(p) (p)->BeginScene()
#define IDirect3DDevice3_EndScene(p) (p)->EndScene()
#define IDirect3DDevice3_GetDirect3D(p,a) (p)->GetDirect3D(a)
#define IDirect3DDevice3_SetCurrentViewport(p,a) (p)->SetCurrentViewport(a)
#define IDirect3DDevice3_GetCurrentViewport(p,a) (p)->GetCurrentViewport(a)
#define IDirect3DDevice3_SetRenderTarget(p,a,b) (p)->SetRenderTarget(a,b)
#define IDirect3DDevice3_GetRenderTarget(p,a) (p)->GetRenderTarget(a)
#define IDirect3DDevice3_Begin(p,a,b,c) (p)->Begin(a,b,c)
#define IDirect3DDevice3_BeginIndexed(p,a,b,c,d,e) (p)->BeginIndexed(a,b,c,d,e)
#define IDirect3DDevice3_Vertex(p,a) (p)->Vertex(a)
#define IDirect3DDevice3_Index(p,a) (p)->Index(a)
#define IDirect3DDevice3_End(p,a) (p)->End(a)
#define IDirect3DDevice3_GetRenderState(p,a,b) (p)->GetRenderState(a,b)
#define IDirect3DDevice3_SetRenderState(p,a,b) (p)->SetRenderState(a,b)
#define IDirect3DDevice3_GetLightState(p,a,b) (p)->GetLightState(a,b)
#define IDirect3DDevice3_SetLightState(p,a,b) (p)->SetLightState(a,b)
#define IDirect3DDevice3_SetTransform(p,a,b) (p)->SetTransform(a,b)
#define IDirect3DDevice3_GetTransform(p,a,b) (p)->GetTransform(a,b)
#define IDirect3DDevice3_MultiplyTransform(p,a,b) (p)->MultiplyTransform(a,b)
#define IDirect3DDevice3_DrawPrimitive(p,a,b,c,d,e) (p)->DrawPrimitive(a,b,c,d,e)
#define IDirect3DDevice3_DrawIndexedPrimitive(p,a,b,c,d,e,f,g) (p)->DrawIndexedPrimitive(a,b,c,d,e,f,g)
#define IDirect3DDevice3_SetClipStatus(p,a) (p)->SetClipStatus(a)
#define IDirect3DDevice3_GetClipStatus(p,a) (p)->GetClipStatus(a)
#define IDirect3DDevice3_DrawPrimitiveStrided(p,a,b,c,d,e) (p)->DrawPrimitiveStrided(a,b,c,d,e)
#define IDirect3DDevice3_DrawIndexedPrimitiveStrided(p,a,b,c,d,e,f,g) (p)->DrawIndexedPrimitiveStrided(a,b,c,d,e,f,g)
#define IDirect3DDevice3_DrawPrimitiveVB(p,a,b,c,d,e) (p)->DrawPrimitiveVB(a,b,c,d,e)
#define IDirect3DDevice3_DrawIndexedPrimitiveVB(p,a,b,c,d,e) (p)->DrawIndexedPrimitiveVB(a,b,c,d,e)
#define IDirect3DDevice3_ComputeSphereVisibility(p,a,b,c,d,e) (p)->ComputeSphereVisibility(a,b,c,d,e)
#define IDirect3DDevice3_GetTexture(p,a,b) (p)->GetTexture(a,b)
#define IDirect3DDevice3_SetTexture(p,a,b) (p)->SetTexture(a,b)
#define IDirect3DDevice3_GetTextureStageState(p,a,b,c) (p)->GetTextureStageState(a,b,c)
#define IDirect3DDevice3_SetTextureStageState(p,a,b,c) (p)->SetTextureStageState(a,b,c)
#define IDirect3DDevice3_ValidateDevice(p,a) (p)->ValidateDevice(a)
#endif
#endif /* DIRECT3D_VERSION >= 0x0600 */

#if(DIRECT3D_VERSION >= 0x0700)
#undef INTERFACE
#define INTERFACE IDirect3DDevice7

DECLARE_INTERFACE_(IDirect3DDevice7, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DDevice7 methods ***/
    STDMETHOD(GetCaps)(THIS_ LPD3DDEVICEDESC7) PURE;
    STDMETHOD(EnumTextureFormats)(THIS_ LPD3DENUMPIXELFORMATSCALLBACK,LPVOID) PURE;
    STDMETHOD(BeginScene)(THIS) PURE;
    STDMETHOD(EndScene)(THIS) PURE;
    STDMETHOD(GetDirect3D)(THIS_ LPDIRECT3D7*) PURE;
    STDMETHOD(SetRenderTarget)(THIS_ LPDIRECTDRAWSURFACE7,DWORD) PURE;
    STDMETHOD(GetRenderTarget)(THIS_ LPDIRECTDRAWSURFACE7 *) PURE;
    STDMETHOD(Clear)(THIS_ DWORD,LPD3DRECT,DWORD,D3DCOLOR,D3DVALUE,DWORD) PURE;
    STDMETHOD(SetTransform)(THIS_ D3DTRANSFORMSTATETYPE,LPD3DMATRIX) PURE;
    STDMETHOD(GetTransform)(THIS_ D3DTRANSFORMSTATETYPE,LPD3DMATRIX) PURE;
    STDMETHOD(SetViewport)(THIS_ LPD3DVIEWPORT7) PURE;
    STDMETHOD(MultiplyTransform)(THIS_ D3DTRANSFORMSTATETYPE,LPD3DMATRIX) PURE;
    STDMETHOD(GetViewport)(THIS_ LPD3DVIEWPORT7) PURE;
    STDMETHOD(SetMaterial)(THIS_ LPD3DMATERIAL7) PURE;
    STDMETHOD(GetMaterial)(THIS_ LPD3DMATERIAL7) PURE;
    STDMETHOD(SetLight)(THIS_ DWORD,LPD3DLIGHT7) PURE;
    STDMETHOD(GetLight)(THIS_ DWORD,LPD3DLIGHT7) PURE;
    STDMETHOD(SetRenderState)(THIS_ D3DRENDERSTATETYPE,DWORD) PURE;
    STDMETHOD(GetRenderState)(THIS_ D3DRENDERSTATETYPE,LPDWORD) PURE;
    STDMETHOD(BeginStateBlock)(THIS) PURE;
    STDMETHOD(EndStateBlock)(THIS_ LPDWORD) PURE;
    STDMETHOD(PreLoad)(THIS_ LPDIRECTDRAWSURFACE7) PURE;
    STDMETHOD(DrawPrimitive)(THIS_ D3DPRIMITIVETYPE,DWORD,LPVOID,DWORD,DWORD) PURE;
    STDMETHOD(DrawIndexedPrimitive)(THIS_ D3DPRIMITIVETYPE,DWORD,LPVOID,DWORD,LPWORD,DWORD,DWORD) PURE;
    STDMETHOD(SetClipStatus)(THIS_ LPD3DCLIPSTATUS) PURE;
    STDMETHOD(GetClipStatus)(THIS_ LPD3DCLIPSTATUS) PURE;
    STDMETHOD(DrawPrimitiveStrided)(THIS_ D3DPRIMITIVETYPE,DWORD,LPD3DDRAWPRIMITIVESTRIDEDDATA,DWORD,DWORD) PURE;
    STDMETHOD(DrawIndexedPrimitiveStrided)(THIS_ D3DPRIMITIVETYPE,DWORD,LPD3DDRAWPRIMITIVESTRIDEDDATA,DWORD,LPWORD,DWORD,DWORD) PURE;
    STDMETHOD(DrawPrimitiveVB)(THIS_ D3DPRIMITIVETYPE,LPDIRECT3DVERTEXBUFFER7,DWORD,DWORD,DWORD) PURE;
    STDMETHOD(DrawIndexedPrimitiveVB)(THIS_ D3DPRIMITIVETYPE,LPDIRECT3DVERTEXBUFFER7,DWORD,DWORD,LPWORD,DWORD,DWORD) PURE;
    STDMETHOD(ComputeSphereVisibility)(THIS_ LPD3DVECTOR,LPD3DVALUE,DWORD,DWORD,LPDWORD) PURE;
    STDMETHOD(GetTexture)(THIS_ DWORD,LPDIRECTDRAWSURFACE7 *) PURE;
    STDMETHOD(SetTexture)(THIS_ DWORD,LPDIRECTDRAWSURFACE7) PURE;
    STDMETHOD(GetTextureStageState)(THIS_ DWORD,D3DTEXTURESTAGESTATETYPE,LPDWORD) PURE;
    STDMETHOD(SetTextureStageState)(THIS_ DWORD,D3DTEXTURESTAGESTATETYPE,DWORD) PURE;
    STDMETHOD(ValidateDevice)(THIS_ LPDWORD) PURE;
    STDMETHOD(ApplyStateBlock)(THIS_ DWORD) PURE;
    STDMETHOD(CaptureStateBlock)(THIS_ DWORD) PURE;
    STDMETHOD(DeleteStateBlock)(THIS_ DWORD) PURE;
    STDMETHOD(CreateStateBlock)(THIS_ D3DSTATEBLOCKTYPE,LPDWORD) PURE;
    STDMETHOD(Load)(THIS_ LPDIRECTDRAWSURFACE7,LPPOINT,LPDIRECTDRAWSURFACE7,LPRECT,DWORD) PURE;
    STDMETHOD(LightEnable)(THIS_ DWORD,BOOL) PURE;
    STDMETHOD(GetLightEnable)(THIS_ DWORD,BOOL*) PURE;
    STDMETHOD(SetClipPlane)(THIS_ DWORD,D3DVALUE*) PURE;
    STDMETHOD(GetClipPlane)(THIS_ DWORD,D3DVALUE*) PURE;
    STDMETHOD(GetInfo)(THIS_ DWORD,LPVOID,DWORD) PURE;
};

typedef struct IDirect3DDevice7 *LPDIRECT3DDEVICE7;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DDevice7_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DDevice7_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DDevice7_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DDevice7_GetCaps(p,a) (p)->lpVtbl->GetCaps(p,a)
#define IDirect3DDevice7_EnumTextureFormats(p,a,b) (p)->lpVtbl->EnumTextureFormats(p,a,b)
#define IDirect3DDevice7_BeginScene(p) (p)->lpVtbl->BeginScene(p)
#define IDirect3DDevice7_EndScene(p) (p)->lpVtbl->EndScene(p)
#define IDirect3DDevice7_GetDirect3D(p,a) (p)->lpVtbl->GetDirect3D(p,a)
#define IDirect3DDevice7_SetRenderTarget(p,a,b) (p)->lpVtbl->SetRenderTarget(p,a,b)
#define IDirect3DDevice7_GetRenderTarget(p,a) (p)->lpVtbl->GetRenderTarget(p,a)
#define IDirect3DDevice7_Clear(p,a,b,c,d,e,f) (p)->lpVtbl->Clear(p,a,b,c,d,e,f)
#define IDirect3DDevice7_SetTransform(p,a,b) (p)->lpVtbl->SetTransform(p,a,b)
#define IDirect3DDevice7_GetTransform(p,a,b) (p)->lpVtbl->GetTransform(p,a,b)
#define IDirect3DDevice7_SetViewport(p,a) (p)->lpVtbl->SetViewport(p,a)
#define IDirect3DDevice7_MultiplyTransform(p,a,b) (p)->lpVtbl->MultiplyTransform(p,a,b)
#define IDirect3DDevice7_GetViewport(p,a) (p)->lpVtbl->GetViewport(p,a)
#define IDirect3DDevice7_SetMaterial(p,a) (p)->lpVtbl->SetMaterial(p,a)
#define IDirect3DDevice7_GetMaterial(p,a) (p)->lpVtbl->GetMaterial(p,a)
#define IDirect3DDevice7_SetLight(p,a,b) (p)->lpVtbl->SetLight(p,a,b)
#define IDirect3DDevice7_GetLight(p,a,b) (p)->lpVtbl->GetLight(p,a,b)
#define IDirect3DDevice7_SetRenderState(p,a,b) (p)->lpVtbl->SetRenderState(p,a,b)
#define IDirect3DDevice7_GetRenderState(p,a,b) (p)->lpVtbl->GetRenderState(p,a,b)
#define IDirect3DDevice7_BeginStateBlock(p) (p)->lpVtbl->BeginStateBlock(p)
#define IDirect3DDevice7_EndStateBlock(p,a) (p)->lpVtbl->EndStateBlock(p,a)
#define IDirect3DDevice7_PreLoad(p,a) (p)->lpVtbl->PreLoad(p,a)
#define IDirect3DDevice7_DrawPrimitive(p,a,b,c,d,e) (p)->lpVtbl->DrawPrimitive(p,a,b,c,d,e)
#define IDirect3DDevice7_DrawIndexedPrimitive(p,a,b,c,d,e,f,g) (p)->lpVtbl->DrawIndexedPrimitive(p,a,b,c,d,e,f,g)
#define IDirect3DDevice7_SetClipStatus(p,a) (p)->lpVtbl->SetClipStatus(p,a)
#define IDirect3DDevice7_GetClipStatus(p,a) (p)->lpVtbl->GetClipStatus(p,a)
#define IDirect3DDevice7_DrawPrimitiveStrided(p,a,b,c,d,e) (p)->lpVtbl->DrawPrimitiveStrided(p,a,b,c,d,e)
#define IDirect3DDevice7_DrawIndexedPrimitiveStrided(p,a,b,c,d,e,f,g) (p)->lpVtbl->DrawIndexedPrimitiveStrided(p,a,b,c,d,e,f,g)
#define IDirect3DDevice7_DrawPrimitiveVB(p,a,b,c,d,e) (p)->lpVtbl->DrawPrimitiveVB(p,a,b,c,d,e)
#define IDirect3DDevice7_DrawIndexedPrimitiveVB(p,a,b,c,d,e,f,g) (p)->lpVtbl->DrawIndexedPrimitiveVB(p,a,b,c,d,e,f,g)
#define IDirect3DDevice7_ComputeSphereVisibility(p,a,b,c,d,e) (p)->lpVtbl->ComputeSphereVisibility(p,a,b,c,d,e)
#define IDirect3DDevice7_GetTexture(p,a,b) (p)->lpVtbl->GetTexture(p,a,b)
#define IDirect3DDevice7_SetTexture(p,a,b) (p)->lpVtbl->SetTexture(p,a,b)
#define IDirect3DDevice7_GetTextureStageState(p,a,b,c) (p)->lpVtbl->GetTextureStageState(p,a,b,c)
#define IDirect3DDevice7_SetTextureStageState(p,a,b,c) (p)->lpVtbl->SetTextureStageState(p,a,b,c)
#define IDirect3DDevice7_ValidateDevice(p,a) (p)->lpVtbl->ValidateDevice(p,a)
#define IDirect3DDevice7_ApplyStateBlock(p,a) (p)->lpVtbl->ApplyStateBlock(p,a)
#define IDirect3DDevice7_CaptureStateBlock(p,a) (p)->lpVtbl->CaptureStateBlock(p,a)
#define IDirect3DDevice7_DeleteStateBlock(p,a) (p)->lpVtbl->DeleteStateBlock(p,a)
#define IDirect3DDevice7_CreateStateBlock(p,a,b) (p)->lpVtbl->CreateStateBlock(p,a,b)
#define IDirect3DDevice7_Load(p,a,b,c,d,e) (p)->lpVtbl->Load(p,a,b,c,d,e)
#define IDirect3DDevice7_LightEnable(p,a,b) (p)->lpVtbl->LightEnable(p,a,b)
#define IDirect3DDevice7_GetLightEnable(p,a,b) (p)->lpVtbl->GetLightEnable(p,a,b)
#define IDirect3DDevice7_SetClipPlane(p,a,b) (p)->lpVtbl->SetClipPlane(p,a,b)
#define IDirect3DDevice7_GetClipPlane(p,a,b) (p)->lpVtbl->GetClipPlane(p,a,b)
#define IDirect3DDevice7_GetInfo(p,a,b,c) (p)->lpVtbl->GetInfo(p,a,b,c)
#else
#define IDirect3DDevice7_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DDevice7_AddRef(p) (p)->AddRef()
#define IDirect3DDevice7_Release(p) (p)->Release()
#define IDirect3DDevice7_GetCaps(p,a) (p)->GetCaps(a)
#define IDirect3DDevice7_EnumTextureFormats(p,a,b) (p)->EnumTextureFormats(a,b)
#define IDirect3DDevice7_BeginScene(p) (p)->BeginScene()
#define IDirect3DDevice7_EndScene(p) (p)->EndScene()
#define IDirect3DDevice7_GetDirect3D(p,a) (p)->GetDirect3D(a)
#define IDirect3DDevice7_SetRenderTarget(p,a,b) (p)->SetRenderTarget(a,b)
#define IDirect3DDevice7_GetRenderTarget(p,a) (p)->GetRenderTarget(a)
#define IDirect3DDevice7_Clear(p,a,b,c,d,e,f) (p)->Clear(a,b,c,d,e,f)
#define IDirect3DDevice7_SetTransform(p,a,b) (p)->SetTransform(a,b)
#define IDirect3DDevice7_GetTransform(p,a,b) (p)->GetTransform(a,b)
#define IDirect3DDevice7_SetViewport(p,a) (p)->SetViewport(a)
#define IDirect3DDevice7_MultiplyTransform(p,a,b) (p)->MultiplyTransform(a,b)
#define IDirect3DDevice7_GetViewport(p,a) (p)->GetViewport(a)
#define IDirect3DDevice7_SetMaterial(p,a) (p)->SetMaterial(a)
#define IDirect3DDevice7_GetMaterial(p,a) (p)->GetMaterial(a)
#define IDirect3DDevice7_SetLight(p,a,b) (p)->SetLight(a,b)
#define IDirect3DDevice7_GetLight(p,a,b) (p)->GetLight(a,b)
#define IDirect3DDevice7_SetRenderState(p,a,b) (p)->SetRenderState(a,b)
#define IDirect3DDevice7_GetRenderState(p,a,b) (p)->GetRenderState(a,b)
#define IDirect3DDevice7_BeginStateBlock(p) (p)->BeginStateBlock()
#define IDirect3DDevice7_EndStateBlock(p,a) (p)->EndStateBlock(a)
#define IDirect3DDevice7_PreLoad(p,a) (p)->PreLoad(a)
#define IDirect3DDevice7_DrawPrimitive(p,a,b,c,d,e) (p)->DrawPrimitive(a,b,c,d,e)
#define IDirect3DDevice7_DrawIndexedPrimitive(p,a,b,c,d,e,f,g) (p)->DrawIndexedPrimitive(a,b,c,d,e,f,g)
#define IDirect3DDevice7_SetClipStatus(p,a) (p)->SetClipStatus(a)
#define IDirect3DDevice7_GetClipStatus(p,a) (p)->GetClipStatus(a)
#define IDirect3DDevice7_DrawPrimitiveStrided(p,a,b,c,d,e) (p)->DrawPrimitiveStrided(a,b,c,d,e)
#define IDirect3DDevice7_DrawIndexedPrimitiveStrided(p,a,b,c,d,e,f,g) (p)->DrawIndexedPrimitiveStrided(a,b,c,d,e,f,g)
#define IDirect3DDevice7_DrawPrimitiveVB(p,a,b,c,d,e) (p)->DrawPrimitiveVB(a,b,c,d,e)
#define IDirect3DDevice7_DrawIndexedPrimitiveVB(p,a,b,c,d,e,f,g) (p)->DrawIndexedPrimitiveVB(a,b,c,d,e,f,g)
#define IDirect3DDevice7_ComputeSphereVisibility(p,a,b,c,d,e) (p)->ComputeSphereVisibility(a,b,c,d,e)
#define IDirect3DDevice7_GetTexture(p,a,b) (p)->GetTexture(a,b)
#define IDirect3DDevice7_SetTexture(p,a,b) (p)->SetTexture(a,b)
#define IDirect3DDevice7_GetTextureStageState(p,a,b,c) (p)->GetTextureStageState(a,b,c)
#define IDirect3DDevice7_SetTextureStageState(p,a,b,c) (p)->SetTextureStageState(a,b,c)
#define IDirect3DDevice7_ValidateDevice(p,a) (p)->ValidateDevice(a)
#define IDirect3DDevice7_ApplyStateBlock(p,a) (p)->ApplyStateBlock(a)
#define IDirect3DDevice7_CaptureStateBlock(p,a) (p)->CaptureStateBlock(a)
#define IDirect3DDevice7_DeleteStateBlock(p,a) (p)->DeleteStateBlock(a)
#define IDirect3DDevice7_CreateStateBlock(p,a,b) (p)->CreateStateBlock(a,b)
#define IDirect3DDevice7_Load(p,a,b,c,d,e) (p)->Load(a,b,c,d,e)
#define IDirect3DDevice7_LightEnable(p,a,b) (p)->LightEnable(a,b)
#define IDirect3DDevice7_GetLightEnable(p,a,b) (p)->GetLightEnable(a,b)
#define IDirect3DDevice7_SetClipPlane(p,a,b) (p)->SetClipPlane(a,b)
#define IDirect3DDevice7_GetClipPlane(p,a,b) (p)->GetClipPlane(a,b)
#define IDirect3DDevice7_GetInfo(p,a,b,c) (p)->GetInfo(a,b,c)
#endif
#endif /* DIRECT3D_VERSION >= 0x0700 */

/*
 * Execute Buffer interface
 */
#undef INTERFACE
#define INTERFACE IDirect3DExecuteBuffer

DECLARE_INTERFACE_(IDirect3DExecuteBuffer, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DExecuteBuffer methods ***/
    STDMETHOD(Initialize)(THIS_ LPDIRECT3DDEVICE,LPD3DEXECUTEBUFFERDESC) PURE;
    STDMETHOD(Lock)(THIS_ LPD3DEXECUTEBUFFERDESC) PURE;
    STDMETHOD(Unlock)(THIS) PURE;
    STDMETHOD(SetExecuteData)(THIS_ LPD3DEXECUTEDATA) PURE;
    STDMETHOD(GetExecuteData)(THIS_ LPD3DEXECUTEDATA) PURE;
    STDMETHOD(Validate)(THIS_ LPDWORD,LPD3DVALIDATECALLBACK,LPVOID,DWORD) PURE;
    STDMETHOD(Optimize)(THIS_ DWORD) PURE;
};

typedef struct IDirect3DExecuteBuffer *LPDIRECT3DEXECUTEBUFFER;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DExecuteBuffer_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DExecuteBuffer_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DExecuteBuffer_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DExecuteBuffer_Initialize(p,a,b) (p)->lpVtbl->Initialize(p,a,b)
#define IDirect3DExecuteBuffer_Lock(p,a) (p)->lpVtbl->Lock(p,a)
#define IDirect3DExecuteBuffer_Unlock(p) (p)->lpVtbl->Unlock(p)
#define IDirect3DExecuteBuffer_SetExecuteData(p,a) (p)->lpVtbl->SetExecuteData(p,a)
#define IDirect3DExecuteBuffer_GetExecuteData(p,a) (p)->lpVtbl->GetExecuteData(p,a)
#define IDirect3DExecuteBuffer_Validate(p,a,b,c,d) (p)->lpVtbl->Validate(p,a,b,c,d)
#define IDirect3DExecuteBuffer_Optimize(p,a) (p)->lpVtbl->Optimize(p,a)
#else
#define IDirect3DExecuteBuffer_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DExecuteBuffer_AddRef(p) (p)->AddRef()
#define IDirect3DExecuteBuffer_Release(p) (p)->Release()
#define IDirect3DExecuteBuffer_Initialize(p,a,b) (p)->Initialize(a,b)
#define IDirect3DExecuteBuffer_Lock(p,a) (p)->Lock(a)
#define IDirect3DExecuteBuffer_Unlock(p) (p)->Unlock()
#define IDirect3DExecuteBuffer_SetExecuteData(p,a) (p)->SetExecuteData(a)
#define IDirect3DExecuteBuffer_GetExecuteData(p,a) (p)->GetExecuteData(a)
#define IDirect3DExecuteBuffer_Validate(p,a,b,c,d) (p)->Validate(a,b,c,d)
#define IDirect3DExecuteBuffer_Optimize(p,a) (p)->Optimize(a)
#endif

/*
 * Light interfaces
 */
#undef INTERFACE
#define INTERFACE IDirect3DLight

DECLARE_INTERFACE_(IDirect3DLight, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DLight methods ***/
    STDMETHOD(Initialize)(THIS_ LPDIRECT3D) PURE;
    STDMETHOD(SetLight)(THIS_ LPD3DLIGHT) PURE;
    STDMETHOD(GetLight)(THIS_ LPD3DLIGHT) PURE;
};

typedef struct IDirect3DLight *LPDIRECT3DLIGHT;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DLight_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DLight_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DLight_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DLight_Initialize(p,a) (p)->lpVtbl->Initialize(p,a)
#define IDirect3DLight_SetLight(p,a) (p)->lpVtbl->SetLight(p,a)
#define IDirect3DLight_GetLight(p,a) (p)->lpVtbl->GetLight(p,a)
#else
#define IDirect3DLight_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DLight_AddRef(p) (p)->AddRef()
#define IDirect3DLight_Release(p) (p)->Release()
#define IDirect3DLight_Initialize(p,a) (p)->Initialize(a)
#define IDirect3DLight_SetLight(p,a) (p)->SetLight(a)
#define IDirect3DLight_GetLight(p,a) (p)->GetLight(a)
#endif

/*
 * Material interfaces
 */
#undef INTERFACE
#define INTERFACE IDirect3DMaterial

DECLARE_INTERFACE_(IDirect3DMaterial, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DMaterial methods ***/
    STDMETHOD(Initialize)(THIS_ LPDIRECT3D) PURE;
    STDMETHOD(SetMaterial)(THIS_ LPD3DMATERIAL) PURE;
    STDMETHOD(GetMaterial)(THIS_ LPD3DMATERIAL) PURE;
    STDMETHOD(GetHandle)(THIS_ LPDIRECT3DDEVICE,LPD3DMATERIALHANDLE) PURE;
    STDMETHOD(Reserve)(THIS) PURE;
    STDMETHOD(Unreserve)(THIS) PURE;
};

typedef struct IDirect3DMaterial *LPDIRECT3DMATERIAL;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DMaterial_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DMaterial_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DMaterial_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DMaterial_Initialize(p,a) (p)->lpVtbl->Initialize(p,a)
#define IDirect3DMaterial_SetMaterial(p,a) (p)->lpVtbl->SetMaterial(p,a)
#define IDirect3DMaterial_GetMaterial(p,a) (p)->lpVtbl->GetMaterial(p,a)
#define IDirect3DMaterial_GetHandle(p,a,b) (p)->lpVtbl->GetHandle(p,a,b)
#define IDirect3DMaterial_Reserve(p) (p)->lpVtbl->Reserve(p)
#define IDirect3DMaterial_Unreserve(p) (p)->lpVtbl->Unreserve(p)
#else
#define IDirect3DMaterial_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DMaterial_AddRef(p) (p)->AddRef()
#define IDirect3DMaterial_Release(p) (p)->Release()
#define IDirect3DMaterial_Initialize(p,a) (p)->Initialize(a)
#define IDirect3DMaterial_SetMaterial(p,a) (p)->SetMaterial(a)
#define IDirect3DMaterial_GetMaterial(p,a) (p)->GetMaterial(a)
#define IDirect3DMaterial_GetHandle(p,a,b) (p)->GetHandle(a,b)
#define IDirect3DMaterial_Reserve(p) (p)->Reserve()
#define IDirect3DMaterial_Unreserve(p) (p)->Unreserve()
#endif

#if(DIRECT3D_VERSION >= 0x0500)
#undef INTERFACE
#define INTERFACE IDirect3DMaterial2

DECLARE_INTERFACE_(IDirect3DMaterial2, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DMaterial2 methods ***/
    STDMETHOD(SetMaterial)(THIS_ LPD3DMATERIAL) PURE;
    STDMETHOD(GetMaterial)(THIS_ LPD3DMATERIAL) PURE;
    STDMETHOD(GetHandle)(THIS_ LPDIRECT3DDEVICE2,LPD3DMATERIALHANDLE) PURE;
};

typedef struct IDirect3DMaterial2 *LPDIRECT3DMATERIAL2;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DMaterial2_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DMaterial2_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DMaterial2_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DMaterial2_SetMaterial(p,a) (p)->lpVtbl->SetMaterial(p,a)
#define IDirect3DMaterial2_GetMaterial(p,a) (p)->lpVtbl->GetMaterial(p,a)
#define IDirect3DMaterial2_GetHandle(p,a,b) (p)->lpVtbl->GetHandle(p,a,b)
#else
#define IDirect3DMaterial2_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DMaterial2_AddRef(p) (p)->AddRef()
#define IDirect3DMaterial2_Release(p) (p)->Release()
#define IDirect3DMaterial2_SetMaterial(p,a) (p)->SetMaterial(a)
#define IDirect3DMaterial2_GetMaterial(p,a) (p)->GetMaterial(a)
#define IDirect3DMaterial2_GetHandle(p,a,b) (p)->GetHandle(a,b)
#endif
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)
#undef INTERFACE
#define INTERFACE IDirect3DMaterial3

DECLARE_INTERFACE_(IDirect3DMaterial3, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DMaterial3 methods ***/
    STDMETHOD(SetMaterial)(THIS_ LPD3DMATERIAL) PURE;
    STDMETHOD(GetMaterial)(THIS_ LPD3DMATERIAL) PURE;
    STDMETHOD(GetHandle)(THIS_ LPDIRECT3DDEVICE3,LPD3DMATERIALHANDLE) PURE;
};

typedef struct IDirect3DMaterial3 *LPDIRECT3DMATERIAL3;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DMaterial3_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DMaterial3_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DMaterial3_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DMaterial3_SetMaterial(p,a) (p)->lpVtbl->SetMaterial(p,a)
#define IDirect3DMaterial3_GetMaterial(p,a) (p)->lpVtbl->GetMaterial(p,a)
#define IDirect3DMaterial3_GetHandle(p,a,b) (p)->lpVtbl->GetHandle(p,a,b)
#else
#define IDirect3DMaterial3_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DMaterial3_AddRef(p) (p)->AddRef()
#define IDirect3DMaterial3_Release(p) (p)->Release()
#define IDirect3DMaterial3_SetMaterial(p,a) (p)->SetMaterial(a)
#define IDirect3DMaterial3_GetMaterial(p,a) (p)->GetMaterial(a)
#define IDirect3DMaterial3_GetHandle(p,a,b) (p)->GetHandle(a,b)
#endif
#endif /* DIRECT3D_VERSION >= 0x0600 */

/*
 * Texture interfaces
 */
#undef INTERFACE
#define INTERFACE IDirect3DTexture

DECLARE_INTERFACE_(IDirect3DTexture, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DTexture methods ***/
    STDMETHOD(Initialize)(THIS_ LPDIRECT3DDEVICE,LPDIRECTDRAWSURFACE) PURE;
    STDMETHOD(GetHandle)(THIS_ LPDIRECT3DDEVICE,LPD3DTEXTUREHANDLE) PURE;
    STDMETHOD(PaletteChanged)(THIS_ DWORD,DWORD) PURE;
    STDMETHOD(Load)(THIS_ LPDIRECT3DTEXTURE) PURE;
    STDMETHOD(Unload)(THIS) PURE;
};

typedef struct IDirect3DTexture *LPDIRECT3DTEXTURE;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DTexture_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DTexture_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DTexture_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DTexture_Initialize(p,a,b) (p)->lpVtbl->Initialize(p,a,b)
#define IDirect3DTexture_GetHandle(p,a,b) (p)->lpVtbl->GetHandle(p,a,b)
#define IDirect3DTexture_PaletteChanged(p,a,b) (p)->lpVtbl->PaletteChanged(p,a,b)
#define IDirect3DTexture_Load(p,a) (p)->lpVtbl->Load(p,a)
#define IDirect3DTexture_Unload(p) (p)->lpVtbl->Unload(p)
#else
#define IDirect3DTexture_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DTexture_AddRef(p) (p)->AddRef()
#define IDirect3DTexture_Release(p) (p)->Release()
#define IDirect3DTexture_Initialize(p,a,b) (p)->Initialize(a,b)
#define IDirect3DTexture_GetHandle(p,a,b) (p)->GetHandle(a,b)
#define IDirect3DTexture_PaletteChanged(p,a,b) (p)->PaletteChanged(a,b)
#define IDirect3DTexture_Load(p,a) (p)->Load(a)
#define IDirect3DTexture_Unload(p) (p)->Unload()
#endif

#if(DIRECT3D_VERSION >= 0x0500)
#undef INTERFACE
#define INTERFACE IDirect3DTexture2

DECLARE_INTERFACE_(IDirect3DTexture2, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DTexture2 methods ***/
    STDMETHOD(GetHandle)(THIS_ LPDIRECT3DDEVICE2,LPD3DTEXTUREHANDLE) PURE;
    STDMETHOD(PaletteChanged)(THIS_ DWORD,DWORD) PURE;
    STDMETHOD(Load)(THIS_ LPDIRECT3DTEXTURE2) PURE;
};

typedef struct IDirect3DTexture2 *LPDIRECT3DTEXTURE2;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DTexture2_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DTexture2_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DTexture2_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DTexture2_GetHandle(p,a,b) (p)->lpVtbl->GetHandle(p,a,b)
#define IDirect3DTexture2_PaletteChanged(p,a,b) (p)->lpVtbl->PaletteChanged(p,a,b)
#define IDirect3DTexture2_Load(p,a) (p)->lpVtbl->Load(p,a)
#else
#define IDirect3DTexture2_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DTexture2_AddRef(p) (p)->AddRef()
#define IDirect3DTexture2_Release(p) (p)->Release()
#define IDirect3DTexture2_GetHandle(p,a,b) (p)->GetHandle(a,b)
#define IDirect3DTexture2_PaletteChanged(p,a,b) (p)->PaletteChanged(a,b)
#define IDirect3DTexture2_Load(p,a) (p)->Load(a)
#endif
#endif /* DIRECT3D_VERSION >= 0x0500 */

/*
 * Viewport interfaces
 */
#undef INTERFACE
#define INTERFACE IDirect3DViewport

DECLARE_INTERFACE_(IDirect3DViewport, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DViewport methods ***/
    STDMETHOD(Initialize)(THIS_ LPDIRECT3D) PURE;
    STDMETHOD(GetViewport)(THIS_ LPD3DVIEWPORT) PURE;
    STDMETHOD(SetViewport)(THIS_ LPD3DVIEWPORT) PURE;
    STDMETHOD(TransformVertices)(THIS_ DWORD,LPD3DTRANSFORMDATA,DWORD,LPDWORD) PURE;
    STDMETHOD(LightElements)(THIS_ DWORD,LPD3DLIGHTDATA) PURE;
    STDMETHOD(SetBackground)(THIS_ D3DMATERIALHANDLE) PURE;
    STDMETHOD(GetBackground)(THIS_ LPD3DMATERIALHANDLE,LPBOOL) PURE;
    STDMETHOD(SetBackgroundDepth)(THIS_ LPDIRECTDRAWSURFACE) PURE;
    STDMETHOD(GetBackgroundDepth)(THIS_ LPDIRECTDRAWSURFACE*,LPBOOL) PURE;
    STDMETHOD(Clear)(THIS_ DWORD,LPD3DRECT,DWORD) PURE;
    STDMETHOD(AddLight)(THIS_ LPDIRECT3DLIGHT) PURE;
    STDMETHOD(DeleteLight)(THIS_ LPDIRECT3DLIGHT) PURE;
    STDMETHOD(NextLight)(THIS_ LPDIRECT3DLIGHT,LPDIRECT3DLIGHT*,DWORD) PURE;
};

typedef struct IDirect3DViewport *LPDIRECT3DVIEWPORT;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DViewport_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DViewport_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DViewport_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DViewport_Initialize(p,a) (p)->lpVtbl->Initialize(p,a)
#define IDirect3DViewport_GetViewport(p,a) (p)->lpVtbl->GetViewport(p,a)
#define IDirect3DViewport_SetViewport(p,a) (p)->lpVtbl->SetViewport(p,a)
#define IDirect3DViewport_TransformVertices(p,a,b,c,d) (p)->lpVtbl->TransformVertices(p,a,b,c,d)
#define IDirect3DViewport_LightElements(p,a,b) (p)->lpVtbl->LightElements(p,a,b)
#define IDirect3DViewport_SetBackground(p,a) (p)->lpVtbl->SetBackground(p,a)
#define IDirect3DViewport_GetBackground(p,a,b) (p)->lpVtbl->GetBackground(p,a,b)
#define IDirect3DViewport_SetBackgroundDepth(p,a) (p)->lpVtbl->SetBackgroundDepth(p,a)
#define IDirect3DViewport_GetBackgroundDepth(p,a,b) (p)->lpVtbl->GetBackgroundDepth(p,a,b)
#define IDirect3DViewport_Clear(p,a,b,c) (p)->lpVtbl->Clear(p,a,b,c)
#define IDirect3DViewport_AddLight(p,a) (p)->lpVtbl->AddLight(p,a)
#define IDirect3DViewport_DeleteLight(p,a) (p)->lpVtbl->DeleteLight(p,a)
#define IDirect3DViewport_NextLight(p,a,b,c) (p)->lpVtbl->NextLight(p,a,b,c)
#else
#define IDirect3DViewport_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DViewport_AddRef(p) (p)->AddRef()
#define IDirect3DViewport_Release(p) (p)->Release()
#define IDirect3DViewport_Initialize(p,a) (p)->Initialize(a)
#define IDirect3DViewport_GetViewport(p,a) (p)->GetViewport(a)
#define IDirect3DViewport_SetViewport(p,a) (p)->SetViewport(a)
#define IDirect3DViewport_TransformVertices(p,a,b,c,d) (p)->TransformVertices(a,b,c,d)
#define IDirect3DViewport_LightElements(p,a,b) (p)->LightElements(a,b)
#define IDirect3DViewport_SetBackground(p,a) (p)->SetBackground(a)
#define IDirect3DViewport_GetBackground(p,a,b) (p)->GetBackground(a,b)
#define IDirect3DViewport_SetBackgroundDepth(p,a) (p)->SetBackgroundDepth(a)
#define IDirect3DViewport_GetBackgroundDepth(p,a,b) (p)->GetBackgroundDepth(a,b)
#define IDirect3DViewport_Clear(p,a,b,c) (p)->Clear(a,b,c)
#define IDirect3DViewport_AddLight(p,a) (p)->AddLight(a)
#define IDirect3DViewport_DeleteLight(p,a) (p)->DeleteLight(a)
#define IDirect3DViewport_NextLight(p,a,b,c) (p)->NextLight(a,b,c)
#endif

#if(DIRECT3D_VERSION >= 0x0500)
#undef INTERFACE
#define INTERFACE IDirect3DViewport2

DECLARE_INTERFACE_(IDirect3DViewport2, IDirect3DViewport)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DViewport methods ***/
    STDMETHOD(Initialize)(THIS_ LPDIRECT3D) PURE;
    STDMETHOD(GetViewport)(THIS_ LPD3DVIEWPORT) PURE;
    STDMETHOD(SetViewport)(THIS_ LPD3DVIEWPORT) PURE;
    STDMETHOD(TransformVertices)(THIS_ DWORD,LPD3DTRANSFORMDATA,DWORD,LPDWORD) PURE;
    STDMETHOD(LightElements)(THIS_ DWORD,LPD3DLIGHTDATA) PURE;
    STDMETHOD(SetBackground)(THIS_ D3DMATERIALHANDLE) PURE;
    STDMETHOD(GetBackground)(THIS_ LPD3DMATERIALHANDLE,LPBOOL) PURE;
    STDMETHOD(SetBackgroundDepth)(THIS_ LPDIRECTDRAWSURFACE) PURE;
    STDMETHOD(GetBackgroundDepth)(THIS_ LPDIRECTDRAWSURFACE*,LPBOOL) PURE;
    STDMETHOD(Clear)(THIS_ DWORD,LPD3DRECT,DWORD) PURE;
    STDMETHOD(AddLight)(THIS_ LPDIRECT3DLIGHT) PURE;
    STDMETHOD(DeleteLight)(THIS_ LPDIRECT3DLIGHT) PURE;
    STDMETHOD(NextLight)(THIS_ LPDIRECT3DLIGHT,LPDIRECT3DLIGHT*,DWORD) PURE;
    STDMETHOD(GetViewport2)(THIS_ LPD3DVIEWPORT2) PURE;
    STDMETHOD(SetViewport2)(THIS_ LPD3DVIEWPORT2) PURE;
};

typedef struct IDirect3DViewport2 *LPDIRECT3DVIEWPORT2;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DViewport2_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DViewport2_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DViewport2_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DViewport2_Initialize(p,a) (p)->lpVtbl->Initialize(p,a)
#define IDirect3DViewport2_GetViewport(p,a) (p)->lpVtbl->GetViewport(p,a)
#define IDirect3DViewport2_SetViewport(p,a) (p)->lpVtbl->SetViewport(p,a)
#define IDirect3DViewport2_TransformVertices(p,a,b,c,d) (p)->lpVtbl->TransformVertices(p,a,b,c,d)
#define IDirect3DViewport2_LightElements(p,a,b) (p)->lpVtbl->LightElements(p,a,b)
#define IDirect3DViewport2_SetBackground(p,a) (p)->lpVtbl->SetBackground(p,a)
#define IDirect3DViewport2_GetBackground(p,a,b) (p)->lpVtbl->GetBackground(p,a,b)
#define IDirect3DViewport2_SetBackgroundDepth(p,a) (p)->lpVtbl->SetBackgroundDepth(p,a)
#define IDirect3DViewport2_GetBackgroundDepth(p,a,b) (p)->lpVtbl->GetBackgroundDepth(p,a,b)
#define IDirect3DViewport2_Clear(p,a,b,c) (p)->lpVtbl->Clear(p,a,b,c)
#define IDirect3DViewport2_AddLight(p,a) (p)->lpVtbl->AddLight(p,a)
#define IDirect3DViewport2_DeleteLight(p,a) (p)->lpVtbl->DeleteLight(p,a)
#define IDirect3DViewport2_NextLight(p,a,b,c) (p)->lpVtbl->NextLight(p,a,b,c)
#define IDirect3DViewport2_GetViewport2(p,a) (p)->lpVtbl->GetViewport2(p,a)
#define IDirect3DViewport2_SetViewport2(p,a) (p)->lpVtbl->SetViewport2(p,a)
#else
#define IDirect3DViewport2_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DViewport2_AddRef(p) (p)->AddRef()
#define IDirect3DViewport2_Release(p) (p)->Release()
#define IDirect3DViewport2_Initialize(p,a) (p)->Initialize(a)
#define IDirect3DViewport2_GetViewport(p,a) (p)->GetViewport(a)
#define IDirect3DViewport2_SetViewport(p,a) (p)->SetViewport(a)
#define IDirect3DViewport2_TransformVertices(p,a,b,c,d) (p)->TransformVertices(a,b,c,d)
#define IDirect3DViewport2_LightElements(p,a,b) (p)->LightElements(a,b)
#define IDirect3DViewport2_SetBackground(p,a) (p)->SetBackground(a)
#define IDirect3DViewport2_GetBackground(p,a,b) (p)->GetBackground(a,b)
#define IDirect3DViewport2_SetBackgroundDepth(p,a) (p)->SetBackgroundDepth(a)
#define IDirect3DViewport2_GetBackgroundDepth(p,a,b) (p)->GetBackgroundDepth(a,b)
#define IDirect3DViewport2_Clear(p,a,b,c) (p)->Clear(a,b,c)
#define IDirect3DViewport2_AddLight(p,a) (p)->AddLight(a)
#define IDirect3DViewport2_DeleteLight(p,a) (p)->DeleteLight(a)
#define IDirect3DViewport2_NextLight(p,a,b,c) (p)->NextLight(a,b,c)
#define IDirect3DViewport2_GetViewport2(p,a) (p)->GetViewport2(a)
#define IDirect3DViewport2_SetViewport2(p,a) (p)->SetViewport2(a)
#endif
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)

#undef INTERFACE
#define INTERFACE IDirect3DViewport3

DECLARE_INTERFACE_(IDirect3DViewport3, IDirect3DViewport2)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DViewport2 methods ***/
    STDMETHOD(Initialize)(THIS_ LPDIRECT3D) PURE;
    STDMETHOD(GetViewport)(THIS_ LPD3DVIEWPORT) PURE;
    STDMETHOD(SetViewport)(THIS_ LPD3DVIEWPORT) PURE;
    STDMETHOD(TransformVertices)(THIS_ DWORD,LPD3DTRANSFORMDATA,DWORD,LPDWORD) PURE;
    STDMETHOD(LightElements)(THIS_ DWORD,LPD3DLIGHTDATA) PURE;
    STDMETHOD(SetBackground)(THIS_ D3DMATERIALHANDLE) PURE;
    STDMETHOD(GetBackground)(THIS_ LPD3DMATERIALHANDLE,LPBOOL) PURE;
    STDMETHOD(SetBackgroundDepth)(THIS_ LPDIRECTDRAWSURFACE) PURE;
    STDMETHOD(GetBackgroundDepth)(THIS_ LPDIRECTDRAWSURFACE*,LPBOOL) PURE;
    STDMETHOD(Clear)(THIS_ DWORD,LPD3DRECT,DWORD) PURE;
    STDMETHOD(AddLight)(THIS_ LPDIRECT3DLIGHT) PURE;
    STDMETHOD(DeleteLight)(THIS_ LPDIRECT3DLIGHT) PURE;
    STDMETHOD(NextLight)(THIS_ LPDIRECT3DLIGHT,LPDIRECT3DLIGHT*,DWORD) PURE;
    STDMETHOD(GetViewport2)(THIS_ LPD3DVIEWPORT2) PURE;
    STDMETHOD(SetViewport2)(THIS_ LPD3DVIEWPORT2) PURE;
    STDMETHOD(SetBackgroundDepth2)(THIS_ LPDIRECTDRAWSURFACE4) PURE;
    STDMETHOD(GetBackgroundDepth2)(THIS_ LPDIRECTDRAWSURFACE4*,LPBOOL) PURE;
    STDMETHOD(Clear2)(THIS_ DWORD,LPD3DRECT,DWORD,D3DCOLOR,D3DVALUE,DWORD) PURE;
};

typedef struct IDirect3DViewport3 *LPDIRECT3DVIEWPORT3;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DViewport3_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DViewport3_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DViewport3_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DViewport3_Initialize(p,a) (p)->lpVtbl->Initialize(p,a)
#define IDirect3DViewport3_GetViewport(p,a) (p)->lpVtbl->GetViewport(p,a)
#define IDirect3DViewport3_SetViewport(p,a) (p)->lpVtbl->SetViewport(p,a)
#define IDirect3DViewport3_TransformVertices(p,a,b,c,d) (p)->lpVtbl->TransformVertices(p,a,b,c,d)
#define IDirect3DViewport3_LightElements(p,a,b) (p)->lpVtbl->LightElements(p,a,b)
#define IDirect3DViewport3_SetBackground(p,a) (p)->lpVtbl->SetBackground(p,a)
#define IDirect3DViewport3_GetBackground(p,a,b) (p)->lpVtbl->GetBackground(p,a,b)
#define IDirect3DViewport3_SetBackgroundDepth(p,a) (p)->lpVtbl->SetBackgroundDepth(p,a)
#define IDirect3DViewport3_GetBackgroundDepth(p,a,b) (p)->lpVtbl->GetBackgroundDepth(p,a,b)
#define IDirect3DViewport3_Clear(p,a,b,c) (p)->lpVtbl->Clear(p,a,b,c)
#define IDirect3DViewport3_AddLight(p,a) (p)->lpVtbl->AddLight(p,a)
#define IDirect3DViewport3_DeleteLight(p,a) (p)->lpVtbl->DeleteLight(p,a)
#define IDirect3DViewport3_NextLight(p,a,b,c) (p)->lpVtbl->NextLight(p,a,b,c)
#define IDirect3DViewport3_GetViewport2(p,a) (p)->lpVtbl->GetViewport2(p,a)
#define IDirect3DViewport3_SetViewport2(p,a) (p)->lpVtbl->SetViewport2(p,a)
#define IDirect3DViewport3_SetBackgroundDepth2(p,a) (p)->lpVtbl->SetBackgroundDepth2(p,a)
#define IDirect3DViewport3_GetBackgroundDepth2(p,a,b) (p)->lpVtbl->GetBackgroundDepth2(p,a,b)
#define IDirect3DViewport3_Clear2(p,a,b,c,d,e,f) (p)->lpVtbl->Clear2(p,a,b,c,d,e,f)
#else
#define IDirect3DViewport3_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DViewport3_AddRef(p) (p)->AddRef()
#define IDirect3DViewport3_Release(p) (p)->Release()
#define IDirect3DViewport3_Initialize(p,a) (p)->Initialize(a)
#define IDirect3DViewport3_GetViewport(p,a) (p)->GetViewport(a)
#define IDirect3DViewport3_SetViewport(p,a) (p)->SetViewport(a)
#define IDirect3DViewport3_TransformVertices(p,a,b,c,d) (p)->TransformVertices(a,b,c,d)
#define IDirect3DViewport3_LightElements(p,a,b) (p)->LightElements(a,b)
#define IDirect3DViewport3_SetBackground(p,a) (p)->SetBackground(a)
#define IDirect3DViewport3_GetBackground(p,a,b) (p)->GetBackground(a,b)
#define IDirect3DViewport3_SetBackgroundDepth(p,a) (p)->SetBackgroundDepth(a)
#define IDirect3DViewport3_GetBackgroundDepth(p,a,b) (p)->GetBackgroundDepth(a,b)
#define IDirect3DViewport3_Clear(p,a,b,c) (p)->Clear(a,b,c)
#define IDirect3DViewport3_AddLight(p,a) (p)->AddLight(a)
#define IDirect3DViewport3_DeleteLight(p,a) (p)->DeleteLight(a)
#define IDirect3DViewport3_NextLight(p,a,b,c) (p)->NextLight(a,b,c)
#define IDirect3DViewport3_GetViewport2(p,a) (p)->GetViewport2(a)
#define IDirect3DViewport3_SetViewport2(p,a) (p)->SetViewport2(a)
#define IDirect3DViewport3_SetBackgroundDepth2(p,a) (p)->SetBackgroundDepth2(a)
#define IDirect3DViewport3_GetBackgroundDepth2(p,a,b) (p)->GetBackgroundDepth2(a,b)
#define IDirect3DViewport3_Clear2(p,a,b,c,d,e,f) (p)->Clear2(a,b,c,d,e,f)
#endif
#endif /* DIRECT3D_VERSION >= 0x0600 */

#if(DIRECT3D_VERSION >= 0x0600)
#undef INTERFACE
#define INTERFACE IDirect3DVertexBuffer

DECLARE_INTERFACE_(IDirect3DVertexBuffer, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DVertexBuffer methods ***/
    STDMETHOD(Lock)(THIS_ DWORD,LPVOID*,LPDWORD) PURE;
    STDMETHOD(Unlock)(THIS) PURE;
    STDMETHOD(ProcessVertices)(THIS_ DWORD,DWORD,DWORD,LPDIRECT3DVERTEXBUFFER,DWORD,LPDIRECT3DDEVICE3,DWORD) PURE;
    STDMETHOD(GetVertexBufferDesc)(THIS_ LPD3DVERTEXBUFFERDESC) PURE;
    STDMETHOD(Optimize)(THIS_ LPDIRECT3DDEVICE3,DWORD) PURE;
};

typedef struct IDirect3DVertexBuffer *LPDIRECT3DVERTEXBUFFER;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DVertexBuffer_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DVertexBuffer_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DVertexBuffer_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DVertexBuffer_Lock(p,a,b,c) (p)->lpVtbl->Lock(p,a,b,c)
#define IDirect3DVertexBuffer_Unlock(p) (p)->lpVtbl->Unlock(p)
#define IDirect3DVertexBuffer_ProcessVertices(p,a,b,c,d,e,f,g) (p)->lpVtbl->ProcessVertices(p,a,b,c,d,e,f,g)
#define IDirect3DVertexBuffer_GetVertexBufferDesc(p,a) (p)->lpVtbl->GetVertexBufferDesc(p,a)
#define IDirect3DVertexBuffer_Optimize(p,a,b) (p)->lpVtbl->Optimize(p,a,b)
#else
#define IDirect3DVertexBuffer_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DVertexBuffer_AddRef(p) (p)->AddRef()
#define IDirect3DVertexBuffer_Release(p) (p)->Release()
#define IDirect3DVertexBuffer_Lock(p,a,b,c) (p)->Lock(a,b,c)
#define IDirect3DVertexBuffer_Unlock(p) (p)->Unlock()
#define IDirect3DVertexBuffer_ProcessVertices(p,a,b,c,d,e,f,g) (p)->ProcessVertices(a,b,c,d,e,f,g)
#define IDirect3DVertexBuffer_GetVertexBufferDesc(p,a) (p)->GetVertexBufferDesc(a)
#define IDirect3DVertexBuffer_Optimize(p,a,b) (p)->Optimize(a,b)
#endif
#endif /* DIRECT3D_VERSION >= 0x0600 */

#if(DIRECT3D_VERSION >= 0x0700)
#undef INTERFACE
#define INTERFACE IDirect3DVertexBuffer7

DECLARE_INTERFACE_(IDirect3DVertexBuffer7, IUnknown)
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, LPVOID * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef)(THIS) PURE;
    STDMETHOD_(ULONG,Release)(THIS) PURE;

    /*** IDirect3DVertexBuffer7 methods ***/
    STDMETHOD(Lock)(THIS_ DWORD,LPVOID*,LPDWORD) PURE;
    STDMETHOD(Unlock)(THIS) PURE;
    STDMETHOD(ProcessVertices)(THIS_ DWORD,DWORD,DWORD,LPDIRECT3DVERTEXBUFFER7,DWORD,LPDIRECT3DDEVICE7,DWORD) PURE;
    STDMETHOD(GetVertexBufferDesc)(THIS_ LPD3DVERTEXBUFFERDESC) PURE;
    STDMETHOD(Optimize)(THIS_ LPDIRECT3DDEVICE7,DWORD) PURE;
    STDMETHOD(ProcessVerticesStrided)(THIS_ DWORD,DWORD,DWORD,LPD3DDRAWPRIMITIVESTRIDEDDATA,DWORD,LPDIRECT3DDEVICE7,DWORD) PURE;
};

typedef struct IDirect3DVertexBuffer7 *LPDIRECT3DVERTEXBUFFER7;

#if !defined(__cplusplus) || defined(CINTERFACE)
#define IDirect3DVertexBuffer7_QueryInterface(p,a,b) (p)->lpVtbl->QueryInterface(p,a,b)
#define IDirect3DVertexBuffer7_AddRef(p) (p)->lpVtbl->AddRef(p)
#define IDirect3DVertexBuffer7_Release(p) (p)->lpVtbl->Release(p)
#define IDirect3DVertexBuffer7_Lock(p,a,b,c) (p)->lpVtbl->Lock(p,a,b,c)
#define IDirect3DVertexBuffer7_Unlock(p) (p)->lpVtbl->Unlock(p)
#define IDirect3DVertexBuffer7_ProcessVertices(p,a,b,c,d,e,f,g) (p)->lpVtbl->ProcessVertices(p,a,b,c,d,e,f,g)
#define IDirect3DVertexBuffer7_GetVertexBufferDesc(p,a) (p)->lpVtbl->GetVertexBufferDesc(p,a)
#define IDirect3DVertexBuffer7_Optimize(p,a,b) (p)->lpVtbl->Optimize(p,a,b)
#define IDirect3DVertexBuffer7_ProcessVerticesStrided(p,a,b,c,d,e,f,g) (p)->lpVtbl->ProcessVerticesStrided(p,a,b,c,d,e,f,g)
#else
#define IDirect3DVertexBuffer7_QueryInterface(p,a,b) (p)->QueryInterface(a,b)
#define IDirect3DVertexBuffer7_AddRef(p) (p)->AddRef()
#define IDirect3DVertexBuffer7_Release(p) (p)->Release()
#define IDirect3DVertexBuffer7_Lock(p,a,b,c) (p)->Lock(a,b,c)
#define IDirect3DVertexBuffer7_Unlock(p) (p)->Unlock()
#define IDirect3DVertexBuffer7_ProcessVertices(p,a,b,c,d,e,f,g) (p)->ProcessVertices(a,b,c,d,e,f,g)
#define IDirect3DVertexBuffer7_GetVertexBufferDesc(p,a) (p)->GetVertexBufferDesc(a)
#define IDirect3DVertexBuffer7_Optimize(p,a,b) (p)->Optimize(a,b)
#define IDirect3DVertexBuffer7_ProcessVerticesStrided(p,a,b,c,d,e,f,g) (p)->ProcessVerticesStrided(a,b,c,d,e,f,g)
#endif
#endif /* DIRECT3D_VERSION >= 0x0700 */

#if(DIRECT3D_VERSION >= 0x0500)
/****************************************************************************
 *
 * Flags for IDirect3DDevice::NextViewport
 *
 ****************************************************************************/

/*
 * Return the next viewport
 */
#define D3DNEXT_NEXT    0x00000001l

/*
 * Return the first viewport
 */
#define D3DNEXT_HEAD    0x00000002l

/*
 * Return the last viewport
 */
#define D3DNEXT_TAIL    0x00000004l


/****************************************************************************
 *
 * Flags for DrawPrimitive/DrawIndexedPrimitive
 *   Also valid for Begin/BeginIndexed
 *   Also valid for VertexBuffer::CreateVertexBuffer
 ****************************************************************************/

/*
 * Wait until the device is ready to draw the primitive
 * This will cause DP to not return DDERR_WASSTILLDRAWING
 */
#define D3DDP_WAIT                  0x00000001l
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if (DIRECT3D_VERSION == 0x0500)
/*
 * Hint that it is acceptable to render the primitive out of order.
 */
#define D3DDP_OUTOFORDER            0x00000002l
#endif


#if(DIRECT3D_VERSION >= 0x0500)
/*
 * Hint that the primitives have been clipped by the application.
 */
#define D3DDP_DONOTCLIP             0x00000004l

/*
 * Hint that the extents need not be updated.
 */
#define D3DDP_DONOTUPDATEEXTENTS    0x00000008l
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)

/*
 * Hint that the lighting should not be applied on vertices.
 */

#define D3DDP_DONOTLIGHT            0x00000010l

#endif /* DIRECT3D_VERSION >= 0x0600 */

/*
 * Direct3D Errors
 * DirectDraw error codes are used when errors not specified here.
 */
#define D3D_OK              DD_OK
#define D3DERR_BADMAJORVERSION      MAKE_DDHRESULT(700)
#define D3DERR_BADMINORVERSION      MAKE_DDHRESULT(701)

#if(DIRECT3D_VERSION >= 0x0500)
/*
 * An invalid device was requested by the application.
 */
#define D3DERR_INVALID_DEVICE   MAKE_DDHRESULT(705)
#define D3DERR_INITFAILED       MAKE_DDHRESULT(706)

/*
 * SetRenderTarget attempted on a device that was
 * QI'd off the render target.
 */
#define D3DERR_DEVICEAGGREGATED MAKE_DDHRESULT(707)
#endif /* DIRECT3D_VERSION >= 0x0500 */

#define D3DERR_EXECUTE_CREATE_FAILED    MAKE_DDHRESULT(710)
#define D3DERR_EXECUTE_DESTROY_FAILED   MAKE_DDHRESULT(711)
#define D3DERR_EXECUTE_LOCK_FAILED  MAKE_DDHRESULT(712)
#define D3DERR_EXECUTE_UNLOCK_FAILED    MAKE_DDHRESULT(713)
#define D3DERR_EXECUTE_LOCKED       MAKE_DDHRESULT(714)
#define D3DERR_EXECUTE_NOT_LOCKED   MAKE_DDHRESULT(715)

#define D3DERR_EXECUTE_FAILED       MAKE_DDHRESULT(716)
#define D3DERR_EXECUTE_CLIPPED_FAILED   MAKE_DDHRESULT(717)

#define D3DERR_TEXTURE_NO_SUPPORT   MAKE_DDHRESULT(720)
#define D3DERR_TEXTURE_CREATE_FAILED    MAKE_DDHRESULT(721)
#define D3DERR_TEXTURE_DESTROY_FAILED   MAKE_DDHRESULT(722)
#define D3DERR_TEXTURE_LOCK_FAILED  MAKE_DDHRESULT(723)
#define D3DERR_TEXTURE_UNLOCK_FAILED    MAKE_DDHRESULT(724)
#define D3DERR_TEXTURE_LOAD_FAILED  MAKE_DDHRESULT(725)
#define D3DERR_TEXTURE_SWAP_FAILED  MAKE_DDHRESULT(726)
#define D3DERR_TEXTURE_LOCKED       MAKE_DDHRESULT(727)
#define D3DERR_TEXTURE_NOT_LOCKED   MAKE_DDHRESULT(728)
#define D3DERR_TEXTURE_GETSURF_FAILED   MAKE_DDHRESULT(729)

#define D3DERR_MATRIX_CREATE_FAILED MAKE_DDHRESULT(730)
#define D3DERR_MATRIX_DESTROY_FAILED    MAKE_DDHRESULT(731)
#define D3DERR_MATRIX_SETDATA_FAILED    MAKE_DDHRESULT(732)
#define D3DERR_MATRIX_GETDATA_FAILED    MAKE_DDHRESULT(733)
#define D3DERR_SETVIEWPORTDATA_FAILED   MAKE_DDHRESULT(734)

#if(DIRECT3D_VERSION >= 0x0500)
#define D3DERR_INVALIDCURRENTVIEWPORT   MAKE_DDHRESULT(735)
#define D3DERR_INVALIDPRIMITIVETYPE     MAKE_DDHRESULT(736)
#define D3DERR_INVALIDVERTEXTYPE        MAKE_DDHRESULT(737)
#define D3DERR_TEXTURE_BADSIZE          MAKE_DDHRESULT(738)
#define D3DERR_INVALIDRAMPTEXTURE       MAKE_DDHRESULT(739)
#endif /* DIRECT3D_VERSION >= 0x0500 */

#define D3DERR_MATERIAL_CREATE_FAILED   MAKE_DDHRESULT(740)
#define D3DERR_MATERIAL_DESTROY_FAILED  MAKE_DDHRESULT(741)
#define D3DERR_MATERIAL_SETDATA_FAILED  MAKE_DDHRESULT(742)
#define D3DERR_MATERIAL_GETDATA_FAILED  MAKE_DDHRESULT(743)

#if(DIRECT3D_VERSION >= 0x0500)
#define D3DERR_INVALIDPALETTE           MAKE_DDHRESULT(744)

#define D3DERR_ZBUFF_NEEDS_SYSTEMMEMORY MAKE_DDHRESULT(745)
#define D3DERR_ZBUFF_NEEDS_VIDEOMEMORY  MAKE_DDHRESULT(746)
#define D3DERR_SURFACENOTINVIDMEM       MAKE_DDHRESULT(747)
#endif /* DIRECT3D_VERSION >= 0x0500 */

#define D3DERR_LIGHT_SET_FAILED     MAKE_DDHRESULT(750)
#if(DIRECT3D_VERSION >= 0x0500)
#define D3DERR_LIGHTHASVIEWPORT     MAKE_DDHRESULT(751)
#define D3DERR_LIGHTNOTINTHISVIEWPORT           MAKE_DDHRESULT(752)
#endif /* DIRECT3D_VERSION >= 0x0500 */

#define D3DERR_SCENE_IN_SCENE       MAKE_DDHRESULT(760)
#define D3DERR_SCENE_NOT_IN_SCENE   MAKE_DDHRESULT(761)
#define D3DERR_SCENE_BEGIN_FAILED   MAKE_DDHRESULT(762)
#define D3DERR_SCENE_END_FAILED     MAKE_DDHRESULT(763)

#if(DIRECT3D_VERSION >= 0x0500)
#define D3DERR_INBEGIN                  MAKE_DDHRESULT(770)
#define D3DERR_NOTINBEGIN               MAKE_DDHRESULT(771)
#define D3DERR_NOVIEWPORTS              MAKE_DDHRESULT(772)
#define D3DERR_VIEWPORTDATANOTSET       MAKE_DDHRESULT(773)
#define D3DERR_VIEWPORTHASNODEVICE      MAKE_DDHRESULT(774)
#define D3DERR_NOCURRENTVIEWPORT        MAKE_DDHRESULT(775)
#endif /* DIRECT3D_VERSION >= 0x0500 */

#if(DIRECT3D_VERSION >= 0x0600)
#define D3DERR_INVALIDVERTEXFORMAT              MAKE_DDHRESULT(2048)

/*
 * Attempted to CreateTexture on a surface that had a color key
 */
#define D3DERR_COLORKEYATTACHED                 MAKE_DDHRESULT(2050)

#define D3DERR_VERTEXBUFFEROPTIMIZED            MAKE_DDHRESULT(2060)
#define D3DERR_VBUF_CREATE_FAILED               MAKE_DDHRESULT(2061)
#define D3DERR_VERTEXBUFFERLOCKED               MAKE_DDHRESULT(2062)
#define D3DERR_VERTEXBUFFERUNLOCKFAILED         MAKE_DDHRESULT(2063)

#define D3DERR_ZBUFFER_NOTPRESENT               MAKE_DDHRESULT(2070)
#define D3DERR_STENCILBUFFER_NOTPRESENT         MAKE_DDHRESULT(2071)

#define D3DERR_WRONGTEXTUREFORMAT               MAKE_DDHRESULT(2072)
#define D3DERR_UNSUPPORTEDCOLOROPERATION        MAKE_DDHRESULT(2073)
#define D3DERR_UNSUPPORTEDCOLORARG              MAKE_DDHRESULT(2074)
#define D3DERR_UNSUPPORTEDALPHAOPERATION        MAKE_DDHRESULT(2075)
#define D3DERR_UNSUPPORTEDALPHAARG              MAKE_DDHRESULT(2076)
#define D3DERR_TOOMANYOPERATIONS                MAKE_DDHRESULT(2077)
#define D3DERR_CONFLICTINGTEXTUREFILTER         MAKE_DDHRESULT(2078)
#define D3DERR_UNSUPPORTEDFACTORVALUE           MAKE_DDHRESULT(2079)
#define D3DERR_CONFLICTINGRENDERSTATE           MAKE_DDHRESULT(2081)
#define D3DERR_UNSUPPORTEDTEXTUREFILTER         MAKE_DDHRESULT(2082)
#define D3DERR_TOOMANYPRIMITIVES                MAKE_DDHRESULT(2083)
#define D3DERR_INVALIDMATRIX                    MAKE_DDHRESULT(2084)
#define D3DERR_TOOMANYVERTICES                  MAKE_DDHRESULT(2085)
#define D3DERR_CONFLICTINGTEXTUREPALETTE        MAKE_DDHRESULT(2086)

#endif /* DIRECT3D_VERSION >= 0x0600 */

#if(DIRECT3D_VERSION >= 0x0700)
#define D3DERR_INVALIDSTATEBLOCK        MAKE_DDHRESULT(2100)
#define D3DERR_INBEGINSTATEBLOCK        MAKE_DDHRESULT(2101)
#define D3DERR_NOTINBEGINSTATEBLOCK     MAKE_DDHRESULT(2102)
#endif /* DIRECT3D_VERSION >= 0x0700 */


#ifdef __cplusplus
};
#endif

#endif /* (DIRECT3D_VERSION < 0x0800) */
#endif /* _D3D_H_ */


#ifdef __cplusplus
extern "C" {
#endif

/*
 * The methods for IUnknown
 */
#define IUNKNOWN_METHODS(kind) \
    STDMETHOD(QueryInterface)	   	(THIS_ REFIID riid, LPVOID *ppvObj) kind; \
    STDMETHOD_(ULONG, AddRef)	   	(THIS) kind; \
    STDMETHOD_(ULONG, Release)	   	(THIS) kind

/*
 * The methods for IDirect3DRMObject
 */
#define IDIRECT3DRMOBJECT_METHODS(kind) \
    STDMETHOD(Clone)			(THIS_ LPUNKNOWN pUnkOuter, REFIID riid, LPVOID *ppvObj) kind; \
    STDMETHOD(AddDestroyCallback)  	(THIS_ D3DRMOBJECTCALLBACK, LPVOID argument) kind; \
    STDMETHOD(DeleteDestroyCallback)	(THIS_ D3DRMOBJECTCALLBACK, LPVOID argument) kind; \
    STDMETHOD(SetAppData)	   	(THIS_ DWORD data) kind; \
    STDMETHOD_(DWORD, GetAppData)  	(THIS) kind; \
    STDMETHOD(SetName)		   	(THIS_ LPCSTR) kind; \
    STDMETHOD(GetName)			(THIS_ LPDWORD lpdwSize, LPSTR lpName) kind; \
    STDMETHOD(GetClassName)		(THIS_ LPDWORD lpdwSize, LPSTR lpName) kind


#define WIN_TYPES(itype, ptype) \
    typedef interface itype FAR *LP##ptype, FAR **LPLP##ptype

WIN_TYPES(IDirect3DRMObject, DIRECT3DRMOBJECT);
WIN_TYPES(IDirect3DRMObject2, DIRECT3DRMOBJECT2);
WIN_TYPES(IDirect3DRMDevice, DIRECT3DRMDEVICE);
WIN_TYPES(IDirect3DRMDevice2, DIRECT3DRMDEVICE2);
WIN_TYPES(IDirect3DRMDevice3, DIRECT3DRMDEVICE3);
WIN_TYPES(IDirect3DRMViewport, DIRECT3DRMVIEWPORT);
WIN_TYPES(IDirect3DRMViewport2, DIRECT3DRMVIEWPORT2);
WIN_TYPES(IDirect3DRMFrame, DIRECT3DRMFRAME);
WIN_TYPES(IDirect3DRMFrame2, DIRECT3DRMFRAME2);
WIN_TYPES(IDirect3DRMFrame3, DIRECT3DRMFRAME3);
WIN_TYPES(IDirect3DRMVisual, DIRECT3DRMVISUAL);
WIN_TYPES(IDirect3DRMMesh, DIRECT3DRMMESH);
WIN_TYPES(IDirect3DRMMeshBuilder, DIRECT3DRMMESHBUILDER);
WIN_TYPES(IDirect3DRMMeshBuilder2, DIRECT3DRMMESHBUILDER2);
WIN_TYPES(IDirect3DRMMeshBuilder3, DIRECT3DRMMESHBUILDER3);
WIN_TYPES(IDirect3DRMFace, DIRECT3DRMFACE);
WIN_TYPES(IDirect3DRMFace2, DIRECT3DRMFACE2);
WIN_TYPES(IDirect3DRMLight, DIRECT3DRMLIGHT);
WIN_TYPES(IDirect3DRMTexture, DIRECT3DRMTEXTURE);
WIN_TYPES(IDirect3DRMTexture2, DIRECT3DRMTEXTURE2);
WIN_TYPES(IDirect3DRMTexture3, DIRECT3DRMTEXTURE3);
WIN_TYPES(IDirect3DRMWrap, DIRECT3DRMWRAP);
WIN_TYPES(IDirect3DRMMaterial, DIRECT3DRMMATERIAL);
WIN_TYPES(IDirect3DRMMaterial2, DIRECT3DRMMATERIAL2);
WIN_TYPES(IDirect3DRMInterpolator, DIRECT3DRMINTERPOLATOR);
WIN_TYPES(IDirect3DRMAnimation, DIRECT3DRMANIMATION);
WIN_TYPES(IDirect3DRMAnimation2, DIRECT3DRMANIMATION2);
WIN_TYPES(IDirect3DRMAnimationSet, DIRECT3DRMANIMATIONSET);
WIN_TYPES(IDirect3DRMAnimationSet2, DIRECT3DRMANIMATIONSET2);
WIN_TYPES(IDirect3DRMUserVisual, DIRECT3DRMUSERVISUAL);
WIN_TYPES(IDirect3DRMShadow, DIRECT3DRMSHADOW);
WIN_TYPES(IDirect3DRMShadow2, DIRECT3DRMSHADOW2);
WIN_TYPES(IDirect3DRMArray, DIRECT3DRMARRAY);
WIN_TYPES(IDirect3DRMObjectArray, DIRECT3DRMOBJECTARRAY);
WIN_TYPES(IDirect3DRMDeviceArray, DIRECT3DRMDEVICEARRAY);
WIN_TYPES(IDirect3DRMFaceArray, DIRECT3DRMFACEARRAY);
WIN_TYPES(IDirect3DRMViewportArray, DIRECT3DRMVIEWPORTARRAY);
WIN_TYPES(IDirect3DRMFrameArray, DIRECT3DRMFRAMEARRAY);
WIN_TYPES(IDirect3DRMAnimationArray, DIRECT3DRMANIMATIONARRAY);
WIN_TYPES(IDirect3DRMVisualArray, DIRECT3DRMVISUALARRAY);
WIN_TYPES(IDirect3DRMPickedArray, DIRECT3DRMPICKEDARRAY);
WIN_TYPES(IDirect3DRMPicked2Array, DIRECT3DRMPICKED2ARRAY);
WIN_TYPES(IDirect3DRMLightArray, DIRECT3DRMLIGHTARRAY);
WIN_TYPES(IDirect3DRMProgressiveMesh, DIRECT3DRMPROGRESSIVEMESH);
WIN_TYPES(IDirect3DRMClippedVisual, DIRECT3DRMCLIPPEDVISUAL);

/*
 * Direct3DRM Object classes
 */
DEFINE_GUID(CLSID_CDirect3DRMDevice,	    0x4fa3568e, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMViewport,	    0x4fa3568f, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMFrame,	    0x4fa35690, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMMesh,	    0x4fa35691, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMMeshBuilder,   0x4fa35692, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMFace,	    0x4fa35693, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMLight,	    0x4fa35694, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMTexture,	    0x4fa35695, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMWrap,	    0x4fa35696, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMMaterial,	    0x4fa35697, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMAnimation,	    0x4fa35698, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMAnimationSet,  0x4fa35699, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMUserVisual,    0x4fa3569a, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMShadow,	    0x4fa3569b, 0x623f, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(CLSID_CDirect3DRMViewportInterpolator, 
0xde9eaa1, 0x3b84, 0x11d0, 0x9b, 0x6d, 0x0, 0x0, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(CLSID_CDirect3DRMFrameInterpolator, 
0xde9eaa2, 0x3b84, 0x11d0, 0x9b, 0x6d, 0x0, 0x0, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(CLSID_CDirect3DRMMeshInterpolator, 
0xde9eaa3, 0x3b84, 0x11d0, 0x9b, 0x6d, 0x0, 0x0, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(CLSID_CDirect3DRMLightInterpolator, 
0xde9eaa6, 0x3b84, 0x11d0, 0x9b, 0x6d, 0x0, 0x0, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(CLSID_CDirect3DRMMaterialInterpolator, 
0xde9eaa7, 0x3b84, 0x11d0, 0x9b, 0x6d, 0x0, 0x0, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(CLSID_CDirect3DRMTextureInterpolator, 
0xde9eaa8, 0x3b84, 0x11d0, 0x9b, 0x6d, 0x0, 0x0, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(CLSID_CDirect3DRMProgressiveMesh, 0x4516ec40, 0x8f20, 0x11d0, 0x9b, 0x6d, 0x00, 0x00, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(CLSID_CDirect3DRMClippedVisual,   0x5434e72d, 0x6d66, 0x11d1, 0xbb, 0xb, 0x0, 0x0, 0xf8, 0x75, 0x86, 0x5a);


/*
 * Direct3DRM Object interfaces
 */
DEFINE_GUID(IID_IDirect3DRMObject, 	    0xeb16cb00, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMObject2,         0x4516ec7c, 0x8f20, 0x11d0, 0x9b, 0x6d, 0x00, 0x00, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(IID_IDirect3DRMDevice, 	    0xe9e19280, 0x6e05, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMDevice2,	    0x4516ec78, 0x8f20, 0x11d0, 0x9b, 0x6d, 0x00, 0x00, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(IID_IDirect3DRMDevice3,     0x549f498b, 0xbfeb, 0x11d1, 0x8e, 0xd8, 0x0, 0xa0, 0xc9, 0x67, 0xa4, 0x82);
DEFINE_GUID(IID_IDirect3DRMViewport, 	    0xeb16cb02, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMViewport2,   0x4a1b1be6, 0xbfed, 0x11d1, 0x8e, 0xd8, 0x0, 0xa0, 0xc9, 0x67, 0xa4, 0x82);
DEFINE_GUID(IID_IDirect3DRMFrame, 	    0xeb16cb03, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMFrame2,	    0xc3dfbd60, 0x3988, 0x11d0, 0x9e, 0xc2, 0x0, 0x0, 0xc0, 0x29, 0x1a, 0xc3);
DEFINE_GUID(IID_IDirect3DRMFrame3,              0xff6b7f70, 0xa40e, 0x11d1, 0x91, 0xf9, 0x0, 0x0, 0xf8, 0x75, 0x8e, 0x66);
DEFINE_GUID(IID_IDirect3DRMVisual, 	    0xeb16cb04, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMMesh, 	    0xa3a80d01, 0x6e12, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMMeshBuilder,	    0xa3a80d02, 0x6e12, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMMeshBuilder2,    0x4516ec77, 0x8f20, 0x11d0, 0x9b, 0x6d, 0x0, 0x0, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(IID_IDirect3DRMMeshBuilder3,    0x4516ec82, 0x8f20, 0x11d0, 0x9b, 0x6d, 0x00, 0x00, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(IID_IDirect3DRMFace, 	    0xeb16cb07, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMFace2,           0x4516ec81, 0x8f20, 0x11d0, 0x9b, 0x6d, 0x00, 0x00, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(IID_IDirect3DRMLight, 	    0xeb16cb08, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMTexture, 	    0xeb16cb09, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMTexture2,        0x120f30c0, 0x1629, 0x11d0, 0x94, 0x1c, 0x0, 0x80, 0xc8, 0xc, 0xfa, 0x7b);
DEFINE_GUID(IID_IDirect3DRMTexture3,        0xff6b7f73, 0xa40e, 0x11d1, 0x91, 0xf9, 0x0, 0x0, 0xf8, 0x75, 0x8e, 0x66);
DEFINE_GUID(IID_IDirect3DRMWrap, 	    0xeb16cb0a, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMMaterial, 	    0xeb16cb0b, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMMaterial2,       0xff6b7f75, 0xa40e, 0x11d1, 0x91, 0xf9, 0x0, 0x0, 0xf8, 0x75, 0x8e, 0x66);
DEFINE_GUID(IID_IDirect3DRMAnimation, 	    0xeb16cb0d, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMAnimation2,      0xff6b7f77, 0xa40e, 0x11d1, 0x91, 0xf9, 0x0, 0x0, 0xf8, 0x75, 0x8e, 0x66);
DEFINE_GUID(IID_IDirect3DRMAnimationSet,    0xeb16cb0e, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMAnimationSet2,   0xff6b7f79, 0xa40e, 0x11d1, 0x91, 0xf9, 0x0, 0x0, 0xf8, 0x75, 0x8e, 0x66);
DEFINE_GUID(IID_IDirect3DRMObjectArray,	    0x242f6bc2, 0x3849, 0x11d0, 0x9b, 0x6d, 0x0, 0x0, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(IID_IDirect3DRMDeviceArray,	    0xeb16cb10, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMViewportArray,   0xeb16cb11, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMFrameArray, 	    0xeb16cb12, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMVisualArray,	    0xeb16cb13, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMLightArray, 	    0xeb16cb14, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMPickedArray,	    0xeb16cb16, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMFaceArray,	    0xeb16cb17, 0xd271, 0x11ce, 0xac, 0x48, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMAnimationArray, 
0xd5f1cae0, 0x4bd7, 0x11d1, 0xb9, 0x74, 0x0, 0x60, 0x8, 0x3e, 0x45, 0xf3);
DEFINE_GUID(IID_IDirect3DRMUserVisual,	    0x59163de0, 0x6d43, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMShadow,	    0xaf359780, 0x6ba3, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRMShadow2,	    0x86b44e25, 0x9c82, 0x11d1, 0xbb, 0xb, 0x0, 0xa0, 0xc9, 0x81, 0xa0, 0xa6);
DEFINE_GUID(IID_IDirect3DRMInterpolator,    0x242f6bc1, 0x3849, 0x11d0, 0x9b, 0x6d, 0x0, 0x0, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(IID_IDirect3DRMProgressiveMesh, 0x4516ec79, 0x8f20, 0x11d0, 0x9b, 0x6d, 0x0, 0x0, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(IID_IDirect3DRMPicked2Array,    0x4516ec7b, 0x8f20, 0x11d0, 0x9b, 0x6d, 0x0, 0x0, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(IID_IDirect3DRMClippedVisual,   0x5434e733, 0x6d66, 0x11d1, 0xbb, 0xb, 0x0, 0x0, 0xf8, 0x75, 0x86, 0x5a);

typedef void (__cdecl *D3DRMOBJECTCALLBACK)(LPDIRECT3DRMOBJECT obj, LPVOID arg);
typedef void (__cdecl *D3DRMFRAMEMOVECALLBACK)(LPDIRECT3DRMFRAME obj, LPVOID arg, D3DVALUE delta);
typedef void (__cdecl *D3DRMFRAME3MOVECALLBACK)(LPDIRECT3DRMFRAME3 obj, LPVOID arg, D3DVALUE delta);
typedef void (__cdecl *D3DRMUPDATECALLBACK)(LPDIRECT3DRMDEVICE obj, LPVOID arg, int, LPD3DRECT);
typedef void (__cdecl *D3DRMDEVICE3UPDATECALLBACK)(LPDIRECT3DRMDEVICE3 obj, LPVOID arg, int, LPD3DRECT);
typedef int (__cdecl *D3DRMUSERVISUALCALLBACK)
    (   LPDIRECT3DRMUSERVISUAL obj, LPVOID arg,	D3DRMUSERVISUALREASON reason,
        LPDIRECT3DRMDEVICE dev, LPDIRECT3DRMVIEWPORT view
    );
typedef HRESULT (__cdecl *D3DRMLOADTEXTURECALLBACK)
    (char *tex_name, void *arg, LPDIRECT3DRMTEXTURE *);
typedef HRESULT (__cdecl *D3DRMLOADTEXTURE3CALLBACK)
    (char *tex_name, void *arg, LPDIRECT3DRMTEXTURE3 *);
typedef void (__cdecl *D3DRMLOADCALLBACK)
    (LPDIRECT3DRMOBJECT object, REFIID objectguid, LPVOID arg);

typedef HRESULT (__cdecl *D3DRMDOWNSAMPLECALLBACK)
    (LPDIRECT3DRMTEXTURE3 lpDirect3DRMTexture, LPVOID pArg,
     LPDIRECTDRAWSURFACE pDDSSrc, LPDIRECTDRAWSURFACE pDDSDst);
typedef HRESULT (__cdecl *D3DRMVALIDATIONCALLBACK)
    (LPDIRECT3DRMTEXTURE3 lpDirect3DRMTexture, LPVOID pArg,
     DWORD dwFlags, DWORD dwcRects, LPRECT pRects);


typedef struct _D3DRMPICKDESC
{
    ULONG	ulFaceIdx;
    LONG	lGroupIdx;
    D3DVECTOR	vPosition;

} D3DRMPICKDESC, *LPD3DRMPICKDESC;

typedef struct _D3DRMPICKDESC2
{
    ULONG	ulFaceIdx;
    LONG	lGroupIdx;
    D3DVECTOR	dvPosition;
    D3DVALUE	tu;
    D3DVALUE	tv;
    D3DVECTOR	dvNormal;
    D3DCOLOR	dcColor;

} D3DRMPICKDESC2, *LPD3DRMPICKDESC2;

#undef INTERFACE
#define INTERFACE IDirect3DRMObject

/*
 * Base class
 */
DECLARE_INTERFACE_(IDirect3DRMObject, IUnknown)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);
};

#undef INTERFACE
#define INTERFACE IDirect3DRMObject2

DECLARE_INTERFACE_(IDirect3DRMObject2, IUnknown)
{
    IUNKNOWN_METHODS(PURE);

    /*
     * IDirect3DRMObject2 methods
     */
    STDMETHOD(AddDestroyCallback)(THIS_ D3DRMOBJECTCALLBACK lpFunc, LPVOID pvArg) PURE;
    STDMETHOD(Clone)(THIS_ LPUNKNOWN pUnkOuter, REFIID riid, LPVOID *ppvObj) PURE; \
    STDMETHOD(DeleteDestroyCallback)(THIS_ D3DRMOBJECTCALLBACK lpFunc, LPVOID pvArg) PURE; \
    STDMETHOD(GetClientData)(THIS_ DWORD dwID, LPVOID* lplpvData) PURE;
    STDMETHOD(GetDirect3DRM)(THIS_ LPDIRECT3DRM* lplpDirect3DRM) PURE;
    STDMETHOD(GetName)(THIS_ LPDWORD lpdwSize, LPSTR lpName) PURE;
    STDMETHOD(SetClientData)(THIS_ DWORD dwID, LPVOID lpvData, DWORD dwFlags) PURE;
    STDMETHOD(SetName)(THIS_ LPCSTR lpName) PURE;
    STDMETHOD(GetAge)(THIS_ DWORD dwFlags, LPDWORD pdwAge) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMVisual

DECLARE_INTERFACE_(IDirect3DRMVisual, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);
};

#undef INTERFACE
#define INTERFACE IDirect3DRMDevice

DECLARE_INTERFACE_(IDirect3DRMDevice, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMDevice methods
     */
    STDMETHOD(Init)(THIS_ ULONG width, ULONG height) PURE;
    STDMETHOD(InitFromD3D)(THIS_ LPDIRECT3D lpD3D, LPDIRECT3DDEVICE lpD3DDev) PURE;
    STDMETHOD(InitFromClipper)(THIS_ LPDIRECTDRAWCLIPPER lpDDClipper, LPGUID lpGUID, int width, int height) PURE;

    STDMETHOD(Update)(THIS) PURE;
    STDMETHOD(AddUpdateCallback)(THIS_ D3DRMUPDATECALLBACK, LPVOID arg) PURE;
    STDMETHOD(DeleteUpdateCallback)(THIS_ D3DRMUPDATECALLBACK, LPVOID arg) PURE;
    STDMETHOD(SetBufferCount)(THIS_ DWORD) PURE;
    STDMETHOD_(DWORD, GetBufferCount)(THIS) PURE;

    STDMETHOD(SetDither)(THIS_ BOOL) PURE;
    STDMETHOD(SetShades)(THIS_ DWORD) PURE;
    STDMETHOD(SetQuality)(THIS_ D3DRMRENDERQUALITY) PURE;
    STDMETHOD(SetTextureQuality)(THIS_ D3DRMTEXTUREQUALITY) PURE;

    STDMETHOD(GetViewports)(THIS_ LPDIRECT3DRMVIEWPORTARRAY *return_views) PURE;

    STDMETHOD_(BOOL, GetDither)(THIS) PURE;
    STDMETHOD_(DWORD, GetShades)(THIS) PURE;
    STDMETHOD_(DWORD, GetHeight)(THIS) PURE;
    STDMETHOD_(DWORD, GetWidth)(THIS) PURE;
    STDMETHOD_(DWORD, GetTrianglesDrawn)(THIS) PURE;
    STDMETHOD_(DWORD, GetWireframeOptions)(THIS) PURE;
    STDMETHOD_(D3DRMRENDERQUALITY, GetQuality)(THIS) PURE;
    STDMETHOD_(D3DCOLORMODEL, GetColorModel)(THIS) PURE;
    STDMETHOD_(D3DRMTEXTUREQUALITY, GetTextureQuality)(THIS) PURE;
    STDMETHOD(GetDirect3DDevice)(THIS_ LPDIRECT3DDEVICE *) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMDevice2

DECLARE_INTERFACE_(IDirect3DRMDevice2, IDirect3DRMDevice)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMDevice methods
     */
    STDMETHOD(Init)(THIS_ ULONG width, ULONG height) PURE;
    STDMETHOD(InitFromD3D)(THIS_ LPDIRECT3D lpD3D, LPDIRECT3DDEVICE lpD3DDev) PURE;
    STDMETHOD(InitFromClipper)(THIS_ LPDIRECTDRAWCLIPPER lpDDClipper, LPGUID lpGUID, int width, int height) PURE;

    STDMETHOD(Update)(THIS) PURE;
    STDMETHOD(AddUpdateCallback)(THIS_ D3DRMUPDATECALLBACK, LPVOID arg) PURE;
    STDMETHOD(DeleteUpdateCallback)(THIS_ D3DRMUPDATECALLBACK, LPVOID arg) PURE;
    STDMETHOD(SetBufferCount)(THIS_ DWORD) PURE;
    STDMETHOD_(DWORD, GetBufferCount)(THIS) PURE;

    STDMETHOD(SetDither)(THIS_ BOOL) PURE;
    STDMETHOD(SetShades)(THIS_ DWORD) PURE;
    STDMETHOD(SetQuality)(THIS_ D3DRMRENDERQUALITY) PURE;
    STDMETHOD(SetTextureQuality)(THIS_ D3DRMTEXTUREQUALITY) PURE;

    STDMETHOD(GetViewports)(THIS_ LPDIRECT3DRMVIEWPORTARRAY *return_views) PURE;

    STDMETHOD_(BOOL, GetDither)(THIS) PURE;
    STDMETHOD_(DWORD, GetShades)(THIS) PURE;
    STDMETHOD_(DWORD, GetHeight)(THIS) PURE;
    STDMETHOD_(DWORD, GetWidth)(THIS) PURE;
    STDMETHOD_(DWORD, GetTrianglesDrawn)(THIS) PURE;
    STDMETHOD_(DWORD, GetWireframeOptions)(THIS) PURE;
    STDMETHOD_(D3DRMRENDERQUALITY, GetQuality)(THIS) PURE;
    STDMETHOD_(D3DCOLORMODEL, GetColorModel)(THIS) PURE;
    STDMETHOD_(D3DRMTEXTUREQUALITY, GetTextureQuality)(THIS) PURE;
    STDMETHOD(GetDirect3DDevice)(THIS_ LPDIRECT3DDEVICE *) PURE;

    /*
     * IDirect3DRMDevice2 methods
     */
    STDMETHOD(InitFromD3D2)(THIS_ LPDIRECT3D2 lpD3D, LPDIRECT3DDEVICE2 lpD3DDev) PURE;
    STDMETHOD(InitFromSurface)(THIS_ LPGUID lpGUID, LPDIRECTDRAW lpDD, LPDIRECTDRAWSURFACE lpDDSBack) PURE;
    STDMETHOD(SetRenderMode)(THIS_ DWORD dwFlags) PURE;
    STDMETHOD_(DWORD, GetRenderMode)(THIS) PURE;
    STDMETHOD(GetDirect3DDevice2)(THIS_ LPDIRECT3DDEVICE2 *) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMDevice3

DECLARE_INTERFACE_(IDirect3DRMDevice3, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMDevice methods
     */
    STDMETHOD(Init)(THIS_ ULONG width, ULONG height) PURE;
    STDMETHOD(InitFromD3D)(THIS_ LPDIRECT3D lpD3D, LPDIRECT3DDEVICE lpD3DDev) PURE;
    STDMETHOD(InitFromClipper)(THIS_ LPDIRECTDRAWCLIPPER lpDDClipper, LPGUID lpGUID, int width, int height) PURE;

    STDMETHOD(Update)(THIS) PURE;
    STDMETHOD(AddUpdateCallback)(THIS_ D3DRMDEVICE3UPDATECALLBACK, LPVOID arg) PURE;
    STDMETHOD(DeleteUpdateCallback)(THIS_ D3DRMDEVICE3UPDATECALLBACK, LPVOID arg) PURE;
    STDMETHOD(SetBufferCount)(THIS_ DWORD) PURE;
    STDMETHOD_(DWORD, GetBufferCount)(THIS) PURE;

    STDMETHOD(SetDither)(THIS_ BOOL) PURE;
    STDMETHOD(SetShades)(THIS_ DWORD) PURE;
    STDMETHOD(SetQuality)(THIS_ D3DRMRENDERQUALITY) PURE;
    STDMETHOD(SetTextureQuality)(THIS_ D3DRMTEXTUREQUALITY) PURE;

    STDMETHOD(GetViewports)(THIS_ LPDIRECT3DRMVIEWPORTARRAY *return_views) PURE;

    STDMETHOD_(BOOL, GetDither)(THIS) PURE;
    STDMETHOD_(DWORD, GetShades)(THIS) PURE;
    STDMETHOD_(DWORD, GetHeight)(THIS) PURE;
    STDMETHOD_(DWORD, GetWidth)(THIS) PURE;
    STDMETHOD_(DWORD, GetTrianglesDrawn)(THIS) PURE;
    STDMETHOD_(DWORD, GetWireframeOptions)(THIS) PURE;
    STDMETHOD_(D3DRMRENDERQUALITY, GetQuality)(THIS) PURE;
    STDMETHOD_(D3DCOLORMODEL, GetColorModel)(THIS) PURE;
    STDMETHOD_(D3DRMTEXTUREQUALITY, GetTextureQuality)(THIS) PURE;
    STDMETHOD(GetDirect3DDevice)(THIS_ LPDIRECT3DDEVICE *) PURE;

    /*
     * IDirect3DRMDevice2 methods
     */
    STDMETHOD(InitFromD3D2)(THIS_ LPDIRECT3D2 lpD3D, LPDIRECT3DDEVICE2 lpD3DDev) PURE;
    STDMETHOD(InitFromSurface)(THIS_ LPGUID lpGUID, LPDIRECTDRAW lpDD, LPDIRECTDRAWSURFACE lpDDSBack, DWORD dwFlags) PURE;
    STDMETHOD(SetRenderMode)(THIS_ DWORD dwFlags) PURE;
    STDMETHOD_(DWORD, GetRenderMode)(THIS) PURE;
    STDMETHOD(GetDirect3DDevice2)(THIS_ LPDIRECT3DDEVICE2 *) PURE;

    /*
     * IDirect3DRMDevice3 methods
     */
    STDMETHOD(FindPreferredTextureFormat)(THIS_ DWORD dwBitDepths, DWORD dwFlags, LPDDPIXELFORMAT lpDDPF) PURE;
    STDMETHOD(RenderStateChange)(THIS_ D3DRENDERSTATETYPE drsType, DWORD dwVal, DWORD dwFlags) PURE;
    STDMETHOD(LightStateChange)(THIS_ D3DLIGHTSTATETYPE drsType, DWORD dwVal, DWORD dwFlags) PURE;
    STDMETHOD(GetStateChangeOptions)(THIS_ DWORD dwStateClass, DWORD dwStateNum, LPDWORD pdwFlags) PURE;
    STDMETHOD(SetStateChangeOptions)(THIS_ DWORD dwStateClass, DWORD dwStateNum, DWORD dwFlags) PURE;
};


#undef INTERFACE
#define INTERFACE IDirect3DRMViewport

DECLARE_INTERFACE_(IDirect3DRMViewport, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMViewport methods
     */
    STDMETHOD(Init)
    (	THIS_ LPDIRECT3DRMDEVICE dev, LPDIRECT3DRMFRAME camera,
	DWORD xpos, DWORD ypos, DWORD width, DWORD height
    ) PURE;
    STDMETHOD(Clear)(THIS) PURE;
    STDMETHOD(Render)(THIS_ LPDIRECT3DRMFRAME) PURE;

    STDMETHOD(SetFront)(THIS_ D3DVALUE) PURE;
    STDMETHOD(SetBack)(THIS_ D3DVALUE) PURE;
    STDMETHOD(SetField)(THIS_ D3DVALUE) PURE;
    STDMETHOD(SetUniformScaling)(THIS_ BOOL) PURE;
    STDMETHOD(SetCamera)(THIS_ LPDIRECT3DRMFRAME) PURE;
    STDMETHOD(SetProjection)(THIS_ D3DRMPROJECTIONTYPE) PURE;
    STDMETHOD(Transform)(THIS_ D3DRMVECTOR4D *d, D3DVECTOR *s) PURE;
    STDMETHOD(InverseTransform)(THIS_ D3DVECTOR *d, D3DRMVECTOR4D *s) PURE;
    STDMETHOD(Configure)(THIS_ LONG x, LONG y, DWORD width, DWORD height) PURE;
    STDMETHOD(ForceUpdate)(THIS_ DWORD x1, DWORD y1, DWORD x2, DWORD y2) PURE;
    STDMETHOD(SetPlane)(THIS_ D3DVALUE left, D3DVALUE right, D3DVALUE bottom, D3DVALUE top) PURE;

    STDMETHOD(GetCamera)(THIS_ LPDIRECT3DRMFRAME *) PURE;
    STDMETHOD(GetDevice)(THIS_ LPDIRECT3DRMDEVICE *) PURE;
    STDMETHOD(GetPlane)(THIS_ D3DVALUE *left, D3DVALUE *right, D3DVALUE *bottom, D3DVALUE *top) PURE;
    STDMETHOD(Pick)(THIS_ LONG x, LONG y, LPDIRECT3DRMPICKEDARRAY *return_visuals) PURE;

    STDMETHOD_(BOOL, GetUniformScaling)(THIS) PURE;
    STDMETHOD_(LONG, GetX)(THIS) PURE;
    STDMETHOD_(LONG, GetY)(THIS) PURE;
    STDMETHOD_(DWORD, GetWidth)(THIS) PURE;
    STDMETHOD_(DWORD, GetHeight)(THIS) PURE;
    STDMETHOD_(D3DVALUE, GetField)(THIS) PURE;
    STDMETHOD_(D3DVALUE, GetBack)(THIS) PURE;
    STDMETHOD_(D3DVALUE, GetFront)(THIS) PURE;
    STDMETHOD_(D3DRMPROJECTIONTYPE, GetProjection)(THIS) PURE;
    STDMETHOD(GetDirect3DViewport)(THIS_ LPDIRECT3DVIEWPORT *) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMViewport2
DECLARE_INTERFACE_(IDirect3DRMViewport2, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMViewport2 methods
     */
    STDMETHOD(Init)
    (	THIS_ LPDIRECT3DRMDEVICE3 dev, LPDIRECT3DRMFRAME3 camera,
	DWORD xpos, DWORD ypos, DWORD width, DWORD height
    ) PURE;
    STDMETHOD(Clear)(THIS_ DWORD dwFlags) PURE;
    STDMETHOD(Render)(THIS_ LPDIRECT3DRMFRAME3) PURE;

    STDMETHOD(SetFront)(THIS_ D3DVALUE) PURE;
    STDMETHOD(SetBack)(THIS_ D3DVALUE) PURE;
    STDMETHOD(SetField)(THIS_ D3DVALUE) PURE;
    STDMETHOD(SetUniformScaling)(THIS_ BOOL) PURE;
    STDMETHOD(SetCamera)(THIS_ LPDIRECT3DRMFRAME3) PURE;
    STDMETHOD(SetProjection)(THIS_ D3DRMPROJECTIONTYPE) PURE;
    STDMETHOD(Transform)(THIS_ D3DRMVECTOR4D *d, D3DVECTOR *s) PURE;
    STDMETHOD(InverseTransform)(THIS_ D3DVECTOR *d, D3DRMVECTOR4D *s) PURE;
    STDMETHOD(Configure)(THIS_ LONG x, LONG y, DWORD width, DWORD height) PURE;
    STDMETHOD(ForceUpdate)(THIS_ DWORD x1, DWORD y1, DWORD x2, DWORD y2) PURE;
    STDMETHOD(SetPlane)(THIS_ D3DVALUE left, D3DVALUE right, D3DVALUE bottom, D3DVALUE top) PURE;

    STDMETHOD(GetCamera)(THIS_ LPDIRECT3DRMFRAME3 *) PURE;
    STDMETHOD(GetDevice)(THIS_ LPDIRECT3DRMDEVICE3 *) PURE;
    STDMETHOD(GetPlane)(THIS_ D3DVALUE *left, D3DVALUE *right, D3DVALUE *bottom, D3DVALUE *top) PURE;
    STDMETHOD(Pick)(THIS_ LONG x, LONG y, LPDIRECT3DRMPICKEDARRAY *return_visuals) PURE;

    STDMETHOD_(BOOL, GetUniformScaling)(THIS) PURE;
    STDMETHOD_(LONG, GetX)(THIS) PURE;
    STDMETHOD_(LONG, GetY)(THIS) PURE;
    STDMETHOD_(DWORD, GetWidth)(THIS) PURE;
    STDMETHOD_(DWORD, GetHeight)(THIS) PURE;
    STDMETHOD_(D3DVALUE, GetField)(THIS) PURE;
    STDMETHOD_(D3DVALUE, GetBack)(THIS) PURE;
    STDMETHOD_(D3DVALUE, GetFront)(THIS) PURE;
    STDMETHOD_(D3DRMPROJECTIONTYPE, GetProjection)(THIS) PURE;
    STDMETHOD(GetDirect3DViewport)(THIS_ LPDIRECT3DVIEWPORT *) PURE;
    STDMETHOD(TransformVectors)(THIS_ DWORD dwNumVectors,
				LPD3DRMVECTOR4D lpDstVectors,
				LPD3DVECTOR lpSrcVectors) PURE;
    STDMETHOD(InverseTransformVectors)(THIS_ DWORD dwNumVectors,
				       LPD3DVECTOR lpDstVectors,
				       LPD3DRMVECTOR4D lpSrcVectors) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMFrame

DECLARE_INTERFACE_(IDirect3DRMFrame, IDirect3DRMVisual)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMFrame methods
     */
    STDMETHOD(AddChild)(THIS_ LPDIRECT3DRMFRAME child) PURE;
    STDMETHOD(AddLight)(THIS_ LPDIRECT3DRMLIGHT) PURE;
    STDMETHOD(AddMoveCallback)(THIS_ D3DRMFRAMEMOVECALLBACK, VOID *arg) PURE;
    STDMETHOD(AddTransform)(THIS_ D3DRMCOMBINETYPE, D3DRMMATRIX4D) PURE;
    STDMETHOD(AddTranslation)(THIS_ D3DRMCOMBINETYPE, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(AddScale)(THIS_ D3DRMCOMBINETYPE, D3DVALUE sx, D3DVALUE sy, D3DVALUE sz) PURE;
    STDMETHOD(AddRotation)(THIS_ D3DRMCOMBINETYPE, D3DVALUE x, D3DVALUE y, D3DVALUE z, D3DVALUE theta) PURE;
    STDMETHOD(AddVisual)(THIS_ LPDIRECT3DRMVISUAL) PURE;
    STDMETHOD(GetChildren)(THIS_ LPDIRECT3DRMFRAMEARRAY *children) PURE;
    STDMETHOD_(D3DCOLOR, GetColor)(THIS) PURE;
    STDMETHOD(GetLights)(THIS_ LPDIRECT3DRMLIGHTARRAY *lights) PURE;
    STDMETHOD_(D3DRMMATERIALMODE, GetMaterialMode)(THIS) PURE;
    STDMETHOD(GetParent)(THIS_ LPDIRECT3DRMFRAME *) PURE;
    STDMETHOD(GetPosition)(THIS_ LPDIRECT3DRMFRAME reference, LPD3DVECTOR return_position) PURE;
    STDMETHOD(GetRotation)(THIS_ LPDIRECT3DRMFRAME reference, LPD3DVECTOR axis, LPD3DVALUE return_theta) PURE;
    STDMETHOD(GetScene)(THIS_ LPDIRECT3DRMFRAME *) PURE;
    STDMETHOD_(D3DRMSORTMODE, GetSortMode)(THIS) PURE;
    STDMETHOD(GetTexture)(THIS_ LPDIRECT3DRMTEXTURE *) PURE;
    STDMETHOD(GetTransform)(THIS_ D3DRMMATRIX4D return_matrix) PURE;
    STDMETHOD(GetVelocity)(THIS_ LPDIRECT3DRMFRAME reference, LPD3DVECTOR return_velocity, BOOL with_rotation) PURE;
    STDMETHOD(GetOrientation)(THIS_ LPDIRECT3DRMFRAME reference, LPD3DVECTOR dir, LPD3DVECTOR up) PURE;
    STDMETHOD(GetVisuals)(THIS_ LPDIRECT3DRMVISUALARRAY *visuals) PURE;
    STDMETHOD(GetTextureTopology)(THIS_ BOOL *wrap_u, BOOL *wrap_v) PURE;
    STDMETHOD(InverseTransform)(THIS_ D3DVECTOR *d, D3DVECTOR *s) PURE;
    STDMETHOD(Load)(THIS_ LPVOID filename, LPVOID name, D3DRMLOADOPTIONS loadflags, D3DRMLOADTEXTURECALLBACK, LPVOID lpArg)PURE;
    STDMETHOD(LookAt)(THIS_ LPDIRECT3DRMFRAME target, LPDIRECT3DRMFRAME reference, D3DRMFRAMECONSTRAINT) PURE;
    STDMETHOD(Move)(THIS_ D3DVALUE delta) PURE;
    STDMETHOD(DeleteChild)(THIS_ LPDIRECT3DRMFRAME) PURE;
    STDMETHOD(DeleteLight)(THIS_ LPDIRECT3DRMLIGHT) PURE;
    STDMETHOD(DeleteMoveCallback)(THIS_ D3DRMFRAMEMOVECALLBACK, VOID *arg) PURE;
    STDMETHOD(DeleteVisual)(THIS_ LPDIRECT3DRMVISUAL) PURE;
    STDMETHOD_(D3DCOLOR, GetSceneBackground)(THIS) PURE;
    STDMETHOD(GetSceneBackgroundDepth)(THIS_ LPDIRECTDRAWSURFACE *) PURE;
    STDMETHOD_(D3DCOLOR, GetSceneFogColor)(THIS) PURE;
    STDMETHOD_(BOOL, GetSceneFogEnable)(THIS) PURE;
    STDMETHOD_(D3DRMFOGMODE, GetSceneFogMode)(THIS) PURE;
    STDMETHOD(GetSceneFogParams)(THIS_ D3DVALUE *return_start, D3DVALUE *return_end, D3DVALUE *return_density) PURE;
    STDMETHOD(SetSceneBackground)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetSceneBackgroundRGB)(THIS_ D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;
    STDMETHOD(SetSceneBackgroundDepth)(THIS_ LPDIRECTDRAWSURFACE) PURE;
    STDMETHOD(SetSceneBackgroundImage)(THIS_ LPDIRECT3DRMTEXTURE) PURE;
    STDMETHOD(SetSceneFogEnable)(THIS_ BOOL) PURE;
    STDMETHOD(SetSceneFogColor)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetSceneFogMode)(THIS_ D3DRMFOGMODE) PURE;
    STDMETHOD(SetSceneFogParams)(THIS_ D3DVALUE start, D3DVALUE end, D3DVALUE density) PURE;
    STDMETHOD(SetColor)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetColorRGB)(THIS_ D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;
    STDMETHOD_(D3DRMZBUFFERMODE, GetZbufferMode)(THIS) PURE;
    STDMETHOD(SetMaterialMode)(THIS_ D3DRMMATERIALMODE) PURE;
    STDMETHOD(SetOrientation)
    (	THIS_ LPDIRECT3DRMFRAME reference,
	D3DVALUE dx, D3DVALUE dy, D3DVALUE dz,
	D3DVALUE ux, D3DVALUE uy, D3DVALUE uz
    ) PURE;
    STDMETHOD(SetPosition)(THIS_ LPDIRECT3DRMFRAME reference, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(SetRotation)(THIS_ LPDIRECT3DRMFRAME reference, D3DVALUE x, D3DVALUE y, D3DVALUE z, D3DVALUE theta) PURE;
    STDMETHOD(SetSortMode)(THIS_ D3DRMSORTMODE) PURE;
    STDMETHOD(SetTexture)(THIS_ LPDIRECT3DRMTEXTURE) PURE;
    STDMETHOD(SetTextureTopology)(THIS_ BOOL wrap_u, BOOL wrap_v) PURE;
    STDMETHOD(SetVelocity)(THIS_ LPDIRECT3DRMFRAME reference, D3DVALUE x, D3DVALUE y, D3DVALUE z, BOOL with_rotation) PURE;
    STDMETHOD(SetZbufferMode)(THIS_ D3DRMZBUFFERMODE) PURE;
    STDMETHOD(Transform)(THIS_ D3DVECTOR *d, D3DVECTOR *s) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMFrame2

DECLARE_INTERFACE_(IDirect3DRMFrame2, IDirect3DRMFrame)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMFrame methods
     */
    STDMETHOD(AddChild)(THIS_ LPDIRECT3DRMFRAME child) PURE;
    STDMETHOD(AddLight)(THIS_ LPDIRECT3DRMLIGHT) PURE;
    STDMETHOD(AddMoveCallback)(THIS_ D3DRMFRAMEMOVECALLBACK, VOID *arg) PURE;
    STDMETHOD(AddTransform)(THIS_ D3DRMCOMBINETYPE, D3DRMMATRIX4D) PURE;
    STDMETHOD(AddTranslation)(THIS_ D3DRMCOMBINETYPE, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(AddScale)(THIS_ D3DRMCOMBINETYPE, D3DVALUE sx, D3DVALUE sy, D3DVALUE sz) PURE;
    STDMETHOD(AddRotation)(THIS_ D3DRMCOMBINETYPE, D3DVALUE x, D3DVALUE y, D3DVALUE z, D3DVALUE theta) PURE;
    STDMETHOD(AddVisual)(THIS_ LPDIRECT3DRMVISUAL) PURE;
    STDMETHOD(GetChildren)(THIS_ LPDIRECT3DRMFRAMEARRAY *children) PURE;
    STDMETHOD_(D3DCOLOR, GetColor)(THIS) PURE;
    STDMETHOD(GetLights)(THIS_ LPDIRECT3DRMLIGHTARRAY *lights) PURE;
    STDMETHOD_(D3DRMMATERIALMODE, GetMaterialMode)(THIS) PURE;
    STDMETHOD(GetParent)(THIS_ LPDIRECT3DRMFRAME *) PURE;
    STDMETHOD(GetPosition)(THIS_ LPDIRECT3DRMFRAME reference, LPD3DVECTOR return_position) PURE;
    STDMETHOD(GetRotation)(THIS_ LPDIRECT3DRMFRAME reference, LPD3DVECTOR axis, LPD3DVALUE return_theta) PURE;
    STDMETHOD(GetScene)(THIS_ LPDIRECT3DRMFRAME *) PURE;
    STDMETHOD_(D3DRMSORTMODE, GetSortMode)(THIS) PURE;
    STDMETHOD(GetTexture)(THIS_ LPDIRECT3DRMTEXTURE *) PURE;
    STDMETHOD(GetTransform)(THIS_ D3DRMMATRIX4D return_matrix) PURE;
    STDMETHOD(GetVelocity)(THIS_ LPDIRECT3DRMFRAME reference, LPD3DVECTOR return_velocity, BOOL with_rotation) PURE;
    STDMETHOD(GetOrientation)(THIS_ LPDIRECT3DRMFRAME reference, LPD3DVECTOR dir, LPD3DVECTOR up) PURE;
    STDMETHOD(GetVisuals)(THIS_ LPDIRECT3DRMVISUALARRAY *visuals) PURE;
    STDMETHOD(GetTextureTopology)(THIS_ BOOL *wrap_u, BOOL *wrap_v) PURE;
    STDMETHOD(InverseTransform)(THIS_ D3DVECTOR *d, D3DVECTOR *s) PURE;
    STDMETHOD(Load)(THIS_ LPVOID filename, LPVOID name, D3DRMLOADOPTIONS loadflags, D3DRMLOADTEXTURECALLBACK, LPVOID lpArg)PURE;
    STDMETHOD(LookAt)(THIS_ LPDIRECT3DRMFRAME target, LPDIRECT3DRMFRAME reference, D3DRMFRAMECONSTRAINT) PURE;
    STDMETHOD(Move)(THIS_ D3DVALUE delta) PURE;
    STDMETHOD(DeleteChild)(THIS_ LPDIRECT3DRMFRAME) PURE;
    STDMETHOD(DeleteLight)(THIS_ LPDIRECT3DRMLIGHT) PURE;
    STDMETHOD(DeleteMoveCallback)(THIS_ D3DRMFRAMEMOVECALLBACK, VOID *arg) PURE;
    STDMETHOD(DeleteVisual)(THIS_ LPDIRECT3DRMVISUAL) PURE;
    STDMETHOD_(D3DCOLOR, GetSceneBackground)(THIS) PURE;
    STDMETHOD(GetSceneBackgroundDepth)(THIS_ LPDIRECTDRAWSURFACE *) PURE;
    STDMETHOD_(D3DCOLOR, GetSceneFogColor)(THIS) PURE;
    STDMETHOD_(BOOL, GetSceneFogEnable)(THIS) PURE;
    STDMETHOD_(D3DRMFOGMODE, GetSceneFogMode)(THIS) PURE;
    STDMETHOD(GetSceneFogParams)(THIS_ D3DVALUE *return_start, D3DVALUE *return_end, D3DVALUE *return_density) PURE;
    STDMETHOD(SetSceneBackground)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetSceneBackgroundRGB)(THIS_ D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;
    STDMETHOD(SetSceneBackgroundDepth)(THIS_ LPDIRECTDRAWSURFACE) PURE;
    STDMETHOD(SetSceneBackgroundImage)(THIS_ LPDIRECT3DRMTEXTURE) PURE;
    STDMETHOD(SetSceneFogEnable)(THIS_ BOOL) PURE;
    STDMETHOD(SetSceneFogColor)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetSceneFogMode)(THIS_ D3DRMFOGMODE) PURE;
    STDMETHOD(SetSceneFogParams)(THIS_ D3DVALUE start, D3DVALUE end, D3DVALUE density) PURE;
    STDMETHOD(SetColor)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetColorRGB)(THIS_ D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;
    STDMETHOD_(D3DRMZBUFFERMODE, GetZbufferMode)(THIS) PURE;
    STDMETHOD(SetMaterialMode)(THIS_ D3DRMMATERIALMODE) PURE;
    STDMETHOD(SetOrientation)
    (	THIS_ LPDIRECT3DRMFRAME reference,
	D3DVALUE dx, D3DVALUE dy, D3DVALUE dz,
	D3DVALUE ux, D3DVALUE uy, D3DVALUE uz
    ) PURE;
    STDMETHOD(SetPosition)(THIS_ LPDIRECT3DRMFRAME reference, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(SetRotation)(THIS_ LPDIRECT3DRMFRAME reference, D3DVALUE x, D3DVALUE y, D3DVALUE z, D3DVALUE theta) PURE;
    STDMETHOD(SetSortMode)(THIS_ D3DRMSORTMODE) PURE;
    STDMETHOD(SetTexture)(THIS_ LPDIRECT3DRMTEXTURE) PURE;
    STDMETHOD(SetTextureTopology)(THIS_ BOOL wrap_u, BOOL wrap_v) PURE;
    STDMETHOD(SetVelocity)(THIS_ LPDIRECT3DRMFRAME reference, D3DVALUE x, D3DVALUE y, D3DVALUE z, BOOL with_rotation) PURE;
    STDMETHOD(SetZbufferMode)(THIS_ D3DRMZBUFFERMODE) PURE;
    STDMETHOD(Transform)(THIS_ D3DVECTOR *d, D3DVECTOR *s) PURE;

    /*
     * IDirect3DRMFrame2 methods
     */
    STDMETHOD(AddMoveCallback2)(THIS_ D3DRMFRAMEMOVECALLBACK, VOID *arg, DWORD dwFlags) PURE;
    STDMETHOD(GetBox)(THIS_ LPD3DRMBOX) PURE;
    STDMETHOD_(BOOL, GetBoxEnable)(THIS) PURE;
    STDMETHOD(GetAxes)(THIS_ LPD3DVECTOR dir, LPD3DVECTOR up);
    STDMETHOD(GetMaterial)(THIS_ LPDIRECT3DRMMATERIAL *) PURE;
    STDMETHOD_(BOOL, GetInheritAxes)(THIS);
    STDMETHOD(GetHierarchyBox)(THIS_ LPD3DRMBOX) PURE;

    STDMETHOD(SetBox)(THIS_ LPD3DRMBOX) PURE;
    STDMETHOD(SetBoxEnable)(THIS_ BOOL) PURE;
    STDMETHOD(SetAxes)(THIS_ D3DVALUE dx, D3DVALUE dy, D3DVALUE dz,
		       D3DVALUE ux, D3DVALUE uy, D3DVALUE uz);
    STDMETHOD(SetInheritAxes)(THIS_ BOOL inherit_from_parent);
    STDMETHOD(SetMaterial)(THIS_ LPDIRECT3DRMMATERIAL) PURE;
    STDMETHOD(SetQuaternion)(THIS_ LPDIRECT3DRMFRAME reference, D3DRMQUATERNION *q) PURE;

    STDMETHOD(RayPick)(THIS_ LPDIRECT3DRMFRAME reference, LPD3DRMRAY ray, DWORD dwFlags, LPDIRECT3DRMPICKED2ARRAY *return_visuals) PURE;
    STDMETHOD(Save)(THIS_ LPCSTR filename, D3DRMXOFFORMAT d3dFormat, 
		    D3DRMSAVEOPTIONS d3dSaveFlags);
};

#undef INTERFACE
#define INTERFACE IDirect3DRMFrame3

DECLARE_INTERFACE_(IDirect3DRMFrame3, IDirect3DRMVisual)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMFrame3 methods
     */
    STDMETHOD(AddChild)(THIS_ LPDIRECT3DRMFRAME3 child) PURE;
    STDMETHOD(AddLight)(THIS_ LPDIRECT3DRMLIGHT) PURE;
    STDMETHOD(AddMoveCallback)(THIS_ D3DRMFRAME3MOVECALLBACK, VOID *arg, DWORD dwFlags) PURE;
    STDMETHOD(AddTransform)(THIS_ D3DRMCOMBINETYPE, D3DRMMATRIX4D) PURE;
    STDMETHOD(AddTranslation)(THIS_ D3DRMCOMBINETYPE, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(AddScale)(THIS_ D3DRMCOMBINETYPE, D3DVALUE sx, D3DVALUE sy, D3DVALUE sz) PURE;
    STDMETHOD(AddRotation)(THIS_ D3DRMCOMBINETYPE, D3DVALUE x, D3DVALUE y, D3DVALUE z, D3DVALUE theta) PURE;
    STDMETHOD(AddVisual)(THIS_ LPUNKNOWN) PURE;
    STDMETHOD(GetChildren)(THIS_ LPDIRECT3DRMFRAMEARRAY *children) PURE;
    STDMETHOD_(D3DCOLOR, GetColor)(THIS) PURE;
    STDMETHOD(GetLights)(THIS_ LPDIRECT3DRMLIGHTARRAY *lights) PURE;
    STDMETHOD_(D3DRMMATERIALMODE, GetMaterialMode)(THIS) PURE;
    STDMETHOD(GetParent)(THIS_ LPDIRECT3DRMFRAME3 *) PURE;
    STDMETHOD(GetPosition)(THIS_ LPDIRECT3DRMFRAME3 reference, LPD3DVECTOR return_position) PURE;
    STDMETHOD(GetRotation)(THIS_ LPDIRECT3DRMFRAME3 reference, LPD3DVECTOR axis, LPD3DVALUE return_theta) PURE;
    STDMETHOD(GetScene)(THIS_ LPDIRECT3DRMFRAME3 *) PURE;
    STDMETHOD_(D3DRMSORTMODE, GetSortMode)(THIS) PURE;
    STDMETHOD(GetTexture)(THIS_ LPDIRECT3DRMTEXTURE3 *) PURE;
    STDMETHOD(GetTransform)(THIS_ LPDIRECT3DRMFRAME3 reference,
			     D3DRMMATRIX4D rmMatrix) PURE;
    STDMETHOD(GetVelocity)(THIS_ LPDIRECT3DRMFRAME3 reference, LPD3DVECTOR return_velocity, BOOL with_rotation) PURE;
    STDMETHOD(GetOrientation)(THIS_ LPDIRECT3DRMFRAME3 reference, LPD3DVECTOR dir, LPD3DVECTOR up) PURE;
    STDMETHOD(GetVisuals)(THIS_ LPDWORD lpdwCount, LPUNKNOWN *) PURE;
    STDMETHOD(InverseTransform)(THIS_ D3DVECTOR *d, D3DVECTOR *s) PURE;
    STDMETHOD(Load)(THIS_ LPVOID filename, LPVOID name, D3DRMLOADOPTIONS loadflags, D3DRMLOADTEXTURE3CALLBACK, LPVOID lpArg)PURE;
    STDMETHOD(LookAt)(THIS_ LPDIRECT3DRMFRAME3 target, LPDIRECT3DRMFRAME3 reference, D3DRMFRAMECONSTRAINT) PURE;
    STDMETHOD(Move)(THIS_ D3DVALUE delta) PURE;
    STDMETHOD(DeleteChild)(THIS_ LPDIRECT3DRMFRAME3) PURE;
    STDMETHOD(DeleteLight)(THIS_ LPDIRECT3DRMLIGHT) PURE;
    STDMETHOD(DeleteMoveCallback)(THIS_ D3DRMFRAME3MOVECALLBACK, VOID *arg) PURE;
    STDMETHOD(DeleteVisual)(THIS_ LPUNKNOWN) PURE;
    STDMETHOD_(D3DCOLOR, GetSceneBackground)(THIS) PURE;
    STDMETHOD(GetSceneBackgroundDepth)(THIS_ LPDIRECTDRAWSURFACE *) PURE;
    STDMETHOD_(D3DCOLOR, GetSceneFogColor)(THIS) PURE;
    STDMETHOD_(BOOL, GetSceneFogEnable)(THIS) PURE;
    STDMETHOD_(D3DRMFOGMODE, GetSceneFogMode)(THIS) PURE;
    STDMETHOD(GetSceneFogParams)(THIS_ D3DVALUE *return_start, D3DVALUE *return_end, D3DVALUE *return_density) PURE;
    STDMETHOD(SetSceneBackground)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetSceneBackgroundRGB)(THIS_ D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;
    STDMETHOD(SetSceneBackgroundDepth)(THIS_ LPDIRECTDRAWSURFACE) PURE;
    STDMETHOD(SetSceneBackgroundImage)(THIS_ LPDIRECT3DRMTEXTURE3) PURE;
    STDMETHOD(SetSceneFogEnable)(THIS_ BOOL) PURE;
    STDMETHOD(SetSceneFogColor)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetSceneFogMode)(THIS_ D3DRMFOGMODE) PURE;
    STDMETHOD(SetSceneFogParams)(THIS_ D3DVALUE start, D3DVALUE end, D3DVALUE density) PURE;
    STDMETHOD(SetColor)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetColorRGB)(THIS_ D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;
    STDMETHOD_(D3DRMZBUFFERMODE, GetZbufferMode)(THIS) PURE;
    STDMETHOD(SetMaterialMode)(THIS_ D3DRMMATERIALMODE) PURE;
    STDMETHOD(SetOrientation)
    (	THIS_ LPDIRECT3DRMFRAME3 reference,
	D3DVALUE dx, D3DVALUE dy, D3DVALUE dz,
	D3DVALUE ux, D3DVALUE uy, D3DVALUE uz
    ) PURE;
    STDMETHOD(SetPosition)(THIS_ LPDIRECT3DRMFRAME3 reference, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(SetRotation)(THIS_ LPDIRECT3DRMFRAME3 reference, D3DVALUE x, D3DVALUE y, D3DVALUE z, D3DVALUE theta) PURE;
    STDMETHOD(SetSortMode)(THIS_ D3DRMSORTMODE) PURE;
    STDMETHOD(SetTexture)(THIS_ LPDIRECT3DRMTEXTURE3) PURE;
    STDMETHOD(SetVelocity)(THIS_ LPDIRECT3DRMFRAME3 reference, D3DVALUE x, D3DVALUE y, D3DVALUE z, BOOL with_rotation) PURE;
    STDMETHOD(SetZbufferMode)(THIS_ D3DRMZBUFFERMODE) PURE;
    STDMETHOD(Transform)(THIS_ D3DVECTOR *d, D3DVECTOR *s) PURE;
    STDMETHOD(GetBox)(THIS_ LPD3DRMBOX) PURE;
    STDMETHOD_(BOOL, GetBoxEnable)(THIS) PURE;
    STDMETHOD(GetAxes)(THIS_ LPD3DVECTOR dir, LPD3DVECTOR up);
    STDMETHOD(GetMaterial)(THIS_ LPDIRECT3DRMMATERIAL2 *) PURE;
    STDMETHOD_(BOOL, GetInheritAxes)(THIS);
    STDMETHOD(GetHierarchyBox)(THIS_ LPD3DRMBOX) PURE;

    STDMETHOD(SetBox)(THIS_ LPD3DRMBOX) PURE;
    STDMETHOD(SetBoxEnable)(THIS_ BOOL) PURE;
    STDMETHOD(SetAxes)(THIS_ D3DVALUE dx, D3DVALUE dy, D3DVALUE dz,
		       D3DVALUE ux, D3DVALUE uy, D3DVALUE uz);
    STDMETHOD(SetInheritAxes)(THIS_ BOOL inherit_from_parent);
    STDMETHOD(SetMaterial)(THIS_ LPDIRECT3DRMMATERIAL2) PURE;
    STDMETHOD(SetQuaternion)(THIS_ LPDIRECT3DRMFRAME3 reference, D3DRMQUATERNION *q) PURE;

    STDMETHOD(RayPick)(THIS_ LPDIRECT3DRMFRAME3 reference, LPD3DRMRAY ray, DWORD dwFlags, LPDIRECT3DRMPICKED2ARRAY *return_visuals) PURE;
    STDMETHOD(Save)(THIS_ LPCSTR filename, D3DRMXOFFORMAT d3dFormat, 
		    D3DRMSAVEOPTIONS d3dSaveFlags);
    STDMETHOD(TransformVectors)(THIS_ LPDIRECT3DRMFRAME3 reference,
				DWORD dwNumVectors,
				LPD3DVECTOR lpDstVectors,
				LPD3DVECTOR lpSrcVectors) PURE;
    STDMETHOD(InverseTransformVectors)(THIS_ LPDIRECT3DRMFRAME3 reference,
				       DWORD dwNumVectors,
				       LPD3DVECTOR lpDstVectors,
				       LPD3DVECTOR lpSrcVectors) PURE;
    STDMETHOD(SetTraversalOptions)(THIS_ DWORD dwFlags) PURE;
    STDMETHOD(GetTraversalOptions)(THIS_ LPDWORD lpdwFlags) PURE;
    STDMETHOD(SetSceneFogMethod)(THIS_ DWORD dwFlags) PURE;
    STDMETHOD(GetSceneFogMethod)(THIS_ LPDWORD lpdwFlags) PURE;
    STDMETHOD(SetMaterialOverride)(THIS_ LPD3DRMMATERIALOVERRIDE) PURE;
    STDMETHOD(GetMaterialOverride)(THIS_ LPD3DRMMATERIALOVERRIDE) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMMesh

DECLARE_INTERFACE_(IDirect3DRMMesh, IDirect3DRMVisual)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMMesh methods
     */
    STDMETHOD(Scale)(THIS_ D3DVALUE sx, D3DVALUE sy, D3DVALUE sz) PURE;
    STDMETHOD(Translate)(THIS_ D3DVALUE tx, D3DVALUE ty, D3DVALUE tz) PURE;
    STDMETHOD(GetBox)(THIS_ D3DRMBOX *) PURE;
    STDMETHOD(AddGroup)(THIS_ unsigned vCount, unsigned fCount, unsigned vPerFace, unsigned *fData, D3DRMGROUPINDEX *returnId) PURE;
    STDMETHOD(SetVertices)(THIS_ D3DRMGROUPINDEX id, unsigned index, unsigned count, D3DRMVERTEX *values) PURE;
    STDMETHOD(SetGroupColor)(THIS_ D3DRMGROUPINDEX id, D3DCOLOR value) PURE;
    STDMETHOD(SetGroupColorRGB)(THIS_ D3DRMGROUPINDEX id, D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;
    STDMETHOD(SetGroupMapping)(THIS_ D3DRMGROUPINDEX id, D3DRMMAPPING value) PURE;
    STDMETHOD(SetGroupQuality)(THIS_ D3DRMGROUPINDEX id, D3DRMRENDERQUALITY value) PURE;
    STDMETHOD(SetGroupMaterial)(THIS_ D3DRMGROUPINDEX id, LPDIRECT3DRMMATERIAL value) PURE;
    STDMETHOD(SetGroupTexture)(THIS_ D3DRMGROUPINDEX id, LPDIRECT3DRMTEXTURE value) PURE;

    STDMETHOD_(unsigned, GetGroupCount)(THIS) PURE;
    STDMETHOD(GetGroup)(THIS_ D3DRMGROUPINDEX id, unsigned *vCount, unsigned *fCount, unsigned *vPerFace, DWORD *fDataSize, unsigned *fData) PURE;
    STDMETHOD(GetVertices)(THIS_ D3DRMGROUPINDEX id, DWORD index, DWORD count, D3DRMVERTEX *returnPtr) PURE;
    STDMETHOD_(D3DCOLOR, GetGroupColor)(THIS_ D3DRMGROUPINDEX id) PURE;
    STDMETHOD_(D3DRMMAPPING, GetGroupMapping)(THIS_ D3DRMGROUPINDEX id) PURE;
    STDMETHOD_(D3DRMRENDERQUALITY, GetGroupQuality)(THIS_ D3DRMGROUPINDEX id) PURE;
    STDMETHOD(GetGroupMaterial)(THIS_ D3DRMGROUPINDEX id, LPDIRECT3DRMMATERIAL *returnPtr) PURE;
    STDMETHOD(GetGroupTexture)(THIS_ D3DRMGROUPINDEX id, LPDIRECT3DRMTEXTURE *returnPtr) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMProgressiveMesh

DECLARE_INTERFACE_(IDirect3DRMProgressiveMesh, IDirect3DRMVisual)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMProgressiveMesh methods
     */
    STDMETHOD(Load) (THIS_ LPVOID lpObjLocation, LPVOID lpObjId, 
		     D3DRMLOADOPTIONS dloLoadflags, D3DRMLOADTEXTURECALLBACK lpCallback,
		     LPVOID lpArg) PURE;
    STDMETHOD(GetLoadStatus) (THIS_ LPD3DRMPMESHLOADSTATUS lpStatus) PURE;
    STDMETHOD(SetMinRenderDetail) (THIS_ D3DVALUE d3dVal) PURE;
    STDMETHOD(Abort) (THIS_ DWORD dwFlags) PURE;
    
    STDMETHOD(GetFaceDetail) (THIS_ LPDWORD lpdwCount) PURE;
    STDMETHOD(GetVertexDetail) (THIS_ LPDWORD lpdwCount) PURE;
    STDMETHOD(SetFaceDetail) (THIS_ DWORD dwCount) PURE;
    STDMETHOD(SetVertexDetail) (THIS_ DWORD dwCount) PURE;
    STDMETHOD(GetFaceDetailRange) (THIS_ LPDWORD lpdwMin, LPDWORD lpdwMax) PURE;
    STDMETHOD(GetVertexDetailRange) (THIS_ LPDWORD lpdwMin, LPDWORD lpdwMax) PURE;
    STDMETHOD(GetDetail) (THIS_ D3DVALUE *lpdvVal) PURE;
    STDMETHOD(SetDetail) (THIS_ D3DVALUE d3dVal) PURE;

    STDMETHOD(RegisterEvents) (THIS_ HANDLE hEvent, DWORD dwFlags, DWORD dwReserved) PURE;
    STDMETHOD(CreateMesh) (THIS_ LPDIRECT3DRMMESH *lplpD3DRMMesh) PURE;
    STDMETHOD(Duplicate) (THIS_ LPDIRECT3DRMPROGRESSIVEMESH *lplpD3DRMPMesh) PURE;
    STDMETHOD(GetBox) (THIS_ LPD3DRMBOX lpBBox) PURE;
    STDMETHOD(SetQuality) (THIS_ D3DRMRENDERQUALITY) PURE;
    STDMETHOD(GetQuality) (THIS_ LPD3DRMRENDERQUALITY lpdwquality) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMShadow

DECLARE_INTERFACE_(IDirect3DRMShadow, IDirect3DRMVisual)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMShadow methods
     */
    STDMETHOD(Init)
    (	THIS_ LPDIRECT3DRMVISUAL visual, LPDIRECT3DRMLIGHT light,
	D3DVALUE px, D3DVALUE py, D3DVALUE pz,
	D3DVALUE nx, D3DVALUE ny, D3DVALUE nz
    ) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMShadow2

DECLARE_INTERFACE_(IDirect3DRMShadow2, IDirect3DRMVisual)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMShadow methods
     */
    STDMETHOD(Init)
    (	THIS_ LPUNKNOWN pUNK, LPDIRECT3DRMLIGHT light,
	D3DVALUE px, D3DVALUE py, D3DVALUE pz,
	D3DVALUE nx, D3DVALUE ny, D3DVALUE nz
    ) PURE;

    /*
     * IDirect3DRMShadow2 methods
     */
    STDMETHOD(GetVisual)(THIS_ LPDIRECT3DRMVISUAL *) PURE;
    STDMETHOD(SetVisual)(THIS_ LPUNKNOWN pUNK, DWORD) PURE;
    STDMETHOD(GetLight)(THIS_ LPDIRECT3DRMLIGHT *) PURE;
    STDMETHOD(SetLight)(THIS_ LPDIRECT3DRMLIGHT, DWORD) PURE;
    STDMETHOD(GetPlane)(THIS_ LPD3DVALUE px, LPD3DVALUE py, LPD3DVALUE pz,
			LPD3DVALUE nx, LPD3DVALUE ny, LPD3DVALUE nz) PURE;
    STDMETHOD(SetPlane)(THIS_ D3DVALUE px, D3DVALUE py, D3DVALUE pz,
			D3DVALUE nx, D3DVALUE ny, D3DVALUE nz, DWORD) PURE;
    STDMETHOD(GetOptions)(THIS_ LPDWORD) PURE;
    STDMETHOD(SetOptions)(THIS_ DWORD) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMFace

DECLARE_INTERFACE_(IDirect3DRMFace, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMFace methods
     */
     STDMETHOD(AddVertex)(THIS_ D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
     STDMETHOD(AddVertexAndNormalIndexed)(THIS_ DWORD vertex, DWORD normal) PURE;
     STDMETHOD(SetColorRGB)(THIS_ D3DVALUE, D3DVALUE, D3DVALUE) PURE;
     STDMETHOD(SetColor)(THIS_ D3DCOLOR) PURE;
     STDMETHOD(SetTexture)(THIS_ LPDIRECT3DRMTEXTURE) PURE;
     STDMETHOD(SetTextureCoordinates)(THIS_ DWORD vertex, D3DVALUE u, D3DVALUE v) PURE;
     STDMETHOD(SetMaterial)(THIS_ LPDIRECT3DRMMATERIAL) PURE;
     STDMETHOD(SetTextureTopology)(THIS_ BOOL wrap_u, BOOL wrap_v) PURE;

     STDMETHOD(GetVertex)(THIS_ DWORD index, D3DVECTOR *vertex, D3DVECTOR *normal) PURE;
     STDMETHOD(GetVertices)(THIS_ DWORD *vertex_count, D3DVECTOR *coords, D3DVECTOR *normals);
     STDMETHOD(GetTextureCoordinates)(THIS_ DWORD vertex, D3DVALUE *u, D3DVALUE *v) PURE;
     STDMETHOD(GetTextureTopology)(THIS_ BOOL *wrap_u, BOOL *wrap_v) PURE;
     STDMETHOD(GetNormal)(THIS_ D3DVECTOR *) PURE;
     STDMETHOD(GetTexture)(THIS_ LPDIRECT3DRMTEXTURE *) PURE;
     STDMETHOD(GetMaterial)(THIS_ LPDIRECT3DRMMATERIAL *) PURE;

     STDMETHOD_(int, GetVertexCount)(THIS) PURE;
     STDMETHOD_(int, GetVertexIndex)(THIS_ DWORD which) PURE;
     STDMETHOD_(int, GetTextureCoordinateIndex)(THIS_ DWORD which) PURE;
     STDMETHOD_(D3DCOLOR, GetColor)(THIS) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMFace2

DECLARE_INTERFACE_(IDirect3DRMFace2, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMFace methods
     */
     STDMETHOD(AddVertex)(THIS_ D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
     STDMETHOD(AddVertexAndNormalIndexed)(THIS_ DWORD vertex, DWORD normal) PURE;
     STDMETHOD(SetColorRGB)(THIS_ D3DVALUE, D3DVALUE, D3DVALUE) PURE;
     STDMETHOD(SetColor)(THIS_ D3DCOLOR) PURE;
     STDMETHOD(SetTexture)(THIS_ LPDIRECT3DRMTEXTURE3) PURE;
     STDMETHOD(SetTextureCoordinates)(THIS_ DWORD vertex, D3DVALUE u, D3DVALUE v) PURE;
     STDMETHOD(SetMaterial)(THIS_ LPDIRECT3DRMMATERIAL2) PURE;
     STDMETHOD(SetTextureTopology)(THIS_ BOOL wrap_u, BOOL wrap_v) PURE;

     STDMETHOD(GetVertex)(THIS_ DWORD index, D3DVECTOR *vertex, D3DVECTOR *normal) PURE;
     STDMETHOD(GetVertices)(THIS_ DWORD *vertex_count, D3DVECTOR *coords, D3DVECTOR *normals);
     STDMETHOD(GetTextureCoordinates)(THIS_ DWORD vertex, D3DVALUE *u, D3DVALUE *v) PURE;
     STDMETHOD(GetTextureTopology)(THIS_ BOOL *wrap_u, BOOL *wrap_v) PURE;
     STDMETHOD(GetNormal)(THIS_ D3DVECTOR *) PURE;
     STDMETHOD(GetTexture)(THIS_ LPDIRECT3DRMTEXTURE3 *) PURE;
     STDMETHOD(GetMaterial)(THIS_ LPDIRECT3DRMMATERIAL2 *) PURE;

     STDMETHOD_(int, GetVertexCount)(THIS) PURE;
     STDMETHOD_(int, GetVertexIndex)(THIS_ DWORD which) PURE;
     STDMETHOD_(int, GetTextureCoordinateIndex)(THIS_ DWORD which) PURE;
     STDMETHOD_(D3DCOLOR, GetColor)(THIS) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMMeshBuilder

DECLARE_INTERFACE_(IDirect3DRMMeshBuilder, IDirect3DRMVisual)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMMeshBuilder methods
     */
    STDMETHOD(Load)(THIS_ LPVOID filename, LPVOID name, D3DRMLOADOPTIONS loadflags, D3DRMLOADTEXTURECALLBACK, LPVOID lpArg) PURE;
    STDMETHOD(Save)(THIS_ const char *filename, D3DRMXOFFORMAT, D3DRMSAVEOPTIONS save) PURE;
    STDMETHOD(Scale)(THIS_ D3DVALUE sx, D3DVALUE sy, D3DVALUE sz) PURE;
    STDMETHOD(Translate)(THIS_ D3DVALUE tx, D3DVALUE ty, D3DVALUE tz) PURE;
    STDMETHOD(SetColorSource)(THIS_ D3DRMCOLORSOURCE) PURE;
    STDMETHOD(GetBox)(THIS_ D3DRMBOX *) PURE;
    STDMETHOD(GenerateNormals)(THIS) PURE;
    STDMETHOD_(D3DRMCOLORSOURCE, GetColorSource)(THIS) PURE;

    STDMETHOD(AddMesh)(THIS_ LPDIRECT3DRMMESH) PURE;
    STDMETHOD(AddMeshBuilder)(THIS_ LPDIRECT3DRMMESHBUILDER) PURE;
    STDMETHOD(AddFrame)(THIS_ LPDIRECT3DRMFRAME) PURE;
    STDMETHOD(AddFace)(THIS_ LPDIRECT3DRMFACE) PURE;
    STDMETHOD(AddFaces)
    (	THIS_ DWORD vcount, D3DVECTOR *vertices, DWORD ncount, D3DVECTOR *normals,
	DWORD *data, LPDIRECT3DRMFACEARRAY*
    ) PURE;
    STDMETHOD(ReserveSpace)(THIS_ DWORD vertex_Count, DWORD normal_count, DWORD face_count) PURE;
    STDMETHOD(SetColorRGB)(THIS_ D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;
    STDMETHOD(SetColor)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetTexture)(THIS_ LPDIRECT3DRMTEXTURE) PURE;
    STDMETHOD(SetMaterial)(THIS_ LPDIRECT3DRMMATERIAL) PURE;
    STDMETHOD(SetTextureTopology)(THIS_ BOOL wrap_u, BOOL wrap_v) PURE;
    STDMETHOD(SetQuality)(THIS_ D3DRMRENDERQUALITY) PURE;
    STDMETHOD(SetPerspective)(THIS_ BOOL) PURE;
    STDMETHOD(SetVertex)(THIS_ DWORD index, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(SetNormal)(THIS_ DWORD index, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(SetTextureCoordinates)(THIS_ DWORD index, D3DVALUE u, D3DVALUE v) PURE;
    STDMETHOD(SetVertexColor)(THIS_ DWORD index, D3DCOLOR) PURE;
    STDMETHOD(SetVertexColorRGB)(THIS_ DWORD index, D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;

    STDMETHOD(GetFaces)(THIS_ LPDIRECT3DRMFACEARRAY*) PURE;
    STDMETHOD(GetVertices)
    (	THIS_ DWORD *vcount, D3DVECTOR *vertices, DWORD *ncount, D3DVECTOR *normals, DWORD *face_data_size, DWORD *face_data
    ) PURE;
    STDMETHOD(GetTextureCoordinates)(THIS_ DWORD index, D3DVALUE *u, D3DVALUE *v) PURE;

    STDMETHOD_(int, AddVertex)(THIS_ D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD_(int, AddNormal)(THIS_ D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(CreateFace)(THIS_ LPDIRECT3DRMFACE*) PURE;
    STDMETHOD_(D3DRMRENDERQUALITY, GetQuality)(THIS) PURE;
    STDMETHOD_(BOOL, GetPerspective)(THIS) PURE;
    STDMETHOD_(int, GetFaceCount)(THIS) PURE;
    STDMETHOD_(int, GetVertexCount)(THIS) PURE;
    STDMETHOD_(D3DCOLOR, GetVertexColor)(THIS_ DWORD index) PURE;

    STDMETHOD(CreateMesh)(THIS_ LPDIRECT3DRMMESH*) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMMeshBuilder2

DECLARE_INTERFACE_(IDirect3DRMMeshBuilder2, IDirect3DRMMeshBuilder)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMMeshBuilder methods
     */
    STDMETHOD(Load)(THIS_ LPVOID filename, LPVOID name, D3DRMLOADOPTIONS loadflags, D3DRMLOADTEXTURECALLBACK, LPVOID lpArg) PURE;
    STDMETHOD(Save)(THIS_ const char *filename, D3DRMXOFFORMAT, D3DRMSAVEOPTIONS save) PURE;
    STDMETHOD(Scale)(THIS_ D3DVALUE sx, D3DVALUE sy, D3DVALUE sz) PURE;
    STDMETHOD(Translate)(THIS_ D3DVALUE tx, D3DVALUE ty, D3DVALUE tz) PURE;
    STDMETHOD(SetColorSource)(THIS_ D3DRMCOLORSOURCE) PURE;
    STDMETHOD(GetBox)(THIS_ D3DRMBOX *) PURE;
    STDMETHOD(GenerateNormals)(THIS) PURE;
    STDMETHOD_(D3DRMCOLORSOURCE, GetColorSource)(THIS) PURE;

    STDMETHOD(AddMesh)(THIS_ LPDIRECT3DRMMESH) PURE;
    STDMETHOD(AddMeshBuilder)(THIS_ LPDIRECT3DRMMESHBUILDER) PURE;
    STDMETHOD(AddFrame)(THIS_ LPDIRECT3DRMFRAME) PURE;
    STDMETHOD(AddFace)(THIS_ LPDIRECT3DRMFACE) PURE;
    STDMETHOD(AddFaces)
    (	THIS_ DWORD vcount, D3DVECTOR *vertices, DWORD ncount, D3DVECTOR *normals,
	DWORD *data, LPDIRECT3DRMFACEARRAY*
    ) PURE;
    STDMETHOD(ReserveSpace)(THIS_ DWORD vertex_Count, DWORD normal_count, DWORD face_count) PURE;
    STDMETHOD(SetColorRGB)(THIS_ D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;
    STDMETHOD(SetColor)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetTexture)(THIS_ LPDIRECT3DRMTEXTURE) PURE;
    STDMETHOD(SetMaterial)(THIS_ LPDIRECT3DRMMATERIAL) PURE;
    STDMETHOD(SetTextureTopology)(THIS_ BOOL wrap_u, BOOL wrap_v) PURE;
    STDMETHOD(SetQuality)(THIS_ D3DRMRENDERQUALITY) PURE;
    STDMETHOD(SetPerspective)(THIS_ BOOL) PURE;
    STDMETHOD(SetVertex)(THIS_ DWORD index, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(SetNormal)(THIS_ DWORD index, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(SetTextureCoordinates)(THIS_ DWORD index, D3DVALUE u, D3DVALUE v) PURE;
    STDMETHOD(SetVertexColor)(THIS_ DWORD index, D3DCOLOR) PURE;
    STDMETHOD(SetVertexColorRGB)(THIS_ DWORD index, D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;

    STDMETHOD(GetFaces)(THIS_ LPDIRECT3DRMFACEARRAY*) PURE;
    STDMETHOD(GetVertices)
    (	THIS_ DWORD *vcount, D3DVECTOR *vertices, DWORD *ncount, D3DVECTOR *normals, DWORD *face_data_size, DWORD *face_data
    ) PURE;
    STDMETHOD(GetTextureCoordinates)(THIS_ DWORD index, D3DVALUE *u, D3DVALUE *v) PURE;

    STDMETHOD_(int, AddVertex)(THIS_ D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD_(int, AddNormal)(THIS_ D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(CreateFace)(THIS_ LPDIRECT3DRMFACE*) PURE;
    STDMETHOD_(D3DRMRENDERQUALITY, GetQuality)(THIS) PURE;
    STDMETHOD_(BOOL, GetPerspective)(THIS) PURE;
    STDMETHOD_(int, GetFaceCount)(THIS) PURE;
    STDMETHOD_(int, GetVertexCount)(THIS) PURE;
    STDMETHOD_(D3DCOLOR, GetVertexColor)(THIS_ DWORD index) PURE;

    STDMETHOD(CreateMesh)(THIS_ LPDIRECT3DRMMESH*) PURE;

    /*
     * IDirect3DRMMeshBuilder2 methods
     */
    STDMETHOD(GenerateNormals2)(THIS_ D3DVALUE crease, DWORD dwFlags) PURE;
    STDMETHOD(GetFace)(THIS_ DWORD index, LPDIRECT3DRMFACE*) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMMeshBuilder3

DECLARE_INTERFACE_(IDirect3DRMMeshBuilder3, IDirect3DRMVisual)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMMeshBuilder3 methods
     */
    STDMETHOD(Load)(THIS_ LPVOID filename, LPVOID name, D3DRMLOADOPTIONS loadflags, D3DRMLOADTEXTURE3CALLBACK, LPVOID lpArg) PURE;
    STDMETHOD(Save)(THIS_ const char *filename, D3DRMXOFFORMAT, D3DRMSAVEOPTIONS save) PURE;
    STDMETHOD(Scale)(THIS_ D3DVALUE sx, D3DVALUE sy, D3DVALUE sz) PURE;
    STDMETHOD(Translate)(THIS_ D3DVALUE tx, D3DVALUE ty, D3DVALUE tz) PURE;
    STDMETHOD(SetColorSource)(THIS_ D3DRMCOLORSOURCE) PURE;
    STDMETHOD(GetBox)(THIS_ D3DRMBOX *) PURE;
    STDMETHOD(GenerateNormals)(THIS_ D3DVALUE crease, DWORD dwFlags) PURE;
    STDMETHOD_(D3DRMCOLORSOURCE, GetColorSource)(THIS) PURE;

    STDMETHOD(AddMesh)(THIS_ LPDIRECT3DRMMESH) PURE;
    STDMETHOD(AddMeshBuilder)(THIS_ LPDIRECT3DRMMESHBUILDER3, DWORD dwFlags) PURE;
    STDMETHOD(AddFrame)(THIS_ LPDIRECT3DRMFRAME3) PURE;
    STDMETHOD(AddFace)(THIS_ LPDIRECT3DRMFACE2) PURE;
    STDMETHOD(AddFaces)
    (	THIS_ DWORD vcount, D3DVECTOR *vertices, DWORD ncount, D3DVECTOR *normals,
	DWORD *data, LPDIRECT3DRMFACEARRAY*
    ) PURE;
    STDMETHOD(ReserveSpace)(THIS_ DWORD vertex_Count, DWORD normal_count, DWORD face_count) PURE;
    STDMETHOD(SetColorRGB)(THIS_ D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;
    STDMETHOD(SetColor)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetTexture)(THIS_ LPDIRECT3DRMTEXTURE3) PURE;
    STDMETHOD(SetMaterial)(THIS_ LPDIRECT3DRMMATERIAL2) PURE;
    STDMETHOD(SetTextureTopology)(THIS_ BOOL wrap_u, BOOL wrap_v) PURE;
    STDMETHOD(SetQuality)(THIS_ D3DRMRENDERQUALITY) PURE;
    STDMETHOD(SetPerspective)(THIS_ BOOL) PURE;
    STDMETHOD(SetVertex)(THIS_ DWORD index, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(SetNormal)(THIS_ DWORD index, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(SetTextureCoordinates)(THIS_ DWORD index, D3DVALUE u, D3DVALUE v) PURE;
    STDMETHOD(SetVertexColor)(THIS_ DWORD index, D3DCOLOR) PURE;
    STDMETHOD(SetVertexColorRGB)(THIS_ DWORD index, D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;
    STDMETHOD(GetFaces)(THIS_ LPDIRECT3DRMFACEARRAY*) PURE;
    STDMETHOD(GetGeometry)
    (	THIS_ DWORD *vcount, D3DVECTOR *vertices, DWORD *ncount, D3DVECTOR *normals, DWORD *face_data_size, DWORD *face_data
    ) PURE;
    STDMETHOD(GetTextureCoordinates)(THIS_ DWORD index, D3DVALUE *u, D3DVALUE *v) PURE;
    STDMETHOD_(int, AddVertex)(THIS_ D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD_(int, AddNormal)(THIS_ D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(CreateFace)(THIS_ LPDIRECT3DRMFACE2 *) PURE;
    STDMETHOD_(D3DRMRENDERQUALITY, GetQuality)(THIS) PURE;
    STDMETHOD_(BOOL, GetPerspective)(THIS) PURE;
    STDMETHOD_(int, GetFaceCount)(THIS) PURE;
    STDMETHOD_(int, GetVertexCount)(THIS) PURE;
    STDMETHOD_(D3DCOLOR, GetVertexColor)(THIS_ DWORD index) PURE;
    STDMETHOD(CreateMesh)(THIS_ LPDIRECT3DRMMESH*) PURE;
    STDMETHOD(GetFace)(THIS_ DWORD index, LPDIRECT3DRMFACE2 *) PURE;
    STDMETHOD(GetVertex)(THIS_ DWORD dwIndex, LPD3DVECTOR lpVector) PURE;
    STDMETHOD(GetNormal)(THIS_ DWORD dwIndex, LPD3DVECTOR lpVector) PURE;
    STDMETHOD(DeleteVertices)(THIS_ DWORD dwIndexFirst, DWORD dwCount) PURE;
    STDMETHOD(DeleteNormals)(THIS_ DWORD dwIndexFirst, DWORD dwCount) PURE;
    STDMETHOD(DeleteFace)(THIS_ LPDIRECT3DRMFACE2) PURE;
    STDMETHOD(Empty)(THIS_ DWORD dwFlags) PURE;
    STDMETHOD(Optimize)(THIS_ DWORD dwFlags) PURE;
    STDMETHOD(AddFacesIndexed)(THIS_ DWORD dwFlags, DWORD *lpdwvIndices, DWORD *dwIndexFirst, DWORD *dwCount) PURE;
    STDMETHOD(CreateSubMesh)(THIS_ LPUNKNOWN *) PURE;
    STDMETHOD(GetParentMesh)(THIS_ DWORD, LPUNKNOWN *) PURE;
    STDMETHOD(GetSubMeshes)(THIS_ LPDWORD lpdwCount, LPUNKNOWN *) PURE;
    STDMETHOD(DeleteSubMesh)(THIS_ LPUNKNOWN) PURE;
    STDMETHOD(Enable)(THIS_ DWORD) PURE;
    STDMETHOD(GetEnable)(THIS_ DWORD *) PURE;
    STDMETHOD(AddTriangles)(THIS_ DWORD dwFlags, DWORD dwFormat,
			    DWORD dwVertexCount, LPVOID lpvData) PURE;
    STDMETHOD(SetVertices)(THIS_ DWORD dwIndexFirst, DWORD dwCount, LPD3DVECTOR) PURE;
    STDMETHOD(GetVertices)(THIS_ DWORD dwIndexFirst, LPDWORD lpdwCount, LPD3DVECTOR) PURE;
    STDMETHOD(SetNormals)(THIS_ DWORD dwIndexFirst, DWORD dwCount, LPD3DVECTOR) PURE;
    STDMETHOD(GetNormals)(THIS_ DWORD dwIndexFirst, LPDWORD lpdwCount, LPD3DVECTOR) PURE;
    STDMETHOD_(int, GetNormalCount)(THIS) PURE;
};  

#undef INTERFACE
#define INTERFACE IDirect3DRMLight

DECLARE_INTERFACE_(IDirect3DRMLight, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMLight methods
     */
    STDMETHOD(SetType)(THIS_ D3DRMLIGHTTYPE) PURE;
    STDMETHOD(SetColor)(THIS_ D3DCOLOR) PURE;
    STDMETHOD(SetColorRGB)(THIS_ D3DVALUE red, D3DVALUE green, D3DVALUE blue) PURE;
    STDMETHOD(SetRange)(THIS_ D3DVALUE) PURE;
    STDMETHOD(SetUmbra)(THIS_ D3DVALUE) PURE;
    STDMETHOD(SetPenumbra)(THIS_ D3DVALUE) PURE;
    STDMETHOD(SetConstantAttenuation)(THIS_ D3DVALUE) PURE;
    STDMETHOD(SetLinearAttenuation)(THIS_ D3DVALUE) PURE;
    STDMETHOD(SetQuadraticAttenuation)(THIS_ D3DVALUE) PURE;

    STDMETHOD_(D3DVALUE, GetRange)(THIS) PURE;
    STDMETHOD_(D3DVALUE, GetUmbra)(THIS) PURE;
    STDMETHOD_(D3DVALUE, GetPenumbra)(THIS) PURE;
    STDMETHOD_(D3DVALUE, GetConstantAttenuation)(THIS) PURE;
    STDMETHOD_(D3DVALUE, GetLinearAttenuation)(THIS) PURE;
    STDMETHOD_(D3DVALUE, GetQuadraticAttenuation)(THIS) PURE;
    STDMETHOD_(D3DCOLOR, GetColor)(THIS) PURE;
    STDMETHOD_(D3DRMLIGHTTYPE, GetType)(THIS) PURE;

    STDMETHOD(SetEnableFrame)(THIS_ LPDIRECT3DRMFRAME) PURE;
    STDMETHOD(GetEnableFrame)(THIS_ LPDIRECT3DRMFRAME*) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMTexture

DECLARE_INTERFACE_(IDirect3DRMTexture, IDirect3DRMVisual)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMTexture methods
     */
    STDMETHOD(InitFromFile)(THIS_ const char *filename) PURE;
    STDMETHOD(InitFromSurface)(THIS_ LPDIRECTDRAWSURFACE lpDDS) PURE;
    STDMETHOD(InitFromResource)(THIS_ HRSRC) PURE;
    STDMETHOD(Changed)(THIS_ BOOL pixels, BOOL palette) PURE;

    STDMETHOD(SetColors)(THIS_ DWORD) PURE;
    STDMETHOD(SetShades)(THIS_ DWORD) PURE;
    STDMETHOD(SetDecalSize)(THIS_ D3DVALUE width, D3DVALUE height) PURE;
    STDMETHOD(SetDecalOrigin)(THIS_ LONG x, LONG y) PURE;
    STDMETHOD(SetDecalScale)(THIS_ DWORD) PURE;
    STDMETHOD(SetDecalTransparency)(THIS_ BOOL) PURE;
    STDMETHOD(SetDecalTransparentColor)(THIS_ D3DCOLOR) PURE;

    STDMETHOD(GetDecalSize)(THIS_ D3DVALUE *width_return, D3DVALUE *height_return) PURE;
    STDMETHOD(GetDecalOrigin)(THIS_ LONG *x_return, LONG *y_return) PURE;

    STDMETHOD_(D3DRMIMAGE *, GetImage)(THIS) PURE;
    STDMETHOD_(DWORD, GetShades)(THIS) PURE;
    STDMETHOD_(DWORD, GetColors)(THIS) PURE;
    STDMETHOD_(DWORD, GetDecalScale)(THIS) PURE;
    STDMETHOD_(BOOL, GetDecalTransparency)(THIS) PURE;
    STDMETHOD_(D3DCOLOR, GetDecalTransparentColor)(THIS) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMTexture2

DECLARE_INTERFACE_(IDirect3DRMTexture2, IDirect3DRMTexture)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMTexture methods
     */
    STDMETHOD(InitFromFile)(THIS_ const char *filename) PURE;
    STDMETHOD(InitFromSurface)(THIS_ LPDIRECTDRAWSURFACE lpDDS) PURE;
    STDMETHOD(InitFromResource)(THIS_ HRSRC) PURE;
    STDMETHOD(Changed)(THIS_ BOOL pixels, BOOL palette) PURE;

    STDMETHOD(SetColors)(THIS_ DWORD) PURE;
    STDMETHOD(SetShades)(THIS_ DWORD) PURE;
    STDMETHOD(SetDecalSize)(THIS_ D3DVALUE width, D3DVALUE height) PURE;
    STDMETHOD(SetDecalOrigin)(THIS_ LONG x, LONG y) PURE;
    STDMETHOD(SetDecalScale)(THIS_ DWORD) PURE;
    STDMETHOD(SetDecalTransparency)(THIS_ BOOL) PURE;
    STDMETHOD(SetDecalTransparentColor)(THIS_ D3DCOLOR) PURE;

    STDMETHOD(GetDecalSize)(THIS_ D3DVALUE *width_return, D3DVALUE *height_return) PURE;
    STDMETHOD(GetDecalOrigin)(THIS_ LONG *x_return, LONG *y_return) PURE;

    STDMETHOD_(D3DRMIMAGE *, GetImage)(THIS) PURE;
    STDMETHOD_(DWORD, GetShades)(THIS) PURE;
    STDMETHOD_(DWORD, GetColors)(THIS) PURE;
    STDMETHOD_(DWORD, GetDecalScale)(THIS) PURE;
    STDMETHOD_(BOOL, GetDecalTransparency)(THIS) PURE;
    STDMETHOD_(D3DCOLOR, GetDecalTransparentColor)(THIS) PURE;

    /*
     * IDirect3DRMTexture2 methods
     */
    STDMETHOD(InitFromImage)(THIS_ LPD3DRMIMAGE) PURE;
    STDMETHOD(InitFromResource2)(THIS_ HMODULE hModule, LPCTSTR strName, LPCTSTR strType) PURE;
    STDMETHOD(GenerateMIPMap)(THIS_ DWORD) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMTexture3

DECLARE_INTERFACE_(IDirect3DRMTexture3, IDirect3DRMVisual)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMTexture3 methods
     */
    STDMETHOD(InitFromFile)(THIS_ const char *filename) PURE;
    STDMETHOD(InitFromSurface)(THIS_ LPDIRECTDRAWSURFACE lpDDS) PURE;
    STDMETHOD(InitFromResource)(THIS_ HRSRC) PURE;
    STDMETHOD(Changed)(THIS_ DWORD dwFlags, DWORD dwcRects, LPRECT pRects) PURE;
    STDMETHOD(SetColors)(THIS_ DWORD) PURE;
    STDMETHOD(SetShades)(THIS_ DWORD) PURE;
    STDMETHOD(SetDecalSize)(THIS_ D3DVALUE width, D3DVALUE height) PURE;
    STDMETHOD(SetDecalOrigin)(THIS_ LONG x, LONG y) PURE;
    STDMETHOD(SetDecalScale)(THIS_ DWORD) PURE;
    STDMETHOD(SetDecalTransparency)(THIS_ BOOL) PURE;
    STDMETHOD(SetDecalTransparentColor)(THIS_ D3DCOLOR) PURE;

    STDMETHOD(GetDecalSize)(THIS_ D3DVALUE *width_return, D3DVALUE *height_return) PURE;
    STDMETHOD(GetDecalOrigin)(THIS_ LONG *x_return, LONG *y_return) PURE;

    STDMETHOD_(D3DRMIMAGE *, GetImage)(THIS) PURE;
    STDMETHOD_(DWORD, GetShades)(THIS) PURE;
    STDMETHOD_(DWORD, GetColors)(THIS) PURE;
    STDMETHOD_(DWORD, GetDecalScale)(THIS) PURE;
    STDMETHOD_(BOOL, GetDecalTransparency)(THIS) PURE;
    STDMETHOD_(D3DCOLOR, GetDecalTransparentColor)(THIS) PURE;
    STDMETHOD(InitFromImage)(THIS_ LPD3DRMIMAGE) PURE;
    STDMETHOD(InitFromResource2)(THIS_ HMODULE hModule, LPCTSTR strName, LPCTSTR strType) PURE;
    STDMETHOD(GenerateMIPMap)(THIS_ DWORD) PURE;
    STDMETHOD(GetSurface)(THIS_ DWORD dwFlags, LPDIRECTDRAWSURFACE* lplpDDS) PURE;
    STDMETHOD(SetCacheOptions)(THIS_ LONG lImportance, DWORD dwFlags) PURE;
    STDMETHOD(GetCacheOptions)(THIS_ LPLONG lplImportance, LPDWORD lpdwFlags) PURE;
    STDMETHOD(SetDownsampleCallback)(THIS_ D3DRMDOWNSAMPLECALLBACK pCallback, LPVOID pArg) PURE;
    STDMETHOD(SetValidationCallback)(THIS_ D3DRMVALIDATIONCALLBACK pCallback, LPVOID pArg) PURE;
};


#undef INTERFACE
#define INTERFACE IDirect3DRMWrap

DECLARE_INTERFACE_(IDirect3DRMWrap, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMWrap methods
     */
    STDMETHOD(Init)
    (	THIS_ D3DRMWRAPTYPE, LPDIRECT3DRMFRAME ref,
	D3DVALUE ox, D3DVALUE oy, D3DVALUE oz,
	D3DVALUE dx, D3DVALUE dy, D3DVALUE dz,
	D3DVALUE ux, D3DVALUE uy, D3DVALUE uz,
	D3DVALUE ou, D3DVALUE ov,
	D3DVALUE su, D3DVALUE sv
    ) PURE;
    STDMETHOD(Apply)(THIS_ LPDIRECT3DRMOBJECT) PURE;
    STDMETHOD(ApplyRelative)(THIS_ LPDIRECT3DRMFRAME frame, LPDIRECT3DRMOBJECT) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMMaterial

DECLARE_INTERFACE_(IDirect3DRMMaterial, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMMaterial methods
     */
    STDMETHOD(SetPower)(THIS_ D3DVALUE power) PURE;
    STDMETHOD(SetSpecular)(THIS_ D3DVALUE r, D3DVALUE g, D3DVALUE b) PURE;
    STDMETHOD(SetEmissive)(THIS_ D3DVALUE r, D3DVALUE g, D3DVALUE b) PURE;

    STDMETHOD_(D3DVALUE, GetPower)(THIS) PURE;
    STDMETHOD(GetSpecular)(THIS_ D3DVALUE* r, D3DVALUE* g, D3DVALUE* b) PURE;
    STDMETHOD(GetEmissive)(THIS_ D3DVALUE* r, D3DVALUE* g, D3DVALUE* b) PURE;
};


#undef INTERFACE
#define INTERFACE IDirect3DRMMaterial2

DECLARE_INTERFACE_(IDirect3DRMMaterial2, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMMaterial2 methods
     */
    STDMETHOD(SetPower)(THIS_ D3DVALUE power) PURE;
    STDMETHOD(SetSpecular)(THIS_ D3DVALUE r, D3DVALUE g, D3DVALUE b) PURE;
    STDMETHOD(SetEmissive)(THIS_ D3DVALUE r, D3DVALUE g, D3DVALUE b) PURE;
    STDMETHOD_(D3DVALUE, GetPower)(THIS) PURE;
    STDMETHOD(GetSpecular)(THIS_ D3DVALUE* r, D3DVALUE* g, D3DVALUE* b) PURE;
    STDMETHOD(GetEmissive)(THIS_ D3DVALUE* r, D3DVALUE* g, D3DVALUE* b) PURE;
    STDMETHOD(GetAmbient)(THIS_ D3DVALUE* r, D3DVALUE* g, D3DVALUE* b) PURE;
    STDMETHOD(SetAmbient)(THIS_ D3DVALUE r, D3DVALUE g, D3DVALUE b) PURE;
};


#undef INTERFACE
#define INTERFACE IDirect3DRMAnimation

DECLARE_INTERFACE_(IDirect3DRMAnimation, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMAnimation methods
     */
    STDMETHOD(SetOptions)(THIS_ D3DRMANIMATIONOPTIONS flags) PURE;
    STDMETHOD(AddRotateKey)(THIS_ D3DVALUE time, D3DRMQUATERNION *q) PURE;
    STDMETHOD(AddPositionKey)(THIS_ D3DVALUE time, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(AddScaleKey)(THIS_ D3DVALUE time, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(DeleteKey)(THIS_ D3DVALUE time) PURE;
    STDMETHOD(SetFrame)(THIS_ LPDIRECT3DRMFRAME frame) PURE;
    STDMETHOD(SetTime)(THIS_ D3DVALUE time) PURE;

    STDMETHOD_(D3DRMANIMATIONOPTIONS, GetOptions)(THIS) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMAnimation2

DECLARE_INTERFACE_(IDirect3DRMAnimation2, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMAnimation2 methods
     */
    STDMETHOD(SetOptions)(THIS_ D3DRMANIMATIONOPTIONS flags) PURE;
    STDMETHOD(AddRotateKey)(THIS_ D3DVALUE time, D3DRMQUATERNION *q) PURE;
    STDMETHOD(AddPositionKey)(THIS_ D3DVALUE time, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(AddScaleKey)(THIS_ D3DVALUE time, D3DVALUE x, D3DVALUE y, D3DVALUE z) PURE;
    STDMETHOD(DeleteKey)(THIS_ D3DVALUE time) PURE;
    STDMETHOD(SetFrame)(THIS_ LPDIRECT3DRMFRAME3 frame) PURE;
    STDMETHOD(SetTime)(THIS_ D3DVALUE time) PURE;

    STDMETHOD_(D3DRMANIMATIONOPTIONS, GetOptions)(THIS) PURE;
    STDMETHOD(GetFrame)(THIS_ LPDIRECT3DRMFRAME3 *lpD3DFrame) PURE;
    STDMETHOD(DeleteKeyByID)(THIS_ DWORD dwID) PURE;
    STDMETHOD(AddKey)(THIS_ LPD3DRMANIMATIONKEY lpKey) PURE;
    STDMETHOD(ModifyKey)(THIS_ LPD3DRMANIMATIONKEY lpKey) PURE;
    STDMETHOD(GetKeys)(THIS_ D3DVALUE dvTimeMin,
		       D3DVALUE dvTimeMax, LPDWORD lpdwNumKeys,
		       LPD3DRMANIMATIONKEY lpKey);
};

#undef INTERFACE
#define INTERFACE IDirect3DRMAnimationSet

DECLARE_INTERFACE_(IDirect3DRMAnimationSet, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMAnimationSet methods
     */
    STDMETHOD(AddAnimation)(THIS_ LPDIRECT3DRMANIMATION aid) PURE;
    STDMETHOD(Load)(THIS_ LPVOID filename, LPVOID name, D3DRMLOADOPTIONS loadflags, D3DRMLOADTEXTURECALLBACK, LPVOID lpArg, LPDIRECT3DRMFRAME parent)PURE;
    STDMETHOD(DeleteAnimation)(THIS_ LPDIRECT3DRMANIMATION aid) PURE;
    STDMETHOD(SetTime)(THIS_ D3DVALUE time) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMAnimationSet2

DECLARE_INTERFACE_(IDirect3DRMAnimationSet2, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMAnimationSet2 methods
     */
    STDMETHOD(AddAnimation)(THIS_ LPDIRECT3DRMANIMATION2 aid) PURE;
    STDMETHOD(Load)(THIS_ LPVOID filename, LPVOID name, D3DRMLOADOPTIONS loadflags, D3DRMLOADTEXTURE3CALLBACK, LPVOID lpArg, LPDIRECT3DRMFRAME3 parent)PURE;
    STDMETHOD(DeleteAnimation)(THIS_ LPDIRECT3DRMANIMATION2 aid) PURE;
    STDMETHOD(SetTime)(THIS_ D3DVALUE time) PURE;
    STDMETHOD(GetAnimations)(THIS_ LPDIRECT3DRMANIMATIONARRAY *) PURE;
};


#undef INTERFACE
#define INTERFACE IDirect3DRMUserVisual

DECLARE_INTERFACE_(IDirect3DRMUserVisual, IDirect3DRMVisual)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMUserVisual methods
     */
    STDMETHOD(Init)(THIS_ D3DRMUSERVISUALCALLBACK fn, void *arg) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMArray

DECLARE_INTERFACE_(IDirect3DRMArray, IUnknown)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD_(DWORD, GetSize)(THIS) PURE;
    /* No GetElement method as it would get overloaded
     * in derived classes, and overloading is
     * a no-no in COM
     */
};

#undef INTERFACE
#define INTERFACE IDirect3DRMObjectArray

DECLARE_INTERFACE_(IDirect3DRMObjectArray, IDirect3DRMArray)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD_(DWORD, GetSize)(THIS) PURE;
    STDMETHOD(GetElement)(THIS_ DWORD index, LPDIRECT3DRMOBJECT *) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMDeviceArray

DECLARE_INTERFACE_(IDirect3DRMDeviceArray, IDirect3DRMArray)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD_(DWORD, GetSize)(THIS) PURE;
    STDMETHOD(GetElement)(THIS_ DWORD index, LPDIRECT3DRMDEVICE *) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMFrameArray

DECLARE_INTERFACE_(IDirect3DRMFrameArray, IDirect3DRMArray)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD_(DWORD, GetSize)(THIS) PURE;
    STDMETHOD(GetElement)(THIS_ DWORD index, LPDIRECT3DRMFRAME *) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMViewportArray

DECLARE_INTERFACE_(IDirect3DRMViewportArray, IDirect3DRMArray)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD_(DWORD, GetSize)(THIS) PURE;
    STDMETHOD(GetElement)(THIS_ DWORD index, LPDIRECT3DRMVIEWPORT *) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMVisualArray

DECLARE_INTERFACE_(IDirect3DRMVisualArray, IDirect3DRMArray)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD_(DWORD, GetSize)(THIS) PURE;
    STDMETHOD(GetElement)(THIS_ DWORD index, LPDIRECT3DRMVISUAL *) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMAnimationArray

DECLARE_INTERFACE_(IDirect3DRMAnimationArray, IDirect3DRMArray)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD_(DWORD, GetSize)(THIS) PURE;
    STDMETHOD(GetElement)(THIS_ DWORD index, LPDIRECT3DRMANIMATION2 *) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMPickedArray

DECLARE_INTERFACE_(IDirect3DRMPickedArray, IDirect3DRMArray)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD_(DWORD, GetSize)(THIS) PURE;
    STDMETHOD(GetPick)(THIS_ DWORD index, LPDIRECT3DRMVISUAL *, LPDIRECT3DRMFRAMEARRAY *, LPD3DRMPICKDESC) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMLightArray

DECLARE_INTERFACE_(IDirect3DRMLightArray, IDirect3DRMArray)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD_(DWORD, GetSize)(THIS) PURE;
    STDMETHOD(GetElement)(THIS_ DWORD index, LPDIRECT3DRMLIGHT *) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMFaceArray

DECLARE_INTERFACE_(IDirect3DRMFaceArray, IDirect3DRMArray)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD_(DWORD, GetSize)(THIS) PURE;
    STDMETHOD(GetElement)(THIS_ DWORD index, LPDIRECT3DRMFACE *) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMPicked2Array

DECLARE_INTERFACE_(IDirect3DRMPicked2Array, IDirect3DRMArray)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD_(DWORD, GetSize)(THIS) PURE;
    STDMETHOD(GetPick)(THIS_ DWORD index, LPDIRECT3DRMVISUAL *, LPDIRECT3DRMFRAMEARRAY *, LPD3DRMPICKDESC2) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMInterpolator

DECLARE_INTERFACE_(IDirect3DRMInterpolator, IDirect3DRMObject)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMInterpolator methods
     */
    STDMETHOD(AttachObject)(THIS_ LPDIRECT3DRMOBJECT) PURE;
    STDMETHOD(GetAttachedObjects)(THIS_ LPDIRECT3DRMOBJECTARRAY *) PURE;
    STDMETHOD(DetachObject)(THIS_ LPDIRECT3DRMOBJECT) PURE;
    STDMETHOD(SetIndex)(THIS_ D3DVALUE) PURE;
    STDMETHOD_(D3DVALUE, GetIndex)(THIS) PURE;
    STDMETHOD(Interpolate)(THIS_ D3DVALUE, LPDIRECT3DRMOBJECT, D3DRMINTERPOLATIONOPTIONS) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRMClippedVisual

DECLARE_INTERFACE_(IDirect3DRMClippedVisual, IDirect3DRMVisual)
{
    IUNKNOWN_METHODS(PURE);
    IDIRECT3DRMOBJECT_METHODS(PURE);

    /*
     * IDirect3DRMClippedVisual methods
     */
    STDMETHOD(Init) (THIS_ LPDIRECT3DRMVISUAL) PURE;
    STDMETHOD(AddPlane) (THIS_ LPDIRECT3DRMFRAME3, LPD3DVECTOR, LPD3DVECTOR, DWORD, LPDWORD) PURE;
    STDMETHOD(DeletePlane)(THIS_ DWORD, DWORD) PURE;
    STDMETHOD(GetPlaneIDs)(THIS_ LPDWORD, LPDWORD, DWORD) PURE;
    STDMETHOD(GetPlane) (THIS_ DWORD, LPDIRECT3DRMFRAME3, LPD3DVECTOR, LPD3DVECTOR, DWORD) PURE;
    STDMETHOD(SetPlane) (THIS_ DWORD, LPDIRECT3DRMFRAME3, LPD3DVECTOR, LPD3DVECTOR, DWORD) PURE;
};

#ifdef __cplusplus
};
#endif
#endif /* _D3DRMOBJ_H_ */


#ifdef __cplusplus
extern "C" {
#endif


DEFINE_GUID(IID_IDirect3DRM,	0x2bc49361, 0x8327, 0x11cf, 0xac, 0x4a, 0x0, 0x0, 0xc0, 0x38, 0x25, 0xa1);
DEFINE_GUID(IID_IDirect3DRM2,	0x4516ecc8, 0x8f20, 0x11d0, 0x9b, 0x6d, 0x00, 0x00, 0xc0, 0x78, 0x1b, 0xc3);
DEFINE_GUID(IID_IDirect3DRM3,   0x4516ec83, 0x8f20, 0x11d0, 0x9b, 0x6d, 0x00, 0x00, 0xc0, 0x78, 0x1b, 0xc3);
WIN_TYPES(IDirect3DRM, DIRECT3DRM);
WIN_TYPES(IDirect3DRM2, DIRECT3DRM2);
WIN_TYPES(IDirect3DRM3, DIRECT3DRM3);

/*
 * Direct3DRM Object Class (for CoCreateInstance())
 */
DEFINE_GUID(CLSID_CDirect3DRM,  0x4516ec41, 0x8f20, 0x11d0, 0x9b, 0x6d, 0x00, 0x00, 0xc0, 0x78, 0x1b, 0xc3);


/* Create a Direct3DRM API */
STDAPI Direct3DRMCreate(LPDIRECT3DRM FAR *lplpDirect3DRM);

#undef INTERFACE
#define INTERFACE IDirect3DRM

DECLARE_INTERFACE_(IDirect3DRM, IUnknown)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD(CreateObject)
	(THIS_ REFCLSID rclsid, LPUNKNOWN pUnkOuter, REFIID riid, LPVOID FAR* ppv) PURE;
    STDMETHOD(CreateFrame)	(THIS_ LPDIRECT3DRMFRAME, LPDIRECT3DRMFRAME *) PURE;
    STDMETHOD(CreateMesh)	(THIS_ LPDIRECT3DRMMESH *) PURE;
    STDMETHOD(CreateMeshBuilder)(THIS_ LPDIRECT3DRMMESHBUILDER *) PURE;
    STDMETHOD(CreateFace)	(THIS_ LPDIRECT3DRMFACE *) PURE;
    STDMETHOD(CreateAnimation)	(THIS_ LPDIRECT3DRMANIMATION *) PURE;
    STDMETHOD(CreateAnimationSet)(THIS_ LPDIRECT3DRMANIMATIONSET *) PURE;
    STDMETHOD(CreateTexture)	(THIS_ LPD3DRMIMAGE, LPDIRECT3DRMTEXTURE *) PURE;
    STDMETHOD(CreateLight)	(THIS_ D3DRMLIGHTTYPE, D3DCOLOR, LPDIRECT3DRMLIGHT *) PURE;
    STDMETHOD(CreateLightRGB)
	(THIS_ D3DRMLIGHTTYPE, D3DVALUE, D3DVALUE, D3DVALUE, LPDIRECT3DRMLIGHT *) PURE;
    STDMETHOD(CreateMaterial)	(THIS_ D3DVALUE, LPDIRECT3DRMMATERIAL *) PURE;
    STDMETHOD(CreateDevice)	(THIS_ DWORD, DWORD, LPDIRECT3DRMDEVICE *) PURE;

    /* Create a Windows Device using DirectDraw surfaces */
    STDMETHOD(CreateDeviceFromSurface)
    (	THIS_ LPGUID lpGUID, LPDIRECTDRAW lpDD,
	LPDIRECTDRAWSURFACE lpDDSBack, LPDIRECT3DRMDEVICE *
    ) PURE;

    /* Create a Windows Device using D3D objects */
    STDMETHOD(CreateDeviceFromD3D)
    (	THIS_ LPDIRECT3D lpD3D, LPDIRECT3DDEVICE lpD3DDev,
	LPDIRECT3DRMDEVICE *
    ) PURE;

    STDMETHOD(CreateDeviceFromClipper)
    (	THIS_ LPDIRECTDRAWCLIPPER lpDDClipper, LPGUID lpGUID,
	int width, int height, LPDIRECT3DRMDEVICE *) PURE;

    STDMETHOD(CreateTextureFromSurface)(THIS_ LPDIRECTDRAWSURFACE lpDDS, LPDIRECT3DRMTEXTURE *) PURE;
   
    STDMETHOD(CreateShadow)
    (	THIS_ LPDIRECT3DRMVISUAL, LPDIRECT3DRMLIGHT,
	D3DVALUE px, D3DVALUE py, D3DVALUE pz,
	D3DVALUE nx, D3DVALUE ny, D3DVALUE nz,
	LPDIRECT3DRMVISUAL *
    ) PURE;
    STDMETHOD(CreateViewport)
    (	THIS_ LPDIRECT3DRMDEVICE, LPDIRECT3DRMFRAME, DWORD, DWORD,
	DWORD, DWORD, LPDIRECT3DRMVIEWPORT *
    ) PURE;
    STDMETHOD(CreateWrap)
    (	THIS_ D3DRMWRAPTYPE, LPDIRECT3DRMFRAME,
	D3DVALUE ox, D3DVALUE oy, D3DVALUE oz,
	D3DVALUE dx, D3DVALUE dy, D3DVALUE dz,
	D3DVALUE ux, D3DVALUE uy, D3DVALUE uz,
	D3DVALUE ou, D3DVALUE ov,
	D3DVALUE su, D3DVALUE sv,
	LPDIRECT3DRMWRAP *
    ) PURE;
    STDMETHOD(CreateUserVisual) (THIS_ D3DRMUSERVISUALCALLBACK, LPVOID lPArg, LPDIRECT3DRMUSERVISUAL *) PURE;
    STDMETHOD(LoadTexture)	(THIS_ const char *, LPDIRECT3DRMTEXTURE *) PURE;
    STDMETHOD(LoadTextureFromResource)	(THIS_ HRSRC rs, LPDIRECT3DRMTEXTURE *) PURE;
   
    STDMETHOD(SetSearchPath)	(THIS_ LPCSTR) PURE;
    STDMETHOD(AddSearchPath)	(THIS_ LPCSTR) PURE;
    STDMETHOD(GetSearchPath)	(THIS_ DWORD *size_return, LPSTR path_return) PURE;
    STDMETHOD(SetDefaultTextureColors)(THIS_ DWORD) PURE;
    STDMETHOD(SetDefaultTextureShades)(THIS_ DWORD) PURE;
   
    STDMETHOD(GetDevices)	(THIS_ LPDIRECT3DRMDEVICEARRAY *) PURE;
    STDMETHOD(GetNamedObject)	(THIS_ const char *, LPDIRECT3DRMOBJECT *) PURE;
   
    STDMETHOD(EnumerateObjects)	(THIS_ D3DRMOBJECTCALLBACK, LPVOID) PURE;
   
    STDMETHOD(Load)		
    (   THIS_ LPVOID, LPVOID, LPIID *, DWORD, D3DRMLOADOPTIONS,
    	D3DRMLOADCALLBACK, LPVOID, D3DRMLOADTEXTURECALLBACK, LPVOID,
	LPDIRECT3DRMFRAME
    ) PURE;
    STDMETHOD(Tick)		(THIS_ D3DVALUE) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRM2

DECLARE_INTERFACE_(IDirect3DRM2, IUnknown)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD(CreateObject)
	(THIS_ REFCLSID rclsid, LPUNKNOWN pUnkOuter, REFIID riid, LPVOID FAR* ppv) PURE;
    STDMETHOD(CreateFrame)	(THIS_ LPDIRECT3DRMFRAME, LPDIRECT3DRMFRAME2 *) PURE;
    STDMETHOD(CreateMesh)	(THIS_ LPDIRECT3DRMMESH *) PURE;
    STDMETHOD(CreateMeshBuilder)(THIS_ LPDIRECT3DRMMESHBUILDER2 *) PURE;
    STDMETHOD(CreateFace)	(THIS_ LPDIRECT3DRMFACE *) PURE;
    STDMETHOD(CreateAnimation)	(THIS_ LPDIRECT3DRMANIMATION *) PURE;
    STDMETHOD(CreateAnimationSet)(THIS_ LPDIRECT3DRMANIMATIONSET *) PURE;
    STDMETHOD(CreateTexture)	(THIS_ LPD3DRMIMAGE, LPDIRECT3DRMTEXTURE2 *) PURE;
    STDMETHOD(CreateLight)	(THIS_ D3DRMLIGHTTYPE, D3DCOLOR, LPDIRECT3DRMLIGHT *) PURE;
    STDMETHOD(CreateLightRGB)
	(THIS_ D3DRMLIGHTTYPE, D3DVALUE, D3DVALUE, D3DVALUE, LPDIRECT3DRMLIGHT *) PURE;
    STDMETHOD(CreateMaterial)	(THIS_ D3DVALUE, LPDIRECT3DRMMATERIAL *) PURE;
    STDMETHOD(CreateDevice)	(THIS_ DWORD, DWORD, LPDIRECT3DRMDEVICE2 *) PURE;

    /* Create a Windows Device using DirectDraw surfaces */
    STDMETHOD(CreateDeviceFromSurface)
    (	THIS_ LPGUID lpGUID, LPDIRECTDRAW lpDD,
	LPDIRECTDRAWSURFACE lpDDSBack, LPDIRECT3DRMDEVICE2 *
    ) PURE;

    /* Create a Windows Device using D3D objects */
    STDMETHOD(CreateDeviceFromD3D)
    (	THIS_ LPDIRECT3D2 lpD3D, LPDIRECT3DDEVICE2 lpD3DDev,
	LPDIRECT3DRMDEVICE2 *
    ) PURE;

    STDMETHOD(CreateDeviceFromClipper)
    (	THIS_ LPDIRECTDRAWCLIPPER lpDDClipper, LPGUID lpGUID,
	int width, int height, LPDIRECT3DRMDEVICE2 *) PURE;

    STDMETHOD(CreateTextureFromSurface)(THIS_ LPDIRECTDRAWSURFACE lpDDS, LPDIRECT3DRMTEXTURE2 *) PURE;
   
    STDMETHOD(CreateShadow)
    (	THIS_ LPDIRECT3DRMVISUAL, LPDIRECT3DRMLIGHT,
	D3DVALUE px, D3DVALUE py, D3DVALUE pz,
	D3DVALUE nx, D3DVALUE ny, D3DVALUE nz,
	LPDIRECT3DRMVISUAL *
    ) PURE;
    STDMETHOD(CreateViewport)
    (	THIS_ LPDIRECT3DRMDEVICE, LPDIRECT3DRMFRAME, DWORD, DWORD,
	DWORD, DWORD, LPDIRECT3DRMVIEWPORT *
    ) PURE;
    STDMETHOD(CreateWrap)
    (	THIS_ D3DRMWRAPTYPE, LPDIRECT3DRMFRAME,
	D3DVALUE ox, D3DVALUE oy, D3DVALUE oz,
	D3DVALUE dx, D3DVALUE dy, D3DVALUE dz,
	D3DVALUE ux, D3DVALUE uy, D3DVALUE uz,
	D3DVALUE ou, D3DVALUE ov,
	D3DVALUE su, D3DVALUE sv,
	LPDIRECT3DRMWRAP *
    ) PURE;
    STDMETHOD(CreateUserVisual) (THIS_ D3DRMUSERVISUALCALLBACK, LPVOID lPArg, LPDIRECT3DRMUSERVISUAL *) PURE;
    STDMETHOD(LoadTexture)	(THIS_ const char *, LPDIRECT3DRMTEXTURE2 *) PURE;
    STDMETHOD(LoadTextureFromResource)	(THIS_ HMODULE hModule, LPCTSTR strName, LPCTSTR strType, LPDIRECT3DRMTEXTURE2 *) PURE;
   
    STDMETHOD(SetSearchPath)	(THIS_ LPCSTR) PURE;
    STDMETHOD(AddSearchPath)	(THIS_ LPCSTR) PURE;
    STDMETHOD(GetSearchPath)	(THIS_ DWORD *size_return, LPSTR path_return) PURE;
    STDMETHOD(SetDefaultTextureColors)(THIS_ DWORD) PURE;
    STDMETHOD(SetDefaultTextureShades)(THIS_ DWORD) PURE;
   
    STDMETHOD(GetDevices)	(THIS_ LPDIRECT3DRMDEVICEARRAY *) PURE;
    STDMETHOD(GetNamedObject)	(THIS_ const char *, LPDIRECT3DRMOBJECT *) PURE;
   
    STDMETHOD(EnumerateObjects)	(THIS_ D3DRMOBJECTCALLBACK, LPVOID) PURE;
   
    STDMETHOD(Load)		
    (   THIS_ LPVOID, LPVOID, LPIID *, DWORD, D3DRMLOADOPTIONS,
    	D3DRMLOADCALLBACK, LPVOID, D3DRMLOADTEXTURECALLBACK, LPVOID,
	LPDIRECT3DRMFRAME
    ) PURE;
    STDMETHOD(Tick)		(THIS_ D3DVALUE) PURE;

    STDMETHOD(CreateProgressiveMesh)(THIS_ LPDIRECT3DRMPROGRESSIVEMESH *) PURE;
};

#undef INTERFACE
#define INTERFACE IDirect3DRM3

DECLARE_INTERFACE_(IDirect3DRM3, IUnknown)
{
    IUNKNOWN_METHODS(PURE);

    STDMETHOD(CreateObject)
	(THIS_ REFCLSID rclsid, LPUNKNOWN pUnkOuter, REFIID riid, LPVOID FAR* ppv) PURE;
    STDMETHOD(CreateFrame)	(THIS_ LPDIRECT3DRMFRAME3, LPDIRECT3DRMFRAME3 *) PURE;
    STDMETHOD(CreateMesh)	(THIS_ LPDIRECT3DRMMESH *) PURE;
    STDMETHOD(CreateMeshBuilder)(THIS_ LPDIRECT3DRMMESHBUILDER3 *) PURE;
    STDMETHOD(CreateFace)	(THIS_ LPDIRECT3DRMFACE2 *) PURE;
    STDMETHOD(CreateAnimation)	(THIS_ LPDIRECT3DRMANIMATION2 *) PURE;
    STDMETHOD(CreateAnimationSet)(THIS_ LPDIRECT3DRMANIMATIONSET2 *) PURE;
    STDMETHOD(CreateTexture)	(THIS_ LPD3DRMIMAGE, LPDIRECT3DRMTEXTURE3 *) PURE;
    STDMETHOD(CreateLight)	(THIS_ D3DRMLIGHTTYPE, D3DCOLOR, LPDIRECT3DRMLIGHT *) PURE;
    STDMETHOD(CreateLightRGB)
	(THIS_ D3DRMLIGHTTYPE, D3DVALUE, D3DVALUE, D3DVALUE, LPDIRECT3DRMLIGHT *) PURE;
    STDMETHOD(CreateMaterial)	(THIS_ D3DVALUE, LPDIRECT3DRMMATERIAL2 *) PURE;
    STDMETHOD(CreateDevice)	(THIS_ DWORD, DWORD, LPDIRECT3DRMDEVICE3 *) PURE;

    /* Create a Windows Device using DirectDraw surfaces */
    STDMETHOD(CreateDeviceFromSurface)
    (	THIS_ LPGUID lpGUID, LPDIRECTDRAW lpDD,
	LPDIRECTDRAWSURFACE lpDDSBack, DWORD dwFlags, LPDIRECT3DRMDEVICE3 *
    ) PURE;

    /* Create a Windows Device using D3D objects */
    STDMETHOD(CreateDeviceFromD3D)
    (	THIS_ LPDIRECT3D2 lpD3D, LPDIRECT3DDEVICE2 lpD3DDev,
	LPDIRECT3DRMDEVICE3 *
    ) PURE;

    STDMETHOD(CreateDeviceFromClipper)
    (	THIS_ LPDIRECTDRAWCLIPPER lpDDClipper, LPGUID lpGUID,
	int width, int height, LPDIRECT3DRMDEVICE3 *) PURE;

    STDMETHOD(CreateTextureFromSurface)(THIS_ LPDIRECTDRAWSURFACE lpDDS, LPDIRECT3DRMTEXTURE3 *) PURE;
   
    STDMETHOD(CreateShadow)
    (	THIS_ LPUNKNOWN, LPDIRECT3DRMLIGHT,
	D3DVALUE px, D3DVALUE py, D3DVALUE pz,
	D3DVALUE nx, D3DVALUE ny, D3DVALUE nz,
	LPDIRECT3DRMSHADOW2 *
    ) PURE;
    STDMETHOD(CreateViewport)
    (	THIS_ LPDIRECT3DRMDEVICE3, LPDIRECT3DRMFRAME3, DWORD, DWORD,
	DWORD, DWORD, LPDIRECT3DRMVIEWPORT2 *
    ) PURE;
    STDMETHOD(CreateWrap)
    (	THIS_ D3DRMWRAPTYPE, LPDIRECT3DRMFRAME3,
	D3DVALUE ox, D3DVALUE oy, D3DVALUE oz,
	D3DVALUE dx, D3DVALUE dy, D3DVALUE dz,
	D3DVALUE ux, D3DVALUE uy, D3DVALUE uz,
	D3DVALUE ou, D3DVALUE ov,
	D3DVALUE su, D3DVALUE sv,
	LPDIRECT3DRMWRAP *
    ) PURE;
    STDMETHOD(CreateUserVisual) (THIS_ D3DRMUSERVISUALCALLBACK, LPVOID lPArg, LPDIRECT3DRMUSERVISUAL *) PURE;
    STDMETHOD(LoadTexture)	(THIS_ const char *, LPDIRECT3DRMTEXTURE3 *) PURE;
    STDMETHOD(LoadTextureFromResource)	(THIS_ HMODULE hModule, LPCTSTR strName, LPCTSTR strType, LPDIRECT3DRMTEXTURE3 *) PURE;
   
    STDMETHOD(SetSearchPath)	(THIS_ LPCSTR) PURE;
    STDMETHOD(AddSearchPath)	(THIS_ LPCSTR) PURE;
    STDMETHOD(GetSearchPath)	(THIS_ DWORD *size_return, LPSTR path_return) PURE;
    STDMETHOD(SetDefaultTextureColors)(THIS_ DWORD) PURE;
    STDMETHOD(SetDefaultTextureShades)(THIS_ DWORD) PURE;
   
    STDMETHOD(GetDevices)	(THIS_ LPDIRECT3DRMDEVICEARRAY *) PURE;
    STDMETHOD(GetNamedObject)	(THIS_ const char *, LPDIRECT3DRMOBJECT *) PURE;
   
    STDMETHOD(EnumerateObjects)	(THIS_ D3DRMOBJECTCALLBACK, LPVOID) PURE;
   
    STDMETHOD(Load)		
    (   THIS_ LPVOID, LPVOID, LPIID *, DWORD, D3DRMLOADOPTIONS,
    	D3DRMLOADCALLBACK, LPVOID, D3DRMLOADTEXTURE3CALLBACK, LPVOID,
	LPDIRECT3DRMFRAME3
    ) PURE;
    STDMETHOD(Tick)		(THIS_ D3DVALUE) PURE;

    STDMETHOD(CreateProgressiveMesh)(THIS_ LPDIRECT3DRMPROGRESSIVEMESH *) PURE;

    /* Used with IDirect3DRMObject2 */
    STDMETHOD(RegisterClient)   (THIS_ REFGUID rguid, LPDWORD lpdwID) PURE;
    STDMETHOD(UnregisterClient) (THIS_ REFGUID rguid) PURE;

    STDMETHOD(CreateClippedVisual) (THIS_ LPDIRECT3DRMVISUAL, LPDIRECT3DRMCLIPPEDVISUAL *) PURE;
    STDMETHOD(SetOptions) (THIS_ DWORD);
    STDMETHOD(GetOptions) (THIS_ LPDWORD);
};

#define D3DRM_OK			DD_OK
#define D3DRMERR_BADOBJECT		MAKE_DDHRESULT(781)
#define D3DRMERR_BADTYPE		MAKE_DDHRESULT(782)
#define D3DRMERR_BADALLOC		MAKE_DDHRESULT(783)
#define D3DRMERR_FACEUSED		MAKE_DDHRESULT(784)
#define D3DRMERR_NOTFOUND		MAKE_DDHRESULT(785)
#define D3DRMERR_NOTDONEYET		MAKE_DDHRESULT(786)
#define D3DRMERR_FILENOTFOUND		MAKE_DDHRESULT(787)
#define D3DRMERR_BADFILE		MAKE_DDHRESULT(788)
#define D3DRMERR_BADDEVICE		MAKE_DDHRESULT(789)
#define D3DRMERR_BADVALUE		MAKE_DDHRESULT(790)
#define D3DRMERR_BADMAJORVERSION	MAKE_DDHRESULT(791)
#define D3DRMERR_BADMINORVERSION	MAKE_DDHRESULT(792)
#define D3DRMERR_UNABLETOEXECUTE	MAKE_DDHRESULT(793)
#define D3DRMERR_LIBRARYNOTFOUND        MAKE_DDHRESULT(794)
#define D3DRMERR_INVALIDLIBRARY         MAKE_DDHRESULT(795)
#define D3DRMERR_PENDING                MAKE_DDHRESULT(796)
#define D3DRMERR_NOTENOUGHDATA          MAKE_DDHRESULT(797)
#define D3DRMERR_REQUESTTOOLARGE        MAKE_DDHRESULT(798)
#define D3DRMERR_REQUESTTOOSMALL        MAKE_DDHRESULT(799)
#define D3DRMERR_CONNECTIONLOST         MAKE_DDHRESULT(800)
#define D3DRMERR_LOADABORTED            MAKE_DDHRESULT(801)
#define D3DRMERR_NOINTERNET             MAKE_DDHRESULT(802)
#define D3DRMERR_BADCACHEFILE           MAKE_DDHRESULT(803)
#define D3DRMERR_BOXNOTSET		MAKE_DDHRESULT(804)
#define D3DRMERR_BADPMDATA              MAKE_DDHRESULT(805)
#define D3DRMERR_CLIENTNOTREGISTERED    MAKE_DDHRESULT(806)
#define D3DRMERR_NOTCREATEDFROMDDS	MAKE_DDHRESULT(807)
#define D3DRMERR_NOSUCHKEY              MAKE_DDHRESULT(808)
#define D3DRMERR_INCOMPATABLEKEY        MAKE_DDHRESULT(809)
#define D3DRMERR_ELEMENTINUSE		MAKE_DDHRESULT(810)
#define D3DRMERR_TEXTUREFORMATNOTFOUND  MAKE_DDHRESULT(811)
#define D3DRMERR_NOTAGGREGATED          MAKE_DDHRESULT(812)

#ifdef __cplusplus
};
#endif

#endif /* _D3DRMAPI_H_ */

#include <urlmon.h>
#if 0
// Bogus definition used to make MIDL compiler happy
typedef void DDSURFACEDESC;

typedef void D3DRMBOX;

typedef void D3DVECTOR;

typedef void D3DRMMATRIX4D;

typedef void *LPSECURITY_ATTRIBUTES;

#endif
#ifdef _DXTRANSIMPL
    #define _DXTRANS_IMPL_EXT _declspec(dllexport)
#else
    #define _DXTRANS_IMPL_EXT _declspec(dllimport)
#endif
















//
//   All GUIDs for DXTransform are declared in DXTGUID.C in the SDK include directory
//
EXTERN_C const GUID DDPF_RGB1;
EXTERN_C const GUID DDPF_RGB2;
EXTERN_C const GUID DDPF_RGB4;
EXTERN_C const GUID DDPF_RGB8;
EXTERN_C const GUID DDPF_RGB332;
EXTERN_C const GUID DDPF_ARGB4444;
EXTERN_C const GUID DDPF_RGB565;
EXTERN_C const GUID DDPF_BGR565;
EXTERN_C const GUID DDPF_RGB555;
EXTERN_C const GUID DDPF_ARGB1555;
EXTERN_C const GUID DDPF_RGB24;
EXTERN_C const GUID DDPF_BGR24;
EXTERN_C const GUID DDPF_RGB32;
EXTERN_C const GUID DDPF_BGR32;
EXTERN_C const GUID DDPF_ABGR32;
EXTERN_C const GUID DDPF_ARGB32;
EXTERN_C const GUID DDPF_PMARGB32;
EXTERN_C const GUID DDPF_A1;
EXTERN_C const GUID DDPF_A2;
EXTERN_C const GUID DDPF_A4;
EXTERN_C const GUID DDPF_A8;
EXTERN_C const GUID DDPF_Z8;
EXTERN_C const GUID DDPF_Z16;
EXTERN_C const GUID DDPF_Z24;
EXTERN_C const GUID DDPF_Z32;
//
//   Component categories
//
EXTERN_C const GUID CATID_DXImageTransform;
EXTERN_C const GUID CATID_DX3DTransform;
EXTERN_C const GUID CATID_DXAuthoringTransform;
EXTERN_C const GUID CATID_DXSurface;
//
//   Service IDs
//
EXTERN_C const GUID SID_SDirectDraw;
EXTERN_C const GUID SID_SDirect3DRM;
#define SID_SDXTaskManager CLSID_DXTaskManager
#define SID_SDXSurfaceFactory IID_IDXSurfaceFactory
#define SID_SDXTransformFactory IID_IDXTransformFactory


extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0000_v0_0_s_ifspec;

#ifndef __IDXBaseObject_INTERFACE_DEFINED__
#define __IDXBaseObject_INTERFACE_DEFINED__

/* interface IDXBaseObject */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXBaseObject;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("17B59B2B-9CC8-11d1-9053-00C04FD9189D")
    IDXBaseObject : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetGenerationId( 
            /* [out] */ ULONG *pID) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IncrementGenerationId( 
            /* [in] */ BOOL bRefresh) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetObjectSize( 
            /* [out] */ ULONG *pcbSize) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXBaseObjectVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXBaseObject * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXBaseObject * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXBaseObject * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetGenerationId )( 
            IDXBaseObject * This,
            /* [out] */ ULONG *pID);
        
        HRESULT ( STDMETHODCALLTYPE *IncrementGenerationId )( 
            IDXBaseObject * This,
            /* [in] */ BOOL bRefresh);
        
        HRESULT ( STDMETHODCALLTYPE *GetObjectSize )( 
            IDXBaseObject * This,
            /* [out] */ ULONG *pcbSize);
        
        END_INTERFACE
    } IDXBaseObjectVtbl;

    interface IDXBaseObject
    {
        CONST_VTBL struct IDXBaseObjectVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXBaseObject_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXBaseObject_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXBaseObject_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXBaseObject_GetGenerationId(This,pID)	\
    (This)->lpVtbl -> GetGenerationId(This,pID)

#define IDXBaseObject_IncrementGenerationId(This,bRefresh)	\
    (This)->lpVtbl -> IncrementGenerationId(This,bRefresh)

#define IDXBaseObject_GetObjectSize(This,pcbSize)	\
    (This)->lpVtbl -> GetObjectSize(This,pcbSize)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXBaseObject_GetGenerationId_Proxy( 
    IDXBaseObject * This,
    /* [out] */ ULONG *pID);


void __RPC_STUB IDXBaseObject_GetGenerationId_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXBaseObject_IncrementGenerationId_Proxy( 
    IDXBaseObject * This,
    /* [in] */ BOOL bRefresh);


void __RPC_STUB IDXBaseObject_IncrementGenerationId_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXBaseObject_GetObjectSize_Proxy( 
    IDXBaseObject * This,
    /* [out] */ ULONG *pcbSize);


void __RPC_STUB IDXBaseObject_GetObjectSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXBaseObject_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0253 */
/* [local] */ 

typedef 
enum DXBNDID
    {	DXB_X	= 0,
	DXB_Y	= 1,
	DXB_Z	= 2,
	DXB_T	= 3
    } 	DXBNDID;

typedef 
enum DXBNDTYPE
    {	DXBT_DISCRETE	= 0,
	DXBT_DISCRETE64	= DXBT_DISCRETE + 1,
	DXBT_CONTINUOUS	= DXBT_DISCRETE64 + 1,
	DXBT_CONTINUOUS64	= DXBT_CONTINUOUS + 1
    } 	DXBNDTYPE;

typedef struct DXDBND
    {
    long Min;
    long Max;
    } 	DXDBND;

typedef DXDBND DXDBNDS[ 4 ];

typedef struct DXDBND64
    {
    LONGLONG Min;
    LONGLONG Max;
    } 	DXDBND64;

typedef DXDBND64 DXDBNDS64[ 4 ];

typedef struct DXCBND
    {
    float Min;
    float Max;
    } 	DXCBND;

typedef DXCBND DXCBNDS[ 4 ];

typedef struct DXCBND64
    {
    double Min;
    double Max;
    } 	DXCBND64;

typedef DXCBND64 DXCBNDS64[ 4 ];

typedef struct DXBNDS
    {
    DXBNDTYPE eType;
    /* [switch_is] */ /* [switch_type] */ union __MIDL___MIDL_itf_dxtrans_0253_0001
        {
        /* [case()] */ DXDBND D[ 4 ];
        /* [case()] */ DXDBND64 LD[ 4 ];
        /* [case()] */ DXCBND C[ 4 ];
        /* [case()] */ DXCBND64 LC[ 4 ];
        } 	u;
    } 	DXBNDS;

typedef long DXDVEC[ 4 ];

typedef LONGLONG DXDVEC64[ 4 ];

typedef float DXCVEC[ 4 ];

typedef double DXCVEC64[ 4 ];

typedef struct DXVEC
    {
    DXBNDTYPE eType;
    /* [switch_is] */ /* [switch_type] */ union __MIDL___MIDL_itf_dxtrans_0253_0002
        {
        /* [case()] */ long D[ 4 ];
        /* [case()] */ LONGLONG LD[ 4 ];
        /* [case()] */ float C[ 4 ];
        /* [case()] */ double LC[ 4 ];
        } 	u;
    } 	DXVEC;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0253_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0253_v0_0_s_ifspec;

#ifndef __IDXTransformFactory_INTERFACE_DEFINED__
#define __IDXTransformFactory_INTERFACE_DEFINED__

/* interface IDXTransformFactory */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXTransformFactory;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6A950B2B-A971-11d1-81C8-0000F87557DB")
    IDXTransformFactory : public IServiceProvider
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetService( 
            /* [in] */ REFGUID guidService,
            /* [in] */ IUnknown *pUnkService,
            /* [in] */ BOOL bWeakReference) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE CreateTransform( 
            /* [size_is][in] */ IUnknown **punkInputs,
            /* [in] */ ULONG ulNumInputs,
            /* [size_is][in] */ IUnknown **punkOutputs,
            /* [in] */ ULONG ulNumOutputs,
            /* [in] */ IPropertyBag *pInitProps,
            /* [in] */ IErrorLog *pErrLog,
            /* [in] */ REFCLSID TransCLSID,
            /* [in] */ REFIID TransIID,
            /* [iid_is][out] */ void **ppTransform) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE InitializeTransform( 
            /* [in] */ IDXTransform *pTransform,
            /* [size_is][in] */ IUnknown **punkInputs,
            /* [in] */ ULONG ulNumInputs,
            /* [size_is][in] */ IUnknown **punkOutputs,
            /* [in] */ ULONG ulNumOutputs,
            /* [in] */ IPropertyBag *pInitProps,
            /* [in] */ IErrorLog *pErrLog) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXTransformFactoryVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXTransformFactory * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXTransformFactory * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXTransformFactory * This);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *QueryService )( 
            IDXTransformFactory * This,
            /* [in] */ REFGUID guidService,
            /* [in] */ REFIID riid,
            /* [out] */ void **ppvObject);
        
        HRESULT ( STDMETHODCALLTYPE *SetService )( 
            IDXTransformFactory * This,
            /* [in] */ REFGUID guidService,
            /* [in] */ IUnknown *pUnkService,
            /* [in] */ BOOL bWeakReference);
        
        HRESULT ( STDMETHODCALLTYPE *CreateTransform )( 
            IDXTransformFactory * This,
            /* [size_is][in] */ IUnknown **punkInputs,
            /* [in] */ ULONG ulNumInputs,
            /* [size_is][in] */ IUnknown **punkOutputs,
            /* [in] */ ULONG ulNumOutputs,
            /* [in] */ IPropertyBag *pInitProps,
            /* [in] */ IErrorLog *pErrLog,
            /* [in] */ REFCLSID TransCLSID,
            /* [in] */ REFIID TransIID,
            /* [iid_is][out] */ void **ppTransform);
        
        HRESULT ( STDMETHODCALLTYPE *InitializeTransform )( 
            IDXTransformFactory * This,
            /* [in] */ IDXTransform *pTransform,
            /* [size_is][in] */ IUnknown **punkInputs,
            /* [in] */ ULONG ulNumInputs,
            /* [size_is][in] */ IUnknown **punkOutputs,
            /* [in] */ ULONG ulNumOutputs,
            /* [in] */ IPropertyBag *pInitProps,
            /* [in] */ IErrorLog *pErrLog);
        
        END_INTERFACE
    } IDXTransformFactoryVtbl;

    interface IDXTransformFactory
    {
        CONST_VTBL struct IDXTransformFactoryVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXTransformFactory_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXTransformFactory_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXTransformFactory_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXTransformFactory_QueryService(This,guidService,riid,ppvObject)	\
    (This)->lpVtbl -> QueryService(This,guidService,riid,ppvObject)


#define IDXTransformFactory_SetService(This,guidService,pUnkService,bWeakReference)	\
    (This)->lpVtbl -> SetService(This,guidService,pUnkService,bWeakReference)

#define IDXTransformFactory_CreateTransform(This,punkInputs,ulNumInputs,punkOutputs,ulNumOutputs,pInitProps,pErrLog,TransCLSID,TransIID,ppTransform)	\
    (This)->lpVtbl -> CreateTransform(This,punkInputs,ulNumInputs,punkOutputs,ulNumOutputs,pInitProps,pErrLog,TransCLSID,TransIID,ppTransform)

#define IDXTransformFactory_InitializeTransform(This,pTransform,punkInputs,ulNumInputs,punkOutputs,ulNumOutputs,pInitProps,pErrLog)	\
    (This)->lpVtbl -> InitializeTransform(This,pTransform,punkInputs,ulNumInputs,punkOutputs,ulNumOutputs,pInitProps,pErrLog)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXTransformFactory_SetService_Proxy( 
    IDXTransformFactory * This,
    /* [in] */ REFGUID guidService,
    /* [in] */ IUnknown *pUnkService,
    /* [in] */ BOOL bWeakReference);


void __RPC_STUB IDXTransformFactory_SetService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransformFactory_CreateTransform_Proxy( 
    IDXTransformFactory * This,
    /* [size_is][in] */ IUnknown **punkInputs,
    /* [in] */ ULONG ulNumInputs,
    /* [size_is][in] */ IUnknown **punkOutputs,
    /* [in] */ ULONG ulNumOutputs,
    /* [in] */ IPropertyBag *pInitProps,
    /* [in] */ IErrorLog *pErrLog,
    /* [in] */ REFCLSID TransCLSID,
    /* [in] */ REFIID TransIID,
    /* [iid_is][out] */ void **ppTransform);


void __RPC_STUB IDXTransformFactory_CreateTransform_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransformFactory_InitializeTransform_Proxy( 
    IDXTransformFactory * This,
    /* [in] */ IDXTransform *pTransform,
    /* [size_is][in] */ IUnknown **punkInputs,
    /* [in] */ ULONG ulNumInputs,
    /* [size_is][in] */ IUnknown **punkOutputs,
    /* [in] */ ULONG ulNumOutputs,
    /* [in] */ IPropertyBag *pInitProps,
    /* [in] */ IErrorLog *pErrLog);


void __RPC_STUB IDXTransformFactory_InitializeTransform_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXTransformFactory_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0254 */
/* [local] */ 

typedef 
enum DXTMISCFLAGS
    {	DXTMF_BLEND_WITH_OUTPUT	= 1L << 0,
	DXTMF_DITHER_OUTPUT	= 1L << 1,
	DXTMF_OPTION_MASK	= 0xffff,
	DXTMF_VALID_OPTIONS	= DXTMF_BLEND_WITH_OUTPUT | DXTMF_DITHER_OUTPUT,
	DXTMF_BLEND_SUPPORTED	= 1L << 16,
	DXTMF_DITHER_SUPPORTED	= 1L << 17,
	DXTMF_INPLACE_OPERATION	= 1L << 24,
	DXTMF_BOUNDS_SUPPORTED	= 1L << 25,
	DXTMF_PLACEMENT_SUPPORTED	= 1L << 26,
	DXTMF_QUALITY_SUPPORTED	= 1L << 27,
	DXTMF_OPAQUE_RESULT	= 1L << 28
    } 	DXTMISCFLAGS;

typedef 
enum DXINOUTINFOFLAGS
    {	DXINOUTF_OPTIONAL	= 1L << 0
    } 	DXINOUTINFOFLAGS;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0254_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0254_v0_0_s_ifspec;

#ifndef __IDXTransform_INTERFACE_DEFINED__
#define __IDXTransform_INTERFACE_DEFINED__

/* interface IDXTransform */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXTransform;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("30A5FB78-E11F-11d1-9064-00C04FD9189D")
    IDXTransform : public IDXBaseObject
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Setup( 
            /* [size_is][in] */ IUnknown *const *punkInputs,
            /* [in] */ ULONG ulNumInputs,
            /* [size_is][in] */ IUnknown *const *punkOutputs,
            /* [in] */ ULONG ulNumOutputs,
            /* [in] */ DWORD dwFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Execute( 
            /* [in] */ const GUID *pRequestID,
            /* [in] */ const DXBNDS *pClipBnds,
            /* [in] */ const DXVEC *pPlacement) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE MapBoundsIn2Out( 
            /* [in] */ const DXBNDS *pInBounds,
            /* [in] */ ULONG ulNumInBnds,
            /* [in] */ ULONG ulOutIndex,
            /* [out] */ DXBNDS *pOutBounds) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE MapBoundsOut2In( 
            /* [in] */ ULONG ulOutIndex,
            /* [in] */ const DXBNDS *pOutBounds,
            /* [in] */ ULONG ulInIndex,
            /* [out] */ DXBNDS *pInBounds) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetMiscFlags( 
            /* [in] */ DWORD dwMiscFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetMiscFlags( 
            /* [out] */ DWORD *pdwMiscFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetInOutInfo( 
            /* [in] */ BOOL bIsOutput,
            /* [in] */ ULONG ulIndex,
            /* [out] */ DWORD *pdwFlags,
            /* [size_is][out] */ GUID *pIDs,
            /* [out][in] */ ULONG *pcIDs,
            /* [out] */ IUnknown **ppUnkCurrentObject) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetQuality( 
            /* [in] */ float fQuality) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetQuality( 
            /* [out] */ float *fQuality) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXTransformVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXTransform * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXTransform * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXTransform * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetGenerationId )( 
            IDXTransform * This,
            /* [out] */ ULONG *pID);
        
        HRESULT ( STDMETHODCALLTYPE *IncrementGenerationId )( 
            IDXTransform * This,
            /* [in] */ BOOL bRefresh);
        
        HRESULT ( STDMETHODCALLTYPE *GetObjectSize )( 
            IDXTransform * This,
            /* [out] */ ULONG *pcbSize);
        
        HRESULT ( STDMETHODCALLTYPE *Setup )( 
            IDXTransform * This,
            /* [size_is][in] */ IUnknown *const *punkInputs,
            /* [in] */ ULONG ulNumInputs,
            /* [size_is][in] */ IUnknown *const *punkOutputs,
            /* [in] */ ULONG ulNumOutputs,
            /* [in] */ DWORD dwFlags);
        
        HRESULT ( STDMETHODCALLTYPE *Execute )( 
            IDXTransform * This,
            /* [in] */ const GUID *pRequestID,
            /* [in] */ const DXBNDS *pClipBnds,
            /* [in] */ const DXVEC *pPlacement);
        
        HRESULT ( STDMETHODCALLTYPE *MapBoundsIn2Out )( 
            IDXTransform * This,
            /* [in] */ const DXBNDS *pInBounds,
            /* [in] */ ULONG ulNumInBnds,
            /* [in] */ ULONG ulOutIndex,
            /* [out] */ DXBNDS *pOutBounds);
        
        HRESULT ( STDMETHODCALLTYPE *MapBoundsOut2In )( 
            IDXTransform * This,
            /* [in] */ ULONG ulOutIndex,
            /* [in] */ const DXBNDS *pOutBounds,
            /* [in] */ ULONG ulInIndex,
            /* [out] */ DXBNDS *pInBounds);
        
        HRESULT ( STDMETHODCALLTYPE *SetMiscFlags )( 
            IDXTransform * This,
            /* [in] */ DWORD dwMiscFlags);
        
        HRESULT ( STDMETHODCALLTYPE *GetMiscFlags )( 
            IDXTransform * This,
            /* [out] */ DWORD *pdwMiscFlags);
        
        HRESULT ( STDMETHODCALLTYPE *GetInOutInfo )( 
            IDXTransform * This,
            /* [in] */ BOOL bIsOutput,
            /* [in] */ ULONG ulIndex,
            /* [out] */ DWORD *pdwFlags,
            /* [size_is][out] */ GUID *pIDs,
            /* [out][in] */ ULONG *pcIDs,
            /* [out] */ IUnknown **ppUnkCurrentObject);
        
        HRESULT ( STDMETHODCALLTYPE *SetQuality )( 
            IDXTransform * This,
            /* [in] */ float fQuality);
        
        HRESULT ( STDMETHODCALLTYPE *GetQuality )( 
            IDXTransform * This,
            /* [out] */ float *fQuality);
        
        END_INTERFACE
    } IDXTransformVtbl;

    interface IDXTransform
    {
        CONST_VTBL struct IDXTransformVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXTransform_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXTransform_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXTransform_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXTransform_GetGenerationId(This,pID)	\
    (This)->lpVtbl -> GetGenerationId(This,pID)

#define IDXTransform_IncrementGenerationId(This,bRefresh)	\
    (This)->lpVtbl -> IncrementGenerationId(This,bRefresh)

#define IDXTransform_GetObjectSize(This,pcbSize)	\
    (This)->lpVtbl -> GetObjectSize(This,pcbSize)


#define IDXTransform_Setup(This,punkInputs,ulNumInputs,punkOutputs,ulNumOutputs,dwFlags)	\
    (This)->lpVtbl -> Setup(This,punkInputs,ulNumInputs,punkOutputs,ulNumOutputs,dwFlags)

#define IDXTransform_Execute(This,pRequestID,pClipBnds,pPlacement)	\
    (This)->lpVtbl -> Execute(This,pRequestID,pClipBnds,pPlacement)

#define IDXTransform_MapBoundsIn2Out(This,pInBounds,ulNumInBnds,ulOutIndex,pOutBounds)	\
    (This)->lpVtbl -> MapBoundsIn2Out(This,pInBounds,ulNumInBnds,ulOutIndex,pOutBounds)

#define IDXTransform_MapBoundsOut2In(This,ulOutIndex,pOutBounds,ulInIndex,pInBounds)	\
    (This)->lpVtbl -> MapBoundsOut2In(This,ulOutIndex,pOutBounds,ulInIndex,pInBounds)

#define IDXTransform_SetMiscFlags(This,dwMiscFlags)	\
    (This)->lpVtbl -> SetMiscFlags(This,dwMiscFlags)

#define IDXTransform_GetMiscFlags(This,pdwMiscFlags)	\
    (This)->lpVtbl -> GetMiscFlags(This,pdwMiscFlags)

#define IDXTransform_GetInOutInfo(This,bIsOutput,ulIndex,pdwFlags,pIDs,pcIDs,ppUnkCurrentObject)	\
    (This)->lpVtbl -> GetInOutInfo(This,bIsOutput,ulIndex,pdwFlags,pIDs,pcIDs,ppUnkCurrentObject)

#define IDXTransform_SetQuality(This,fQuality)	\
    (This)->lpVtbl -> SetQuality(This,fQuality)

#define IDXTransform_GetQuality(This,fQuality)	\
    (This)->lpVtbl -> GetQuality(This,fQuality)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXTransform_Setup_Proxy( 
    IDXTransform * This,
    /* [size_is][in] */ IUnknown *const *punkInputs,
    /* [in] */ ULONG ulNumInputs,
    /* [size_is][in] */ IUnknown *const *punkOutputs,
    /* [in] */ ULONG ulNumOutputs,
    /* [in] */ DWORD dwFlags);


void __RPC_STUB IDXTransform_Setup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_Execute_Proxy( 
    IDXTransform * This,
    /* [in] */ const GUID *pRequestID,
    /* [in] */ const DXBNDS *pClipBnds,
    /* [in] */ const DXVEC *pPlacement);


void __RPC_STUB IDXTransform_Execute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_MapBoundsIn2Out_Proxy( 
    IDXTransform * This,
    /* [in] */ const DXBNDS *pInBounds,
    /* [in] */ ULONG ulNumInBnds,
    /* [in] */ ULONG ulOutIndex,
    /* [out] */ DXBNDS *pOutBounds);


void __RPC_STUB IDXTransform_MapBoundsIn2Out_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_MapBoundsOut2In_Proxy( 
    IDXTransform * This,
    /* [in] */ ULONG ulOutIndex,
    /* [in] */ const DXBNDS *pOutBounds,
    /* [in] */ ULONG ulInIndex,
    /* [out] */ DXBNDS *pInBounds);


void __RPC_STUB IDXTransform_MapBoundsOut2In_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_SetMiscFlags_Proxy( 
    IDXTransform * This,
    /* [in] */ DWORD dwMiscFlags);


void __RPC_STUB IDXTransform_SetMiscFlags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_GetMiscFlags_Proxy( 
    IDXTransform * This,
    /* [out] */ DWORD *pdwMiscFlags);


void __RPC_STUB IDXTransform_GetMiscFlags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_GetInOutInfo_Proxy( 
    IDXTransform * This,
    /* [in] */ BOOL bIsOutput,
    /* [in] */ ULONG ulIndex,
    /* [out] */ DWORD *pdwFlags,
    /* [size_is][out] */ GUID *pIDs,
    /* [out][in] */ ULONG *pcIDs,
    /* [out] */ IUnknown **ppUnkCurrentObject);


void __RPC_STUB IDXTransform_GetInOutInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_SetQuality_Proxy( 
    IDXTransform * This,
    /* [in] */ float fQuality);


void __RPC_STUB IDXTransform_SetQuality_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTransform_GetQuality_Proxy( 
    IDXTransform * This,
    /* [out] */ float *fQuality);


void __RPC_STUB IDXTransform_GetQuality_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXTransform_INTERFACE_DEFINED__ */


#ifndef __IDXSurfacePick_INTERFACE_DEFINED__
#define __IDXSurfacePick_INTERFACE_DEFINED__

/* interface IDXSurfacePick */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXSurfacePick;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("30A5FB79-E11F-11d1-9064-00C04FD9189D")
    IDXSurfacePick : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE PointPick( 
            /* [in] */ const DXVEC *pPoint,
            /* [out] */ ULONG *pulInputSurfaceIndex,
            /* [out] */ DXVEC *pInputPoint) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXSurfacePickVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXSurfacePick * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXSurfacePick * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXSurfacePick * This);
        
        HRESULT ( STDMETHODCALLTYPE *PointPick )( 
            IDXSurfacePick * This,
            /* [in] */ const DXVEC *pPoint,
            /* [out] */ ULONG *pulInputSurfaceIndex,
            /* [out] */ DXVEC *pInputPoint);
        
        END_INTERFACE
    } IDXSurfacePickVtbl;

    interface IDXSurfacePick
    {
        CONST_VTBL struct IDXSurfacePickVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXSurfacePick_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXSurfacePick_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXSurfacePick_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXSurfacePick_PointPick(This,pPoint,pulInputSurfaceIndex,pInputPoint)	\
    (This)->lpVtbl -> PointPick(This,pPoint,pulInputSurfaceIndex,pInputPoint)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXSurfacePick_PointPick_Proxy( 
    IDXSurfacePick * This,
    /* [in] */ const DXVEC *pPoint,
    /* [out] */ ULONG *pulInputSurfaceIndex,
    /* [out] */ DXVEC *pInputPoint);


void __RPC_STUB IDXSurfacePick_PointPick_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXSurfacePick_INTERFACE_DEFINED__ */


#ifndef __IDXTBindHost_INTERFACE_DEFINED__
#define __IDXTBindHost_INTERFACE_DEFINED__

/* interface IDXTBindHost */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXTBindHost;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D26BCE55-E9DC-11d1-9066-00C04FD9189D")
    IDXTBindHost : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetBindHost( 
            /* [in] */ IBindHost *pBindHost) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXTBindHostVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXTBindHost * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXTBindHost * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXTBindHost * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetBindHost )( 
            IDXTBindHost * This,
            /* [in] */ IBindHost *pBindHost);
        
        END_INTERFACE
    } IDXTBindHostVtbl;

    interface IDXTBindHost
    {
        CONST_VTBL struct IDXTBindHostVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXTBindHost_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXTBindHost_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXTBindHost_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXTBindHost_SetBindHost(This,pBindHost)	\
    (This)->lpVtbl -> SetBindHost(This,pBindHost)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXTBindHost_SetBindHost_Proxy( 
    IDXTBindHost * This,
    /* [in] */ IBindHost *pBindHost);


void __RPC_STUB IDXTBindHost_SetBindHost_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXTBindHost_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0257 */
/* [local] */ 

typedef void __stdcall __stdcall DXTASKPROC( 
    void *pTaskData,
    BOOL *pbContinueProcessing);

typedef DXTASKPROC *PFNDXTASKPROC;

typedef void __stdcall __stdcall DXAPCPROC( 
    DWORD dwData);

typedef DXAPCPROC *PFNDXAPCPROC;

#ifdef __cplusplus
typedef struct DXTMTASKINFO
{
    PFNDXTASKPROC pfnTaskProc;       // Pointer to function to execute
    PVOID         pTaskData;         // Pointer to argument data
    PFNDXAPCPROC  pfnCompletionAPC;  // Pointer to completion APC proc
    DWORD         dwCompletionData;  // Pointer to APC proc data
    const GUID*   pRequestID;        // Used to identify groups of tasks
} DXTMTASKINFO;
#else
typedef struct DXTMTASKINFO
    {
    PVOID pfnTaskProc;
    PVOID pTaskData;
    PVOID pfnCompletionAPC;
    DWORD dwCompletionData;
    const GUID *pRequestID;
    } 	DXTMTASKINFO;

#endif


extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0257_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0257_v0_0_s_ifspec;

#ifndef __IDXTaskManager_INTERFACE_DEFINED__
#define __IDXTaskManager_INTERFACE_DEFINED__

/* interface IDXTaskManager */
/* [object][unique][helpstring][uuid][local] */ 


EXTERN_C const IID IID_IDXTaskManager;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("254DBBC1-F922-11d0-883A-3C8B00C10000")
    IDXTaskManager : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE QueryNumProcessors( 
            /* [out] */ ULONG *pulNumProc) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetThreadPoolSize( 
            /* [in] */ ULONG ulNumThreads) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetThreadPoolSize( 
            /* [out] */ ULONG *pulNumThreads) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetConcurrencyLimit( 
            /* [in] */ ULONG ulNumThreads) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetConcurrencyLimit( 
            /* [out] */ ULONG *pulNumThreads) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ScheduleTasks( 
            /* [in] */ DXTMTASKINFO TaskInfo[  ],
            /* [in] */ HANDLE Events[  ],
            /* [out] */ DWORD TaskIDs[  ],
            /* [in] */ ULONG ulNumTasks,
            /* [in] */ ULONG ulWaitPeriod) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE TerminateTasks( 
            /* [in] */ DWORD TaskIDs[  ],
            /* [in] */ ULONG ulCount,
            /* [in] */ ULONG ulTimeOut) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE TerminateRequest( 
            /* [in] */ REFIID RequestID,
            /* [in] */ ULONG ulTimeOut) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXTaskManagerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXTaskManager * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXTaskManager * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXTaskManager * This);
        
        HRESULT ( STDMETHODCALLTYPE *QueryNumProcessors )( 
            IDXTaskManager * This,
            /* [out] */ ULONG *pulNumProc);
        
        HRESULT ( STDMETHODCALLTYPE *SetThreadPoolSize )( 
            IDXTaskManager * This,
            /* [in] */ ULONG ulNumThreads);
        
        HRESULT ( STDMETHODCALLTYPE *GetThreadPoolSize )( 
            IDXTaskManager * This,
            /* [out] */ ULONG *pulNumThreads);
        
        HRESULT ( STDMETHODCALLTYPE *SetConcurrencyLimit )( 
            IDXTaskManager * This,
            /* [in] */ ULONG ulNumThreads);
        
        HRESULT ( STDMETHODCALLTYPE *GetConcurrencyLimit )( 
            IDXTaskManager * This,
            /* [out] */ ULONG *pulNumThreads);
        
        HRESULT ( STDMETHODCALLTYPE *ScheduleTasks )( 
            IDXTaskManager * This,
            /* [in] */ DXTMTASKINFO TaskInfo[  ],
            /* [in] */ HANDLE Events[  ],
            /* [out] */ DWORD TaskIDs[  ],
            /* [in] */ ULONG ulNumTasks,
            /* [in] */ ULONG ulWaitPeriod);
        
        HRESULT ( STDMETHODCALLTYPE *TerminateTasks )( 
            IDXTaskManager * This,
            /* [in] */ DWORD TaskIDs[  ],
            /* [in] */ ULONG ulCount,
            /* [in] */ ULONG ulTimeOut);
        
        HRESULT ( STDMETHODCALLTYPE *TerminateRequest )( 
            IDXTaskManager * This,
            /* [in] */ REFIID RequestID,
            /* [in] */ ULONG ulTimeOut);
        
        END_INTERFACE
    } IDXTaskManagerVtbl;

    interface IDXTaskManager
    {
        CONST_VTBL struct IDXTaskManagerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXTaskManager_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXTaskManager_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXTaskManager_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXTaskManager_QueryNumProcessors(This,pulNumProc)	\
    (This)->lpVtbl -> QueryNumProcessors(This,pulNumProc)

#define IDXTaskManager_SetThreadPoolSize(This,ulNumThreads)	\
    (This)->lpVtbl -> SetThreadPoolSize(This,ulNumThreads)

#define IDXTaskManager_GetThreadPoolSize(This,pulNumThreads)	\
    (This)->lpVtbl -> GetThreadPoolSize(This,pulNumThreads)

#define IDXTaskManager_SetConcurrencyLimit(This,ulNumThreads)	\
    (This)->lpVtbl -> SetConcurrencyLimit(This,ulNumThreads)

#define IDXTaskManager_GetConcurrencyLimit(This,pulNumThreads)	\
    (This)->lpVtbl -> GetConcurrencyLimit(This,pulNumThreads)

#define IDXTaskManager_ScheduleTasks(This,TaskInfo,Events,TaskIDs,ulNumTasks,ulWaitPeriod)	\
    (This)->lpVtbl -> ScheduleTasks(This,TaskInfo,Events,TaskIDs,ulNumTasks,ulWaitPeriod)

#define IDXTaskManager_TerminateTasks(This,TaskIDs,ulCount,ulTimeOut)	\
    (This)->lpVtbl -> TerminateTasks(This,TaskIDs,ulCount,ulTimeOut)

#define IDXTaskManager_TerminateRequest(This,RequestID,ulTimeOut)	\
    (This)->lpVtbl -> TerminateRequest(This,RequestID,ulTimeOut)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXTaskManager_QueryNumProcessors_Proxy( 
    IDXTaskManager * This,
    /* [out] */ ULONG *pulNumProc);


void __RPC_STUB IDXTaskManager_QueryNumProcessors_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_SetThreadPoolSize_Proxy( 
    IDXTaskManager * This,
    /* [in] */ ULONG ulNumThreads);


void __RPC_STUB IDXTaskManager_SetThreadPoolSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_GetThreadPoolSize_Proxy( 
    IDXTaskManager * This,
    /* [out] */ ULONG *pulNumThreads);


void __RPC_STUB IDXTaskManager_GetThreadPoolSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_SetConcurrencyLimit_Proxy( 
    IDXTaskManager * This,
    /* [in] */ ULONG ulNumThreads);


void __RPC_STUB IDXTaskManager_SetConcurrencyLimit_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_GetConcurrencyLimit_Proxy( 
    IDXTaskManager * This,
    /* [out] */ ULONG *pulNumThreads);


void __RPC_STUB IDXTaskManager_GetConcurrencyLimit_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_ScheduleTasks_Proxy( 
    IDXTaskManager * This,
    /* [in] */ DXTMTASKINFO TaskInfo[  ],
    /* [in] */ HANDLE Events[  ],
    /* [out] */ DWORD TaskIDs[  ],
    /* [in] */ ULONG ulNumTasks,
    /* [in] */ ULONG ulWaitPeriod);


void __RPC_STUB IDXTaskManager_ScheduleTasks_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_TerminateTasks_Proxy( 
    IDXTaskManager * This,
    /* [in] */ DWORD TaskIDs[  ],
    /* [in] */ ULONG ulCount,
    /* [in] */ ULONG ulTimeOut);


void __RPC_STUB IDXTaskManager_TerminateTasks_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTaskManager_TerminateRequest_Proxy( 
    IDXTaskManager * This,
    /* [in] */ REFIID RequestID,
    /* [in] */ ULONG ulTimeOut);


void __RPC_STUB IDXTaskManager_TerminateRequest_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXTaskManager_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0258 */
/* [local] */ 

#ifdef __cplusplus
/////////////////////////////////////////////////////

class DXBASESAMPLE;
class DXSAMPLE;
class DXPMSAMPLE;

/////////////////////////////////////////////////////

class DXBASESAMPLE
{
public:
    BYTE Blue;
    BYTE Green;
    BYTE Red;
    BYTE Alpha;
    DXBASESAMPLE() {}
    DXBASESAMPLE(const BYTE alpha, const BYTE red, const BYTE green, const BYTE blue) :
        Alpha(alpha),
        Red(red),
        Green(green),
        Blue(blue) {}
    DXBASESAMPLE(const DWORD val) { *this = (*(DXBASESAMPLE *)&val); }
    operator DWORD () const {return *((DWORD *)this); }
    DWORD operator=(const DWORD val) { return *this = *((DXBASESAMPLE *)&val); }
}; // DXBASESAMPLE

/////////////////////////////////////////////////////

class DXSAMPLE : public DXBASESAMPLE
{
public:
    DXSAMPLE() {}
    DXSAMPLE(const BYTE alpha, const BYTE red, const BYTE green, const BYTE blue) :
         DXBASESAMPLE(alpha, red, green, blue) {}
    DXSAMPLE(const DWORD val) { *this = (*(DXSAMPLE *)&val); }
    operator DWORD () const {return *((DWORD *)this); }
    DWORD operator=(const DWORD val) { return *this = *((DXSAMPLE *)&val); }
    operator DXPMSAMPLE() const;
}; // DXSAMPLE

/////////////////////////////////////////////////////

class DXPMSAMPLE : public DXBASESAMPLE
{
public:
    DXPMSAMPLE() {}
    DXPMSAMPLE(const BYTE alpha, const BYTE red, const BYTE green, const BYTE blue) :
         DXBASESAMPLE(alpha, red, green, blue) {}
    DXPMSAMPLE(const DWORD val) { *this = (*(DXPMSAMPLE *)&val); }
    operator DWORD () const {return *((DWORD *)this); }
    DWORD operator=(const DWORD val) { return *this = *((DXPMSAMPLE *)&val); }
    operator DXSAMPLE() const;
}; // DXPMSAMPLE

//
// The following cast operators are to prevent a direct assignment of a DXSAMPLE to a DXPMSAMPLE
//
inline DXSAMPLE::operator DXPMSAMPLE() const { return *((DXPMSAMPLE *)this); }
inline DXPMSAMPLE::operator DXSAMPLE() const { return *((DXSAMPLE *)this); }
#else // !__cplusplus
typedef struct DXBASESAMPLE
    {
    BYTE Blue;
    BYTE Green;
    BYTE Red;
    BYTE Alpha;
    } 	DXBASESAMPLE;

typedef struct DXSAMPLE
    {
    BYTE Blue;
    BYTE Green;
    BYTE Red;
    BYTE Alpha;
    } 	DXSAMPLE;

typedef struct DXPMSAMPLE
    {
    BYTE Blue;
    BYTE Green;
    BYTE Red;
    BYTE Alpha;
    } 	DXPMSAMPLE;

#endif // !__cplusplus
typedef 
enum DXRUNTYPE
    {	DXRUNTYPE_CLEAR	= 0,
	DXRUNTYPE_OPAQUE	= 1,
	DXRUNTYPE_TRANS	= 2,
	DXRUNTYPE_UNKNOWN	= 3
    } 	DXRUNTYPE;

#define	DX_MAX_RUN_INFO_COUNT	( 128 )

// Ignore the definition used by MIDL for TLB generation
#if 0
typedef struct DXRUNINFO
    {
    ULONG Bitfields;
    } 	DXRUNINFO;

#endif // 0
typedef struct DXRUNINFO
{
    ULONG   Type  : 2;   // Type
    ULONG   Count : 30;  // Number of samples in run
} DXRUNINFO;
typedef 
enum DXSFCREATE
    {	DXSF_FORMAT_IS_CLSID	= 1L << 0,
	DXSF_NO_LAZY_DDRAW_LOCK	= 1L << 1
    } 	DXSFCREATE;

typedef 
enum DXBLTOPTIONS
    {	DXBOF_DO_OVER	= 1L << 0,
	DXBOF_DITHER	= 1L << 1
    } 	DXBLTOPTIONS;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0258_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0258_v0_0_s_ifspec;

#ifndef __IDXSurfaceFactory_INTERFACE_DEFINED__
#define __IDXSurfaceFactory_INTERFACE_DEFINED__

/* interface IDXSurfaceFactory */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXSurfaceFactory;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("144946F5-C4D4-11d1-81D1-0000F87557DB")
    IDXSurfaceFactory : public IUnknown
    {
    public:
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE CreateSurface( 
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ DWORD dwFlags,
            /* [in] */ IUnknown *punkOuter,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE CreateFromDDSurface( 
            /* [in] */ IUnknown *pDDrawSurface,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ DWORD dwFlags,
            /* [in] */ IUnknown *punkOuter,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface) = 0;
        
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE LoadImage( 
            /* [in] */ const LPWSTR pszFileName,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface) = 0;
        
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE LoadImageFromStream( 
            /* [in] */ IStream *pStream,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface) = 0;
        
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE CopySurfaceToNewFormat( 
            /* [in] */ IDXSurface *pSrc,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pDestFormatID,
            /* [out] */ IDXSurface **ppNewSurface) = 0;
        
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE CreateD3DRMTexture( 
            /* [in] */ IDXSurface *pSrc,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ IUnknown *pD3DRM3,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppTexture3) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE BitBlt( 
            /* [in] */ IDXSurface *pDest,
            /* [in] */ const DXVEC *pPlacement,
            /* [in] */ IDXSurface *pSrc,
            /* [in] */ const DXBNDS *pClipBounds,
            /* [in] */ DWORD dwFlags) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXSurfaceFactoryVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXSurfaceFactory * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXSurfaceFactory * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXSurfaceFactory * This);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *CreateSurface )( 
            IDXSurfaceFactory * This,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ DWORD dwFlags,
            /* [in] */ IUnknown *punkOuter,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface);
        
        HRESULT ( STDMETHODCALLTYPE *CreateFromDDSurface )( 
            IDXSurfaceFactory * This,
            /* [in] */ IUnknown *pDDrawSurface,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ DWORD dwFlags,
            /* [in] */ IUnknown *punkOuter,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *LoadImage )( 
            IDXSurfaceFactory * This,
            /* [in] */ const LPWSTR pszFileName,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *LoadImageFromStream )( 
            IDXSurfaceFactory * This,
            /* [in] */ IStream *pStream,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppDXSurface);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *CopySurfaceToNewFormat )( 
            IDXSurfaceFactory * This,
            /* [in] */ IDXSurface *pSrc,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pDestFormatID,
            /* [out] */ IDXSurface **ppNewSurface);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *CreateD3DRMTexture )( 
            IDXSurfaceFactory * This,
            /* [in] */ IDXSurface *pSrc,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ IUnknown *pD3DRM3,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppTexture3);
        
        HRESULT ( STDMETHODCALLTYPE *BitBlt )( 
            IDXSurfaceFactory * This,
            /* [in] */ IDXSurface *pDest,
            /* [in] */ const DXVEC *pPlacement,
            /* [in] */ IDXSurface *pSrc,
            /* [in] */ const DXBNDS *pClipBounds,
            /* [in] */ DWORD dwFlags);
        
        END_INTERFACE
    } IDXSurfaceFactoryVtbl;

    interface IDXSurfaceFactory
    {
        CONST_VTBL struct IDXSurfaceFactoryVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXSurfaceFactory_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXSurfaceFactory_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXSurfaceFactory_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXSurfaceFactory_CreateSurface(This,pDirectDraw,pDDSurfaceDesc,pFormatID,pBounds,dwFlags,punkOuter,riid,ppDXSurface)	\
    (This)->lpVtbl -> CreateSurface(This,pDirectDraw,pDDSurfaceDesc,pFormatID,pBounds,dwFlags,punkOuter,riid,ppDXSurface)

#define IDXSurfaceFactory_CreateFromDDSurface(This,pDDrawSurface,pFormatID,dwFlags,punkOuter,riid,ppDXSurface)	\
    (This)->lpVtbl -> CreateFromDDSurface(This,pDDrawSurface,pFormatID,dwFlags,punkOuter,riid,ppDXSurface)

#define IDXSurfaceFactory_LoadImage(This,pszFileName,pDirectDraw,pDDSurfaceDesc,pFormatID,riid,ppDXSurface)	\
    (This)->lpVtbl -> LoadImage(This,pszFileName,pDirectDraw,pDDSurfaceDesc,pFormatID,riid,ppDXSurface)

#define IDXSurfaceFactory_LoadImageFromStream(This,pStream,pDirectDraw,pDDSurfaceDesc,pFormatID,riid,ppDXSurface)	\
    (This)->lpVtbl -> LoadImageFromStream(This,pStream,pDirectDraw,pDDSurfaceDesc,pFormatID,riid,ppDXSurface)

#define IDXSurfaceFactory_CopySurfaceToNewFormat(This,pSrc,pDirectDraw,pDDSurfaceDesc,pDestFormatID,ppNewSurface)	\
    (This)->lpVtbl -> CopySurfaceToNewFormat(This,pSrc,pDirectDraw,pDDSurfaceDesc,pDestFormatID,ppNewSurface)

#define IDXSurfaceFactory_CreateD3DRMTexture(This,pSrc,pDirectDraw,pD3DRM3,riid,ppTexture3)	\
    (This)->lpVtbl -> CreateD3DRMTexture(This,pSrc,pDirectDraw,pD3DRM3,riid,ppTexture3)

#define IDXSurfaceFactory_BitBlt(This,pDest,pPlacement,pSrc,pClipBounds,dwFlags)	\
    (This)->lpVtbl -> BitBlt(This,pDest,pPlacement,pSrc,pClipBounds,dwFlags)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [local] */ HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_CreateSurface_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ IUnknown *pDirectDraw,
    /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
    /* [in] */ const GUID *pFormatID,
    /* [in] */ const DXBNDS *pBounds,
    /* [in] */ DWORD dwFlags,
    /* [in] */ IUnknown *punkOuter,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppDXSurface);


void __RPC_STUB IDXSurfaceFactory_CreateSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_CreateFromDDSurface_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ IUnknown *pDDrawSurface,
    /* [in] */ const GUID *pFormatID,
    /* [in] */ DWORD dwFlags,
    /* [in] */ IUnknown *punkOuter,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppDXSurface);


void __RPC_STUB IDXSurfaceFactory_CreateFromDDSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [local] */ HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_LoadImage_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ const LPWSTR pszFileName,
    /* [in] */ IUnknown *pDirectDraw,
    /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
    /* [in] */ const GUID *pFormatID,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppDXSurface);


void __RPC_STUB IDXSurfaceFactory_LoadImage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [local] */ HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_LoadImageFromStream_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ IStream *pStream,
    /* [in] */ IUnknown *pDirectDraw,
    /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
    /* [in] */ const GUID *pFormatID,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppDXSurface);


void __RPC_STUB IDXSurfaceFactory_LoadImageFromStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [local] */ HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_CopySurfaceToNewFormat_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ IDXSurface *pSrc,
    /* [in] */ IUnknown *pDirectDraw,
    /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
    /* [in] */ const GUID *pDestFormatID,
    /* [out] */ IDXSurface **ppNewSurface);


void __RPC_STUB IDXSurfaceFactory_CopySurfaceToNewFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [local] */ HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_CreateD3DRMTexture_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ IDXSurface *pSrc,
    /* [in] */ IUnknown *pDirectDraw,
    /* [in] */ IUnknown *pD3DRM3,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppTexture3);


void __RPC_STUB IDXSurfaceFactory_CreateD3DRMTexture_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceFactory_BitBlt_Proxy( 
    IDXSurfaceFactory * This,
    /* [in] */ IDXSurface *pDest,
    /* [in] */ const DXVEC *pPlacement,
    /* [in] */ IDXSurface *pSrc,
    /* [in] */ const DXBNDS *pClipBounds,
    /* [in] */ DWORD dwFlags);


void __RPC_STUB IDXSurfaceFactory_BitBlt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXSurfaceFactory_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0259 */
/* [local] */ 

typedef 
enum DXSURFMODCOMPOP
    {	DXSURFMOD_COMP_OVER	= 0,
	DXSURFMOD_COMP_ALPHA_MASK	= 1,
	DXSURFMOD_COMP_MAX_VALID	= 1
    } 	DXSURFMODCOMPOP;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0259_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0259_v0_0_s_ifspec;

#ifndef __IDXSurfaceModifier_INTERFACE_DEFINED__
#define __IDXSurfaceModifier_INTERFACE_DEFINED__

/* interface IDXSurfaceModifier */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXSurfaceModifier;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9EA3B637-C37D-11d1-905E-00C04FD9189D")
    IDXSurfaceModifier : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetFillColor( 
            /* [in] */ DXSAMPLE Color) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetFillColor( 
            /* [out] */ DXSAMPLE *pColor) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetBounds( 
            /* [in] */ const DXBNDS *pBounds) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetBackground( 
            /* [in] */ IDXSurface *pSurface) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetBackground( 
            /* [out] */ IDXSurface **ppSurface) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetCompositeOperation( 
            /* [in] */ DXSURFMODCOMPOP CompOp) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetCompositeOperation( 
            /* [out] */ DXSURFMODCOMPOP *pCompOp) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetForeground( 
            /* [in] */ IDXSurface *pSurface,
            /* [in] */ BOOL bTile,
            /* [in] */ const POINT *pOrigin) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetForeground( 
            /* [out] */ IDXSurface **ppSurface,
            /* [out] */ BOOL *pbTile,
            /* [out] */ POINT *pOrigin) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetOpacity( 
            /* [in] */ float Opacity) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetOpacity( 
            /* [out] */ float *pOpacity) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetLookup( 
            /* [in] */ IDXLookupTable *pLookupTable) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetLookup( 
            /* [out] */ IDXLookupTable **ppLookupTable) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXSurfaceModifierVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXSurfaceModifier * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXSurfaceModifier * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXSurfaceModifier * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetFillColor )( 
            IDXSurfaceModifier * This,
            /* [in] */ DXSAMPLE Color);
        
        HRESULT ( STDMETHODCALLTYPE *GetFillColor )( 
            IDXSurfaceModifier * This,
            /* [out] */ DXSAMPLE *pColor);
        
        HRESULT ( STDMETHODCALLTYPE *SetBounds )( 
            IDXSurfaceModifier * This,
            /* [in] */ const DXBNDS *pBounds);
        
        HRESULT ( STDMETHODCALLTYPE *SetBackground )( 
            IDXSurfaceModifier * This,
            /* [in] */ IDXSurface *pSurface);
        
        HRESULT ( STDMETHODCALLTYPE *GetBackground )( 
            IDXSurfaceModifier * This,
            /* [out] */ IDXSurface **ppSurface);
        
        HRESULT ( STDMETHODCALLTYPE *SetCompositeOperation )( 
            IDXSurfaceModifier * This,
            /* [in] */ DXSURFMODCOMPOP CompOp);
        
        HRESULT ( STDMETHODCALLTYPE *GetCompositeOperation )( 
            IDXSurfaceModifier * This,
            /* [out] */ DXSURFMODCOMPOP *pCompOp);
        
        HRESULT ( STDMETHODCALLTYPE *SetForeground )( 
            IDXSurfaceModifier * This,
            /* [in] */ IDXSurface *pSurface,
            /* [in] */ BOOL bTile,
            /* [in] */ const POINT *pOrigin);
        
        HRESULT ( STDMETHODCALLTYPE *GetForeground )( 
            IDXSurfaceModifier * This,
            /* [out] */ IDXSurface **ppSurface,
            /* [out] */ BOOL *pbTile,
            /* [out] */ POINT *pOrigin);
        
        HRESULT ( STDMETHODCALLTYPE *SetOpacity )( 
            IDXSurfaceModifier * This,
            /* [in] */ float Opacity);
        
        HRESULT ( STDMETHODCALLTYPE *GetOpacity )( 
            IDXSurfaceModifier * This,
            /* [out] */ float *pOpacity);
        
        HRESULT ( STDMETHODCALLTYPE *SetLookup )( 
            IDXSurfaceModifier * This,
            /* [in] */ IDXLookupTable *pLookupTable);
        
        HRESULT ( STDMETHODCALLTYPE *GetLookup )( 
            IDXSurfaceModifier * This,
            /* [out] */ IDXLookupTable **ppLookupTable);
        
        END_INTERFACE
    } IDXSurfaceModifierVtbl;

    interface IDXSurfaceModifier
    {
        CONST_VTBL struct IDXSurfaceModifierVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXSurfaceModifier_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXSurfaceModifier_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXSurfaceModifier_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXSurfaceModifier_SetFillColor(This,Color)	\
    (This)->lpVtbl -> SetFillColor(This,Color)

#define IDXSurfaceModifier_GetFillColor(This,pColor)	\
    (This)->lpVtbl -> GetFillColor(This,pColor)

#define IDXSurfaceModifier_SetBounds(This,pBounds)	\
    (This)->lpVtbl -> SetBounds(This,pBounds)

#define IDXSurfaceModifier_SetBackground(This,pSurface)	\
    (This)->lpVtbl -> SetBackground(This,pSurface)

#define IDXSurfaceModifier_GetBackground(This,ppSurface)	\
    (This)->lpVtbl -> GetBackground(This,ppSurface)

#define IDXSurfaceModifier_SetCompositeOperation(This,CompOp)	\
    (This)->lpVtbl -> SetCompositeOperation(This,CompOp)

#define IDXSurfaceModifier_GetCompositeOperation(This,pCompOp)	\
    (This)->lpVtbl -> GetCompositeOperation(This,pCompOp)

#define IDXSurfaceModifier_SetForeground(This,pSurface,bTile,pOrigin)	\
    (This)->lpVtbl -> SetForeground(This,pSurface,bTile,pOrigin)

#define IDXSurfaceModifier_GetForeground(This,ppSurface,pbTile,pOrigin)	\
    (This)->lpVtbl -> GetForeground(This,ppSurface,pbTile,pOrigin)

#define IDXSurfaceModifier_SetOpacity(This,Opacity)	\
    (This)->lpVtbl -> SetOpacity(This,Opacity)

#define IDXSurfaceModifier_GetOpacity(This,pOpacity)	\
    (This)->lpVtbl -> GetOpacity(This,pOpacity)

#define IDXSurfaceModifier_SetLookup(This,pLookupTable)	\
    (This)->lpVtbl -> SetLookup(This,pLookupTable)

#define IDXSurfaceModifier_GetLookup(This,ppLookupTable)	\
    (This)->lpVtbl -> GetLookup(This,ppLookupTable)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetFillColor_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ DXSAMPLE Color);


void __RPC_STUB IDXSurfaceModifier_SetFillColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_GetFillColor_Proxy( 
    IDXSurfaceModifier * This,
    /* [out] */ DXSAMPLE *pColor);


void __RPC_STUB IDXSurfaceModifier_GetFillColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetBounds_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ const DXBNDS *pBounds);


void __RPC_STUB IDXSurfaceModifier_SetBounds_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetBackground_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ IDXSurface *pSurface);


void __RPC_STUB IDXSurfaceModifier_SetBackground_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_GetBackground_Proxy( 
    IDXSurfaceModifier * This,
    /* [out] */ IDXSurface **ppSurface);


void __RPC_STUB IDXSurfaceModifier_GetBackground_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetCompositeOperation_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ DXSURFMODCOMPOP CompOp);


void __RPC_STUB IDXSurfaceModifier_SetCompositeOperation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_GetCompositeOperation_Proxy( 
    IDXSurfaceModifier * This,
    /* [out] */ DXSURFMODCOMPOP *pCompOp);


void __RPC_STUB IDXSurfaceModifier_GetCompositeOperation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetForeground_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ IDXSurface *pSurface,
    /* [in] */ BOOL bTile,
    /* [in] */ const POINT *pOrigin);


void __RPC_STUB IDXSurfaceModifier_SetForeground_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_GetForeground_Proxy( 
    IDXSurfaceModifier * This,
    /* [out] */ IDXSurface **ppSurface,
    /* [out] */ BOOL *pbTile,
    /* [out] */ POINT *pOrigin);


void __RPC_STUB IDXSurfaceModifier_GetForeground_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetOpacity_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ float Opacity);


void __RPC_STUB IDXSurfaceModifier_SetOpacity_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_GetOpacity_Proxy( 
    IDXSurfaceModifier * This,
    /* [out] */ float *pOpacity);


void __RPC_STUB IDXSurfaceModifier_GetOpacity_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_SetLookup_Proxy( 
    IDXSurfaceModifier * This,
    /* [in] */ IDXLookupTable *pLookupTable);


void __RPC_STUB IDXSurfaceModifier_SetLookup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurfaceModifier_GetLookup_Proxy( 
    IDXSurfaceModifier * This,
    /* [out] */ IDXLookupTable **ppLookupTable);


void __RPC_STUB IDXSurfaceModifier_GetLookup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXSurfaceModifier_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0260 */
/* [local] */ 

typedef 
enum DXSAMPLEFORMATENUM
    {	DXPF_FLAGSMASK	= 0xffff0000,
	DXPF_NONPREMULT	= 0x10000,
	DXPF_TRANSPARENCY	= 0x20000,
	DXPF_TRANSLUCENCY	= 0x40000,
	DXPF_2BITERROR	= 0x200000,
	DXPF_3BITERROR	= 0x300000,
	DXPF_4BITERROR	= 0x400000,
	DXPF_5BITERROR	= 0x500000,
	DXPF_ERRORMASK	= 0x700000,
	DXPF_NONSTANDARD	= 0,
	DXPF_PMARGB32	= 1 | DXPF_TRANSPARENCY | DXPF_TRANSLUCENCY,
	DXPF_ARGB32	= 2 | DXPF_NONPREMULT | DXPF_TRANSPARENCY | DXPF_TRANSLUCENCY,
	DXPF_ARGB4444	= 3 | DXPF_NONPREMULT | DXPF_TRANSPARENCY | DXPF_TRANSLUCENCY | DXPF_4BITERROR,
	DXPF_A8	= 4 | DXPF_TRANSPARENCY | DXPF_TRANSLUCENCY,
	DXPF_RGB32	= 5,
	DXPF_RGB24	= 6,
	DXPF_RGB565	= 7 | DXPF_3BITERROR,
	DXPF_RGB555	= 8 | DXPF_3BITERROR,
	DXPF_RGB8	= 9 | DXPF_5BITERROR,
	DXPF_ARGB1555	= 10 | DXPF_TRANSPARENCY | DXPF_3BITERROR,
	DXPF_RGB32_CK	= DXPF_RGB32 | DXPF_TRANSPARENCY,
	DXPF_RGB24_CK	= DXPF_RGB24 | DXPF_TRANSPARENCY,
	DXPF_RGB555_CK	= DXPF_RGB555 | DXPF_TRANSPARENCY,
	DXPF_RGB565_CK	= DXPF_RGB565 | DXPF_TRANSPARENCY,
	DXPF_RGB8_CK	= DXPF_RGB8 | DXPF_TRANSPARENCY
    } 	DXSAMPLEFORMATENUM;

typedef 
enum DXLOCKSURF
    {	DXLOCKF_READ	= 0,
	DXLOCKF_READWRITE	= 1 << 0,
	DXLOCKF_EXISTINGINFOONLY	= 1 << 1,
	DXLOCKF_WANTRUNINFO	= 1 << 2,
	DXLOCKF_NONPREMULT	= 1 << 16,
	DXLOCKF_VALIDFLAGS	= DXLOCKF_READWRITE | DXLOCKF_EXISTINGINFOONLY | DXLOCKF_WANTRUNINFO | DXLOCKF_NONPREMULT
    } 	DXLOCKSURF;

typedef 
enum DXSURFSTATUS
    {	DXSURF_TRANSIENT	= 1 << 0,
	DXSURF_READONLY	= 1 << 1,
	DXSURF_VALIDFLAGS	= DXSURF_TRANSIENT | DXSURF_READONLY
    } 	DXSURFSTATUS;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0260_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0260_v0_0_s_ifspec;

#ifndef __IDXSurface_INTERFACE_DEFINED__
#define __IDXSurface_INTERFACE_DEFINED__

/* interface IDXSurface */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXSurface;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B39FD73F-E139-11d1-9065-00C04FD9189D")
    IDXSurface : public IDXBaseObject
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetPixelFormat( 
            /* [out] */ GUID *pFormatID,
            /* [out] */ DXSAMPLEFORMATENUM *pSampleFormatEnum) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetBounds( 
            /* [out] */ DXBNDS *pBounds) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetStatusFlags( 
            /* [out] */ DWORD *pdwStatusFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetStatusFlags( 
            /* [in] */ DWORD dwStatusFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE LockSurface( 
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ ULONG ulTimeOut,
            /* [in] */ DWORD dwFlags,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppPointer,
            /* [out] */ ULONG *pulGenerationId) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetDirectDrawSurface( 
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppSurface) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetColorKey( 
            DXSAMPLE *pColorKey) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetColorKey( 
            DXSAMPLE ColorKey) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE LockSurfaceDC( 
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ ULONG ulTimeOut,
            /* [in] */ DWORD dwFlags,
            /* [out] */ IDXDCLock **ppDCLock) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetAppData( 
            DWORD_PTR dwAppData) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetAppData( 
            DWORD_PTR *pdwAppData) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXSurfaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXSurface * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXSurface * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXSurface * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetGenerationId )( 
            IDXSurface * This,
            /* [out] */ ULONG *pID);
        
        HRESULT ( STDMETHODCALLTYPE *IncrementGenerationId )( 
            IDXSurface * This,
            /* [in] */ BOOL bRefresh);
        
        HRESULT ( STDMETHODCALLTYPE *GetObjectSize )( 
            IDXSurface * This,
            /* [out] */ ULONG *pcbSize);
        
        HRESULT ( STDMETHODCALLTYPE *GetPixelFormat )( 
            IDXSurface * This,
            /* [out] */ GUID *pFormatID,
            /* [out] */ DXSAMPLEFORMATENUM *pSampleFormatEnum);
        
        HRESULT ( STDMETHODCALLTYPE *GetBounds )( 
            IDXSurface * This,
            /* [out] */ DXBNDS *pBounds);
        
        HRESULT ( STDMETHODCALLTYPE *GetStatusFlags )( 
            IDXSurface * This,
            /* [out] */ DWORD *pdwStatusFlags);
        
        HRESULT ( STDMETHODCALLTYPE *SetStatusFlags )( 
            IDXSurface * This,
            /* [in] */ DWORD dwStatusFlags);
        
        HRESULT ( STDMETHODCALLTYPE *LockSurface )( 
            IDXSurface * This,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ ULONG ulTimeOut,
            /* [in] */ DWORD dwFlags,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppPointer,
            /* [out] */ ULONG *pulGenerationId);
        
        HRESULT ( STDMETHODCALLTYPE *GetDirectDrawSurface )( 
            IDXSurface * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppSurface);
        
        HRESULT ( STDMETHODCALLTYPE *GetColorKey )( 
            IDXSurface * This,
            DXSAMPLE *pColorKey);
        
        HRESULT ( STDMETHODCALLTYPE *SetColorKey )( 
            IDXSurface * This,
            DXSAMPLE ColorKey);
        
        HRESULT ( STDMETHODCALLTYPE *LockSurfaceDC )( 
            IDXSurface * This,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ ULONG ulTimeOut,
            /* [in] */ DWORD dwFlags,
            /* [out] */ IDXDCLock **ppDCLock);
        
        HRESULT ( STDMETHODCALLTYPE *SetAppData )( 
            IDXSurface * This,
            DWORD_PTR dwAppData);
        
        HRESULT ( STDMETHODCALLTYPE *GetAppData )( 
            IDXSurface * This,
            DWORD_PTR *pdwAppData);
        
        END_INTERFACE
    } IDXSurfaceVtbl;

    interface IDXSurface
    {
        CONST_VTBL struct IDXSurfaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXSurface_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXSurface_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXSurface_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXSurface_GetGenerationId(This,pID)	\
    (This)->lpVtbl -> GetGenerationId(This,pID)

#define IDXSurface_IncrementGenerationId(This,bRefresh)	\
    (This)->lpVtbl -> IncrementGenerationId(This,bRefresh)

#define IDXSurface_GetObjectSize(This,pcbSize)	\
    (This)->lpVtbl -> GetObjectSize(This,pcbSize)


#define IDXSurface_GetPixelFormat(This,pFormatID,pSampleFormatEnum)	\
    (This)->lpVtbl -> GetPixelFormat(This,pFormatID,pSampleFormatEnum)

#define IDXSurface_GetBounds(This,pBounds)	\
    (This)->lpVtbl -> GetBounds(This,pBounds)

#define IDXSurface_GetStatusFlags(This,pdwStatusFlags)	\
    (This)->lpVtbl -> GetStatusFlags(This,pdwStatusFlags)

#define IDXSurface_SetStatusFlags(This,dwStatusFlags)	\
    (This)->lpVtbl -> SetStatusFlags(This,dwStatusFlags)

#define IDXSurface_LockSurface(This,pBounds,ulTimeOut,dwFlags,riid,ppPointer,pulGenerationId)	\
    (This)->lpVtbl -> LockSurface(This,pBounds,ulTimeOut,dwFlags,riid,ppPointer,pulGenerationId)

#define IDXSurface_GetDirectDrawSurface(This,riid,ppSurface)	\
    (This)->lpVtbl -> GetDirectDrawSurface(This,riid,ppSurface)

#define IDXSurface_GetColorKey(This,pColorKey)	\
    (This)->lpVtbl -> GetColorKey(This,pColorKey)

#define IDXSurface_SetColorKey(This,ColorKey)	\
    (This)->lpVtbl -> SetColorKey(This,ColorKey)

#define IDXSurface_LockSurfaceDC(This,pBounds,ulTimeOut,dwFlags,ppDCLock)	\
    (This)->lpVtbl -> LockSurfaceDC(This,pBounds,ulTimeOut,dwFlags,ppDCLock)

#define IDXSurface_SetAppData(This,dwAppData)	\
    (This)->lpVtbl -> SetAppData(This,dwAppData)

#define IDXSurface_GetAppData(This,pdwAppData)	\
    (This)->lpVtbl -> GetAppData(This,pdwAppData)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXSurface_GetPixelFormat_Proxy( 
    IDXSurface * This,
    /* [out] */ GUID *pFormatID,
    /* [out] */ DXSAMPLEFORMATENUM *pSampleFormatEnum);


void __RPC_STUB IDXSurface_GetPixelFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_GetBounds_Proxy( 
    IDXSurface * This,
    /* [out] */ DXBNDS *pBounds);


void __RPC_STUB IDXSurface_GetBounds_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_GetStatusFlags_Proxy( 
    IDXSurface * This,
    /* [out] */ DWORD *pdwStatusFlags);


void __RPC_STUB IDXSurface_GetStatusFlags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_SetStatusFlags_Proxy( 
    IDXSurface * This,
    /* [in] */ DWORD dwStatusFlags);


void __RPC_STUB IDXSurface_SetStatusFlags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_LockSurface_Proxy( 
    IDXSurface * This,
    /* [in] */ const DXBNDS *pBounds,
    /* [in] */ ULONG ulTimeOut,
    /* [in] */ DWORD dwFlags,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppPointer,
    /* [out] */ ULONG *pulGenerationId);


void __RPC_STUB IDXSurface_LockSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_GetDirectDrawSurface_Proxy( 
    IDXSurface * This,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppSurface);


void __RPC_STUB IDXSurface_GetDirectDrawSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_GetColorKey_Proxy( 
    IDXSurface * This,
    DXSAMPLE *pColorKey);


void __RPC_STUB IDXSurface_GetColorKey_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_SetColorKey_Proxy( 
    IDXSurface * This,
    DXSAMPLE ColorKey);


void __RPC_STUB IDXSurface_SetColorKey_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_LockSurfaceDC_Proxy( 
    IDXSurface * This,
    /* [in] */ const DXBNDS *pBounds,
    /* [in] */ ULONG ulTimeOut,
    /* [in] */ DWORD dwFlags,
    /* [out] */ IDXDCLock **ppDCLock);


void __RPC_STUB IDXSurface_LockSurfaceDC_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_SetAppData_Proxy( 
    IDXSurface * This,
    DWORD_PTR dwAppData);


void __RPC_STUB IDXSurface_SetAppData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXSurface_GetAppData_Proxy( 
    IDXSurface * This,
    DWORD_PTR *pdwAppData);


void __RPC_STUB IDXSurface_GetAppData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXSurface_INTERFACE_DEFINED__ */


#ifndef __IDXSurfaceInit_INTERFACE_DEFINED__
#define __IDXSurfaceInit_INTERFACE_DEFINED__

/* interface IDXSurfaceInit */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXSurfaceInit;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9EA3B639-C37D-11d1-905E-00C04FD9189D")
    IDXSurfaceInit : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE InitSurface( 
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ DWORD dwFlags) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXSurfaceInitVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXSurfaceInit * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXSurfaceInit * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXSurfaceInit * This);
        
        HRESULT ( STDMETHODCALLTYPE *InitSurface )( 
            IDXSurfaceInit * This,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ DWORD dwFlags);
        
        END_INTERFACE
    } IDXSurfaceInitVtbl;

    interface IDXSurfaceInit
    {
        CONST_VTBL struct IDXSurfaceInitVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXSurfaceInit_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXSurfaceInit_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXSurfaceInit_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXSurfaceInit_InitSurface(This,pDirectDraw,pDDSurfaceDesc,pFormatID,pBounds,dwFlags)	\
    (This)->lpVtbl -> InitSurface(This,pDirectDraw,pDDSurfaceDesc,pFormatID,pBounds,dwFlags)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXSurfaceInit_InitSurface_Proxy( 
    IDXSurfaceInit * This,
    /* [in] */ IUnknown *pDirectDraw,
    /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
    /* [in] */ const GUID *pFormatID,
    /* [in] */ const DXBNDS *pBounds,
    /* [in] */ DWORD dwFlags);


void __RPC_STUB IDXSurfaceInit_InitSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXSurfaceInit_INTERFACE_DEFINED__ */


#ifndef __IDXARGBSurfaceInit_INTERFACE_DEFINED__
#define __IDXARGBSurfaceInit_INTERFACE_DEFINED__

/* interface IDXARGBSurfaceInit */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXARGBSurfaceInit;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9EA3B63A-C37D-11d1-905E-00C04FD9189D")
    IDXARGBSurfaceInit : public IDXSurfaceInit
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE InitFromDDSurface( 
            /* [in] */ IUnknown *pDDrawSurface,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ DWORD dwFlags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE InitFromRawSurface( 
            /* [in] */ IDXRawSurface *pRawSurface) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXARGBSurfaceInitVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXARGBSurfaceInit * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXARGBSurfaceInit * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXARGBSurfaceInit * This);
        
        HRESULT ( STDMETHODCALLTYPE *InitSurface )( 
            IDXARGBSurfaceInit * This,
            /* [in] */ IUnknown *pDirectDraw,
            /* [in] */ const DDSURFACEDESC *pDDSurfaceDesc,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ const DXBNDS *pBounds,
            /* [in] */ DWORD dwFlags);
        
        HRESULT ( STDMETHODCALLTYPE *InitFromDDSurface )( 
            IDXARGBSurfaceInit * This,
            /* [in] */ IUnknown *pDDrawSurface,
            /* [in] */ const GUID *pFormatID,
            /* [in] */ DWORD dwFlags);
        
        HRESULT ( STDMETHODCALLTYPE *InitFromRawSurface )( 
            IDXARGBSurfaceInit * This,
            /* [in] */ IDXRawSurface *pRawSurface);
        
        END_INTERFACE
    } IDXARGBSurfaceInitVtbl;

    interface IDXARGBSurfaceInit
    {
        CONST_VTBL struct IDXARGBSurfaceInitVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXARGBSurfaceInit_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXARGBSurfaceInit_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXARGBSurfaceInit_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXARGBSurfaceInit_InitSurface(This,pDirectDraw,pDDSurfaceDesc,pFormatID,pBounds,dwFlags)	\
    (This)->lpVtbl -> InitSurface(This,pDirectDraw,pDDSurfaceDesc,pFormatID,pBounds,dwFlags)


#define IDXARGBSurfaceInit_InitFromDDSurface(This,pDDrawSurface,pFormatID,dwFlags)	\
    (This)->lpVtbl -> InitFromDDSurface(This,pDDrawSurface,pFormatID,dwFlags)

#define IDXARGBSurfaceInit_InitFromRawSurface(This,pRawSurface)	\
    (This)->lpVtbl -> InitFromRawSurface(This,pRawSurface)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXARGBSurfaceInit_InitFromDDSurface_Proxy( 
    IDXARGBSurfaceInit * This,
    /* [in] */ IUnknown *pDDrawSurface,
    /* [in] */ const GUID *pFormatID,
    /* [in] */ DWORD dwFlags);


void __RPC_STUB IDXARGBSurfaceInit_InitFromDDSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXARGBSurfaceInit_InitFromRawSurface_Proxy( 
    IDXARGBSurfaceInit * This,
    /* [in] */ IDXRawSurface *pRawSurface);


void __RPC_STUB IDXARGBSurfaceInit_InitFromRawSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXARGBSurfaceInit_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0263 */
/* [local] */ 

typedef struct tagDXNATIVETYPEINFO
    {
    BYTE *pCurrentData;
    BYTE *pFirstByte;
    long lPitch;
    DWORD dwColorKey;
    } 	DXNATIVETYPEINFO;

typedef struct tagDXPACKEDRECTDESC
    {
    DXBASESAMPLE *pSamples;
    BOOL bPremult;
    RECT rect;
    long lRowPadding;
    } 	DXPACKEDRECTDESC;

typedef struct tagDXOVERSAMPLEDESC
    {
    POINT p;
    DXPMSAMPLE Color;
    } 	DXOVERSAMPLEDESC;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0263_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0263_v0_0_s_ifspec;

#ifndef __IDXARGBReadPtr_INTERFACE_DEFINED__
#define __IDXARGBReadPtr_INTERFACE_DEFINED__

/* interface IDXARGBReadPtr */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXARGBReadPtr;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EAAAC2D6-C290-11d1-905D-00C04FD9189D")
    IDXARGBReadPtr : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetSurface( 
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppSurface) = 0;
        
        virtual DXSAMPLEFORMATENUM STDMETHODCALLTYPE GetNativeType( 
            /* [out] */ DXNATIVETYPEINFO *pInfo) = 0;
        
        virtual void STDMETHODCALLTYPE Move( 
            /* [in] */ long cSamples) = 0;
        
        virtual void STDMETHODCALLTYPE MoveToRow( 
            /* [in] */ ULONG y) = 0;
        
        virtual void STDMETHODCALLTYPE MoveToXY( 
            /* [in] */ ULONG x,
            /* [in] */ ULONG y) = 0;
        
        virtual ULONG STDMETHODCALLTYPE MoveAndGetRunInfo( 
            /* [in] */ ULONG Row,
            /* [out] */ const DXRUNINFO **ppInfo) = 0;
        
        virtual DXSAMPLE *STDMETHODCALLTYPE Unpack( 
            /* [in] */ DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bMove) = 0;
        
        virtual DXPMSAMPLE *STDMETHODCALLTYPE UnpackPremult( 
            /* [in] */ DXPMSAMPLE *pSamples,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bMove) = 0;
        
        virtual void STDMETHODCALLTYPE UnpackRect( 
            /* [in] */ const DXPACKEDRECTDESC *pRectDesc) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXARGBReadPtrVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXARGBReadPtr * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXARGBReadPtr * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXARGBReadPtr * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetSurface )( 
            IDXARGBReadPtr * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppSurface);
        
        DXSAMPLEFORMATENUM ( STDMETHODCALLTYPE *GetNativeType )( 
            IDXARGBReadPtr * This,
            /* [out] */ DXNATIVETYPEINFO *pInfo);
        
        void ( STDMETHODCALLTYPE *Move )( 
            IDXARGBReadPtr * This,
            /* [in] */ long cSamples);
        
        void ( STDMETHODCALLTYPE *MoveToRow )( 
            IDXARGBReadPtr * This,
            /* [in] */ ULONG y);
        
        void ( STDMETHODCALLTYPE *MoveToXY )( 
            IDXARGBReadPtr * This,
            /* [in] */ ULONG x,
            /* [in] */ ULONG y);
        
        ULONG ( STDMETHODCALLTYPE *MoveAndGetRunInfo )( 
            IDXARGBReadPtr * This,
            /* [in] */ ULONG Row,
            /* [out] */ const DXRUNINFO **ppInfo);
        
        DXSAMPLE *( STDMETHODCALLTYPE *Unpack )( 
            IDXARGBReadPtr * This,
            /* [in] */ DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bMove);
        
        DXPMSAMPLE *( STDMETHODCALLTYPE *UnpackPremult )( 
            IDXARGBReadPtr * This,
            /* [in] */ DXPMSAMPLE *pSamples,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bMove);
        
        void ( STDMETHODCALLTYPE *UnpackRect )( 
            IDXARGBReadPtr * This,
            /* [in] */ const DXPACKEDRECTDESC *pRectDesc);
        
        END_INTERFACE
    } IDXARGBReadPtrVtbl;

    interface IDXARGBReadPtr
    {
        CONST_VTBL struct IDXARGBReadPtrVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXARGBReadPtr_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXARGBReadPtr_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXARGBReadPtr_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXARGBReadPtr_GetSurface(This,riid,ppSurface)	\
    (This)->lpVtbl -> GetSurface(This,riid,ppSurface)

#define IDXARGBReadPtr_GetNativeType(This,pInfo)	\
    (This)->lpVtbl -> GetNativeType(This,pInfo)

#define IDXARGBReadPtr_Move(This,cSamples)	\
    (This)->lpVtbl -> Move(This,cSamples)

#define IDXARGBReadPtr_MoveToRow(This,y)	\
    (This)->lpVtbl -> MoveToRow(This,y)

#define IDXARGBReadPtr_MoveToXY(This,x,y)	\
    (This)->lpVtbl -> MoveToXY(This,x,y)

#define IDXARGBReadPtr_MoveAndGetRunInfo(This,Row,ppInfo)	\
    (This)->lpVtbl -> MoveAndGetRunInfo(This,Row,ppInfo)

#define IDXARGBReadPtr_Unpack(This,pSamples,cSamples,bMove)	\
    (This)->lpVtbl -> Unpack(This,pSamples,cSamples,bMove)

#define IDXARGBReadPtr_UnpackPremult(This,pSamples,cSamples,bMove)	\
    (This)->lpVtbl -> UnpackPremult(This,pSamples,cSamples,bMove)

#define IDXARGBReadPtr_UnpackRect(This,pRectDesc)	\
    (This)->lpVtbl -> UnpackRect(This,pRectDesc)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXARGBReadPtr_GetSurface_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void **ppSurface);


void __RPC_STUB IDXARGBReadPtr_GetSurface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


DXSAMPLEFORMATENUM STDMETHODCALLTYPE IDXARGBReadPtr_GetNativeType_Proxy( 
    IDXARGBReadPtr * This,
    /* [out] */ DXNATIVETYPEINFO *pInfo);


void __RPC_STUB IDXARGBReadPtr_GetNativeType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadPtr_Move_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ long cSamples);


void __RPC_STUB IDXARGBReadPtr_Move_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadPtr_MoveToRow_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ ULONG y);


void __RPC_STUB IDXARGBReadPtr_MoveToRow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadPtr_MoveToXY_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ ULONG x,
    /* [in] */ ULONG y);


void __RPC_STUB IDXARGBReadPtr_MoveToXY_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


ULONG STDMETHODCALLTYPE IDXARGBReadPtr_MoveAndGetRunInfo_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ ULONG Row,
    /* [out] */ const DXRUNINFO **ppInfo);


void __RPC_STUB IDXARGBReadPtr_MoveAndGetRunInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


DXSAMPLE *STDMETHODCALLTYPE IDXARGBReadPtr_Unpack_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ DXSAMPLE *pSamples,
    /* [in] */ ULONG cSamples,
    /* [in] */ BOOL bMove);


void __RPC_STUB IDXARGBReadPtr_Unpack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


DXPMSAMPLE *STDMETHODCALLTYPE IDXARGBReadPtr_UnpackPremult_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ DXPMSAMPLE *pSamples,
    /* [in] */ ULONG cSamples,
    /* [in] */ BOOL bMove);


void __RPC_STUB IDXARGBReadPtr_UnpackPremult_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadPtr_UnpackRect_Proxy( 
    IDXARGBReadPtr * This,
    /* [in] */ const DXPACKEDRECTDESC *pRectDesc);


void __RPC_STUB IDXARGBReadPtr_UnpackRect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXARGBReadPtr_INTERFACE_DEFINED__ */


#ifndef __IDXARGBReadWritePtr_INTERFACE_DEFINED__
#define __IDXARGBReadWritePtr_INTERFACE_DEFINED__

/* interface IDXARGBReadWritePtr */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXARGBReadWritePtr;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EAAAC2D7-C290-11d1-905D-00C04FD9189D")
    IDXARGBReadWritePtr : public IDXARGBReadPtr
    {
    public:
        virtual void STDMETHODCALLTYPE PackAndMove( 
            /* [in] */ const DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples) = 0;
        
        virtual void STDMETHODCALLTYPE PackPremultAndMove( 
            /* [in] */ const DXPMSAMPLE *pSamples,
            /* [in] */ ULONG cSamples) = 0;
        
        virtual void STDMETHODCALLTYPE PackRect( 
            /* [in] */ const DXPACKEDRECTDESC *pRectDesc) = 0;
        
        virtual void STDMETHODCALLTYPE CopyAndMoveBoth( 
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ IDXARGBReadPtr *pSrc,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bIsOpaque) = 0;
        
        virtual void STDMETHODCALLTYPE CopyRect( 
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ const RECT *pDestRect,
            /* [in] */ IDXARGBReadPtr *pSrc,
            /* [in] */ const POINT *pSrcOrigin,
            /* [in] */ BOOL bIsOpaque) = 0;
        
        virtual void STDMETHODCALLTYPE FillAndMove( 
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ DXPMSAMPLE SampVal,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bDoOver) = 0;
        
        virtual void STDMETHODCALLTYPE FillRect( 
            /* [in] */ const RECT *pRect,
            /* [in] */ DXPMSAMPLE SampVal,
            /* [in] */ BOOL bDoOver) = 0;
        
        virtual void STDMETHODCALLTYPE OverSample( 
            /* [in] */ const DXOVERSAMPLEDESC *pOverDesc) = 0;
        
        virtual void STDMETHODCALLTYPE OverArrayAndMove( 
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ const DXPMSAMPLE *pSrc,
            /* [in] */ ULONG cSamples) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXARGBReadWritePtrVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXARGBReadWritePtr * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXARGBReadWritePtr * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetSurface )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppSurface);
        
        DXSAMPLEFORMATENUM ( STDMETHODCALLTYPE *GetNativeType )( 
            IDXARGBReadWritePtr * This,
            /* [out] */ DXNATIVETYPEINFO *pInfo);
        
        void ( STDMETHODCALLTYPE *Move )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ long cSamples);
        
        void ( STDMETHODCALLTYPE *MoveToRow )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ ULONG y);
        
        void ( STDMETHODCALLTYPE *MoveToXY )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ ULONG x,
            /* [in] */ ULONG y);
        
        ULONG ( STDMETHODCALLTYPE *MoveAndGetRunInfo )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ ULONG Row,
            /* [out] */ const DXRUNINFO **ppInfo);
        
        DXSAMPLE *( STDMETHODCALLTYPE *Unpack )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bMove);
        
        DXPMSAMPLE *( STDMETHODCALLTYPE *UnpackPremult )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ DXPMSAMPLE *pSamples,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bMove);
        
        void ( STDMETHODCALLTYPE *UnpackRect )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ const DXPACKEDRECTDESC *pRectDesc);
        
        void ( STDMETHODCALLTYPE *PackAndMove )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ const DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples);
        
        void ( STDMETHODCALLTYPE *PackPremultAndMove )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ const DXPMSAMPLE *pSamples,
            /* [in] */ ULONG cSamples);
        
        void ( STDMETHODCALLTYPE *PackRect )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ const DXPACKEDRECTDESC *pRectDesc);
        
        void ( STDMETHODCALLTYPE *CopyAndMoveBoth )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ IDXARGBReadPtr *pSrc,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bIsOpaque);
        
        void ( STDMETHODCALLTYPE *CopyRect )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ const RECT *pDestRect,
            /* [in] */ IDXARGBReadPtr *pSrc,
            /* [in] */ const POINT *pSrcOrigin,
            /* [in] */ BOOL bIsOpaque);
        
        void ( STDMETHODCALLTYPE *FillAndMove )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ DXPMSAMPLE SampVal,
            /* [in] */ ULONG cSamples,
            /* [in] */ BOOL bDoOver);
        
        void ( STDMETHODCALLTYPE *FillRect )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ const RECT *pRect,
            /* [in] */ DXPMSAMPLE SampVal,
            /* [in] */ BOOL bDoOver);
        
        void ( STDMETHODCALLTYPE *OverSample )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ const DXOVERSAMPLEDESC *pOverDesc);
        
        void ( STDMETHODCALLTYPE *OverArrayAndMove )( 
            IDXARGBReadWritePtr * This,
            /* [in] */ DXBASESAMPLE *pScratchBuffer,
            /* [in] */ const DXPMSAMPLE *pSrc,
            /* [in] */ ULONG cSamples);
        
        END_INTERFACE
    } IDXARGBReadWritePtrVtbl;

    interface IDXARGBReadWritePtr
    {
        CONST_VTBL struct IDXARGBReadWritePtrVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXARGBReadWritePtr_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXARGBReadWritePtr_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXARGBReadWritePtr_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXARGBReadWritePtr_GetSurface(This,riid,ppSurface)	\
    (This)->lpVtbl -> GetSurface(This,riid,ppSurface)

#define IDXARGBReadWritePtr_GetNativeType(This,pInfo)	\
    (This)->lpVtbl -> GetNativeType(This,pInfo)

#define IDXARGBReadWritePtr_Move(This,cSamples)	\
    (This)->lpVtbl -> Move(This,cSamples)

#define IDXARGBReadWritePtr_MoveToRow(This,y)	\
    (This)->lpVtbl -> MoveToRow(This,y)

#define IDXARGBReadWritePtr_MoveToXY(This,x,y)	\
    (This)->lpVtbl -> MoveToXY(This,x,y)

#define IDXARGBReadWritePtr_MoveAndGetRunInfo(This,Row,ppInfo)	\
    (This)->lpVtbl -> MoveAndGetRunInfo(This,Row,ppInfo)

#define IDXARGBReadWritePtr_Unpack(This,pSamples,cSamples,bMove)	\
    (This)->lpVtbl -> Unpack(This,pSamples,cSamples,bMove)

#define IDXARGBReadWritePtr_UnpackPremult(This,pSamples,cSamples,bMove)	\
    (This)->lpVtbl -> UnpackPremult(This,pSamples,cSamples,bMove)

#define IDXARGBReadWritePtr_UnpackRect(This,pRectDesc)	\
    (This)->lpVtbl -> UnpackRect(This,pRectDesc)


#define IDXARGBReadWritePtr_PackAndMove(This,pSamples,cSamples)	\
    (This)->lpVtbl -> PackAndMove(This,pSamples,cSamples)

#define IDXARGBReadWritePtr_PackPremultAndMove(This,pSamples,cSamples)	\
    (This)->lpVtbl -> PackPremultAndMove(This,pSamples,cSamples)

#define IDXARGBReadWritePtr_PackRect(This,pRectDesc)	\
    (This)->lpVtbl -> PackRect(This,pRectDesc)

#define IDXARGBReadWritePtr_CopyAndMoveBoth(This,pScratchBuffer,pSrc,cSamples,bIsOpaque)	\
    (This)->lpVtbl -> CopyAndMoveBoth(This,pScratchBuffer,pSrc,cSamples,bIsOpaque)

#define IDXARGBReadWritePtr_CopyRect(This,pScratchBuffer,pDestRect,pSrc,pSrcOrigin,bIsOpaque)	\
    (This)->lpVtbl -> CopyRect(This,pScratchBuffer,pDestRect,pSrc,pSrcOrigin,bIsOpaque)

#define IDXARGBReadWritePtr_FillAndMove(This,pScratchBuffer,SampVal,cSamples,bDoOver)	\
    (This)->lpVtbl -> FillAndMove(This,pScratchBuffer,SampVal,cSamples,bDoOver)

#define IDXARGBReadWritePtr_FillRect(This,pRect,SampVal,bDoOver)	\
    (This)->lpVtbl -> FillRect(This,pRect,SampVal,bDoOver)

#define IDXARGBReadWritePtr_OverSample(This,pOverDesc)	\
    (This)->lpVtbl -> OverSample(This,pOverDesc)

#define IDXARGBReadWritePtr_OverArrayAndMove(This,pScratchBuffer,pSrc,cSamples)	\
    (This)->lpVtbl -> OverArrayAndMove(This,pScratchBuffer,pSrc,cSamples)

#endif /* COBJMACROS */


#endif 	/* C style interface */



void STDMETHODCALLTYPE IDXARGBReadWritePtr_PackAndMove_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ const DXSAMPLE *pSamples,
    /* [in] */ ULONG cSamples);


void __RPC_STUB IDXARGBReadWritePtr_PackAndMove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_PackPremultAndMove_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ const DXPMSAMPLE *pSamples,
    /* [in] */ ULONG cSamples);


void __RPC_STUB IDXARGBReadWritePtr_PackPremultAndMove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_PackRect_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ const DXPACKEDRECTDESC *pRectDesc);


void __RPC_STUB IDXARGBReadWritePtr_PackRect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_CopyAndMoveBoth_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ DXBASESAMPLE *pScratchBuffer,
    /* [in] */ IDXARGBReadPtr *pSrc,
    /* [in] */ ULONG cSamples,
    /* [in] */ BOOL bIsOpaque);


void __RPC_STUB IDXARGBReadWritePtr_CopyAndMoveBoth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_CopyRect_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ DXBASESAMPLE *pScratchBuffer,
    /* [in] */ const RECT *pDestRect,
    /* [in] */ IDXARGBReadPtr *pSrc,
    /* [in] */ const POINT *pSrcOrigin,
    /* [in] */ BOOL bIsOpaque);


void __RPC_STUB IDXARGBReadWritePtr_CopyRect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_FillAndMove_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ DXBASESAMPLE *pScratchBuffer,
    /* [in] */ DXPMSAMPLE SampVal,
    /* [in] */ ULONG cSamples,
    /* [in] */ BOOL bDoOver);


void __RPC_STUB IDXARGBReadWritePtr_FillAndMove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_FillRect_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ const RECT *pRect,
    /* [in] */ DXPMSAMPLE SampVal,
    /* [in] */ BOOL bDoOver);


void __RPC_STUB IDXARGBReadWritePtr_FillRect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_OverSample_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ const DXOVERSAMPLEDESC *pOverDesc);


void __RPC_STUB IDXARGBReadWritePtr_OverSample_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


void STDMETHODCALLTYPE IDXARGBReadWritePtr_OverArrayAndMove_Proxy( 
    IDXARGBReadWritePtr * This,
    /* [in] */ DXBASESAMPLE *pScratchBuffer,
    /* [in] */ const DXPMSAMPLE *pSrc,
    /* [in] */ ULONG cSamples);


void __RPC_STUB IDXARGBReadWritePtr_OverArrayAndMove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXARGBReadWritePtr_INTERFACE_DEFINED__ */


#ifndef __IDXDCLock_INTERFACE_DEFINED__
#define __IDXDCLock_INTERFACE_DEFINED__

/* interface IDXDCLock */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXDCLock;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0F619456-CF39-11d1-905E-00C04FD9189D")
    IDXDCLock : public IUnknown
    {
    public:
        virtual HDC STDMETHODCALLTYPE GetDC( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXDCLockVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXDCLock * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXDCLock * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXDCLock * This);
        
        HDC ( STDMETHODCALLTYPE *GetDC )( 
            IDXDCLock * This);
        
        END_INTERFACE
    } IDXDCLockVtbl;

    interface IDXDCLock
    {
        CONST_VTBL struct IDXDCLockVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXDCLock_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXDCLock_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXDCLock_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXDCLock_GetDC(This)	\
    (This)->lpVtbl -> GetDC(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HDC STDMETHODCALLTYPE IDXDCLock_GetDC_Proxy( 
    IDXDCLock * This);


void __RPC_STUB IDXDCLock_GetDC_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXDCLock_INTERFACE_DEFINED__ */


#ifndef __IDXTScaleOutput_INTERFACE_DEFINED__
#define __IDXTScaleOutput_INTERFACE_DEFINED__

/* interface IDXTScaleOutput */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXTScaleOutput;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B2024B50-EE77-11d1-9066-00C04FD9189D")
    IDXTScaleOutput : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetOutputSize( 
            /* [in] */ const SIZE OutSize,
            /* [in] */ BOOL bMaintainAspect) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXTScaleOutputVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXTScaleOutput * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXTScaleOutput * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXTScaleOutput * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetOutputSize )( 
            IDXTScaleOutput * This,
            /* [in] */ const SIZE OutSize,
            /* [in] */ BOOL bMaintainAspect);
        
        END_INTERFACE
    } IDXTScaleOutputVtbl;

    interface IDXTScaleOutput
    {
        CONST_VTBL struct IDXTScaleOutputVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXTScaleOutput_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXTScaleOutput_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXTScaleOutput_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXTScaleOutput_SetOutputSize(This,OutSize,bMaintainAspect)	\
    (This)->lpVtbl -> SetOutputSize(This,OutSize,bMaintainAspect)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXTScaleOutput_SetOutputSize_Proxy( 
    IDXTScaleOutput * This,
    /* [in] */ const SIZE OutSize,
    /* [in] */ BOOL bMaintainAspect);


void __RPC_STUB IDXTScaleOutput_SetOutputSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXTScaleOutput_INTERFACE_DEFINED__ */


#ifndef __IDXGradient_INTERFACE_DEFINED__
#define __IDXGradient_INTERFACE_DEFINED__

/* interface IDXGradient */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXGradient;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B2024B51-EE77-11d1-9066-00C04FD9189D")
    IDXGradient : public IDXTScaleOutput
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetGradient( 
            DXSAMPLE StartColor,
            DXSAMPLE EndColor,
            BOOL bHorizontal) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetOutputSize( 
            /* [out] */ SIZE *pOutSize) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXGradientVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXGradient * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXGradient * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXGradient * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetOutputSize )( 
            IDXGradient * This,
            /* [in] */ const SIZE OutSize,
            /* [in] */ BOOL bMaintainAspect);
        
        HRESULT ( STDMETHODCALLTYPE *SetGradient )( 
            IDXGradient * This,
            DXSAMPLE StartColor,
            DXSAMPLE EndColor,
            BOOL bHorizontal);
        
        HRESULT ( STDMETHODCALLTYPE *GetOutputSize )( 
            IDXGradient * This,
            /* [out] */ SIZE *pOutSize);
        
        END_INTERFACE
    } IDXGradientVtbl;

    interface IDXGradient
    {
        CONST_VTBL struct IDXGradientVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXGradient_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXGradient_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXGradient_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXGradient_SetOutputSize(This,OutSize,bMaintainAspect)	\
    (This)->lpVtbl -> SetOutputSize(This,OutSize,bMaintainAspect)


#define IDXGradient_SetGradient(This,StartColor,EndColor,bHorizontal)	\
    (This)->lpVtbl -> SetGradient(This,StartColor,EndColor,bHorizontal)

#define IDXGradient_GetOutputSize(This,pOutSize)	\
    (This)->lpVtbl -> GetOutputSize(This,pOutSize)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXGradient_SetGradient_Proxy( 
    IDXGradient * This,
    DXSAMPLE StartColor,
    DXSAMPLE EndColor,
    BOOL bHorizontal);


void __RPC_STUB IDXGradient_SetGradient_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXGradient_GetOutputSize_Proxy( 
    IDXGradient * This,
    /* [out] */ SIZE *pOutSize);


void __RPC_STUB IDXGradient_GetOutputSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXGradient_INTERFACE_DEFINED__ */


#ifndef __IDXTScale_INTERFACE_DEFINED__
#define __IDXTScale_INTERFACE_DEFINED__

/* interface IDXTScale */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXTScale;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B39FD742-E139-11d1-9065-00C04FD9189D")
    IDXTScale : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetScales( 
            /* [in] */ float Scales[ 2 ]) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetScales( 
            /* [out] */ float Scales[ 2 ]) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ScaleFitToSize( 
            /* [out][in] */ DXBNDS *pClipBounds,
            /* [in] */ SIZE FitToSize,
            /* [in] */ BOOL bMaintainAspect) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXTScaleVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXTScale * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXTScale * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXTScale * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetScales )( 
            IDXTScale * This,
            /* [in] */ float Scales[ 2 ]);
        
        HRESULT ( STDMETHODCALLTYPE *GetScales )( 
            IDXTScale * This,
            /* [out] */ float Scales[ 2 ]);
        
        HRESULT ( STDMETHODCALLTYPE *ScaleFitToSize )( 
            IDXTScale * This,
            /* [out][in] */ DXBNDS *pClipBounds,
            /* [in] */ SIZE FitToSize,
            /* [in] */ BOOL bMaintainAspect);
        
        END_INTERFACE
    } IDXTScaleVtbl;

    interface IDXTScale
    {
        CONST_VTBL struct IDXTScaleVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXTScale_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXTScale_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXTScale_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXTScale_SetScales(This,Scales)	\
    (This)->lpVtbl -> SetScales(This,Scales)

#define IDXTScale_GetScales(This,Scales)	\
    (This)->lpVtbl -> GetScales(This,Scales)

#define IDXTScale_ScaleFitToSize(This,pClipBounds,FitToSize,bMaintainAspect)	\
    (This)->lpVtbl -> ScaleFitToSize(This,pClipBounds,FitToSize,bMaintainAspect)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXTScale_SetScales_Proxy( 
    IDXTScale * This,
    /* [in] */ float Scales[ 2 ]);


void __RPC_STUB IDXTScale_SetScales_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTScale_GetScales_Proxy( 
    IDXTScale * This,
    /* [out] */ float Scales[ 2 ]);


void __RPC_STUB IDXTScale_GetScales_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXTScale_ScaleFitToSize_Proxy( 
    IDXTScale * This,
    /* [out][in] */ DXBNDS *pClipBounds,
    /* [in] */ SIZE FitToSize,
    /* [in] */ BOOL bMaintainAspect);


void __RPC_STUB IDXTScale_ScaleFitToSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXTScale_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0269 */
/* [local] */ 

typedef 
enum DISPIDDXEFFECT
    {	DISPID_DXECAPABILITIES	= 10000,
	DISPID_DXEPROGRESS	= DISPID_DXECAPABILITIES + 1,
	DISPID_DXESTEP	= DISPID_DXEPROGRESS + 1,
	DISPID_DXEDURATION	= DISPID_DXESTEP + 1,
	DISPID_DXE_NEXT_ID	= DISPID_DXEDURATION + 1
    } 	DISPIDDXBOUNDEDEFFECT;

typedef 
enum DXEFFECTTYPE
    {	DXTET_PERIODIC	= 1 << 0,
	DXTET_MORPH	= 1 << 1
    } 	DXEFFECTTYPE;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0269_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0269_v0_0_s_ifspec;

#ifndef __IDXEffect_INTERFACE_DEFINED__
#define __IDXEffect_INTERFACE_DEFINED__

/* interface IDXEffect */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IDXEffect;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E31FB81B-1335-11d1-8189-0000F87557DB")
    IDXEffect : public IDispatch
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Capabilities( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Progress( 
            /* [retval][out] */ float *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Progress( 
            /* [in] */ float newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_StepResolution( 
            /* [retval][out] */ float *pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Duration( 
            /* [retval][out] */ float *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Duration( 
            /* [in] */ float newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXEffectVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXEffect * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXEffect * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXEffect * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDXEffect * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDXEffect * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDXEffect * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDXEffect * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Capabilities )( 
            IDXEffect * This,
            /* [retval][out] */ long *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Progress )( 
            IDXEffect * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Progress )( 
            IDXEffect * This,
            /* [in] */ float newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StepResolution )( 
            IDXEffect * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            IDXEffect * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Duration )( 
            IDXEffect * This,
            /* [in] */ float newVal);
        
        END_INTERFACE
    } IDXEffectVtbl;

    interface IDXEffect
    {
        CONST_VTBL struct IDXEffectVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXEffect_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXEffect_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXEffect_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXEffect_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IDXEffect_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IDXEffect_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IDXEffect_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IDXEffect_get_Capabilities(This,pVal)	\
    (This)->lpVtbl -> get_Capabilities(This,pVal)

#define IDXEffect_get_Progress(This,pVal)	\
    (This)->lpVtbl -> get_Progress(This,pVal)

#define IDXEffect_put_Progress(This,newVal)	\
    (This)->lpVtbl -> put_Progress(This,newVal)

#define IDXEffect_get_StepResolution(This,pVal)	\
    (This)->lpVtbl -> get_StepResolution(This,pVal)

#define IDXEffect_get_Duration(This,pVal)	\
    (This)->lpVtbl -> get_Duration(This,pVal)

#define IDXEffect_put_Duration(This,newVal)	\
    (This)->lpVtbl -> put_Duration(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDXEffect_get_Capabilities_Proxy( 
    IDXEffect * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDXEffect_get_Capabilities_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDXEffect_get_Progress_Proxy( 
    IDXEffect * This,
    /* [retval][out] */ float *pVal);


void __RPC_STUB IDXEffect_get_Progress_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IDXEffect_put_Progress_Proxy( 
    IDXEffect * This,
    /* [in] */ float newVal);


void __RPC_STUB IDXEffect_put_Progress_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDXEffect_get_StepResolution_Proxy( 
    IDXEffect * This,
    /* [retval][out] */ float *pVal);


void __RPC_STUB IDXEffect_get_StepResolution_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDXEffect_get_Duration_Proxy( 
    IDXEffect * This,
    /* [retval][out] */ float *pVal);


void __RPC_STUB IDXEffect_get_Duration_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IDXEffect_put_Duration_Proxy( 
    IDXEffect * This,
    /* [in] */ float newVal);


void __RPC_STUB IDXEffect_put_Duration_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXEffect_INTERFACE_DEFINED__ */


#ifndef __IDXLookupTable_INTERFACE_DEFINED__
#define __IDXLookupTable_INTERFACE_DEFINED__

/* interface IDXLookupTable */
/* [object][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXLookupTable;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("01BAFC7F-9E63-11d1-9053-00C04FD9189D")
    IDXLookupTable : public IDXBaseObject
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetTables( 
            /* [out] */ BYTE RedLUT[ 256 ],
            /* [out] */ BYTE GreenLUT[ 256 ],
            /* [out] */ BYTE BlueLUT[ 256 ],
            /* [out] */ BYTE AlphaLUT[ 256 ]) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IsChannelIdentity( 
            /* [out] */ DXBASESAMPLE *pSampleBools) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetIndexValues( 
            /* [in] */ ULONG Index,
            /* [out] */ DXBASESAMPLE *pSample) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ApplyTables( 
            /* [out][in] */ DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXLookupTableVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXLookupTable * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXLookupTable * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXLookupTable * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetGenerationId )( 
            IDXLookupTable * This,
            /* [out] */ ULONG *pID);
        
        HRESULT ( STDMETHODCALLTYPE *IncrementGenerationId )( 
            IDXLookupTable * This,
            /* [in] */ BOOL bRefresh);
        
        HRESULT ( STDMETHODCALLTYPE *GetObjectSize )( 
            IDXLookupTable * This,
            /* [out] */ ULONG *pcbSize);
        
        HRESULT ( STDMETHODCALLTYPE *GetTables )( 
            IDXLookupTable * This,
            /* [out] */ BYTE RedLUT[ 256 ],
            /* [out] */ BYTE GreenLUT[ 256 ],
            /* [out] */ BYTE BlueLUT[ 256 ],
            /* [out] */ BYTE AlphaLUT[ 256 ]);
        
        HRESULT ( STDMETHODCALLTYPE *IsChannelIdentity )( 
            IDXLookupTable * This,
            /* [out] */ DXBASESAMPLE *pSampleBools);
        
        HRESULT ( STDMETHODCALLTYPE *GetIndexValues )( 
            IDXLookupTable * This,
            /* [in] */ ULONG Index,
            /* [out] */ DXBASESAMPLE *pSample);
        
        HRESULT ( STDMETHODCALLTYPE *ApplyTables )( 
            IDXLookupTable * This,
            /* [out][in] */ DXSAMPLE *pSamples,
            /* [in] */ ULONG cSamples);
        
        END_INTERFACE
    } IDXLookupTableVtbl;

    interface IDXLookupTable
    {
        CONST_VTBL struct IDXLookupTableVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXLookupTable_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXLookupTable_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXLookupTable_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXLookupTable_GetGenerationId(This,pID)	\
    (This)->lpVtbl -> GetGenerationId(This,pID)

#define IDXLookupTable_IncrementGenerationId(This,bRefresh)	\
    (This)->lpVtbl -> IncrementGenerationId(This,bRefresh)

#define IDXLookupTable_GetObjectSize(This,pcbSize)	\
    (This)->lpVtbl -> GetObjectSize(This,pcbSize)


#define IDXLookupTable_GetTables(This,RedLUT,GreenLUT,BlueLUT,AlphaLUT)	\
    (This)->lpVtbl -> GetTables(This,RedLUT,GreenLUT,BlueLUT,AlphaLUT)

#define IDXLookupTable_IsChannelIdentity(This,pSampleBools)	\
    (This)->lpVtbl -> IsChannelIdentity(This,pSampleBools)

#define IDXLookupTable_GetIndexValues(This,Index,pSample)	\
    (This)->lpVtbl -> GetIndexValues(This,Index,pSample)

#define IDXLookupTable_ApplyTables(This,pSamples,cSamples)	\
    (This)->lpVtbl -> ApplyTables(This,pSamples,cSamples)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXLookupTable_GetTables_Proxy( 
    IDXLookupTable * This,
    /* [out] */ BYTE RedLUT[ 256 ],
    /* [out] */ BYTE GreenLUT[ 256 ],
    /* [out] */ BYTE BlueLUT[ 256 ],
    /* [out] */ BYTE AlphaLUT[ 256 ]);


void __RPC_STUB IDXLookupTable_GetTables_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXLookupTable_IsChannelIdentity_Proxy( 
    IDXLookupTable * This,
    /* [out] */ DXBASESAMPLE *pSampleBools);


void __RPC_STUB IDXLookupTable_IsChannelIdentity_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXLookupTable_GetIndexValues_Proxy( 
    IDXLookupTable * This,
    /* [in] */ ULONG Index,
    /* [out] */ DXBASESAMPLE *pSample);


void __RPC_STUB IDXLookupTable_GetIndexValues_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDXLookupTable_ApplyTables_Proxy( 
    IDXLookupTable * This,
    /* [out][in] */ DXSAMPLE *pSamples,
    /* [in] */ ULONG cSamples);


void __RPC_STUB IDXLookupTable_ApplyTables_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXLookupTable_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_dxtrans_0271 */
/* [local] */ 

typedef struct DXRAWSURFACEINFO
    {
    BYTE *pFirstByte;
    long lPitch;
    ULONG Width;
    ULONG Height;
    const GUID *pPixelFormat;
    HDC hdc;
    DWORD dwColorKey;
    DXBASESAMPLE *pPalette;
    } 	DXRAWSURFACEINFO;



extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0271_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_dxtrans_0271_v0_0_s_ifspec;

#ifndef __IDXRawSurface_INTERFACE_DEFINED__
#define __IDXRawSurface_INTERFACE_DEFINED__

/* interface IDXRawSurface */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IDXRawSurface;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("09756C8A-D96A-11d1-9062-00C04FD9189D")
    IDXRawSurface : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetSurfaceInfo( 
            DXRAWSURFACEINFO *pSurfaceInfo) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDXRawSurfaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDXRawSurface * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDXRawSurface * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDXRawSurface * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetSurfaceInfo )( 
            IDXRawSurface * This,
            DXRAWSURFACEINFO *pSurfaceInfo);
        
        END_INTERFACE
    } IDXRawSurfaceVtbl;

    interface IDXRawSurface
    {
        CONST_VTBL struct IDXRawSurfaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDXRawSurface_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDXRawSurface_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDXRawSurface_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDXRawSurface_GetSurfaceInfo(This,pSurfaceInfo)	\
    (This)->lpVtbl -> GetSurfaceInfo(This,pSurfaceInfo)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IDXRawSurface_GetSurfaceInfo_Proxy( 
    IDXRawSurface * This,
    DXRAWSURFACEINFO *pSurfaceInfo);


void __RPC_STUB IDXRawSurface_GetSurfaceInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDXRawSurface_INTERFACE_DEFINED__ */


#ifndef __IHTMLDXTransform_INTERFACE_DEFINED__
#define __IHTMLDXTransform_INTERFACE_DEFINED__

/* interface IHTMLDXTransform */
/* [object][local][unique][helpstring][uuid] */ 


EXTERN_C const IID IID_IHTMLDXTransform;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("30E2AB7D-4FDD-4159-B7EA-DC722BF4ADE5")
    IHTMLDXTransform : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetHostUrl( 
            BSTR bstrHostUrl) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHTMLDXTransformVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IHTMLDXTransform * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IHTMLDXTransform * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IHTMLDXTransform * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetHostUrl )( 
            IHTMLDXTransform * This,
            BSTR bstrHostUrl);
        
        END_INTERFACE
    } IHTMLDXTransformVtbl;

    interface IHTMLDXTransform
    {
        CONST_VTBL struct IHTMLDXTransformVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHTMLDXTransform_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IHTMLDXTransform_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IHTMLDXTransform_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IHTMLDXTransform_SetHostUrl(This,bstrHostUrl)	\
    (This)->lpVtbl -> SetHostUrl(This,bstrHostUrl)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IHTMLDXTransform_SetHostUrl_Proxy( 
    IHTMLDXTransform * This,
    BSTR bstrHostUrl);


void __RPC_STUB IHTMLDXTransform_SetHostUrl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IHTMLDXTransform_INTERFACE_DEFINED__ */



#ifndef __DXTRANSLib_LIBRARY_DEFINED__
#define __DXTRANSLib_LIBRARY_DEFINED__

/* library DXTRANSLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_DXTRANSLib;

EXTERN_C const CLSID CLSID_DXTransformFactory;

#ifdef __cplusplus

class DECLSPEC_UUID("D1FE6762-FC48-11D0-883A-3C8B00C10000")
DXTransformFactory;
#endif

EXTERN_C const CLSID CLSID_DXTaskManager;

#ifdef __cplusplus

class DECLSPEC_UUID("4CB26C03-FF93-11d0-817E-0000F87557DB")
DXTaskManager;
#endif

EXTERN_C const CLSID CLSID_DXTScale;

#ifdef __cplusplus

class DECLSPEC_UUID("555278E2-05DB-11D1-883A-3C8B00C10000")
DXTScale;
#endif

EXTERN_C const CLSID CLSID_DXSurface;

#ifdef __cplusplus

class DECLSPEC_UUID("0E890F83-5F79-11D1-9043-00C04FD9189D")
DXSurface;
#endif

EXTERN_C const CLSID CLSID_DXSurfaceModifier;

#ifdef __cplusplus

class DECLSPEC_UUID("3E669F1D-9C23-11d1-9053-00C04FD9189D")
DXSurfaceModifier;
#endif

EXTERN_C const CLSID CLSID_DXGradient;

#ifdef __cplusplus

class DECLSPEC_UUID("C6365470-F667-11d1-9067-00C04FD9189D")
DXGradient;
#endif
#endif /* __DXTRANSLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif

#include "amstream.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

/* interface __MIDL_itf_qedit_0000 */
/* [local] */ 










typedef /* [public] */ 
enum __MIDL___MIDL_itf_qedit_0000_0001
    {	DEXTERF_JUMP	= 0,
	DEXTERF_INTERPOLATE	= DEXTERF_JUMP + 1
    } 	DEXTERF;

typedef /* [public][public][public][public] */ struct __MIDL___MIDL_itf_qedit_0000_0002
    {
    BSTR Name;
    DISPID dispID;
    LONG nValues;
    } 	DEXTER_PARAM;

typedef /* [public][public][public][public] */ struct __MIDL___MIDL_itf_qedit_0000_0003
    {
    VARIANT v;
    REFERENCE_TIME rt;
    DWORD dwInterp;
    } 	DEXTER_VALUE;


enum __MIDL___MIDL_itf_qedit_0000_0004
    {	DEXTER_AUDIO_JUMP	= 0,
	DEXTER_AUDIO_INTERPOLATE	= DEXTER_AUDIO_JUMP + 1
    } ;
typedef /* [public] */ struct __MIDL___MIDL_itf_qedit_0000_0005
    {
    REFERENCE_TIME rtEnd;
    double dLevel;
    BOOL bMethod;
    } 	DEXTER_AUDIO_VOLUMEENVELOPE;


enum __MIDL___MIDL_itf_qedit_0000_0006
    {	TIMELINE_INSERT_MODE_INSERT	= 1,
	TIMELINE_INSERT_MODE_OVERLAY	= 2
    } ;
typedef /* [public][public][public][public][public][public][public][public] */ 
enum __MIDL___MIDL_itf_qedit_0000_0007
    {	TIMELINE_MAJOR_TYPE_COMPOSITE	= 1,
	TIMELINE_MAJOR_TYPE_TRACK	= 2,
	TIMELINE_MAJOR_TYPE_SOURCE	= 4,
	TIMELINE_MAJOR_TYPE_TRANSITION	= 8,
	TIMELINE_MAJOR_TYPE_EFFECT	= 16,
	TIMELINE_MAJOR_TYPE_GROUP	= 128
    } 	TIMELINE_MAJOR_TYPE;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_qedit_0000_0008
    {	DEXTERF_BOUNDING	= -1,
	DEXTERF_EXACTLY_AT	= 0,
	DEXTERF_FORWARDS	= 1
    } 	DEXTERF_TRACK_SEARCH_FLAGS;

typedef struct _SCompFmt0
    {
    long nFormatId;
    AM_MEDIA_TYPE MediaType;
    } 	SCompFmt0;


enum __MIDL___MIDL_itf_qedit_0000_0009
    {	RESIZEF_STRETCH	= 0,
	RESIZEF_CROP	= RESIZEF_STRETCH + 1,
	RESIZEF_PRESERVEASPECTRATIO	= RESIZEF_CROP + 1,
	RESIZEF_PRESERVEASPECTRATIO_NOLETTERBOX	= RESIZEF_PRESERVEASPECTRATIO + 1
    } ;

enum __MIDL___MIDL_itf_qedit_0000_0010
    {	CONNECTF_DYNAMIC_NONE	= 0,
	CONNECTF_DYNAMIC_SOURCES	= 0x1,
	CONNECTF_DYNAMIC_EFFECTS	= 0x2
    } ;

enum __MIDL___MIDL_itf_qedit_0000_0011
    {	SFN_VALIDATEF_CHECK	= 0x1,
	SFN_VALIDATEF_POPUP	= 0x2,
	SFN_VALIDATEF_TELLME	= 0x4,
	SFN_VALIDATEF_REPLACE	= 0x8,
	SFN_VALIDATEF_USELOCAL	= 0x10,
	SFN_VALIDATEF_NOFIND	= 0x20,
	SFN_VALIDATEF_IGNOREMUTED	= 0x40,
	SFN_VALIDATEF_END	= SFN_VALIDATEF_IGNOREMUTED + 1
    } ;

enum __MIDL___MIDL_itf_qedit_0000_0012
    {	DXTKEY_RGB	= 0,
	DXTKEY_NONRED	= DXTKEY_RGB + 1,
	DXTKEY_LUMINANCE	= DXTKEY_NONRED + 1,
	DXTKEY_ALPHA	= DXTKEY_LUMINANCE + 1,
	DXTKEY_HUE	= DXTKEY_ALPHA + 1
    } ;


extern RPC_IF_HANDLE __MIDL_itf_qedit_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_qedit_0000_v0_0_s_ifspec;

#ifndef __IPropertySetter_INTERFACE_DEFINED__
#define __IPropertySetter_INTERFACE_DEFINED__

/* interface IPropertySetter */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IPropertySetter;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("AE9472BD-B0C3-11D2-8D24-00A0C9441E20")
    IPropertySetter : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE LoadXML( 
            /* [in] */ IUnknown *pxml) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE PrintXML( 
            /* [out] */ char *pszXML,
            /* [in] */ int cbXML,
            /* [out] */ int *pcbPrinted,
            /* [in] */ int indent) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE CloneProps( 
            /* [out] */ IPropertySetter **ppSetter,
            /* [in] */ REFERENCE_TIME rtStart,
            /* [in] */ REFERENCE_TIME rtStop) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE AddProp( 
            /* [in] */ DEXTER_PARAM Param,
            /* [in] */ DEXTER_VALUE *paValue) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetProps( 
            /* [out] */ LONG *pcParams,
            /* [out] */ DEXTER_PARAM **paParam,
            /* [out] */ DEXTER_VALUE **paValue) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE FreeProps( 
            /* [in] */ LONG cParams,
            /* [in] */ DEXTER_PARAM *paParam,
            /* [in] */ DEXTER_VALUE *paValue) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ClearProps( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SaveToBlob( 
            /* [out] */ LONG *pcSize,
            /* [out] */ BYTE **ppb) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE LoadFromBlob( 
            /* [in] */ LONG cSize,
            /* [in] */ BYTE *pb) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetProps( 
            /* [in] */ IUnknown *pTarget,
            /* [in] */ REFERENCE_TIME rtNow) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPropertySetterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IPropertySetter * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IPropertySetter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IPropertySetter * This);
        
        HRESULT ( STDMETHODCALLTYPE *LoadXML )( 
            IPropertySetter * This,
            /* [in] */ IUnknown *pxml);
        
        HRESULT ( STDMETHODCALLTYPE *PrintXML )( 
            IPropertySetter * This,
            /* [out] */ char *pszXML,
            /* [in] */ int cbXML,
            /* [out] */ int *pcbPrinted,
            /* [in] */ int indent);
        
        HRESULT ( STDMETHODCALLTYPE *CloneProps )( 
            IPropertySetter * This,
            /* [out] */ IPropertySetter **ppSetter,
            /* [in] */ REFERENCE_TIME rtStart,
            /* [in] */ REFERENCE_TIME rtStop);
        
        HRESULT ( STDMETHODCALLTYPE *AddProp )( 
            IPropertySetter * This,
            /* [in] */ DEXTER_PARAM Param,
            /* [in] */ DEXTER_VALUE *paValue);
        
        HRESULT ( STDMETHODCALLTYPE *GetProps )( 
            IPropertySetter * This,
            /* [out] */ LONG *pcParams,
            /* [out] */ DEXTER_PARAM **paParam,
            /* [out] */ DEXTER_VALUE **paValue);
        
        HRESULT ( STDMETHODCALLTYPE *FreeProps )( 
            IPropertySetter * This,
            /* [in] */ LONG cParams,
            /* [in] */ DEXTER_PARAM *paParam,
            /* [in] */ DEXTER_VALUE *paValue);
        
        HRESULT ( STDMETHODCALLTYPE *ClearProps )( 
            IPropertySetter * This);
        
        HRESULT ( STDMETHODCALLTYPE *SaveToBlob )( 
            IPropertySetter * This,
            /* [out] */ LONG *pcSize,
            /* [out] */ BYTE **ppb);
        
        HRESULT ( STDMETHODCALLTYPE *LoadFromBlob )( 
            IPropertySetter * This,
            /* [in] */ LONG cSize,
            /* [in] */ BYTE *pb);
        
        HRESULT ( STDMETHODCALLTYPE *SetProps )( 
            IPropertySetter * This,
            /* [in] */ IUnknown *pTarget,
            /* [in] */ REFERENCE_TIME rtNow);
        
        END_INTERFACE
    } IPropertySetterVtbl;

    interface IPropertySetter
    {
        CONST_VTBL struct IPropertySetterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPropertySetter_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPropertySetter_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPropertySetter_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPropertySetter_LoadXML(This,pxml)	\
    (This)->lpVtbl -> LoadXML(This,pxml)

#define IPropertySetter_PrintXML(This,pszXML,cbXML,pcbPrinted,indent)	\
    (This)->lpVtbl -> PrintXML(This,pszXML,cbXML,pcbPrinted,indent)

#define IPropertySetter_CloneProps(This,ppSetter,rtStart,rtStop)	\
    (This)->lpVtbl -> CloneProps(This,ppSetter,rtStart,rtStop)

#define IPropertySetter_AddProp(This,Param,paValue)	\
    (This)->lpVtbl -> AddProp(This,Param,paValue)

#define IPropertySetter_GetProps(This,pcParams,paParam,paValue)	\
    (This)->lpVtbl -> GetProps(This,pcParams,paParam,paValue)

#define IPropertySetter_FreeProps(This,cParams,paParam,paValue)	\
    (This)->lpVtbl -> FreeProps(This,cParams,paParam,paValue)

#define IPropertySetter_ClearProps(This)	\
    (This)->lpVtbl -> ClearProps(This)

#define IPropertySetter_SaveToBlob(This,pcSize,ppb)	\
    (This)->lpVtbl -> SaveToBlob(This,pcSize,ppb)

#define IPropertySetter_LoadFromBlob(This,cSize,pb)	\
    (This)->lpVtbl -> LoadFromBlob(This,cSize,pb)

#define IPropertySetter_SetProps(This,pTarget,rtNow)	\
    (This)->lpVtbl -> SetProps(This,pTarget,rtNow)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IPropertySetter_LoadXML_Proxy( 
    IPropertySetter * This,
    /* [in] */ IUnknown *pxml);


void __RPC_STUB IPropertySetter_LoadXML_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IPropertySetter_PrintXML_Proxy( 
    IPropertySetter * This,
    /* [out] */ char *pszXML,
    /* [in] */ int cbXML,
    /* [out] */ int *pcbPrinted,
    /* [in] */ int indent);


void __RPC_STUB IPropertySetter_PrintXML_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IPropertySetter_CloneProps_Proxy( 
    IPropertySetter * This,
    /* [out] */ IPropertySetter **ppSetter,
    /* [in] */ REFERENCE_TIME rtStart,
    /* [in] */ REFERENCE_TIME rtStop);


void __RPC_STUB IPropertySetter_CloneProps_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IPropertySetter_AddProp_Proxy( 
    IPropertySetter * This,
    /* [in] */ DEXTER_PARAM Param,
    /* [in] */ DEXTER_VALUE *paValue);


void __RPC_STUB IPropertySetter_AddProp_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IPropertySetter_GetProps_Proxy( 
    IPropertySetter * This,
    /* [out] */ LONG *pcParams,
    /* [out] */ DEXTER_PARAM **paParam,
    /* [out] */ DEXTER_VALUE **paValue);


void __RPC_STUB IPropertySetter_GetProps_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IPropertySetter_FreeProps_Proxy( 
    IPropertySetter * This,
    /* [in] */ LONG cParams,
    /* [in] */ DEXTER_PARAM *paParam,
    /* [in] */ DEXTER_VALUE *paValue);


void __RPC_STUB IPropertySetter_FreeProps_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IPropertySetter_ClearProps_Proxy( 
    IPropertySetter * This);


void __RPC_STUB IPropertySetter_ClearProps_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IPropertySetter_SaveToBlob_Proxy( 
    IPropertySetter * This,
    /* [out] */ LONG *pcSize,
    /* [out] */ BYTE **ppb);


void __RPC_STUB IPropertySetter_SaveToBlob_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IPropertySetter_LoadFromBlob_Proxy( 
    IPropertySetter * This,
    /* [in] */ LONG cSize,
    /* [in] */ BYTE *pb);


void __RPC_STUB IPropertySetter_LoadFromBlob_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IPropertySetter_SetProps_Proxy( 
    IPropertySetter * This,
    /* [in] */ IUnknown *pTarget,
    /* [in] */ REFERENCE_TIME rtNow);


void __RPC_STUB IPropertySetter_SetProps_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPropertySetter_INTERFACE_DEFINED__ */


#ifndef __IDxtCompositor_INTERFACE_DEFINED__
#define __IDxtCompositor_INTERFACE_DEFINED__

/* interface IDxtCompositor */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IDxtCompositor;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("BB44391E-6ABD-422f-9E2E-385C9DFF51FC")
    IDxtCompositor : public IDXEffect
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OffsetX( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OffsetX( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OffsetY( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OffsetY( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Width( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Width( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Height( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Height( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SrcOffsetX( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SrcOffsetX( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SrcOffsetY( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SrcOffsetY( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SrcWidth( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SrcWidth( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SrcHeight( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SrcHeight( 
            /* [in] */ long newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDxtCompositorVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDxtCompositor * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDxtCompositor * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDxtCompositor * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDxtCompositor * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDxtCompositor * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDxtCompositor * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDxtCompositor * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Capabilities )( 
            IDxtCompositor * This,
            /* [retval][out] */ long *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Progress )( 
            IDxtCompositor * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Progress )( 
            IDxtCompositor * This,
            /* [in] */ float newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StepResolution )( 
            IDxtCompositor * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            IDxtCompositor * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Duration )( 
            IDxtCompositor * This,
            /* [in] */ float newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OffsetX )( 
            IDxtCompositor * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OffsetX )( 
            IDxtCompositor * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OffsetY )( 
            IDxtCompositor * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OffsetY )( 
            IDxtCompositor * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Width )( 
            IDxtCompositor * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Width )( 
            IDxtCompositor * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Height )( 
            IDxtCompositor * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Height )( 
            IDxtCompositor * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SrcOffsetX )( 
            IDxtCompositor * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SrcOffsetX )( 
            IDxtCompositor * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SrcOffsetY )( 
            IDxtCompositor * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SrcOffsetY )( 
            IDxtCompositor * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SrcWidth )( 
            IDxtCompositor * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SrcWidth )( 
            IDxtCompositor * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SrcHeight )( 
            IDxtCompositor * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SrcHeight )( 
            IDxtCompositor * This,
            /* [in] */ long newVal);
        
        END_INTERFACE
    } IDxtCompositorVtbl;

    interface IDxtCompositor
    {
        CONST_VTBL struct IDxtCompositorVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDxtCompositor_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDxtCompositor_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDxtCompositor_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDxtCompositor_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IDxtCompositor_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IDxtCompositor_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IDxtCompositor_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IDxtCompositor_get_Capabilities(This,pVal)	\
    (This)->lpVtbl -> get_Capabilities(This,pVal)

#define IDxtCompositor_get_Progress(This,pVal)	\
    (This)->lpVtbl -> get_Progress(This,pVal)

#define IDxtCompositor_put_Progress(This,newVal)	\
    (This)->lpVtbl -> put_Progress(This,newVal)

#define IDxtCompositor_get_StepResolution(This,pVal)	\
    (This)->lpVtbl -> get_StepResolution(This,pVal)

#define IDxtCompositor_get_Duration(This,pVal)	\
    (This)->lpVtbl -> get_Duration(This,pVal)

#define IDxtCompositor_put_Duration(This,newVal)	\
    (This)->lpVtbl -> put_Duration(This,newVal)


#define IDxtCompositor_get_OffsetX(This,pVal)	\
    (This)->lpVtbl -> get_OffsetX(This,pVal)

#define IDxtCompositor_put_OffsetX(This,newVal)	\
    (This)->lpVtbl -> put_OffsetX(This,newVal)

#define IDxtCompositor_get_OffsetY(This,pVal)	\
    (This)->lpVtbl -> get_OffsetY(This,pVal)

#define IDxtCompositor_put_OffsetY(This,newVal)	\
    (This)->lpVtbl -> put_OffsetY(This,newVal)

#define IDxtCompositor_get_Width(This,pVal)	\
    (This)->lpVtbl -> get_Width(This,pVal)

#define IDxtCompositor_put_Width(This,newVal)	\
    (This)->lpVtbl -> put_Width(This,newVal)

#define IDxtCompositor_get_Height(This,pVal)	\
    (This)->lpVtbl -> get_Height(This,pVal)

#define IDxtCompositor_put_Height(This,newVal)	\
    (This)->lpVtbl -> put_Height(This,newVal)

#define IDxtCompositor_get_SrcOffsetX(This,pVal)	\
    (This)->lpVtbl -> get_SrcOffsetX(This,pVal)

#define IDxtCompositor_put_SrcOffsetX(This,newVal)	\
    (This)->lpVtbl -> put_SrcOffsetX(This,newVal)

#define IDxtCompositor_get_SrcOffsetY(This,pVal)	\
    (This)->lpVtbl -> get_SrcOffsetY(This,pVal)

#define IDxtCompositor_put_SrcOffsetY(This,newVal)	\
    (This)->lpVtbl -> put_SrcOffsetY(This,newVal)

#define IDxtCompositor_get_SrcWidth(This,pVal)	\
    (This)->lpVtbl -> get_SrcWidth(This,pVal)

#define IDxtCompositor_put_SrcWidth(This,newVal)	\
    (This)->lpVtbl -> put_SrcWidth(This,newVal)

#define IDxtCompositor_get_SrcHeight(This,pVal)	\
    (This)->lpVtbl -> get_SrcHeight(This,pVal)

#define IDxtCompositor_put_SrcHeight(This,newVal)	\
    (This)->lpVtbl -> put_SrcHeight(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_get_OffsetX_Proxy( 
    IDxtCompositor * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtCompositor_get_OffsetX_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_put_OffsetX_Proxy( 
    IDxtCompositor * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtCompositor_put_OffsetX_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_get_OffsetY_Proxy( 
    IDxtCompositor * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtCompositor_get_OffsetY_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_put_OffsetY_Proxy( 
    IDxtCompositor * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtCompositor_put_OffsetY_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_get_Width_Proxy( 
    IDxtCompositor * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtCompositor_get_Width_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_put_Width_Proxy( 
    IDxtCompositor * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtCompositor_put_Width_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_get_Height_Proxy( 
    IDxtCompositor * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtCompositor_get_Height_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_put_Height_Proxy( 
    IDxtCompositor * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtCompositor_put_Height_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_get_SrcOffsetX_Proxy( 
    IDxtCompositor * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtCompositor_get_SrcOffsetX_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_put_SrcOffsetX_Proxy( 
    IDxtCompositor * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtCompositor_put_SrcOffsetX_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_get_SrcOffsetY_Proxy( 
    IDxtCompositor * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtCompositor_get_SrcOffsetY_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_put_SrcOffsetY_Proxy( 
    IDxtCompositor * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtCompositor_put_SrcOffsetY_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_get_SrcWidth_Proxy( 
    IDxtCompositor * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtCompositor_get_SrcWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_put_SrcWidth_Proxy( 
    IDxtCompositor * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtCompositor_put_SrcWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_get_SrcHeight_Proxy( 
    IDxtCompositor * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtCompositor_get_SrcHeight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtCompositor_put_SrcHeight_Proxy( 
    IDxtCompositor * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtCompositor_put_SrcHeight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDxtCompositor_INTERFACE_DEFINED__ */


#ifndef __IDxtAlphaSetter_INTERFACE_DEFINED__
#define __IDxtAlphaSetter_INTERFACE_DEFINED__

/* interface IDxtAlphaSetter */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IDxtAlphaSetter;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4EE9EAD9-DA4D-43d0-9383-06B90C08B12B")
    IDxtAlphaSetter : public IDXEffect
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Alpha( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Alpha( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AlphaRamp( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AlphaRamp( 
            /* [in] */ double newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDxtAlphaSetterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDxtAlphaSetter * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDxtAlphaSetter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDxtAlphaSetter * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDxtAlphaSetter * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDxtAlphaSetter * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDxtAlphaSetter * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDxtAlphaSetter * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Capabilities )( 
            IDxtAlphaSetter * This,
            /* [retval][out] */ long *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Progress )( 
            IDxtAlphaSetter * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Progress )( 
            IDxtAlphaSetter * This,
            /* [in] */ float newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StepResolution )( 
            IDxtAlphaSetter * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            IDxtAlphaSetter * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Duration )( 
            IDxtAlphaSetter * This,
            /* [in] */ float newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Alpha )( 
            IDxtAlphaSetter * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Alpha )( 
            IDxtAlphaSetter * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AlphaRamp )( 
            IDxtAlphaSetter * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AlphaRamp )( 
            IDxtAlphaSetter * This,
            /* [in] */ double newVal);
        
        END_INTERFACE
    } IDxtAlphaSetterVtbl;

    interface IDxtAlphaSetter
    {
        CONST_VTBL struct IDxtAlphaSetterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDxtAlphaSetter_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDxtAlphaSetter_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDxtAlphaSetter_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDxtAlphaSetter_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IDxtAlphaSetter_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IDxtAlphaSetter_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IDxtAlphaSetter_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IDxtAlphaSetter_get_Capabilities(This,pVal)	\
    (This)->lpVtbl -> get_Capabilities(This,pVal)

#define IDxtAlphaSetter_get_Progress(This,pVal)	\
    (This)->lpVtbl -> get_Progress(This,pVal)

#define IDxtAlphaSetter_put_Progress(This,newVal)	\
    (This)->lpVtbl -> put_Progress(This,newVal)

#define IDxtAlphaSetter_get_StepResolution(This,pVal)	\
    (This)->lpVtbl -> get_StepResolution(This,pVal)

#define IDxtAlphaSetter_get_Duration(This,pVal)	\
    (This)->lpVtbl -> get_Duration(This,pVal)

#define IDxtAlphaSetter_put_Duration(This,newVal)	\
    (This)->lpVtbl -> put_Duration(This,newVal)


#define IDxtAlphaSetter_get_Alpha(This,pVal)	\
    (This)->lpVtbl -> get_Alpha(This,pVal)

#define IDxtAlphaSetter_put_Alpha(This,newVal)	\
    (This)->lpVtbl -> put_Alpha(This,newVal)

#define IDxtAlphaSetter_get_AlphaRamp(This,pVal)	\
    (This)->lpVtbl -> get_AlphaRamp(This,pVal)

#define IDxtAlphaSetter_put_AlphaRamp(This,newVal)	\
    (This)->lpVtbl -> put_AlphaRamp(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtAlphaSetter_get_Alpha_Proxy( 
    IDxtAlphaSetter * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtAlphaSetter_get_Alpha_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtAlphaSetter_put_Alpha_Proxy( 
    IDxtAlphaSetter * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtAlphaSetter_put_Alpha_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtAlphaSetter_get_AlphaRamp_Proxy( 
    IDxtAlphaSetter * This,
    /* [retval][out] */ double *pVal);


void __RPC_STUB IDxtAlphaSetter_get_AlphaRamp_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtAlphaSetter_put_AlphaRamp_Proxy( 
    IDxtAlphaSetter * This,
    /* [in] */ double newVal);


void __RPC_STUB IDxtAlphaSetter_put_AlphaRamp_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDxtAlphaSetter_INTERFACE_DEFINED__ */


#ifndef __IDxtJpeg_INTERFACE_DEFINED__
#define __IDxtJpeg_INTERFACE_DEFINED__

/* interface IDxtJpeg */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IDxtJpeg;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DE75D011-7A65-11D2-8CEA-00A0C9441E20")
    IDxtJpeg : public IDXEffect
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaskNum( 
            /* [retval][out] */ long *__MIDL_0018) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaskNum( 
            /* [in] */ long __MIDL_0019) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaskName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaskName( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ScaleX( 
            /* [retval][out] */ double *__MIDL_0020) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ScaleX( 
            /* [in] */ double __MIDL_0021) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ScaleY( 
            /* [retval][out] */ double *__MIDL_0022) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ScaleY( 
            /* [in] */ double __MIDL_0023) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OffsetX( 
            /* [retval][out] */ long *__MIDL_0024) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OffsetX( 
            /* [in] */ long __MIDL_0025) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OffsetY( 
            /* [retval][out] */ long *__MIDL_0026) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OffsetY( 
            /* [in] */ long __MIDL_0027) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ReplicateX( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ReplicateX( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ReplicateY( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ReplicateY( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BorderColor( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BorderColor( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BorderWidth( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BorderWidth( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BorderSoftness( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BorderSoftness( 
            /* [in] */ long newVal) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ApplyChanges( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE LoadDefSettings( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDxtJpegVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDxtJpeg * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDxtJpeg * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDxtJpeg * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDxtJpeg * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDxtJpeg * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDxtJpeg * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDxtJpeg * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Capabilities )( 
            IDxtJpeg * This,
            /* [retval][out] */ long *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Progress )( 
            IDxtJpeg * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Progress )( 
            IDxtJpeg * This,
            /* [in] */ float newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StepResolution )( 
            IDxtJpeg * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            IDxtJpeg * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Duration )( 
            IDxtJpeg * This,
            /* [in] */ float newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MaskNum )( 
            IDxtJpeg * This,
            /* [retval][out] */ long *__MIDL_0018);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaskNum )( 
            IDxtJpeg * This,
            /* [in] */ long __MIDL_0019);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MaskName )( 
            IDxtJpeg * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaskName )( 
            IDxtJpeg * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ScaleX )( 
            IDxtJpeg * This,
            /* [retval][out] */ double *__MIDL_0020);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ScaleX )( 
            IDxtJpeg * This,
            /* [in] */ double __MIDL_0021);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ScaleY )( 
            IDxtJpeg * This,
            /* [retval][out] */ double *__MIDL_0022);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ScaleY )( 
            IDxtJpeg * This,
            /* [in] */ double __MIDL_0023);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OffsetX )( 
            IDxtJpeg * This,
            /* [retval][out] */ long *__MIDL_0024);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OffsetX )( 
            IDxtJpeg * This,
            /* [in] */ long __MIDL_0025);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OffsetY )( 
            IDxtJpeg * This,
            /* [retval][out] */ long *__MIDL_0026);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OffsetY )( 
            IDxtJpeg * This,
            /* [in] */ long __MIDL_0027);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ReplicateX )( 
            IDxtJpeg * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ReplicateX )( 
            IDxtJpeg * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ReplicateY )( 
            IDxtJpeg * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ReplicateY )( 
            IDxtJpeg * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BorderColor )( 
            IDxtJpeg * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BorderColor )( 
            IDxtJpeg * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BorderWidth )( 
            IDxtJpeg * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BorderWidth )( 
            IDxtJpeg * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BorderSoftness )( 
            IDxtJpeg * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BorderSoftness )( 
            IDxtJpeg * This,
            /* [in] */ long newVal);
        
        HRESULT ( STDMETHODCALLTYPE *ApplyChanges )( 
            IDxtJpeg * This);
        
        HRESULT ( STDMETHODCALLTYPE *LoadDefSettings )( 
            IDxtJpeg * This);
        
        END_INTERFACE
    } IDxtJpegVtbl;

    interface IDxtJpeg
    {
        CONST_VTBL struct IDxtJpegVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDxtJpeg_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDxtJpeg_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDxtJpeg_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDxtJpeg_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IDxtJpeg_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IDxtJpeg_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IDxtJpeg_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IDxtJpeg_get_Capabilities(This,pVal)	\
    (This)->lpVtbl -> get_Capabilities(This,pVal)

#define IDxtJpeg_get_Progress(This,pVal)	\
    (This)->lpVtbl -> get_Progress(This,pVal)

#define IDxtJpeg_put_Progress(This,newVal)	\
    (This)->lpVtbl -> put_Progress(This,newVal)

#define IDxtJpeg_get_StepResolution(This,pVal)	\
    (This)->lpVtbl -> get_StepResolution(This,pVal)

#define IDxtJpeg_get_Duration(This,pVal)	\
    (This)->lpVtbl -> get_Duration(This,pVal)

#define IDxtJpeg_put_Duration(This,newVal)	\
    (This)->lpVtbl -> put_Duration(This,newVal)


#define IDxtJpeg_get_MaskNum(This,__MIDL_0018)	\
    (This)->lpVtbl -> get_MaskNum(This,__MIDL_0018)

#define IDxtJpeg_put_MaskNum(This,__MIDL_0019)	\
    (This)->lpVtbl -> put_MaskNum(This,__MIDL_0019)

#define IDxtJpeg_get_MaskName(This,pVal)	\
    (This)->lpVtbl -> get_MaskName(This,pVal)

#define IDxtJpeg_put_MaskName(This,newVal)	\
    (This)->lpVtbl -> put_MaskName(This,newVal)

#define IDxtJpeg_get_ScaleX(This,__MIDL_0020)	\
    (This)->lpVtbl -> get_ScaleX(This,__MIDL_0020)

#define IDxtJpeg_put_ScaleX(This,__MIDL_0021)	\
    (This)->lpVtbl -> put_ScaleX(This,__MIDL_0021)

#define IDxtJpeg_get_ScaleY(This,__MIDL_0022)	\
    (This)->lpVtbl -> get_ScaleY(This,__MIDL_0022)

#define IDxtJpeg_put_ScaleY(This,__MIDL_0023)	\
    (This)->lpVtbl -> put_ScaleY(This,__MIDL_0023)

#define IDxtJpeg_get_OffsetX(This,__MIDL_0024)	\
    (This)->lpVtbl -> get_OffsetX(This,__MIDL_0024)

#define IDxtJpeg_put_OffsetX(This,__MIDL_0025)	\
    (This)->lpVtbl -> put_OffsetX(This,__MIDL_0025)

#define IDxtJpeg_get_OffsetY(This,__MIDL_0026)	\
    (This)->lpVtbl -> get_OffsetY(This,__MIDL_0026)

#define IDxtJpeg_put_OffsetY(This,__MIDL_0027)	\
    (This)->lpVtbl -> put_OffsetY(This,__MIDL_0027)

#define IDxtJpeg_get_ReplicateX(This,pVal)	\
    (This)->lpVtbl -> get_ReplicateX(This,pVal)

#define IDxtJpeg_put_ReplicateX(This,newVal)	\
    (This)->lpVtbl -> put_ReplicateX(This,newVal)

#define IDxtJpeg_get_ReplicateY(This,pVal)	\
    (This)->lpVtbl -> get_ReplicateY(This,pVal)

#define IDxtJpeg_put_ReplicateY(This,newVal)	\
    (This)->lpVtbl -> put_ReplicateY(This,newVal)

#define IDxtJpeg_get_BorderColor(This,pVal)	\
    (This)->lpVtbl -> get_BorderColor(This,pVal)

#define IDxtJpeg_put_BorderColor(This,newVal)	\
    (This)->lpVtbl -> put_BorderColor(This,newVal)

#define IDxtJpeg_get_BorderWidth(This,pVal)	\
    (This)->lpVtbl -> get_BorderWidth(This,pVal)

#define IDxtJpeg_put_BorderWidth(This,newVal)	\
    (This)->lpVtbl -> put_BorderWidth(This,newVal)

#define IDxtJpeg_get_BorderSoftness(This,pVal)	\
    (This)->lpVtbl -> get_BorderSoftness(This,pVal)

#define IDxtJpeg_put_BorderSoftness(This,newVal)	\
    (This)->lpVtbl -> put_BorderSoftness(This,newVal)

#define IDxtJpeg_ApplyChanges(This)	\
    (This)->lpVtbl -> ApplyChanges(This)

#define IDxtJpeg_LoadDefSettings(This)	\
    (This)->lpVtbl -> LoadDefSettings(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_get_MaskNum_Proxy( 
    IDxtJpeg * This,
    /* [retval][out] */ long *__MIDL_0018);


void __RPC_STUB IDxtJpeg_get_MaskNum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_put_MaskNum_Proxy( 
    IDxtJpeg * This,
    /* [in] */ long __MIDL_0019);


void __RPC_STUB IDxtJpeg_put_MaskNum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_get_MaskName_Proxy( 
    IDxtJpeg * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IDxtJpeg_get_MaskName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_put_MaskName_Proxy( 
    IDxtJpeg * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IDxtJpeg_put_MaskName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_get_ScaleX_Proxy( 
    IDxtJpeg * This,
    /* [retval][out] */ double *__MIDL_0020);


void __RPC_STUB IDxtJpeg_get_ScaleX_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_put_ScaleX_Proxy( 
    IDxtJpeg * This,
    /* [in] */ double __MIDL_0021);


void __RPC_STUB IDxtJpeg_put_ScaleX_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_get_ScaleY_Proxy( 
    IDxtJpeg * This,
    /* [retval][out] */ double *__MIDL_0022);


void __RPC_STUB IDxtJpeg_get_ScaleY_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_put_ScaleY_Proxy( 
    IDxtJpeg * This,
    /* [in] */ double __MIDL_0023);


void __RPC_STUB IDxtJpeg_put_ScaleY_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_get_OffsetX_Proxy( 
    IDxtJpeg * This,
    /* [retval][out] */ long *__MIDL_0024);


void __RPC_STUB IDxtJpeg_get_OffsetX_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_put_OffsetX_Proxy( 
    IDxtJpeg * This,
    /* [in] */ long __MIDL_0025);


void __RPC_STUB IDxtJpeg_put_OffsetX_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_get_OffsetY_Proxy( 
    IDxtJpeg * This,
    /* [retval][out] */ long *__MIDL_0026);


void __RPC_STUB IDxtJpeg_get_OffsetY_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_put_OffsetY_Proxy( 
    IDxtJpeg * This,
    /* [in] */ long __MIDL_0027);


void __RPC_STUB IDxtJpeg_put_OffsetY_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_get_ReplicateX_Proxy( 
    IDxtJpeg * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtJpeg_get_ReplicateX_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_put_ReplicateX_Proxy( 
    IDxtJpeg * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtJpeg_put_ReplicateX_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_get_ReplicateY_Proxy( 
    IDxtJpeg * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtJpeg_get_ReplicateY_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_put_ReplicateY_Proxy( 
    IDxtJpeg * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtJpeg_put_ReplicateY_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_get_BorderColor_Proxy( 
    IDxtJpeg * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtJpeg_get_BorderColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_put_BorderColor_Proxy( 
    IDxtJpeg * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtJpeg_put_BorderColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_get_BorderWidth_Proxy( 
    IDxtJpeg * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtJpeg_get_BorderWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_put_BorderWidth_Proxy( 
    IDxtJpeg * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtJpeg_put_BorderWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_get_BorderSoftness_Proxy( 
    IDxtJpeg * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IDxtJpeg_get_BorderSoftness_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtJpeg_put_BorderSoftness_Proxy( 
    IDxtJpeg * This,
    /* [in] */ long newVal);


void __RPC_STUB IDxtJpeg_put_BorderSoftness_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDxtJpeg_ApplyChanges_Proxy( 
    IDxtJpeg * This);


void __RPC_STUB IDxtJpeg_ApplyChanges_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IDxtJpeg_LoadDefSettings_Proxy( 
    IDxtJpeg * This);


void __RPC_STUB IDxtJpeg_LoadDefSettings_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDxtJpeg_INTERFACE_DEFINED__ */


#ifndef __IDxtKey_INTERFACE_DEFINED__
#define __IDxtKey_INTERFACE_DEFINED__

/* interface IDxtKey */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IDxtKey;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("3255de56-38fb-4901-b980-94b438010d7b")
    IDxtKey : public IDXEffect
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_KeyType( 
            /* [retval][out] */ int *__MIDL_0028) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_KeyType( 
            /* [in] */ int __MIDL_0029) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Hue( 
            /* [retval][out] */ int *__MIDL_0030) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Hue( 
            /* [in] */ int __MIDL_0031) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Luminance( 
            /* [retval][out] */ int *__MIDL_0032) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Luminance( 
            /* [in] */ int __MIDL_0033) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RGB( 
            /* [retval][out] */ DWORD *__MIDL_0034) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RGB( 
            /* [in] */ DWORD __MIDL_0035) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Similarity( 
            /* [retval][out] */ int *__MIDL_0036) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Similarity( 
            /* [in] */ int __MIDL_0037) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Invert( 
            /* [retval][out] */ BOOL *__MIDL_0038) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Invert( 
            /* [in] */ BOOL __MIDL_0039) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDxtKeyVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDxtKey * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDxtKey * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDxtKey * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDxtKey * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDxtKey * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDxtKey * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDxtKey * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Capabilities )( 
            IDxtKey * This,
            /* [retval][out] */ long *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Progress )( 
            IDxtKey * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Progress )( 
            IDxtKey * This,
            /* [in] */ float newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StepResolution )( 
            IDxtKey * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            IDxtKey * This,
            /* [retval][out] */ float *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Duration )( 
            IDxtKey * This,
            /* [in] */ float newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_KeyType )( 
            IDxtKey * This,
            /* [retval][out] */ int *__MIDL_0028);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_KeyType )( 
            IDxtKey * This,
            /* [in] */ int __MIDL_0029);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Hue )( 
            IDxtKey * This,
            /* [retval][out] */ int *__MIDL_0030);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Hue )( 
            IDxtKey * This,
            /* [in] */ int __MIDL_0031);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Luminance )( 
            IDxtKey * This,
            /* [retval][out] */ int *__MIDL_0032);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Luminance )( 
            IDxtKey * This,
            /* [in] */ int __MIDL_0033);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RGB )( 
            IDxtKey * This,
            /* [retval][out] */ DWORD *__MIDL_0034);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RGB )( 
            IDxtKey * This,
            /* [in] */ DWORD __MIDL_0035);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Similarity )( 
            IDxtKey * This,
            /* [retval][out] */ int *__MIDL_0036);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Similarity )( 
            IDxtKey * This,
            /* [in] */ int __MIDL_0037);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Invert )( 
            IDxtKey * This,
            /* [retval][out] */ BOOL *__MIDL_0038);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Invert )( 
            IDxtKey * This,
            /* [in] */ BOOL __MIDL_0039);
        
        END_INTERFACE
    } IDxtKeyVtbl;

    interface IDxtKey
    {
        CONST_VTBL struct IDxtKeyVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDxtKey_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDxtKey_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDxtKey_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDxtKey_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IDxtKey_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IDxtKey_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IDxtKey_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IDxtKey_get_Capabilities(This,pVal)	\
    (This)->lpVtbl -> get_Capabilities(This,pVal)

#define IDxtKey_get_Progress(This,pVal)	\
    (This)->lpVtbl -> get_Progress(This,pVal)

#define IDxtKey_put_Progress(This,newVal)	\
    (This)->lpVtbl -> put_Progress(This,newVal)

#define IDxtKey_get_StepResolution(This,pVal)	\
    (This)->lpVtbl -> get_StepResolution(This,pVal)

#define IDxtKey_get_Duration(This,pVal)	\
    (This)->lpVtbl -> get_Duration(This,pVal)

#define IDxtKey_put_Duration(This,newVal)	\
    (This)->lpVtbl -> put_Duration(This,newVal)


#define IDxtKey_get_KeyType(This,__MIDL_0028)	\
    (This)->lpVtbl -> get_KeyType(This,__MIDL_0028)

#define IDxtKey_put_KeyType(This,__MIDL_0029)	\
    (This)->lpVtbl -> put_KeyType(This,__MIDL_0029)

#define IDxtKey_get_Hue(This,__MIDL_0030)	\
    (This)->lpVtbl -> get_Hue(This,__MIDL_0030)

#define IDxtKey_put_Hue(This,__MIDL_0031)	\
    (This)->lpVtbl -> put_Hue(This,__MIDL_0031)

#define IDxtKey_get_Luminance(This,__MIDL_0032)	\
    (This)->lpVtbl -> get_Luminance(This,__MIDL_0032)

#define IDxtKey_put_Luminance(This,__MIDL_0033)	\
    (This)->lpVtbl -> put_Luminance(This,__MIDL_0033)

#define IDxtKey_get_RGB(This,__MIDL_0034)	\
    (This)->lpVtbl -> get_RGB(This,__MIDL_0034)

#define IDxtKey_put_RGB(This,__MIDL_0035)	\
    (This)->lpVtbl -> put_RGB(This,__MIDL_0035)

#define IDxtKey_get_Similarity(This,__MIDL_0036)	\
    (This)->lpVtbl -> get_Similarity(This,__MIDL_0036)

#define IDxtKey_put_Similarity(This,__MIDL_0037)	\
    (This)->lpVtbl -> put_Similarity(This,__MIDL_0037)

#define IDxtKey_get_Invert(This,__MIDL_0038)	\
    (This)->lpVtbl -> get_Invert(This,__MIDL_0038)

#define IDxtKey_put_Invert(This,__MIDL_0039)	\
    (This)->lpVtbl -> put_Invert(This,__MIDL_0039)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtKey_get_KeyType_Proxy( 
    IDxtKey * This,
    /* [retval][out] */ int *__MIDL_0028);


void __RPC_STUB IDxtKey_get_KeyType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtKey_put_KeyType_Proxy( 
    IDxtKey * This,
    /* [in] */ int __MIDL_0029);


void __RPC_STUB IDxtKey_put_KeyType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtKey_get_Hue_Proxy( 
    IDxtKey * This,
    /* [retval][out] */ int *__MIDL_0030);


void __RPC_STUB IDxtKey_get_Hue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtKey_put_Hue_Proxy( 
    IDxtKey * This,
    /* [in] */ int __MIDL_0031);


void __RPC_STUB IDxtKey_put_Hue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtKey_get_Luminance_Proxy( 
    IDxtKey * This,
    /* [retval][out] */ int *__MIDL_0032);


void __RPC_STUB IDxtKey_get_Luminance_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtKey_put_Luminance_Proxy( 
    IDxtKey * This,
    /* [in] */ int __MIDL_0033);


void __RPC_STUB IDxtKey_put_Luminance_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtKey_get_RGB_Proxy( 
    IDxtKey * This,
    /* [retval][out] */ DWORD *__MIDL_0034);


void __RPC_STUB IDxtKey_get_RGB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtKey_put_RGB_Proxy( 
    IDxtKey * This,
    /* [in] */ DWORD __MIDL_0035);


void __RPC_STUB IDxtKey_put_RGB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtKey_get_Similarity_Proxy( 
    IDxtKey * This,
    /* [retval][out] */ int *__MIDL_0036);


void __RPC_STUB IDxtKey_get_Similarity_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtKey_put_Similarity_Proxy( 
    IDxtKey * This,
    /* [in] */ int __MIDL_0037);


void __RPC_STUB IDxtKey_put_Similarity_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IDxtKey_get_Invert_Proxy( 
    IDxtKey * This,
    /* [retval][out] */ BOOL *__MIDL_0038);


void __RPC_STUB IDxtKey_get_Invert_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IDxtKey_put_Invert_Proxy( 
    IDxtKey * This,
    /* [in] */ BOOL __MIDL_0039);


void __RPC_STUB IDxtKey_put_Invert_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDxtKey_INTERFACE_DEFINED__ */


#ifndef __IMediaLocator_INTERFACE_DEFINED__
#define __IMediaLocator_INTERFACE_DEFINED__

/* interface IMediaLocator */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IMediaLocator;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("288581E0-66CE-11d2-918F-00C0DF10D434")
    IMediaLocator : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE FindMediaFile( 
            BSTR Input,
            BSTR FilterString,
            BSTR *pOutput,
            long Flags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE AddFoundLocation( 
            BSTR DirectoryName) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMediaLocatorVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMediaLocator * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMediaLocator * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMediaLocator * This);
        
        HRESULT ( STDMETHODCALLTYPE *FindMediaFile )( 
            IMediaLocator * This,
            BSTR Input,
            BSTR FilterString,
            BSTR *pOutput,
            long Flags);
        
        HRESULT ( STDMETHODCALLTYPE *AddFoundLocation )( 
            IMediaLocator * This,
            BSTR DirectoryName);
        
        END_INTERFACE
    } IMediaLocatorVtbl;

    interface IMediaLocator
    {
        CONST_VTBL struct IMediaLocatorVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMediaLocator_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IMediaLocator_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IMediaLocator_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IMediaLocator_FindMediaFile(This,Input,FilterString,pOutput,Flags)	\
    (This)->lpVtbl -> FindMediaFile(This,Input,FilterString,pOutput,Flags)

#define IMediaLocator_AddFoundLocation(This,DirectoryName)	\
    (This)->lpVtbl -> AddFoundLocation(This,DirectoryName)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IMediaLocator_FindMediaFile_Proxy( 
    IMediaLocator * This,
    BSTR Input,
    BSTR FilterString,
    BSTR *pOutput,
    long Flags);


void __RPC_STUB IMediaLocator_FindMediaFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IMediaLocator_AddFoundLocation_Proxy( 
    IMediaLocator * This,
    BSTR DirectoryName);


void __RPC_STUB IMediaLocator_AddFoundLocation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMediaLocator_INTERFACE_DEFINED__ */


#ifndef __IMediaDet_INTERFACE_DEFINED__
#define __IMediaDet_INTERFACE_DEFINED__

/* interface IMediaDet */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IMediaDet;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("65BD0710-24D2-4ff7-9324-ED2E5D3ABAFA")
    IMediaDet : public IUnknown
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Filter( 
            /* [retval][out] */ IUnknown **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Filter( 
            /* [in] */ IUnknown *newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OutputStreams( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CurrentStream( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CurrentStream( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StreamType( 
            /* [retval][out] */ GUID *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StreamTypeB( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StreamLength( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Filename( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Filename( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetBitmapBits( 
            double StreamTime,
            long *pBufferSize,
            char *pBuffer,
            long Width,
            long Height) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE WriteBitmapBits( 
            double StreamTime,
            long Width,
            long Height,
            BSTR Filename) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StreamMediaType( 
            /* [retval][out] */ AM_MEDIA_TYPE *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetSampleGrabber( 
            /* [out] */ ISampleGrabber **ppVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FrameRate( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EnterBitmapGrabMode( 
            double SeekTime) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMediaDetVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMediaDet * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMediaDet * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMediaDet * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Filter )( 
            IMediaDet * This,
            /* [retval][out] */ IUnknown **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Filter )( 
            IMediaDet * This,
            /* [in] */ IUnknown *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OutputStreams )( 
            IMediaDet * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CurrentStream )( 
            IMediaDet * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CurrentStream )( 
            IMediaDet * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StreamType )( 
            IMediaDet * This,
            /* [retval][out] */ GUID *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StreamTypeB )( 
            IMediaDet * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StreamLength )( 
            IMediaDet * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Filename )( 
            IMediaDet * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Filename )( 
            IMediaDet * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBitmapBits )( 
            IMediaDet * This,
            double StreamTime,
            long *pBufferSize,
            char *pBuffer,
            long Width,
            long Height);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WriteBitmapBits )( 
            IMediaDet * This,
            double StreamTime,
            long Width,
            long Height,
            BSTR Filename);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StreamMediaType )( 
            IMediaDet * This,
            /* [retval][out] */ AM_MEDIA_TYPE *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetSampleGrabber )( 
            IMediaDet * This,
            /* [out] */ ISampleGrabber **ppVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FrameRate )( 
            IMediaDet * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EnterBitmapGrabMode )( 
            IMediaDet * This,
            double SeekTime);
        
        END_INTERFACE
    } IMediaDetVtbl;

    interface IMediaDet
    {
        CONST_VTBL struct IMediaDetVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMediaDet_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IMediaDet_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IMediaDet_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IMediaDet_get_Filter(This,pVal)	\
    (This)->lpVtbl -> get_Filter(This,pVal)

#define IMediaDet_put_Filter(This,newVal)	\
    (This)->lpVtbl -> put_Filter(This,newVal)

#define IMediaDet_get_OutputStreams(This,pVal)	\
    (This)->lpVtbl -> get_OutputStreams(This,pVal)

#define IMediaDet_get_CurrentStream(This,pVal)	\
    (This)->lpVtbl -> get_CurrentStream(This,pVal)

#define IMediaDet_put_CurrentStream(This,newVal)	\
    (This)->lpVtbl -> put_CurrentStream(This,newVal)

#define IMediaDet_get_StreamType(This,pVal)	\
    (This)->lpVtbl -> get_StreamType(This,pVal)

#define IMediaDet_get_StreamTypeB(This,pVal)	\
    (This)->lpVtbl -> get_StreamTypeB(This,pVal)

#define IMediaDet_get_StreamLength(This,pVal)	\
    (This)->lpVtbl -> get_StreamLength(This,pVal)

#define IMediaDet_get_Filename(This,pVal)	\
    (This)->lpVtbl -> get_Filename(This,pVal)

#define IMediaDet_put_Filename(This,newVal)	\
    (This)->lpVtbl -> put_Filename(This,newVal)

#define IMediaDet_GetBitmapBits(This,StreamTime,pBufferSize,pBuffer,Width,Height)	\
    (This)->lpVtbl -> GetBitmapBits(This,StreamTime,pBufferSize,pBuffer,Width,Height)

#define IMediaDet_WriteBitmapBits(This,StreamTime,Width,Height,Filename)	\
    (This)->lpVtbl -> WriteBitmapBits(This,StreamTime,Width,Height,Filename)

#define IMediaDet_get_StreamMediaType(This,pVal)	\
    (This)->lpVtbl -> get_StreamMediaType(This,pVal)

#define IMediaDet_GetSampleGrabber(This,ppVal)	\
    (This)->lpVtbl -> GetSampleGrabber(This,ppVal)

#define IMediaDet_get_FrameRate(This,pVal)	\
    (This)->lpVtbl -> get_FrameRate(This,pVal)

#define IMediaDet_EnterBitmapGrabMode(This,SeekTime)	\
    (This)->lpVtbl -> EnterBitmapGrabMode(This,SeekTime)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IMediaDet_get_Filter_Proxy( 
    IMediaDet * This,
    /* [retval][out] */ IUnknown **pVal);


void __RPC_STUB IMediaDet_get_Filter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IMediaDet_put_Filter_Proxy( 
    IMediaDet * This,
    /* [in] */ IUnknown *newVal);


void __RPC_STUB IMediaDet_put_Filter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IMediaDet_get_OutputStreams_Proxy( 
    IMediaDet * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IMediaDet_get_OutputStreams_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IMediaDet_get_CurrentStream_Proxy( 
    IMediaDet * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IMediaDet_get_CurrentStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IMediaDet_put_CurrentStream_Proxy( 
    IMediaDet * This,
    /* [in] */ long newVal);


void __RPC_STUB IMediaDet_put_CurrentStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IMediaDet_get_StreamType_Proxy( 
    IMediaDet * This,
    /* [retval][out] */ GUID *pVal);


void __RPC_STUB IMediaDet_get_StreamType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IMediaDet_get_StreamTypeB_Proxy( 
    IMediaDet * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IMediaDet_get_StreamTypeB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IMediaDet_get_StreamLength_Proxy( 
    IMediaDet * This,
    /* [retval][out] */ double *pVal);


void __RPC_STUB IMediaDet_get_StreamLength_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IMediaDet_get_Filename_Proxy( 
    IMediaDet * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IMediaDet_get_Filename_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IMediaDet_put_Filename_Proxy( 
    IMediaDet * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IMediaDet_put_Filename_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMediaDet_GetBitmapBits_Proxy( 
    IMediaDet * This,
    double StreamTime,
    long *pBufferSize,
    char *pBuffer,
    long Width,
    long Height);


void __RPC_STUB IMediaDet_GetBitmapBits_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMediaDet_WriteBitmapBits_Proxy( 
    IMediaDet * This,
    double StreamTime,
    long Width,
    long Height,
    BSTR Filename);


void __RPC_STUB IMediaDet_WriteBitmapBits_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IMediaDet_get_StreamMediaType_Proxy( 
    IMediaDet * This,
    /* [retval][out] */ AM_MEDIA_TYPE *pVal);


void __RPC_STUB IMediaDet_get_StreamMediaType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMediaDet_GetSampleGrabber_Proxy( 
    IMediaDet * This,
    /* [out] */ ISampleGrabber **ppVal);


void __RPC_STUB IMediaDet_GetSampleGrabber_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IMediaDet_get_FrameRate_Proxy( 
    IMediaDet * This,
    /* [retval][out] */ double *pVal);


void __RPC_STUB IMediaDet_get_FrameRate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMediaDet_EnterBitmapGrabMode_Proxy( 
    IMediaDet * This,
    double SeekTime);


void __RPC_STUB IMediaDet_EnterBitmapGrabMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMediaDet_INTERFACE_DEFINED__ */


#ifndef __IGrfCache_INTERFACE_DEFINED__
#define __IGrfCache_INTERFACE_DEFINED__

/* interface IGrfCache */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IGrfCache;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("AE9472BE-B0C3-11D2-8D24-00A0C9441E20")
    IGrfCache : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddFilter( 
            IGrfCache *ChainedCache,
            LONGLONG ID,
            const IBaseFilter *pFilter,
            LPCWSTR pName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConnectPins( 
            IGrfCache *ChainedCache,
            LONGLONG PinID1,
            const IPin *pPin1,
            LONGLONG PinID2,
            const IPin *pPin2) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetGraph( 
            const IGraphBuilder *pGraph) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DoConnectionsNow( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGrfCacheVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGrfCache * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGrfCache * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGrfCache * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGrfCache * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGrfCache * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGrfCache * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGrfCache * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddFilter )( 
            IGrfCache * This,
            IGrfCache *ChainedCache,
            LONGLONG ID,
            const IBaseFilter *pFilter,
            LPCWSTR pName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConnectPins )( 
            IGrfCache * This,
            IGrfCache *ChainedCache,
            LONGLONG PinID1,
            const IPin *pPin1,
            LONGLONG PinID2,
            const IPin *pPin2);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetGraph )( 
            IGrfCache * This,
            const IGraphBuilder *pGraph);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DoConnectionsNow )( 
            IGrfCache * This);
        
        END_INTERFACE
    } IGrfCacheVtbl;

    interface IGrfCache
    {
        CONST_VTBL struct IGrfCacheVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGrfCache_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IGrfCache_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IGrfCache_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IGrfCache_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IGrfCache_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IGrfCache_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IGrfCache_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IGrfCache_AddFilter(This,ChainedCache,ID,pFilter,pName)	\
    (This)->lpVtbl -> AddFilter(This,ChainedCache,ID,pFilter,pName)

#define IGrfCache_ConnectPins(This,ChainedCache,PinID1,pPin1,PinID2,pPin2)	\
    (This)->lpVtbl -> ConnectPins(This,ChainedCache,PinID1,pPin1,PinID2,pPin2)

#define IGrfCache_SetGraph(This,pGraph)	\
    (This)->lpVtbl -> SetGraph(This,pGraph)

#define IGrfCache_DoConnectionsNow(This)	\
    (This)->lpVtbl -> DoConnectionsNow(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGrfCache_AddFilter_Proxy( 
    IGrfCache * This,
    IGrfCache *ChainedCache,
    LONGLONG ID,
    const IBaseFilter *pFilter,
    LPCWSTR pName);


void __RPC_STUB IGrfCache_AddFilter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGrfCache_ConnectPins_Proxy( 
    IGrfCache * This,
    IGrfCache *ChainedCache,
    LONGLONG PinID1,
    const IPin *pPin1,
    LONGLONG PinID2,
    const IPin *pPin2);


void __RPC_STUB IGrfCache_ConnectPins_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGrfCache_SetGraph_Proxy( 
    IGrfCache * This,
    const IGraphBuilder *pGraph);


void __RPC_STUB IGrfCache_SetGraph_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGrfCache_DoConnectionsNow_Proxy( 
    IGrfCache * This);


void __RPC_STUB IGrfCache_DoConnectionsNow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IGrfCache_INTERFACE_DEFINED__ */


#ifndef __IRenderEngine_INTERFACE_DEFINED__
#define __IRenderEngine_INTERFACE_DEFINED__

/* interface IRenderEngine */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IRenderEngine;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6BEE3A81-66C9-11d2-918F-00C0DF10D434")
    IRenderEngine : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetTimelineObject( 
            IAMTimeline *pTimeline) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetTimelineObject( 
            /* [out] */ IAMTimeline **ppTimeline) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetFilterGraph( 
            /* [out] */ IGraphBuilder **ppFG) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetFilterGraph( 
            IGraphBuilder *pFG) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetInterestRange( 
            REFERENCE_TIME Start,
            REFERENCE_TIME Stop) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetInterestRange2( 
            double Start,
            double Stop) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetRenderRange( 
            REFERENCE_TIME Start,
            REFERENCE_TIME Stop) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetRenderRange2( 
            double Start,
            double Stop) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetGroupOutputPin( 
            long Group,
            /* [out] */ IPin **ppRenderPin) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ScrapIt( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE RenderOutputPins( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetVendorString( 
            /* [retval][out] */ BSTR *pVendorID) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ConnectFrontEnd( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetSourceConnectCallback( 
            IGrfCache *pCallback) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetDynamicReconnectLevel( 
            long Level) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE DoSmartRecompression( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE UseInSmartRecompressionGraph( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetSourceNameValidation( 
            BSTR FilterString,
            IMediaLocator *pOverride,
            LONG Flags) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Commit( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Decommit( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetCaps( 
            long Index,
            long *pReturn) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IRenderEngineVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRenderEngine * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRenderEngine * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRenderEngine * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetTimelineObject )( 
            IRenderEngine * This,
            IAMTimeline *pTimeline);
        
        HRESULT ( STDMETHODCALLTYPE *GetTimelineObject )( 
            IRenderEngine * This,
            /* [out] */ IAMTimeline **ppTimeline);
        
        HRESULT ( STDMETHODCALLTYPE *GetFilterGraph )( 
            IRenderEngine * This,
            /* [out] */ IGraphBuilder **ppFG);
        
        HRESULT ( STDMETHODCALLTYPE *SetFilterGraph )( 
            IRenderEngine * This,
            IGraphBuilder *pFG);
        
        HRESULT ( STDMETHODCALLTYPE *SetInterestRange )( 
            IRenderEngine * This,
            REFERENCE_TIME Start,
            REFERENCE_TIME Stop);
        
        HRESULT ( STDMETHODCALLTYPE *SetInterestRange2 )( 
            IRenderEngine * This,
            double Start,
            double Stop);
        
        HRESULT ( STDMETHODCALLTYPE *SetRenderRange )( 
            IRenderEngine * This,
            REFERENCE_TIME Start,
            REFERENCE_TIME Stop);
        
        HRESULT ( STDMETHODCALLTYPE *SetRenderRange2 )( 
            IRenderEngine * This,
            double Start,
            double Stop);
        
        HRESULT ( STDMETHODCALLTYPE *GetGroupOutputPin )( 
            IRenderEngine * This,
            long Group,
            /* [out] */ IPin **ppRenderPin);
        
        HRESULT ( STDMETHODCALLTYPE *ScrapIt )( 
            IRenderEngine * This);
        
        HRESULT ( STDMETHODCALLTYPE *RenderOutputPins )( 
            IRenderEngine * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetVendorString )( 
            IRenderEngine * This,
            /* [retval][out] */ BSTR *pVendorID);
        
        HRESULT ( STDMETHODCALLTYPE *ConnectFrontEnd )( 
            IRenderEngine * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetSourceConnectCallback )( 
            IRenderEngine * This,
            IGrfCache *pCallback);
        
        HRESULT ( STDMETHODCALLTYPE *SetDynamicReconnectLevel )( 
            IRenderEngine * This,
            long Level);
        
        HRESULT ( STDMETHODCALLTYPE *DoSmartRecompression )( 
            IRenderEngine * This);
        
        HRESULT ( STDMETHODCALLTYPE *UseInSmartRecompressionGraph )( 
            IRenderEngine * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetSourceNameValidation )( 
            IRenderEngine * This,
            BSTR FilterString,
            IMediaLocator *pOverride,
            LONG Flags);
        
        HRESULT ( STDMETHODCALLTYPE *Commit )( 
            IRenderEngine * This);
        
        HRESULT ( STDMETHODCALLTYPE *Decommit )( 
            IRenderEngine * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetCaps )( 
            IRenderEngine * This,
            long Index,
            long *pReturn);
        
        END_INTERFACE
    } IRenderEngineVtbl;

    interface IRenderEngine
    {
        CONST_VTBL struct IRenderEngineVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRenderEngine_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IRenderEngine_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IRenderEngine_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IRenderEngine_SetTimelineObject(This,pTimeline)	\
    (This)->lpVtbl -> SetTimelineObject(This,pTimeline)

#define IRenderEngine_GetTimelineObject(This,ppTimeline)	\
    (This)->lpVtbl -> GetTimelineObject(This,ppTimeline)

#define IRenderEngine_GetFilterGraph(This,ppFG)	\
    (This)->lpVtbl -> GetFilterGraph(This,ppFG)

#define IRenderEngine_SetFilterGraph(This,pFG)	\
    (This)->lpVtbl -> SetFilterGraph(This,pFG)

#define IRenderEngine_SetInterestRange(This,Start,Stop)	\
    (This)->lpVtbl -> SetInterestRange(This,Start,Stop)

#define IRenderEngine_SetInterestRange2(This,Start,Stop)	\
    (This)->lpVtbl -> SetInterestRange2(This,Start,Stop)

#define IRenderEngine_SetRenderRange(This,Start,Stop)	\
    (This)->lpVtbl -> SetRenderRange(This,Start,Stop)

#define IRenderEngine_SetRenderRange2(This,Start,Stop)	\
    (This)->lpVtbl -> SetRenderRange2(This,Start,Stop)

#define IRenderEngine_GetGroupOutputPin(This,Group,ppRenderPin)	\
    (This)->lpVtbl -> GetGroupOutputPin(This,Group,ppRenderPin)

#define IRenderEngine_ScrapIt(This)	\
    (This)->lpVtbl -> ScrapIt(This)

#define IRenderEngine_RenderOutputPins(This)	\
    (This)->lpVtbl -> RenderOutputPins(This)

#define IRenderEngine_GetVendorString(This,pVendorID)	\
    (This)->lpVtbl -> GetVendorString(This,pVendorID)

#define IRenderEngine_ConnectFrontEnd(This)	\
    (This)->lpVtbl -> ConnectFrontEnd(This)

#define IRenderEngine_SetSourceConnectCallback(This,pCallback)	\
    (This)->lpVtbl -> SetSourceConnectCallback(This,pCallback)

#define IRenderEngine_SetDynamicReconnectLevel(This,Level)	\
    (This)->lpVtbl -> SetDynamicReconnectLevel(This,Level)

#define IRenderEngine_DoSmartRecompression(This)	\
    (This)->lpVtbl -> DoSmartRecompression(This)

#define IRenderEngine_UseInSmartRecompressionGraph(This)	\
    (This)->lpVtbl -> UseInSmartRecompressionGraph(This)

#define IRenderEngine_SetSourceNameValidation(This,FilterString,pOverride,Flags)	\
    (This)->lpVtbl -> SetSourceNameValidation(This,FilterString,pOverride,Flags)

#define IRenderEngine_Commit(This)	\
    (This)->lpVtbl -> Commit(This)

#define IRenderEngine_Decommit(This)	\
    (This)->lpVtbl -> Decommit(This)

#define IRenderEngine_GetCaps(This,Index,pReturn)	\
    (This)->lpVtbl -> GetCaps(This,Index,pReturn)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IRenderEngine_SetTimelineObject_Proxy( 
    IRenderEngine * This,
    IAMTimeline *pTimeline);


void __RPC_STUB IRenderEngine_SetTimelineObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_GetTimelineObject_Proxy( 
    IRenderEngine * This,
    /* [out] */ IAMTimeline **ppTimeline);


void __RPC_STUB IRenderEngine_GetTimelineObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_GetFilterGraph_Proxy( 
    IRenderEngine * This,
    /* [out] */ IGraphBuilder **ppFG);


void __RPC_STUB IRenderEngine_GetFilterGraph_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_SetFilterGraph_Proxy( 
    IRenderEngine * This,
    IGraphBuilder *pFG);


void __RPC_STUB IRenderEngine_SetFilterGraph_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_SetInterestRange_Proxy( 
    IRenderEngine * This,
    REFERENCE_TIME Start,
    REFERENCE_TIME Stop);


void __RPC_STUB IRenderEngine_SetInterestRange_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_SetInterestRange2_Proxy( 
    IRenderEngine * This,
    double Start,
    double Stop);


void __RPC_STUB IRenderEngine_SetInterestRange2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_SetRenderRange_Proxy( 
    IRenderEngine * This,
    REFERENCE_TIME Start,
    REFERENCE_TIME Stop);


void __RPC_STUB IRenderEngine_SetRenderRange_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_SetRenderRange2_Proxy( 
    IRenderEngine * This,
    double Start,
    double Stop);


void __RPC_STUB IRenderEngine_SetRenderRange2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_GetGroupOutputPin_Proxy( 
    IRenderEngine * This,
    long Group,
    /* [out] */ IPin **ppRenderPin);


void __RPC_STUB IRenderEngine_GetGroupOutputPin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_ScrapIt_Proxy( 
    IRenderEngine * This);


void __RPC_STUB IRenderEngine_ScrapIt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_RenderOutputPins_Proxy( 
    IRenderEngine * This);


void __RPC_STUB IRenderEngine_RenderOutputPins_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_GetVendorString_Proxy( 
    IRenderEngine * This,
    /* [retval][out] */ BSTR *pVendorID);


void __RPC_STUB IRenderEngine_GetVendorString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_ConnectFrontEnd_Proxy( 
    IRenderEngine * This);


void __RPC_STUB IRenderEngine_ConnectFrontEnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_SetSourceConnectCallback_Proxy( 
    IRenderEngine * This,
    IGrfCache *pCallback);


void __RPC_STUB IRenderEngine_SetSourceConnectCallback_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_SetDynamicReconnectLevel_Proxy( 
    IRenderEngine * This,
    long Level);


void __RPC_STUB IRenderEngine_SetDynamicReconnectLevel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_DoSmartRecompression_Proxy( 
    IRenderEngine * This);


void __RPC_STUB IRenderEngine_DoSmartRecompression_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_UseInSmartRecompressionGraph_Proxy( 
    IRenderEngine * This);


void __RPC_STUB IRenderEngine_UseInSmartRecompressionGraph_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_SetSourceNameValidation_Proxy( 
    IRenderEngine * This,
    BSTR FilterString,
    IMediaLocator *pOverride,
    LONG Flags);


void __RPC_STUB IRenderEngine_SetSourceNameValidation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_Commit_Proxy( 
    IRenderEngine * This);


void __RPC_STUB IRenderEngine_Commit_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_Decommit_Proxy( 
    IRenderEngine * This);


void __RPC_STUB IRenderEngine_Decommit_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IRenderEngine_GetCaps_Proxy( 
    IRenderEngine * This,
    long Index,
    long *pReturn);


void __RPC_STUB IRenderEngine_GetCaps_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IRenderEngine_INTERFACE_DEFINED__ */


#ifndef __IFindCompressorCB_INTERFACE_DEFINED__
#define __IFindCompressorCB_INTERFACE_DEFINED__

/* interface IFindCompressorCB */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IFindCompressorCB;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F03FA8DE-879A-4d59-9B2C-26BB1CF83461")
    IFindCompressorCB : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetCompressor( 
            AM_MEDIA_TYPE *pType,
            AM_MEDIA_TYPE *pCompType,
            /* [out] */ IBaseFilter **ppFilter) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IFindCompressorCBVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IFindCompressorCB * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IFindCompressorCB * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IFindCompressorCB * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetCompressor )( 
            IFindCompressorCB * This,
            AM_MEDIA_TYPE *pType,
            AM_MEDIA_TYPE *pCompType,
            /* [out] */ IBaseFilter **ppFilter);
        
        END_INTERFACE
    } IFindCompressorCBVtbl;

    interface IFindCompressorCB
    {
        CONST_VTBL struct IFindCompressorCBVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IFindCompressorCB_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IFindCompressorCB_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IFindCompressorCB_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IFindCompressorCB_GetCompressor(This,pType,pCompType,ppFilter)	\
    (This)->lpVtbl -> GetCompressor(This,pType,pCompType,ppFilter)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IFindCompressorCB_GetCompressor_Proxy( 
    IFindCompressorCB * This,
    AM_MEDIA_TYPE *pType,
    AM_MEDIA_TYPE *pCompType,
    /* [out] */ IBaseFilter **ppFilter);


void __RPC_STUB IFindCompressorCB_GetCompressor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IFindCompressorCB_INTERFACE_DEFINED__ */


#ifndef __ISmartRenderEngine_INTERFACE_DEFINED__
#define __ISmartRenderEngine_INTERFACE_DEFINED__

/* interface ISmartRenderEngine */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_ISmartRenderEngine;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F03FA8CE-879A-4d59-9B2C-26BB1CF83461")
    ISmartRenderEngine : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetGroupCompressor( 
            long Group,
            IBaseFilter *pCompressor) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetGroupCompressor( 
            long Group,
            IBaseFilter **pCompressor) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetFindCompressorCB( 
            IFindCompressorCB *pCallback) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISmartRenderEngineVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISmartRenderEngine * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISmartRenderEngine * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISmartRenderEngine * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetGroupCompressor )( 
            ISmartRenderEngine * This,
            long Group,
            IBaseFilter *pCompressor);
        
        HRESULT ( STDMETHODCALLTYPE *GetGroupCompressor )( 
            ISmartRenderEngine * This,
            long Group,
            IBaseFilter **pCompressor);
        
        HRESULT ( STDMETHODCALLTYPE *SetFindCompressorCB )( 
            ISmartRenderEngine * This,
            IFindCompressorCB *pCallback);
        
        END_INTERFACE
    } ISmartRenderEngineVtbl;

    interface ISmartRenderEngine
    {
        CONST_VTBL struct ISmartRenderEngineVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISmartRenderEngine_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ISmartRenderEngine_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ISmartRenderEngine_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ISmartRenderEngine_SetGroupCompressor(This,Group,pCompressor)	\
    (This)->lpVtbl -> SetGroupCompressor(This,Group,pCompressor)

#define ISmartRenderEngine_GetGroupCompressor(This,Group,pCompressor)	\
    (This)->lpVtbl -> GetGroupCompressor(This,Group,pCompressor)

#define ISmartRenderEngine_SetFindCompressorCB(This,pCallback)	\
    (This)->lpVtbl -> SetFindCompressorCB(This,pCallback)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE ISmartRenderEngine_SetGroupCompressor_Proxy( 
    ISmartRenderEngine * This,
    long Group,
    IBaseFilter *pCompressor);


void __RPC_STUB ISmartRenderEngine_SetGroupCompressor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE ISmartRenderEngine_GetGroupCompressor_Proxy( 
    ISmartRenderEngine * This,
    long Group,
    IBaseFilter **pCompressor);


void __RPC_STUB ISmartRenderEngine_GetGroupCompressor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE ISmartRenderEngine_SetFindCompressorCB_Proxy( 
    ISmartRenderEngine * This,
    IFindCompressorCB *pCallback);


void __RPC_STUB ISmartRenderEngine_SetFindCompressorCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ISmartRenderEngine_INTERFACE_DEFINED__ */


#ifndef __IAMTimelineObj_INTERFACE_DEFINED__
#define __IAMTimelineObj_INTERFACE_DEFINED__

/* interface IAMTimelineObj */
/* [unique][helpstring][uuid][local][object] */ 


EXTERN_C const IID IID_IAMTimelineObj;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("78530B77-61F9-11D2-8CAD-00A024580902")
    IAMTimelineObj : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetStartStop( 
            REFERENCE_TIME *pStart,
            REFERENCE_TIME *pStop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetStartStop2( 
            REFTIME *pStart,
            REFTIME *pStop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE FixTimes( 
            REFERENCE_TIME *pStart,
            REFERENCE_TIME *pStop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE FixTimes2( 
            REFTIME *pStart,
            REFTIME *pStop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetStartStop( 
            REFERENCE_TIME Start,
            REFERENCE_TIME Stop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetStartStop2( 
            REFTIME Start,
            REFTIME Stop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPropertySetter( 
            /* [retval][out] */ IPropertySetter **pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetPropertySetter( 
            IPropertySetter *newVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSubObject( 
            /* [retval][out] */ IUnknown **pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetSubObject( 
            IUnknown *newVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetSubObjectGUID( 
            GUID newVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetSubObjectGUIDB( 
            BSTR newVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSubObjectGUID( 
            GUID *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSubObjectGUIDB( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSubObjectLoaded( 
            BOOL *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetTimelineType( 
            TIMELINE_MAJOR_TYPE *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetTimelineType( 
            TIMELINE_MAJOR_TYPE newVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetUserID( 
            long *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetUserID( 
            long newVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetGenID( 
            long *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetUserName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetUserName( 
            BSTR newVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetUserData( 
            BYTE *pData,
            long *pSize) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetUserData( 
            BYTE *pData,
            long Size) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMuted( 
            BOOL *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetMuted( 
            BOOL newVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetLocked( 
            BOOL *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetLocked( 
            BOOL newVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDirtyRange( 
            REFERENCE_TIME *pStart,
            REFERENCE_TIME *pStop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDirtyRange2( 
            REFTIME *pStart,
            REFTIME *pStop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetDirtyRange( 
            REFERENCE_TIME Start,
            REFERENCE_TIME Stop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetDirtyRange2( 
            REFTIME Start,
            REFTIME Stop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ClearDirty( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RemoveAll( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetTimelineNoRef( 
            IAMTimeline **ppResult) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetGroupIBelongTo( 
            /* [out] */ IAMTimelineGroup **ppGroup) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetEmbedDepth( 
            long *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMTimelineObjVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMTimelineObj * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMTimelineObj * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMTimelineObj * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetStartStop )( 
            IAMTimelineObj * This,
            REFERENCE_TIME *pStart,
            REFERENCE_TIME *pStop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetStartStop2 )( 
            IAMTimelineObj * This,
            REFTIME *pStart,
            REFTIME *pStop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *FixTimes )( 
            IAMTimelineObj * This,
            REFERENCE_TIME *pStart,
            REFERENCE_TIME *pStop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *FixTimes2 )( 
            IAMTimelineObj * This,
            REFTIME *pStart,
            REFTIME *pStop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetStartStop )( 
            IAMTimelineObj * This,
            REFERENCE_TIME Start,
            REFERENCE_TIME Stop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetStartStop2 )( 
            IAMTimelineObj * This,
            REFTIME Start,
            REFTIME Stop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetPropertySetter )( 
            IAMTimelineObj * This,
            /* [retval][out] */ IPropertySetter **pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetPropertySetter )( 
            IAMTimelineObj * This,
            IPropertySetter *newVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetSubObject )( 
            IAMTimelineObj * This,
            /* [retval][out] */ IUnknown **pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetSubObject )( 
            IAMTimelineObj * This,
            IUnknown *newVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetSubObjectGUID )( 
            IAMTimelineObj * This,
            GUID newVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetSubObjectGUIDB )( 
            IAMTimelineObj * This,
            BSTR newVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetSubObjectGUID )( 
            IAMTimelineObj * This,
            GUID *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetSubObjectGUIDB )( 
            IAMTimelineObj * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetSubObjectLoaded )( 
            IAMTimelineObj * This,
            BOOL *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetTimelineType )( 
            IAMTimelineObj * This,
            TIMELINE_MAJOR_TYPE *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetTimelineType )( 
            IAMTimelineObj * This,
            TIMELINE_MAJOR_TYPE newVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetUserID )( 
            IAMTimelineObj * This,
            long *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetUserID )( 
            IAMTimelineObj * This,
            long newVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetGenID )( 
            IAMTimelineObj * This,
            long *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetUserName )( 
            IAMTimelineObj * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetUserName )( 
            IAMTimelineObj * This,
            BSTR newVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetUserData )( 
            IAMTimelineObj * This,
            BYTE *pData,
            long *pSize);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetUserData )( 
            IAMTimelineObj * This,
            BYTE *pData,
            long Size);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetMuted )( 
            IAMTimelineObj * This,
            BOOL *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetMuted )( 
            IAMTimelineObj * This,
            BOOL newVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetLocked )( 
            IAMTimelineObj * This,
            BOOL *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetLocked )( 
            IAMTimelineObj * This,
            BOOL newVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetDirtyRange )( 
            IAMTimelineObj * This,
            REFERENCE_TIME *pStart,
            REFERENCE_TIME *pStop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetDirtyRange2 )( 
            IAMTimelineObj * This,
            REFTIME *pStart,
            REFTIME *pStop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetDirtyRange )( 
            IAMTimelineObj * This,
            REFERENCE_TIME Start,
            REFERENCE_TIME Stop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetDirtyRange2 )( 
            IAMTimelineObj * This,
            REFTIME Start,
            REFTIME Stop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *ClearDirty )( 
            IAMTimelineObj * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IAMTimelineObj * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *RemoveAll )( 
            IAMTimelineObj * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTimelineNoRef )( 
            IAMTimelineObj * This,
            IAMTimeline **ppResult);
        
        HRESULT ( STDMETHODCALLTYPE *GetGroupIBelongTo )( 
            IAMTimelineObj * This,
            /* [out] */ IAMTimelineGroup **ppGroup);
        
        HRESULT ( STDMETHODCALLTYPE *GetEmbedDepth )( 
            IAMTimelineObj * This,
            long *pVal);
        
        END_INTERFACE
    } IAMTimelineObjVtbl;

    interface IAMTimelineObj
    {
        CONST_VTBL struct IAMTimelineObjVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMTimelineObj_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMTimelineObj_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMTimelineObj_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMTimelineObj_GetStartStop(This,pStart,pStop)	\
    (This)->lpVtbl -> GetStartStop(This,pStart,pStop)

#define IAMTimelineObj_GetStartStop2(This,pStart,pStop)	\
    (This)->lpVtbl -> GetStartStop2(This,pStart,pStop)

#define IAMTimelineObj_FixTimes(This,pStart,pStop)	\
    (This)->lpVtbl -> FixTimes(This,pStart,pStop)

#define IAMTimelineObj_FixTimes2(This,pStart,pStop)	\
    (This)->lpVtbl -> FixTimes2(This,pStart,pStop)

#define IAMTimelineObj_SetStartStop(This,Start,Stop)	\
    (This)->lpVtbl -> SetStartStop(This,Start,Stop)

#define IAMTimelineObj_SetStartStop2(This,Start,Stop)	\
    (This)->lpVtbl -> SetStartStop2(This,Start,Stop)

#define IAMTimelineObj_GetPropertySetter(This,pVal)	\
    (This)->lpVtbl -> GetPropertySetter(This,pVal)

#define IAMTimelineObj_SetPropertySetter(This,newVal)	\
    (This)->lpVtbl -> SetPropertySetter(This,newVal)

#define IAMTimelineObj_GetSubObject(This,pVal)	\
    (This)->lpVtbl -> GetSubObject(This,pVal)

#define IAMTimelineObj_SetSubObject(This,newVal)	\
    (This)->lpVtbl -> SetSubObject(This,newVal)

#define IAMTimelineObj_SetSubObjectGUID(This,newVal)	\
    (This)->lpVtbl -> SetSubObjectGUID(This,newVal)

#define IAMTimelineObj_SetSubObjectGUIDB(This,newVal)	\
    (This)->lpVtbl -> SetSubObjectGUIDB(This,newVal)

#define IAMTimelineObj_GetSubObjectGUID(This,pVal)	\
    (This)->lpVtbl -> GetSubObjectGUID(This,pVal)

#define IAMTimelineObj_GetSubObjectGUIDB(This,pVal)	\
    (This)->lpVtbl -> GetSubObjectGUIDB(This,pVal)

#define IAMTimelineObj_GetSubObjectLoaded(This,pVal)	\
    (This)->lpVtbl -> GetSubObjectLoaded(This,pVal)

#define IAMTimelineObj_GetTimelineType(This,pVal)	\
    (This)->lpVtbl -> GetTimelineType(This,pVal)

#define IAMTimelineObj_SetTimelineType(This,newVal)	\
    (This)->lpVtbl -> SetTimelineType(This,newVal)

#define IAMTimelineObj_GetUserID(This,pVal)	\
    (This)->lpVtbl -> GetUserID(This,pVal)

#define IAMTimelineObj_SetUserID(This,newVal)	\
    (This)->lpVtbl -> SetUserID(This,newVal)

#define IAMTimelineObj_GetGenID(This,pVal)	\
    (This)->lpVtbl -> GetGenID(This,pVal)

#define IAMTimelineObj_GetUserName(This,pVal)	\
    (This)->lpVtbl -> GetUserName(This,pVal)

#define IAMTimelineObj_SetUserName(This,newVal)	\
    (This)->lpVtbl -> SetUserName(This,newVal)

#define IAMTimelineObj_GetUserData(This,pData,pSize)	\
    (This)->lpVtbl -> GetUserData(This,pData,pSize)

#define IAMTimelineObj_SetUserData(This,pData,Size)	\
    (This)->lpVtbl -> SetUserData(This,pData,Size)

#define IAMTimelineObj_GetMuted(This,pVal)	\
    (This)->lpVtbl -> GetMuted(This,pVal)

#define IAMTimelineObj_SetMuted(This,newVal)	\
    (This)->lpVtbl -> SetMuted(This,newVal)

#define IAMTimelineObj_GetLocked(This,pVal)	\
    (This)->lpVtbl -> GetLocked(This,pVal)

#define IAMTimelineObj_SetLocked(This,newVal)	\
    (This)->lpVtbl -> SetLocked(This,newVal)

#define IAMTimelineObj_GetDirtyRange(This,pStart,pStop)	\
    (This)->lpVtbl -> GetDirtyRange(This,pStart,pStop)

#define IAMTimelineObj_GetDirtyRange2(This,pStart,pStop)	\
    (This)->lpVtbl -> GetDirtyRange2(This,pStart,pStop)

#define IAMTimelineObj_SetDirtyRange(This,Start,Stop)	\
    (This)->lpVtbl -> SetDirtyRange(This,Start,Stop)

#define IAMTimelineObj_SetDirtyRange2(This,Start,Stop)	\
    (This)->lpVtbl -> SetDirtyRange2(This,Start,Stop)

#define IAMTimelineObj_ClearDirty(This)	\
    (This)->lpVtbl -> ClearDirty(This)

#define IAMTimelineObj_Remove(This)	\
    (This)->lpVtbl -> Remove(This)

#define IAMTimelineObj_RemoveAll(This)	\
    (This)->lpVtbl -> RemoveAll(This)

#define IAMTimelineObj_GetTimelineNoRef(This,ppResult)	\
    (This)->lpVtbl -> GetTimelineNoRef(This,ppResult)

#define IAMTimelineObj_GetGroupIBelongTo(This,ppGroup)	\
    (This)->lpVtbl -> GetGroupIBelongTo(This,ppGroup)

#define IAMTimelineObj_GetEmbedDepth(This,pVal)	\
    (This)->lpVtbl -> GetEmbedDepth(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetStartStop_Proxy( 
    IAMTimelineObj * This,
    REFERENCE_TIME *pStart,
    REFERENCE_TIME *pStop);


void __RPC_STUB IAMTimelineObj_GetStartStop_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetStartStop2_Proxy( 
    IAMTimelineObj * This,
    REFTIME *pStart,
    REFTIME *pStop);


void __RPC_STUB IAMTimelineObj_GetStartStop2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_FixTimes_Proxy( 
    IAMTimelineObj * This,
    REFERENCE_TIME *pStart,
    REFERENCE_TIME *pStop);


void __RPC_STUB IAMTimelineObj_FixTimes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_FixTimes2_Proxy( 
    IAMTimelineObj * This,
    REFTIME *pStart,
    REFTIME *pStop);


void __RPC_STUB IAMTimelineObj_FixTimes2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetStartStop_Proxy( 
    IAMTimelineObj * This,
    REFERENCE_TIME Start,
    REFERENCE_TIME Stop);


void __RPC_STUB IAMTimelineObj_SetStartStop_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetStartStop2_Proxy( 
    IAMTimelineObj * This,
    REFTIME Start,
    REFTIME Stop);


void __RPC_STUB IAMTimelineObj_SetStartStop2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetPropertySetter_Proxy( 
    IAMTimelineObj * This,
    /* [retval][out] */ IPropertySetter **pVal);


void __RPC_STUB IAMTimelineObj_GetPropertySetter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetPropertySetter_Proxy( 
    IAMTimelineObj * This,
    IPropertySetter *newVal);


void __RPC_STUB IAMTimelineObj_SetPropertySetter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetSubObject_Proxy( 
    IAMTimelineObj * This,
    /* [retval][out] */ IUnknown **pVal);


void __RPC_STUB IAMTimelineObj_GetSubObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetSubObject_Proxy( 
    IAMTimelineObj * This,
    IUnknown *newVal);


void __RPC_STUB IAMTimelineObj_SetSubObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetSubObjectGUID_Proxy( 
    IAMTimelineObj * This,
    GUID newVal);


void __RPC_STUB IAMTimelineObj_SetSubObjectGUID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetSubObjectGUIDB_Proxy( 
    IAMTimelineObj * This,
    BSTR newVal);


void __RPC_STUB IAMTimelineObj_SetSubObjectGUIDB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetSubObjectGUID_Proxy( 
    IAMTimelineObj * This,
    GUID *pVal);


void __RPC_STUB IAMTimelineObj_GetSubObjectGUID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetSubObjectGUIDB_Proxy( 
    IAMTimelineObj * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IAMTimelineObj_GetSubObjectGUIDB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetSubObjectLoaded_Proxy( 
    IAMTimelineObj * This,
    BOOL *pVal);


void __RPC_STUB IAMTimelineObj_GetSubObjectLoaded_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetTimelineType_Proxy( 
    IAMTimelineObj * This,
    TIMELINE_MAJOR_TYPE *pVal);


void __RPC_STUB IAMTimelineObj_GetTimelineType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetTimelineType_Proxy( 
    IAMTimelineObj * This,
    TIMELINE_MAJOR_TYPE newVal);


void __RPC_STUB IAMTimelineObj_SetTimelineType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetUserID_Proxy( 
    IAMTimelineObj * This,
    long *pVal);


void __RPC_STUB IAMTimelineObj_GetUserID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetUserID_Proxy( 
    IAMTimelineObj * This,
    long newVal);


void __RPC_STUB IAMTimelineObj_SetUserID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetGenID_Proxy( 
    IAMTimelineObj * This,
    long *pVal);


void __RPC_STUB IAMTimelineObj_GetGenID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetUserName_Proxy( 
    IAMTimelineObj * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IAMTimelineObj_GetUserName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetUserName_Proxy( 
    IAMTimelineObj * This,
    BSTR newVal);


void __RPC_STUB IAMTimelineObj_SetUserName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetUserData_Proxy( 
    IAMTimelineObj * This,
    BYTE *pData,
    long *pSize);


void __RPC_STUB IAMTimelineObj_GetUserData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetUserData_Proxy( 
    IAMTimelineObj * This,
    BYTE *pData,
    long Size);


void __RPC_STUB IAMTimelineObj_SetUserData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetMuted_Proxy( 
    IAMTimelineObj * This,
    BOOL *pVal);


void __RPC_STUB IAMTimelineObj_GetMuted_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetMuted_Proxy( 
    IAMTimelineObj * This,
    BOOL newVal);


void __RPC_STUB IAMTimelineObj_SetMuted_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetLocked_Proxy( 
    IAMTimelineObj * This,
    BOOL *pVal);


void __RPC_STUB IAMTimelineObj_GetLocked_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetLocked_Proxy( 
    IAMTimelineObj * This,
    BOOL newVal);


void __RPC_STUB IAMTimelineObj_SetLocked_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetDirtyRange_Proxy( 
    IAMTimelineObj * This,
    REFERENCE_TIME *pStart,
    REFERENCE_TIME *pStop);


void __RPC_STUB IAMTimelineObj_GetDirtyRange_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetDirtyRange2_Proxy( 
    IAMTimelineObj * This,
    REFTIME *pStart,
    REFTIME *pStop);


void __RPC_STUB IAMTimelineObj_GetDirtyRange2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetDirtyRange_Proxy( 
    IAMTimelineObj * This,
    REFERENCE_TIME Start,
    REFERENCE_TIME Stop);


void __RPC_STUB IAMTimelineObj_SetDirtyRange_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_SetDirtyRange2_Proxy( 
    IAMTimelineObj * This,
    REFTIME Start,
    REFTIME Stop);


void __RPC_STUB IAMTimelineObj_SetDirtyRange2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_ClearDirty_Proxy( 
    IAMTimelineObj * This);


void __RPC_STUB IAMTimelineObj_ClearDirty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_Remove_Proxy( 
    IAMTimelineObj * This);


void __RPC_STUB IAMTimelineObj_Remove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineObj_RemoveAll_Proxy( 
    IAMTimelineObj * This);


void __RPC_STUB IAMTimelineObj_RemoveAll_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetTimelineNoRef_Proxy( 
    IAMTimelineObj * This,
    IAMTimeline **ppResult);


void __RPC_STUB IAMTimelineObj_GetTimelineNoRef_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetGroupIBelongTo_Proxy( 
    IAMTimelineObj * This,
    /* [out] */ IAMTimelineGroup **ppGroup);


void __RPC_STUB IAMTimelineObj_GetGroupIBelongTo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineObj_GetEmbedDepth_Proxy( 
    IAMTimelineObj * This,
    long *pVal);


void __RPC_STUB IAMTimelineObj_GetEmbedDepth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMTimelineObj_INTERFACE_DEFINED__ */


#ifndef __IAMTimelineEffectable_INTERFACE_DEFINED__
#define __IAMTimelineEffectable_INTERFACE_DEFINED__

/* interface IAMTimelineEffectable */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMTimelineEffectable;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EAE58537-622E-11d2-8CAD-00A024580902")
    IAMTimelineEffectable : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EffectInsBefore( 
            IAMTimelineObj *pFX,
            long priority) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EffectSwapPriorities( 
            long PriorityA,
            long PriorityB) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EffectGetCount( 
            long *pCount) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetEffect( 
            /* [out] */ IAMTimelineObj **ppFx,
            long Which) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMTimelineEffectableVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMTimelineEffectable * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMTimelineEffectable * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMTimelineEffectable * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *EffectInsBefore )( 
            IAMTimelineEffectable * This,
            IAMTimelineObj *pFX,
            long priority);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *EffectSwapPriorities )( 
            IAMTimelineEffectable * This,
            long PriorityA,
            long PriorityB);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *EffectGetCount )( 
            IAMTimelineEffectable * This,
            long *pCount);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetEffect )( 
            IAMTimelineEffectable * This,
            /* [out] */ IAMTimelineObj **ppFx,
            long Which);
        
        END_INTERFACE
    } IAMTimelineEffectableVtbl;

    interface IAMTimelineEffectable
    {
        CONST_VTBL struct IAMTimelineEffectableVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMTimelineEffectable_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMTimelineEffectable_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMTimelineEffectable_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMTimelineEffectable_EffectInsBefore(This,pFX,priority)	\
    (This)->lpVtbl -> EffectInsBefore(This,pFX,priority)

#define IAMTimelineEffectable_EffectSwapPriorities(This,PriorityA,PriorityB)	\
    (This)->lpVtbl -> EffectSwapPriorities(This,PriorityA,PriorityB)

#define IAMTimelineEffectable_EffectGetCount(This,pCount)	\
    (This)->lpVtbl -> EffectGetCount(This,pCount)

#define IAMTimelineEffectable_GetEffect(This,ppFx,Which)	\
    (This)->lpVtbl -> GetEffect(This,ppFx,Which)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineEffectable_EffectInsBefore_Proxy( 
    IAMTimelineEffectable * This,
    IAMTimelineObj *pFX,
    long priority);


void __RPC_STUB IAMTimelineEffectable_EffectInsBefore_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineEffectable_EffectSwapPriorities_Proxy( 
    IAMTimelineEffectable * This,
    long PriorityA,
    long PriorityB);


void __RPC_STUB IAMTimelineEffectable_EffectSwapPriorities_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineEffectable_EffectGetCount_Proxy( 
    IAMTimelineEffectable * This,
    long *pCount);


void __RPC_STUB IAMTimelineEffectable_EffectGetCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineEffectable_GetEffect_Proxy( 
    IAMTimelineEffectable * This,
    /* [out] */ IAMTimelineObj **ppFx,
    long Which);


void __RPC_STUB IAMTimelineEffectable_GetEffect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMTimelineEffectable_INTERFACE_DEFINED__ */


#ifndef __IAMTimelineEffect_INTERFACE_DEFINED__
#define __IAMTimelineEffect_INTERFACE_DEFINED__

/* interface IAMTimelineEffect */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMTimelineEffect;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("BCE0C264-622D-11d2-8CAD-00A024580902")
    IAMTimelineEffect : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EffectGetPriority( 
            long *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMTimelineEffectVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMTimelineEffect * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMTimelineEffect * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMTimelineEffect * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *EffectGetPriority )( 
            IAMTimelineEffect * This,
            long *pVal);
        
        END_INTERFACE
    } IAMTimelineEffectVtbl;

    interface IAMTimelineEffect
    {
        CONST_VTBL struct IAMTimelineEffectVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMTimelineEffect_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMTimelineEffect_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMTimelineEffect_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMTimelineEffect_EffectGetPriority(This,pVal)	\
    (This)->lpVtbl -> EffectGetPriority(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineEffect_EffectGetPriority_Proxy( 
    IAMTimelineEffect * This,
    long *pVal);


void __RPC_STUB IAMTimelineEffect_EffectGetPriority_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMTimelineEffect_INTERFACE_DEFINED__ */


#ifndef __IAMTimelineTransable_INTERFACE_DEFINED__
#define __IAMTimelineTransable_INTERFACE_DEFINED__

/* interface IAMTimelineTransable */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMTimelineTransable;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("378FA386-622E-11d2-8CAD-00A024580902")
    IAMTimelineTransable : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE TransAdd( 
            IAMTimelineObj *pTrans) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE TransGetCount( 
            long *pCount) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetNextTrans( 
            /* [out] */ IAMTimelineObj **ppTrans,
            REFERENCE_TIME *pInOut) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetNextTrans2( 
            /* [out] */ IAMTimelineObj **ppTrans,
            REFTIME *pInOut) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetTransAtTime( 
            /* [out] */ IAMTimelineObj **ppObj,
            REFERENCE_TIME Time,
            long SearchDirection) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetTransAtTime2( 
            /* [out] */ IAMTimelineObj **ppObj,
            REFTIME Time,
            long SearchDirection) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMTimelineTransableVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMTimelineTransable * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMTimelineTransable * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMTimelineTransable * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *TransAdd )( 
            IAMTimelineTransable * This,
            IAMTimelineObj *pTrans);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *TransGetCount )( 
            IAMTimelineTransable * This,
            long *pCount);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetNextTrans )( 
            IAMTimelineTransable * This,
            /* [out] */ IAMTimelineObj **ppTrans,
            REFERENCE_TIME *pInOut);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetNextTrans2 )( 
            IAMTimelineTransable * This,
            /* [out] */ IAMTimelineObj **ppTrans,
            REFTIME *pInOut);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetTransAtTime )( 
            IAMTimelineTransable * This,
            /* [out] */ IAMTimelineObj **ppObj,
            REFERENCE_TIME Time,
            long SearchDirection);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetTransAtTime2 )( 
            IAMTimelineTransable * This,
            /* [out] */ IAMTimelineObj **ppObj,
            REFTIME Time,
            long SearchDirection);
        
        END_INTERFACE
    } IAMTimelineTransableVtbl;

    interface IAMTimelineTransable
    {
        CONST_VTBL struct IAMTimelineTransableVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMTimelineTransable_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMTimelineTransable_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMTimelineTransable_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMTimelineTransable_TransAdd(This,pTrans)	\
    (This)->lpVtbl -> TransAdd(This,pTrans)

#define IAMTimelineTransable_TransGetCount(This,pCount)	\
    (This)->lpVtbl -> TransGetCount(This,pCount)

#define IAMTimelineTransable_GetNextTrans(This,ppTrans,pInOut)	\
    (This)->lpVtbl -> GetNextTrans(This,ppTrans,pInOut)

#define IAMTimelineTransable_GetNextTrans2(This,ppTrans,pInOut)	\
    (This)->lpVtbl -> GetNextTrans2(This,ppTrans,pInOut)

#define IAMTimelineTransable_GetTransAtTime(This,ppObj,Time,SearchDirection)	\
    (This)->lpVtbl -> GetTransAtTime(This,ppObj,Time,SearchDirection)

#define IAMTimelineTransable_GetTransAtTime2(This,ppObj,Time,SearchDirection)	\
    (This)->lpVtbl -> GetTransAtTime2(This,ppObj,Time,SearchDirection)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTransable_TransAdd_Proxy( 
    IAMTimelineTransable * This,
    IAMTimelineObj *pTrans);


void __RPC_STUB IAMTimelineTransable_TransAdd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTransable_TransGetCount_Proxy( 
    IAMTimelineTransable * This,
    long *pCount);


void __RPC_STUB IAMTimelineTransable_TransGetCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTransable_GetNextTrans_Proxy( 
    IAMTimelineTransable * This,
    /* [out] */ IAMTimelineObj **ppTrans,
    REFERENCE_TIME *pInOut);


void __RPC_STUB IAMTimelineTransable_GetNextTrans_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTransable_GetNextTrans2_Proxy( 
    IAMTimelineTransable * This,
    /* [out] */ IAMTimelineObj **ppTrans,
    REFTIME *pInOut);


void __RPC_STUB IAMTimelineTransable_GetNextTrans2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTransable_GetTransAtTime_Proxy( 
    IAMTimelineTransable * This,
    /* [out] */ IAMTimelineObj **ppObj,
    REFERENCE_TIME Time,
    long SearchDirection);


void __RPC_STUB IAMTimelineTransable_GetTransAtTime_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTransable_GetTransAtTime2_Proxy( 
    IAMTimelineTransable * This,
    /* [out] */ IAMTimelineObj **ppObj,
    REFTIME Time,
    long SearchDirection);


void __RPC_STUB IAMTimelineTransable_GetTransAtTime2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMTimelineTransable_INTERFACE_DEFINED__ */


#ifndef __IAMTimelineSplittable_INTERFACE_DEFINED__
#define __IAMTimelineSplittable_INTERFACE_DEFINED__

/* interface IAMTimelineSplittable */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMTimelineSplittable;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A0F840A0-D590-11d2-8D55-00A0C9441E20")
    IAMTimelineSplittable : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SplitAt( 
            REFERENCE_TIME Time) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SplitAt2( 
            REFTIME Time) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMTimelineSplittableVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMTimelineSplittable * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMTimelineSplittable * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMTimelineSplittable * This);
        
        HRESULT ( STDMETHODCALLTYPE *SplitAt )( 
            IAMTimelineSplittable * This,
            REFERENCE_TIME Time);
        
        HRESULT ( STDMETHODCALLTYPE *SplitAt2 )( 
            IAMTimelineSplittable * This,
            REFTIME Time);
        
        END_INTERFACE
    } IAMTimelineSplittableVtbl;

    interface IAMTimelineSplittable
    {
        CONST_VTBL struct IAMTimelineSplittableVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMTimelineSplittable_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMTimelineSplittable_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMTimelineSplittable_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMTimelineSplittable_SplitAt(This,Time)	\
    (This)->lpVtbl -> SplitAt(This,Time)

#define IAMTimelineSplittable_SplitAt2(This,Time)	\
    (This)->lpVtbl -> SplitAt2(This,Time)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IAMTimelineSplittable_SplitAt_Proxy( 
    IAMTimelineSplittable * This,
    REFERENCE_TIME Time);


void __RPC_STUB IAMTimelineSplittable_SplitAt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineSplittable_SplitAt2_Proxy( 
    IAMTimelineSplittable * This,
    REFTIME Time);


void __RPC_STUB IAMTimelineSplittable_SplitAt2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMTimelineSplittable_INTERFACE_DEFINED__ */


#ifndef __IAMTimelineTrans_INTERFACE_DEFINED__
#define __IAMTimelineTrans_INTERFACE_DEFINED__

/* interface IAMTimelineTrans */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMTimelineTrans;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("BCE0C265-622D-11d2-8CAD-00A024580902")
    IAMTimelineTrans : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCutPoint( 
            REFERENCE_TIME *pTLTime) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCutPoint2( 
            REFTIME *pTLTime) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetCutPoint( 
            REFERENCE_TIME TLTime) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetCutPoint2( 
            REFTIME TLTime) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSwapInputs( 
            BOOL *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetSwapInputs( 
            BOOL pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCutsOnly( 
            BOOL *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetCutsOnly( 
            BOOL pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMTimelineTransVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMTimelineTrans * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMTimelineTrans * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMTimelineTrans * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetCutPoint )( 
            IAMTimelineTrans * This,
            REFERENCE_TIME *pTLTime);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetCutPoint2 )( 
            IAMTimelineTrans * This,
            REFTIME *pTLTime);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetCutPoint )( 
            IAMTimelineTrans * This,
            REFERENCE_TIME TLTime);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetCutPoint2 )( 
            IAMTimelineTrans * This,
            REFTIME TLTime);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetSwapInputs )( 
            IAMTimelineTrans * This,
            BOOL *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetSwapInputs )( 
            IAMTimelineTrans * This,
            BOOL pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetCutsOnly )( 
            IAMTimelineTrans * This,
            BOOL *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetCutsOnly )( 
            IAMTimelineTrans * This,
            BOOL pVal);
        
        END_INTERFACE
    } IAMTimelineTransVtbl;

    interface IAMTimelineTrans
    {
        CONST_VTBL struct IAMTimelineTransVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMTimelineTrans_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMTimelineTrans_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMTimelineTrans_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMTimelineTrans_GetCutPoint(This,pTLTime)	\
    (This)->lpVtbl -> GetCutPoint(This,pTLTime)

#define IAMTimelineTrans_GetCutPoint2(This,pTLTime)	\
    (This)->lpVtbl -> GetCutPoint2(This,pTLTime)

#define IAMTimelineTrans_SetCutPoint(This,TLTime)	\
    (This)->lpVtbl -> SetCutPoint(This,TLTime)

#define IAMTimelineTrans_SetCutPoint2(This,TLTime)	\
    (This)->lpVtbl -> SetCutPoint2(This,TLTime)

#define IAMTimelineTrans_GetSwapInputs(This,pVal)	\
    (This)->lpVtbl -> GetSwapInputs(This,pVal)

#define IAMTimelineTrans_SetSwapInputs(This,pVal)	\
    (This)->lpVtbl -> SetSwapInputs(This,pVal)

#define IAMTimelineTrans_GetCutsOnly(This,pVal)	\
    (This)->lpVtbl -> GetCutsOnly(This,pVal)

#define IAMTimelineTrans_SetCutsOnly(This,pVal)	\
    (This)->lpVtbl -> SetCutsOnly(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrans_GetCutPoint_Proxy( 
    IAMTimelineTrans * This,
    REFERENCE_TIME *pTLTime);


void __RPC_STUB IAMTimelineTrans_GetCutPoint_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrans_GetCutPoint2_Proxy( 
    IAMTimelineTrans * This,
    REFTIME *pTLTime);


void __RPC_STUB IAMTimelineTrans_GetCutPoint2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrans_SetCutPoint_Proxy( 
    IAMTimelineTrans * This,
    REFERENCE_TIME TLTime);


void __RPC_STUB IAMTimelineTrans_SetCutPoint_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrans_SetCutPoint2_Proxy( 
    IAMTimelineTrans * This,
    REFTIME TLTime);


void __RPC_STUB IAMTimelineTrans_SetCutPoint2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrans_GetSwapInputs_Proxy( 
    IAMTimelineTrans * This,
    BOOL *pVal);


void __RPC_STUB IAMTimelineTrans_GetSwapInputs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrans_SetSwapInputs_Proxy( 
    IAMTimelineTrans * This,
    BOOL pVal);


void __RPC_STUB IAMTimelineTrans_SetSwapInputs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrans_GetCutsOnly_Proxy( 
    IAMTimelineTrans * This,
    BOOL *pVal);


void __RPC_STUB IAMTimelineTrans_GetCutsOnly_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrans_SetCutsOnly_Proxy( 
    IAMTimelineTrans * This,
    BOOL pVal);


void __RPC_STUB IAMTimelineTrans_SetCutsOnly_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMTimelineTrans_INTERFACE_DEFINED__ */


#ifndef __IAMTimelineSrc_INTERFACE_DEFINED__
#define __IAMTimelineSrc_INTERFACE_DEFINED__

/* interface IAMTimelineSrc */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMTimelineSrc;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("78530B79-61F9-11D2-8CAD-00A024580902")
    IAMTimelineSrc : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMediaTimes( 
            REFERENCE_TIME *pStart,
            REFERENCE_TIME *pStop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMediaTimes2( 
            REFTIME *pStart,
            REFTIME *pStop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ModifyStopTime( 
            REFERENCE_TIME Stop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ModifyStopTime2( 
            REFTIME Stop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE FixMediaTimes( 
            REFERENCE_TIME *pStart,
            REFERENCE_TIME *pStop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE FixMediaTimes2( 
            REFTIME *pStart,
            REFTIME *pStop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetMediaTimes( 
            REFERENCE_TIME Start,
            REFERENCE_TIME Stop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetMediaTimes2( 
            REFTIME Start,
            REFTIME Stop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetMediaLength( 
            REFERENCE_TIME Length) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetMediaLength2( 
            REFTIME Length) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMediaLength( 
            REFERENCE_TIME *pLength) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMediaLength2( 
            REFTIME *pLength) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMediaName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetMediaName( 
            BSTR newVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SpliceWithNext( 
            IAMTimelineObj *pNext) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetStreamNumber( 
            long *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetStreamNumber( 
            long Val) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IsNormalRate( 
            BOOL *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDefaultFPS( 
            double *pFPS) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetDefaultFPS( 
            double FPS) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetStretchMode( 
            int *pnStretchMode) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetStretchMode( 
            int nStretchMode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMTimelineSrcVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMTimelineSrc * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMTimelineSrc * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMTimelineSrc * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetMediaTimes )( 
            IAMTimelineSrc * This,
            REFERENCE_TIME *pStart,
            REFERENCE_TIME *pStop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetMediaTimes2 )( 
            IAMTimelineSrc * This,
            REFTIME *pStart,
            REFTIME *pStop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *ModifyStopTime )( 
            IAMTimelineSrc * This,
            REFERENCE_TIME Stop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *ModifyStopTime2 )( 
            IAMTimelineSrc * This,
            REFTIME Stop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *FixMediaTimes )( 
            IAMTimelineSrc * This,
            REFERENCE_TIME *pStart,
            REFERENCE_TIME *pStop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *FixMediaTimes2 )( 
            IAMTimelineSrc * This,
            REFTIME *pStart,
            REFTIME *pStop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetMediaTimes )( 
            IAMTimelineSrc * This,
            REFERENCE_TIME Start,
            REFERENCE_TIME Stop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetMediaTimes2 )( 
            IAMTimelineSrc * This,
            REFTIME Start,
            REFTIME Stop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetMediaLength )( 
            IAMTimelineSrc * This,
            REFERENCE_TIME Length);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetMediaLength2 )( 
            IAMTimelineSrc * This,
            REFTIME Length);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetMediaLength )( 
            IAMTimelineSrc * This,
            REFERENCE_TIME *pLength);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetMediaLength2 )( 
            IAMTimelineSrc * This,
            REFTIME *pLength);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetMediaName )( 
            IAMTimelineSrc * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetMediaName )( 
            IAMTimelineSrc * This,
            BSTR newVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SpliceWithNext )( 
            IAMTimelineSrc * This,
            IAMTimelineObj *pNext);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetStreamNumber )( 
            IAMTimelineSrc * This,
            long *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetStreamNumber )( 
            IAMTimelineSrc * This,
            long Val);
        
        HRESULT ( STDMETHODCALLTYPE *IsNormalRate )( 
            IAMTimelineSrc * This,
            BOOL *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetDefaultFPS )( 
            IAMTimelineSrc * This,
            double *pFPS);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetDefaultFPS )( 
            IAMTimelineSrc * This,
            double FPS);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetStretchMode )( 
            IAMTimelineSrc * This,
            int *pnStretchMode);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetStretchMode )( 
            IAMTimelineSrc * This,
            int nStretchMode);
        
        END_INTERFACE
    } IAMTimelineSrcVtbl;

    interface IAMTimelineSrc
    {
        CONST_VTBL struct IAMTimelineSrcVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMTimelineSrc_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMTimelineSrc_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMTimelineSrc_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMTimelineSrc_GetMediaTimes(This,pStart,pStop)	\
    (This)->lpVtbl -> GetMediaTimes(This,pStart,pStop)

#define IAMTimelineSrc_GetMediaTimes2(This,pStart,pStop)	\
    (This)->lpVtbl -> GetMediaTimes2(This,pStart,pStop)

#define IAMTimelineSrc_ModifyStopTime(This,Stop)	\
    (This)->lpVtbl -> ModifyStopTime(This,Stop)

#define IAMTimelineSrc_ModifyStopTime2(This,Stop)	\
    (This)->lpVtbl -> ModifyStopTime2(This,Stop)

#define IAMTimelineSrc_FixMediaTimes(This,pStart,pStop)	\
    (This)->lpVtbl -> FixMediaTimes(This,pStart,pStop)

#define IAMTimelineSrc_FixMediaTimes2(This,pStart,pStop)	\
    (This)->lpVtbl -> FixMediaTimes2(This,pStart,pStop)

#define IAMTimelineSrc_SetMediaTimes(This,Start,Stop)	\
    (This)->lpVtbl -> SetMediaTimes(This,Start,Stop)

#define IAMTimelineSrc_SetMediaTimes2(This,Start,Stop)	\
    (This)->lpVtbl -> SetMediaTimes2(This,Start,Stop)

#define IAMTimelineSrc_SetMediaLength(This,Length)	\
    (This)->lpVtbl -> SetMediaLength(This,Length)

#define IAMTimelineSrc_SetMediaLength2(This,Length)	\
    (This)->lpVtbl -> SetMediaLength2(This,Length)

#define IAMTimelineSrc_GetMediaLength(This,pLength)	\
    (This)->lpVtbl -> GetMediaLength(This,pLength)

#define IAMTimelineSrc_GetMediaLength2(This,pLength)	\
    (This)->lpVtbl -> GetMediaLength2(This,pLength)

#define IAMTimelineSrc_GetMediaName(This,pVal)	\
    (This)->lpVtbl -> GetMediaName(This,pVal)

#define IAMTimelineSrc_SetMediaName(This,newVal)	\
    (This)->lpVtbl -> SetMediaName(This,newVal)

#define IAMTimelineSrc_SpliceWithNext(This,pNext)	\
    (This)->lpVtbl -> SpliceWithNext(This,pNext)

#define IAMTimelineSrc_GetStreamNumber(This,pVal)	\
    (This)->lpVtbl -> GetStreamNumber(This,pVal)

#define IAMTimelineSrc_SetStreamNumber(This,Val)	\
    (This)->lpVtbl -> SetStreamNumber(This,Val)

#define IAMTimelineSrc_IsNormalRate(This,pVal)	\
    (This)->lpVtbl -> IsNormalRate(This,pVal)

#define IAMTimelineSrc_GetDefaultFPS(This,pFPS)	\
    (This)->lpVtbl -> GetDefaultFPS(This,pFPS)

#define IAMTimelineSrc_SetDefaultFPS(This,FPS)	\
    (This)->lpVtbl -> SetDefaultFPS(This,FPS)

#define IAMTimelineSrc_GetStretchMode(This,pnStretchMode)	\
    (This)->lpVtbl -> GetStretchMode(This,pnStretchMode)

#define IAMTimelineSrc_SetStretchMode(This,nStretchMode)	\
    (This)->lpVtbl -> SetStretchMode(This,nStretchMode)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_GetMediaTimes_Proxy( 
    IAMTimelineSrc * This,
    REFERENCE_TIME *pStart,
    REFERENCE_TIME *pStop);


void __RPC_STUB IAMTimelineSrc_GetMediaTimes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_GetMediaTimes2_Proxy( 
    IAMTimelineSrc * This,
    REFTIME *pStart,
    REFTIME *pStop);


void __RPC_STUB IAMTimelineSrc_GetMediaTimes2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_ModifyStopTime_Proxy( 
    IAMTimelineSrc * This,
    REFERENCE_TIME Stop);


void __RPC_STUB IAMTimelineSrc_ModifyStopTime_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_ModifyStopTime2_Proxy( 
    IAMTimelineSrc * This,
    REFTIME Stop);


void __RPC_STUB IAMTimelineSrc_ModifyStopTime2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_FixMediaTimes_Proxy( 
    IAMTimelineSrc * This,
    REFERENCE_TIME *pStart,
    REFERENCE_TIME *pStop);


void __RPC_STUB IAMTimelineSrc_FixMediaTimes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_FixMediaTimes2_Proxy( 
    IAMTimelineSrc * This,
    REFTIME *pStart,
    REFTIME *pStop);


void __RPC_STUB IAMTimelineSrc_FixMediaTimes2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_SetMediaTimes_Proxy( 
    IAMTimelineSrc * This,
    REFERENCE_TIME Start,
    REFERENCE_TIME Stop);


void __RPC_STUB IAMTimelineSrc_SetMediaTimes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_SetMediaTimes2_Proxy( 
    IAMTimelineSrc * This,
    REFTIME Start,
    REFTIME Stop);


void __RPC_STUB IAMTimelineSrc_SetMediaTimes2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_SetMediaLength_Proxy( 
    IAMTimelineSrc * This,
    REFERENCE_TIME Length);


void __RPC_STUB IAMTimelineSrc_SetMediaLength_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_SetMediaLength2_Proxy( 
    IAMTimelineSrc * This,
    REFTIME Length);


void __RPC_STUB IAMTimelineSrc_SetMediaLength2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_GetMediaLength_Proxy( 
    IAMTimelineSrc * This,
    REFERENCE_TIME *pLength);


void __RPC_STUB IAMTimelineSrc_GetMediaLength_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_GetMediaLength2_Proxy( 
    IAMTimelineSrc * This,
    REFTIME *pLength);


void __RPC_STUB IAMTimelineSrc_GetMediaLength2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_GetMediaName_Proxy( 
    IAMTimelineSrc * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IAMTimelineSrc_GetMediaName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_SetMediaName_Proxy( 
    IAMTimelineSrc * This,
    BSTR newVal);


void __RPC_STUB IAMTimelineSrc_SetMediaName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_SpliceWithNext_Proxy( 
    IAMTimelineSrc * This,
    IAMTimelineObj *pNext);


void __RPC_STUB IAMTimelineSrc_SpliceWithNext_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_GetStreamNumber_Proxy( 
    IAMTimelineSrc * This,
    long *pVal);


void __RPC_STUB IAMTimelineSrc_GetStreamNumber_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_SetStreamNumber_Proxy( 
    IAMTimelineSrc * This,
    long Val);


void __RPC_STUB IAMTimelineSrc_SetStreamNumber_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineSrc_IsNormalRate_Proxy( 
    IAMTimelineSrc * This,
    BOOL *pVal);


void __RPC_STUB IAMTimelineSrc_IsNormalRate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_GetDefaultFPS_Proxy( 
    IAMTimelineSrc * This,
    double *pFPS);


void __RPC_STUB IAMTimelineSrc_GetDefaultFPS_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_SetDefaultFPS_Proxy( 
    IAMTimelineSrc * This,
    double FPS);


void __RPC_STUB IAMTimelineSrc_SetDefaultFPS_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_GetStretchMode_Proxy( 
    IAMTimelineSrc * This,
    int *pnStretchMode);


void __RPC_STUB IAMTimelineSrc_GetStretchMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineSrc_SetStretchMode_Proxy( 
    IAMTimelineSrc * This,
    int nStretchMode);


void __RPC_STUB IAMTimelineSrc_SetStretchMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMTimelineSrc_INTERFACE_DEFINED__ */


#ifndef __IAMTimelineTrack_INTERFACE_DEFINED__
#define __IAMTimelineTrack_INTERFACE_DEFINED__

/* interface IAMTimelineTrack */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMTimelineTrack;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EAE58538-622E-11d2-8CAD-00A024580902")
    IAMTimelineTrack : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SrcAdd( 
            IAMTimelineObj *pSource) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetNextSrc( 
            /* [out] */ IAMTimelineObj **ppSrc,
            REFERENCE_TIME *pInOut) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetNextSrc2( 
            /* [out] */ IAMTimelineObj **ppSrc,
            REFTIME *pInOut) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE MoveEverythingBy( 
            REFERENCE_TIME Start,
            REFERENCE_TIME MoveBy) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE MoveEverythingBy2( 
            REFTIME Start,
            REFTIME MoveBy) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSourcesCount( 
            long *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE AreYouBlank( 
            long *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSrcAtTime( 
            /* [out] */ IAMTimelineObj **ppSrc,
            REFERENCE_TIME Time,
            long SearchDirection) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSrcAtTime2( 
            /* [out] */ IAMTimelineObj **ppSrc,
            REFTIME Time,
            long SearchDirection) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE InsertSpace( 
            REFERENCE_TIME rtStart,
            REFERENCE_TIME rtEnd) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE InsertSpace2( 
            REFTIME rtStart,
            REFTIME rtEnd) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ZeroBetween( 
            REFERENCE_TIME rtStart,
            REFERENCE_TIME rtEnd) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ZeroBetween2( 
            REFTIME rtStart,
            REFTIME rtEnd) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetNextSrcEx( 
            IAMTimelineObj *pLast,
            /* [out] */ IAMTimelineObj **ppNext) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMTimelineTrackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMTimelineTrack * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMTimelineTrack * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMTimelineTrack * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SrcAdd )( 
            IAMTimelineTrack * This,
            IAMTimelineObj *pSource);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetNextSrc )( 
            IAMTimelineTrack * This,
            /* [out] */ IAMTimelineObj **ppSrc,
            REFERENCE_TIME *pInOut);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetNextSrc2 )( 
            IAMTimelineTrack * This,
            /* [out] */ IAMTimelineObj **ppSrc,
            REFTIME *pInOut);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *MoveEverythingBy )( 
            IAMTimelineTrack * This,
            REFERENCE_TIME Start,
            REFERENCE_TIME MoveBy);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *MoveEverythingBy2 )( 
            IAMTimelineTrack * This,
            REFTIME Start,
            REFTIME MoveBy);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetSourcesCount )( 
            IAMTimelineTrack * This,
            long *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *AreYouBlank )( 
            IAMTimelineTrack * This,
            long *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetSrcAtTime )( 
            IAMTimelineTrack * This,
            /* [out] */ IAMTimelineObj **ppSrc,
            REFERENCE_TIME Time,
            long SearchDirection);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetSrcAtTime2 )( 
            IAMTimelineTrack * This,
            /* [out] */ IAMTimelineObj **ppSrc,
            REFTIME Time,
            long SearchDirection);
        
        HRESULT ( STDMETHODCALLTYPE *InsertSpace )( 
            IAMTimelineTrack * This,
            REFERENCE_TIME rtStart,
            REFERENCE_TIME rtEnd);
        
        HRESULT ( STDMETHODCALLTYPE *InsertSpace2 )( 
            IAMTimelineTrack * This,
            REFTIME rtStart,
            REFTIME rtEnd);
        
        HRESULT ( STDMETHODCALLTYPE *ZeroBetween )( 
            IAMTimelineTrack * This,
            REFERENCE_TIME rtStart,
            REFERENCE_TIME rtEnd);
        
        HRESULT ( STDMETHODCALLTYPE *ZeroBetween2 )( 
            IAMTimelineTrack * This,
            REFTIME rtStart,
            REFTIME rtEnd);
        
        HRESULT ( STDMETHODCALLTYPE *GetNextSrcEx )( 
            IAMTimelineTrack * This,
            IAMTimelineObj *pLast,
            /* [out] */ IAMTimelineObj **ppNext);
        
        END_INTERFACE
    } IAMTimelineTrackVtbl;

    interface IAMTimelineTrack
    {
        CONST_VTBL struct IAMTimelineTrackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMTimelineTrack_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMTimelineTrack_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMTimelineTrack_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMTimelineTrack_SrcAdd(This,pSource)	\
    (This)->lpVtbl -> SrcAdd(This,pSource)

#define IAMTimelineTrack_GetNextSrc(This,ppSrc,pInOut)	\
    (This)->lpVtbl -> GetNextSrc(This,ppSrc,pInOut)

#define IAMTimelineTrack_GetNextSrc2(This,ppSrc,pInOut)	\
    (This)->lpVtbl -> GetNextSrc2(This,ppSrc,pInOut)

#define IAMTimelineTrack_MoveEverythingBy(This,Start,MoveBy)	\
    (This)->lpVtbl -> MoveEverythingBy(This,Start,MoveBy)

#define IAMTimelineTrack_MoveEverythingBy2(This,Start,MoveBy)	\
    (This)->lpVtbl -> MoveEverythingBy2(This,Start,MoveBy)

#define IAMTimelineTrack_GetSourcesCount(This,pVal)	\
    (This)->lpVtbl -> GetSourcesCount(This,pVal)

#define IAMTimelineTrack_AreYouBlank(This,pVal)	\
    (This)->lpVtbl -> AreYouBlank(This,pVal)

#define IAMTimelineTrack_GetSrcAtTime(This,ppSrc,Time,SearchDirection)	\
    (This)->lpVtbl -> GetSrcAtTime(This,ppSrc,Time,SearchDirection)

#define IAMTimelineTrack_GetSrcAtTime2(This,ppSrc,Time,SearchDirection)	\
    (This)->lpVtbl -> GetSrcAtTime2(This,ppSrc,Time,SearchDirection)

#define IAMTimelineTrack_InsertSpace(This,rtStart,rtEnd)	\
    (This)->lpVtbl -> InsertSpace(This,rtStart,rtEnd)

#define IAMTimelineTrack_InsertSpace2(This,rtStart,rtEnd)	\
    (This)->lpVtbl -> InsertSpace2(This,rtStart,rtEnd)

#define IAMTimelineTrack_ZeroBetween(This,rtStart,rtEnd)	\
    (This)->lpVtbl -> ZeroBetween(This,rtStart,rtEnd)

#define IAMTimelineTrack_ZeroBetween2(This,rtStart,rtEnd)	\
    (This)->lpVtbl -> ZeroBetween2(This,rtStart,rtEnd)

#define IAMTimelineTrack_GetNextSrcEx(This,pLast,ppNext)	\
    (This)->lpVtbl -> GetNextSrcEx(This,pLast,ppNext)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrack_SrcAdd_Proxy( 
    IAMTimelineTrack * This,
    IAMTimelineObj *pSource);


void __RPC_STUB IAMTimelineTrack_SrcAdd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrack_GetNextSrc_Proxy( 
    IAMTimelineTrack * This,
    /* [out] */ IAMTimelineObj **ppSrc,
    REFERENCE_TIME *pInOut);


void __RPC_STUB IAMTimelineTrack_GetNextSrc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrack_GetNextSrc2_Proxy( 
    IAMTimelineTrack * This,
    /* [out] */ IAMTimelineObj **ppSrc,
    REFTIME *pInOut);


void __RPC_STUB IAMTimelineTrack_GetNextSrc2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrack_MoveEverythingBy_Proxy( 
    IAMTimelineTrack * This,
    REFERENCE_TIME Start,
    REFERENCE_TIME MoveBy);


void __RPC_STUB IAMTimelineTrack_MoveEverythingBy_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrack_MoveEverythingBy2_Proxy( 
    IAMTimelineTrack * This,
    REFTIME Start,
    REFTIME MoveBy);


void __RPC_STUB IAMTimelineTrack_MoveEverythingBy2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrack_GetSourcesCount_Proxy( 
    IAMTimelineTrack * This,
    long *pVal);


void __RPC_STUB IAMTimelineTrack_GetSourcesCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrack_AreYouBlank_Proxy( 
    IAMTimelineTrack * This,
    long *pVal);


void __RPC_STUB IAMTimelineTrack_AreYouBlank_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrack_GetSrcAtTime_Proxy( 
    IAMTimelineTrack * This,
    /* [out] */ IAMTimelineObj **ppSrc,
    REFERENCE_TIME Time,
    long SearchDirection);


void __RPC_STUB IAMTimelineTrack_GetSrcAtTime_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineTrack_GetSrcAtTime2_Proxy( 
    IAMTimelineTrack * This,
    /* [out] */ IAMTimelineObj **ppSrc,
    REFTIME Time,
    long SearchDirection);


void __RPC_STUB IAMTimelineTrack_GetSrcAtTime2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineTrack_InsertSpace_Proxy( 
    IAMTimelineTrack * This,
    REFERENCE_TIME rtStart,
    REFERENCE_TIME rtEnd);


void __RPC_STUB IAMTimelineTrack_InsertSpace_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineTrack_InsertSpace2_Proxy( 
    IAMTimelineTrack * This,
    REFTIME rtStart,
    REFTIME rtEnd);


void __RPC_STUB IAMTimelineTrack_InsertSpace2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineTrack_ZeroBetween_Proxy( 
    IAMTimelineTrack * This,
    REFERENCE_TIME rtStart,
    REFERENCE_TIME rtEnd);


void __RPC_STUB IAMTimelineTrack_ZeroBetween_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineTrack_ZeroBetween2_Proxy( 
    IAMTimelineTrack * This,
    REFTIME rtStart,
    REFTIME rtEnd);


void __RPC_STUB IAMTimelineTrack_ZeroBetween2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineTrack_GetNextSrcEx_Proxy( 
    IAMTimelineTrack * This,
    IAMTimelineObj *pLast,
    /* [out] */ IAMTimelineObj **ppNext);


void __RPC_STUB IAMTimelineTrack_GetNextSrcEx_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMTimelineTrack_INTERFACE_DEFINED__ */


#ifndef __IAMTimelineVirtualTrack_INTERFACE_DEFINED__
#define __IAMTimelineVirtualTrack_INTERFACE_DEFINED__

/* interface IAMTimelineVirtualTrack */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMTimelineVirtualTrack;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A8ED5F80-C2C7-11d2-8D39-00A0C9441E20")
    IAMTimelineVirtualTrack : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE TrackGetPriority( 
            long *pPriority) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetTrackDirty( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMTimelineVirtualTrackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMTimelineVirtualTrack * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMTimelineVirtualTrack * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMTimelineVirtualTrack * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *TrackGetPriority )( 
            IAMTimelineVirtualTrack * This,
            long *pPriority);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetTrackDirty )( 
            IAMTimelineVirtualTrack * This);
        
        END_INTERFACE
    } IAMTimelineVirtualTrackVtbl;

    interface IAMTimelineVirtualTrack
    {
        CONST_VTBL struct IAMTimelineVirtualTrackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMTimelineVirtualTrack_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMTimelineVirtualTrack_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMTimelineVirtualTrack_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMTimelineVirtualTrack_TrackGetPriority(This,pPriority)	\
    (This)->lpVtbl -> TrackGetPriority(This,pPriority)

#define IAMTimelineVirtualTrack_SetTrackDirty(This)	\
    (This)->lpVtbl -> SetTrackDirty(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineVirtualTrack_TrackGetPriority_Proxy( 
    IAMTimelineVirtualTrack * This,
    long *pPriority);


void __RPC_STUB IAMTimelineVirtualTrack_TrackGetPriority_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineVirtualTrack_SetTrackDirty_Proxy( 
    IAMTimelineVirtualTrack * This);


void __RPC_STUB IAMTimelineVirtualTrack_SetTrackDirty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMTimelineVirtualTrack_INTERFACE_DEFINED__ */


#ifndef __IAMTimelineComp_INTERFACE_DEFINED__
#define __IAMTimelineComp_INTERFACE_DEFINED__

/* interface IAMTimelineComp */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMTimelineComp;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EAE58536-622E-11d2-8CAD-00A024580902")
    IAMTimelineComp : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE VTrackInsBefore( 
            IAMTimelineObj *pVirtualTrack,
            long Priority) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE VTrackSwapPriorities( 
            long VirtualTrackA,
            long VirtualTrackB) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE VTrackGetCount( 
            long *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetVTrack( 
            /* [out] */ IAMTimelineObj **ppVirtualTrack,
            long Which) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCountOfType( 
            long *pVal,
            long *pValWithComps,
            TIMELINE_MAJOR_TYPE MajorType) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetRecursiveLayerOfType( 
            /* [out] */ IAMTimelineObj **ppVirtualTrack,
            long WhichLayer,
            TIMELINE_MAJOR_TYPE Type) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetRecursiveLayerOfTypeI( 
            /* [out] */ IAMTimelineObj **ppVirtualTrack,
            /* [out][in] */ long *pWhichLayer,
            TIMELINE_MAJOR_TYPE Type) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetNextVTrack( 
            IAMTimelineObj *pVirtualTrack,
            /* [out] */ IAMTimelineObj **ppNextVirtualTrack) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMTimelineCompVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMTimelineComp * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMTimelineComp * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMTimelineComp * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *VTrackInsBefore )( 
            IAMTimelineComp * This,
            IAMTimelineObj *pVirtualTrack,
            long Priority);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *VTrackSwapPriorities )( 
            IAMTimelineComp * This,
            long VirtualTrackA,
            long VirtualTrackB);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *VTrackGetCount )( 
            IAMTimelineComp * This,
            long *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetVTrack )( 
            IAMTimelineComp * This,
            /* [out] */ IAMTimelineObj **ppVirtualTrack,
            long Which);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetCountOfType )( 
            IAMTimelineComp * This,
            long *pVal,
            long *pValWithComps,
            TIMELINE_MAJOR_TYPE MajorType);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetRecursiveLayerOfType )( 
            IAMTimelineComp * This,
            /* [out] */ IAMTimelineObj **ppVirtualTrack,
            long WhichLayer,
            TIMELINE_MAJOR_TYPE Type);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetRecursiveLayerOfTypeI )( 
            IAMTimelineComp * This,
            /* [out] */ IAMTimelineObj **ppVirtualTrack,
            /* [out][in] */ long *pWhichLayer,
            TIMELINE_MAJOR_TYPE Type);
        
        HRESULT ( STDMETHODCALLTYPE *GetNextVTrack )( 
            IAMTimelineComp * This,
            IAMTimelineObj *pVirtualTrack,
            /* [out] */ IAMTimelineObj **ppNextVirtualTrack);
        
        END_INTERFACE
    } IAMTimelineCompVtbl;

    interface IAMTimelineComp
    {
        CONST_VTBL struct IAMTimelineCompVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMTimelineComp_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMTimelineComp_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMTimelineComp_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMTimelineComp_VTrackInsBefore(This,pVirtualTrack,Priority)	\
    (This)->lpVtbl -> VTrackInsBefore(This,pVirtualTrack,Priority)

#define IAMTimelineComp_VTrackSwapPriorities(This,VirtualTrackA,VirtualTrackB)	\
    (This)->lpVtbl -> VTrackSwapPriorities(This,VirtualTrackA,VirtualTrackB)

#define IAMTimelineComp_VTrackGetCount(This,pVal)	\
    (This)->lpVtbl -> VTrackGetCount(This,pVal)

#define IAMTimelineComp_GetVTrack(This,ppVirtualTrack,Which)	\
    (This)->lpVtbl -> GetVTrack(This,ppVirtualTrack,Which)

#define IAMTimelineComp_GetCountOfType(This,pVal,pValWithComps,MajorType)	\
    (This)->lpVtbl -> GetCountOfType(This,pVal,pValWithComps,MajorType)

#define IAMTimelineComp_GetRecursiveLayerOfType(This,ppVirtualTrack,WhichLayer,Type)	\
    (This)->lpVtbl -> GetRecursiveLayerOfType(This,ppVirtualTrack,WhichLayer,Type)

#define IAMTimelineComp_GetRecursiveLayerOfTypeI(This,ppVirtualTrack,pWhichLayer,Type)	\
    (This)->lpVtbl -> GetRecursiveLayerOfTypeI(This,ppVirtualTrack,pWhichLayer,Type)

#define IAMTimelineComp_GetNextVTrack(This,pVirtualTrack,ppNextVirtualTrack)	\
    (This)->lpVtbl -> GetNextVTrack(This,pVirtualTrack,ppNextVirtualTrack)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineComp_VTrackInsBefore_Proxy( 
    IAMTimelineComp * This,
    IAMTimelineObj *pVirtualTrack,
    long Priority);


void __RPC_STUB IAMTimelineComp_VTrackInsBefore_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineComp_VTrackSwapPriorities_Proxy( 
    IAMTimelineComp * This,
    long VirtualTrackA,
    long VirtualTrackB);


void __RPC_STUB IAMTimelineComp_VTrackSwapPriorities_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineComp_VTrackGetCount_Proxy( 
    IAMTimelineComp * This,
    long *pVal);


void __RPC_STUB IAMTimelineComp_VTrackGetCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineComp_GetVTrack_Proxy( 
    IAMTimelineComp * This,
    /* [out] */ IAMTimelineObj **ppVirtualTrack,
    long Which);


void __RPC_STUB IAMTimelineComp_GetVTrack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineComp_GetCountOfType_Proxy( 
    IAMTimelineComp * This,
    long *pVal,
    long *pValWithComps,
    TIMELINE_MAJOR_TYPE MajorType);


void __RPC_STUB IAMTimelineComp_GetCountOfType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineComp_GetRecursiveLayerOfType_Proxy( 
    IAMTimelineComp * This,
    /* [out] */ IAMTimelineObj **ppVirtualTrack,
    long WhichLayer,
    TIMELINE_MAJOR_TYPE Type);


void __RPC_STUB IAMTimelineComp_GetRecursiveLayerOfType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineComp_GetRecursiveLayerOfTypeI_Proxy( 
    IAMTimelineComp * This,
    /* [out] */ IAMTimelineObj **ppVirtualTrack,
    /* [out][in] */ long *pWhichLayer,
    TIMELINE_MAJOR_TYPE Type);


void __RPC_STUB IAMTimelineComp_GetRecursiveLayerOfTypeI_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineComp_GetNextVTrack_Proxy( 
    IAMTimelineComp * This,
    IAMTimelineObj *pVirtualTrack,
    /* [out] */ IAMTimelineObj **ppNextVirtualTrack);


void __RPC_STUB IAMTimelineComp_GetNextVTrack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMTimelineComp_INTERFACE_DEFINED__ */


#ifndef __IAMTimelineGroup_INTERFACE_DEFINED__
#define __IAMTimelineGroup_INTERFACE_DEFINED__

/* interface IAMTimelineGroup */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMTimelineGroup;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9EED4F00-B8A6-11d2-8023-00C0DF10D434")
    IAMTimelineGroup : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetTimeline( 
            IAMTimeline *pTimeline) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetTimeline( 
            /* [out] */ IAMTimeline **ppTimeline) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPriority( 
            long *pPriority) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMediaType( 
            /* [out] */ AM_MEDIA_TYPE *__MIDL_0040) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetMediaType( 
            /* [in] */ AM_MEDIA_TYPE *__MIDL_0041) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetOutputFPS( 
            double FPS) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetOutputFPS( 
            double *pFPS) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetGroupName( 
            BSTR pGroupName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetGroupName( 
            /* [retval][out] */ BSTR *pGroupName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetPreviewMode( 
            BOOL fPreview) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPreviewMode( 
            BOOL *pfPreview) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetMediaTypeForVB( 
            /* [in] */ long Val) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetOutputBuffering( 
            /* [out] */ int *pnBuffer) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetOutputBuffering( 
            /* [in] */ int nBuffer) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetSmartRecompressFormat( 
            long *pFormat) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetSmartRecompressFormat( 
            long **ppFormat) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IsSmartRecompressFormatSet( 
            BOOL *pVal) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IsRecompressFormatDirty( 
            BOOL *pVal) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ClearRecompressFormatDirty( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetRecompFormatFromSource( 
            IAMTimelineSrc *pSource) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMTimelineGroupVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMTimelineGroup * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMTimelineGroup * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMTimelineGroup * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetTimeline )( 
            IAMTimelineGroup * This,
            IAMTimeline *pTimeline);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetTimeline )( 
            IAMTimelineGroup * This,
            /* [out] */ IAMTimeline **ppTimeline);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetPriority )( 
            IAMTimelineGroup * This,
            long *pPriority);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetMediaType )( 
            IAMTimelineGroup * This,
            /* [out] */ AM_MEDIA_TYPE *__MIDL_0040);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetMediaType )( 
            IAMTimelineGroup * This,
            /* [in] */ AM_MEDIA_TYPE *__MIDL_0041);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetOutputFPS )( 
            IAMTimelineGroup * This,
            double FPS);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetOutputFPS )( 
            IAMTimelineGroup * This,
            double *pFPS);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetGroupName )( 
            IAMTimelineGroup * This,
            BSTR pGroupName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetGroupName )( 
            IAMTimelineGroup * This,
            /* [retval][out] */ BSTR *pGroupName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetPreviewMode )( 
            IAMTimelineGroup * This,
            BOOL fPreview);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetPreviewMode )( 
            IAMTimelineGroup * This,
            BOOL *pfPreview);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetMediaTypeForVB )( 
            IAMTimelineGroup * This,
            /* [in] */ long Val);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetOutputBuffering )( 
            IAMTimelineGroup * This,
            /* [out] */ int *pnBuffer);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetOutputBuffering )( 
            IAMTimelineGroup * This,
            /* [in] */ int nBuffer);
        
        HRESULT ( STDMETHODCALLTYPE *SetSmartRecompressFormat )( 
            IAMTimelineGroup * This,
            long *pFormat);
        
        HRESULT ( STDMETHODCALLTYPE *GetSmartRecompressFormat )( 
            IAMTimelineGroup * This,
            long **ppFormat);
        
        HRESULT ( STDMETHODCALLTYPE *IsSmartRecompressFormatSet )( 
            IAMTimelineGroup * This,
            BOOL *pVal);
        
        HRESULT ( STDMETHODCALLTYPE *IsRecompressFormatDirty )( 
            IAMTimelineGroup * This,
            BOOL *pVal);
        
        HRESULT ( STDMETHODCALLTYPE *ClearRecompressFormatDirty )( 
            IAMTimelineGroup * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetRecompFormatFromSource )( 
            IAMTimelineGroup * This,
            IAMTimelineSrc *pSource);
        
        END_INTERFACE
    } IAMTimelineGroupVtbl;

    interface IAMTimelineGroup
    {
        CONST_VTBL struct IAMTimelineGroupVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMTimelineGroup_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMTimelineGroup_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMTimelineGroup_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMTimelineGroup_SetTimeline(This,pTimeline)	\
    (This)->lpVtbl -> SetTimeline(This,pTimeline)

#define IAMTimelineGroup_GetTimeline(This,ppTimeline)	\
    (This)->lpVtbl -> GetTimeline(This,ppTimeline)

#define IAMTimelineGroup_GetPriority(This,pPriority)	\
    (This)->lpVtbl -> GetPriority(This,pPriority)

#define IAMTimelineGroup_GetMediaType(This,__MIDL_0040)	\
    (This)->lpVtbl -> GetMediaType(This,__MIDL_0040)

#define IAMTimelineGroup_SetMediaType(This,__MIDL_0041)	\
    (This)->lpVtbl -> SetMediaType(This,__MIDL_0041)

#define IAMTimelineGroup_SetOutputFPS(This,FPS)	\
    (This)->lpVtbl -> SetOutputFPS(This,FPS)

#define IAMTimelineGroup_GetOutputFPS(This,pFPS)	\
    (This)->lpVtbl -> GetOutputFPS(This,pFPS)

#define IAMTimelineGroup_SetGroupName(This,pGroupName)	\
    (This)->lpVtbl -> SetGroupName(This,pGroupName)

#define IAMTimelineGroup_GetGroupName(This,pGroupName)	\
    (This)->lpVtbl -> GetGroupName(This,pGroupName)

#define IAMTimelineGroup_SetPreviewMode(This,fPreview)	\
    (This)->lpVtbl -> SetPreviewMode(This,fPreview)

#define IAMTimelineGroup_GetPreviewMode(This,pfPreview)	\
    (This)->lpVtbl -> GetPreviewMode(This,pfPreview)

#define IAMTimelineGroup_SetMediaTypeForVB(This,Val)	\
    (This)->lpVtbl -> SetMediaTypeForVB(This,Val)

#define IAMTimelineGroup_GetOutputBuffering(This,pnBuffer)	\
    (This)->lpVtbl -> GetOutputBuffering(This,pnBuffer)

#define IAMTimelineGroup_SetOutputBuffering(This,nBuffer)	\
    (This)->lpVtbl -> SetOutputBuffering(This,nBuffer)

#define IAMTimelineGroup_SetSmartRecompressFormat(This,pFormat)	\
    (This)->lpVtbl -> SetSmartRecompressFormat(This,pFormat)

#define IAMTimelineGroup_GetSmartRecompressFormat(This,ppFormat)	\
    (This)->lpVtbl -> GetSmartRecompressFormat(This,ppFormat)

#define IAMTimelineGroup_IsSmartRecompressFormatSet(This,pVal)	\
    (This)->lpVtbl -> IsSmartRecompressFormatSet(This,pVal)

#define IAMTimelineGroup_IsRecompressFormatDirty(This,pVal)	\
    (This)->lpVtbl -> IsRecompressFormatDirty(This,pVal)

#define IAMTimelineGroup_ClearRecompressFormatDirty(This)	\
    (This)->lpVtbl -> ClearRecompressFormatDirty(This)

#define IAMTimelineGroup_SetRecompFormatFromSource(This,pSource)	\
    (This)->lpVtbl -> SetRecompFormatFromSource(This,pSource)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_SetTimeline_Proxy( 
    IAMTimelineGroup * This,
    IAMTimeline *pTimeline);


void __RPC_STUB IAMTimelineGroup_SetTimeline_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_GetTimeline_Proxy( 
    IAMTimelineGroup * This,
    /* [out] */ IAMTimeline **ppTimeline);


void __RPC_STUB IAMTimelineGroup_GetTimeline_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_GetPriority_Proxy( 
    IAMTimelineGroup * This,
    long *pPriority);


void __RPC_STUB IAMTimelineGroup_GetPriority_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_GetMediaType_Proxy( 
    IAMTimelineGroup * This,
    /* [out] */ AM_MEDIA_TYPE *__MIDL_0040);


void __RPC_STUB IAMTimelineGroup_GetMediaType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_SetMediaType_Proxy( 
    IAMTimelineGroup * This,
    /* [in] */ AM_MEDIA_TYPE *__MIDL_0041);


void __RPC_STUB IAMTimelineGroup_SetMediaType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_SetOutputFPS_Proxy( 
    IAMTimelineGroup * This,
    double FPS);


void __RPC_STUB IAMTimelineGroup_SetOutputFPS_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_GetOutputFPS_Proxy( 
    IAMTimelineGroup * This,
    double *pFPS);


void __RPC_STUB IAMTimelineGroup_GetOutputFPS_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_SetGroupName_Proxy( 
    IAMTimelineGroup * This,
    BSTR pGroupName);


void __RPC_STUB IAMTimelineGroup_SetGroupName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_GetGroupName_Proxy( 
    IAMTimelineGroup * This,
    /* [retval][out] */ BSTR *pGroupName);


void __RPC_STUB IAMTimelineGroup_GetGroupName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_SetPreviewMode_Proxy( 
    IAMTimelineGroup * This,
    BOOL fPreview);


void __RPC_STUB IAMTimelineGroup_SetPreviewMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_GetPreviewMode_Proxy( 
    IAMTimelineGroup * This,
    BOOL *pfPreview);


void __RPC_STUB IAMTimelineGroup_GetPreviewMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_SetMediaTypeForVB_Proxy( 
    IAMTimelineGroup * This,
    /* [in] */ long Val);


void __RPC_STUB IAMTimelineGroup_SetMediaTypeForVB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_GetOutputBuffering_Proxy( 
    IAMTimelineGroup * This,
    /* [out] */ int *pnBuffer);


void __RPC_STUB IAMTimelineGroup_GetOutputBuffering_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimelineGroup_SetOutputBuffering_Proxy( 
    IAMTimelineGroup * This,
    /* [in] */ int nBuffer);


void __RPC_STUB IAMTimelineGroup_SetOutputBuffering_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineGroup_SetSmartRecompressFormat_Proxy( 
    IAMTimelineGroup * This,
    long *pFormat);


void __RPC_STUB IAMTimelineGroup_SetSmartRecompressFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineGroup_GetSmartRecompressFormat_Proxy( 
    IAMTimelineGroup * This,
    long **ppFormat);


void __RPC_STUB IAMTimelineGroup_GetSmartRecompressFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineGroup_IsSmartRecompressFormatSet_Proxy( 
    IAMTimelineGroup * This,
    BOOL *pVal);


void __RPC_STUB IAMTimelineGroup_IsSmartRecompressFormatSet_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineGroup_IsRecompressFormatDirty_Proxy( 
    IAMTimelineGroup * This,
    BOOL *pVal);


void __RPC_STUB IAMTimelineGroup_IsRecompressFormatDirty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineGroup_ClearRecompressFormatDirty_Proxy( 
    IAMTimelineGroup * This);


void __RPC_STUB IAMTimelineGroup_ClearRecompressFormatDirty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimelineGroup_SetRecompFormatFromSource_Proxy( 
    IAMTimelineGroup * This,
    IAMTimelineSrc *pSource);


void __RPC_STUB IAMTimelineGroup_SetRecompFormatFromSource_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMTimelineGroup_INTERFACE_DEFINED__ */


#ifndef __IAMTimeline_INTERFACE_DEFINED__
#define __IAMTimeline_INTERFACE_DEFINED__

/* interface IAMTimeline */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMTimeline;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("78530B74-61F9-11D2-8CAD-00A024580902")
    IAMTimeline : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE CreateEmptyNode( 
            /* [out] */ IAMTimelineObj **ppObj,
            TIMELINE_MAJOR_TYPE Type) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE AddGroup( 
            IAMTimelineObj *pGroup) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE RemGroupFromList( 
            IAMTimelineObj *pGroup) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetGroup( 
            /* [out] */ IAMTimelineObj **ppGroup,
            long WhichGroup) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetGroupCount( 
            long *pCount) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ClearAllGroups( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetInsertMode( 
            long *pMode) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetInsertMode( 
            long Mode) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EnableTransitions( 
            BOOL fEnabled) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE TransitionsEnabled( 
            BOOL *pfEnabled) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EnableEffects( 
            BOOL fEnabled) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EffectsEnabled( 
            BOOL *pfEnabled) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetInterestRange( 
            REFERENCE_TIME Start,
            REFERENCE_TIME Stop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDuration( 
            REFERENCE_TIME *pDuration) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDuration2( 
            double *pDuration) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetDefaultFPS( 
            double FPS) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDefaultFPS( 
            double *pFPS) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE IsDirty( 
            BOOL *pDirty) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDirtyRange( 
            REFERENCE_TIME *pStart,
            REFERENCE_TIME *pStop) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCountOfType( 
            long Group,
            long *pVal,
            long *pValWithComps,
            TIMELINE_MAJOR_TYPE MajorType) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE ValidateSourceNames( 
            long ValidateFlags,
            IMediaLocator *pOverride,
            LONG_PTR NotifyEventHandle) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetDefaultTransition( 
            GUID *pGuid) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetDefaultTransition( 
            GUID *pGuid) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetDefaultEffect( 
            GUID *pGuid) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetDefaultEffect( 
            GUID *pGuid) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetDefaultTransitionB( 
            BSTR pGuid) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetDefaultTransitionB( 
            /* [retval][out] */ BSTR *pGuid) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetDefaultEffectB( 
            BSTR pGuid) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetDefaultEffectB( 
            /* [retval][out] */ BSTR *pGuid) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMTimelineVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMTimeline * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMTimeline * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMTimeline * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *CreateEmptyNode )( 
            IAMTimeline * This,
            /* [out] */ IAMTimelineObj **ppObj,
            TIMELINE_MAJOR_TYPE Type);
        
        HRESULT ( STDMETHODCALLTYPE *AddGroup )( 
            IAMTimeline * This,
            IAMTimelineObj *pGroup);
        
        HRESULT ( STDMETHODCALLTYPE *RemGroupFromList )( 
            IAMTimeline * This,
            IAMTimelineObj *pGroup);
        
        HRESULT ( STDMETHODCALLTYPE *GetGroup )( 
            IAMTimeline * This,
            /* [out] */ IAMTimelineObj **ppGroup,
            long WhichGroup);
        
        HRESULT ( STDMETHODCALLTYPE *GetGroupCount )( 
            IAMTimeline * This,
            long *pCount);
        
        HRESULT ( STDMETHODCALLTYPE *ClearAllGroups )( 
            IAMTimeline * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetInsertMode )( 
            IAMTimeline * This,
            long *pMode);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetInsertMode )( 
            IAMTimeline * This,
            long Mode);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *EnableTransitions )( 
            IAMTimeline * This,
            BOOL fEnabled);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *TransitionsEnabled )( 
            IAMTimeline * This,
            BOOL *pfEnabled);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *EnableEffects )( 
            IAMTimeline * This,
            BOOL fEnabled);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *EffectsEnabled )( 
            IAMTimeline * This,
            BOOL *pfEnabled);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetInterestRange )( 
            IAMTimeline * This,
            REFERENCE_TIME Start,
            REFERENCE_TIME Stop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetDuration )( 
            IAMTimeline * This,
            REFERENCE_TIME *pDuration);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetDuration2 )( 
            IAMTimeline * This,
            double *pDuration);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetDefaultFPS )( 
            IAMTimeline * This,
            double FPS);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetDefaultFPS )( 
            IAMTimeline * This,
            double *pFPS);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *IsDirty )( 
            IAMTimeline * This,
            BOOL *pDirty);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetDirtyRange )( 
            IAMTimeline * This,
            REFERENCE_TIME *pStart,
            REFERENCE_TIME *pStop);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetCountOfType )( 
            IAMTimeline * This,
            long Group,
            long *pVal,
            long *pValWithComps,
            TIMELINE_MAJOR_TYPE MajorType);
        
        HRESULT ( STDMETHODCALLTYPE *ValidateSourceNames )( 
            IAMTimeline * This,
            long ValidateFlags,
            IMediaLocator *pOverride,
            LONG_PTR NotifyEventHandle);
        
        HRESULT ( STDMETHODCALLTYPE *SetDefaultTransition )( 
            IAMTimeline * This,
            GUID *pGuid);
        
        HRESULT ( STDMETHODCALLTYPE *GetDefaultTransition )( 
            IAMTimeline * This,
            GUID *pGuid);
        
        HRESULT ( STDMETHODCALLTYPE *SetDefaultEffect )( 
            IAMTimeline * This,
            GUID *pGuid);
        
        HRESULT ( STDMETHODCALLTYPE *GetDefaultEffect )( 
            IAMTimeline * This,
            GUID *pGuid);
        
        HRESULT ( STDMETHODCALLTYPE *SetDefaultTransitionB )( 
            IAMTimeline * This,
            BSTR pGuid);
        
        HRESULT ( STDMETHODCALLTYPE *GetDefaultTransitionB )( 
            IAMTimeline * This,
            /* [retval][out] */ BSTR *pGuid);
        
        HRESULT ( STDMETHODCALLTYPE *SetDefaultEffectB )( 
            IAMTimeline * This,
            BSTR pGuid);
        
        HRESULT ( STDMETHODCALLTYPE *GetDefaultEffectB )( 
            IAMTimeline * This,
            /* [retval][out] */ BSTR *pGuid);
        
        END_INTERFACE
    } IAMTimelineVtbl;

    interface IAMTimeline
    {
        CONST_VTBL struct IAMTimelineVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMTimeline_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMTimeline_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMTimeline_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMTimeline_CreateEmptyNode(This,ppObj,Type)	\
    (This)->lpVtbl -> CreateEmptyNode(This,ppObj,Type)

#define IAMTimeline_AddGroup(This,pGroup)	\
    (This)->lpVtbl -> AddGroup(This,pGroup)

#define IAMTimeline_RemGroupFromList(This,pGroup)	\
    (This)->lpVtbl -> RemGroupFromList(This,pGroup)

#define IAMTimeline_GetGroup(This,ppGroup,WhichGroup)	\
    (This)->lpVtbl -> GetGroup(This,ppGroup,WhichGroup)

#define IAMTimeline_GetGroupCount(This,pCount)	\
    (This)->lpVtbl -> GetGroupCount(This,pCount)

#define IAMTimeline_ClearAllGroups(This)	\
    (This)->lpVtbl -> ClearAllGroups(This)

#define IAMTimeline_GetInsertMode(This,pMode)	\
    (This)->lpVtbl -> GetInsertMode(This,pMode)

#define IAMTimeline_SetInsertMode(This,Mode)	\
    (This)->lpVtbl -> SetInsertMode(This,Mode)

#define IAMTimeline_EnableTransitions(This,fEnabled)	\
    (This)->lpVtbl -> EnableTransitions(This,fEnabled)

#define IAMTimeline_TransitionsEnabled(This,pfEnabled)	\
    (This)->lpVtbl -> TransitionsEnabled(This,pfEnabled)

#define IAMTimeline_EnableEffects(This,fEnabled)	\
    (This)->lpVtbl -> EnableEffects(This,fEnabled)

#define IAMTimeline_EffectsEnabled(This,pfEnabled)	\
    (This)->lpVtbl -> EffectsEnabled(This,pfEnabled)

#define IAMTimeline_SetInterestRange(This,Start,Stop)	\
    (This)->lpVtbl -> SetInterestRange(This,Start,Stop)

#define IAMTimeline_GetDuration(This,pDuration)	\
    (This)->lpVtbl -> GetDuration(This,pDuration)

#define IAMTimeline_GetDuration2(This,pDuration)	\
    (This)->lpVtbl -> GetDuration2(This,pDuration)

#define IAMTimeline_SetDefaultFPS(This,FPS)	\
    (This)->lpVtbl -> SetDefaultFPS(This,FPS)

#define IAMTimeline_GetDefaultFPS(This,pFPS)	\
    (This)->lpVtbl -> GetDefaultFPS(This,pFPS)

#define IAMTimeline_IsDirty(This,pDirty)	\
    (This)->lpVtbl -> IsDirty(This,pDirty)

#define IAMTimeline_GetDirtyRange(This,pStart,pStop)	\
    (This)->lpVtbl -> GetDirtyRange(This,pStart,pStop)

#define IAMTimeline_GetCountOfType(This,Group,pVal,pValWithComps,MajorType)	\
    (This)->lpVtbl -> GetCountOfType(This,Group,pVal,pValWithComps,MajorType)

#define IAMTimeline_ValidateSourceNames(This,ValidateFlags,pOverride,NotifyEventHandle)	\
    (This)->lpVtbl -> ValidateSourceNames(This,ValidateFlags,pOverride,NotifyEventHandle)

#define IAMTimeline_SetDefaultTransition(This,pGuid)	\
    (This)->lpVtbl -> SetDefaultTransition(This,pGuid)

#define IAMTimeline_GetDefaultTransition(This,pGuid)	\
    (This)->lpVtbl -> GetDefaultTransition(This,pGuid)

#define IAMTimeline_SetDefaultEffect(This,pGuid)	\
    (This)->lpVtbl -> SetDefaultEffect(This,pGuid)

#define IAMTimeline_GetDefaultEffect(This,pGuid)	\
    (This)->lpVtbl -> GetDefaultEffect(This,pGuid)

#define IAMTimeline_SetDefaultTransitionB(This,pGuid)	\
    (This)->lpVtbl -> SetDefaultTransitionB(This,pGuid)

#define IAMTimeline_GetDefaultTransitionB(This,pGuid)	\
    (This)->lpVtbl -> GetDefaultTransitionB(This,pGuid)

#define IAMTimeline_SetDefaultEffectB(This,pGuid)	\
    (This)->lpVtbl -> SetDefaultEffectB(This,pGuid)

#define IAMTimeline_GetDefaultEffectB(This,pGuid)	\
    (This)->lpVtbl -> GetDefaultEffectB(This,pGuid)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_CreateEmptyNode_Proxy( 
    IAMTimeline * This,
    /* [out] */ IAMTimelineObj **ppObj,
    TIMELINE_MAJOR_TYPE Type);


void __RPC_STUB IAMTimeline_CreateEmptyNode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_AddGroup_Proxy( 
    IAMTimeline * This,
    IAMTimelineObj *pGroup);


void __RPC_STUB IAMTimeline_AddGroup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_RemGroupFromList_Proxy( 
    IAMTimeline * This,
    IAMTimelineObj *pGroup);


void __RPC_STUB IAMTimeline_RemGroupFromList_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_GetGroup_Proxy( 
    IAMTimeline * This,
    /* [out] */ IAMTimelineObj **ppGroup,
    long WhichGroup);


void __RPC_STUB IAMTimeline_GetGroup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_GetGroupCount_Proxy( 
    IAMTimeline * This,
    long *pCount);


void __RPC_STUB IAMTimeline_GetGroupCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_ClearAllGroups_Proxy( 
    IAMTimeline * This);


void __RPC_STUB IAMTimeline_ClearAllGroups_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_GetInsertMode_Proxy( 
    IAMTimeline * This,
    long *pMode);


void __RPC_STUB IAMTimeline_GetInsertMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_SetInsertMode_Proxy( 
    IAMTimeline * This,
    long Mode);


void __RPC_STUB IAMTimeline_SetInsertMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_EnableTransitions_Proxy( 
    IAMTimeline * This,
    BOOL fEnabled);


void __RPC_STUB IAMTimeline_EnableTransitions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_TransitionsEnabled_Proxy( 
    IAMTimeline * This,
    BOOL *pfEnabled);


void __RPC_STUB IAMTimeline_TransitionsEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_EnableEffects_Proxy( 
    IAMTimeline * This,
    BOOL fEnabled);


void __RPC_STUB IAMTimeline_EnableEffects_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_EffectsEnabled_Proxy( 
    IAMTimeline * This,
    BOOL *pfEnabled);


void __RPC_STUB IAMTimeline_EffectsEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_SetInterestRange_Proxy( 
    IAMTimeline * This,
    REFERENCE_TIME Start,
    REFERENCE_TIME Stop);


void __RPC_STUB IAMTimeline_SetInterestRange_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_GetDuration_Proxy( 
    IAMTimeline * This,
    REFERENCE_TIME *pDuration);


void __RPC_STUB IAMTimeline_GetDuration_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_GetDuration2_Proxy( 
    IAMTimeline * This,
    double *pDuration);


void __RPC_STUB IAMTimeline_GetDuration2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_SetDefaultFPS_Proxy( 
    IAMTimeline * This,
    double FPS);


void __RPC_STUB IAMTimeline_SetDefaultFPS_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_GetDefaultFPS_Proxy( 
    IAMTimeline * This,
    double *pFPS);


void __RPC_STUB IAMTimeline_GetDefaultFPS_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_IsDirty_Proxy( 
    IAMTimeline * This,
    BOOL *pDirty);


void __RPC_STUB IAMTimeline_IsDirty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_GetDirtyRange_Proxy( 
    IAMTimeline * This,
    REFERENCE_TIME *pStart,
    REFERENCE_TIME *pStop);


void __RPC_STUB IAMTimeline_GetDirtyRange_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMTimeline_GetCountOfType_Proxy( 
    IAMTimeline * This,
    long Group,
    long *pVal,
    long *pValWithComps,
    TIMELINE_MAJOR_TYPE MajorType);


void __RPC_STUB IAMTimeline_GetCountOfType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_ValidateSourceNames_Proxy( 
    IAMTimeline * This,
    long ValidateFlags,
    IMediaLocator *pOverride,
    LONG_PTR NotifyEventHandle);


void __RPC_STUB IAMTimeline_ValidateSourceNames_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_SetDefaultTransition_Proxy( 
    IAMTimeline * This,
    GUID *pGuid);


void __RPC_STUB IAMTimeline_SetDefaultTransition_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_GetDefaultTransition_Proxy( 
    IAMTimeline * This,
    GUID *pGuid);


void __RPC_STUB IAMTimeline_GetDefaultTransition_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_SetDefaultEffect_Proxy( 
    IAMTimeline * This,
    GUID *pGuid);


void __RPC_STUB IAMTimeline_SetDefaultEffect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_GetDefaultEffect_Proxy( 
    IAMTimeline * This,
    GUID *pGuid);


void __RPC_STUB IAMTimeline_GetDefaultEffect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_SetDefaultTransitionB_Proxy( 
    IAMTimeline * This,
    BSTR pGuid);


void __RPC_STUB IAMTimeline_SetDefaultTransitionB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_GetDefaultTransitionB_Proxy( 
    IAMTimeline * This,
    /* [retval][out] */ BSTR *pGuid);


void __RPC_STUB IAMTimeline_GetDefaultTransitionB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_SetDefaultEffectB_Proxy( 
    IAMTimeline * This,
    BSTR pGuid);


void __RPC_STUB IAMTimeline_SetDefaultEffectB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE IAMTimeline_GetDefaultEffectB_Proxy( 
    IAMTimeline * This,
    /* [retval][out] */ BSTR *pGuid);


void __RPC_STUB IAMTimeline_GetDefaultEffectB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMTimeline_INTERFACE_DEFINED__ */


#ifndef __IXml2Dex_INTERFACE_DEFINED__
#define __IXml2Dex_INTERFACE_DEFINED__

/* interface IXml2Dex */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IXml2Dex;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("18C628ED-962A-11D2-8D08-00A0C9441E20")
    IXml2Dex : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CreateGraphFromFile( 
            /* [out] */ IUnknown **ppGraph,
            IUnknown *pTimeline,
            BSTR Filename) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE WriteGrfFile( 
            IUnknown *pGraph,
            BSTR FileName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE WriteXMLFile( 
            IUnknown *pTimeline,
            BSTR FileName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ReadXMLFile( 
            IUnknown *pTimeline,
            BSTR XMLName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Delete( 
            IUnknown *pTimeline,
            double dStart,
            double dEnd) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE WriteXMLPart( 
            IUnknown *pTimeline,
            double dStart,
            double dEnd,
            BSTR FileName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PasteXMLFile( 
            IUnknown *pTimeline,
            double dStart,
            BSTR FileName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CopyXML( 
            IUnknown *pTimeline,
            double dStart,
            double dEnd) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PasteXML( 
            IUnknown *pTimeline,
            double dStart) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Reset( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ReadXML( 
            IUnknown *pTimeline,
            IUnknown *pXML) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE WriteXML( 
            IUnknown *pTimeline,
            BSTR *pbstrXML) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IXml2DexVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IXml2Dex * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IXml2Dex * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IXml2Dex * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IXml2Dex * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IXml2Dex * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IXml2Dex * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IXml2Dex * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateGraphFromFile )( 
            IXml2Dex * This,
            /* [out] */ IUnknown **ppGraph,
            IUnknown *pTimeline,
            BSTR Filename);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WriteGrfFile )( 
            IXml2Dex * This,
            IUnknown *pGraph,
            BSTR FileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WriteXMLFile )( 
            IXml2Dex * This,
            IUnknown *pTimeline,
            BSTR FileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ReadXMLFile )( 
            IXml2Dex * This,
            IUnknown *pTimeline,
            BSTR XMLName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IXml2Dex * This,
            IUnknown *pTimeline,
            double dStart,
            double dEnd);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WriteXMLPart )( 
            IXml2Dex * This,
            IUnknown *pTimeline,
            double dStart,
            double dEnd,
            BSTR FileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PasteXMLFile )( 
            IXml2Dex * This,
            IUnknown *pTimeline,
            double dStart,
            BSTR FileName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CopyXML )( 
            IXml2Dex * This,
            IUnknown *pTimeline,
            double dStart,
            double dEnd);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PasteXML )( 
            IXml2Dex * This,
            IUnknown *pTimeline,
            double dStart);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Reset )( 
            IXml2Dex * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ReadXML )( 
            IXml2Dex * This,
            IUnknown *pTimeline,
            IUnknown *pXML);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WriteXML )( 
            IXml2Dex * This,
            IUnknown *pTimeline,
            BSTR *pbstrXML);
        
        END_INTERFACE
    } IXml2DexVtbl;

    interface IXml2Dex
    {
        CONST_VTBL struct IXml2DexVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IXml2Dex_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IXml2Dex_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IXml2Dex_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IXml2Dex_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IXml2Dex_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IXml2Dex_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IXml2Dex_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IXml2Dex_CreateGraphFromFile(This,ppGraph,pTimeline,Filename)	\
    (This)->lpVtbl -> CreateGraphFromFile(This,ppGraph,pTimeline,Filename)

#define IXml2Dex_WriteGrfFile(This,pGraph,FileName)	\
    (This)->lpVtbl -> WriteGrfFile(This,pGraph,FileName)

#define IXml2Dex_WriteXMLFile(This,pTimeline,FileName)	\
    (This)->lpVtbl -> WriteXMLFile(This,pTimeline,FileName)

#define IXml2Dex_ReadXMLFile(This,pTimeline,XMLName)	\
    (This)->lpVtbl -> ReadXMLFile(This,pTimeline,XMLName)

#define IXml2Dex_Delete(This,pTimeline,dStart,dEnd)	\
    (This)->lpVtbl -> Delete(This,pTimeline,dStart,dEnd)

#define IXml2Dex_WriteXMLPart(This,pTimeline,dStart,dEnd,FileName)	\
    (This)->lpVtbl -> WriteXMLPart(This,pTimeline,dStart,dEnd,FileName)

#define IXml2Dex_PasteXMLFile(This,pTimeline,dStart,FileName)	\
    (This)->lpVtbl -> PasteXMLFile(This,pTimeline,dStart,FileName)

#define IXml2Dex_CopyXML(This,pTimeline,dStart,dEnd)	\
    (This)->lpVtbl -> CopyXML(This,pTimeline,dStart,dEnd)

#define IXml2Dex_PasteXML(This,pTimeline,dStart)	\
    (This)->lpVtbl -> PasteXML(This,pTimeline,dStart)

#define IXml2Dex_Reset(This)	\
    (This)->lpVtbl -> Reset(This)

#define IXml2Dex_ReadXML(This,pTimeline,pXML)	\
    (This)->lpVtbl -> ReadXML(This,pTimeline,pXML)

#define IXml2Dex_WriteXML(This,pTimeline,pbstrXML)	\
    (This)->lpVtbl -> WriteXML(This,pTimeline,pbstrXML)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXml2Dex_CreateGraphFromFile_Proxy( 
    IXml2Dex * This,
    /* [out] */ IUnknown **ppGraph,
    IUnknown *pTimeline,
    BSTR Filename);


void __RPC_STUB IXml2Dex_CreateGraphFromFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXml2Dex_WriteGrfFile_Proxy( 
    IXml2Dex * This,
    IUnknown *pGraph,
    BSTR FileName);


void __RPC_STUB IXml2Dex_WriteGrfFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXml2Dex_WriteXMLFile_Proxy( 
    IXml2Dex * This,
    IUnknown *pTimeline,
    BSTR FileName);


void __RPC_STUB IXml2Dex_WriteXMLFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXml2Dex_ReadXMLFile_Proxy( 
    IXml2Dex * This,
    IUnknown *pTimeline,
    BSTR XMLName);


void __RPC_STUB IXml2Dex_ReadXMLFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXml2Dex_Delete_Proxy( 
    IXml2Dex * This,
    IUnknown *pTimeline,
    double dStart,
    double dEnd);


void __RPC_STUB IXml2Dex_Delete_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXml2Dex_WriteXMLPart_Proxy( 
    IXml2Dex * This,
    IUnknown *pTimeline,
    double dStart,
    double dEnd,
    BSTR FileName);


void __RPC_STUB IXml2Dex_WriteXMLPart_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXml2Dex_PasteXMLFile_Proxy( 
    IXml2Dex * This,
    IUnknown *pTimeline,
    double dStart,
    BSTR FileName);


void __RPC_STUB IXml2Dex_PasteXMLFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXml2Dex_CopyXML_Proxy( 
    IXml2Dex * This,
    IUnknown *pTimeline,
    double dStart,
    double dEnd);


void __RPC_STUB IXml2Dex_CopyXML_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXml2Dex_PasteXML_Proxy( 
    IXml2Dex * This,
    IUnknown *pTimeline,
    double dStart);


void __RPC_STUB IXml2Dex_PasteXML_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXml2Dex_Reset_Proxy( 
    IXml2Dex * This);


void __RPC_STUB IXml2Dex_Reset_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXml2Dex_ReadXML_Proxy( 
    IXml2Dex * This,
    IUnknown *pTimeline,
    IUnknown *pXML);


void __RPC_STUB IXml2Dex_ReadXML_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXml2Dex_WriteXML_Proxy( 
    IXml2Dex * This,
    IUnknown *pTimeline,
    BSTR *pbstrXML);


void __RPC_STUB IXml2Dex_WriteXML_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IXml2Dex_INTERFACE_DEFINED__ */


#ifndef __IAMErrorLog_INTERFACE_DEFINED__
#define __IAMErrorLog_INTERFACE_DEFINED__

/* interface IAMErrorLog */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMErrorLog;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E43E73A2-0EFA-11d3-9601-00A0C9441E20")
    IAMErrorLog : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE LogError( 
            long Severity,
            BSTR pErrorString,
            long ErrorCode,
            long hresult,
            /* [in] */ VARIANT *pExtraInfo) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMErrorLogVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMErrorLog * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMErrorLog * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMErrorLog * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *LogError )( 
            IAMErrorLog * This,
            long Severity,
            BSTR pErrorString,
            long ErrorCode,
            long hresult,
            /* [in] */ VARIANT *pExtraInfo);
        
        END_INTERFACE
    } IAMErrorLogVtbl;

    interface IAMErrorLog
    {
        CONST_VTBL struct IAMErrorLogVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMErrorLog_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMErrorLog_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMErrorLog_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMErrorLog_LogError(This,Severity,pErrorString,ErrorCode,hresult,pExtraInfo)	\
    (This)->lpVtbl -> LogError(This,Severity,pErrorString,ErrorCode,hresult,pExtraInfo)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAMErrorLog_LogError_Proxy( 
    IAMErrorLog * This,
    long Severity,
    BSTR pErrorString,
    long ErrorCode,
    long hresult,
    /* [in] */ VARIANT *pExtraInfo);


void __RPC_STUB IAMErrorLog_LogError_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMErrorLog_INTERFACE_DEFINED__ */


#ifndef __IAMSetErrorLog_INTERFACE_DEFINED__
#define __IAMSetErrorLog_INTERFACE_DEFINED__

/* interface IAMSetErrorLog */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAMSetErrorLog;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("963566DA-BE21-4eaf-88E9-35704F8F52A1")
    IAMSetErrorLog : public IUnknown
    {
    public:
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ErrorLog( 
            /* [retval][out] */ IAMErrorLog **pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_ErrorLog( 
            /* [in] */ IAMErrorLog *newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAMSetErrorLogVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAMSetErrorLog * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAMSetErrorLog * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAMSetErrorLog * This);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorLog )( 
            IAMSetErrorLog * This,
            /* [retval][out] */ IAMErrorLog **pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ErrorLog )( 
            IAMSetErrorLog * This,
            /* [in] */ IAMErrorLog *newVal);
        
        END_INTERFACE
    } IAMSetErrorLogVtbl;

    interface IAMSetErrorLog
    {
        CONST_VTBL struct IAMSetErrorLogVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAMSetErrorLog_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAMSetErrorLog_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAMSetErrorLog_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAMSetErrorLog_get_ErrorLog(This,pVal)	\
    (This)->lpVtbl -> get_ErrorLog(This,pVal)

#define IAMSetErrorLog_put_ErrorLog(This,newVal)	\
    (This)->lpVtbl -> put_ErrorLog(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IAMSetErrorLog_get_ErrorLog_Proxy( 
    IAMSetErrorLog * This,
    /* [retval][out] */ IAMErrorLog **pVal);


void __RPC_STUB IAMSetErrorLog_get_ErrorLog_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IAMSetErrorLog_put_ErrorLog_Proxy( 
    IAMSetErrorLog * This,
    /* [in] */ IAMErrorLog *newVal);


void __RPC_STUB IAMSetErrorLog_put_ErrorLog_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAMSetErrorLog_INTERFACE_DEFINED__ */


#ifndef __ISampleGrabberCB_INTERFACE_DEFINED__
#define __ISampleGrabberCB_INTERFACE_DEFINED__

/* interface ISampleGrabberCB */
/* [unique][helpstring][local][uuid][object] */ 


EXTERN_C const IID IID_ISampleGrabberCB;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0579154A-2B53-4994-B0D0-E773148EFF85")
    ISampleGrabberCB : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SampleCB( 
            double SampleTime,
            IMediaSample *pSample) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE BufferCB( 
            double SampleTime,
            BYTE *pBuffer,
            long BufferLen) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISampleGrabberCBVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISampleGrabberCB * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISampleGrabberCB * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISampleGrabberCB * This);
        
        HRESULT ( STDMETHODCALLTYPE *SampleCB )( 
            ISampleGrabberCB * This,
            double SampleTime,
            IMediaSample *pSample);
        
        HRESULT ( STDMETHODCALLTYPE *BufferCB )( 
            ISampleGrabberCB * This,
            double SampleTime,
            BYTE *pBuffer,
            long BufferLen);
        
        END_INTERFACE
    } ISampleGrabberCBVtbl;

    interface ISampleGrabberCB
    {
        CONST_VTBL struct ISampleGrabberCBVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISampleGrabberCB_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ISampleGrabberCB_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ISampleGrabberCB_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ISampleGrabberCB_SampleCB(This,SampleTime,pSample)	\
    (This)->lpVtbl -> SampleCB(This,SampleTime,pSample)

#define ISampleGrabberCB_BufferCB(This,SampleTime,pBuffer,BufferLen)	\
    (This)->lpVtbl -> BufferCB(This,SampleTime,pBuffer,BufferLen)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE ISampleGrabberCB_SampleCB_Proxy( 
    ISampleGrabberCB * This,
    double SampleTime,
    IMediaSample *pSample);


void __RPC_STUB ISampleGrabberCB_SampleCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE ISampleGrabberCB_BufferCB_Proxy( 
    ISampleGrabberCB * This,
    double SampleTime,
    BYTE *pBuffer,
    long BufferLen);


void __RPC_STUB ISampleGrabberCB_BufferCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ISampleGrabberCB_INTERFACE_DEFINED__ */


#ifndef __ISampleGrabber_INTERFACE_DEFINED__
#define __ISampleGrabber_INTERFACE_DEFINED__

/* interface ISampleGrabber */
/* [unique][helpstring][local][uuid][object] */ 


EXTERN_C const IID IID_ISampleGrabber;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6B652FFF-11FE-4fce-92AD-0266B5D7C78F")
    ISampleGrabber : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetOneShot( 
            BOOL OneShot) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetMediaType( 
            const AM_MEDIA_TYPE *pType) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetConnectedMediaType( 
            AM_MEDIA_TYPE *pType) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetBufferSamples( 
            BOOL BufferThem) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetCurrentBuffer( 
            /* [out][in] */ long *pBufferSize,
            /* [out] */ long *pBuffer) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetCurrentSample( 
            /* [retval][out] */ IMediaSample **ppSample) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetCallback( 
            ISampleGrabberCB *pCallback,
            long WhichMethodToCallback) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISampleGrabberVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISampleGrabber * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISampleGrabber * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISampleGrabber * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetOneShot )( 
            ISampleGrabber * This,
            BOOL OneShot);
        
        HRESULT ( STDMETHODCALLTYPE *SetMediaType )( 
            ISampleGrabber * This,
            const AM_MEDIA_TYPE *pType);
        
        HRESULT ( STDMETHODCALLTYPE *GetConnectedMediaType )( 
            ISampleGrabber * This,
            AM_MEDIA_TYPE *pType);
        
        HRESULT ( STDMETHODCALLTYPE *SetBufferSamples )( 
            ISampleGrabber * This,
            BOOL BufferThem);
        
        HRESULT ( STDMETHODCALLTYPE *GetCurrentBuffer )( 
            ISampleGrabber * This,
            /* [out][in] */ long *pBufferSize,
            /* [out] */ long *pBuffer);
        
        HRESULT ( STDMETHODCALLTYPE *GetCurrentSample )( 
            ISampleGrabber * This,
            /* [retval][out] */ IMediaSample **ppSample);
        
        HRESULT ( STDMETHODCALLTYPE *SetCallback )( 
            ISampleGrabber * This,
            ISampleGrabberCB *pCallback,
            long WhichMethodToCallback);
        
        END_INTERFACE
    } ISampleGrabberVtbl;

    interface ISampleGrabber
    {
        CONST_VTBL struct ISampleGrabberVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISampleGrabber_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ISampleGrabber_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ISampleGrabber_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ISampleGrabber_SetOneShot(This,OneShot)	\
    (This)->lpVtbl -> SetOneShot(This,OneShot)

#define ISampleGrabber_SetMediaType(This,pType)	\
    (This)->lpVtbl -> SetMediaType(This,pType)

#define ISampleGrabber_GetConnectedMediaType(This,pType)	\
    (This)->lpVtbl -> GetConnectedMediaType(This,pType)

#define ISampleGrabber_SetBufferSamples(This,BufferThem)	\
    (This)->lpVtbl -> SetBufferSamples(This,BufferThem)

#define ISampleGrabber_GetCurrentBuffer(This,pBufferSize,pBuffer)	\
    (This)->lpVtbl -> GetCurrentBuffer(This,pBufferSize,pBuffer)

#define ISampleGrabber_GetCurrentSample(This,ppSample)	\
    (This)->lpVtbl -> GetCurrentSample(This,ppSample)

#define ISampleGrabber_SetCallback(This,pCallback,WhichMethodToCallback)	\
    (This)->lpVtbl -> SetCallback(This,pCallback,WhichMethodToCallback)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE ISampleGrabber_SetOneShot_Proxy( 
    ISampleGrabber * This,
    BOOL OneShot);


void __RPC_STUB ISampleGrabber_SetOneShot_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE ISampleGrabber_SetMediaType_Proxy( 
    ISampleGrabber * This,
    const AM_MEDIA_TYPE *pType);


void __RPC_STUB ISampleGrabber_SetMediaType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE ISampleGrabber_GetConnectedMediaType_Proxy( 
    ISampleGrabber * This,
    AM_MEDIA_TYPE *pType);


void __RPC_STUB ISampleGrabber_GetConnectedMediaType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE ISampleGrabber_SetBufferSamples_Proxy( 
    ISampleGrabber * This,
    BOOL BufferThem);


void __RPC_STUB ISampleGrabber_SetBufferSamples_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE ISampleGrabber_GetCurrentBuffer_Proxy( 
    ISampleGrabber * This,
    /* [out][in] */ long *pBufferSize,
    /* [out] */ long *pBuffer);


void __RPC_STUB ISampleGrabber_GetCurrentBuffer_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE ISampleGrabber_GetCurrentSample_Proxy( 
    ISampleGrabber * This,
    /* [retval][out] */ IMediaSample **ppSample);


void __RPC_STUB ISampleGrabber_GetCurrentSample_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE ISampleGrabber_SetCallback_Proxy( 
    ISampleGrabber * This,
    ISampleGrabberCB *pCallback,
    long WhichMethodToCallback);


void __RPC_STUB ISampleGrabber_SetCallback_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ISampleGrabber_INTERFACE_DEFINED__ */



#ifndef __DexterLib_LIBRARY_DEFINED__
#define __DexterLib_LIBRARY_DEFINED__

/* library DexterLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_DexterLib;

EXTERN_C const CLSID CLSID_AMTimeline;

#ifdef __cplusplus

class DECLSPEC_UUID("78530B75-61F9-11D2-8CAD-00A024580902")
AMTimeline;
#endif

EXTERN_C const CLSID CLSID_AMTimelineObj;

#ifdef __cplusplus

class DECLSPEC_UUID("78530B78-61F9-11D2-8CAD-00A024580902")
AMTimelineObj;
#endif

EXTERN_C const CLSID CLSID_AMTimelineSrc;

#ifdef __cplusplus

class DECLSPEC_UUID("78530B7A-61F9-11D2-8CAD-00A024580902")
AMTimelineSrc;
#endif

EXTERN_C const CLSID CLSID_AMTimelineTrack;

#ifdef __cplusplus

class DECLSPEC_UUID("8F6C3C50-897B-11d2-8CFB-00A0C9441E20")
AMTimelineTrack;
#endif

EXTERN_C const CLSID CLSID_AMTimelineComp;

#ifdef __cplusplus

class DECLSPEC_UUID("74D2EC80-6233-11d2-8CAD-00A024580902")
AMTimelineComp;
#endif

EXTERN_C const CLSID CLSID_AMTimelineGroup;

#ifdef __cplusplus

class DECLSPEC_UUID("F6D371E1-B8A6-11d2-8023-00C0DF10D434")
AMTimelineGroup;
#endif

EXTERN_C const CLSID CLSID_AMTimelineTrans;

#ifdef __cplusplus

class DECLSPEC_UUID("74D2EC81-6233-11d2-8CAD-00A024580902")
AMTimelineTrans;
#endif

EXTERN_C const CLSID CLSID_AMTimelineEffect;

#ifdef __cplusplus

class DECLSPEC_UUID("74D2EC82-6233-11d2-8CAD-00A024580902")
AMTimelineEffect;
#endif

EXTERN_C const CLSID CLSID_RenderEngine;

#ifdef __cplusplus

class DECLSPEC_UUID("64D8A8E0-80A2-11d2-8CF3-00A0C9441E20")
RenderEngine;
#endif

EXTERN_C const CLSID CLSID_SmartRenderEngine;

#ifdef __cplusplus

class DECLSPEC_UUID("498B0949-BBE9-4072-98BE-6CCAEB79DC6F")
SmartRenderEngine;
#endif

EXTERN_C const CLSID CLSID_AudMixer;

#ifdef __cplusplus

class DECLSPEC_UUID("036A9790-C153-11d2-9EF7-006008039E37")
AudMixer;
#endif

EXTERN_C const CLSID CLSID_Xml2Dex;

#ifdef __cplusplus

class DECLSPEC_UUID("18C628EE-962A-11D2-8D08-00A0C9441E20")
Xml2Dex;
#endif

EXTERN_C const CLSID CLSID_MediaLocator;

#ifdef __cplusplus

class DECLSPEC_UUID("CC1101F2-79DC-11D2-8CE6-00A0C9441E20")
MediaLocator;
#endif

EXTERN_C const CLSID CLSID_PropertySetter;

#ifdef __cplusplus

class DECLSPEC_UUID("ADF95821-DED7-11d2-ACBE-0080C75E246E")
PropertySetter;
#endif

EXTERN_C const CLSID CLSID_MediaDet;

#ifdef __cplusplus

class DECLSPEC_UUID("65BD0711-24D2-4ff7-9324-ED2E5D3ABAFA")
MediaDet;
#endif

EXTERN_C const CLSID CLSID_SampleGrabber;

#ifdef __cplusplus

class DECLSPEC_UUID("C1F400A0-3F08-11d3-9F0B-006008039E37")
SampleGrabber;
#endif

EXTERN_C const CLSID CLSID_NullRenderer;

#ifdef __cplusplus

class DECLSPEC_UUID("C1F400A4-3F08-11d3-9F0B-006008039E37")
NullRenderer;
#endif

EXTERN_C const CLSID CLSID_DxtCompositor;

#ifdef __cplusplus

class DECLSPEC_UUID("BB44391D-6ABD-422f-9E2E-385C9DFF51FC")
DxtCompositor;
#endif

EXTERN_C const CLSID CLSID_DxtAlphaSetter;

#ifdef __cplusplus

class DECLSPEC_UUID("506D89AE-909A-44f7-9444-ABD575896E35")
DxtAlphaSetter;
#endif

EXTERN_C const CLSID CLSID_DxtJpeg;

#ifdef __cplusplus

class DECLSPEC_UUID("DE75D012-7A65-11D2-8CEA-00A0C9441E20")
DxtJpeg;
#endif

EXTERN_C const CLSID CLSID_ColorSource;

#ifdef __cplusplus

class DECLSPEC_UUID("0cfdd070-581a-11d2-9ee6-006008039e37")
ColorSource;
#endif

EXTERN_C const CLSID CLSID_DxtKey;

#ifdef __cplusplus

class DECLSPEC_UUID("C5B19592-145E-11d3-9F04-006008039E37")
DxtKey;
#endif
#endif /* __DexterLib_LIBRARY_DEFINED__ */

/* interface __MIDL_itf_qedit_0450 */
/* [local] */ 


enum __MIDL___MIDL_itf_qedit_0450_0001
    {	E_NOTINTREE	= 0x80040400,
	E_RENDER_ENGINE_IS_BROKEN	= 0x80040401,
	E_MUST_INIT_RENDERER	= 0x80040402,
	E_NOTDETERMINED	= 0x80040403,
	E_NO_TIMELINE	= 0x80040404,
	S_WARN_OUTPUTRESET	= 40404
    } ;
#define DEX_IDS_BAD_SOURCE_NAME    1400
#define DEX_IDS_BAD_SOURCE_NAME2    1401
#define DEX_IDS_MISSING_SOURCE_NAME    1402
#define DEX_IDS_UNKNOWN_SOURCE    1403
#define DEX_IDS_INSTALL_PROBLEM    1404
#define DEX_IDS_NO_SOURCE_NAMES    1405
#define DEX_IDS_BAD_MEDIATYPE    1406
#define DEX_IDS_STREAM_NUMBER    1407
#define DEX_IDS_OUTOFMEMORY        1408
#define DEX_IDS_DIBSEQ_NOTALLSAME    1409
#define DEX_IDS_CLIPTOOSHORT        1410
#define DEX_IDS_INVALID_DXT        1411
#define DEX_IDS_INVALID_DEFAULT_DXT    1412
#define DEX_IDS_NO_3D        1413
#define DEX_IDS_BROKEN_DXT        1414
#define DEX_IDS_NO_SUCH_PROPERTY    1415
#define DEX_IDS_ILLEGAL_PROPERTY_VAL    1416
#define DEX_IDS_INVALID_XML        1417
#define DEX_IDS_CANT_FIND_FILTER    1418
#define DEX_IDS_DISK_WRITE_ERROR    1419
#define DEX_IDS_INVALID_AUDIO_FX    1420
#define DEX_IDS_CANT_FIND_COMPRESSOR 1421
#define DEX_IDS_TIMELINE_PARSE    1426
#define DEX_IDS_GRAPH_ERROR        1427
#define DEX_IDS_GRID_ERROR        1428
#define DEX_IDS_INTERFACE_ERROR    1429
EXTERN_GUID(CLSID_VideoEffects1Category, 0xcc7bfb42, 0xf175, 0x11d1, 0xa3, 0x92, 0x0, 0xe0, 0x29, 0x1f, 0x39, 0x59);
EXTERN_GUID(CLSID_VideoEffects2Category, 0xcc7bfb43, 0xf175, 0x11d1, 0xa3, 0x92, 0x0, 0xe0, 0x29, 0x1f, 0x39, 0x59);
EXTERN_GUID(CLSID_AudioEffects1Category, 0xcc7bfb44, 0xf175, 0x11d1, 0xa3, 0x92, 0x0, 0xe0, 0x29, 0x1f, 0x39, 0x59);
EXTERN_GUID(CLSID_AudioEffects2Category, 0xcc7bfb45, 0xf175, 0x11d1, 0xa3, 0x92, 0x0, 0xe0, 0x29, 0x1f, 0x39, 0x59);


extern RPC_IF_HANDLE __MIDL_itf_qedit_0450_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_qedit_0450_v0_0_s_ifspec;

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif



#include <gdiplus.h>

#include <math.h>
#include <mm3dnow.h>      //mmx routines
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <float.h>
#include <vector>

using namespace std;


class vec2D;
typedef struct _facerect {
        RECT rect;          //face rectangle
        unsigned int x;     //[x,y] face center coords
        unsigned int y;
        float diag;         //diag size

        vec2D* face;        //face data, normalized to 0.0 - 1.0 range
} FACERECT, *PFACERECT;



#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


