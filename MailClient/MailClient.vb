﻿Imports System.Net.Mail
Public Class MailClient
    Public Sub SendMail(ByVal sSenderID As String, ByVal sSenderPassword As String, ByVal sReceiversID As String, ByVal sSubject As String, ByVal sBody As String)
        Dim smtpServer As New SmtpClient()
        Dim mail As New MailMessage
        smtpServer.Credentials = New Net.NetworkCredential(sSenderID, sSenderPassword)
        smtpServer.Port = 587
        smtpServer.Host = "smtp.gmail.com"
        smtpServer.EnableSsl = True
        mail.From = New MailAddress(sSenderID)
        mail.To.Add(sReceiversID)
        mail.Subject = sSubject
        mail.Body = sBody
        smtpServer.Send(mail)
    End Sub
End Class
