namespace ChatNDraw
{
    partial class ChatNDrawForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChatNDrawForm));
            this.txtChatLog = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.txtChat = new System.Windows.Forms.TextBox();
            this.lblChat = new System.Windows.Forms.Label();
            this.dlgPenColor = new System.Windows.Forms.ColorDialog();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnConnect = new System.Windows.Forms.ToolStripButton();
            this.btnDisconnect = new System.Windows.Forms.ToolStripButton();
            this.sep3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnRoom = new System.Windows.Forms.ToolStripButton();
            this.btnLobby = new System.Windows.Forms.ToolStripButton();
            this.sep1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnErase = new System.Windows.Forms.ToolStripButton();
            this.btnSelectColor = new System.Windows.Forms.ToolStripButton();
            this.sep2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnServerStats = new System.Windows.Forms.ToolStripButton();
            this.btnStart = new System.Windows.Forms.ToolStripButton();
            this.pnlDraw = new System.Windows.Forms.Panel();
            this.grpUsers = new System.Windows.Forms.GroupBox();
            this.lvUsers = new System.Windows.Forms.ListView();
            this.imagesUsers = new System.Windows.Forms.ImageList(this.components);
            this.connection = new SCG.Prism.PrismConnection(this.components);
            this.dlgUserInfo = new SCG.Prism.PrismUserInfoDialog(this.components);
            this.dlgServerStats = new SCG.Prism.PrismServerStatsDialog(this.components);
            this.realtime = new SCG.Prism.GameCoordinator(this.components);
            this.toolStrip1.SuspendLayout();
            this.grpUsers.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtChatLog
            // 
            this.txtChatLog.BackColor = System.Drawing.Color.White;
            this.txtChatLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChatLog.Location = new System.Drawing.Point(256, 240);
            this.txtChatLog.Multiline = true;
            this.txtChatLog.Name = "txtChatLog";
            this.txtChatLog.ReadOnly = true;
            this.txtChatLog.Size = new System.Drawing.Size(368, 176);
            this.txtChatLog.TabIndex = 2;
            // 
            // btnSend
            // 
            this.btnSend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSend.Enabled = false;
            this.btnSend.Location = new System.Drawing.Point(552, 424);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 7;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // txtChat
            // 
            this.txtChat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChat.Enabled = false;
            this.txtChat.Location = new System.Drawing.Point(288, 424);
            this.txtChat.Name = "txtChat";
            this.txtChat.Size = new System.Drawing.Size(256, 20);
            this.txtChat.TabIndex = 6;
            // 
            // lblChat
            // 
            this.lblChat.AutoSize = true;
            this.lblChat.Location = new System.Drawing.Point(256, 424);
            this.lblChat.Name = "lblChat";
            this.lblChat.Size = new System.Drawing.Size(32, 13);
            this.lblChat.TabIndex = 5;
            this.lblChat.Text = "Chat:";
            // 
            // dlgPenColor
            // 
            this.dlgPenColor.AnyColor = true;
            this.dlgPenColor.Color = System.Drawing.Color.Red;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnConnect,
            this.btnDisconnect,
            this.sep3,
            this.btnRoom,
            this.btnLobby,
            this.sep1,
            this.btnErase,
            this.btnSelectColor,
            this.sep2,
            this.btnServerStats,
            this.btnStart});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(632, 52);
            this.toolStrip1.TabIndex = 10;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnConnect
            // 
            this.btnConnect.Image = ((System.Drawing.Image)(resources.GetObject("btnConnect.Image")));
            this.btnConnect.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnConnect.ImageTransparentColor = System.Drawing.Color.White;
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(51, 49);
            this.btnConnect.Text = "Connect";
            this.btnConnect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Enabled = false;
            this.btnDisconnect.Image = ((System.Drawing.Image)(resources.GetObject("btnDisconnect.Image")));
            this.btnDisconnect.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDisconnect.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(63, 49);
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // sep3
            // 
            this.sep3.Name = "sep3";
            this.sep3.Size = new System.Drawing.Size(6, 52);
            // 
            // btnRoom
            // 
            this.btnRoom.Enabled = false;
            this.btnRoom.Image = ((System.Drawing.Image)(resources.GetObject("btnRoom.Image")));
            this.btnRoom.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnRoom.ImageTransparentColor = System.Drawing.Color.White;
            this.btnRoom.Name = "btnRoom";
            this.btnRoom.Size = new System.Drawing.Size(67, 49);
            this.btnRoom.Text = "Enter Room";
            this.btnRoom.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnRoom.Click += new System.EventHandler(this.btnRoom_Click);
            // 
            // btnLobby
            // 
            this.btnLobby.Enabled = false;
            this.btnLobby.Image = ((System.Drawing.Image)(resources.GetObject("btnLobby.Image")));
            this.btnLobby.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnLobby.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.btnLobby.Name = "btnLobby";
            this.btnLobby.Size = new System.Drawing.Size(89, 49);
            this.btnLobby.Text = "Return to Lobby";
            this.btnLobby.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnLobby.Click += new System.EventHandler(this.btnLobby_Click);
            // 
            // sep1
            // 
            this.sep1.Name = "sep1";
            this.sep1.Size = new System.Drawing.Size(6, 52);
            // 
            // btnErase
            // 
            this.btnErase.Image = ((System.Drawing.Image)(resources.GetObject("btnErase.Image")));
            this.btnErase.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnErase.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.btnErase.Name = "btnErase";
            this.btnErase.Size = new System.Drawing.Size(80, 49);
            this.btnErase.Text = "Erase Drawing";
            this.btnErase.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnErase.Click += new System.EventHandler(this.btnErase_Click);
            // 
            // btnSelectColor
            // 
            this.btnSelectColor.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectColor.Image")));
            this.btnSelectColor.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnSelectColor.ImageTransparentColor = System.Drawing.Color.White;
            this.btnSelectColor.Name = "btnSelectColor";
            this.btnSelectColor.Size = new System.Drawing.Size(68, 49);
            this.btnSelectColor.Text = "Select Color";
            this.btnSelectColor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSelectColor.Click += new System.EventHandler(this.btnSelectColor_Click);
            // 
            // sep2
            // 
            this.sep2.Name = "sep2";
            this.sep2.Size = new System.Drawing.Size(6, 52);
            // 
            // btnServerStats
            // 
            this.btnServerStats.Enabled = false;
            this.btnServerStats.Image = ((System.Drawing.Image)(resources.GetObject("btnServerStats.Image")));
            this.btnServerStats.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnServerStats.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.btnServerStats.Name = "btnServerStats";
            this.btnServerStats.Size = new System.Drawing.Size(71, 49);
            this.btnServerStats.Text = "Server Stats";
            this.btnServerStats.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnServerStats.Click += new System.EventHandler(this.btnServerStats_Click);
            // 
            // btnStart
            // 
            this.btnStart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(35, 49);
            this.btnStart.Text = "Start";
            this.btnStart.ToolTipText = "GameCoordinator Test - Start Game";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // pnlDraw
            // 
            this.pnlDraw.BackColor = System.Drawing.Color.Black;
            this.pnlDraw.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pnlDraw.Location = new System.Drawing.Point(256, 56);
            this.pnlDraw.Name = "pnlDraw";
            this.pnlDraw.Size = new System.Drawing.Size(368, 176);
            this.pnlDraw.TabIndex = 11;
            this.pnlDraw.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlDraw_Paint);
            this.pnlDraw.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlDraw_MouseMove);
            this.pnlDraw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlDraw_MouseDown);
            this.pnlDraw.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlDraw_MouseUp);
            // 
            // grpUsers
            // 
            this.grpUsers.Controls.Add(this.lvUsers);
            this.grpUsers.Location = new System.Drawing.Point(8, 56);
            this.grpUsers.Name = "grpUsers";
            this.grpUsers.Size = new System.Drawing.Size(240, 360);
            this.grpUsers.TabIndex = 12;
            this.grpUsers.TabStop = false;
            this.grpUsers.Text = "Users";
            // 
            // lvUsers
            // 
            this.lvUsers.LargeImageList = this.imagesUsers;
            this.lvUsers.Location = new System.Drawing.Point(8, 16);
            this.lvUsers.Name = "lvUsers";
            this.lvUsers.Size = new System.Drawing.Size(224, 336);
            this.lvUsers.TabIndex = 0;
            this.lvUsers.UseCompatibleStateImageBehavior = false;
            this.lvUsers.DoubleClick += new System.EventHandler(this.lvUsers_DoubleClick);
            // 
            // imagesUsers
            // 
            this.imagesUsers.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imagesUsers.ImageSize = new System.Drawing.Size(32, 32);
            this.imagesUsers.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // connection
            // 
            this.connection.Active = false;
            this.connection.Host = "localhost";
            this.connection.Port = 4000;
            this.connection.SubjectName = "ChatNDraw";
            this.connection.LoginError += new System.EventHandler<SCG.Prism.MessageEventArgs>(this.connection_LoginError);
            this.connection.DataMessageReceived += new System.EventHandler<SCG.Prism.PrismUserMessageEventArgs>(this.connection_DataMessageReceived);
            this.connection.UserLeftRoom += new System.EventHandler<SCG.Prism.PrismUserEventArgs>(this.connection_UserLeftRoom);
            this.connection.UserAddedToRoom += new System.EventHandler<SCG.Prism.PrismUserEventArgs>(this.connection_UserAddedToRoom);
            this.connection.LoginOK += new System.EventHandler<System.EventArgs>(this.connection_LoginOK);
            this.connection.JoinedRoom += new System.EventHandler<SCG.Prism.RoomNameEventArgs>(this.connection_JoinedRoom);
            this.connection.ChatMessageReceived += new System.EventHandler<SCG.Prism.PrismUserMessageEventArgs>(this.connection_ChatMessageReceived);
            this.connection.ServerStatsReceived += new System.EventHandler<SCG.Prism.PrismServerStatsEventArgs>(this.connection_ServerStatsReceived);
            this.connection.Disconnected += new System.EventHandler<System.EventArgs>(this.connection_Disconnected);
            this.connection.PrismError += new System.EventHandler<SCG.Prism.MessageEventArgs>(this.connection_PrismError);
            this.connection.RoomRemoved += new System.EventHandler<SCG.Prism.RoomNameEventArgs>(this.connection_RoomRemoved);
            this.connection.RoomAdded += new System.EventHandler<SCG.Prism.RoomNameEventArgs>(this.connection_RoomAdded);
            this.connection.AdminMessageReceived += new System.EventHandler<SCG.Prism.MessageEventArgs>(this.connection_AdminMessageReceived);
            this.connection.UserInfoChanged += new System.EventHandler<SCG.Prism.PrismUserEventArgs>(this.connection_UserInfoChanged);
            // 
            // dlgUserInfo
            // 
            this.dlgUserInfo.EditState = SCG.Prism.PrismUserInfoDialogState.DisplayOnly;
            this.dlgUserInfo.Title = "User Info";
            // 
            // dlgServerStats
            // 
            this.dlgServerStats.Title = "PrismServer Statistics";
            // 
            // realtime
            // 
            this.realtime.Impulse = 0;
            this.realtime.ImpulseLengthMS = 1000;
            this.realtime.Prism = this.connection;
            this.realtime.ImpulseProcessed += new System.EventHandler<System.EventArgs>(this.realtime_ImpulseProcessed);
            this.realtime.GetOrders += new System.EventHandler<SCG.Prism.OrdersEventArgs>(this.realtime_GetOrders);
            this.realtime.ProcessOrdersGetChanges += new System.EventHandler<SCG.Prism.OrdersEventArgs>(this.realtime_ProcessOrdersGetChanges);
            this.realtime.ProcessChanges += new System.EventHandler<SCG.Prism.MessageEventArgs>(this.realtime_ProcessChanges);
            // 
            // ChatNDrawForm
            // 
            this.AcceptButton = this.btnSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(632, 453);
            this.Controls.Add(this.grpUsers);
            this.Controls.Add(this.pnlDraw);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.txtChat);
            this.Controls.Add(this.lblChat);
            this.Controls.Add(this.txtChatLog);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChatNDrawForm";
            this.Text = "Silicon Commander Games Chat \'N Draw .NET 5.1";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.grpUsers.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SCG.Prism.PrismConnection connection;
        private System.Windows.Forms.TextBox txtChatLog;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TextBox txtChat;
        private System.Windows.Forms.Label lblChat;
        private System.Windows.Forms.ColorDialog dlgPenColor;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel pnlDraw;
        private System.Windows.Forms.GroupBox grpUsers;
        private System.Windows.Forms.ListView lvUsers;
        private System.Windows.Forms.ImageList imagesUsers;
        private System.Windows.Forms.ToolStripButton btnErase;
        private System.Windows.Forms.ToolStripButton btnSelectColor;
        private System.Windows.Forms.ToolStripButton btnConnect;
        private System.Windows.Forms.ToolStripButton btnDisconnect;
        private System.Windows.Forms.ToolStripSeparator sep1;
        private SCG.Prism.PrismUserInfoDialog dlgUserInfo;
        private System.Windows.Forms.ToolStripSeparator sep2;
        private System.Windows.Forms.ToolStripButton btnServerStats;
        private SCG.Prism.PrismServerStatsDialog dlgServerStats;
        private System.Windows.Forms.ToolStripSeparator sep3;
        private System.Windows.Forms.ToolStripButton btnRoom;
        private System.Windows.Forms.ToolStripButton btnLobby;
        private System.Windows.Forms.ToolStripButton btnStart;
        private SCG.Prism.GameCoordinator realtime;        
    }
}

