namespace SCG.Prism
{
    partial class UserInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpUserInfo = new System.Windows.Forms.GroupBox();
            this.btnGlyph = new System.Windows.Forms.Button();
            this.txtPassword2 = new System.Windows.Forms.TextBox();
            this.lblPassword2 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.picGlyph = new System.Windows.Forms.PictureBox();
            this.grpPersonal = new System.Windows.Forms.GroupBox();
            this.txtEMail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtCountry = new System.Windows.Forms.TextBox();
            this.lblCountry = new System.Windows.Forms.Label();
            this.txtStateZip = new System.Windows.Forms.TextBox();
            this.lblStateZip = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.grpProfile = new System.Windows.Forms.GroupBox();
            this.txtProfile = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.dlgOpenGlyph = new System.Windows.Forms.OpenFileDialog();
            this.grpStatistics = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLastLoginDate = new System.Windows.Forms.Label();
            this.lblCreationDateStat = new System.Windows.Forms.Label();
            this.lblCreationDate = new System.Windows.Forms.Label();
            this.lblLastLoginDateStat = new System.Windows.Forms.Label();
            this.lblLoginCount = new System.Windows.Forms.Label();
            this.lblLoginCountStat = new System.Windows.Forms.Label();
            this.grpUserInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picGlyph)).BeginInit();
            this.grpPersonal.SuspendLayout();
            this.grpProfile.SuspendLayout();
            this.grpStatistics.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpUserInfo
            // 
            this.grpUserInfo.Controls.Add(this.btnGlyph);
            this.grpUserInfo.Controls.Add(this.txtPassword2);
            this.grpUserInfo.Controls.Add(this.lblPassword2);
            this.grpUserInfo.Controls.Add(this.txtPassword);
            this.grpUserInfo.Controls.Add(this.lblPassword);
            this.grpUserInfo.Controls.Add(this.txtUserName);
            this.grpUserInfo.Controls.Add(this.lblUserName);
            this.grpUserInfo.Controls.Add(this.picGlyph);
            this.grpUserInfo.Location = new System.Drawing.Point(8, 8);
            this.grpUserInfo.Name = "grpUserInfo";
            this.grpUserInfo.Size = new System.Drawing.Size(240, 104);
            this.grpUserInfo.TabIndex = 0;
            this.grpUserInfo.TabStop = false;
            this.grpUserInfo.Text = "User Information";
            // 
            // btnGlyph
            // 
            this.btnGlyph.Location = new System.Drawing.Point(8, 64);
            this.btnGlyph.Name = "btnGlyph";
            this.btnGlyph.Size = new System.Drawing.Size(48, 23);
            this.btnGlyph.TabIndex = 7;
            this.btnGlyph.Text = "Glyph";
            this.btnGlyph.UseVisualStyleBackColor = true;
            this.btnGlyph.Click += new System.EventHandler(this.btnGlyph_Click);
            // 
            // txtPassword2
            // 
            this.txtPassword2.Location = new System.Drawing.Point(128, 68);
            this.txtPassword2.Name = "txtPassword2";
            this.txtPassword2.PasswordChar = '*';
            this.txtPassword2.Size = new System.Drawing.Size(104, 20);
            this.txtPassword2.TabIndex = 6;
            // 
            // lblPassword2
            // 
            this.lblPassword2.AutoSize = true;
            this.lblPassword2.Location = new System.Drawing.Point(64, 68);
            this.lblPassword2.Name = "lblPassword2";
            this.lblPassword2.Size = new System.Drawing.Size(51, 13);
            this.lblPassword2.TabIndex = 5;
            this.lblPassword2.Text = "Re-Type:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(128, 48);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(104, 20);
            this.txtPassword.TabIndex = 4;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(64, 48);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(56, 13);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Password:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(128, 24);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(104, 20);
            this.txtUserName.TabIndex = 2;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(64, 24);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(63, 13);
            this.lblUserName.TabIndex = 1;
            this.lblUserName.Text = "User Name:";
            // 
            // picGlyph
            // 
            this.picGlyph.Location = new System.Drawing.Point(16, 24);
            this.picGlyph.Name = "picGlyph";
            this.picGlyph.Size = new System.Drawing.Size(32, 32);
            this.picGlyph.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picGlyph.TabIndex = 0;
            this.picGlyph.TabStop = false;
            // 
            // grpPersonal
            // 
            this.grpPersonal.Controls.Add(this.txtEMail);
            this.grpPersonal.Controls.Add(this.lblEmail);
            this.grpPersonal.Controls.Add(this.txtCountry);
            this.grpPersonal.Controls.Add(this.lblCountry);
            this.grpPersonal.Controls.Add(this.txtStateZip);
            this.grpPersonal.Controls.Add(this.lblStateZip);
            this.grpPersonal.Controls.Add(this.txtCity);
            this.grpPersonal.Controls.Add(this.lblCity);
            this.grpPersonal.Controls.Add(this.txtAddress2);
            this.grpPersonal.Controls.Add(this.txtAddress1);
            this.grpPersonal.Controls.Add(this.lblAddress);
            this.grpPersonal.Controls.Add(this.txtLastName);
            this.grpPersonal.Controls.Add(this.lblLastName);
            this.grpPersonal.Controls.Add(this.txtFirstName);
            this.grpPersonal.Controls.Add(this.lblFirstName);
            this.grpPersonal.Location = new System.Drawing.Point(8, 112);
            this.grpPersonal.Name = "grpPersonal";
            this.grpPersonal.Size = new System.Drawing.Size(240, 208);
            this.grpPersonal.TabIndex = 1;
            this.grpPersonal.TabStop = false;
            this.grpPersonal.Text = "Personal Information";
            // 
            // txtEMail
            // 
            this.txtEMail.Location = new System.Drawing.Point(88, 176);
            this.txtEMail.Name = "txtEMail";
            this.txtEMail.Size = new System.Drawing.Size(144, 20);
            this.txtEMail.TabIndex = 14;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(16, 176);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(39, 13);
            this.lblEmail.TabIndex = 13;
            this.lblEmail.Text = "E-Mail:";
            // 
            // txtCountry
            // 
            this.txtCountry.Location = new System.Drawing.Point(88, 152);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(144, 20);
            this.txtCountry.TabIndex = 12;
            // 
            // lblCountry
            // 
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(16, 152);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(46, 13);
            this.lblCountry.TabIndex = 11;
            this.lblCountry.Text = "Country:";
            // 
            // txtStateZip
            // 
            this.txtStateZip.Location = new System.Drawing.Point(88, 132);
            this.txtStateZip.Name = "txtStateZip";
            this.txtStateZip.Size = new System.Drawing.Size(144, 20);
            this.txtStateZip.TabIndex = 10;
            // 
            // lblStateZip
            // 
            this.lblStateZip.AutoSize = true;
            this.lblStateZip.Location = new System.Drawing.Point(16, 132);
            this.lblStateZip.Name = "lblStateZip";
            this.lblStateZip.Size = new System.Drawing.Size(55, 13);
            this.lblStateZip.TabIndex = 9;
            this.lblStateZip.Text = "State/Zip:";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(88, 112);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(144, 20);
            this.txtCity.TabIndex = 8;
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(16, 112);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(27, 13);
            this.lblCity.TabIndex = 7;
            this.lblCity.Text = "City:";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(88, 92);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(144, 20);
            this.txtAddress2.TabIndex = 6;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(88, 72);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(144, 20);
            this.txtAddress1.TabIndex = 5;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(16, 72);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(48, 13);
            this.lblAddress.TabIndex = 4;
            this.lblAddress.Text = "Address:";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(88, 44);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(144, 20);
            this.txtLastName.TabIndex = 3;
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(16, 44);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(61, 13);
            this.lblLastName.TabIndex = 2;
            this.lblLastName.Text = "Last Name:";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(88, 24);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(144, 20);
            this.txtFirstName.TabIndex = 1;
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(16, 24);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(60, 13);
            this.lblFirstName.TabIndex = 0;
            this.lblFirstName.Text = "First Name:";
            // 
            // grpProfile
            // 
            this.grpProfile.Controls.Add(this.txtProfile);
            this.grpProfile.Location = new System.Drawing.Point(256, 8);
            this.grpProfile.Name = "grpProfile";
            this.grpProfile.Size = new System.Drawing.Size(232, 200);
            this.grpProfile.TabIndex = 2;
            this.grpProfile.TabStop = false;
            this.grpProfile.Text = "Profile";
            // 
            // txtProfile
            // 
            this.txtProfile.BackColor = System.Drawing.Color.Linen;
            this.txtProfile.Location = new System.Drawing.Point(8, 16);
            this.txtProfile.Multiline = true;
            this.txtProfile.Name = "txtProfile";
            this.txtProfile.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtProfile.Size = new System.Drawing.Size(216, 176);
            this.txtProfile.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(336, 328);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(416, 328);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // dlgOpenGlyph
            // 
            this.dlgOpenGlyph.DefaultExt = "bmp";
            this.dlgOpenGlyph.FileName = "openFileDialog1";
            this.dlgOpenGlyph.Filter = "Bitmap files|*.bmp";
            this.dlgOpenGlyph.Title = "Select Glyph Bitmap";
            // 
            // grpStatistics
            // 
            this.grpStatistics.Controls.Add(this.lblLoginCountStat);
            this.grpStatistics.Controls.Add(this.lblLoginCount);
            this.grpStatistics.Controls.Add(this.lblLastLoginDateStat);
            this.grpStatistics.Controls.Add(this.label2);
            this.grpStatistics.Controls.Add(this.lblLastLoginDate);
            this.grpStatistics.Controls.Add(this.lblCreationDateStat);
            this.grpStatistics.Controls.Add(this.lblCreationDate);
            this.grpStatistics.Location = new System.Drawing.Point(256, 208);
            this.grpStatistics.Name = "grpStatistics";
            this.grpStatistics.Size = new System.Drawing.Size(232, 104);
            this.grpStatistics.TabIndex = 5;
            this.grpStatistics.TabStop = false;
            this.grpStatistics.Text = "Statistics";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(16, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 3;
            // 
            // lblLastLoginDate
            // 
            this.lblLastLoginDate.AutoSize = true;
            this.lblLastLoginDate.Location = new System.Drawing.Point(16, 48);
            this.lblLastLoginDate.Name = "lblLastLoginDate";
            this.lblLastLoginDate.Size = new System.Drawing.Size(110, 13);
            this.lblLastLoginDate.TabIndex = 2;
            this.lblLastLoginDate.Text = "Last Login Date/Time";
            // 
            // lblCreationDateStat
            // 
            this.lblCreationDateStat.AutoSize = true;
            this.lblCreationDateStat.ForeColor = System.Drawing.Color.Navy;
            this.lblCreationDateStat.Location = new System.Drawing.Point(16, 32);
            this.lblCreationDateStat.Name = "lblCreationDateStat";
            this.lblCreationDateStat.Size = new System.Drawing.Size(53, 13);
            this.lblCreationDateStat.TabIndex = 1;
            this.lblCreationDateStat.Text = "1/1/2000";
            // 
            // lblCreationDate
            // 
            this.lblCreationDate.AutoSize = true;
            this.lblCreationDate.Location = new System.Drawing.Point(16, 16);
            this.lblCreationDate.Name = "lblCreationDate";
            this.lblCreationDate.Size = new System.Drawing.Size(100, 13);
            this.lblCreationDate.TabIndex = 0;
            this.lblCreationDate.Text = "Creation Date/Time";
            // 
            // lblLastLoginDateStat
            // 
            this.lblLastLoginDateStat.AutoSize = true;
            this.lblLastLoginDateStat.ForeColor = System.Drawing.Color.Navy;
            this.lblLastLoginDateStat.Location = new System.Drawing.Point(16, 64);
            this.lblLastLoginDateStat.Name = "lblLastLoginDateStat";
            this.lblLastLoginDateStat.Size = new System.Drawing.Size(53, 13);
            this.lblLastLoginDateStat.TabIndex = 4;
            this.lblLastLoginDateStat.Text = "1/1/2000";
            // 
            // lblLoginCount
            // 
            this.lblLoginCount.AutoSize = true;
            this.lblLoginCount.Location = new System.Drawing.Point(16, 80);
            this.lblLoginCount.Name = "lblLoginCount";
            this.lblLoginCount.Size = new System.Drawing.Size(67, 13);
            this.lblLoginCount.TabIndex = 5;
            this.lblLoginCount.Text = "Login Count:";
            // 
            // lblLoginCountStat
            // 
            this.lblLoginCountStat.AutoSize = true;
            this.lblLoginCountStat.ForeColor = System.Drawing.Color.Navy;
            this.lblLoginCountStat.Location = new System.Drawing.Point(80, 80);
            this.lblLoginCountStat.Name = "lblLoginCountStat";
            this.lblLoginCountStat.Size = new System.Drawing.Size(53, 13);
            this.lblLoginCountStat.TabIndex = 6;
            this.lblLoginCountStat.Text = "1/1/2000";
            // 
            // UserInfoForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(498, 357);
            this.Controls.Add(this.grpStatistics);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.grpProfile);
            this.Controls.Add(this.grpPersonal);
            this.Controls.Add(this.grpUserInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "UserInfoForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "User Information";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UserInfoForm_FormClosing);
            this.grpUserInfo.ResumeLayout(false);
            this.grpUserInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picGlyph)).EndInit();
            this.grpPersonal.ResumeLayout(false);
            this.grpPersonal.PerformLayout();
            this.grpProfile.ResumeLayout(false);
            this.grpProfile.PerformLayout();
            this.grpStatistics.ResumeLayout(false);
            this.grpStatistics.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpUserInfo;
        private System.Windows.Forms.Label lblPassword;
        public System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblUserName;
        public System.Windows.Forms.PictureBox picGlyph;
        private System.Windows.Forms.Button btnGlyph;
        public System.Windows.Forms.TextBox txtPassword2;
        private System.Windows.Forms.Label lblPassword2;
        public System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.GroupBox grpPersonal;
        public System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label lblCity;
        public System.Windows.Forms.TextBox txtAddress2;
        public System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.Label lblAddress;
        public System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label lblLastName;
        public System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label lblFirstName;
        public System.Windows.Forms.TextBox txtEMail;
        private System.Windows.Forms.Label lblEmail;
        public System.Windows.Forms.TextBox txtCountry;
        private System.Windows.Forms.Label lblCountry;
        public System.Windows.Forms.TextBox txtStateZip;
        private System.Windows.Forms.Label lblStateZip;
        private System.Windows.Forms.GroupBox grpProfile;
        public System.Windows.Forms.TextBox txtProfile;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.OpenFileDialog dlgOpenGlyph;
        private System.Windows.Forms.GroupBox grpStatistics;
        private System.Windows.Forms.Label lblLastLoginDate;
        private System.Windows.Forms.Label lblCreationDateStat;
        private System.Windows.Forms.Label lblCreationDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblLoginCount;
        private System.Windows.Forms.Label lblLastLoginDateStat;
        private System.Windows.Forms.Label lblLoginCountStat;
    }
}