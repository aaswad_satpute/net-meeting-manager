#region copyright
/*
* Copyright (c) 2007, Dion Kurczek
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DION KURCZEK ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DION KURCZEK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SCG.Prism
{
    //Display and optionally edit PrismUser information
    public partial class UserInfoForm : Form
    {
        //Private members
        private bool _editable;
        private bool _newUserEditable;

        //Constructors
        public UserInfoForm()
        {
            InitializeComponent();
        }

        //Allow editing?
        public bool UserEditable
        {
            get
            {
                return _editable;
            }
            set
            {
                _editable = value;
                txtUserName.Enabled = false;
                lblPassword.Visible = value;
                txtPassword.Visible = value;
                lblPassword2.Visible = value;
                txtPassword2.Visible = value;
                btnGlyph.Visible = value;
                txtFirstName.Enabled = value;
                txtLastName.Enabled = value;
                txtAddress1.Enabled = value;
                txtAddress2.Enabled = value;
                txtCity.Enabled = value;
                txtStateZip.Enabled = value;
                txtCountry.Enabled = value;
                txtEMail.Enabled = value;
                txtProfile.Enabled = value;
                btnCancel.Visible = value;
            }
        }

        //New user being edited?
        public bool NewUserEditable
        {
            get
            {
                return _newUserEditable;
            }
            set
            {
                if (value)
                    UserEditable = value;
                _newUserEditable = value;               
                txtUserName.Enabled = value;
                grpStatistics.Visible = !value;
            }
        }

        //Populate form with user information
        public void PopulateForm(PrismUser user)
        {
            txtUserName.Text = user.UserName;
            txtPassword.Text = user.Password;
            txtPassword2.Text = user.Password;
            picGlyph.Image = user.Glyph;
            txtFirstName.Text = user.FirstName;
            txtLastName.Text = user.LastName;
            txtAddress1.Text = user.Address1;
            txtAddress2.Text = user.Address2;
            txtCity.Text = user.City;
            txtStateZip.Text = user.StateZip;
            txtCountry.Text = user.Country;
            txtEMail.Text = user.Email;
            txtProfile.Text = user.Profile;
            lblCreationDateStat.Text = user.CreationDate.ToString();
            lblLastLoginDateStat.Text = user.LastLoginDate.ToString();
            lblLoginCountStat.Text = user.LoginCount.ToString();
        }

        //Populate a PrismUser from form fields
        public void PopulateUserInfo(PrismUser user)
        {
            user.UserName = txtUserName.Text;
            user.Password = txtPassword.Text;
            user.Glyph = (Bitmap)picGlyph.Image;
            user.FirstName = txtFirstName.Text;
            user.LastName = txtLastName.Text;
            user.Address1 = txtAddress1.Text;
            user.Address2 = txtAddress2.Text;
            user.City = txtCity.Text;
            user.StateZip = txtStateZip.Text;
            user.Country = txtCountry.Text;
            user.Email = txtEMail.Text;
            user.Profile = txtProfile.Text;
        }

        //Assign the glyph
        private void btnGlyph_Click(object sender, EventArgs e)
        {
            if (dlgOpenGlyph.ShowDialog() == DialogResult.OK)
                picGlyph.Image = (Bitmap)Bitmap.FromFile(dlgOpenGlyph.FileName);
        }

        //Make sure user name and password fields are populated
        private void UserInfoForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                if (txtUserName.Text == "")
                {
                    MessageBox.Show("Please specify a User Name");
                    txtUserName.Focus();
                    e.Cancel = true;
                }
                else if (txtPassword.Text == "")
                {
                    MessageBox.Show("Please specify a Password");
                    txtPassword.Focus();
                    e.Cancel = true;
                }
                else if (txtPassword.Text != txtPassword2.Text)
                {
                    MessageBox.Show("The specified Passwords do not match");
                    txtPassword2.Focus();
                    e.Cancel = true;
                }
            }
        }
  

    }
}