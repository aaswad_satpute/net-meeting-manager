namespace PrismServerAdmin
{
    partial class PrismServerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrismServerForm));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Entire Server", 2, 2);
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Staging Area", 1, 1);
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.btnStart = new System.Windows.Forms.ToolStripButton();
            this.btnStop = new System.Windows.Forms.ToolStripButton();
            this.btnAdminMsg = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnClearLogs = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSettings = new System.Windows.Forms.ToolStripButton();
            this.btnStats = new System.Windows.Forms.ToolStripButton();
            this.statusMain = new System.Windows.Forms.StatusStrip();
            this.statusState = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusGuests = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusTimeActive = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treePrism = new System.Windows.Forms.TreeView();
            this.imlistPrism = new System.Windows.Forms.ImageList(this.components);
            this.tabMain = new System.Windows.Forms.TabControl();
            this.pageGuests = new System.Windows.Forms.TabPage();
            this.lvGuests = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.pageConnLog = new System.Windows.Forms.TabPage();
            this.txtConnLog = new System.Windows.Forms.TextBox();
            this.pageChatLog = new System.Windows.Forms.TabPage();
            this.txtChatLog = new System.Windows.Forms.TextBox();
            this.prism = new SCG.Prism.PrismServer(this.components);
            this.prismDB = new SCG.Prism.PrismServerFileImplementation(this.components);
            this.dlgUserInfo = new SCG.Prism.PrismUserInfoDialog(this.components);
            this.dlgStats = new SCG.Prism.PrismServerStatsDialog(this.components);
            this.timerUI = new System.Windows.Forms.Timer(this.components);
            this.toolStripContainer1.SuspendLayout();
            this.tsMain.SuspendLayout();
            this.statusMain.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.pageGuests.SuspendLayout();
            this.pageConnLog.SuspendLayout();
            this.pageChatLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            this.toolStripContainer1.BottomToolStripPanelVisible = false;
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(150, 150);
            this.toolStripContainer1.LeftToolStripPanelVisible = false;
            this.toolStripContainer1.Location = new System.Drawing.Point(448, 152);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.RightToolStripPanelVisible = false;
            this.toolStripContainer1.Size = new System.Drawing.Size(150, 175);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // tsMain
            // 
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnStart,
            this.btnStop,
            this.btnAdminMsg,
            this.toolStripSeparator1,
            this.btnClearLogs,
            this.toolStripSeparator2,
            this.btnSettings,
            this.btnStats});
            this.tsMain.Location = new System.Drawing.Point(0, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.Size = new System.Drawing.Size(632, 52);
            this.tsMain.TabIndex = 2;
            this.tsMain.Text = "toolStrip1";
            // 
            // btnStart
            // 
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnStart.ImageTransparentColor = System.Drawing.Color.White;
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(36, 49);
            this.btnStart.Text = "Start";
            this.btnStart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnStart.ToolTipText = "Start Server";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Image = ((System.Drawing.Image)(resources.GetObject("btnStop.Image")));
            this.btnStop.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnStop.ImageTransparentColor = System.Drawing.Color.White;
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(36, 49);
            this.btnStop.Text = "Stop";
            this.btnStop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnStop.ToolTipText = "Stop Server";
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnAdminMsg
            // 
            this.btnAdminMsg.Enabled = false;
            this.btnAdminMsg.Image = ((System.Drawing.Image)(resources.GetObject("btnAdminMsg.Image")));
            this.btnAdminMsg.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnAdminMsg.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.btnAdminMsg.Name = "btnAdminMsg";
            this.btnAdminMsg.Size = new System.Drawing.Size(62, 49);
            this.btnAdminMsg.Text = "Admin Msg";
            this.btnAdminMsg.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnAdminMsg.ToolTipText = "Send an Admin Message to Guests";
            this.btnAdminMsg.Click += new System.EventHandler(this.btnAdminMsg_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 52);
            // 
            // btnClearLogs
            // 
            this.btnClearLogs.Image = ((System.Drawing.Image)(resources.GetObject("btnClearLogs.Image")));
            this.btnClearLogs.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnClearLogs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClearLogs.Name = "btnClearLogs";
            this.btnClearLogs.Size = new System.Drawing.Size(61, 49);
            this.btnClearLogs.Text = "Clear Logs";
            this.btnClearLogs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnClearLogs.Click += new System.EventHandler(this.btnClearLogs_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 52);
            // 
            // btnSettings
            // 
            this.btnSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnSettings.Image")));
            this.btnSettings.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnSettings.ImageTransparentColor = System.Drawing.Color.White;
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(50, 49);
            this.btnSettings.Text = "Settings";
            this.btnSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSettings.ToolTipText = "Change Server Settings";
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnStats
            // 
            this.btnStats.Enabled = false;
            this.btnStats.Image = ((System.Drawing.Image)(resources.GetObject("btnStats.Image")));
            this.btnStats.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnStats.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.btnStats.Name = "btnStats";
            this.btnStats.Size = new System.Drawing.Size(71, 49);
            this.btnStats.Text = "Server Stats";
            this.btnStats.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnStats.ToolTipText = "View Server Statistics";
            this.btnStats.Click += new System.EventHandler(this.btnStats_Click);
            // 
            // statusMain
            // 
            this.statusMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusState,
            this.statusGuests,
            this.statusTimeActive});
            this.statusMain.Location = new System.Drawing.Point(0, 431);
            this.statusMain.Name = "statusMain";
            this.statusMain.Size = new System.Drawing.Size(632, 22);
            this.statusMain.TabIndex = 6;
            this.statusMain.Text = "statusStrip1";
            // 
            // statusState
            // 
            this.statusState.AutoSize = false;
            this.statusState.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.statusState.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.statusState.Name = "statusState";
            this.statusState.Size = new System.Drawing.Size(200, 17);
            this.statusState.Text = "Inactive";
            // 
            // statusGuests
            // 
            this.statusGuests.AutoSize = false;
            this.statusGuests.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.statusGuests.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.statusGuests.Name = "statusGuests";
            this.statusGuests.Size = new System.Drawing.Size(120, 17);
            this.statusGuests.Text = "0 Guests";
            // 
            // statusTimeActive
            // 
            this.statusTimeActive.AutoSize = false;
            this.statusTimeActive.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.statusTimeActive.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.statusTimeActive.Name = "statusTimeActive";
            this.statusTimeActive.Size = new System.Drawing.Size(200, 17);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 52);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treePrism);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabMain);
            this.splitContainer1.Size = new System.Drawing.Size(632, 379);
            this.splitContainer1.SplitterDistance = 220;
            this.splitContainer1.TabIndex = 7;
            // 
            // treePrism
            // 
            this.treePrism.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treePrism.HideSelection = false;
            this.treePrism.ImageIndex = 0;
            this.treePrism.ImageList = this.imlistPrism;
            this.treePrism.Location = new System.Drawing.Point(0, 0);
            this.treePrism.Name = "treePrism";
            treeNode1.ImageIndex = 2;
            treeNode1.Name = "Node1";
            treeNode1.SelectedImageIndex = 2;
            treeNode1.Text = "Entire Server";
            treeNode2.ImageIndex = 1;
            treeNode2.Name = "Node0";
            treeNode2.SelectedImageIndex = 1;
            treeNode2.Text = "Staging Area";
            this.treePrism.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            this.treePrism.SelectedImageIndex = 0;
            this.treePrism.Size = new System.Drawing.Size(220, 379);
            this.treePrism.TabIndex = 0;
            this.treePrism.MouseUp += new System.Windows.Forms.MouseEventHandler(this.treePrism_MouseUp);
            this.treePrism.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treePrism_AfterSelect);
            // 
            // imlistPrism
            // 
            this.imlistPrism.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlistPrism.ImageStream")));
            this.imlistPrism.TransparentColor = System.Drawing.Color.Transparent;
            this.imlistPrism.Images.SetKeyName(0, "");
            this.imlistPrism.Images.SetKeyName(1, "");
            this.imlistPrism.Images.SetKeyName(2, "Server.bmp");
            this.imlistPrism.Images.SetKeyName(3, "Door.bmp");
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.pageGuests);
            this.tabMain.Controls.Add(this.pageConnLog);
            this.tabMain.Controls.Add(this.pageChatLog);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(408, 379);
            this.tabMain.TabIndex = 0;
            // 
            // pageGuests
            // 
            this.pageGuests.Controls.Add(this.lvGuests);
            this.pageGuests.Location = new System.Drawing.Point(4, 22);
            this.pageGuests.Name = "pageGuests";
            this.pageGuests.Padding = new System.Windows.Forms.Padding(3);
            this.pageGuests.Size = new System.Drawing.Size(400, 353);
            this.pageGuests.TabIndex = 0;
            this.pageGuests.Text = "Guests";
            this.pageGuests.UseVisualStyleBackColor = true;
            // 
            // lvGuests
            // 
            this.lvGuests.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lvGuests.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvGuests.FullRowSelect = true;
            this.lvGuests.HideSelection = false;
            this.lvGuests.Location = new System.Drawing.Point(3, 3);
            this.lvGuests.MultiSelect = false;
            this.lvGuests.Name = "lvGuests";
            this.lvGuests.Size = new System.Drawing.Size(394, 347);
            this.lvGuests.SmallImageList = this.imlistPrism;
            this.lvGuests.TabIndex = 0;
            this.lvGuests.UseCompatibleStateImageBehavior = false;
            this.lvGuests.View = System.Windows.Forms.View.Details;
            this.lvGuests.DoubleClick += new System.EventHandler(this.lvGuests_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "IP Address";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "User Name";
            this.columnHeader2.Width = 150;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Latency";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Login Count";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader4.Width = 70;
            // 
            // pageConnLog
            // 
            this.pageConnLog.Controls.Add(this.txtConnLog);
            this.pageConnLog.Location = new System.Drawing.Point(4, 22);
            this.pageConnLog.Name = "pageConnLog";
            this.pageConnLog.Padding = new System.Windows.Forms.Padding(3);
            this.pageConnLog.Size = new System.Drawing.Size(400, 353);
            this.pageConnLog.TabIndex = 1;
            this.pageConnLog.Text = "Connection Log";
            this.pageConnLog.UseVisualStyleBackColor = true;
            // 
            // txtConnLog
            // 
            this.txtConnLog.BackColor = System.Drawing.Color.Cornsilk;
            this.txtConnLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtConnLog.Location = new System.Drawing.Point(3, 3);
            this.txtConnLog.Multiline = true;
            this.txtConnLog.Name = "txtConnLog";
            this.txtConnLog.ReadOnly = true;
            this.txtConnLog.Size = new System.Drawing.Size(394, 347);
            this.txtConnLog.TabIndex = 0;
            // 
            // pageChatLog
            // 
            this.pageChatLog.Controls.Add(this.txtChatLog);
            this.pageChatLog.Location = new System.Drawing.Point(4, 22);
            this.pageChatLog.Name = "pageChatLog";
            this.pageChatLog.Padding = new System.Windows.Forms.Padding(3);
            this.pageChatLog.Size = new System.Drawing.Size(400, 353);
            this.pageChatLog.TabIndex = 2;
            this.pageChatLog.Text = "Chat Log";
            this.pageChatLog.UseVisualStyleBackColor = true;
            // 
            // txtChatLog
            // 
            this.txtChatLog.BackColor = System.Drawing.Color.Azure;
            this.txtChatLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtChatLog.Location = new System.Drawing.Point(3, 3);
            this.txtChatLog.Multiline = true;
            this.txtChatLog.Name = "txtChatLog";
            this.txtChatLog.ReadOnly = true;
            this.txtChatLog.Size = new System.Drawing.Size(394, 347);
            this.txtChatLog.TabIndex = 1;
            // 
            // prism
            // 
            this.prism.Active = false;
            this.prism.Implementation = this.prismDB;
            this.prism.LobbyName = "Lobby";
            this.prism.PingInterval = 30;
            this.prism.Port = 4000;
            this.prism.ProhibitSameIP = false;
            this.prism.ProhibitSameUserName = true;
            this.prism.GuestDisconnected += new System.EventHandler<SCG.Prism.PrismGuestEventArgs>(this.prism_GuestDisconnected);
            this.prism.GuestDisconnectedSameIP += new System.EventHandler<SCG.Prism.PrismGuestEventArgs>(this.prism_GuestDisconnectedSameIP);
            this.prism.Pinging += new System.EventHandler<System.EventArgs>(this.prism_Pinging);
            this.prism.RoomRemoved += new System.EventHandler<SCG.Prism.PrismRoomEventArgs>(this.prism_RoomRemoved);
            this.prism.GuestConnected += new System.EventHandler<SCG.Prism.PrismGuestEventArgs>(this.prism_GuestConnected);
            this.prism.RoomAdded += new System.EventHandler<SCG.Prism.PrismRoomEventArgs>(this.prism_RoomAdded);
            this.prism.GuestChat += new System.EventHandler<SCG.Prism.PrismGuestMsgEventArgs>(this.prism_GuestChat);
            this.prism.GuestLoggedIn += new System.EventHandler<SCG.Prism.PrismGuestEventArgs>(this.prism_GuestLoggedIn);
            // 
            // prismDB
            // 
            this.prismDB.Server = this.prism;
            // 
            // dlgUserInfo
            // 
            this.dlgUserInfo.EditState = SCG.Prism.PrismUserInfoDialogState.DisplayOnly;
            this.dlgUserInfo.Title = "User Info";
            // 
            // dlgStats
            // 
            this.dlgStats.Title = "Server Statistics";
            // 
            // timerUI
            // 
            this.timerUI.Enabled = true;
            this.timerUI.Tick += new System.EventHandler(this.timerUI_Tick);
            // 
            // PrismServerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 453);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusMain);
            this.Controls.Add(this.tsMain);
            this.Controls.Add(this.toolStripContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrismServerForm";
            this.Text = "Silicon Commander Games PrismServer.NET 5.1";
            this.Load += new System.EventHandler(this.PrismServerForm_Load);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.statusMain.ResumeLayout(false);
            this.statusMain.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabMain.ResumeLayout(false);
            this.pageGuests.ResumeLayout(false);
            this.pageConnLog.ResumeLayout(false);
            this.pageConnLog.PerformLayout();
            this.pageChatLog.ResumeLayout(false);
            this.pageChatLog.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStripButton btnStart;
        private System.Windows.Forms.ToolStripButton btnStop;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabPage pageGuests;
        private System.Windows.Forms.TabPage pageConnLog;
        private System.Windows.Forms.ToolStripStatusLabel statusState;
        private System.Windows.Forms.ToolStripStatusLabel statusGuests;
        private System.Windows.Forms.TextBox txtConnLog;
        private System.Windows.Forms.ToolStripStatusLabel statusTimeActive;
        private System.Windows.Forms.ToolStripButton btnSettings;
        private System.Windows.Forms.ListView lvGuests;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ImageList imlistPrism;
        private System.Windows.Forms.TabPage pageChatLog;
        private System.Windows.Forms.TextBox txtChatLog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnClearLogs;
        private System.Windows.Forms.ToolStripButton btnStats;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ToolStripButton btnAdminMsg;
        protected internal SCG.Prism.PrismServer prism;
        protected internal System.Windows.Forms.ToolStrip tsMain;
        protected internal System.Windows.Forms.StatusStrip statusMain;
        protected internal SCG.Prism.PrismServerFileImplementation prismDB;
        protected internal SCG.Prism.PrismUserInfoDialog dlgUserInfo;
        protected internal SCG.Prism.PrismServerStatsDialog dlgStats;
        protected internal System.Windows.Forms.Timer timerUI;
        protected internal System.Windows.Forms.TabControl tabMain;
        protected internal System.Windows.Forms.TreeView treePrism;
    }
}

