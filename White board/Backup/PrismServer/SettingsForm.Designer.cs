namespace PrismServerAdmin
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblPort = new System.Windows.Forms.Label();
            this.numPort = new System.Windows.Forms.NumericUpDown();
            this.cbProhibitMultipleConnections = new System.Windows.Forms.CheckBox();
            this.cbProhibitSameNameLogins = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numPort)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(208, 112);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(128, 112);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(8, 8);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(69, 13);
            this.lblPort.TabIndex = 2;
            this.lblPort.Text = "Port Number:";
            // 
            // numPort
            // 
            this.numPort.Location = new System.Drawing.Point(96, 8);
            this.numPort.Maximum = new decimal(new int[] {
            32760,
            0,
            0,
            0});
            this.numPort.Name = "numPort";
            this.numPort.Size = new System.Drawing.Size(120, 20);
            this.numPort.TabIndex = 3;
            this.numPort.Value = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            // 
            // cbProhibitMultipleConnections
            // 
            this.cbProhibitMultipleConnections.AutoSize = true;
            this.cbProhibitMultipleConnections.Location = new System.Drawing.Point(16, 40);
            this.cbProhibitMultipleConnections.Name = "cbProhibitMultipleConnections";
            this.cbProhibitMultipleConnections.Size = new System.Drawing.Size(267, 17);
            this.cbProhibitMultipleConnections.TabIndex = 4;
            this.cbProhibitMultipleConnections.Text = "Prohibit Multiple Connections from same IP Address";
            this.cbProhibitMultipleConnections.UseVisualStyleBackColor = true;
            // 
            // cbProhibitSameNameLogins
            // 
            this.cbProhibitSameNameLogins.AutoSize = true;
            this.cbProhibitSameNameLogins.Location = new System.Drawing.Point(16, 64);
            this.cbProhibitSameNameLogins.Name = "cbProhibitSameNameLogins";
            this.cbProhibitSameNameLogins.Size = new System.Drawing.Size(241, 17);
            this.cbProhibitSameNameLogins.TabIndex = 5;
            this.cbProhibitSameNameLogins.Text = "Prohibit Multiple Logins from same User Name";
            this.cbProhibitSameNameLogins.UseVisualStyleBackColor = true;
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(287, 140);
            this.Controls.Add(this.cbProhibitSameNameLogins);
            this.Controls.Add(this.cbProhibitMultipleConnections);
            this.Controls.Add(this.numPort);
            this.Controls.Add(this.lblPort);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "PrismServer Settings";
            ((System.ComponentModel.ISupportInitialize)(this.numPort)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblPort;
        public System.Windows.Forms.NumericUpDown numPort;
        public System.Windows.Forms.CheckBox cbProhibitMultipleConnections;
        public System.Windows.Forms.CheckBox cbProhibitSameNameLogins;
    }
}