#region copyright
/*
* Copyright (c) 2007, Dion Kurczek
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DION KURCZEK ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DION KURCZEK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using SCG.Prism;

namespace PrismServerAdmin
{
    //PrismServer - A form class that implements the PrismServer server and user admin interface
    public partial class PrismServerForm : Form
    {
        //Constants
        private static int SUBJECT = 0;
        private static int STAGING_AREA = 1;
        private static int ENTIRE_SERVER = 2;
        private static int ROOM = 3;

        //Private members
        private Queue<string> _connLogQ = new Queue<string>();
        private Queue<PrismGuest> _guestConnect = new Queue<PrismGuest>();
        private Queue<PrismGuest> _guestDisconnect = new Queue<PrismGuest>();
        private Queue<PrismGuest> _guestLogin = new Queue<PrismGuest>();
        private Queue<PrismRoom> _roomsAdded = new Queue<PrismRoom>();
        private Queue<PrismRoom> _roomsRemoved = new Queue<PrismRoom>();
        private Queue<string> _chat = new Queue<string>();

        //Constructor
        public PrismServerForm()
        {
            InitializeComponent();
        }

        //Clicked Start Button - start server listening
        private void btnStart_Click(object sender, EventArgs e)
        {
            prism.Active = true;
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            btnSettings.Enabled = false;
            btnStats.Enabled = true;
            btnAdminMsg.Enabled = true;
            statusState.Text = "Active: Port " + prism.Port;           
        }

        //Clicked Stop Button - shut down server
        private void btnStop_Click(object sender, EventArgs e)
        {
            btnStop.Enabled = false;
            btnStart.Enabled = true;
            btnSettings.Enabled = true;
            btnStats.Enabled = false;
            btnAdminMsg.Enabled = false;
            prism.Active = false;
            statusState.Text = "Inactive";
        }

        //A Guest has been connected
        private void prism_GuestConnected(object sender, PrismGuestEventArgs e)
        {
            //Update the connection log
            UpdateConnLog("Guest Connected: " + e.Guest.IPAddress);
            
            //Add the guest to the list for UI processing
            lock (_guestConnect)
            {
                _guestConnect.Enqueue(e.Guest);
            }
        }   
  
        //Locate a tree node with the specified text, at the specified level
        protected TreeNode FindNode(string s, int level, TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
                if (node.Level == level && node.Text == s)
                    return node;
            return null;
        }

        //Return correct plural form of value
        protected string Pluralize(int n, string s)
        {
            string Plural = n + " " + s;
            if (n != 1)
                Plural += "s";
            return Plural;
        }

        //The server is beginning a ping cycle
        private void prism_Pinging(object sender, EventArgs e)
        {
            Invoke(new PingCallback(Pinged));
        }
        private delegate void PingCallback();
        private void Pinged()
        {
            TimeSpan tsActive = DateTime.Now - prism.WhenActivated;
            string s = "";
            int d = (int)tsActive.TotalDays;
            if (d > 0)
                s = Pluralize(d, "day");
            if (tsActive.Hours > 0 || s != "")
                s += " " + Pluralize(tsActive.Hours, "hr");
            if (tsActive.Minutes > 0 || s != "")
                s += " " + Pluralize(tsActive.Minutes, "min");
            s += " " + Pluralize(tsActive.Seconds, "sec");
            statusTimeActive.Text = s;
        }

        //A guest has been disconnected
        private void prism_GuestDisconnected(object sender, PrismGuestEventArgs e)
        {
            if (e.Guest.User != null)
                UpdateConnLog("User Disconnected: " + e.Guest.User.UserName);
            UpdateConnLog("Guest Disconnected: " + e.Guest.IPAddress);

            lock (_guestDisconnect)
            {
                _guestDisconnect.Enqueue(e.Guest);
            }
        }

        //Add an item to the connection log
        protected void UpdateConnLog(string s)
        {
            lock (_connLogQ)
            {
                _connLogQ.Enqueue(DateTime.Now + ": " + s);
            }
        }

        //A guest was disconnected because they are already logged in with the same IP
        private void prism_GuestDisconnectedSameIP(object sender, PrismGuestEventArgs e)
        {
            UpdateConnLog("Guest Disconnected duplicate IP: " + e.Guest.IPAddress);
            lock (_guestDisconnect)
            {
                _guestDisconnect.Enqueue(e.Guest);
            }
        }

        //A guest has logged in
        private void prism_GuestLoggedIn(object sender, PrismGuestEventArgs e)
        {
            UpdateConnLog("User Logged in: " + e.Guest.User.UserName);
            lock (_guestLogin)
            {
                _guestLogin.Enqueue(e.Guest);
            }
        }

        //Display/change PrismServer settings - persist changes to config file
        private void btnSettings_Click(object sender, EventArgs e)
        {
            SettingsForm frmSettings = new SettingsForm();
            frmSettings.numPort.Value = prism.Port;
            frmSettings.cbProhibitMultipleConnections.Checked = prism.ProhibitSameIP;
            frmSettings.cbProhibitSameNameLogins.Checked = prism.ProhibitSameUserName;
            if (frmSettings.ShowDialog() == DialogResult.OK)
            {
                prism.Port = (int)frmSettings.numPort.Value;
                prism.ProhibitSameIP = frmSettings.cbProhibitMultipleConnections.Checked;
                prism.ProhibitSameUserName = frmSettings.cbProhibitSameNameLogins.Checked;
                prism.Implementation.SaveSettings();
            }
        }

        //Load server settings on form load
        private void PrismServerForm_Load(object sender, EventArgs e)
        {
            prism.Implementation.LoadSettings();
            //Click "Entire Server" context node
            treePrism.SelectedNode = treePrism.Nodes[0];
            
            //Start the chart service at startup
            btnStart_Click(sender, e);
            
            //Hide chart application
            //This application will be closed by the ASM2 application
            this.Hide();
        }

        //A room was added - notify UI it needs to update tree
        private void prism_RoomAdded(object sender, PrismRoomEventArgs e)
        {
            UpdateConnLog("Room Added: " + e.Room.SubjectName + ":" + e.Room.RoomName);
            lock (_roomsAdded)
            {
                _roomsAdded.Enqueue(e.Room);
            }
        }

        //A room was removed - notify UI it needs to update tree
        private void prism_RoomRemoved(object sender, PrismRoomEventArgs e)
        {
            UpdateConnLog("Room Removed: " + e.Room.SubjectName + ":" + e.Room.RoomName);
            lock (_roomsRemoved)
            {
                _roomsRemoved.Enqueue(e.Room);
            }
        }

        //Is the specified guest in the selected treeview context?
        protected bool IsGuestInSelectedContext(PrismGuest guest)
        {
            TreeNode node = treePrism.SelectedNode;
            if (node.ImageIndex == ENTIRE_SERVER)
                return true;
            else if (node.ImageIndex == STAGING_AREA)
                return (guest.User == null);
            else if (node.Level == 0)
                return (guest.SubjectName == node.Text);
            else 
            {
                if (guest.Room != null)
                    return (guest.Room.RoomName == node.Text);
                else
                    return false;
            }
        }

        //Add the specified guest to the list view
        private void AddGuestToListView(PrismGuest guest)
        {
            ListViewItem li = lvGuests.Items.Add(guest.IPAddress.ToString());
            if (guest.User != null)
            {
                imlistPrism.Images.Add(guest.User.Glyph);
                li.ImageIndex = imlistPrism.Images.Count - 1;
                li.SubItems.Add(guest.User.UserName);
                int LatencyMillis = (int)guest.Latency.TotalMilliseconds;
                li.SubItems.Add(LatencyMillis.ToString());
                li.SubItems.Add(guest.User.LoginCount.ToString());
            }
            else
            {
                li.SubItems.Add("");
                li.SubItems.Add("");
            }            
        }

        //Double clicked on a guest - show user info
        private void lvGuests_DoubleClick(object sender, EventArgs e)
        {
            if (lvGuests.SelectedItems.Count > 0)
            {
                ListViewItem li = lvGuests.SelectedItems[0];
                PrismGuest guest = prism.FindGuest(li.SubItems[1].Text);
                if (guest != null)
                    if (guest.User != null)
                        dlgUserInfo.ShowDialog(guest.User);
            }
        }

        //Clicked a node, update guest list
        private void treePrism_AfterSelect(object sender, TreeViewEventArgs e)
        {
            RefreshGuests();
        }

        //Also update it on MouseUp to catch clicks on same node
        private void treePrism_MouseUp(object sender, MouseEventArgs e)
        {
            RefreshGuests();
        }

        //Clear and refresh the guest list view
        protected void RefreshGuests()
        {
            //Clear list view
            lvGuests.Items.Clear();
            //Clear image list

            while (imlistPrism.Images.Count > 4)
                imlistPrism.Images.RemoveAt(4);

            //Add guests in the selected context
            lock (prism.Guests)
            {
                foreach (PrismGuest guest in prism.Guests)
                {
                    if (IsGuestInSelectedContext(guest))
                        AddGuestToListView(guest);
                }
            }
        }

        //Chat received, add to chat log queue
        private void prism_GuestChat(object sender, PrismGuestMsgEventArgs e)
        {
            if (IsGuestInSelectedContext(e.Guest))
            {
                lock (_chat)
                {
                    string s = e.Guest.User.UserName + ": " + e.Msg;
                    _chat.Enqueue(s);
                }
            }
        }

        //Clear Logs
        private void btnClearLogs_Click(object sender, EventArgs e)
        {
            txtConnLog.Clear();
            txtChatLog.Clear();
        }

        //Display server stats
        private void btnStats_Click(object sender, EventArgs e)
        {
            dlgStats.ShowDialog(prism.GetServerStats());
        }

        //Send admin message
        private void btnAdminMsg_Click(object sender, EventArgs e)
        {
            AdminMsgForm frmAdmin = new AdminMsgForm();
            if (frmAdmin.ShowDialog() == DialogResult.OK)
            {
                //If ListView is focused, send it to the selected Guest only
                if (lvGuests.Focused)
                {
                    if (lvGuests.SelectedItems.Count == 1)
                    {
                        ListViewItem li = lvGuests.SelectedItems[0];
                        PrismGuest g = prism.FindGuest(li.SubItems[1].Text);
                        if (g != null)
                            g.SendAdminMessage(frmAdmin.txtAdminMsg.Text);
                    }
                }
                //Send it to selected Server Context
                else
                {
                    bool ShouldSend;
                    TreeNode tn = treePrism.SelectedNode;
                    lock (prism.Guests)
                    {
                        foreach (PrismGuest g in prism.Guests)
                        {
                            if (tn.ImageIndex == ENTIRE_SERVER)
                                ShouldSend = true;
                            else if (tn.ImageIndex == SUBJECT)
                                ShouldSend = (g.SubjectName == tn.Text);
                            else if (tn.ImageIndex == STAGING_AREA)
                                ShouldSend = (g.Room == null);
                            else
                            {
                                if (g.Room != null)
                                    ShouldSend = (g.SubjectName == tn.Parent.Text && g.Room.RoomName == tn.Text);
                                else
                                    ShouldSend = false;
                            }
                            if (ShouldSend)
                                g.SendAdminMessage(frmAdmin.txtAdminMsg.Text);
                        }
                    }
                }
            }
        }

        //Update UI elements based on actions that have ocurred, in thread-safe way
        private void timerUI_Tick(object sender, EventArgs e)
        {
            //Status bar
            statusGuests.Text = Pluralize(prism.Guests.Count, "Connected Guest");            

            //This queue contains strings that need to be added to the connection log
            lock (_connLogQ)
            {
                while (_connLogQ.Count > 0)
                {
                    string s = _connLogQ.Dequeue();
                    //Add login to connection log
                    txtConnLog.AppendText(s);
                    txtConnLog.AppendText(Environment.NewLine);
                }
            }

            //This queue contains guests that have just logged in
            lock (_guestLogin)
            {
                while (_guestLogin.Count > 0)
                {
                    PrismGuest g = _guestLogin.Dequeue();
                    if (IsGuestInSelectedContext(g))
                        AddGuestToListView(g);
                }
            }

            //This queue contains guests who have disconnected - remove them from list
            lock (_guestDisconnect)
            {
                while (_guestDisconnect.Count > 0)
                {
                    PrismGuest g = _guestDisconnect.Dequeue();
                    //First try to find based on user name, if user assigned
                    if (g.User != null)
                    {
                        foreach (ListViewItem li in lvGuests.Items)
                            if (li.SubItems[1].Text == g.User.UserName)
                            {
                                lvGuests.Items.Remove(li);
                                break;
                            }
                    }
                    //next try to match on IP address
                    else
                    {
                        foreach (ListViewItem li in lvGuests.Items)
                            if (li.Text == g.IPAddress.ToString())
                            {
                                lvGuests.Items.Remove(li);
                                break;
                            }
                    }
                }
            }

            //Process rooms that need to be added to interface
            lock (_roomsAdded)
            {
                while (_roomsAdded.Count > 0)
                {
                    PrismRoom room = _roomsAdded.Dequeue();
                    TreeNode node = FindNode(room.SubjectName, 0, treePrism.Nodes);
                    if (node == null)
                    {
                        node = treePrism.Nodes.Add(room.SubjectName);
                        node.ImageIndex = SUBJECT;
                        node.SelectedImageIndex = SUBJECT;
                    }
                    node = node.Nodes.Add(room.RoomName);
                    node.ImageIndex = ROOM;
                    node.SelectedImageIndex = ROOM;
                }
            }

            //Process rooms that need to be removed from the interface
            lock (_roomsRemoved)
            {
                while (_roomsRemoved.Count > 0)
                {
                    PrismRoom room = _roomsRemoved.Dequeue();
                    TreeNode node = FindNode(room.SubjectName, 0, treePrism.Nodes);
                    if (node != null)
                    {
                        node = FindNode(room.RoomName, 1, node.Nodes);
                        if (node != null)
                            treePrism.Nodes.Remove(node);
                    }
                }
            }

            //Process lines of chat
            lock (_chat)
            {
                while (_chat.Count > 0)
                {
                    txtChatLog.AppendText(_chat.Dequeue());
                    txtChatLog.AppendText(Environment.NewLine);
                }
            }
        }

    }
}