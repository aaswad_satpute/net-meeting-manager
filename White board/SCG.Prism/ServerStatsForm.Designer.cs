namespace SCG.Prism
{
    partial class ServerStatsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpRunningTime = new System.Windows.Forms.GroupBox();
            this.lblRunSpan = new System.Windows.Forms.Label();
            this.lblRunning = new System.Windows.Forms.Label();
            this.lblWhenActivated = new System.Windows.Forms.Label();
            this.lblActivated = new System.Windows.Forms.Label();
            this.grpGuests = new System.Windows.Forms.GroupBox();
            this.lblPeakGuests = new System.Windows.Forms.Label();
            this.lblTotalGuests = new System.Windows.Forms.Label();
            this.lblCurrentGuests = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grpRooms = new System.Windows.Forms.GroupBox();
            this.lblPeakRooms = new System.Windows.Forms.Label();
            this.lblTotalRooms = new System.Windows.Forms.Label();
            this.lblCurrentRooms = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblBytesWritten = new System.Windows.Forms.Label();
            this.lblBytesRead = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.grpRunningTime.SuspendLayout();
            this.grpGuests.SuspendLayout();
            this.grpRooms.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpRunningTime
            // 
            this.grpRunningTime.Controls.Add(this.lblRunSpan);
            this.grpRunningTime.Controls.Add(this.lblRunning);
            this.grpRunningTime.Controls.Add(this.lblWhenActivated);
            this.grpRunningTime.Controls.Add(this.lblActivated);
            this.grpRunningTime.Location = new System.Drawing.Point(8, 8);
            this.grpRunningTime.Name = "grpRunningTime";
            this.grpRunningTime.Size = new System.Drawing.Size(192, 80);
            this.grpRunningTime.TabIndex = 0;
            this.grpRunningTime.TabStop = false;
            this.grpRunningTime.Text = "Running Time";
            // 
            // lblRunSpan
            // 
            this.lblRunSpan.AutoSize = true;
            this.lblRunSpan.ForeColor = System.Drawing.Color.Navy;
            this.lblRunSpan.Location = new System.Drawing.Point(16, 56);
            this.lblRunSpan.Name = "lblRunSpan";
            this.lblRunSpan.Size = new System.Drawing.Size(32, 13);
            this.lblRunSpan.TabIndex = 3;
            this.lblRunSpan.Text = "0 min";
            // 
            // lblRunning
            // 
            this.lblRunning.AutoSize = true;
            this.lblRunning.Location = new System.Drawing.Point(16, 40);
            this.lblRunning.Name = "lblRunning";
            this.lblRunning.Size = new System.Drawing.Size(146, 13);
            this.lblRunning.TabIndex = 2;
            this.lblRunning.Text = "Server has been Running for:";
            // 
            // lblWhenActivated
            // 
            this.lblWhenActivated.ForeColor = System.Drawing.Color.Navy;
            this.lblWhenActivated.Location = new System.Drawing.Point(80, 24);
            this.lblWhenActivated.Name = "lblWhenActivated";
            this.lblWhenActivated.Size = new System.Drawing.Size(104, 16);
            this.lblWhenActivated.TabIndex = 1;
            this.lblWhenActivated.Text = "1/1/2001";
            this.lblWhenActivated.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblActivated
            // 
            this.lblActivated.AutoSize = true;
            this.lblActivated.Location = new System.Drawing.Point(16, 24);
            this.lblActivated.Name = "lblActivated";
            this.lblActivated.Size = new System.Drawing.Size(55, 13);
            this.lblActivated.TabIndex = 0;
            this.lblActivated.Text = "Activated:";
            // 
            // grpGuests
            // 
            this.grpGuests.Controls.Add(this.lblPeakGuests);
            this.grpGuests.Controls.Add(this.lblTotalGuests);
            this.grpGuests.Controls.Add(this.lblCurrentGuests);
            this.grpGuests.Controls.Add(this.label3);
            this.grpGuests.Controls.Add(this.label2);
            this.grpGuests.Controls.Add(this.label1);
            this.grpGuests.Location = new System.Drawing.Point(8, 88);
            this.grpGuests.Name = "grpGuests";
            this.grpGuests.Size = new System.Drawing.Size(192, 80);
            this.grpGuests.TabIndex = 2;
            this.grpGuests.TabStop = false;
            this.grpGuests.Text = "Guests Connected";
            // 
            // lblPeakGuests
            // 
            this.lblPeakGuests.ForeColor = System.Drawing.Color.Navy;
            this.lblPeakGuests.Location = new System.Drawing.Point(128, 56);
            this.lblPeakGuests.Name = "lblPeakGuests";
            this.lblPeakGuests.Size = new System.Drawing.Size(53, 13);
            this.lblPeakGuests.TabIndex = 5;
            this.lblPeakGuests.Text = "0";
            this.lblPeakGuests.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTotalGuests
            // 
            this.lblTotalGuests.ForeColor = System.Drawing.Color.Navy;
            this.lblTotalGuests.Location = new System.Drawing.Point(128, 40);
            this.lblTotalGuests.Name = "lblTotalGuests";
            this.lblTotalGuests.Size = new System.Drawing.Size(53, 13);
            this.lblTotalGuests.TabIndex = 4;
            this.lblTotalGuests.Text = "0";
            this.lblTotalGuests.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCurrentGuests
            // 
            this.lblCurrentGuests.ForeColor = System.Drawing.Color.Navy;
            this.lblCurrentGuests.Location = new System.Drawing.Point(128, 24);
            this.lblCurrentGuests.Name = "lblCurrentGuests";
            this.lblCurrentGuests.Size = new System.Drawing.Size(53, 13);
            this.lblCurrentGuests.TabIndex = 3;
            this.lblCurrentGuests.Text = "0";
            this.lblCurrentGuests.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Peak Number:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Total Number:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Currently:";
            // 
            // grpRooms
            // 
            this.grpRooms.Controls.Add(this.lblPeakRooms);
            this.grpRooms.Controls.Add(this.lblTotalRooms);
            this.grpRooms.Controls.Add(this.lblCurrentRooms);
            this.grpRooms.Controls.Add(this.label9);
            this.grpRooms.Controls.Add(this.label10);
            this.grpRooms.Controls.Add(this.label11);
            this.grpRooms.Location = new System.Drawing.Point(208, 88);
            this.grpRooms.Name = "grpRooms";
            this.grpRooms.Size = new System.Drawing.Size(192, 80);
            this.grpRooms.TabIndex = 3;
            this.grpRooms.TabStop = false;
            this.grpRooms.Text = "Rooms Created";
            // 
            // lblPeakRooms
            // 
            this.lblPeakRooms.ForeColor = System.Drawing.Color.Navy;
            this.lblPeakRooms.Location = new System.Drawing.Point(128, 56);
            this.lblPeakRooms.Name = "lblPeakRooms";
            this.lblPeakRooms.Size = new System.Drawing.Size(53, 13);
            this.lblPeakRooms.TabIndex = 5;
            this.lblPeakRooms.Text = "0";
            this.lblPeakRooms.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTotalRooms
            // 
            this.lblTotalRooms.ForeColor = System.Drawing.Color.Navy;
            this.lblTotalRooms.Location = new System.Drawing.Point(128, 40);
            this.lblTotalRooms.Name = "lblTotalRooms";
            this.lblTotalRooms.Size = new System.Drawing.Size(53, 13);
            this.lblTotalRooms.TabIndex = 4;
            this.lblTotalRooms.Text = "0";
            this.lblTotalRooms.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCurrentRooms
            // 
            this.lblCurrentRooms.ForeColor = System.Drawing.Color.Navy;
            this.lblCurrentRooms.Location = new System.Drawing.Point(128, 24);
            this.lblCurrentRooms.Name = "lblCurrentRooms";
            this.lblCurrentRooms.Size = new System.Drawing.Size(53, 13);
            this.lblCurrentRooms.TabIndex = 3;
            this.lblCurrentRooms.Text = "0";
            this.lblCurrentRooms.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Peak Number:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Total Number:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Currently:";
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(320, 176);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblBytesWritten);
            this.groupBox1.Controls.Add(this.lblBytesRead);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(208, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(192, 80);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Bandwidth";
            // 
            // lblBytesWritten
            // 
            this.lblBytesWritten.ForeColor = System.Drawing.Color.Navy;
            this.lblBytesWritten.Location = new System.Drawing.Point(80, 40);
            this.lblBytesWritten.Name = "lblBytesWritten";
            this.lblBytesWritten.Size = new System.Drawing.Size(99, 13);
            this.lblBytesWritten.TabIndex = 8;
            this.lblBytesWritten.Text = "0";
            this.lblBytesWritten.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblBytesRead
            // 
            this.lblBytesRead.ForeColor = System.Drawing.Color.Navy;
            this.lblBytesRead.Location = new System.Drawing.Point(80, 24);
            this.lblBytesRead.Name = "lblBytesRead";
            this.lblBytesRead.Size = new System.Drawing.Size(99, 13);
            this.lblBytesRead.TabIndex = 7;
            this.lblBytesRead.Text = "0";
            this.lblBytesRead.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Bytes Written:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Bytes Read:";
            // 
            // ServerStatsForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 204);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.grpRooms);
            this.Controls.Add(this.grpGuests);
            this.Controls.Add(this.grpRunningTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ServerStatsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "PrismServer Statistics";
            this.grpRunningTime.ResumeLayout(false);
            this.grpRunningTime.PerformLayout();
            this.grpGuests.ResumeLayout(false);
            this.grpGuests.PerformLayout();
            this.grpRooms.ResumeLayout(false);
            this.grpRooms.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpRunningTime;
        private System.Windows.Forms.Label lblWhenActivated;
        private System.Windows.Forms.Label lblActivated;
        private System.Windows.Forms.Label lblRunSpan;
        private System.Windows.Forms.Label lblRunning;
        private System.Windows.Forms.GroupBox grpGuests;
        private System.Windows.Forms.Label lblPeakGuests;
        private System.Windows.Forms.Label lblTotalGuests;
        private System.Windows.Forms.Label lblCurrentGuests;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpRooms;
        private System.Windows.Forms.Label lblPeakRooms;
        private System.Windows.Forms.Label lblTotalRooms;
        private System.Windows.Forms.Label lblCurrentRooms;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblBytesWritten;
        private System.Windows.Forms.Label lblBytesRead;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}