#region copyright
/*
* Copyright (c) 2007, Dion Kurczek
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DION KURCZEK ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DION KURCZEK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace SCG.Prism
{
    [ToolboxBitmap(typeof(PrismUserInfoDialog), "PrismUserInfoDialog")]
    public partial class PrismUserInfoDialog : Component
    {
        //Private members
        private UserInfoForm _frmUserInfo = new UserInfoForm();
        private PrismUserInfoDialogState _state;
        private string _title = "User Info";

        //Constructors
        public PrismUserInfoDialog()
        {
            InitializeComponent();
        }
        public PrismUserInfoDialog(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        //Access dialog state
        public PrismUserInfoDialogState EditState
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
            }
        }

        //Dialog title
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }

        //Show the dialog       
        public DialogResult ShowDialog(PrismUser user)
        {
            //Assign fields to form
            _frmUserInfo.Text = Title;
            _frmUserInfo.PopulateForm(user);
            //Set editign mode            
            _frmUserInfo.UserEditable = (_state != PrismUserInfoDialogState.DisplayOnly);
            _frmUserInfo.NewUserEditable = (_state == PrismUserInfoDialogState.NewUser);
            //Show the dialog
            DialogResult dr = _frmUserInfo.ShowDialog();
            //Store the info back in the user object
            _frmUserInfo.PopulateUserInfo(user);
            return dr;
        }
    }

    //Controls the editable state of the user info dialog
    public enum PrismUserInfoDialogState
    {
        DisplayOnly, EditUser, NewUser
    }
}
