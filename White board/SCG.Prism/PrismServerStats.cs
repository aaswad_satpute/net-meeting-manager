using System;
using System.Collections.Generic;
using System.Text;

namespace SCG.Prism
{
    //Represents server statistics
    public class PrismServerStats
    {
        //Private members
        private DateTime _startTime;
        private int _bytesRead;
        private int _bytesWritten;
        private int _connections;
        private int _totalConnections;
        private int _peakConnections;
        private int _rooms;
        private int _totalRooms;
        private int _peakRooms;

        //Constructor
        public PrismServerStats(DateTime startTime, int bytesRead, int bytesWritten,
            int connections, int totalConnections, int peakConnections, int rooms,
            int totalRooms, int peakRooms)
        {
            _startTime = startTime;
            _bytesRead = bytesRead;
            _bytesWritten = bytesWritten;
            _connections = connections;
            _totalConnections = totalConnections;
            _peakConnections = peakConnections;
            _rooms = rooms;
            _totalRooms = totalRooms;
            _peakRooms = peakRooms;
        }
        public PrismServerStats(ref string s)
        {
            _startTime = Tokenizer.GetTokenDateTime(ref s);
            _connections = Tokenizer.GetTokenInt(ref s);
            _bytesRead = Tokenizer.GetTokenInt(ref s);
            _bytesWritten = Tokenizer.GetTokenInt(ref s);            
            _totalConnections = Tokenizer.GetTokenInt(ref s);
            _peakConnections = Tokenizer.GetTokenInt(ref s);
            _rooms = Tokenizer.GetTokenInt(ref s);
            _totalRooms = Tokenizer.GetTokenInt(ref s);
            _peakRooms = Tokenizer.GetTokenInt(ref s);
        }

        //Public members
        public DateTime StartTime
        {
            get
            {
                return _startTime;
            }
        }
        public int BytesRead
        {
            get
            {
                return _bytesRead;
            }
        }
        public int BytesWritten
        {
            get
            {
                return _bytesWritten;
            }
        }
        public int Connections
        {
            get
            {
                return _connections;
            }
        }
        public int TotalConnections
        {
            get
            {
                return _totalConnections;
            }
        }
        public int PeakConnections
        {
            get
            {
                return _peakConnections;
            }
        }
        public int Rooms
        {
            get
            {
                return _rooms;
            }
        }
        public int TotalRooms
        {
            get
            {
                return _totalRooms;
            }
        }
        public int PeakRooms
        {
            get
            {
                return _peakRooms;
            }
        }
        
        //Public methods
        public override string ToString()
        {
            return Tokenizer.Tokenize(_startTime, _connections, _bytesRead, _bytesWritten,
                _totalConnections, _peakConnections, _rooms, _totalRooms, _peakRooms);
        }
    }

    //Server Stats events arg
    public class PrismServerStatsEventArgs : EventArgs
    {
        //Private members
        private PrismServerStats _stats;

        //Constructor
        public PrismServerStatsEventArgs(PrismServerStats stats)
        {
            _stats = stats;
        }

        //Public peoperties
        public PrismServerStats ServerStats
        {
            get
            {
                return _stats;
            }
        }
    }
}
