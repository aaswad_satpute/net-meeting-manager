#region copyright
/*
* Copyright (c) 2007, Dion Kurczek
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DION KURCZEK ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DION KURCZEK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Drawing;
using System.IO;

namespace SCG.Prism
{
    //PrismServerFileImplementation uses a simple file-based database to manage PrismUsers.
    [ToolboxBitmap(typeof(PrismServerFileImplementation), "PrismServerFileImplementation")]
    public partial class PrismServerFileImplementation : PrismServerImplementation
    {
        //Protected members
        protected bool _initialized;
        protected string _userDir = Directory.GetCurrentDirectory() + "\\Users";
        protected string _settingsFile;

        //Constructors
        public PrismServerFileImplementation()
        {
            InitializeComponent();
        }
        public PrismServerFileImplementation(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        //Overridden methods from PrismServerImplementation       

        //Is the specified user name valid?  See if we can create a temp file with it
        public override bool CheckUserName(string userName, ref string msg)
        {
            if (!base.CheckUserName(userName, ref msg))
                return false;
            //If a user exists with this name it is technically valid
            if (UserExists(userName))
                return true;
            //See if we can create a file with this username
            try
            {
                string UserFile = GetUserFileName(userName);
                FileStream fs = File.Create(UserFile);
                fs.Close();
                //OK, delete the file - we don't need it
                File.Delete(UserFile);
                return true;
            }
            catch
            {
                msg = "The specified User Name contains invalid characters";
                return false;
            }
        }

        //Is the specified password valid?
        public override bool IsPasswordValid(string userName, string password)
        {           
            PrismUser TempUser = new PrismUser();
            TempUser.UserName = userName;
            PopulateUserInfo(TempUser);
            return (password == TempUser.Password);
        }

        //Load user information from a .pru file
        public override void PopulateUserInfo(PrismUser user)
        {           
            string UserFile = GetUserFileName(user.UserName);
            if (File.Exists(UserFile))
            {
                string UserString = File.ReadAllText(UserFile);
                user.FromString(ref UserString);
            }
        }

        //Store the user information to a .pru file
        public override void StoreUserInfo(PrismUser user)
        {          
            string UserString = user.ToString();
            File.WriteAllText(GetUserFileName(user.UserName), UserString);
        }

        //Does the specified user exist?
        public override bool UserExists(string userName)
        {           
            return File.Exists(GetUserFileName(userName));
        }

        //Protected helper functions

        //Load all of the locally stored PrismUsers from their files
        protected override void Initialize()
        {
            if (!_initialized)
            {
                _settingsFile = _userDir + "\\Settings.txt";
                _initialized = true;
                //If the Users directory does not exist, create it
                if (!Directory.Exists(_userDir))
                    Directory.CreateDirectory(_userDir);
            }
        }

        //Return the file name for a specified user name
        protected string GetUserFileName(string userName)
        {
            return _userDir + "\\" + userName + ".pru";
        }

        //Load server settings
        public override void LoadSettings()
        {           
            if (File.Exists(_settingsFile))
            {
                string s = File.ReadAllText(_settingsFile);
                Server.Port = Tokenizer.GetTokenInt(ref s);
                Server.ProhibitSameIP = Tokenizer.GetTokenBool(ref s);
                Server.ProhibitSameUserName = Tokenizer.GetTokenBool(ref s);
            }
        }

        //Save server settings
        public override void SaveSettings()
        {           
            string s = Tokenizer.Tokenize(Server.Port, Server.ProhibitSameIP, Server.ProhibitSameUserName);
            File.WriteAllText(_settingsFile, s);
        }

    }
}
