#region copyright
/*
* Copyright (c) 2007, Dion Kurczek
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DION KURCZEK ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DION KURCZEK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace SCG.Prism
{    
    //PrismConnection component allows client applications to connect to and communicate with a PrismServer
    [ToolboxBitmap(typeof(PrismConnection),"PrismConnection")]
    public partial class PrismConnection : Component
    {
        //Event Handlers 
        public event EventHandler<EventArgs> Disconnected;      
        public event EventHandler<EventArgs> LoginOK;
        public event EventHandler<MessageEventArgs> LoginError;
        public event EventHandler<MessageEventArgs> PrismError;
        public event EventHandler<PrismUserEventArgs> UserAddedToRoom;
        public event EventHandler<PrismUserEventArgs> UserLeftRoom;
        public event EventHandler<RoomNameEventArgs> JoinedRoom;
        public event EventHandler<EventArgs> StartSignal;
        public event EventHandler<RoomNameEventArgs> RoomRemoved;
        public event EventHandler<RoomNameEventArgs> RoomAdded;
        public event EventHandler<PrismUserMessageEventArgs> ChatMessageReceived;
        public event EventHandler<PrismUserMessageEventArgs> DataMessageReceived;
        public event EventHandler<PrismUserEventArgs> UserInfoChanged;
        public event EventHandler<PrismServerStatsEventArgs> ServerStatsReceived;
        public event EventHandler<MessageEventArgs> AdminMessageReceived;
        public event EventHandler<CustomCommandEventArgs> CustomCommandReceived;
        public event EventHandler<PrismUserEventArgs> UserLatencyUpdated;
        public event EventHandler<RoomCountEventArgs> RoomCountChanged;
    
        //Constructors
        public PrismConnection()
        {
            InitializeComponent();
        }
        public PrismConnection(IContainer container)
        {
            container.Add(this);
            InitializeComponent();            
        }

        //Active property - Set to true to attempt to connect to specified PrismServer
        //Will throw a SocketException if unable to connect!
        [Browsable(false)]
        public bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                if (_active != value)
                {                    
                    //Setting Active to true - make connection
                    if (value)
                    {
                        _clientSocket = new TcpClient();
                        _clientSocket.Connect(Host, Port);

                        //Only now are we "officially" connected
                        _active = true;

                        //Create a new PrismNetworkStream to process this connection
                        _stream = new PrismNetworkStream(_clientSocket);

                        //Create a new reader thread
                        _readerThread = new Thread(ExecuteReaderThread);
                        _readerThread.IsBackground = true;
                        _readerThread.Start();
                    }
                    //Setting Active to false - shut down connection
                    else
                    {
                        try
                        {
                            _stream.Close();
                        }
                        catch
                        {
                        }

                        //Update Active property with false value
                        _active = value;
                        _loggedIn = false;

                        //Trigger Disconnected event to client app
                        if (Disconnected != null)
                            Disconnected(this, new EventArgs());
                    }                    
                }
            }
        }

        //Host property - Specifies the host name of the PrismServer being connected to
        public string Host
        {
            get
            {
                return _host;
            }
            set
            {
                //Only allow change to Host property if not Active
                if (!Active)
                    _host = value;
            }
        }

        //Port property - Specified the port number the component should connect on
        public int Port
        {
            get
            {
                return _port;
            }
            set
            {
                //Only allow change to Port if not activated
                if (!Active)
                    _port = value;
            }
        }

        //SubjectName property - specified the application name context this client is connecting under
        public string SubjectName
        {
            get
            {
                return _subjectName;
            }
            set
            {
                //Only allow change to SubjectName if not active
                if (!Active)
                    _subjectName = value;
            }
        }                      

        //Access the PrismUser that represents the user who is connected
        [Browsable(false)]
        public PrismUser ThisUser
        {
            get
            {
                return _user;
            }
            internal set
            {
                _user = value;
            }
        }

        //Is the connection "Logged In" to a valid guest account?
        [Browsable(false)]
        public bool LoggedIn
        {
            get
            {
                return _loggedIn;
            }
        }

        //Access the current room name
        [Browsable(false)]
        public string RoomName
        {
            get
            {
                return _roomName;
            }
        }

        //Access current list of users in the current room
        [Browsable(false)]
        public List<PrismUser> Users
        {
            get
            {
                return _userList;
            }
        }

        //Find a user with the specified name
        public PrismUser FindUser(string userName)
        {
            lock (_userList)
            {
                foreach (PrismUser user in Users)
                    if (user.UserName == userName)
                        return user;
            }
            return null;
        }

        //Protocol methods follow

        //Login with existing credentials
        public void Login(string userName, string password)
        {
            WriteTokens( "LOGIN", userName, password, SubjectName );
        }

        //Login as a new user - PrismUser object must be populated
        public void LoginNew(PrismUser newUser)
        {
            WriteTokens("LOGINNEW", newUser.ToString(), SubjectName);
        }

        //Attempt to enter an existing room
        public void EnterRoom(string roomName)
        {
            WriteTokens("JOINROOM", roomName);
        }

        //Attempt to create a new room
        public void CreateRoom(string roomName, int maxUsers)
        {
            WriteTokens("CREATEROOM", roomName, maxUsers);
        }

        //Send a chat message
        public void SendChat(string chatText)
        {
            WriteTokens("CHAT", chatText);
        }

        //Send a data message
        public void SendData(string dataText)
        {
            WriteTokens("DATA", dataText);
        }

        //Request to save the specified user info changes to the server
        public void SaveUserInfo(PrismUser user)
        {
            WriteTokens("SAVEUSER", user.ToString());
        }

        //Request Server statistics
        public void ServerStats()
        {
            WriteTokens("SERVERSTATS");
        }

        //Send Custom command to the server
        public void CustomCommand(string CommandName, string Params)
        {
            WriteTokens("CUSTOM", CommandName, Params);
        }

        //Private members
        private bool _active;
        private string _host;
        private int _port;
        private PrismNetworkStream _stream;
        private TcpClient _clientSocket;
        private Thread _readerThread;
        private string _subjectName;
        private PrismUser _user;
        private bool _loggedIn;
        private List<PrismUser> _userList = new List<PrismUser>();
        private string _roomName;

        //Write method - write tokens to underlying stream
        private void WriteTokens(params object[] tokens)
        {
            if (Active)
            {
                //Attempt to read from underlying NetworkStream
                try
                {
                    _stream.WriteTokens(tokens);
                }

                //Disconnect socket on an error
                catch
                {
                    Active = false;
                }
            }
        }

        //Reader thread execution method
        private void ExecuteReaderThread()
        {
            List<string> tokenList = new List<string>();
            string command;
            string s;
            PrismUser user;

            //Keep thread going as long as PrismConnection is Active
            while (Active)
            {
                //Read a command from the PrismServer - disconnect on error
                try
                {
                    command = _stream.ReadTokens(tokenList);
                }
                catch
                {
                    Active = false;
                    break;
                }

                //Process Known PrismProtocol commands
                switch (command)
                {
                    //Return a "PING" to the server - heartbeat keeps connection alive
                    case "PING":
                        WriteTokens("PING");
                        break;

                    //Login Error
                    case "LOGINERROR":
                        //Trigger the LoginError event to client app
                        if (LoginError != null)
                            LoginError(this, new MessageEventArgs(tokenList[0]));
                        break;

                    //Login OK message - a UserString object is returned
                    case "LOGINOK":

                        //Create a PrismUser object
                        _user = new PrismUser();

                        //Populate data from User String
                        string Token = tokenList[0];
                        _user.FromString(ref Token);

                        //Record logged in state locally
                        _loggedIn = true;

                        //Trigger the Login OK event to client app
                        if (LoginOK != null)
                            LoginOK(this, new EventArgs());

                        //All defined rooms are also passed - add events for these
                        if (RoomAdded != null)
                        {
                            string Rooms = tokenList[1];
                            while (Rooms != "")
                            {
                                s = Tokenizer.GetToken(ref Rooms);
                                RoomAdded(this, new RoomNameEventArgs(s));
                            }
                        }
                        break;

                    //General Error message
                    case "ERROR":
                        if (PrismError != null)
                            PrismError(this, new MessageEventArgs(tokenList[0]));
                        break;

                    //User has joined a room
                    case "JOINROOM":

                        //First delete the existing guest list maintained in current room
                        _userList.Clear();

                        //Assign room name to local property
                        _roomName = tokenList[0];

                        //Fire an event telling client room name has changed
                        if (JoinedRoom != null)
                            JoinedRoom(this, new RoomNameEventArgs(_roomName));

                        //Parse each user object from descriptor strings
                        string UserString = tokenList[1];
                        while (UserString != "")
                        {
                            s = Tokenizer.GetToken(ref UserString);
                            user = new PrismUser();
                            user.FromString(ref s);

                            //If this is the "current" user, use the same instance
                            if (user.UserName == ThisUser.UserName)
                            {
                                s = user.ToString();
                                ThisUser.FromString(ref s);
                                user = ThisUser;
                            }
                            _userList.Add(user);

                            //Fire an event letting client know guest added to current room
                            if (UserAddedToRoom != null)
                                UserAddedToRoom(this, new PrismUserEventArgs(user));
                        }
                        break;

                    //A new user has entered the current room
                    case "ENTERROOM":

                        //Create a PrismUser object for the user who entered
                        s = tokenList[0];
                        user = new PrismUser();
                        user.FromString(ref s);

                        //Add the new user to the user list
                        _userList.Add(user);

                        //Trigger an event
                        if (UserAddedToRoom != null)
                            UserAddedToRoom(this, new PrismUserEventArgs(user));
                        break;

                    //A user has left the current room
                    case "LEAVEROOM":

                        //Locate the user with the specified user name
                        user = FindUser(tokenList[0]);
                        if (user != null)
                        {
                            //Trigger event to client that user will be removed
                            if (UserLeftRoom != null)
                                UserLeftRoom(this, new PrismUserEventArgs(user));

                            //Remove them from the room
                            _userList.Remove(user);
                        }
                        break;

                    //Room count has changed for specified room
                    case "ROOMCOUNTCHANGE":
                        if (RoomCountChanged != null)
                        {
                            string roomName = tokenList[0];
                            int count = Int32.Parse(tokenList[1]);
                            RoomCountChanged(this, new RoomCountEventArgs(roomName, count));
                        }
                        break;

                    //The subject has Started!  MaxGuests have entered the subject
                    case "START":
                        if (StartSignal != null)
                            StartSignal(this, new EventArgs());
                        break;

                    //A room was removed, trigger event to client
                    case "ROOMREMOVED":
                        if (RoomRemoved != null)
                            RoomRemoved(this, new RoomNameEventArgs(tokenList[0]));
                        break;

                    //A room was added, notify client of new room name
                    case "ROOMADDED":
                        if (RoomAdded != null)
                            RoomAdded(this, new RoomNameEventArgs(tokenList[0]));
                        break;

                    //A chat message has come in
                    case "CHAT":
                        user = FindUser(tokenList[0]);
                        if (user != null)
                            if (ChatMessageReceived != null)
                                ChatMessageReceived(this, new PrismUserMessageEventArgs(user, tokenList[1]));
                        break;

                    //A data message has come in
                    case "DATA":
                        user = FindUser(tokenList[0]);
                        if (user != null)
                            if (DataMessageReceived != null)
                                DataMessageReceived(this, new PrismUserMessageEventArgs(user, tokenList[1]));
                        break;

                    //User information for a user has changed
                    case "USERINFOCHANGE":
                        s = tokenList[0];
                        user = new PrismUser();
                        user.FromString(ref s);

                        //Is there a matching user?
                        PrismUser matchingUser = FindUser(user.UserName);
                        if (matchingUser != null)
                        {
                            //Yes, copy their information
                            s = tokenList[0];
                            matchingUser.FromString(ref s);

                            //And notify client via event
                            if (UserInfoChanged != null)
                                UserInfoChanged(this, new PrismUserEventArgs(matchingUser));
                        }
                        break;

                    //Server stats were received
                    case "SERVERSTATS":
                        if (ServerStatsReceived != null)
                        {
                            string StatString = tokenList[0];
                            PrismServerStats stats = new PrismServerStats(ref StatString);
                            ServerStatsReceived(this, new PrismServerStatsEventArgs(stats));
                        }
                        break;

                    //Receieved an Admin Message
                    case "ADMINMSG":
                        if (AdminMessageReceived != null)
                            AdminMessageReceived(this, new MessageEventArgs(tokenList[0]));
                        break;

                    //A custom command was received
                    case "CUSTOM":
                        if (CustomCommandReceived != null)
                            CustomCommandReceived(this, new CustomCommandEventArgs(tokenList[0], tokenList[1]));
                        break;
                    //A Latency update was received
                    case "LATENCY":
                        string userName = tokenList[0];
                        int latency = Int32.Parse(tokenList[1]);
                        lock(_userList)
                            foreach(PrismUser u in _userList)
                                if (u.UserName == userName)
                                {
                                    u.LatencyMS = latency;
                                    if (UserLatencyUpdated != null)
                                        UserLatencyUpdated(this, new PrismUserEventArgs(u));
                                    break;
                                }
                        break;
                }
                Thread.Sleep(10);
            }
        }
    }

    //Event argument classes
    public class MessageEventArgs : EventArgs
    {
        private string _msg;

        public MessageEventArgs(string msg)
        {
            _msg = msg;
        }

        public string Msg
        {
            get
            {
                return _msg;
            }
        }
    }
    public class CustomCommandEventArgs : EventArgs
    {
        private string _commandName;
        private string _params;

        public CustomCommandEventArgs(string commandString, string cmdParams)
        {          
            _commandName = commandString;
            _params = cmdParams;
        }

        public string CommandString
        {
            get
            {
                return _commandName;
            }
        }
        public string Params
        {
            get
            {
                return _params;
            }
        }
    }
    public class RoomNameEventArgs : EventArgs
    {
        private string _roomName;

        public RoomNameEventArgs(string roomName)
        {
            _roomName = roomName;
        }

        public string RoomName
        {
            get
            {
                return _roomName;
            }
        }
    }
    public class RoomCountEventArgs : RoomNameEventArgs
    {
        public RoomCountEventArgs(string roomName, int count)
            : base(roomName)
        {
            _count = count;
        }

        public int Count
        {
            get
            {
                return _count;
            }
        }

        private int _count;
    }
}
