#region copyright
/*
* Copyright (c) 2007, Dion Kurczek
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DION KURCZEK ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DION KURCZEK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SCG.Prism
{
    //Displays PrismServer statistics
    public partial class ServerStatsForm : Form
    {
        //Constructor
        public ServerStatsForm()
        {
            InitializeComponent();
        }

        //Return correct plural form of value
        public string Pluralize(int n, string s)
        {
            string Plural = n + " " + s;
            if (n != 1)
                Plural += "s";
            return Plural;
        }

        //Populate info
        public void PopulateStats(PrismServerStats stats)
        {
            lblWhenActivated.Text = stats.StartTime.ToString();
            TimeSpan tsActive = DateTime.Now - stats.StartTime;
            string s = "";
            int d = (int)tsActive.TotalDays;
            if (d > 0)
                s = Pluralize(d, "day");
            if (tsActive.Hours > 0 || s != "")
                s += " " + Pluralize(tsActive.Hours, "hr");
            if (tsActive.Minutes > 0 || s != "")
                s += " " + Pluralize(tsActive.Minutes, "min");
            s += " " + Pluralize(tsActive.Seconds, "sec");
            lblRunSpan.Text = s;
            lblBytesRead.Text = stats.BytesRead.ToString("N0");
            lblBytesWritten.Text = stats.BytesWritten.ToString("N0");
            lblCurrentGuests.Text = stats.Connections.ToString();
            lblTotalGuests.Text = stats.TotalConnections.ToString();
            lblPeakGuests.Text = stats.PeakConnections.ToString();
            lblCurrentRooms.Text = stats.Rooms.ToString();
            lblTotalRooms.Text = stats.TotalRooms.ToString();
            lblPeakRooms.Text = stats.PeakRooms.ToString();
        }
    }
}