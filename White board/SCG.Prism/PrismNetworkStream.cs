#region copyright
/*
* Copyright (c) 2007, Dion Kurczek
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DION KURCZEK ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DION KURCZEK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace SCG.Prism
{
    //PrismNetworkStream reads individual messages from a NetworkStream, and adopts the message
    //length specifier prefix when sending data and reading messages
    public class PrismNetworkStream
    {
        //Constructor
        public PrismNetworkStream(TcpClient client)
        {
            _client = client;
            _stream = client.GetStream();        
        }

        //Access number of bytes read/written during last operation
        public int BytesRead
        {
            get
            {
                return _bytesRead;
            }
        }
        public int BytesWritten
        {
            get
            {
                return _bytesWritten;
            }
        }

        //Access total number of bytes read and written
        public int TotalBytesRead
        {
            get
            {
                return _totalBytesRead;
            }
        }
        public int TotalBytesWritten
        {
            get
            {
                return _totalBytesWritten;
            }
        }

        //Close the underlying stream, and socket
        public void Close()
        {
            //Set local flag so running thread can terminate during read operations
            _closed = true;
            //Close underlying NetworkStream, and socket
            _stream.Close();
            _client.Close();
        }

        //Determine if the connection was closed explictly
        public bool WasClosed
        {
            get
            {
                return _closed;
            }
        }

        //PrismServer uses "Tokenized" input/output strings.  These strings contain a series of
        //pipe (|) delimited tokens.  Each token is composed of a length prefix, followed by the
        //pipe delimiter, follwed by the text of the token.  In this way, the pipe character itself
        //can also be safely used in messaging.
        public void WriteTokens(params object[] tokens)
        {
            string s = Tokenizer.Tokenize(tokens);
            WriteString(s);
        }

        //De-tokenize a string and return the main COMMAND string and a list of optional tokens
        public string ReadTokens(List<string> stringList)
        {
            string CommandString = "";
            stringList.Clear();
            string s = ReadString();
            if (s.Length > 0)
                CommandString = Tokenizer.GetToken(ref s);
            while (s.Length > 0)
                stringList.Add(Tokenizer.GetToken(ref s));
            return CommandString;
        }

        //Constants
        private static int BufferSize = 1024;

        //Private Members
        private NetworkStream _stream;
        private TcpClient _client;
        private int _desiredMessagelength = 0;
        private string _incomingMessage = "";
        private byte[] _buffer = new byte[BufferSize];
        private bool _closed = false;
        private int _bytesRead = 0;
        private int _bytesWritten = 0;
        private int _totalBytesRead = 0;
        private int _totalBytesWritten = 0;

        //Read a string from the NetworkStream
        private string ReadString()
        {
            string ReturnString = "";
            int Pos;
            int BytesRead;

            do
            {
                //Do we have a message length already defined?
                if (_desiredMessagelength > 0)
                {
                    //Yes, does the string contain the full message?
                    if (_incomingMessage.Length >= _desiredMessagelength)
                    {
                        //Yes, pluck out the message
                        ReturnString = _incomingMessage.Substring(0, _desiredMessagelength);
                        _incomingMessage = _incomingMessage.Substring(_desiredMessagelength, _incomingMessage.Length - _desiredMessagelength);
                        _desiredMessagelength = 0;
                    }
                    else
                    {
                        //No, need to pull more data from socket and append to incoming string
                        BytesRead = _stream.Read(_buffer, 0, BufferSize);
                        if (BytesRead == 0)
                            Thread.Sleep(10);
                        else
                            _incomingMessage = _incomingMessage + Encoding.ASCII.GetString(_buffer, 0, BytesRead);
                    }
                }
                //No, is there a length specifier in the string?
                else
                {
                    Pos = _incomingMessage.IndexOf('|');
                    if (Pos > 0)
                    {
                        //We have a length specifier, parse it out and allow loop to continue
                        _desiredMessagelength = Int32.Parse(_incomingMessage.Substring(0, Pos));
                        _incomingMessage = _incomingMessage.Substring(Pos + 1, _incomingMessage.Length - Pos - 1);                        
                    }
                    else
                    {
                        //No length specifier - time to read some data from the socket
                        BytesRead = _stream.Read(_buffer, 0, BufferSize);
                        if (BytesRead == 0)
                            Thread.Sleep(10);
                        else
                            _incomingMessage = _incomingMessage + Encoding.ASCII.GetString(_buffer, 0, BytesRead);
                    }
                }               
            } while (ReturnString == "" && _stream.CanRead && !_closed);
            _bytesRead += ReturnString.Length;
            _totalBytesRead += ReturnString.Length;
            return ReturnString;
        }

        //Write a string to the NetworkStream
        private void WriteString(string s)
        {
            if (_stream.CanWrite)
            {
                s = s.Length + "|" + s;
                byte[] outputBytes = Encoding.ASCII.GetBytes(s);
                _stream.Write(outputBytes, 0, outputBytes.Length);
                _bytesWritten += outputBytes.Length;
                _totalBytesWritten += outputBytes.Length;
            }
        }
    }
}
