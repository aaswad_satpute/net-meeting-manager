#region copyright
/*
* Copyright (c) 2007, Dion Kurczek
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DION KURCZEK ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DION KURCZEK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Drawing;

namespace SCG.Prism
{
    [ToolboxBitmap(typeof(GameCoordinator), "GameCoordinator")]
    public partial class GameCoordinator : Component
    {
        //public members

        //events
        public event EventHandler<MessageEventArgs> ProcessChanges;
        public event EventHandler<OrdersEventArgs> ProcessOrdersGetChanges;
        public event EventHandler<OrdersEventArgs> GetOrders;
        public event EventHandler<EventArgs> ImpulseProcessed;

        //constructors
        public GameCoordinator()
        {
            InitializeComponent();
        }
        public GameCoordinator(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        //PrismConnection component
        public PrismConnection Prism
        {
            get
            {
                return _prism;
            }
            set
            {
                _prism = value;
                if (_prism != null)
                {
                    //hook into data received message so we can process Orders and Changes msgs
                    _prism.DataMessageReceived += new EventHandler<PrismUserMessageEventArgs>(DataMsgRcvd);
                    _prism.UserLeftRoom += new EventHandler<PrismUserEventArgs>(UserLeftSession);
                }
            }
        }

        //Is the session active?
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool Active
        {
            get
            {
                return _active;
            }
        }

        //Is the session paused?
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool Paused
        {
            get
            {
                return _paused;
            }
            set
            {
                _paused = value;
            }
        }

        //Provide access to the list of orders to the host can process them
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Dictionary<PrismUser, string> Orders
        {
            get
            {
                return _orders;
            }
        }

        //Current Impulse number
        public int Impulse
        {
            get
            {
                return _impulse;
            }
            set
            {
                _impulse = value;
            }
        }

        //Lenth of impulses in milliseconds
        public int ImpulseLengthMS
        {
            get
            {
                return _impulseLengthMS;
            }
            set
            {
                _impulseLengthMS = value;
            }
        }

        //Is the current player the host?
        public bool IsHost
        {
            get
            {
                if (!_prism.LoggedIn)
                    return true;
                if (_prism.ThisUser == null)
                    return true;

                //allow for correct result even before subject starts
                if (_activeMembers.Count == 0 || !_activeMembers.Contains(Prism.ThisUser))
                    return _prism.ThisUser == _prism.Users[0];
                else
                    return _prism.ThisUser == _activeMembers[0];
            }
        }

        //Start the session with the specified impulse number
        public void Start(int impulse)
        {
            if (_prism == null)
                throw new InvalidOperationException("PrismConnection component must be assigned");

            _active = true;
            _impulse = impulse;
            _orders.Clear();

            //add active members, allow it to work in offline mode
            _activeMembers.Clear();           
            foreach (PrismUser user in _prism.Users)
                _activeMembers.Add(user);
            if (_activeMembers.Count == 0)
            {
                PrismUser dummy = new PrismUser();
                _prism.ThisUser = dummy;
                _activeMembers.Add(dummy);
            }

            //launch a thread to handle submission of orders at assigned intervals
            gameThread = new Thread(new ThreadStart(GameThreadExecute));
            gameThread.IsBackground = true;
            gameThread.Start();
        }

        //Stop the session
        public void Stop()
        {
            _active = false;
        }

        //Remove a player
        public void RemoveUser(PrismUser user)
        {
            if (_orders.ContainsKey(user))
                _orders.Remove(user);
            if (_activeMembers.Contains(user))
                _activeMembers.Remove(user);
        }

        //private members
        private bool _active = false;
        private int _impulse = 0;
        private int _impulseLengthMS = 1000;
        private PrismConnection _prism = null;
        private List<PrismUser> _activeMembers = new List<PrismUser>();
        private Dictionary<PrismUser, string> _orders = new Dictionary<PrismUser, string>();
        private Thread gameThread = null;
        private string _changes = null;
        private bool _paused = false;

        //event handler for receipt of Data message - look for Orders and Changes msgs
        private void DataMsgRcvd(object sender, PrismUserMessageEventArgs e)
        {
            //process orders received
            if (e.Msg.StartsWith("ORDERS|"))
            {
                if (!_orders.ContainsKey(e.User))
                    _orders.Add(e.User, e.Msg.Substring(7));
            }
            //process change string
            else if (e.Msg.StartsWith("CHANGES|"))
            {               
                _changes = e.Msg.Substring(8);
            }
        }

        //user has left the room, allow session to continue
        private void UserLeftSession(object sender, PrismUserEventArgs e)
        {
            RemoveUser(e.User);
        }

        //primary game thread
        private void GameThreadExecute()
        {
            DateTime wait = DateTime.Now.AddMilliseconds(ImpulseLengthMS);
            while (Active)
            {
                //delay until next impulse             
                while (DateTime.Now < wait && Active)
                    Thread.Sleep(10);

                //pause
                while (Paused && Active)
                    Thread.Sleep(10);

                //get current orders and submit them
                string orders = "";
                if (GetOrders != null)
                {
                    OrdersEventArgs oea = new OrdersEventArgs();
                    GetOrders(this, oea);
                    orders = oea.Orders;
                }

                //Host submits directly, others send their orders to host
                if (IsHost)                
                    _orders[_prism.ThisUser] = orders;                              
                else
                    _prism.SendData("ORDERS|" + orders);
             
                //wait until all orders are in, or change message comes back
                bool processed = false;
                while (!processed && Active)
                {
                    if (IsHost)
                    {
                        if (_orders.Count == _activeMembers.Count)
                        {
                            //process impulse, send change string to other players
                            if (ProcessOrdersGetChanges != null)
                            {
                                OrdersEventArgs oea = new OrdersEventArgs();
                                ProcessOrdersGetChanges(this, oea);
                                _prism.SendData("CHANGES|" + oea.Orders);
                            }
                            processed = true;
                        }
                    }
                    else
                    {
                        //other players wait to process the change string
                        if (_changes != null)
                        {
                            if (ProcessChanges != null)
                                ProcessChanges(this, new MessageEventArgs(_changes));
                            processed = true;
                        }
                    }
                    if (!processed)
                        Thread.Sleep(10);
                }

                //Allow UI to respond
                _changes = null;
                _orders.Clear();
                if (ImpulseProcessed != null)
                    ImpulseProcessed(this, EventArgs.Empty);

                //determine next impulse end time
                if (Active)
                {
                    _impulse++;
                    wait = wait.AddMilliseconds(ImpulseLengthMS);
                    if (wait < DateTime.Now)
                        wait = DateTime.Now;
                }
            }
        }
    }

    //event handler for passing orders back
    public class OrdersEventArgs : EventArgs
    {
        //public members

        //constructor
        public OrdersEventArgs()
        {
        }

        //access the orders returned
        public string Orders
        {
            get
            {
                return _orders;
            }
            set
            {
                _orders = value;
            }
        }

        //private members
        private string _orders = "";
    }
}
