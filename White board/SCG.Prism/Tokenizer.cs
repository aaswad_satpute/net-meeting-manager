using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;

namespace SCG.Prism
{
    //The Tokenizer class provides static methods to tokenize a list of arguments into a compacted
    //string.  It also provides methods to extract tokens from the compacted string and return them
    //in a variety of data formats
    public static class Tokenizer
    {
        //Strip a single token from a string using the supplied delimiter
        public static string StripToken(ref string s, char delim)
        {
            string ReturnString;

            int n = s.IndexOf(delim);
            if (n == -1)
            {
                ReturnString = s;
                s = "";
            }
            else
            {
                ReturnString = s.Substring(0, n);
                s = s.Substring(n + 1, s.Length - n - 1);
            }
            return ReturnString;
        }

        //Tokenize a list of parameters into a single string.  The string can later be de-tokenized
        //by calling the various GetToken methods of the StringTokenizer class
        public static string Tokenize( params object[] tokens )
        {
            StringBuilder sb = new StringBuilder();
            foreach (object obj in tokens)
            {
                string Token;
                //Convert a bitmap to a string
                if (obj is Bitmap)
                {
                    Bitmap bmp = (Bitmap)obj;
                    MemoryStream ms = new MemoryStream();
                    bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    //Establish version control for bitmap encoding (compress in future version)
                    Token = "V1|";
                    Token += Convert.ToBase64String(ms.ToArray());
                }
                //Standard format for DateTimes
                else if (obj is DateTime)
                {
                    DateTime dt = (DateTime)obj;
                    Token = dt.Year + "|" + dt.Month + "|" + dt.Day + "|" + dt.Hour + "|" +
                        dt.Minute + "|" + dt.Second;
                }
                //Boolean type
                else if (obj is Boolean)
                {
                    bool b = (bool)obj;
                    if (b)
                        Token = "1";
                    else
                        Token = "0";
                }
                //Use default ToString() implementation for all other object types
                else
                {
                    Token = obj.ToString();
                }
                AppendToken(sb, Token);
            }
            return sb.ToString();
        }

        //Add a string token to a StringBuilder.  Tokenizer first appends the length 
        //of the token, followed by the "|" Delimiter.  The actual token is appended afterward
        public static void AppendToken(StringBuilder sb, string token)
        {
            sb.Append(token.Length);
            sb.Append("|");
            sb.Append(token);
        }

        //Strip the next token using the "Length Prefix" protocol
        public static string GetToken(ref string s)
        {
            if (s == "")
                return "";
            int n = Int32.Parse(StripToken(ref s, '|'));
            string ResultString = s.Substring(0, n);
            s = s.Substring(n, s.Length - n);
            return ResultString;
        }

        //Strip the next token and return a Bitmap object
        public static Bitmap GetTokenBitmap(ref string s)
        {
            string Token = GetToken(ref s);
            //We only have 1 version of encoding now, dump "V1" token
            StripToken(ref Token, '|');
            MemoryStream ms = new MemoryStream(Convert.FromBase64String(Token));
            Bitmap bmp = (Bitmap)Bitmap.FromStream(ms);
            return bmp;
        }

        //Strip the next token and return a DateTime object
        public static DateTime GetTokenDateTime(ref string s)
        {
            string Token = GetToken(ref s);
            int Year = Int32.Parse(StripToken(ref Token, '|'));
            int Month = Int32.Parse(StripToken(ref Token, '|'));
            int Day = Int32.Parse(StripToken(ref Token, '|'));
            int Hour = Int32.Parse(StripToken(ref Token, '|'));
            int Minute = Int32.Parse(StripToken(ref Token, '|'));
            int Second = Int32.Parse(StripToken(ref Token, '|'));            
            return new DateTime(Year, Month, Day, Hour, Minute, Second);
        }

        //Strip the next integer token
        public static int GetTokenInt(ref string s)
        {
            string Token = GetToken(ref s);
            return Int32.Parse(Token);
        }

        //Strip the next double token
        public static double GetTokenDouble(ref string s)
        {
            string Token = GetToken(ref s);
            return Double.Parse(Token);
        }

        //Strip the next boolean token
        public static bool GetTokenBool(ref string s)
        {
            if (GetToken(ref s) == "1")
                return true;
            else
                return false;
        }
    }
}
