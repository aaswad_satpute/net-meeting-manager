#region copyright
/*
* Copyright (c) 2007, Dion Kurczek
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DION KURCZEK ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DION KURCZEK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Drawing;

namespace SCG.Prism
{
    //Provides an implementation of user management for a PrismServer.  PrismServer's Implementation
    //property must be assigned to an instance of this componnt, or a derived one, before activation.
    //You can derive from this class to provide specialized user management for your PrismServer.
    [ToolboxBitmap(typeof(PrismServerImplementation), "PrismServerImplementation")]
    public partial class PrismServerImplementation : Component
    {
        //Private fields
        private PrismServer _server;

        //Constructors
        public PrismServerImplementation()
        {
            InitializeComponent();
        }
        public PrismServerImplementation(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        //Access associated PrismServer
        [Browsable(false)]
        public PrismServer Server
        {
            get
            {
                return _server;
            }
            set
            {
                _server = value;
                Initialize();
            }
        }

        //The methods below define a minimal user management scheme that results in all user names
        //being valid, thus the ability to create and store user information is not defined.  Override
        //the methods below to implement a custom user management scheme.

        //Does the specified user name exist in the account database?
        public virtual bool UserExists(string userName)
        {
            return (userName != "");
        }

        //Is the password valid for the supplied user name?
        public virtual bool IsPasswordValid(string userName, string password)
        {
            return (password != "");
        }

        //Lookup and populate user information for the specified user
        public virtual void PopulateUserInfo(PrismUser user)
        {            
        }

        //Check the validity of a User Name, populate an error message if invalid
        public virtual bool CheckUserName(string userName, ref string msg)
        {
            if (userName == "")
            {
                msg = "User Name cannot be blank";
                return false;
            }
            return true;
        }

        //Check the validity of a Password, populate an error message if invalid
        public virtual bool CheckPassword(string password, ref string msg)
        {
            if (password == "")
            {
                msg = "Password cannot be blank";
                return false;
            }
            return true;
        }

        //Store the user information in the database
        public virtual void StoreUserInfo(PrismUser user)
        {
        }

        //Are the specified Room parameters valid?
        public virtual bool CheckRoom(string roomName, int maxUsers, ref string msg)
        {
            if (roomName == "")
            {
                msg = "Room name cannot be blank";
                return false;
            }
            if (maxUsers < 0)
            {
                msg = "Maximum number of users must be greater or equal to zero";
                return false;
            }
            return true;
        }

        //Save server settings somewhere
        public virtual void SaveSettings()
        {
        }

        //Load server settings from somewhere
        public virtual void LoadSettings()
        {
        }

        //This method will be called when the "Server" property is assigned
        protected virtual void Initialize()
        {
        }

        //Process a custom command
        public virtual void ProcessCustomCommand(string commandName, string commandParams)
        {
        }

    }
}
