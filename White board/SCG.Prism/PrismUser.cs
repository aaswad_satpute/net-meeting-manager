using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SCG.Prism
{
    //PrismUser represents information about a "user" of the chat server
    public class PrismUser
    {
        //Private members
        private Dictionary<string, string> _details = new Dictionary<string, string>();
        private Bitmap _glyph = new Bitmap(32, 32);
        private string _userName = "";
        private DateTime _creationDate = DateTime.MinValue;
        private DateTime _lastLoginDate = DateTime.MinValue;
        private string _password = "";
        private int _loginCount = 0;
        private string _firstName = "";
        private string _lastName = "";
        private string _address1 = "";
        private string _address2 = "";
        private string _city = "";
        private string _stateZip = "";
        private string _country = "";
        private string _email = "";
        private string _profile = "";
        private int _latencyMS = 0;

        //Constructor
        public PrismUser()
        {
        }

        //Glyph property is a 32x32 bitmap that the user has selected to represent themselves
        public Bitmap Glyph
        {
            get
            {
                return _glyph;
            }
//All incoming bitmaps are automatically converted to 32x32 images
            set
            {
                if (value != null)
                    _glyph = new Bitmap(value, new Size(32, 32));
            }
        }

        //User Name is a unique string "handle" of this user
        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        //User's password
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }

        //Date user was first established
        public DateTime CreationDate
        {
            get
            {
                return _creationDate;
            }
            set
            {
                _creationDate = value;
            }
        }

        //Login information
        public DateTime LastLoginDate
        {
            get
            {
                return _lastLoginDate;
            }
        }
        public int LoginCount
        {
            get
            {
                return _loginCount;
            }
        }

        //Descriptive information common to all users
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }
        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }
        }
        public string Address1
        {
            get
            {
                return _address1;
            }
            set
            {
                _address1 = value;
            }
        }
        public string Address2
        {
            get
            {
                return _address2;
            }
            set
            {
                _address2 = value;
            }
        }
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                _city = value;
            }
        }
        public string StateZip
        {
            get
            {
                return _stateZip;
            }
            set
            {
                _stateZip = value;
            }
        }
        public string Country
        {
            get
            {
                return _country;
            }
            set
            {
                _country = value;
            }
        }
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }
        public string Profile
        {
            get
            {
                return _profile;
            }
            set
            {
                _profile = value;
            }
        }

        //Latency
        public int LatencyMS
        {
            get
            {
                return _latencyMS;
            }
            internal set
            {
                _latencyMS = value;
            }
        }

        //Details are ad-hoc name/value pairs that can store pieces of info about the user
        public string GetDetail(string Key)
        {
            if (_details.ContainsKey(Key))
                return _details[Key];
            else
                return "";
        }
        public void SetDetail(string Key, string Value)
        {
            _details[Key] = Value;
        }

        //Record the action of a user logging in
        public void RecordLogin()
        {
            _loginCount++;
            _lastLoginDate = DateTime.Now;
        }

        //Presistence - Convert to a string
        public override string ToString()
        {
            StringBuilder sbDetails = new StringBuilder();            
            foreach (KeyValuePair<string, string> kvp in _details)
            {
                Tokenizer.AppendToken(sbDetails, kvp.Key);
                Tokenizer.AppendToken(sbDetails, kvp.Value);
            }
            string DetailString = sbDetails.ToString();
            return Tokenizer.Tokenize( _userName, _password, _creationDate, _lastLoginDate,
              _glyph, _loginCount, _firstName, _lastName, _address1, _address2, _city,
              _stateZip, _country, _email, DetailString, _profile );
        }

        //Populate from a string
        public void FromString(ref string s)
        {
            string Key;
            string Value;
            _userName = Tokenizer.GetToken(ref s);
            _password = Tokenizer.GetToken(ref s);
            _creationDate = Tokenizer.GetTokenDateTime(ref s);
            _lastLoginDate = Tokenizer.GetTokenDateTime(ref s);
            _glyph = Tokenizer.GetTokenBitmap(ref s);
            _loginCount = Tokenizer.GetTokenInt(ref s);
            _firstName = Tokenizer.GetToken(ref s);
            _lastName = Tokenizer.GetToken(ref s);
            _address1 = Tokenizer.GetToken(ref s);
            _address2 = Tokenizer.GetToken(ref s);
            _city = Tokenizer.GetToken(ref s);
            _stateZip = Tokenizer.GetToken(ref s);
            _country = Tokenizer.GetToken(ref s);
            _email = Tokenizer.GetToken(ref s);
            _details.Clear();
            string DetailString = Tokenizer.GetToken(ref s);
            while (DetailString != "")
            {
                Key = Tokenizer.GetToken(ref DetailString);
                Value = Tokenizer.GetToken(ref DetailString);
                _details.Add(Key, Value);
            }
            _profile = Tokenizer.GetToken(ref s);
        }
    }

    //Event handler arguments that accepts a PrismUser parameter
    public class PrismUserEventArgs : EventArgs
    {
        private PrismUser _user;

        public PrismUserEventArgs(PrismUser user)
        {
            _user = user;
        }

        public PrismUser User
        {
            get
            {
                return _user;
            }
        }
    }
    public class PrismUserMessageEventArgs : EventArgs
    {
        private PrismUser _user;
        private string _msg;

        public PrismUserMessageEventArgs(PrismUser user, string msg)
        {
            _user = user;
            _msg = msg;
        }

        public PrismUser User
        {
            get
            {
                return _user;
            }
        }

        public string Msg
        {
            get
            {
                return _msg;
            }
        }
    }
}
