#region copyright
/*
* Copyright (c) 2007, Dion Kurczek
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DION KURCZEK ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DION KURCZEK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SCG.Prism;

namespace ChatNDraw
{

    //Chat N' Draw main form class
    public partial class ChatNDrawForm : Form
    {
        //Private members
        private LoginForm _frmLogin = new LoginForm();
        private List<Stroke> _strokeList = new List<Stroke>();
        private List<Stroke> _outgoing = new List<Stroke>();
        private bool _drawing = false;
        private Point _startDraw = new Point();
        private Bitmap _buffer = new Bitmap(368, 176);
        private SelectRoomForm _frmSelectRoom = new SelectRoomForm();
        private DateTime _started;

        //Constructor
        public ChatNDrawForm()
        {
            InitializeComponent();
            //I think this is safe since only 1 thread is updating the UI anyway, so conflicts are not possible
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        //Login was accepted
        private void connection_LoginOK(object sender, EventArgs e)
        {
            MessageBox.Show("Welcome to PrismServer!  You are now logged in!", "PrismServer Login Success");
            EnableChatControls(true);
        }

        //Login error received from server
        private void connection_LoginError(object sender, MessageEventArgs e)
        {
            MessageBox.Show(e.Msg, "Login Error");
            EnableLoginControls(true);
        }

        //Disconnected from server
        private void connection_Disconnected(object sender, EventArgs e)
        {
            MessageBox.Show("You have been disconnected", "PrismServer Error");
            EnableLoginControls(true);
            EnableChatControls(false);
        }

        //Send chat, called from button click and when they hit "return"
        private void SendChat()
        {           
            if (txtChat.Text != "")
            {
                connection.SendChat(txtChat.Text);
                txtChatLog.AppendText(connection.ThisUser.UserName + ": " + txtChat.Text);
                txtChatLog.AppendText(Environment.NewLine);
                txtChat.Text = "";
                txtChat.Focus();
            }
        }

        //Send a line of chat when send button clicked
        private void btnSend_Click(object sender, EventArgs e)
        {
            SendChat();
        }

        //Received a chat message
        private void connection_ChatMessageReceived(object sender, PrismUserMessageEventArgs e)
        {
            txtChatLog.AppendText(e.User.UserName + "> " + e.Msg);
            txtChatLog.AppendText(Environment.NewLine);
        }

        //Received error message from server
        private void connection_PrismError(object sender, MessageEventArgs e)
        {
            MessageBox.Show(e.Msg, "PrismServer Error");
        }

        //Enable or disable controls based on connection state
        private void EnableLoginControls(bool b)
        {
            btnConnect.Enabled = b;
            btnDisconnect.Enabled = !b;
        }

        //Enable or disable the chat capablity
        private void EnableChatControls(bool b)
        {
            txtChat.Enabled = b;
            btnSend.Enabled = b;
            if (!b)
                lvUsers.Items.Clear();
            btnServerStats.Enabled = b;
            btnRoom.Enabled = b;
            btnLobby.Enabled = b;
        }

        //Clicked Login button, open login dialog
        private void btnConnect_Click(object sender, EventArgs e)
        {            
            switch (_frmLogin.ShowDialog())
            {
                //Login with existing account
                case DialogResult.OK:
                    connection.Host = _frmLogin.txtHost.Text;
                    connection.Port = (int)_frmLogin.numPort.Value;
                    try
                    {
                        connection.Active = true;
                        connection.Login(_frmLogin.txtUserName.Text, _frmLogin.txtPassword.Text);
                        EnableLoginControls(false);
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show(error.Message, "PrismServer Connection Error");
                    }
                    break;
                //Login as a new user
                case DialogResult.Yes:
                    connection.Host = _frmLogin.txtHost.Text;
                    connection.Port = (int)_frmLogin.numPort.Value;
                    try
                    {
                        connection.Active = true;
                        connection.LoginNew(_frmLogin.User);
                        EnableLoginControls(false);
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show(error.Message, "PrismServer Connection Error");
                    }
                    break;
            }
        }

        //Draw the shared board
        private void pnlDraw_Paint(object sender, PaintEventArgs e)
        {            
            Graphics gb = Graphics.FromImage(_buffer);
            Graphics g = e.Graphics;
            Brush b = Brushes.Black;
            Pen p = new Pen(b);
            gb.FillRectangle(b, pnlDraw.ClientRectangle);
            foreach (Stroke s in _strokeList)
            {               
                p.Color = s.Color;
                gb.DrawLine(p, s.Start, s.End);
            }            
            g.DrawImage(_buffer, new Point(0, 0));
        }

        //A User was added to the room - add an icon for them
        private void connection_UserAddedToRoom(object sender, PrismUserEventArgs e)
        {
            ListViewItem li = new ListViewItem();
            li.Text = e.User.UserName;
            imagesUsers.Images.Add(e.User.Glyph);
            li.ImageIndex = imagesUsers.Images.Count - 1;
            lvUsers.Items.Add(li);
            txtChatLog.AppendText(e.User.UserName + " entered the Room");
            txtChatLog.AppendText(Environment.NewLine);
        }

        //A User was double clicked - display user info
        private void lvUsers_DoubleClick(object sender, EventArgs e)
        {
            if (lvUsers.SelectedItems.Count > 0)
            {
                ListViewItem li = lvUsers.SelectedItems[0];
                string UserName = li.Text;
                PrismUser User = connection.FindUser(UserName);
                //Allow editing if it is the logged in user
                if (User == connection.ThisUser)
                    dlgUserInfo.EditState = PrismUserInfoDialogState.EditUser;
                else
                    dlgUserInfo.EditState = PrismUserInfoDialogState.DisplayOnly;
                //Request changes to be saved to PrismServer if made
                if (dlgUserInfo.ShowDialog(User) == DialogResult.OK)
                    if (dlgUserInfo.EditState == PrismUserInfoDialogState.EditUser)
                        connection.SaveUserInfo(User);
            }
        }

        //User info change accepted by server
        private void connection_UserInfoChanged(object sender, PrismUserEventArgs e)
        {
            MessageBox.Show("User Info has changed for: " + e.User.UserName);
            ListViewItem li = FindItem(e.User.UserName);
            if (li != null)
            {                
                int n = li.ImageIndex;
                imagesUsers.Images[n] = e.User.Glyph;
                lvUsers.Refresh();
            }
        }

        //A User has left the room
        private void connection_UserLeftRoom(object sender, PrismUserEventArgs e)
        {
            ListViewItem li = FindItem(e.User.UserName);
            if (li != null)
                lvUsers.Items.Remove(li);
            txtChatLog.AppendText(e.User.UserName + " left the Room");
            txtChatLog.AppendText(Environment.NewLine);

        }

        //Locate a list view item with the specified text
        private ListViewItem FindItem(string s)
        {
            foreach (ListViewItem li in lvUsers.Items)
                if (li.Text == s)
                    return li;
            return null;
        }

        //Mouse is down!
        private void pnlDraw_MouseDown(object sender, MouseEventArgs e)
        {
            _drawing = true;
            _startDraw.X = e.X;
            _startDraw.Y = e.Y;
        }

        //Mouse moved - are we drawing?
        private void pnlDraw_MouseMove(object sender, MouseEventArgs e)
        {
            if (_drawing)
            {
                Stroke s = new Stroke(_startDraw, new Point(e.X, e.Y), dlgPenColor.Color);
                _strokeList.Add(s);                
                if (connection.Active)
                    _outgoing.Add(s);
                _startDraw.X = e.X;
                _startDraw.Y = e.Y;
                pnlDraw.Invalidate();
            }
        }

        //Mouse up - make the final stroke and turn off flag
        private void pnlDraw_MouseUp(object sender, MouseEventArgs e)
        {
            Stroke s = new Stroke(_startDraw, new Point(e.X, e.Y), dlgPenColor.Color);
            _strokeList.Add(s);
            if (connection.Active)
            {
                _outgoing.Add(s);
                StringBuilder sbStrokes = new StringBuilder();
                foreach (Stroke stroke in _outgoing)                
                    Tokenizer.AppendToken(sbStrokes, stroke.ToString());
                _outgoing.Clear();
                connection.SendData(sbStrokes.ToString());
            }
            pnlDraw.Invalidate();
            _drawing = false;        
        }

        //Select a new color
        private void btnSelectColor_Click(object sender, EventArgs e)
        {
            dlgPenColor.ShowDialog();
        }

        //Erase the drawing
        private void btnErase_Click(object sender, EventArgs e)
        {
            _strokeList.Clear();
            if (connection.Active)
                connection.SendData("CLEAR");
            pnlDraw.Invalidate();
        }

        //Received a data message, stroke list, or clear
        private void connection_DataMessageReceived(object sender, PrismUserMessageEventArgs e)
        {
            if (e.Msg == "CLEAR")
            {
                _strokeList.Clear();
                pnlDraw.Invalidate();
            }
            else if (e.Msg.StartsWith("ORDERS") || e.Msg.StartsWith("CHANGES"))
            {
                //nop
            }
            else
            {
                string StrokeString = e.Msg;
                while (StrokeString != "")
                {
                    string token = Tokenizer.GetToken(ref StrokeString);
                    Stroke s = new Stroke(ref token);
                    _strokeList.Add(s);
                }
                pnlDraw.Invalidate();
            }
        }

        //Manually disconnect
        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            connection.Active = false;
            EnableLoginControls(true);
        }

        //User joined the room
        private void connection_JoinedRoom(object sender, RoomNameEventArgs e)
        {
            txtChatLog.AppendText("You joined Room: " + e.RoomName);
            txtChatLog.AppendText(Environment.NewLine);
            lvUsers.Items.Clear();
        }

        //A Room was added
        private void connection_RoomAdded(object sender, RoomNameEventArgs e)
        {
            txtChatLog.AppendText("A Room was created: " + e.RoomName);
            txtChatLog.AppendText(Environment.NewLine);
            //Add room name to select room form list
            _frmSelectRoom.cmbRooms.Items.Add(e.RoomName);
        }

        //Request Server Stats
        private void btnServerStats_Click(object sender, EventArgs e)
        {
            connection.ServerStats();
        }

        //Server stats were received
        private void connection_ServerStatsReceived(object sender, PrismServerStatsEventArgs e)
        {
            dlgServerStats.ShowDialog(e.ServerStats);
        }

        //A room was removed
        private void connection_RoomRemoved(object sender, RoomNameEventArgs e)
        {
            txtChatLog.AppendText("A Room was removed: " + e.RoomName);
            txtChatLog.AppendText(Environment.NewLine);
            //Remove room name from select room form list
            _frmSelectRoom.cmbRooms.Items.Remove(e.RoomName);
        }

        //Return to the lobby
        private void btnLobby_Click(object sender, EventArgs e)
        {
            connection.EnterRoom("Lobby");
        }

        //Enter a room, or create a new room
        private void btnRoom_Click(object sender, EventArgs e)
        {
            if (_frmSelectRoom.ShowDialog() == DialogResult.OK)
            {
                string s = _frmSelectRoom.cmbRooms.Text;
                if (_frmSelectRoom.cmbRooms.Items.IndexOf(s) >= 0)
                    connection.EnterRoom(s);
                else
                    connection.CreateRoom(s, 0);
            }
        }

        //Admin Message was received
        private void connection_AdminMessageReceived(object sender, MessageEventArgs e)
        {
            System.Media.SystemSounds.Asterisk.Play();
            txtChatLog.AppendText("MESSAGE FROM ADMINISTRATOR: " + e.Msg);
            txtChatLog.AppendText(Environment.NewLine);
        }

        //start game
        private void btnStart_Click(object sender, EventArgs e)
        {
            if (!realtime.Active)
            {
                _started = DateTime.Now;
                realtime.Start(0);
            }
            else
                realtime.Stop();
        }

        //return orders string
        private void realtime_GetOrders(object sender, OrdersEventArgs e)
        {
            e.Orders = connection.ThisUser.UserName + ":Orders";
            txtChatLog.AppendText("Submitting Order String: " + e.Orders + Environment.NewLine);
        }

        //Impulse processed, record it
        private void realtime_ImpulseProcessed(object sender, EventArgs e)
        {
            Invoke(new ImpulseProcessedCallback(ImpulseProcessedThreadSafe));
        }
        private delegate void ImpulseProcessedCallback();
        private void ImpulseProcessedThreadSafe()
        {
            //determine length of time it took to process impulse
            TimeSpan length = DateTime.Now - _started;
            _started = DateTime.Now;
            string msg = "Impulse " + realtime.Impulse + " Processed, span = " + length.ToString();
            txtChatLog.AppendText(msg + Environment.NewLine);
        }

        //process the change string that's returned here
        private void realtime_ProcessChanges(object sender, MessageEventArgs e)
        {
            txtChatLog.AppendText("Process Changes: " + e.Msg + Environment.NewLine);
        }

        //process the orders in the Orders list, set the change string
        private void realtime_ProcessOrdersGetChanges(object sender, OrdersEventArgs e)
        {
            e.Orders = "Changes";
            txtChatLog.AppendText("Processing Orders for " + realtime.Orders.Count + " players" + Environment.NewLine);
        }
    }

    //This class represents a single stroke drawn on the drawing board
    internal class Stroke
    {
        //Private members       
        private Point _start = new Point();
        private Point _end = new Point();
        private Color _color = new Color();

        //Constructor
        public Stroke(Point ptStart, Point ptEnd, Color color)
        {
            _start = ptStart;
            _end = ptEnd;
            _color = color;
        }
        public Stroke(ref string s)
        {
            FromString(ref s);
        }

        //Access methods
        public Point Start
        {
            get
            {
                return _start;
            }
        }
        public Point End
        {
            get
            {
                return _end;
            }
        }
        public Color Color
        {
            get
            {
                return _color;
            }
        }

        //Persistence
        public override string ToString()
        {
            return Tokenizer.Tokenize(_start.X, _start.Y, _end.X, _end.Y, _color.R,
                _color.G, _color.B);
        }
        public void FromString(ref string s)
        {
            int R, G, B;
            _start.X = Tokenizer.GetTokenInt(ref s);
            _start.Y = Tokenizer.GetTokenInt(ref s);
            _end.X = Tokenizer.GetTokenInt(ref s);
            _end.Y = Tokenizer.GetTokenInt(ref s);
            R = Tokenizer.GetTokenInt(ref s);
            G = Tokenizer.GetTokenInt(ref s);
            B = Tokenizer.GetTokenInt(ref s);
            _color = Color.FromArgb(R, G, B);
        }
    }

}