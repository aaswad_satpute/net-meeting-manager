#region copyright
/*
* Copyright (c) 2007, Dion Kurczek
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DION KURCZEK ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DION KURCZEK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SCG.Prism;

namespace ChatNDraw
{
    //Provide login information to PrismServer
    public partial class LoginForm : Form
    {      
        //Private members
        private UserInfoForm _frmUserInfo = new UserInfoForm();
        private PrismUser _user = new PrismUser();

        //Constructors
        public LoginForm()
        {
            InitializeComponent();
        }

        //Access the PrismUser object that contains the user information from UserInfo dialog
        public PrismUser User
        {
            get
            {
                return _user;
            }
        }

        //Create a new user
        private void btnNewUser_Click(object sender, EventArgs e)
        {
            if (dlgUserInfo.ShowDialog(_user) == DialogResult.OK)
                DialogResult = DialogResult.Yes;
        }

        //Make sure info is populated
        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (txtHost.Text == "")
            {
                MessageBox.Show("Please specify a Host");
                txtHost.Focus();
                e.Cancel = true;
            }
            if (DialogResult == DialogResult.OK)
            {
                if (txtUserName.Text == "")
                {
                    MessageBox.Show("Please specify a User Name");
                    txtUserName.Focus();
                    e.Cancel = true;
                }
                else if (txtPassword.Text == "")
                {
                    MessageBox.Show("Please specity a Password");
                    txtPassword.Focus();
                    e.Cancel = true;
                }
            }
        }
    }
}