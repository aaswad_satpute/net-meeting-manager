﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ChatNDraw")]
[assembly: AssemblyDescription("Chat 'N Draw for PrismServer")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Silicon Commander Games")]
[assembly: AssemblyProduct("ChatNDraw")]
[assembly: AssemblyCopyright("Copyright ©  2005")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f2baa321-b8a4-4b9c-a3c9-880c4995a12a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("5.1.0.1")]
[assembly: AssemblyFileVersion("1.0.0.0")]
