﻿namespace WinSharer
{
    partial class fmMail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtTo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.buSend = new System.Windows.Forms.Button();
            this.pickDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pickTime = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Attendee";
            // 
            // txtTo
            // 
            this.txtTo.Location = new System.Drawing.Point(89, 19);
            this.txtTo.Multiline = true;
            this.txtTo.Name = "txtTo";
            this.txtTo.Size = new System.Drawing.Size(492, 20);
            this.txtTo.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Meeting Title";
            // 
            // txtSubject
            // 
            this.txtSubject.Location = new System.Drawing.Point(89, 53);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(492, 20);
            this.txtSubject.TabIndex = 3;
            // 
            // buSend
            // 
            this.buSend.Location = new System.Drawing.Point(446, 86);
            this.buSend.Name = "buSend";
            this.buSend.Size = new System.Drawing.Size(135, 23);
            this.buSend.TabIndex = 6;
            this.buSend.Text = "Send Invitation";
            this.buSend.UseVisualStyleBackColor = true;
            this.buSend.Click += new System.EventHandler(this.buSend_Click);
            // 
            // pickDate
            // 
            this.pickDate.Location = new System.Drawing.Point(89, 85);
            this.pickDate.Name = "pickDate";
            this.pickDate.Size = new System.Drawing.Size(133, 20);
            this.pickDate.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Meeting Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(251, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Meeting Time";
            // 
            // pickTime
            // 
            this.pickTime.CustomFormat = "HH:mm";
            this.pickTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.pickTime.Location = new System.Drawing.Point(328, 85);
            this.pickTime.Name = "pickTime";
            this.pickTime.ShowUpDown = true;
            this.pickTime.Size = new System.Drawing.Size(60, 20);
            this.pickTime.TabIndex = 9;
            // 
            // fmMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 134);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pickTime);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pickDate);
            this.Controls.Add(this.buSend);
            this.Controls.Add(this.txtSubject);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTo);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fmMail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Create New Meeting";
            this.Load += new System.EventHandler(this.fmMail_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.Button buSend;
        private System.Windows.Forms.DateTimePicker pickDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker pickTime;
    }
}