
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using RDPCOMAPILib;
using ChatNDraw;

namespace WinSharer
{
    public partial class WinSharer : Form
    {
        private System.Diagnostics.Process procChartServer;
        private ChatNDrawForm myChatNDrawForm;

        public WinSharer()
        {
            InitializeComponent();
        }

        void OnAttendeeDisconnected(object pDisconnectInfo)
        {
            IRDPSRAPIAttendeeDisconnectInfo pDiscInfo = pDisconnectInfo as IRDPSRAPIAttendeeDisconnectInfo;
            LogTextBox.Text += ("Attendee Disconnected: " + pDiscInfo.Attendee.RemoteName + Environment.NewLine);
        }

     
        void OnControlLevelChangeRequest(object pObjAttendee, CTRL_LEVEL RequestedLevel)
        {
            IRDPSRAPIAttendee pAttendee = pObjAttendee as IRDPSRAPIAttendee;
            pAttendee.ControlLevel = RequestedLevel;
        }

        protected RDPSession m_pRdpSession = null;

        private void OnAttendeeConnected(object pObjAttendee)
        {
            IRDPSRAPIAttendee pAttendee = pObjAttendee as IRDPSRAPIAttendee;
            pAttendee.ControlLevel = CTRL_LEVEL.CTRL_LEVEL_VIEW;
            LogTextBox.Text += ("Attendee Connected: " + pAttendee.RemoteName + Environment.NewLine);
        }

        public void WriteToFile(string InviteString)
        {
            using (StreamWriter sw = File.CreateText("inv.xml"))
            {
                sw.WriteLine (InviteString);
            }

        }

        private void StartChatServer()
        {
            procChartServer = new  System.Diagnostics.Process();
            procChartServer.EnableRaisingEvents = false;
            procChartServer.StartInfo.FileName = "PrismServer.exe";
            procChartServer.Start();
        }

        private void StartChatClient()
        {
            myChatNDrawForm = new ChatNDrawForm();
            myChatNDrawForm.Show();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                m_pRdpSession = new RDPSession();
                fmMail myfmMail = new fmMail();
                

                m_pRdpSession.OnAttendeeConnected += new _IRDPSessionEvents_OnAttendeeConnectedEventHandler(OnAttendeeConnected);
                m_pRdpSession.OnAttendeeDisconnected += new _IRDPSessionEvents_OnAttendeeDisconnectedEventHandler(OnAttendeeDisconnected);
                m_pRdpSession.OnControlLevelChangeRequest += new _IRDPSessionEvents_OnControlLevelChangeRequestEventHandler(OnControlLevelChangeRequest);

                m_pRdpSession.Open();
                IRDPSRAPIInvitation pInvitation = m_pRdpSession.Invitations.CreateInvitation("WinPresenter", "PresentationGroup", "", 5);
                string invitationString = pInvitation.ConnectionString;
                WriteToFile(invitationString);
                myfmMail.sTicket = invitationString;
                myfmMail.ShowDialog(this);

                StartChatServer();

                LogTextBox.Text += "Presentation Started. Your Desktop is being shared." + Environment.NewLine;

                StartChatClient();
            }
            catch (Exception ex)
            {
                LogTextBox.Text += "Error occured while starting presentation. Error: " + ex.ToString() + Environment.NewLine;
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            try
            {
                m_pRdpSession.Close();
                LogTextBox.Text += "Presentation Stopped." + Environment.NewLine;
                Marshal.ReleaseComObject(m_pRdpSession);
                m_pRdpSession = null;
            }
            catch (Exception ex)
            {
                LogTextBox.Text += "Error occured while stopping presentation. Error: " + ex.ToString();
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void WinSharer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (procChartServer != null)
            {
                if (procChartServer.HasExited == false)
                    procChartServer.Kill();
            }
        }

        private void WinSharer_Load(object sender, EventArgs e)
        {

        }

    }
}