

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AxRDPCOMAPILib;
using ChatNDraw;
using System.Diagnostics;               // For prcesss related information
using System.Runtime.InteropServices;   // For DLL importing 
using MailClient;


namespace WinViewer
{
    public partial class WinViewer : Form
    {
        public string sTicket;
        private ChatNDrawForm myChatNDrawForm;

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        public WinViewer()
        {
            InitializeComponent();
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            fmTicket myfmTicket = new fmTicket();
            myfmTicket.ShowDialog(this);

            //string ConnectionString = ReadFromFile();
            string ConnectionString = myfmTicket.sTicket;

            if (ConnectionString != null)
            {
                try
                {
                    StreamReader re1 = File.OpenText("user.txt");
                    string input1 = null;
                    string sAdressee1 = "";
                    while ((input1 = re1.ReadLine()) != null)
                    {
                        sAdressee1 = input1;
                    }
                    re1.Close();
                    //Aaswad
                    pRdpViewer.Connect(ConnectionString, sAdressee1, "");
                    // pRdpViewer.Connect(ConnectionString, "Veiwer1", "");
                }
                catch (Exception ex)
                {
                    LogTextBox.Text += ("Error in Connecting. Error Info: " + ex.ToString() + Environment.NewLine);
                }
            }
        }

        private void DisconnectButton_Click(object sender, EventArgs e)
        {
            pRdpViewer.Disconnect();
        }

        private string ReadFromFile()
        {
            string ReadText = null;
            string FileName = null;
            string[] args = Environment.GetCommandLineArgs();
            
            if (args.Length == 2)
            {
                if (!args[1].EndsWith("inv.xml"))
                {
                    FileName = args[1] + @"\" + "inv.xml";
                }
                else
                {
                    FileName = args[1];
                }
            }
            else
            {
                FileName = "inv.xml";
            }
            
            LogTextBox.Text += ("Reading the connection string from the file name " +
                FileName + Environment.NewLine);
            try
            {
                using (StreamReader sr = File.OpenText(FileName))
                {
                    ReadText = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                LogTextBox.Text += ("Error in Reading input file. Error Info: " + ex.ToString() + Environment.NewLine);
            }
            return ReadText;
        }

        private void OnConnectionEstablished(object sender, EventArgs e)
        {
            LogTextBox.Text += "Connection Established" + Environment.NewLine;
        }

        private void OnError(object sender, _IRDPSessionEvents_OnErrorEvent e)
        {
            int ErrorCode = (int)e.errorInfo;
            LogTextBox.Text += ("Error 0x" + ErrorCode.ToString("X") + Environment.NewLine);
        }

        private void OnConnectionTerminated(object sender, _IRDPSessionEvents_OnConnectionTerminatedEvent e)
        {
            LogTextBox.Text += "Connection Terminated. Reason: " + e.discReason + Environment.NewLine;
        }

        private void ControlButton_Click(object sender, EventArgs e)
        {
            pRdpViewer.RequestControl(RDPCOMAPILib.CTRL_LEVEL.CTRL_LEVEL_INTERACTIVE);
        }

        private void OnConnectionFailed(object sender, EventArgs e)
        {
            LogTextBox.Text += "Connection Failed." + Environment.NewLine;
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            
            

            myChatNDrawForm = new ChatNDrawForm();
            myChatNDrawForm.Show(this);
        }

        private void StartFaceDetection()
        { 
            System.Diagnostics.Process procFaceDetection = new System.Diagnostics.Process();
            procFaceDetection.EnableRaisingEvents = false;
            procFaceDetection.StartInfo.FileName = "VidCap.exe";
            procFaceDetection.Start();
            procFaceDetection.WaitForExit();
                        
            //Ajay-Todo:
            //1. Send email to server
            StreamReader re = File.OpenText("Atd.aaswad");
            string input = null;
            string sMessage = "";
            while ((input = re.ReadLine()) != null)
            {
                sMessage = input;
            }
            re.Close();

            /*MailClient.MailClient myMailClient = new MailClient.MailClient();
            if (sMessage.Trim() == "")
            {
                sMessage = "Absent";
            }

           re = File.OpenText("user.txt");
            input = null;
            string sAdressee = "";
            while ((input = re.ReadLine()) != null)
            {
                sAdressee = input;
            }
            re.Close();*/

            //myMailClient.SendMail(sAdressee, "aaswadssmartmeetingmanager", "server.asm2@gmail.com", sMessage, "");
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            StartFaceDetection();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripButton6_Click(sender,e);
            timer1.Stop();
        }


    }
}