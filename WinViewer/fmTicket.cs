﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinViewer
{
    public partial class fmTicket : Form
    {
        public string sTicket;

        public fmTicket()
        {
            InitializeComponent();
        }

        private void buJoin_Click(object sender, EventArgs e)
        {
            sTicket = txtTicket.Text;
            this.DialogResult = DialogResult.OK;
        }

    }
}
